trigger ImageLinksTrigger on ImageLinks__c (after insert,after update, before insert, before update) {
    //--------------------------------------------------------------------------------
    //INITIALIZATION 
    //--------------------------------------------------------------------------------
    try{
        ImageLinksHelper.initialize(trigger.new);
    }
    catch(Exception e){
        system.debug(e.getMessage()+'*****'+e.getStackTraceString());
    }
    //--------------------------------------------------------------------------------
    //BEFORE-EVENT PROCESSING
    //--------------------------------------------------------------------------------
    if(trigger.isBefore){
        //-------------------------
        //BEFORE Logic added by Abhishek Sep 18 2015
        ImageLinksHelper.updateProperty(trigger.new);
        //-------------------------
        if(trigger.isInsert){
        //-------------------------
        //BEFORE INSERT
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isUpdate){
        //-------------------------
        //BEFORE UPDATE
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isDelete){
        //-------------------------
        //BEFORE DELETE
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isUnDelete){
        //-------------------------
        //BEFORE UNDELETE
        //-------------------------
        //Nothing yet...
        }
    }
    //-----------------------------------------------------------------------------------
    //AFTER-EVENT PROCESSING
    //-----------------------------------------------------------------------------------
    if(trigger.isAfter){
        if(trigger.isInsert){
            ImageLinksHelper.setMinutesOnProperty();
            ImageLinksHelper.setAllImagesReceivedFlag();
            ImageLinksHelper.verifyLocationGPS();
        }
        if(trigger.isUpdate){
            ImageLinksHelper.setMinutesOnProperty();
            ImageLinksHelper.setAllImagesReceivedFlag();
            ImageLinksHelper.verifyLocationGPS();
        }
        if(trigger.isDelete){
        //-------------------------
        //AFTER DELETE
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isUndelete){
        //-------------------------
        //AFTER UNDELETE
        //-------------------------
        //Nothing yet...
        }
    }
    //---------------------------------------------------------------------------
    //FINALIZATION
    //---------------------------------------------------------------------------
    ImageLinksHelper.finalize();
}