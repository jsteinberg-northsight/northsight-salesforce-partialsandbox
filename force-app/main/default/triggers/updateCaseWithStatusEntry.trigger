trigger updateCaseWithStatusEntry on Case (before update)
{
    /**
    //Get unique set of User Ids for the cases
    Set<Id> ownerSet = new Set<Id>();
    for (Case currentCase : Trigger.new)
    {
        ownerSet.add(currentCase.OwnerId);
    }
    
    //Create a user to contact map
    Map<Id, Id> userContactMap = new Map<Id, Id>();
    for (User currentUser : [select id, contactId from user where id IN: ownerSet])
    {
        userContactMap.put(currentUser.Id, currentUser.contactId);
    }
    
    //Padmesh Soni (06/18/2014) - SM-172 - Northsight 6/12/2014
    //Query record of Record Type of Case object
    //List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
                                        //AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
    
    //Padmesh Soni (06/18/2014) - SM-172 - Northsight 6/12/2014
    //Set to hold all customer type Case record types Ids
    /*
    Set<Id> customerRecordTypes = new Set<Id>();
    
    //Padmesh Soni (06/18/2014) - SM-172 - Northsight 6/12/2014
    //Loop through RecordType query result
    for(RecordType recordType : [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
                                    AND SobjectType = 'Case' AND IsActive = true]) {
        
        //populate set of recordtype ids
        customerRecordTypes.add(recordType.Id);
    }
    */
    //Loop thru bulk insert
    /**for (integer i = 0; i < Trigger.new.size(); i++)
    {   
        //if case owner's contact Id does not match the case's contact id, clear it
        //Padmesh Soni (06/16/2014) - SM-172 - Northsight 6/12/2014
        //Code Commented
        //Padmesh Soni (06/18/2014) - SM-172 - Northsight 6/12/2014
        //New condition added  If the record type is not "SUPPORT" then allow the code
        //if (Trigger.new[i].RecordTypeId != recordTypes[0].Id && userContactMap.get(Trigger.new[i].OwnerId) != Trigger.new[i].ContactId)
        //if (!customerRecordTypes.contains(Trigger.new[i].RecordTypeId) && userContactMap.get(Trigger.new[i].OwnerId) != Trigger.new[i].ContactId)
        if(userContactMap.get(Trigger.new[i].OwnerId) != Trigger.new[i].ContactId)
            Trigger.new[i].ContactId = null;
        
        //Do we need to update the status record for this Case?
        //if (caseItem.Last_Status_Update__c <> null) 
        //{
            if (Trigger.new[i].OwnerId <> Trigger.old[i].OwnerId)
            {
                //Update Case
                Trigger.new[i].Last_Status_By_Current_Owner__c = false;
                Trigger.new[i].Last_Status_Update__c = null;
                Trigger.new[i].Last_Expected_Upload_Date__c = null;
                Trigger.new[i].Last_Delay_Reason__c = '';
                Trigger.new[i].Last_Status_Explanation__c = '';
                Trigger.new[i].Last_Order_Status__c = '';
                Trigger.new[i].Status_Count_by_Vendor__c = 0;
            }
        //} 
    }
    **/
}