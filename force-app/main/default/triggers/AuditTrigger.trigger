trigger AuditTrigger on Audit__c (after delete, after insert, after undelete) {
//--------------------------------------------------------------------------------
//INITIALIZATION 
//--------------------------------------------------------------------------------
AuditTriggerHelper.initialize(trigger.new,trigger.oldMap, trigger.newMap);
//--------------------------------------------------------------------------------
//BEFORE-EVENT PROCESSING
//--------------------------------------------------------------------------------
if(trigger.isBefore){
	if(trigger.isInsert){
		//-------------------------
		//BEFORE INSERT
		//-------------------------
		//Nothing yet...
	}
	if(trigger.isUpdate){
		//-------------------------
		//BEFORE UPDATE
		//-------------------------
		//Nothing yet...
	}
	if(trigger.isDelete){
		//-------------------------
		//BEFORE DELETE
		//-------------------------
		//Nothing yet...
	}
}
//-----------------------------------------------------------------------------------
//AFTER-EVENT PROCESSING
//-----------------------------------------------------------------------------------
if(trigger.isAfter){
	if(trigger.isInsert){
		//--------------------------
		//AFTER INSERT
		//--------------------------
		AuditTriggerHelper.updateOrderImportFields();
	}
	if(trigger.isUpdate){
		//--------------------------
		//AFTER UPDATE
		//--------------------------
		//DO NOTHING
		
	}
	if(trigger.isDelete){	
		//--------------------------
		//AFTER DELETE
		//--------------------------
		AuditTriggerHelper.updateOrderImportFields();
	}
	if(trigger.isUnDelete){
		//--------------------------
		//AFTER UNDELETE
		//--------------------------
		AuditTriggerHelper.updateOrderImportFields();
	}
	
}
//---------------------------------------------------------------------------
//FINALIZATION
//---------------------------------------------------------------------------
AuditTriggerHelper.finalize();
}