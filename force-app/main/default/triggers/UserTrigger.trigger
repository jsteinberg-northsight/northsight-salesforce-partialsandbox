/**
 *  Description     :   This trigger is used for handling all pre and post DML operation functionality on User standard object.
 *
 *  Created By      :   
 *
 *  Created Date    :   
 *
 *  Current Version :   V1.3
 *
 *  Revisiion Logs  :   V1.0 - Created
 *                      V1.1 - Modified - Padmesh Soni(04/09/2014) - Pre processing functionality added for calling handler function which is using for
 *                                        validate Alias from another existing user. 
 *                      V1.2 - Modified - Padmesh Soni(10/06/2014) - SM-232: Prevent parenthesis from being in names
 *                      V1.3 - Modified - Padmesh Soni(10/21/2014) - VCP-32:Prevent certain Profiles from being selected
 **/
trigger UserTrigger on User (after insert, before insert, before update) {
    
    if (Trigger.isInsert && Trigger.isAfter) {
        /* After Insert */
        UserTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
    }
    
    //Check for event type
    if(Trigger.isBefore) {
        
        //Check for request type
        if(Trigger.isInsert || Trigger.isUpdate) {
            
            //Code added - Padmesh Soni(04/09/2014) - Pre processing functionality added for 
            //calling handler function which is using for validate Alias from another existing user.
            //Call method to validate User's Alias is not duplicate
            UserTriggerHandler.validateUniqueAlias(Trigger.new, Trigger.oldMap);
            
            //Code added - Padmesh Soni(10/06/2014) - SM-232: Prevent parenthesis from being in names
            //Call helper class method
            UserTriggerHandler.validateParenthesisInAliasOrNames(Trigger.new, Trigger.oldMap);
            
            //Code added - Padmesh Soni(10/21/2014) - VCP-32:Prevent certain Profiles from being selected
            //Call helper class method
            UserTriggerHandler.preventUsersProfile(Trigger.new, Trigger.oldMap);
        }
    }
}