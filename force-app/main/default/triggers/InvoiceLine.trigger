trigger InvoiceLine on Invoice_Line__c (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
		if (Trigger.isBefore) {
		    if (Trigger.isInsert) {
				InvoiceLineTriggerHelper.setInvoiceLineFieldsFromParentInvoice(trigger.new);
		    } 
		    if (Trigger.isUpdate) {
		    	InvoiceLineTriggerHelper.setInvoiceLineFieldsFromParentInvoice(trigger.new);
		    }
		    if (Trigger.isDelete) {
		    	for(Invoice_Line__c il : trigger.old){
		    		if (il.locked__c == true)
		    			il.addError('cannot delete a locked invoice line');
		    	
		    	}
		    
		    }
		}

		if (Trigger.IsAfter) {
			if (Trigger.isInsert) {} 
			if (Trigger.isUpdate) {}
			if (Trigger.isDelete) {}
	  
		}
}