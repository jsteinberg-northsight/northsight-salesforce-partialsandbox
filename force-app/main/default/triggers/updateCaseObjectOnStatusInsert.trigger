trigger updateCaseObjectOnStatusInsert on Status__c (after insert)
{
    Set<string> caseIdList = new Set<string>();
    
    //Loop thru all the records getting inserted and get the unique set of case Ids
    for (Status__c statusItemForCaseId : Trigger.new)
    {
        //If case ID is not in the list, add it
        if (caseIdList.contains(statusItemForCaseId.Case__c) == false)
            caseIdList.add(statusItemForCaseId.Case__c);
    }
    
    //Padmesh Soni (06/17/2014) - Support Case Record Type
    //Query record of Record Type of Case object
	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
										AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
	
	//Get in-memory table of Cases being affected
    //Padmesh Soni (06/17/2014) - Support Case Record Type
    //New filter added of record type
    List<Case> caseList = 
        [select ownerId,
                last_status_update__c,
                last_expected_upload_date__c,
                last_delay_reason__c,
                last_status_explanation__c,
                status_needed__c,
                last_status_by_current_owner__c,
                Status_Count_by_Vendor__c 
         from Case where id IN :caseIdList AND RecordTypeId NOT IN: recordTypes];
    
    //Loop thru bulk insert
    for (Status__c statusItem : Trigger.new)
    {
        //get case object
        for (Case caseObject : caseList) 
        {
            //Match - Do updates
            if (statusItem.Case__c == caseObject.Id)
            {
                caseObject.Last_Status_Update__c = Datetime.now();
                caseObject.Last_Expected_Upload_Date__c = statusItem.Expected_Upload_date__c;
                caseObject.Last_Delay_Reason__c = statusItem.Delay_Reason__c;
                caseObject.Last_Status_Explanation__c = statusItem.Explanation__c;
                caseObject.Status_Needed__c = false;
                caseObject.Last_Order_Status__c = statusItem.Order_Status__c;
                
                if (caseObject.Status_Count_by_Vendor__c == null)
                  caseObject.Status_Count_by_Vendor__c = 1;
                else
                  caseObject.Status_Count_by_Vendor__c = caseObject.Status_Count_by_Vendor__c + 1;
                
                if (caseObject.Last_Status_By_Current_Owner__c == false)
                {
                    if (caseObject.OwnerId == statusItem.CreatedById)
                    {
                        caseObject.Last_Status_By_Current_Owner__c = true;  
                    }
                }
            }
        } 
    }
    
    update caseList; 
}