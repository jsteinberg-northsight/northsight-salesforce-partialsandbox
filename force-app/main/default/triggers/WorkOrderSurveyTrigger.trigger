trigger WorkOrderSurveyTrigger on Work_Order_Survey__c (after delete, after insert, after undelete, 
after update) {
	
	if(trigger.old != null){
		for(sObject o: trigger.old){
			Work_Order_Survey__c oldSurvey = (Work_Order_Survey__c)o;
			OrderAssignmentHelper.pruvanPushSet.add(oldSurvey.Work_Order_Number__c);
		}
	}
	
	if(trigger.new != null){
		for(sObject o: trigger.new){
			Work_Order_Survey__c survey = (Work_Order_Survey__c)o; 
			OrderAssignmentHelper.pruvanPushSet.add(survey.Work_Order_Number__c);
		}
	}

}