trigger WOInstructionsTrigger on Work_Order_Instructions__c (before insert, after delete, after insert, after undelete, after update) {

	//Check for event type
	if(Trigger.isAfter) {
		
		//Check for request type
		if(Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete || Trigger.isUnDelete) {
			
			//Call helper class method to update Summary on Work Order
			WOInstructionsTriggerHelper.updateVendorSummaryOnOrder(Trigger.new, Trigger.oldMap);
		}
	}
	
	//Check for event type
	if(Trigger.isBefore) {
		
		//Check for request type
		if(Trigger.isInsert) {
			
			//Call helper class method to update Summary on Work Order
			WOInstructionsTriggerHelper.updateSortFromLine(Trigger.new);
		}
	}
/**set<Id> summariesNeeded = new Set<Id>();
for(Work_Order_Instructions__c ins:trigger.new){
	if(trigger.isDelete){
		summariesNeeded.add(ins.Case__c);
	}
	if(trigger.isUnDelete){
		summariesNeeded.add(ins.Case__c);
	}
	if(trigger.isInsert){
		summariesNeeded.add(ins.Case__c);
	}
	if(trigger.isUpdate){
		Work_Order_Instructions__c old = trigger.oldMap.get(ins.Id);
		if(ins.HashKey__c!=old.HashKey__c || ins.RecordTypeId!=old.RecordTypeId){
			summariesNeeded.add(ins.Case__c);
		}
	}
	
}
if(summariesNeeded.size()>0){
	Case[] summaryCases = [select Id, (Select Instruction_Type__c,Item_Instructions__c from Work_Order_Instructions__r where RecordType.DeveloperName = 'Vendor_Instructions') from Case where id in :summariesNeeded];
	for(Case c:summaryCases){
		string summary = '';
		for(Work_Order_Instructions__c i:c.Work_Order_Instructions__r){
			/*summary+='<b>'+i.Instruction_Type__c!=null?i.Instruction_Type__c.escapeHtml4():''+'&nbsp;-&nbsp;</b>';
			summary+=i.Item_Instructions__c!=null?i.Item_Instructions__c:''+'<br/><br/>';*/
			/**string summaryType = i.Instruction_Type__c!=null?i.Instruction_Type__c:'';
			string summaryInstructions = i.Item_Instructions__c!=null?i.Item_Instructions__c:'';
			summary+='<b>'+summaryType+' - '+'</b>';
			summary+=summaryInstructions+'<br/><br/>';
		}
		c.Vendor_Instructions_Summary__c = summary;
	}		
	update summaryCases;
}**/

}