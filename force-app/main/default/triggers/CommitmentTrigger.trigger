trigger CommitmentTrigger on Client_Commitment__c (before insert) {
Set<Id> caseIds = new Set<Id>();
for(Client_Commitment__c c:trigger.new){
	if(caseIds.contains(c.Work_Order__c)){
		c.Work_Order__c.AddError('Cannot add more than one new Commitment to the same Work Order.  Duplicate Commit found for Work Order: '+c.Work_Order__c);
	}
	caseIds.add(c.Work_Order__c);
}
List<Client_Commitment__c> oldCommitments = [select Id,Sync_Status__c from Client_Commitment__c where Work_Order__c in: caseIds and Sync_Status__c = 'Unsent'];
for(Client_Commitment__c c: oldCommitments){
	c.Sync_Status__c = 'Void';
}
update oldCommitments;

}