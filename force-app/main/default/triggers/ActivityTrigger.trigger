trigger ActivityTrigger on Task (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	//--------------------------------------------------------------------------------
    //INITIALIZATION 
    //--------------------------------------------------------------------------------
    ActivityHelper.initialize(trigger.new);
    //--------------------------------------------------------------------------------
    //BEFORE-EVENT PROCESSING
    //--------------------------------------------------------------------------------
    if(trigger.isBefore){
        //-------------------------
        //BEFORE INSERT
        //-------------------------
        if(trigger.isInsert){
        //-------------------------
        //BEFORE INSERT
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isUpdate){
        //-------------------------
        //BEFORE UPDATE
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isDelete){
        //-------------------------
        //BEFORE DELETE
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isUnDelete){
        //-------------------------
        //BEFORE UNDELETE
        //-------------------------
        //Nothing yet...
        }
    }
    //-----------------------------------------------------------------------------------
    //AFTER-EVENT PROCESSING
    //-----------------------------------------------------------------------------------
    if(trigger.isAfter){
        if(trigger.isInsert){
        	ActivityHelper.setCaseActivityFields();
        }
        if(trigger.isUpdate){
        //-------------------------
        //AFTER UPDATE
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isDelete){
        //-------------------------
        //AFTER DELETE
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isUndelete){
        //-------------------------
        //AFTER UNDELETE
        //-------------------------
        //Nothing yet...
        }
    }
    //---------------------------------------------------------------------------
    //FINALIZATION
    //---------------------------------------------------------------------------
    ActivityHelper.finalize();
}