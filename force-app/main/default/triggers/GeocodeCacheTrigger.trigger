trigger GeocodeCacheTrigger on Geocode_Cache__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	
	
	//=====================================================================================================================================
	//Trigger is Before
	//=====================================================================================================================================
	if(trigger.isBefore){
		if(trigger.isUpdate){
			if(trigger.newMap != null){
				GeocodeCacheHelper.getTriggerNewMap(trigger.newMap);
			}
			
			if(GeocodeCacheHelper.geosAlreadyUpdated == false){
				GeocodeCacheHelper.createInspectionWorkOrder();
			}
		}
	}
	
	//=====================================================================================================================================
	//Trigger is After
	//=====================================================================================================================================
	
	
	
	
	
	//GeocodeCacheHelper.finalize();
}