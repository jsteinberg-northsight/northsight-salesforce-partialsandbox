//-------------------------------------
//Complex Assignment: Modified round robin using assignment zones, priorities, limits, and variable number of assignments per turn  
//- Don K.  
//- Salesforce Superheroes LLC.
//-------------------------------------
trigger OrderAssignmentPreprocess on Case (before insert, before delete, before update) {
    OrderAssignmentHelper.triggerDepth++;

    if(!OrderAssignmentHelper.initialized){
        OrderAssignmentHelper.initialize();

        //Check for event type
        if(Trigger.isBefore) {
            //Check for request type
            if(Trigger.isUpdate) {
                //Call helper class method
                OrderAssignmentHelper.updateCaseStatusEntry(Trigger.new, Trigger.oldMap);
            }
        }
    }
    //Code added - Padmesh Soni (10/03/2014) - SM-211: Workflow needed to update last open verification values on CL orders
    if(Trigger.isInsert || Trigger.isUpdate) {
        OrderAssignmentHelper.populateLastOpenVerification(Trigger.new, Trigger.oldMap);
    }


    if(trigger.isDelete){

    }
    else{
        PruvanStatusLogicHelper.initialize(trigger.new);
        PruvanStatusLogicHelper.setOrderStatuses();
        for(Case c:Trigger.new){
            if(c.Loan_Type__c!=NULL){
                c.Loan_Type__c = c.Loan_Type__c.Replace('LoanType.','');
            }
        }
        if(Trigger.isInsert || Trigger.isUpdate)
            OrderAssignmentHelper.populateContactIdOnCase(trigger.new, trigger.oldMap);
        //--------------------------------------
    }
}