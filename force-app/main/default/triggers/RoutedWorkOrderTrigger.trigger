trigger RoutedWorkOrderTrigger on Route_Stop__c (after insert, after update, before delete) {

if(trigger.isDelete){
	List<Status__c> statusesToDelete = [select id from Status__c where Route_Stop__c in :trigger.oldMap.keySet()];
	delete statusesToDelete;
}
else{
	List<Status__c> newStatuses = new List<Status__c>();
	Map<Id,Route_Stop__c> stopMap = new Map<Id,Route_Stop__c>([select id,Route__r.Est_Completion_Date__c from Route_Stop__c where Id in :trigger.newMap.keySet()]);
	if(trigger.isInsert){
	for(Route_Stop__c routeStop:trigger.new)
		{
			if(routeStop.Work_Order__c!=null){
				newStatuses.add(new Status__c(Case__c=routeStop.Work_Order__c, Route_Id__c=routeStop.Route__c, Route_Stop__c = routeStop.Id, Order_Status__c = 'On Route', Auto_Created__c=true, Delay_Reason__c='Not Delayed', Expected_Upload_Date__c=stopMap.get(routeStop.Id).Route__r.Est_Completion_Date__c));
			}	
		}
	}
	
	if(trigger.isUpdate){
	List<Id> stopIdsForStatusDelete = new List<Id>();
	for(Route_Stop__c routeStop:trigger.new)
		{
			if(routeStop.Work_Order__c != trigger.oldMap.get(routeStop.Id).Work_Order__c){
				if(routeStop.Work_Order__c!=null){
					newStatuses.add(new Status__c(Case__c=routeStop.Work_Order__c, Route_Id__c=routeStop.Route__c, Route_Stop__c = routeStop.Id, Order_Status__c = 'On Route', Auto_Created__c=true, Delay_Reason__c='Not Delayed', Expected_Upload_Date__c=stopMap.get(routeStop.Id).Route__r.Est_Completion_Date__c));
				}
				if(trigger.oldMap.get(routeStop.Id).Work_Order__c!=null){
					stopIdsForStatusDelete.add(routeStop.Id);
				}
			}
		}
	List<Status__c> statusesToDelete = [select id from Status__c where Route_Stop__c in :stopIdsForStatusDelete];
	delete statusesToDelete;
	}
	if(newStatuses.size()>0){
		insert newStatuses;
	}
}
}