trigger OrderAssignmentPostProcess on Case (after insert, after delete, after undelete, after update) {
    if(!trigger.isUndelete){        
        OrderAssignmentHelper.triggerDepth--;
    }
    System.assert(OrderAssignmentHelper.triggerDepth >= 0,'triggerDepth less than 0!');
    
    public List<Geocode_Cache__c> LastGrassCutDate = new List<Geocode_Cache__c>();
    private Map<Id,Case> trackingMap = new Map<Id,Case>();
    private Map<Id,List<Case>> submittedCountMap = new Map<Id,List<Case>>();
    
    if(trigger.newMap != null){
        OrderAssignmentHelper.getTriggerNewMap(trigger.newMap);
    }
    if(trigger.oldMap != null){
        OrderAssignmentHelper.getTriggerOldMap(trigger.oldMap);
    } 
    //Update counts after assignments 
    Set<id> countsToUpdate = new Set<id>();
    boolean needCountsUpdate = false;
    
    if(!trigger.isDelete&&!trigger.isUndelete){
        //workCodeProcessList
        Map<Id, Case> triggerOldMap = trigger.oldMap;
        List<Case> workCodeProcessList = new List<Case>();
        for(Case c:trigger.new){
            if(triggerOldMap == null || triggerOldMap.get(c.Id) == null || c.Work_Code__c != triggerOldMap.get(c.Id).Work_Code__c){
                
                    workCodeProcessList.add(c);
                
            }
            
        }
        if(workCodeProcessList.size()>0){
            //create work order surveys
            WorkOrderSurveyHelper.initialize(workCodeProcessList);
            WorkOrderSurveyHelper.finalize();
            //create work order instructions
            WorkOrderInstructionsHelper.initialize(workCodeProcessList);
            WorkOrderInstructionsHelper.finalize();
        }
    }
    if(trigger.isDelete){
        needCountsUpdate = true;
        for(Case c:trigger.old){
            countsToUpdate.add(c.ContactId);
        }
    }else if(trigger.isInsert){
            
        //if(OrderAssignmentHelper.propsAlreadyUpdated == false){
        //  OrderAssignmentHelper.propertyUpdateQueries();
        //}
        needCountsUpdate = true;
        for(Case c:trigger.new){
            
            if(c.ContactId!=null){
                countsToUpdate.add(c.ContactId);
            }
        }
        
        //inserted orders may be late, check this using the EmergencyCommitHelper
        EmergencyCommitHelper.initialize(trigger.New);
        EmergencyCommitHelper.createEmergencyCommitments();
        EmergencyCommitHelper.finalize();
    }
    else if(trigger.isUpdate){

        submittedCountMap = new Map<Id,List<Case>>();
        //set to hold the ids of cases that have their inspections checkbox checked
        Set<Id> ordersToMake = new Set<Id>();
        
        for(Case c:trigger.new){
            //add the id to ordersToMake if Generate_Follow_Up__c is true
            if(c.Generate_Follow_Up__c == true && trigger.OldMap.get(c.Id).Generate_Follow_Up__c != true){
                ordersToMake.add(c.Id);
            }
            
            //need to take into account both 'Pre-Pending' AND 'Pending' statuses since not all orders would be set to 'Pre-Pending' (a workflow rule updates )
            if(c.ContactId != null && ((c.Approval_Status__c == 'Pre-Pending' || c.Approval_Status__c == 'Pending')
            &&(trigger.OldMap.get(c.Id).Approval_Status__c != 'Pre-Pending' && trigger.OldMap.get(c.Id).Approval_Status__c != 'Pending')))
            {
                if(!trackingMap.containsKey(c.Id)){
                    trackingMap.put(c.Id, c);
                }
                
                Contact con = OrderAssignmentHelper.contactsToUpdate.get(c.contactId);
                if(con==null){
                    OrderAssignmentHelper.contactsToUpdate.put(c.contactId, new Contact(Id=c.contactId,Last_Submitted_Date_Time__c = DateTime.Now()));
                }else{
                    con.Last_Submitted_Date_Time__c = DateTime.Now();
                }
            }
    
            if(c.OwnerId!=trigger.OldMap.get(c.Id).OwnerId || c.ContactId!=trigger.OldMap.get(c.Id).ContactId||c.Serviceable__c!=trigger.OldMap.get(c.Id).Serviceable__c){
                needCountsUpdate = true;
    
                
                if(c.ContactId!=null){
                    countsToUpdate.add(c.ContactId);
                }
                
                
                if(trigger.OldMap.get(c.Id).ContactId!=null){
                    countsToUpdate.add(trigger.OldMap.get(c.Id).ContactId);
                }
            }
        }
        
        //if there are inspection orders to make
        if(ordersToMake != null && ordersToMake.size() > 0){
            InspectionOrdersHelper.init(ordersToMake);
        }
    }
    
     if(trigger.isUnDelete){
        Set<Case> workOrdersToReassign = new Set<Case>();
        
        
        //if(OrderAssignmentHelper.propsAlreadyUpdated == false){
            
        //}
        
        for(Case c:trigger.new){    
            if(c.Serviceable__c=='Yes'){
                //--------------------------------------------------------------------------------------------------------------
                //remove the contactid and set the Automatic_Reassignment__c flag so the before update trigger knows to handle it. 
                //--------------------------------------------------------------------------------------------------------------
                
                workOrdersToReassign.Add(new Case(id=c.Id,ContactId = null,Assignment_Log__c=null, Automatic_Reassignment__c = true));
            }
                    
        }
        if(workOrdersToReassign.Size()>0){
                List<Case> workOrderList = new List<Case>();
            workOrderList.AddAll(workOrdersToReassign);
            update workOrderList;
        }
        //---------------------------------------------------------------------------------
        //Don't update counts in the undelete! Wait for the subsequent update.
        //---------------------------------------------------------------------------------
        needCountsUpdate = false;
    }

    if(needCountsUpdate){

        
        if(countsToUpdate.Size()>0){
            OrderAssignmentHelper.updateOrderCounts(countsToUpdate);
        }
    }
    needCountsUpdate = false;
    //Only run the finalize method on updates, deletes and undeletes since we don't have anything to update until after the assignment system runs.  
    //Immediate subsequent update will run this code for new orders. 
    if(trigger.isUpdate || trigger.isDelete || trigger.isUnDelete){
        OrderAssignmentHelper.finalize();
    }
    //Code added - Padmesh Soni (09/09/2014) - OAM-75 Submitted Today/Yesterday Not Updating
    //Moved after finalize to ensure that contacts are updated before this method runs.  
    //- Don Kotter (10/8/2014)
    //Call the method to udpate Submitted Count Today field on Contact
    if(trigger.isUpdate && ContactSubmittedOrderCountsHelper.BYPASS_SUBMIT_COUNT_UPDATES){
            ContactSubmittedOrderCountsHelper.populateContactSubmittedCount(Trigger.new, Trigger.oldMap);
    }


}