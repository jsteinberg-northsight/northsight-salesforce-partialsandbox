trigger UpdateOderStatusOnQuarantine on Geocode_Cache__c (after update) {
	Set<id> quarantineChangedGeocodeCaches = new Set<Id>();
	Set<String> approvalStatusesToCheck = new Set<String>{'Not Started','Quarantined','Quarantined-Flagged'};
	for(Geocode_Cache__c gc:trigger.new){
		if(gc.quarantined__c != trigger.oldMap.get(gc.Id).quarantined__c){
			quarantineChangedGeocodeCaches.add(gc.Id);
		}
		if(gc.flag__c != trigger.oldMap.get(gc.Id).flag__c){
			quarantineChangedGeocodeCaches.add(gc.Id);
		}
	}
	
	//Padmesh Soni (06/17/2014) - Support Case Record Type
    //Query record of Record Type of Case object
	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
										AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
	
	//Padmesh Soni (06/17/2014) - Support Case Record Type
    //New filter added of record type
    //Padmesh Soni (06/23/2014) - Support Case Record Type
    //Status filter changed from 'Cancelled' to 'Canceled' just only spelling mistake
    List<Case> workOrders= [select id,Geocode_Cache__c,Approval_Status__c,Override_Quarantine__C,quarantine__c, quarantine_flagged__c from Case 
								where RecordTypeId NOT IN: recordTypes AND Geocode_Cache__c in :quarantineChangedGeocodeCaches and Approval_Status__c in :approvalStatusesToCheck 
								and status != 'Closed' and status !='Canceled' and isClosed = False and Canceled__c = false 
								and createdDate > 2012-10-01T00:00:00Z];
	
	List<Case> workOrdersForUpdate  = new List<Case>();
	for(Case c:workorders){
		if(c.quarantine__c==1 && c.quarantine_flagged__c == 0)
			{
				c.Approval_Status__c = 'Quarantined';
				workOrdersForUpdate.add(c);
			}
		if(c.quarantine_flagged__c==1)
			{
				c.Approval_Status__c = 'Quarantined-Flagged';
				workOrdersForUpdate.add(c);
			}
		if(c.quarantine__c==0 && c.quarantine_flagged__c ==0)
			{
				if(c.Approval_Status__c == 'Quarantined' || c.Approval_Status__c == 'Quarantined-Flagged'){
					c.Approval_Status__c = 'Not Started';
					workOrdersForUpdate.add(c);
				}
			}
	}
	OrderAssignmentHelper.skipPropertyUpdate = true; 
	if(workOrdersForUpdate.Size()>0){
		upsert workOrdersForUpdate;
	}
	OrderAssignmentHelper.skipPropertyUpdate = false;
	
	
}