trigger RequestTrigger on Request__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	//--------------------------------------------------------------------------------
    //INITIALIZATION 
    //--------------------------------------------------------------------------------
    RequestHelper.initialize();
    //--------------------------------------------------------------------------------
    //BEFORE-EVENT PROCESSING
    //--------------------------------------------------------------------------------
    if(trigger.isBefore){
        if(trigger.isInsert){
            //-------------------------
            //BEFORE INSERT
            //-------------------------
        	if (RequestHelper.userIsCommunityUser(UserInfo.getProfileId())) {
        		RequestHelper.setClientName(trigger.new, UserInfo.getUserId());
        	}

            RequestTriggerController.setPropertyId(trigger.new);
        }
        if(trigger.isUpdate){
        //-------------------------
        //BEFORE UPDATE
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isDelete){
        //-------------------------
        //BEFORE DELETE
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isUnDelete){
        //-------------------------
        //BEFORE UNDELETE
        //-------------------------
        //Nothing yet...
        }
    }
    //-----------------------------------------------------------------------------------
    //AFTER-EVENT PROCESSING
    //-----------------------------------------------------------------------------------
    if(trigger.isAfter){
        if(trigger.isInsert){
        //-------------------------
        //AFTER INSERT
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isUpdate){
        //-------------------------
        //AFTER UPDATE
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isDelete){
        //-------------------------
        //AFTER DELETE
        //-------------------------
        //Nothing yet...
        }
        if(trigger.isUndelete){
        //-------------------------
        //AFTER UNDELETE
        //-------------------------
        //Nothing yet...
        }
    }
    //---------------------------------------------------------------------------
    //FINALIZATION
    //---------------------------------------------------------------------------
    //Nothing yet...
}