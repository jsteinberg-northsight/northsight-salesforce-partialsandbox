trigger PaymentDisputeTrigger on Payment_Dispute__c (before insert) {
	
	if (Trigger.isInsert && Trigger.isBefore) {
		/* Before Insert */
		PaymentDisputeTriggerHandler.onBeforeInsert(Trigger.new, Trigger.newMap);
	}
}