trigger RouteTrigger on Route__c (after insert) {
	if (Trigger.isInsert && Trigger.isAfter) {
		/* After Insert */
		RouteTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
	}
}