/**
 *  Purpose         :   This is trigger on Attachement to performing data updation on it or its related Parent records.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   09/26/2014
 *
 *  Current Version :   V1.0
 *
 *  Revision Log    :   V1.0 - Created - SM-215: Evaluate work involved to upload EchoSign documents to Content
 **/
trigger Trigger_Attachement on Attachment (after insert) {
	
	//Check for event type
	if(Trigger.isAfter) {
		
		//Check for request type
		if(Trigger.isInsert) {
			
			//Call helper class method
			////Commented out untested code for deployment.  
			////Uncomment the next line to activate SM-215
			AttachementTriggerHelper.stampContentOnContact(trigger.new);
			
		}
	}
}