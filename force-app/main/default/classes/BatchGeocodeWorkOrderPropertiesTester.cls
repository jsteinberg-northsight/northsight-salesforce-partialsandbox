@isTest(seeAllData=false)
private class BatchGeocodeWorkOrderPropertiesTester {
//    private static ClientZone__c newClientZone;
//    private static ClientZipCode__c txClientZip;
//    private static ClientZipCode__c ctClientZip;
//    
//    
//    //Padmesh Soni (04/29/2014) - Required Maintenance: Unit Test Improvements 
//    //Test record created for Group
//    private static Group qurantineGrp;
//    
//    //Padmesh Soni (04/29/2014) - Required Maintenance: Unit Test Improvements 
//    //New generic method created
//    private static void createGroup() {
//        
//        qurantineGrp = new Group(Name = 'Quarantine_Managers', DeveloperName = 'QuarantineManagers');
//        insert qurantineGrp;
//    }
//    
//    private static void generateClientZoneData(){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new client zone
//        newClientZone = new ClientZone__c(
//            Client_Name__c = 'SG',
//            Client_Department__c = 'REO',
//            Order_Type__c = 'Routine',
//            Name = 'SG-RGC:TX53'
//        );
//        insert newClientZone;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new client zip code
//        txClientZip = new ClientZipCode__c(
//            ClientZone__c = newClientZone.Id,
//            Zip_Code_Value__c = '78704'
//        );
//        insert txClientZip;
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new client zip code
//        ctClientZip = new ClientZipCode__c(
//            ClientZone__c = newClientZone.Id,
//            Zip_Code_Value__c = '60562'
//        );
//        insert ctClientZip;
//    }
//    
//    private static Case buildTestCaseWithPartialAddress(){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account;
//        //create a new case
//        case partialCase = new Case(
//            Street_Address__c = '515 Partial St',
//            State__c = 'CT',
//            City__c = 'Bridgeport',
//            Zip_Code__c = '60562',
//            Vendor_Code__c = 'TRYWZYM',
//            Client__c = 'SG',
//            Work_Order_Type__c = 'Routine',
//            Work_Ordered__c = 'Grass Recut',
//            Loan_Type__c = 'REO',
//            Client_Name__c = account.Id
//        );
//        //insert partialCase;
//        return partialCase;
//    }
//    
//    private static List<Case> buildTestCasesWithSameAddress(){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account;
//        //create a new case
//        Case sameAddressCase1 = new Case(
//            Street_Address__c = '1234 Street',
//            State__c = 'TX',
//            City__c = 'Austin',
//            Zip_Code__c = '78704',
//            Vendor_Code__c = 'SRSRSR',
//            Client__c = 'SG',
//            Work_Order_Type__c = 'Routine',
//            Work_Ordered__c = 'Initial Grass Cut',
//            Loan_Type__c = 'REO',
//            Client_Name__c = account.Id
//        );
//        //insert sameAddressCase1;
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new case
//        Case sameAddressCase2 = new Case(
//            Street_Address__c = '1234 Street',
//            State__c = 'TX',
//            City__c = 'Austin',
//            Zip_Code__c = '78704',
//            Vendor_Code__c = 'RSRSRS',
//            Client__c = 'SG',
//            Work_Order_Type__c = 'Routine',
//            Work_Ordered__c = 'Grass Recut',
//            Loan_Type__c = 'REO',
//            Client_Name__c = account.Id
//        );
//        return new Case[]{sameAddressCase1,sameAddressCase2};
//        //insert sameAddressCase2;
//    }
//    
//    private static Case buildTestCaseWithExistingProperty(Geocode_Cache__c newGeocodeCache){
//
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account;
//        //create a new case
//        Case existingPropertyCase = new Case(
//            Street_Address__c = '400 Cedar St',
//            State__c = 'CT',
//            City__c = 'Bridgeport',
//            Zip_Code__c = '60562',
//            Vendor_Code__c = 'TRYWZYM',
//            Client__c = 'SG',
//            Work_Order_Type__c = 'Routine',
//            Work_Ordered__c = 'Grass Recut',
//            Loan_Type__c = 'REO',
//            Client_Name__c = account.Id
//        );
//        //insert existingPropertyCase;
//        return existingPropertyCase;
//    }
//    
//    private static Case buildTestCaseWithExistingPropertyNoGeolocation(Geocode_Cache__c newGeocodeCache){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new geocode_cache        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account;
//        //create a new case
//        case existingPropertyCase = new Case(
//            Street_Address__c = '400 Cedar St',
//            State__c = 'CT',
//            City__c = 'Bridgeport',
//            Zip_Code__c = '60562',
//            Vendor_Code__c = 'TRYWZYM',
//            Client__c = 'SG',
//            Work_Order_Type__c = 'Routine',
//            Work_Ordered__c = 'Grass Recut',
//            Loan_Type__c = 'REO',
//            Client_Name__c = account.Id
//        );
//        //insert existingPropertyCase;
//        return existingPropertyCase;
//    }
//
//    //--------------------------------------------------------
//    //TESTS
//    //Unit tests for method testing and regression testing. 
//    //--------------------------------------------------------
//        
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - testing that the BatchGeocodeWorkOrderProperties class is functioning correctly
//    static testMethod void testBatchClass() {
//        
//        //Padmesh Soni (06/18/2014) - Support Case Record Type
//        //Query record of Record Type of Case object
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
//                                            AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
//        
//        //Assert statement
//        System.assertEquals(1, recordTypes.size());
//        
//        createGroup();
//        generateClientZoneData();
//                //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new geocode_cache
//        Geocode_Cache__c newGeocodeCache = new Geocode_Cache__c(
//            Address_Hash__c = '400cedarstbrgprtct60562',
//            Formatted_Address__c = '400 CEDAR ST, BRIDGEPORT, CT 60562',
//            Formatted_Street_Address__c = '400 CEDAR ST',
//            Formatted_City__c = 'BRIDGEPORT',
//            Formatted_State__c = 'CT',
//            Formatted_Zip_Code__c = '60562',
//            Street_Address__c = '400 Cedar St',
//            City__c = 'Bridgeport',
//            State__c = 'CT',
//            Zip_Code__c = '60562',
//            Location__Latitude__s = 0,//location__latitude__s = 11.11111111,
//            Location__Longitude__s = 0//location__longitude__s = -111.22222222
//        );
//        insert newGeocodeCache;
//        Case existingPropertyCase = buildTestCaseWithExistingProperty(newGeocodeCache);
//        Case[] sameAddressCases = buildTestCasesWithSameAddress();
//        Case partialCase = buildTestCaseWithPartialAddress();
//        
//        //OrderAssignmentHelper.init() is only called once from trigger, so must insert all cases in one go
//        Case[] insertAllCases = new Case[]{};
//        insertAllCases.add(existingPropertyCase);
//        insertAllCases.add(sameAddressCases[0]);
//        insertAllCases.add(sameAddressCases[1]);
//        insertAllCases.add(partialCase);
//        insertAllCases.add(new Case(RecordTypeId = recordTypes[0].Id, Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN, 
//                                        Scheduled_Date_Override__c = Date.today(), Street_Address__c = '1234 Street', State__c = 'TX', 
//                                       City__c = 'Austin', Zip_Code__c = '78704'));
//        insert insertAllCases;
//        
//        Case c = [select geocode_cache__c,geocode_cache__r.Address_Hash__c from case where id =: existingPropertyCase.id];
//        Geocode_Cache__c g = [select id,Address_Hash__c from geocode_cache__c where id =: newGeocodeCache.id];
//        system.assertEquals(c.geocode_cache__r.Address_Hash__c, g.Address_Hash__c);
//        system.assertEquals(c.geocode_cache__c, g.id);
//        
//        system.Test.startTest(); 
//            //create and run a new instance of the BatchGeocodeWorkOrderProperties class
//            BatchGeocodeWorkOrderProperties testbatch = new BatchGeocodeWorkOrderProperties();
//            database.executeBatch(testbatch, 200);
//        system.Test.stopTest();
//        
//        //assert the existing geocode_cache has the correct address hash by selecting it thru the case table (this also ensures that geocode_cache has been linked to a case)
//        system.assertEquals('400cedarstbrgprtct60562', [select Address_Hash__c from Geocode_Cache__c where Id in (select Geocode_Cache__c from Case where Id = :existingPropertyCase.Id)].Address_Hash__c);
//        
//        //Code modified - Padmesh Soni (12/15/2014) - CCP-15: Fill County on Property
//        //New field added into query County__c
//        // Analyze results
//        Geocode_Cache__c[] geoResults = [SELECT Address_Hash__c, Quarantined__c, Partial_Match__c, Location__Latitude__s, Location__Longitude__s, 
//                                            Geo_System__c, Geo_Timestamp__c, Url_Used__c, County__c FROM Geocode_Cache__c];
//        
//        system.debug('*****'+geoResults+'*****');
//        
//        // Should have 3 geocode cache records 
//        system.assertEquals(3, geoResults.size());
//        
//        for(Geocode_Cache__c geo : geoResults) {
//            if(geo.Address_Hash__c == '515partialstbrgprtct60562') {
//                system.assertEquals(true, geo.Quarantined__c);
//                system.assertEquals(true, geo.Partial_Match__c);
//                system.assertEquals('BatchGeocodeWorkOrderProperties.cls', geo.Geo_System__c);
//                system.assert(Datetime.now() >= geo.Geo_Timestamp__c);
//                system.assert(Datetime.now().addMinutes(-1) < geo.Geo_Timestamp__c);
//                system.assertEquals('http://northsight-maprelay-env.elasticbeanstalk.com/?address=515+Partial+St%2C+Bridgeport%2C+Ct+60562&sensor=false', geo.Url_Used__c);
//                
//                //Code added - Padmesh Soni (12/15/2014) - CCP-15: Fill County on Property
//                //New assert statement added
//                system.assertEquals('Santa Clara', geo.County__c);
//            } else if(geo.Address_Hash__c == '400cedarstbrgprtct60562') {
//                system.assertEquals(false, geo.Quarantined__c);
//                system.assert(geo.Location__Latitude__s == 37.4229181);
//                system.assert(geo.Location__Longitude__s == -122.0854212);
//                system.assertEquals('BatchGeocodeWorkOrderProperties.cls', geo.Geo_System__c);
//                system.assert(Datetime.now() >= geo.Geo_Timestamp__c);
//                system.assert(Datetime.now().addMinutes(-1) < geo.Geo_Timestamp__c);
//                system.assertEquals('http://northsight-maprelay-env.elasticbeanstalk.com/?address=400+Cedar+St%2C+Bridgeport%2C+Ct+60562&sensor=false', geo.Url_Used__c);
//            } else {
//                system.assertEquals(false, geo.Quarantined__c);
//                system.assert(geo.Location__Latitude__s == 37.4229181);
//                system.assert(geo.Location__Longitude__s == -122.0854212);
//                system.assertEquals('BatchGeocodeWorkOrderProperties.cls', geo.Geo_System__c);
//                system.assert(Datetime.now() >= geo.Geo_Timestamp__c);
//                system.assert(Datetime.now().addMinutes(-1) < geo.Geo_Timestamp__c);
//                system.assertEquals('http://northsight-maprelay-env.elasticbeanstalk.com/?address=1234+Street%2C+Austin%2C+Tx+78704&sensor=false', geo.Url_Used__c);
//            }
//        }
//    }
//    
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - testing the BatchGeocodeWorkOrderProperties class error handling
//    static testMethod void testErrorTolerance(){
//        generateClientZoneData();
//        Case partialCase = buildTestCaseWithPartialAddress();
//        
//        insert partialCase;
//        //set the case's override_quarantine to true
//        Case newCase = [SELECT Id, Override_Quarantine__c FROM Case WHERE Id =: partialCase.Id];
//        system.assertEquals(false, newCase.Override_Quarantine__c);
//        newCase.Override_Quarantine__c = true;
//        update newCase;
//        
//        //set the property to be quarantined
//        Geocode_Cache__c newGC = [SELECT Id, quarantined__c from Geocode_Cache__c where address_hash__c = '515partialstbrgprtct60562'];
//        system.assertEquals(false, newGC.quarantined__c);
//        newGC.quarantined__c = true;
//        update newGC;
//        
//        system.Test.startTest();
//            //create and run a new instance of the BatchGeocodeWorkOrderProperties class
//            BatchGeocodeWorkOrderProperties testbatch = new BatchGeocodeWorkOrderProperties();
//            database.executeBatch(testbatch, 200);
//        system.Test.stopTest();
//        
//        List<System_Alerts__c> saList = new List<System_Alerts__c>();
//        saList = [SELECT Id, Subject_Line__c, Process_Name__c, Body__c FROM System_Alerts__c];
//        system.assertEquals(1, saList.size());//assert that a system alert was generated
//        for(System_Alerts__c sa : saList){//assert that the alert involves the geocoder
//            system.assertEquals('Geocoder Alert',sa.subject_line__c);
//            system.assertEquals('Geocoder',sa.Process_Name__c);
//            system.debug('System Alert Body' + '\r\n' + sa.Body__c);
//        }
//    }
//    
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - testing the select query logic and geocoding logic to ensure properties that have any combination of the following are selected/geocoded:
//    //Latitude is 0 or null AND longitude is 0 or null
//    //OR
//    //last geocoded date is null
//    //property last geocoded date is null and geolocation values are null
//    static testmethod void testPropertiesLogic_NoGeocodedDate_NoGeolocation(){
//        generateClientZoneData();
//                Geocode_Cache__c newGeocodeCache = new Geocode_Cache__c(
//            Address_Hash__c = '400cedarstbrgprtct60562',
//            Formatted_Address__c = '400 CEDAR ST, BRIDGEPORT, CT 60562',
//            Formatted_Street_Address__c = '400 CEDAR ST',
//            Formatted_City__c = 'BRIDGEPORT',
//            Formatted_State__c = 'CT',
//            Formatted_Zip_Code__c = '60562',
//            Street_Address__c = '400 Cedar St',
//            City__c = 'Bridgeport',
//            State__c = 'CT',
//            Zip_Code__c = '60562'
//        );
//        insert newGeocodeCache;
//        insert buildTestCaseWithExistingPropertyNoGeolocation(newGeocodeCache);
//        //select the property to assert the fields are null
//        Geocode_Cache__c p = [SELECT Last_Geocoded__c, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c WHERE Id =: newGeocodeCache.Id];
//            system.assertEquals(null, p.Last_Geocoded__c);
//            system.assertEquals(null, p.Location__Latitude__s);
//            system.assertEquals(null, p.Location__Longitude__s);
//        system.Test.startTest();
//            //create and run a new instance of the BatchGeocodeWorkOrderProperties class
//            BatchGeocodeWorkOrderProperties testbatch = new BatchGeocodeWorkOrderProperties();
//            database.executeBatch(testbatch, 5);
//        system.Test.stopTest();
//        //select back the property to assert the fields are no longer null, and the geolocation values are > 0
//        p = [SELECT Last_Geocoded__c, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c WHERE Id =: newGeocodeCache.Id];
//            system.assertNotEquals(null, p.Last_Geocoded__c);
//            system.assertNotEquals(null, p.Location__Latitude__s);
//            system.assertNotEquals(null, p.Location__Longitude__s);
//            system.assertNotEquals(0, p.Location__Latitude__s);
//            system.assertNotEquals(0, p.Location__Longitude__s);
//    }
//    //property last geocoded date is null and geolocation values are valid
//    static testmethod void testPropertiesLogic_NoGeocodedDate_ValidGeolocation(){
//        generateClientZoneData();
//                        Geocode_Cache__c newGeocodeCache = new Geocode_Cache__c(
//            Address_Hash__c = '400cedarstbrgprtct60562',
//            Formatted_Address__c = '400 CEDAR ST, BRIDGEPORT, CT 60562',
//            Formatted_Street_Address__c = '400 CEDAR ST',
//            Formatted_City__c = 'BRIDGEPORT',
//            Formatted_State__c = 'CT',
//            Formatted_Zip_Code__c = '60562',
//            Street_Address__c = '400 Cedar St',
//            City__c = 'Bridgeport',
//            State__c = 'CT',
//            Zip_Code__c = '60562'
//        );
//        insert newGeocodeCache;
//        insert buildTestCaseWithExistingPropertyNoGeolocation(newGeocodeCache);
//        //select the property to assert the fields are null
//        Geocode_Cache__c p = [SELECT Last_Geocoded__c, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c WHERE Id =: newGeocodeCache.Id];
//            system.assertEquals(null, p.Last_Geocoded__c);
//            system.assertEquals(null, p.Location__Latitude__s);
//            system.assertEquals(null, p.Location__Longitude__s);
//        p.Location__Latitude__s = 1.111111;
//        p.Location__Longitude__s = -2.222222;
//        update p;
//        p = [SELECT Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c WHERE Id =: newGeocodeCache.Id];
//            system.assertEquals(1.111111, p.Location__Latitude__s);
//            system.assertEquals(-2.222222, p.Location__Longitude__s);
//        //test of property with null last geocoded date and valid geolocation
//        system.Test.startTest();
//            //create and run a new instance of the BatchGeocodeWorkOrderProperties class
//            BatchGeocodeWorkOrderProperties testbatch = new BatchGeocodeWorkOrderProperties();
//            database.executeBatch(testbatch, 5);
//        system.Test.stopTest();
//        p = [SELECT Last_Geocoded__c, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c WHERE Id =: newGeocodeCache.Id];
//            system.assertNotEquals(null, p.Last_Geocoded__c);
//    }
//    //property last geocoded date is non-null and geolocation values are null
//    static testmethod void testPropertiesLogic_ValidGeocodedDate_NullGeolocation(){
//        generateClientZoneData();
//                        Geocode_Cache__c newGeocodeCache = new Geocode_Cache__c(
//            Address_Hash__c = '400cedarstbrgprtct60562',
//            Formatted_Address__c = '400 CEDAR ST, BRIDGEPORT, CT 60562',
//            Formatted_Street_Address__c = '400 CEDAR ST',
//            Formatted_City__c = 'BRIDGEPORT',
//            Formatted_State__c = 'CT',
//            Formatted_Zip_Code__c = '60562',
//            Street_Address__c = '400 Cedar St',
//            City__c = 'Bridgeport',
//            State__c = 'CT',
//            Zip_Code__c = '60562'
//        );
//        insert newGeocodeCache;
//        insert buildTestCaseWithExistingPropertyNoGeolocation(newGeocodeCache);
//        //select the property to assert the fields are null
//        Geocode_Cache__c p = [SELECT Last_Geocoded__c, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c WHERE Id =: newGeocodeCache.Id];
//            system.assertEquals(null, p.Last_Geocoded__c);
//            system.assertEquals(null, p.Location__Latitude__s);
//            system.assertEquals(null, p.Location__Longitude__s);
//        //update the property so that the last geocoded date is today
//        p.Last_Geocoded__c = Date.today();
//        update p;
//        p = [SELECT Last_Geocoded__c FROM Geocode_Cache__c WHERE Id =: newGeocodeCache.Id];
//            system.assertEquals(Date.today(), p.Last_Geocoded__c);
//        //test of property with null geolocation and non-null last geocoded date
//        system.Test.startTest();
//            //create and run a new instance of the BatchGeocodeWorkOrderProperties class
//            BatchGeocodeWorkOrderProperties testbatch = new BatchGeocodeWorkOrderProperties();
//            database.executeBatch(testbatch, 5);
//        system.Test.stopTest();
//        p = [SELECT Last_Geocoded__c, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c WHERE Id =: newGeocodeCache.Id];
//            system.assertNotEquals(null, p.Location__Latitude__s);
//            system.assertNotEquals(null, p.Location__Longitude__s);
//            system.assertNotEquals(0, p.Location__Latitude__s);
//            system.assertNotEquals(0, p.Location__Longitude__s);
//    }
//    //property last geocoded date is null and geolocation values are 0
//    static testmethod void testPropertiesLogic_NoGeocodedDate_InvalidGeolocation(){
//        generateClientZoneData();
//                        Geocode_Cache__c newGeocodeCache = new Geocode_Cache__c(
//            Address_Hash__c = '400cedarstbrgprtct60562',
//            Formatted_Address__c = '400 CEDAR ST, BRIDGEPORT, CT 60562',
//            Formatted_Street_Address__c = '400 CEDAR ST',
//            Formatted_City__c = 'BRIDGEPORT',
//            Formatted_State__c = 'CT',
//            Formatted_Zip_Code__c = '60562',
//            Street_Address__c = '400 Cedar St',
//            City__c = 'Bridgeport',
//            State__c = 'CT',
//            Zip_Code__c = '60562'
//        );
//        insert newGeocodeCache;
//        insert buildTestCaseWithExistingPropertyNoGeolocation(newGeocodeCache);
//        //select the property to assert the fields are null
//        Geocode_Cache__c p = [SELECT Last_Geocoded__c, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c WHERE Id =: newGeocodeCache.Id];
//            system.assertEquals(null, p.Last_Geocoded__c);
//            system.assertEquals(null, p.Location__Latitude__s);
//            system.assertEquals(null, p.Location__Longitude__s);
//        //update the property so that the last geocoded field is null and the geolocation values are 0
//        p.Location__Latitude__s = 0;
//        p.Location__Longitude__s = 0;
//        update p;
//        p = [SELECT Last_Geocoded__c, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c WHERE Id =: newGeocodeCache.Id];
//            system.assertEquals(null, p.Last_Geocoded__c);
//            system.assertEquals(0, p.Location__Latitude__s);
//            system.assertEquals(0, p.Location__Longitude__s);
//        //test of property with null last geocoded date and geolocation values of 0
//        system.Test.startTest();
//            //create and run a new instance of the BatchGeocodeWorkOrderProperties class
//            BatchGeocodeWorkOrderProperties testbatch = new BatchGeocodeWorkOrderProperties();
//            database.executeBatch(testbatch, 5);
//        system.Test.stopTest();
//        p = [SELECT Last_Geocoded__c, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c WHERE Id =: newGeocodeCache.Id];
//            system.assertNotEquals(null, p.Last_Geocoded__c);
//            system.assertNotEquals(0, p.Location__Latitude__s);
//            system.assertNotEquals(0, p.Location__Longitude__s);
//            system.assertNotEquals(null, p.Location__Latitude__s);
//            system.assertNotEquals(null, p.Location__Longitude__s);
//    }
}