@isTest(SeeAllData=false)
public with sharing class ActivityHelperTester {
////TESTING AS PROCESSOR 
//	private static Account 				newAccount;
//	private static Contact 				newContact;
//	private static Profile 				newProfile;
//	private static Profile 				nonPortalProfile;
//	private static User    				newUser;
//	private static User    				nonPortalUser;
//	private static Geocode_Cache__c 	newGeocode;
//	private static RecordType   		rt;
//	private static Case					newCase;
//	private static Task					newTask;
//	private static Work_Order_Setup__c 	wos;
//	/*************************************************************************************************************************************************************************/
//	/*
//	this sections will create the necessary objects needed for the case object to be tested on
//	*/
//	private static void generateTestData(){
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new account
//		newAccount = new Account(
//			Name = 'Tester Account',
//			RecordTypeId = '01240000000Ubta'
//		);
//		insert newAccount;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new contact
//		//default record type: OLD-Customer: 01240000000UPSmAAO
//		//Processor Record Type ID: 01240000000UbtfAAC
//		newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest',
//			RecordTypeId = '01240000000UbtfAAC'
//		);
//		insert newContact;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new user with the Customer Portal Manager Standard profile
//		newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Processors'];
//		//create a new user with the profile
//		newUser = new User(
//			alias = 'standt',
//			contactId = newContact.Id,
//			email = 'standarduser@testorg.com',
//			emailencodingkey = 'UTF-8',
//			lastname = 'Testing',
//			languagelocalekey = 'en_US',
//			localesidkey = 'en_US',
//			profileId = newProfile.Id,
//			timezonesidkey = 'America/Los_Angeles', username = 'standarduser@northsight.test', isActive = true
//		);
//		insert newUser;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new property
//		newGeocode = new Geocode_Cache__c(
//			address_hash__c = '400cedarstbrdgprtct60562',
//			quarantined__c = false,
//			location__latitude__s = 11.11111111,
//			location__longitude__s = -111.22222222,
//			Street_Address__c = '400 Cedar st',
//			Zip_Code__c = '60562',
//			City__c = 'Bridgeport',
//			State__c = 'CT'
//		);
//		insert newGeocode;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create work orders with the REO record type
//		rt = [SELECT Id, Name FROM RecordType WHERE Name =: 'REO' LIMIT 1];
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account;
//		//create a work order with the record type
//		newCase = new Case(
//			Client__c = 'NOT-SG',
//			Due_Date__c = Date.today(),
//			Backyard_Serviced__c = 'Yes',
//			Excessive_Growth__c = 'No',
//			Debris_Removed__c = 'No',
//			Shrubs_Trimmed__c = 'No',
//			Vendor_Code__c = 'CHIGRS',
//			State__c = 'CT',
//			Status = 'Open',
//			City__c = 'Bridgeport',
//			Zip_Code__c = '60562',
//			Description = 'Work order for grass cutting',
//			OwnerId = newUser.Id,
//			ContactId = newContact.Id,
//			Street_Address__c = '400 Cedar st',
//			Geocode_Cache__c = newGeocode.Id,
//			Work_Ordered__c = 'Initial Grass Cut',
//			Work_Ordered_Text__c = 'Grass Cutting',
//			Approval_Status__c = 'Not Started',
//			RecordTypeId = rt.Id,
//			Client_Name__c = account.Id
//		);
//		insert newCase;
//		
//		wos = new Work_Order_Setup__c(
//			Work_Ordered_Text__c = 'Grass Cutting', 
//			Work_Ordered__c = 'Initial Grass Cut', 
//			Order_Type__c = 'Routine', 
//			Client__c = 'NOT-SG'
//		);
//	}
//	
//	//--------------------------------------------------------
//   	//PRE-TEST SETUP
//	//create a task object attached to the case
//	private static void generateTaskItem(String i){
//		nonPortalProfile = [SELECT Id FROM Profile WHERE Name = 'Super Vendor Manager'];
//		//create a new user with the profile
//		nonPortalUser = new User(
//			alias = 'nonPort'+i,
//			email = 'nonPortalUser@testorg.com',
//			emailencodingkey = 'UTF-8',
//			lastname = 'NonPortal',
//			languagelocalekey = 'en_US',
//			localesidkey = 'en_US',
//			profileId = nonPortalProfile.Id,
//			timezonesidkey = 'America/Los_Angeles', username = 'nonPortalUser' + i + '@test.com', isActive = true
//		);
//		insert nonPortalUser;
//		
//		newTask = new Task(
//			OwnerId = nonPortalUser.Id,
//			WhatId = newCase.Id,
//			Type = 'Other',
//			Subject = 'Subject: ' + i,
//			Description = 'Comments, Comments, Comments: ' + i,
//			Status = 'Not Started',
//			Priority = 'High'
//		);
//		insert newTask;
//		system.assert(newTask.Id != null);
//	}
//	/*************************************************************************************************************************************************************************/
//	//--------------------------------------------------------
//   	//TESTS
//   	//Unit tests for method testing and regression testing. 
//   	//--------------------------------------------------------
//   		
//   	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - create two tasks and assert that the last activity fields on a case object
//   	//correspond with the values from a task that was just created
//	public static testmethod void testActivityHelper(){
//		system.Test.startTest();
//		generateTestData();
//		generateTaskItem('1');
//		
//		//debug
//		for(Contact con : [SELECT RecordTypeId FROM Contact WHERE AccountId =: newAccount.Id]){
//			System.assertEquals('01240000000UbtfAAC', con.RecordTypeId);
//		}
//		//debug
//		
//		for(Case c : [SELECT Last_Activity_Datetime__c, Last_Activity_Comments__c, Last_Activity_Subject__c, Last_Activity_Type__c FROM Case WHERE Id =: newCase.Id]){
//			for(Task t : [SELECT CreatedDate, Description, Subject, Type FROM Task WHERE Subject = 'Subject: 1']){
//				system.assertEquals(c.Last_Activity_Datetime__c, t.CreatedDate);
//				system.assertEquals(c.Last_Activity_Comments__c, t.Description);
//				system.assertEquals(c.Last_Activity_Subject__c, t.Subject);
//				system.assertEquals(c.Last_Activity_Type__c, t.Type);
//			}
//		}
//		
//		generateTaskItem('2');
//		
//		for(Case c : [SELECT Last_Activity_Datetime__c, Last_Activity_Comments__c, Last_Activity_Subject__c, Last_Activity_Type__c FROM Case WHERE Id =: newCase.Id]){
//			for(Task t : [SELECT CreatedDate, Description, Subject, Type FROM Task WHERE Subject = 'Subject: 2']){
//				system.assertEquals(c.Last_Activity_Datetime__c, t.CreatedDate);
//				system.assertEquals(c.Last_Activity_Comments__c, t.Description);
//				system.assertEquals(c.Last_Activity_Subject__c, t.Subject);
//				system.assertEquals(c.Last_Activity_Type__c, t.Type);
//			}
//		}
//		
//		system.Test.stopTest();
//	}
//	
//	//Padmesh Soni (06/17/2014) - Support Case Record Type
//	//New method added to test the functionality after adding RecordTypeId filter into ActivityHelper class
//	static testmethod void testActivityHelperAfterInculdingRecordType(){
//		
//		//List to hold Account test records
//		List<Account> accounts = new List<Account>();
//		accounts.add(new Account(Name = 'Tester Account',RecordTypeId='01240000000Ubta'));
//		insert accounts;
//		
//		//List to hold Contact test records
//		List<Contact> contacts = new List<Contact>();
//		contacts.add(new Contact(LastName = 'Tester',AccountId = accounts[0].Id,Pruvan_Username__c = 'NSBobTest', RecordTypeId = '01240000000UbtfAAC'));
//		insert contacts;
//		
//		//create a new user with the Customer Portal Manager Standard profile
//		List<Profile> profiles = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Processors' OR Name = 'Super Vendor Manager' ORDER BY Name];
//		
//		//assert statement here
//		System.assertEquals(2, profiles.size());
//		
//		//create a new user with the profile
//		List<User> users = new List<User>();
//		users.add(new User(alias = 'standt',contactId = contacts[0].Id, email = 'standarduser@testorg.com', emailencodingkey = 'UTF-8', 
//								lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = profiles[0].Id,
//								timezonesidkey = 'America/Los_Angeles', username = 'standarduser@northsight.test', isActive = true));
//		users.add(new User(alias = 'nonPort', email = 'nonPortalUser@testorg.com', emailencodingkey = 'UTF-8', lastname = 'NonPortal',
//							languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = profiles[1].Id, 
//							timezonesidkey = 'America/Los_Angeles', username = 'nonPortalUser@test.com', isActive = true));
//		insert users;
//		
//		//create a new property
//		List<Geocode_Cache__c> geoCodes = new List<Geocode_Cache__c>();
//		geoCodes.add(newGeocode = new Geocode_Cache__c(address_hash__c = '400cedarstbrdgprtct60562', quarantined__c = false, location__latitude__s = 11.11111111,
//														location__longitude__s = -111.22222222, Street_Address__c = '400 Cedar st', Zip_Code__c = '60562',
//														City__c = 'Bridgeport', State__c = 'CT'));
//		insert geoCodes;
//		
//		//Query record of Record Type of Case object
//		List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT OR DeveloperName = 'REO') 
//											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true ORDER BY DeveloperName LIMIT 2];
//		
//		//Assert statement
//		System.assertEquals(2, recordTypes.size());
//		
//		//create a work order with the record type
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//		List<Case> workOrders = new List<Case>();
//		workOrders.add(new Case(Client__c = 'NOT-SG', Due_Date__c = Date.today(), Backyard_Serviced__c = 'Yes', Excessive_Growth__c = 'No',
//									Debris_Removed__c = 'No', Shrubs_Trimmed__c = 'No', Vendor_Code__c = 'CHIGRS', State__c = 'CT', Status = 'Open',
//									City__c = 'Bridgeport', Zip_Code__c = '60562', Description = 'Work order for grass cutting', OwnerId = users[0].Id,
//									ContactId = contacts[0].Id, Street_Address__c = '400 Cedar st', Geocode_Cache__c = geoCodes[0].Id, Work_Ordered__c = 'Initial Grass Cut',
//									Work_Ordered_Text__c = 'Grass Cutting', Approval_Status__c = 'Not Started', RecordTypeId = recordTypes[0].Id,Client_Name__c = account.Id));
//		workOrders.add(new Case(Client__c = 'NOT-SG', Due_Date__c = Date.today(), Backyard_Serviced__c = 'Yes', Excessive_Growth__c = 'No',
//									Debris_Removed__c = 'No', Shrubs_Trimmed__c = 'No', Vendor_Code__c = 'CHIGRS', State__c = 'CT', Status = 'Open',
//									City__c = 'Bridgeport', Zip_Code__c = '60562', Description = 'Work order for grass cutting', OwnerId = users[0].Id,
//									ContactId = contacts[0].Id, Street_Address__c = '400 Cedar st', Geocode_Cache__c = geoCodes[0].Id, Work_Ordered__c = 'Initial Grass Cut',
//									Work_Ordered_Text__c = 'Grass Cutting', Approval_Status__c = 'Not Started', RecordTypeId = recordTypes[1].Id,Client_Name__c = account.Id));
//		insert workOrders;
//		
//		
//		//List to hold tasks records
//		List<Task> tasks = new List<Task>();
//		
//		//Loop through Tasks reocrds
//		for(integer i=0; i < workOrders.size(); i++) {
//		
//			//populate tasks records
//			tasks.add(new Task(OwnerId = users[1].Id, WhatId = workOrders[i].Id, Type = 'Other', Subject = 'Subject: ' + i, 
//								Description = 'Comments, Comments, Comments: ' + i, Status = 'Not Started', Priority = 'High'));
//		}
//		
//		//Test starts here
//		Test.startTest();
//		
//		//insert tasks here
//		insert tasks;
//		
//		//variable to hold counter
//		integer counter = 0;
//		
//		//Loop through query result of Case
//		for(Case c : [SELECT Last_Activity_Datetime__c, Last_Activity_Comments__c, Last_Activity_Subject__c, Last_Activity_Type__c 
//							FROM Case WHERE Id IN: workOrders ORDER BY CaseNumber]) {
//			
//			//Check for first time looping
//			if(counter == 0) {
//				
//				//Loop through Task records
//				for(Task t : [SELECT CreatedDate, Description, Subject, Type FROM Task WHERE Subject = 'Subject: 0']) {
//					
//					system.assertEquals(c.Last_Activity_Datetime__c, t.CreatedDate);
//					system.assertEquals(c.Last_Activity_Comments__c, t.Description);
//					system.assertEquals(c.Last_Activity_Subject__c, t.Subject);
//					system.assertEquals(c.Last_Activity_Type__c, t.Type);
//				}
//			} else {
//				
//				system.assertEquals(c.Last_Activity_Datetime__c, null);
//				system.assertEquals(c.Last_Activity_Comments__c, null);
//				system.assertEquals(c.Last_Activity_Subject__c, null);
//				system.assertEquals(c.Last_Activity_Type__c, null);
//			}
//			counter++;
//		}
//				
//		//Test stops here
//		Test.stopTest();
//}
}