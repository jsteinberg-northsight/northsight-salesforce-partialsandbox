@isTest(seeAllData=false)
private class Test_WorkOrderResultSummaryController {
    
    private static Case currentWO;
    private static List<ImageLinks__c> imageLinks;
    private static List<ImageLinkRlt__c> imageLinkRlts;
    
    @testSetup
    private static void createTestData() {
        
        List<User> users = new List<User>();
        users.add(new User(alias = 'standt',email = 'standarduser@testorg.com', emailencodingkey = 'UTF-8', 
                                lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = Userinfo.getProfileId(),
                                timezonesidkey = 'America/Los_Angeles', username = 'standarduser@northsight.test', isActive = true));
        insert users;
        
        List<Account> accounts = new List<Account>();
        accounts.add(new Account(Name = 'Tester Account', Account_Supervisor__c = Userinfo.getUserId(), Status__c = 'Active'));
        insert accounts;
        
        List<Contact> contacts = new List<Contact>();
        contacts.add(new Contact(LastName = 'Tester',AccountId = accounts[0].Id,Pruvan_Username__c = 'NSBobTest', FirstName = 'Test'));
        insert contacts;
        
        List<Geocode_Cache__c> geoCodes = new List<Geocode_Cache__c>();
        geoCodes.add(new Geocode_Cache__c(address_hash__c = '400cedarstbrdgprtct60562', quarantined__c = false, location__latitude__s = 11.11111111,
                                                        location__longitude__s = -111.22222222, Street_Address__c = '400 Cedar st', Zip_Code__c = '60562',
                                                        City__c = 'Bridgeport', State__c = 'CT'));
        insert geoCodes;
        
        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT OR DeveloperName = 'REO') 
                                            AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true ORDER BY DeveloperName LIMIT 2];
        
        System.assertEquals(2, recordTypes.size());
        
        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
                                        
        Account account =  new Account(Client_Code__c = 'NOT-SG', Name = 'Tester Account 1',recordTypeId=accountRecordTypes[0].Id, Status__c = 'Active', Account_Supervisor__c = Userinfo.getUserId());
        insert account; 

        currentWO = new Case(Client__c = 'NOT-SG', Due_Date__c = Date.today(), Backyard_Serviced__c = 'Yes', Excessive_Growth__c = 'No',
                                    Debris_Removed__c = 'No', Shrubs_Trimmed__c = 'No', Vendor_Code__c = 'Follow', State__c = 'CT', Status = 'Open',
                                    City__c = 'Bridgeport', Zip_Code__c = '60562', Description = 'Work order for grass cutting', OwnerId = users[0].Id,
                                    ContactId = contacts[0].Id, Street_Address__c = '400 Cedar st', Geocode_Cache__c = geoCodes[0].Id, Work_Ordered__c = 'Initial Grass Cut',
                                    Work_Ordered_Text__c = 'Grass Cutting', Approval_Status__c = 'Not Started', RecordTypeId = recordTypes[0].Id,Client_Name__c = account.Id);
        
        insert currentWO;
        
        Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true, Formatted_State__c = 'TX');
        insert property;
        
        List<Invoice__c> invoices = new List<Invoice__c>();
        invoices.add(new Invoice__c(Property__c = property.Id, Work_Order__c = currentWO.Id));
        insert invoices;
        
        imageLinks = new List<ImageLinks__c>();
        imageLinks.add(new ImageLinks__c(CaseId__c = currentWO.Id, Name = 'Image Link 1', ImageUrl__c = 'www.google.com/s.jpg'));
        imageLinks.add(new ImageLinks__c(CaseId__c = currentWO.Id, Name = 'Image Link 2', ImageUrl__c = 'www.google.com/s.jpg'));
        imageLinks.add(new ImageLinks__c(CaseId__c = currentWO.Id, Name = 'Image Link 3', ImageUrl__c = 'www.google.com/s.jpg'));
        
        insert imageLinks;
        
        List<Product2> products = new List<Product2>();
        products.add(new Product2(Name = 'Test 1', Family = 'Cleaning', IsActive = true));
        
		insert products;
		
		List<Invoice_Line__c> invoiceLines = new List<Invoice_Line__c>();
		invoiceLines.add(new Invoice_Line__c(Invoice__c = invoices[0].Id, Product__c = products[0].Id, Quantity__c = 1, Unit_Price__c = 100, Property__c = property.Id));
		insert invoiceLines;
		
        List<ImageLinkRlt__c> imageRlts = new List<ImageLinkRlt__c>();
        imageRlts.add(new ImageLinkRlt__c(Invoice_Line__c = invoiceLines[0].Id, Image_Link__c = imageLinks[0].Id, Label__c = 'Before'));
        imageRlts.add(new ImageLinkRlt__c(Invoice_Line__c = invoiceLines[0].Id, Image_Link__c = imageLinks[0].Id, Label__c = 'After'));
        imageRlts.add(new ImageLinkRlt__c(Invoice_Line__c = invoiceLines[0].Id, Image_Link__c = imageLinks[0].Id, Label__c = 'During'));
        imageRlts.add(new ImageLinkRlt__c(Invoice_Line__c = invoiceLines[0].Id, Image_Link__c = imageLinks[1].Id, Label__c = 'Before'));
        imageRlts.add(new ImageLinkRlt__c(Invoice_Line__c = invoiceLines[0].Id, Image_Link__c = imageLinks[1].Id, Label__c = 'After'));
        imageRlts.add(new ImageLinkRlt__c(Invoice_Line__c = invoiceLines[0].Id, Image_Link__c = imageLinks[1].Id, Label__c = 'During'));
        insert imageRlts;
    }
    
    static testmethod void testMethod1(){
        
        currentWO = [SELECT Id FROM Case];
        
        Test.startTest();
        
        Apexpages.StandardController SC = new Apexpages.StandardController(currentWO);
        WorkOrderResultSummaryController controller = new WorkOrderResultSummaryController(SC);
        
        Test.stopTest();
    }
}