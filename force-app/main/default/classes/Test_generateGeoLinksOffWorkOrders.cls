@isTest
private class Test_generateGeoLinksOffWorkOrders {
//
///**
// *  Purpose         :   This is used for testing and covering generateGeoLinksOffWorkOrders class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/20/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Support Case Record Type
// *	
// *	Coverage		:	100%
// **/
// 	
// 	//New method to test the functionality of generateGeoLinksOffWorkOrders class
//    static testMethod void myUnitTest() {
//        
//        //Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client'
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(3, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accountsOnOrders = new List<Account>();
//    	accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accountsOnOrders;
//		
//		//create a new client zone
//        ClientZone__c newClientZone = new ClientZone__c(Client_Name__c = 'SG', Client_Department__c = 'REO', Order_Type__c = 'Routine',
//            												Name = 'SG-RGC:TX53');
//        insert newClientZone;
//        
//        //List to hold ClientZipCode object
//        List<ClientZipCode__c> clientZipCodes = new List<ClientZipCode__c>();
//        clientZipCodes.add(new ClientZipCode__c(Name = 'TX', ClientZone__c = newClientZone.Id, Zip_Code_Value__c = '78704'));
//        clientZipCodes.add(new ClientZipCode__c(Name = 'LA', ClientZone__c = newClientZone.Id, Zip_Code_Value__c = '60562'));
//        clientZipCodes.add(new ClientZipCode__c(Name = 'AZ', ClientZone__c = newClientZone.Id, Zip_Code_Value__c = '45621'));
//        insert clientZipCodes;
//        
//		//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX',
//    								Department__c = 'Test', Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual', Zip_Code__c = '78704', 
//    								Updated_Date_Time__c = DateTime.now()));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Zip_Code__c = '78704',  
//    								Updated_Date_Time__c = DateTime.now().addHours(2)));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Zip_Code__c = '78704',
//    								Department__c = 'Test', Assignment_Program__c = 'Mannual', Updated_Date_Time__c = DateTime.now(), Vendor_Code__c = 'FOLLOW'));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Zip_Code__c = '60562',
//    								Department__c = 'Test1', Work_Ordered__c = 'Grass Initial Cut', Assignment_Program__c = 'OrderSelect', 
//    								Updated_Date_Time__c = DateTime.now().addHours(5), Vendor_Code__c = 'FOLLOW'));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[2].Id, AccountId = accountsOnOrders[1].Id, Due_Date__c = Date.today().addDays(1), 
//    								Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Zip_Code__c = '45621',
//    								Department__c = 'Test1', Work_Ordered__c = 'Grass Initial Cut', Assignment_Program__c = 'OrderSelect', 
//    								Updated_Date_Time__c = DateTime.now().addHours(5), Vendor_Code__c = 'FOLLOW'));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//    	//Test starts here
//    	Test.startTest();
//    	
//    	//Query result of Case records
//    	List<Id> caseIds = new List<Id>();
//    	Map<Id, Case> caseMap= new Map<Id, Case>([SELECT Id FROM Case WHERE Id IN: workOrders]);
//    	caseIds.addAll(caseMap.keySet());
//    	
//    	System.debug('caseIds ::::'+ caseIds);
//    	
//    	//Call controller method
//    	generateGeoLinksOffWorkOrders.generateLinks(caseIds);
//    	
//    	//Query result of GeoZone Link
//    	List<GeoZoneLink__c> geoZoneLinks = [SELECT Id FROM GeoZoneLink__c];
//    	
//    	//Test stops here
//    	Test.stopTest();
//    }
}