global class ScheduleSnowOrderEmails implements Schedulable {
    /**CRON systax: second minute hour(24) dayofmonth month dayofweek year
    daily at 7am
    */
    public static String CRON_EXP = '0 0 19 * * ? *';

    //main method
    global static String startScheduledProcess() {
        ScheduleSnowOrderEmails s = new ScheduleSnowOrderEmails();
        return System.schedule('Daily Snow Email', CRON_EXP, s);
    }

    global void execute(SchedulableContext sc) {

        List<Contact> contactList = new List<Contact>();

        //Get a distinct list of contact IDs for open snow removal work orders
        for(AggregateResult cid: (List<AggregateResult>) [
            SELECT ContactId
            FROM Case
            WHERE ContactId <> ''
                AND Work_Ordered__c = 'Snow Removal'
                AND Approval_Status__c = 'Not Started'
                AND Canceled__c = false AND Status != 'Closed'
            GROUP BY ContactId
        ]) {
            contactList.add(new Contact(
                Id = (String)cid.get('ContactId')
            ));
        }

        //Create email list using the Open_Snow_Orders template
        if (!contactList.isEmpty()) {
            EmailTemplate template = [SELECT id FROM EmailTemplate WHERE DeveloperName = 'Open_Snow_Orders'];
            List<Messaging.SingleEmailMessage> mailsToSend = new List<Messaging.SingleEmailMessage>();
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@northsight.com'];
            
            for (Contact c : contactList)
            {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(c.Id);
                mail.setWhatId(c.Id);
                mail.setTemplateId(template.Id);
                mail.saveAsActivity = false;
                if (owea.size() > 0) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                mailsToSend.add(mail);
            }
            //Send Email
            if(mailsToSend.size() > 0)
            {
                Messaging.sendEmail(mailsToSend);       
            }
        }
    }
}