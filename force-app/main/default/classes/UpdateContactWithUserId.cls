public without sharing class UpdateContactWithUserId {

	static public void setUserId(List<User> newUsers, Map<Id, User> userMap) {
		List<Id> contactIds = new List<Id>();
		List<Id> userIds = new List<Id>();

		for(User currentUser : newUsers) {
			if (currentUser.ContactId != null) {
				contactIds.add(currentUser.ContactId);
				userIds.add(currentUser.Id);
			}
		}
		if (!contactIds.IsEmpty()) {
			UpdateContactWithUserId.updateContacts(contactIds, userIds);
		}
	}
	
	@future
	static public void updateContacts(List<Id> contactIds, List<Id> userIds) {
		List<Contact> contactList = new List<Contact>();
		for(Integer i = 0; i < contactIds.size(); i++) {
			contactList.add(new Contact(Id = contactIds[i], Portal_User_Id__c = userIds[i]));
		}
		update contactList;
	}
}