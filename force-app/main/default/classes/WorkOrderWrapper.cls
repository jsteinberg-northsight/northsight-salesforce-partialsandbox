public without sharing class WorkOrderWrapper implements Comparable{
		public case wo {get; private set;}
		public boolean selected {get{
		return ApexPages.currentPage().getParameters().keySet().contains(wo.Id);
		return false;
		}
		}
		public static string sortField='Id';
		public static string sortDirection='Up';
		public decimal latitude {get;set;}
		public decimal longitude {get;set;}
		public string addressTypes {get;set;}
		public string recurringNotes {get;set;}
		public string addressHash {get;set;}
		//----------------------------------------------
		public string referenceNumber{
			get{return wo == null ? null : wo.Reference_Number__c;}
			set;
		}
		public string workOrdered{
			get{return wo == null ? null : wo.Work_Ordered__c;}
			set;
		}
		public string woNumber{
			get{return wo == null ? null : wo.casenumber;}
			set;
		}
		public string displayAddress{
			get{return wo == null ? null : wo.Display_Address__c;}
			set;
		}
		public Id woId{
			get{return wo == null ? null : wo.Id;}
			set;
		}
		//---------------------------------------------
		public string getRowColor(){
			if(wo.Scheduled_Date__c<Date.Today()){
				return 'red';
			}else if(wo.Scheduled_Date__c==Date.Today()){
				return 'yellow';
			}else if(wo.Scheduled_Date__c>Date.Today() && wo.Routing_Serviceable__c=='Yes'){
				return 'green';
			}else{
				return 'white';
			}
		}
		//-----------------------------------
		//
		//-----------------------------------
		private integer directionMultiplier{
			get{
				return WorkOrderWrapper.sortDirection.toLowerCase()=='down'?-1:1;
			}
		}
		private integer CompareString(WorkOrderWrapper otherWorkOrder){
				if( (string)((sObject)this.wo).get(sortField) > (string)((sObject)otherWorkOrder.wo).get(sortField) ){
					return 1*directionMultiplier;
				}else if( (string)((sObject)this.wo).get(sortField) < (string)((sObject)otherWorkOrder.wo).get(sortField) ){
					return -1*directionMultiplier;
				}else{
					return 0;
				}
		}
		private integer CompareDate(WorkOrderWrapper otherWorkOrder){
				if( (Date)((sObject)this.wo).get(sortField) > (Date)((sObject)otherWorkOrder.wo).get(sortField) ){
					return 1*directionMultiplier;
				}else if( (Date)((sObject)this.wo).get(sortField) < (Date)((sObject)otherWorkOrder.wo).get(sortField) ){
					return -1*directionMultiplier;
				}else{
					return 0;
				}
		}
		/*
		public integer CompareDateTime(WorkOrderWrapper otherWorkOrder){
				if( (DateTime)((sObject)this.wo).get(sortField) > (DateTime)((sObject)otherWorkOrder.wo).get(sortField) ){
					return 1*directionMultiplier;
				}else if( (DateTime)((sObject)this.wo).get(sortField) < (DateTime)((sObject)otherWorkOrder.wo).get(sortField) ){
					return -1*directionMultiplier;
				}else{
					return 0;
				}
		}
		*/
		private integer CompareDecimal(WorkOrderWrapper otherWorkOrder){
				if( (Decimal)((sObject)this.wo).get(sortField) > (Decimal)((sObject)otherWorkOrder.wo).get(sortField) ){
					return 1*directionMultiplier;
				}else if( (Decimal)((sObject)this.wo).get(sortField) < (Decimal)((sObject)otherWorkOrder.wo).get(sortField) ){
					return -1*directionMultiplier;
				}else{
					return 0;
				}
		}
	    /*
		private integer CompareDouble(WorkOrderWrapper otherWorkOrder){
				if( (Double)((sObject)this.wo).get(sortField) > (Double)((sObject)otherWorkOrder.wo).get(sortField) ){
					return 1*directionMultiplier;
				}else if( (Double)((sObject)this.wo).get(sortField) < (Double)((sObject)otherWorkOrder.wo).get(sortField) ){
					return -1*directionMultiplier;
				}else{
					return 0;
				}
		}
		*/
		/*
		public integer CompareId(WorkOrderWrapper otherWorkOrder){
				if( (Id)((sObject)this.wo).get(sortField) > (Id)((sObject)otherWorkOrder.wo).get(sortField) ){
					return 1*directionMultiplier;
				}else if( (Id)((sObject)this.wo).get(sortField) < (Id)((sObject)otherWorkOrder.wo).get(sortField) ){
					return -1*directionMultiplier;
				}else{
					return 0;
				}
		}
		*/
		private integer CompareInteger(WorkOrderWrapper otherWorkOrder){
				if( (Integer)((sObject)this.wo).get(sortField) > (Integer)((sObject)otherWorkOrder.wo).get(sortField) ){
					return 1*directionMultiplier;
				}else if( (Integer)((sObject)this.wo).get(sortField) < (Integer)((sObject)otherWorkOrder.wo).get(sortField) ){
					return -1*directionMultiplier;
				}else{
					return 0;
				}
		}
		/*
		public integer CompareLong(WorkOrderWrapper otherWorkOrder){
				if( (Long)((sObject)this.wo).get(sortField) > (Long)((sObject)otherWorkOrder.wo).get(sortField) ){
					return 1*directionMultiplier;
				}else if( (Long)((sObject)this.wo).get(sortField) < (Long)((sObject)otherWorkOrder.wo).get(sortField) ){
					return -1*directionMultiplier;
				}else{
					return 0;
				}
		}
		*/
		/*
		private integer CompareTime(WorkOrderWrapper otherWorkOrder){
				if( (Time)((sObject)this.wo).get(sortField) > (Time)((sObject)otherWorkOrder.wo).get(sortField) ){
					return 1*directionMultiplier;
				}else if( (Time)((sObject)this.wo).get(sortField) < (Time)((sObject)otherWorkOrder.wo).get(sortField) ){
					return -1*directionMultiplier;
				}else{
					return 0;
				}
		}
		*/
		private integer CompareDefault(WorkOrderWrapper otherWorkOrder){
				if( string.valueOf(((sObject)this.wo).get(sortField)) > String.valueof(((sObject)otherWorkOrder.wo).get(sortField)) ){
					return 1*directionMultiplier;
				}else if( string.valueOf(((sObject)this.wo).get(sortField)) < string.valueOf(((sObject)otherWorkOrder.wo).get(sortField)) ){
					return -1*directionMultiplier;
				}else{
					return 0;
				}
		}
		
		
		public integer CompareTo(Object compareTo){
			try{
				if(compareTo == null){
					return 1;
				}
				
				WorkOrderWrapper otherWorkOrder = (WorkOrderWrapper)compareTo;
				object compareValue = ((sObject)this.wo).get(sortField);
				
				if(compareValue == null){
					compareValue = ((sObject)((WorkOrderWrapper)compareTo).wo).get(sortField);
					
					if(compareValue == null){
						return 0;
					}
					else{
						return -1;
					}
				}
				
				if(compareValue instanceof String) return this.CompareString(otherWorkOrder);
				if(compareValue instanceof Date) return this.CompareDate(otherWorkOrder);
				//if(compareValue instanceof DateTime) return this.CompareDateTime(otherWorkOrder);
				if(compareValue instanceof Decimal) return this.CompareDecimal(otherWorkOrder);
				//if(compareValue instanceof Double) return this.CompareDouble(otherWorkOrder);
				if(compareValue instanceof Integer) return this.CompareInteger(otherWorkOrder);
				//if(compareValue instanceof Long) return this.CompareLong(otherWorkOrder);
				//if(compareValue instanceof Time) return this.CompareTime(otherWorkOrder);
				return this.CompareDefault(otherWorkOrder);
			}catch(Exception e){
				ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()+' : '+e.getStackTraceString());
				ApexPages.addMessage(errorMsg);
				return 0;
			}
		}
		
		
		public WorkOrderWrapper(case c){
			wo = c;
			//selected = false;
		}
		
		
		//TEST METHOD
		public static testmethod void testSort(){
			list<WorkOrderWrapper> cases = new list<WorkOrderWrapper>();
			for(integer x=0;x<20;x++){
				
				Case c = new Case(Customer_Invoice_Bags__c=1.5+x/10,Client_Number__c=25+x,Client__c='SG',Work_Ordered_Text__c = 'HEDGE TRIM'+(x>10?string.valueof(x):''),Vendor_Code__c='AYSGRS',Street_Address__c=string.valueof(33999+(x>10?x:0))+' Stanley St.', State__c = 'LA', Zip_Code__c = string.valueof(69999+(x>10?x:0)),Due_Date__c = Date.today() + (x>10?x:0) - 15);
				cases.add(new WorkOrderWrapper(c));
			}
			Map<string,Schema.sObjectType>gd =  Schema.getGlobalDescribe();
			string[] tmp_list = new string[] {};
			schema.describesobjectresult tbldesc = gd.get('Case').getDescribe();
			set<string> fieldSet = tbldesc.fields.getMap().keySet();
			for(string field:fieldSet){
			WorkOrderWrapper.sortDirection = 'Down';
			System.assert(cases[0].directionMultiplier == -1 );
			WorkOrderWrapper.sortField = field;	
			cases.sort();
			System.assert(cases[0].CompareTo(cases[1])<=0);
			WorkOrderWrapper.sortDirection = 'up';
			System.assert(cases[0].CompareTo(cases[1])>=0);
			cases.sort();
			System.assert(cases[0].CompareTo(cases[1])<=0);
			}
		} 
		
}