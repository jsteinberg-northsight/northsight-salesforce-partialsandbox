public with sharing class WorkOrderResultSummaryController {
    
    public String currentWOId;
    public List<ImageLinkWrap> imageLinks {get;set;}
    public Map<String, ImageLinkWrap> mapImageLinkRWithILId;
    public Map<String, Map<String, List<ImageLinkWrap>>> mapImageLinks {get; set;}
    public List<String> labels {get;set;}
    public Boolean isOverThreshold {get; set;}
        
    public WorkOrderResultSummaryController(Apexpages.StandardController SC) {
        
        currentWOId = SC.getId();
        
        imageLinks = new List<ImageLinkWrap>();
        mapImageLinkRWithILId = new Map<String, ImageLinkWrap>();
        mapImageLinks = new Map<String, Map<String, List<ImageLinkWrap>>>();
        labels = new List<String>();
        isOverThreshold = false;
        inIt();
    }
    
    public void inIt() {
        
        if(String.isNotBlank(currentWOId)) {
            
            for(ImageLinks__c imageLink : [SELECT Id, Disable_Photos__c, ImageUrl__c, CaseId__c, CaseId__r.Date_Serviced1__c, CaseId__r.CaseNumber, CaseId__r.Work_Ordered__c, 
                                                Image_URL_Medium__c, Image_URL_Small__c, GPS_Location__Latitude__s, GPS_Location__Longitude__s
                                                FROM ImageLinks__c
                                                WHERE CaseId__c =: currentWOId AND Disable_Photos__c = false]) {
                
                ImageLinkWrap imageLWrap = new ImageLinkWrap(imageLink, null);
                
                mapImageLinkRWithILId.put(imageLink.Id, imageLWrap);
            }
            
            if(mapImageLinkRWithILId.keyset().size() > 0) {
                
                Set<String> leftImageLinks = new Set<String>();
                leftImageLinks = mapImageLinkRWithILId.keyset();
                
                for(ImageLinkRlt__c imageLR : [SELECT Id, Image_Link__c, Label__c, Name, Image_Link__r.Disable_Photos__c, Image_Link__r.ImageUrl__c, Image_Link__r.CaseId__c, 
                                                    Image_Link__r.CaseId__r.Date_Serviced1__c, Image_Link__r.CaseId__r.CaseNumber, Image_Link__r.CaseId__r.Work_Ordered__c,
                                                    Invoice_Line__r.Product__r.Name, Image_Link__r.Image_URL_Medium__c, Image_Link__r.Image_URL_Small__c, Invoice_Line__r.Product__c,
                                                    Image_Link__r.GPS_Location__Latitude__s, Image_Link__r.GPS_Location__Longitude__s
                                                    FROM ImageLinkRlt__c WHERE Image_Link__c IN : mapImageLinkRWithILId.keyset() AND Label__c != null AND Invoice_Line__c != null AND Image_Link__r.Disable_Photos__c = false
                                                    ORDER BY Image_Link__r.GPS_Timestamp__c, Image_Link__r.CreatedDate, Image_Link__r.Original_fileName__c LIMIT 3000]) {
                    
                    ImageLinkWrap imageLWrap1 = new ImageLinkWrap(null, imageLR);
                    
                    System.debug('labels :::'+ imageLR.Label__c);
                                
                    if(imageLR.Invoice_Line__r.Product__c != null && String.isNotBlank(imageLR.Invoice_Line__r.Product__r.Name)) {
                        
                        if(!mapImageLinks.containsKey(imageLR.Invoice_Line__r.Product__r.Name)) {
                    		
                    		mapImageLinks.put(imageLR.Invoice_Line__r.Product__r.Name, new Map<String, List<ImageLinkWrap>>());
                    	}
                        if(mapImageLinks.containsKey(imageLR.Invoice_Line__r.Product__r.Name)) {
                            
                            if(mapImageLinks.get(imageLR.Invoice_Line__r.Product__r.Name).containsKey(imageLR.Label__c)) {
                            
                                mapImageLinks.get(imageLR.Invoice_Line__r.Product__r.Name).get(imageLR.Label__c).add(imageLWrap1);
                            } else {
                                
                                labels.add(imageLR.Label__c);
                                mapImageLinks.get(imageLR.Invoice_Line__r.Product__r.Name).put(imageLR.Label__c, new List<ImageLinkWrap>{imageLWrap1});
                            } 
                        }
                    } else {
                        
                        if(!mapImageLinks.containsKey(Label.NON_PRODUCT)) {
                    		
                    		mapImageLinks.put(Label.NON_PRODUCT, new Map<String, List<ImageLinkWrap>>());
                    	}
                        
                        if(mapImageLinks.containsKey(Label.NON_PRODUCT)) {
                            
                            if(mapImageLinks.get(Label.NON_PRODUCT).containsKey(imageLR.Label__c)) {
                            
                                mapImageLinks.get(Label.NON_PRODUCT).get(imageLR.Label__c).add(imageLWrap1);
                            } else {
                                
                                labels.add(imageLR.Label__c);
                                System.debug('labels :::'+ imageLR.Label__c);
                                mapImageLinks.get(Label.NON_PRODUCT).put(imageLR.Label__c, new List<ImageLinkWrap>{imageLWrap1});
                            }
                        }
                    }
                    /**if(mapImageLinks.containsKey(imageLR.Label__c)) {
                        
                        mapImageLinks.get(imageLR.Label__c).add(mapImageLinkRWithILId.get(imageLR.Image_Link__c));
                    } else {
                        
                        labels.add(imageLR.Label__c);
                        mapImageLinks.put(imageLR.Label__c, new List<ImageLinkWrap>{mapImageLinkRWithILId.get(imageLR.Image_Link__c)});
                    }**/
                    
                    if(leftImageLinks.contains(imageLR.Image_Link__c)) {
                        
                        leftImageLinks.remove(imageLR.Image_Link__c);
                    }
                    
                    System.debug('labels :::'+ labels);
                    
                }
                
                if(leftImageLinks.size() > 0) {
                    
                    for(String iLId : leftImageLinks) {
                        
                        if(!mapImageLinks.containsKey(Label.NON_PRODUCT)) {
                    		
                    		mapImageLinks.put(Label.NON_PRODUCT, new Map<String, List<ImageLinkWrap>>());
                    	}
                    	
                        if(mapImageLinks.containsKey(Label.NON_PRODUCT)) {
                        
                            if(mapImageLinks.get(Label.NON_PRODUCT).containsKey(Label.UNLABELLED_IMAGES)) {
                            
                                mapImageLinks.get(Label.NON_PRODUCT).get(Label.UNLABELLED_IMAGES).add(mapImageLinkRWithILId.get(iLId));
                            } else {
                                
                                labels.add(Label.UNLABELLED_IMAGES);
                                mapImageLinks.get(Label.NON_PRODUCT).put(Label.UNLABELLED_IMAGES, new List<ImageLinkWrap>{mapImageLinkRWithILId.get(iLId)});
                            }
                        } 
                        /**if(mapImageLinks.containsKey(Label.UNLABELLED_IMAGES)) {
                        
                            mapImageLinks.get(Label.UNLABELLED_IMAGES).add(mapImageLinkRWithILId.get(iLId));
                        } else {
                            
                            labels.add(Label.UNLABELLED_IMAGES);
                            mapImageLinks.put(Label.UNLABELLED_IMAGES, new List<ImageLinkWrap>{mapImageLinkRWithILId.get(iLId)});
                        }**/
                    }
                }
                
                Integer totalImages = 0;
                for(String labelValue : mapImageLinks.keyset()) {
                    
                    totalImages += mapImageLinks.get(labelValue).size();
                }
                
                if(totalImages > 1200) {
                    
                    isOverThreshold = true;
                }
                    system.debug('isOverThreshold :::'+ isOverThreshold);
                
                if(labels.size() > 0) {
                    
                    List<String> tempLabels = new List<String>();
                    
                    Set<String> setTempLabels = new Set<String>();
                    
                    Set<Integer> indexes = new Set<Integer>();
                    System.debug('labels :::'+ labels);
                        
                    if(String.isNotBlank(Label.LABEL_ORDER)) {
                        
                        for(String labelName : Label.LABEL_ORDER.split(',')) {
                            
                            for(String originalLabel : labels) {
                                
                                if(originalLabel.equalsIgnoreCase(labelName)) {
                                    
                                    if(!setTempLabels.contains(originalLabel)) {
                                    	tempLabels.add(originalLabel);
                                    	setTempLabels.add(originalLabel);
                                    }
                                    indexes.add(labels.indexOf(originalLabel));
                                }
                            }
                        }
                        
                        System.debug('indexes :::'+ indexes + ' labels ::: '+ labels);
                        
                        List<String> middleList = new List<String>();
                        
                        for(integer i = 0 ; i < labels.size(); i++) {
                        	
                        	if(!indexes.contains(i)) {
                        		
                        		middleList.add(labels[i]);
                        	}
                        }
                                                
                        if(labels.size() > 0) {
                            
                            for(String needToAdd : middleList) {
                           		
                           		boolean isExist = false;
                           		
                           		for(String alreadyValue : tempLabels) {
                           			
                           			if(needToAdd == alreadyValue) {
                           				
                           				isExist = true;
                           			}
                           		}
                           		
                           		if(!isExist) {
                           			
                           			tempLabels.add(needToAdd);
                           		}
                           	}
                            labels = tempLabels;
                        }
                        System.debug('tempLabels :::'+ labels);
                        
                    }
                }
            }
            
        }
    }
    
    public class ImageLinkWrap {
        
        public ImageLinks__c imageLink {get; set;}
        public ImageLinkRlt__c imageLinkRelations {get; set;}
        
        public ImageLinkWrap(ImageLinks__c imageLink, ImageLinkRlt__c imageLinkRelations) {
            
            this.imageLink = imageLink;
            this.imageLinkRelations = imageLinkRelations;
        }
    }
}