@isTest
private class updateCaseObjectOnStatusInsertTest
{
//    static testMethod void myUnitTest() 
//    {
//    	List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//    	
//        Case NewCase = new Case();
//        NewCase.Zip_Code__c = '90311';
//        NewCase.Vendor_Code__c = 'SRSRSR';
//        NewCase.Street_Address__c = 'new street';
//        NewCase.Client_Name__c = account.Id;
//        insert NewCase; 
//        
//        Status__c NewStatus = new Status__c(case__c = NewCase.Id, Expected_Upload_Date__c = Date.today(), Delay_Reason__c='Nothing', Order_Status__c = 'Done');
//        insert NewStatus;
//    }
//    
//    //Padmesh Soni (06/17/2014) - Support Case Record Type
//    //New test method added for testing the functionality after 
//    //adding "SUPPORT" RecordType filter into Case query 
//	static testMethod void testUpdateCaseObjectOnStatusInsert() {
//    	
//    	List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//    	
//    	
//    	//Query record of Record Type of Case object
//		List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
//											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
//		
//		//Assert statement
//		System.assertEquals(1, recordTypes.size());
//		
//		//List to hold Case records for testing
//		List<Case> workOrders = new List<Case>();
//		workOrders.add(new Case(Zip_Code__c = '90311', Vendor_Code__c = 'SRSRSR', Street_Address__c = 'new street', RecordTypeId = recordTypes[0].Id,Client_Name__c = account.Id));
//		workOrders.add(new Case(Zip_Code__c = '90313', Vendor_Code__c = 'SRSRSP', Street_Address__c = '123, Test',Client_Name__c = account.Id));
//		workOrders.add(new Case(Zip_Code__c = '90315', Vendor_Code__c = 'SRSRSQ', Street_Address__c = '123',Client_Name__c = account.Id));
//		
//		//insert Case here
//		insert workOrders;
//		
//		workOrders[2].Status_Count_by_Vendor__c =1;
//		update workOrders[2];
//		
//		//List to hold Status records
//		List<Status__c> statuses = new List<Status__c>();
//		statuses.add(new Status__c(case__c = workOrders[0].Id, Expected_Upload_Date__c = Date.today(), Delay_Reason__c='Nothing', 
//									Order_Status__c = 'Done'));
//		statuses.add(new Status__c(case__c = workOrders[1].Id, Expected_Upload_Date__c = Date.today(), Delay_Reason__c='Nothing', 
//									Order_Status__c = 'Done', Explanation__c = 'Testing is going on.'));
//		statuses.add(new Status__c(case__c = workOrders[2].Id, Expected_Upload_Date__c = Date.today(), Delay_Reason__c='Nothing', 
//									Order_Status__c = 'Done', Explanation__c = 'Testing is going on.'));
//		
//		//Test starts here
//		Test.startTest();
//		
//        //insert Status here
//		insert statuses;
//		
//		workOrders = [SELECT  ownerId, last_status_update__c, last_expected_upload_date__c, last_delay_reason__c, last_status_explanation__c,
//                		status_needed__c, last_status_by_current_owner__c, Status_Count_by_Vendor__c, Last_Order_Status__c FROM Case WHERE Id IN: workOrders ORDER BY CaseNumber];
//        
//        statuses = [SELECT Id, Expected_Upload_date__c, Delay_Reason__c, Explanation__c, Order_Status__c 
//        				FROM Status__c WHERE Id IN:statuses ORDER BY NAME];
//        
//        //assert statements here
//        System.assertEquals(workOrders[0].Last_Status_Update__c, null);
//		System.assertEquals(workOrders[1].Last_Status_Update__c.date(), Datetime.now().date());
//		System.assertEquals(workOrders[1].Last_Expected_Upload_Date__c, statuses[1].Expected_Upload_date__c);
//		System.assertEquals(workOrders[1].Last_Delay_Reason__c, statuses[1].Delay_Reason__c);
//		System.assertEquals(workOrders[1].Last_Status_Explanation__c, statuses[1].Explanation__c);
//		System.assertEquals(workOrders[1].Status_Needed__c, false);
//		System.assertEquals(workOrders[1].Last_Order_Status__c, statuses[1].Order_Status__c);
//		System.assertEquals(workOrders[1].Status_Count_by_Vendor__c, 1);
//		System.assertEquals(workOrders[2].Last_Status_Update__c.date(), Datetime.now().date());
//		System.assertEquals(workOrders[2].Last_Expected_Upload_Date__c, statuses[2].Expected_Upload_date__c);
//		System.assertEquals(workOrders[2].Last_Delay_Reason__c, statuses[2].Delay_Reason__c);
//		System.assertEquals(workOrders[2].Last_Status_Explanation__c, statuses[2].Explanation__c);
//		System.assertEquals(workOrders[2].Status_Needed__c, false);
//		System.assertEquals(workOrders[2].Last_Order_Status__c, statuses[2].Order_Status__c);
//		System.assertEquals(workOrders[2].Status_Count_by_Vendor__c, 2);
//		
//		//Test stops here
//		Test.stopTest();
//    }
}