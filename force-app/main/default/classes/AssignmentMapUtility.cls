/**
 *	Description		:	This is utility class for containing some specific bunch of code related to Assignment Map functionality.
 *
 *	Created By		:	Padmesh Soni
 *
 *	Created Date	:	09/17/2014
 *
 *	Current Version	:	V1.1
 *
 *	Revisiion Logs	:	V1.0 - Created - Padmesh Soni (09/17/2014) - OAM-78: Add  the ability to be able to plot all vendors on the map
 *											not just the ones with assigned work orders.
 *						V1.1 - Modified - Padmesh Soni (10/03/2014) - OAM-78: Add  the ability to be able to plot all vendors on the map not just
 *											 the ones with assigned work orders.
 **/
public with sharing class AssignmentMapUtility {

	public transient String vendorsOnMapJSON {get; set;}
	private double minLat = 0.0;
	private double minLon = 0.0;
	private double maxLat = 0.0;
	private double maxLon = 0.0;
	private Set<String> excludeVendors;
	private Set<String> excludeConIds;
	private Set<String> contactStatus;
	private Set<String> contactActive;

	public AssignmentMapUtility() {

		vendorsOnMapJSON = '';
		excludeVendors = new Set<String>();
		excludeConIds = new Set<String>();
		contactStatus = new Set<String>();
		contactActive = new Set<String>();
	}

	/**
	 *	@descpriton		:	This method is used to get all vendors who are having Status 1 (1-New) as JSON string.
 	 *
	 *	@param			:	Set<Id> excludeVendors
	 *
	 *	@return			:	String
	 **/
	public void getVendorsToPlot() {

		//Instance of PageReference
		PageReference p = ApexPages.currentPage();

		//Map to hold all parameters from Page URL
		Map<string, string> params = p.getParameters();

		//Getting minimum latitude from URL parameter minLat
		minLat = double.valueOf(params.get(AssignmentMapConstants.URL_PARAM_MINLAT));

		//Getting minimum latitude from URL parameter minLon
		minLon = double.valueOf(params.get(AssignmentMapConstants.URL_PARAM_MINLON));

		//Getting minimum latitude from URL parameter maxLat
		maxLat = double.valueOf(params.get(AssignmentMapConstants.URL_PARAM_MAXLAT));

		//Getting minimum latitude from URL parameter maxLon
		maxLon = double.valueOf(params.get(AssignmentMapConstants.URL_PARAM_MAXLON));

		//Getting minimum latitude from URL parameter maxLon
		String vendorStatusSelected = params.get('vendorStatusSelected');

		//Check for not blank
		if(String.isNotBlank(vendorStatusSelected)) {

			//Spliting by comma
			for(String vendStatus : vendorStatusSelected.split(',')) {
				contactStatus.add(vendStatus);
			}
		}

		//Getting active vendors
		String activeVendorsSelected = params.get('activeVendors');

		//Check for not blank
		if(String.isNotBlank(activeVendorsSelected)) {

			//Spliting by comma
			for(String vendActive : activeVendorsSelected.split(',')) {

				contactActive.add(vendActive);
			}
		}

		//Getting minimum latitude from URL parameter maxLon
		String vendorsToExclude = params.get('excludeVendors');

		//System.debug('vendorsToExclude ::::'+ vendorsToExclude);

		//Check for not blank
		if(String.isNotBlank(vendorsToExclude)) {

			//Spliting by comma
			for(String vendorId : vendorsToExclude.split(',')) {

				excludeVendors.add(vendorId);
			}
		}

		//Getting minimum latitude from URL parameter maxLon
		String contactsToExclude = params.get('excludeContacts');

		//Check for not blank
		if(String.isNotBlank(contactsToExclude)) {

			//Spliting by comma
			for(String conId : contactsToExclude.split(',')) {

				excludeConIds.add(conId);
			}
		}

		//List to hold Wrapper list
		List<VendorToPlotWrapper> vendorWrapList = new List<VendorToPlotWrapper>();
		User[] users;
		if(contactActive.size()>0){
		users = [SELECT Id, Alias,Name, ContactId, Contact.Name, Contact.FirstName, Contact.LastName,
				Contact.Other_Address_Geocode__Latitude__s, Contact.Other_Address_Geocode__Longitude__s,
				Contact.OtherStreet, Contact.OtherState, Contact.OtherCountry, Contact.OtherCity, Contact.OtherPostalCode,
				Contact.Email, Contact.MobilePhone,Contact.Last_Submitted_Date_Time__c, Contact.Phone
				FROM User WHERE Id NOT IN: excludeVendors AND IsPortalEnabled = true AND IsActive = true
				AND Profile.UserLicense.Name IN: AssignmentMapConstants.USER_LICENSE_OF_VENDORS_ON_MAP
				AND Contact.RecordType.DeveloperName = 'Vendor' AND Contact.Status__c != null
				AND Contact.Status__c IN: contactStatus AND
				Contact.Other_Address_Geocode__Latitude__s != null AND Contact.Other_Address_Geocode__Latitude__s != 0.0
				AND Contact.Other_Address_Geocode__Latitude__s != null AND Contact.Other_Address_Geocode__Latitude__s != 0.0
				AND
				  Contact.Services_Provided__c IN: contactActive
				 AND
				 Contact.Other_Address_Geocode__Latitude__s >=: minLat
				 AND Contact.Other_Address_Geocode__Longitude__s >=: minLon
				 AND Contact.Other_Address_Geocode__Latitude__s <=: maxLat
				 AND Contact.Other_Address_Geocode__Longitude__s <=: maxLon
				 ];
		}else{
			users = [SELECT Id, Alias,Name, ContactId, Contact.Name, Contact.FirstName, Contact.LastName,
				Contact.Other_Address_Geocode__Latitude__s, Contact.Other_Address_Geocode__Longitude__s,
				Contact.OtherStreet, Contact.OtherState, Contact.OtherCountry, Contact.OtherCity, Contact.OtherPostalCode,
				Contact.Email, Contact.MobilePhone,Contact.Last_Submitted_Date_Time__c, Contact.Phone
				FROM User WHERE Id NOT IN: excludeVendors AND IsPortalEnabled = true AND IsActive = true
				AND Profile.UserLicense.Name IN: AssignmentMapConstants.USER_LICENSE_OF_VENDORS_ON_MAP
				AND Contact.RecordType.DeveloperName = 'Vendor' AND Contact.Status__c != null
				AND Contact.Status__c IN: contactStatus AND
				Contact.Other_Address_Geocode__Latitude__s != null AND Contact.Other_Address_Geocode__Latitude__s != 0.0
				AND Contact.Other_Address_Geocode__Latitude__s != null AND Contact.Other_Address_Geocode__Latitude__s != 0.0
				AND
				 Contact.Other_Address_Geocode__Latitude__s >=: minLat
				 AND Contact.Other_Address_Geocode__Longitude__s >=: minLon
				 AND Contact.Other_Address_Geocode__Latitude__s <=: maxLat
				 AND Contact.Other_Address_Geocode__Longitude__s <=: maxLon];
		}
		//Loop through query result of Users
		
		for(User usr : users)
			 {

			//populate set
			excludeConIds.add(usr.ContactId);

			//String to hold First Name and Last Name
			String fName = usr.Contact.FirstName;
			String lName = usr.Contact.LastName;

			//initiails letter of Contact
			String initials = (fName != null? fName.left(1): fName) + (lName != null? lName.left(1): lName);

			//Geolocation values of Contact
			String geoCodeVlaues = String.valueOf(usr.Contact.Other_Address_Geocode__Latitude__s)
									+ ',' + String.valueOf(usr.Contact.Other_Address_Geocode__Longitude__s);

			//Other Street Address values of Contact
			String otherStreetVlaues = String.valueOf(usr.Contact.OtherStreet);

			//Other City, State, PostalCode Address values of Contact
			String otherAddressVlaues = String.valueOf(usr.Contact.OtherCity) + ',' + String.valueOf(usr.Contact.OtherState) + ','
										+ String.valueOf(usr.Contact.OtherPostalCode);

			//Email
			String otherEmail = String.valueOf(usr.Contact.Email);

			//Mobile
			String mobilePhone = String.valueOf(usr.Contact.MobilePhone);

			//Regular phone
			String regularPhone = String.valueOf(usr.Contact.Phone);

			//Last date
			String LastSubmittedDate = String.valueOf(usr.Contact.Last_Submitted_Date_Time__c);

			//add instance of wrapper into list
			vendorWrapList.add(new VendorToPlotWrapper(usr.Id, usr.ContactId, usr.Contact.Name, usr.Alias, initials, geoCodeVlaues, otherStreetVlaues,
													otherAddressVlaues,otherEmail,mobilePhone,LastSubmittedDate,regularPhone));
		}

		//Code added - Padmesh Soni (10/03/2014) - OAM-78: Add  the ability to be able to plot all vendors
		//on the map not just the ones with assigned work orders.
		//Logic added for Contacts also to be ploted on map
		//Loop through query result of Contacts
		for(Contact vendorCon : [SELECT Id, Name, FirstName, LastName, Other_Address_Geocode__Latitude__s, Other_Address_Geocode__Longitude__s,
									OtherStreet, OtherState, OtherCountry, OtherCity, OtherPostalCode, Email, MobilePhone, Last_Submitted_Date_Time__c, Phone FROM Contact
									WHERE Id NOT IN: excludeConIds AND RecordType.DeveloperName = 'Vendor'
									AND Status__c != null AND Status__c IN: contactStatus AND Other_Address_Geocode__Latitude__s != null
									AND Other_Address_Geocode__Latitude__s != 0.0 AND Other_Address_Geocode__Latitude__s != null
									AND Other_Address_Geocode__Latitude__s != 0.0 AND Other_Address_Geocode__Latitude__s >=: minLat
									AND Other_Address_Geocode__Longitude__s >=: minLon  AND Other_Address_Geocode__Latitude__s <=: maxLat
									AND Other_Address_Geocode__Longitude__s <=: maxLon]) {

			//String to hold First Name and Last Name
			String fName = vendorCon.FirstName;
			String lName = vendorCon.LastName;

			//initiails letter of Contact
			String initials = (fName != null? fName.left(1): fName) + (lName != null? lName.left(1): lName);

			//Geolocation values of Contact
			String geoCodeVlaues = String.valueOf(vendorCon.Other_Address_Geocode__Latitude__s)
									+ ',' + String.valueOf(vendorCon.Other_Address_Geocode__Longitude__s);

			//Other Street Address values of Contact
			String otherStreetVlaues = String.valueOf(vendorCon.OtherStreet);

			//Other City, State, PostalCode Address values of Contact
			String otherAddressVlaues = String.valueOf(vendorCon.OtherCity) + ',' + String.valueOf(vendorCon.OtherState) + ','
										+ String.valueOf(vendorCon.OtherPostalCode);

			String conAlias = (fName != null? fName.toLowerCase().left(1): fName) + lName.toLowerCase().left(4);
			//Email
			String Email = String.valueOf(vendorCon.Email);

			//Mobile
			String MobilePhone = String.valueOf(vendorCon.MobilePhone);
			//Last date
			String LastSubmittedDate = String.valueOf(vendorCon.Last_Submitted_Date_Time__c);

			//Regular Phone
			String RegularPhone = String.valueOf(vendorCon.Phone);

			//add instance of wrapper into list
			vendorWrapList.add(new VendorToPlotWrapper(vendorCon.Id, vendorCon.Id, vendorCon.Name, conAlias, initials, geoCodeVlaues, otherStreetVlaues,
													otherAddressVlaues,Email,MobilePhone,LastSubmittedDate,RegularPhone));
		}

		//Check for size of list
		if(vendorWrapList.size() > 0)
			vendorsOnMapJSON = JSON.serialize(vendorWrapList);
	}

	//Wrapper to hold all vendors
	public class VendorToPlotWrapper{

		//class properties
		public String vendorId {get; set;}
		public String vendorContactId {get; set;}
		public String vendorAlias {get; set;}
		public String vendorName {get; set;}
		public String vendorInitials {get; set;}
		public String vendorGeocode {get; set;}
		public String vendorOtherStreet {get; set;}
		public String vendorOtherAddress {get; set;}
		public String vendorEmail {get; set;}
		public String vendorMobilePhone {get; set;}
		public String vendorLastSubmittedDate {get; set;}
		public String vendorRegularPhone {get; set;}

		//Constructor defintion
		public VendorToPlotWrapper(String vendorId, String vendorContactId, String vendorName, String vendorAlias, String vendorInitials,
								String vendorGeocode, String vendorOtherStreet, String vendorOtherAddress,String vendorEmail, String vendorMobilePhone,
								String vendorLastSubmittedDate, String vendorRegularPhone) {

			this.vendorId = vendorId;
			this.vendorContactId = vendorContactId;
			this.vendorAlias = vendorAlias;
			this.vendorName = vendorName;
			this.vendorInitials = vendorInitials;
			this.vendorGeocode = vendorGeocode;
			this.vendorOtherStreet = vendorOtherStreet;
			this.vendorOtherAddress = vendorOtherAddress;
			this.vendorEmail = vendorEmail;
			this.vendorMobilePhone = vendorMobilePhone;
			this.vendorLastSubmittedDate = vendorLastSubmittedDate;
			this.vendorRegularPhone = vendorRegularPhone;
		}
	}
	
	public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
	
}