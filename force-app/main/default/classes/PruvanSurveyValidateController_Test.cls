@isTest()
/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/
*/
public with sharing class PruvanSurveyValidateController_Test {
//	public static testmethod void testSurveyValidateController(){
//		/***********************************************************************************************************************************/
//		/*
//		this sections handles the creation of objects that will need to be used by some of the functions for testing purposes
//		*/
//		//create a new account
//		Account newAccount = new Account(
//			Name = 'Tester Account',
//			RecordTypeId = '01240000000Ubta'
//		);
//		insert newAccount;
//		//assert that the account exists
//		Account a = [SELECT Id FROM Account WHERE Id =: newAccount.Id];
//		system.assert(a != null);
//	
//		//create a new contact
//		Contact newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest',
//			RecordTypeId = '01240000000UbtfAAC'
//		);
//		insert newContact;
//		//assert that the contact exists
//		Contact c = [SELECT Id FROM Contact WHERE Id =: newContact.Id];
//		system.assert(c != null);
//		
//		//Create a new user with the Customer Portal Manager Standard profile
//		Profile newProfile = [SELECT Id FROM profile WHERE Name = 'Partner Community - Processors'];
//		system.assert(newProfile != null);
//		User newUser = new User(
//			alias = 'standt', 
//			contactId = newContact.id, 
//			email='standarduser@testorg.com',
//	  		emailencodingkey='UTF-8', 
//	  		lastname='Testing', 
//	  		languagelocalekey='en_US',
//	  		localesidkey='en_US', 
//	  		profileid = newProfile.Id,
//	  		timezonesidkey='America/Los_Angeles', username='standarduser@northsight.test',IsActive=true
//	  	);
//  		insert newUser;
//  		//assert that the user exists
//		User u = [SELECT Id FROM User WHERE Id =: newUser.Id];
//		system.assert(u != null);
//	
//		//create a new geocode_cache__c
//		Geocode_Cache__c newGeocode = new Geocode_Cache__c(
//			address_hash__c = '400cedarstbrdgprtct60562',
//        	quarantined__c = false,
//        	location__latitude__s = 11.11111111,
//        	location__longitude__s = -111.22222222
//		);
//		insert newGeocode;
//		//assert that the geocode exists
//		Geocode_Cache__c g = [SELECT Id FROM Geocode_Cache__c WHERE Id =: newGeocode.Id];		
//		system.assert(g != null);
//	
//	
//		//create a new case
//		Case newCase = new Case(
//			Client__c = 'SG',
//			Due_Date__c = Date.today(),
//			Backyard_Serviced__c = 'Yes',
//			Excessive_Growth__c = 'No',
//			Debris_Removed__c = 'No',
//			Shrubs_Trimmed__c = 'Yes',
//			Work_Completed__c = 'No',
//			Vendor_Code__c = 'CHIGRS',
//			State__c = 'CT',
//			Status = 'Open',
//			City__c = 'Bridgeport',
//			Zip_Code__c = '60562',
//			Description = 'Work order for grass cutting',
//			OwnerId = newUser.Id,
//			ContactId = newContact.Id,
//			Street_Address__c = '400 Cedar st',
//			Geocode_Cache__c = newGeocode.Id,
//			Work_Ordered__c = 'Initial Grass Cut',
//			Work_Ordered_Text__c = 'Grass Cutting',
//			Reference_Number__c = '99999999'
//		);
//		insert newCase;
//		//assert that the case exists
//		Case cs = [SELECT Id, CaseNumber, Reference_Number__c FROM Case WHERE Id =: newCase.Id];
//		system.assert(cs != null);
//		/***********************************************************************************************************************************/
//		//set the payload var to mimic the one that would be sent by Pruvan
//		string surveyValidatePayload = '{"uuId":"9f307fb6-4c21-4d15-aabe-6c3fd9ea399d","username":"sarcj07TEST","key2":"NSTST::' + cs.CaseNumber + '","key3":"45022","key4":"InitialGrassCut","key5":null,"fileType":"survey","template":"sarcj07TEST::GC_001-v4","uploaderVersion":"mobile 3.8.7","srCertifiedPhoto":"1","srCertifiedTime":"1","srCertifiedLocation":null,"srPictureCount":null,"gpsLatitude":"30.0966739","gpsLongitude":"-90.4925435","gpsTimestamp":"1377811452","gpsAccuracy":"3013.0","timestamp":1377811542,"meta":{"parentId":"74283","surveyTemplateId":"sarcj07TEST::GC_001-v4","currentQuestionIndex":"12","surveyId":"021CB2EC-A10A-46D6-85C5-B0D527299819"},"answers":[{"id":"Work_Completed__c","answer":["Yes"],"hint":""},{"id":"Backyard_Serviced__c","answer":["Yes"],"hint":""},{"id":"Excessive_Growth__c","answer":["No"],"hint":""},{"id":"Shrubs_Trimmed__c","answer":["Yes"],"hint":""},{"id":"Debris_Removed__c","answer":["No"],"hint":""}]}';
//		
//		//instantiate the page
//		PageReference pageRef = Page.PruvanSurveyValidate;
//		
//		//set the page as the test starting point
//		Test.setCurrentPage(pageRef);
//		
//		//instantiate the controller
//		PruvanSurveyValidateController controller = new PruvanSurveyValidateController();
//		
//		//add the payload to the page parameters
//		ApexPages.currentPage().getParameters().put('payload', surveyValidatePayload);
//		
//		//assert that we get an error before controller execution
//		//system.assertEquals('{"error":"The web service call has failed to hit the proper web service to initiate execution."}', controller.getResponse());
//		
//		//fire off the controller's execute method
//		controller.execute();
//
//		//assert that we get the expected value back from the controller
//		//system.assertEquals('{"meta":{"surveyTemplateId":"sarcj07TEST::GC_001-v4","surveyId":"021CB2EC-A10A-46D6-85C5-B0D527299819","currentQuestionIndex":"12","parentId":"74283"},"error":null}', controller.getResponse());
//	}
}