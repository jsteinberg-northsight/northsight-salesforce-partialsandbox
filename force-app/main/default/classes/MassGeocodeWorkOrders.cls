public with sharing class MassGeocodeWorkOrders {
	/*
	public WorkOrderWrapper[] workorders{get;set;}
	public integer workOrderCount{get;set;}
	public boolean autorun{get;set;}
	private Map<id,Geocode_Cache__c> caseGeoCacheMap = new Map<id,Geocode_Cache__c>();
	private Map<string,Case[]> addrWOMap = new Map<string,Case[]>();
	private Map<string,Geocode_Cache__c> addrGeoMap = new Map<string,Geocode_Cache__c>();
	public MassGeocodeWorkOrders(){
		autorun=false;
		init();		
	}
	private void init(){
		caseGeoCacheMap = new Map<id,Geocode_Cache__c>();
		addrWOMap = new Map<string,Case[]>();
		addrGeoMap = new Map<string,Geocode_Cache__c>();
		AggregateResult r = [select count(id) WOCount from Case where Serviceable__c = 'Yes' and Geocode_Cache__c = null];
		workOrderCount = (integer)r.get('WOCount');
		Case[] cases = [select id,CaseNumber, Geocode_Cache__c, Street_Add_Concat__c, Street_Address__c, City__c, State__c, Zip_Code__c from Case where Serviceable__c = 'Yes' and Geocode_Cache__c = null limit 200];
		//Case[] cases = [select id, CaseNumber, Geocode_Cache__c, Street_Add_Concat__c, Street_Address__c, City__c, State__c, Zip_Code__c from Case where Serviceable__c = 'Yes' and ((Geocode_Cache__r.Location__Latitude__s = 0 and Geocode_Cache__r.Location__Longitude__s = 0 and Geocode_Cache__r.Last_Geocoded__c = null) or geocode_cache__c = null) limit 200];
		workorders = new WorkOrderWrapper[]{};
		preCheckGeocodeCache(cases);
		for(string addrKey: addrWOMap.KeySet()){
			for(Case c:addrWOMap.get(addrKey)){
				if(c.Geocode_Cache__c != null){
					WorkOrderWrapper w = new WorkOrderWrapper(c);
					w.addressHash = addrKey;
					workorders.add(w);
					
				}
			}
		}
	}
	private Case[] preCheckGeocodeCache(Case[] cases){
		for(Case workorder:cases){
			string addrKey = GeoUtilities.createAddressHash(workorder.Street_Add_Concat__c);
			if(!addrWOMap.containsKey(addrKey)){
				addrWOMap.put(addrKey,new Case[]{});	
			}
				
				addrWOMap.get(addrKey).add(workorder);
		}
		Geocode_Cache__c[] cacheList = [select id,Address_hash__c,quarantined__c, Flag__c from Geocode_Cache__c where Address_hash__c in :addrWOMap.KeySet()];
		for(Geocode_Cache__c cache:cacheList){
			for(Case c:addrWOMap.get(cache.Address_hash__c)){
				c.Geocode_Cache__c = cache.id;
				caseGeoCacheMap.put(c.Id,cache);
			}
		}
		return cases;
	}
	public void saveGeocodeRecords(){
		for(WorkOrderWrapper wo:workOrders){
			
			if((!addrGeoMap.ContainsKey(wo.addressHash)) && wo.latitude != 0 && wo.longitude != 0){
				boolean partial = true;
				string geocode_status = 'OK';
				if(wo.addressTypes.contains('street_address') || wo.addressTypes.contains('premise') || wo.addressTypes.contains('subpremise')) {
	    			partial = false;
	    		} else {
	    			partial = true;
	    			geocode_status = 'NOT_PRECISE';
	    		}
				addrGeoMap.put(wo.addressHash,new Geocode_Cache__c(
					Last_Geocoded__c = Date.Today(), 
					Address_hash__c = wo.addressHash,
					location__latitude__s= wo.latitude,
					location__longitude__s =  wo.longitude,
					Formatted_Address__c = GeoUtilities.formatAddress(wo.wo.Street_Address__c,wo.wo.City__c,wo.wo.State__c,wo.wo.Zip_Code__c),
					Formatted_Street_Address__c = GeoUtilities.capitalizeAll(GeoUtilities.preformatAddress(wo.wo.Street_Address__c)),
					Formatted_City__c = wo.wo.City__c,
					Formatted_State__c = wo.wo.State__c,
					Formatted_Zip_Code__c = wo.wo.Zip_Code__c,
    				Street_Address__c = wo.wo.Street_Address__c,
					Geocode_Status__c = geocode_status,
					Partial_Match__c = partial,
					Quarantined__c = partial
					)
					); 
			}	
		}
		insert addrGeoMap.Values();
		for(string addrKey:addrGeoMap.KeySet()){
			for(Case c:addrWOMap.get(addrKey)){
				caseGeoCacheMap.put(c.id,addrGeoMap.get(addrKey));
			}
		}
		savePrematchedGeocodeCache();
		init();
	}
	public void savePrematchedGeocodeCache(){
		List<Case> cases = new List<Case>();
		for(Id caseId:caseGeoCacheMap.KeySet()){
			
			if(caseGeoCacheMap.get(caseId).Quarantined__c&&caseGeoCacheMap.get(caseId).Flag__c){
				cases.add(new Case(Id=caseId,Approval_Status__c = 'Quarantined-Flagged',Geocode_Cache__c = caseGeoCacheMap.get(caseId).Id));
			}else if(caseGeoCacheMap.get(caseId).Quarantined__c){
				cases.add(new Case(Id=caseId,Approval_Status__c = 'Quarantined',Geocode_Cache__c = caseGeoCacheMap.get(caseId).Id));
			}
			else{
				cases.add(new Case(Id=caseId,Automatic_Reassignment__c = true,Geocode_Cache__c = caseGeoCacheMap.get(caseId).Id));
			} 
		}
		database.update(cases,false);
		
		/*******************************************************************************************************************/
		/*
		//create a list for case IDs
		List<ID> caseIDs = new List<ID>();
		//add case IDs from ordersToUpdate into caseIDs
		for(Case cID : cases){
			caseIDs.add(cID.Id);
		}
		//call the generateLinks method in the generateGeoLinksOffWorkOrders class sending a list of order IDs
		//the list of order IDs will be used to create geozone links
		generateGeoLinksOffWorkOrders.generateLinks(caseIDs);
		/*******************************************************************************************************************/
	/*}
	
	public static testmethod void test(){
	/*******************************************************************************************************************/
    /*//create a new client zone test obj for client zip codes
	ClientZone__c cz = new ClientZone__c();
	cz.Client_Name__c = 'SG';
	cz.Order_Type__c = 'Routine';
	insert cz;
	system.assert(cz.Id != null);
	
	//create new client zip code test objs for cases
	ClientZipCode__c[] zcList = new ClientZipCode__c[] {};
	
	zcList.add(new ClientZipCode__c(
		ClientZone__c = cz.Id,
		Zip_Code_Value__c = '78704'
	));
	
	zcList.add(new ClientZipCode__c(
		ClientZone__c = cz.Id,
		Zip_Code_Value__c = '60562'
	));
	insert zcList;
	
    //create a list for test cases and add cases to it
        case[] testcases = new case[] {};
        
        testcases.add(new case(
        						street_address__c = '1234 Street',
        						state__c = 'TX',
        						city__c = 'Austin',
        						zip_code__c = '78704',
        						Vendor_code__c = 'SRSRSR',
        						Work_Order_Type__c = 'Routine',
        						Client__c = 'SG',
        						Approval_Status__c = 'Not Started',
        						Canceled__c = false
        ));
        
        // Test case where the same address is already processed
        testcases.add(new case(
        						street_address__c = '1234 Street',
        						state__c = 'TX',
        						city__c = 'Austin',
        						zip_code__c = '78704',
        						Vendor_code__c = 'SRSRSR',
        						Work_Order_Type__c = 'Routine',
        						Client__c = 'SG',
        						Approval_Status__c = 'Not Started',
        						Canceled__c = false
        ));
        
        // test case where address is already in the geocode table
        testcases.add(new case(
        						street_address__c = '400 Cedar st',
        						state__c = 'CT',
        						city__c = 'Bridgeport',
        						zip_code__c = '60562',
        						Vendor_code__c = 'TRYWZYM',
        						Work_Order_Type__c = 'Routine',
        						Client__c = 'SG',
        						Approval_Status__c = 'Not Started',
        						Canceled__c = false
        ));
        
        // Test case where the address is partial
        testcases.add(new case(
        						street_address__c = '515 partial st',
        						state__c = 'CT',
        						city__c = 'Bridgeport',
        						zip_code__c = '60562',
        						Vendor_code__c = 'TRYWZYM',
        						Work_Order_Type__c = 'Routine',
        						Client__c = 'SG',
        						Approval_Status__c = 'Not Started',
        						Canceled__c = false
        ));
        insert testcases;
        
        //create a map of string to id and insert the client zip codes into the map as Map<Zip_Code_Value__c, ClientZipCode__c>
        Map<string, Id> czcMap = new Map<string, Id>();         
        for(ClientZipCode__c czc : [SELECT Id, Zip_Code_Value__c FROM ClientZipCode__c WHERE Id IN: zcList]){
        	czcMap.put(czc.Zip_Code_Value__c, czc.Id);
        }
        
        //create a list for the test cases
        List<Case> updateTestCases = new List<Case>();
        for(Case c : [SELECT Id, ClientZipCode__c, Zip_Code__c FROM Case WHERE Id IN: testcases]){
        	//check the map if there is a matching zip code btw a case and a client zip code
        	if(czcMap.containsKey(c.Zip_Code__c)){
        		//add the clientzipcode to the case
        		c.ClientZipCode__c = czcMap.get(c.Zip_Code__c);
        		system.assert(c.ClientZipCode__c != null);
        		updateTestCases.add(c);
        	}
        }
        update updateTestCases;
        
        MassGeocodeWorkOrders newTestCases = new MassGeocodeWorkOrders();
        
        newTestCases.init();
        
		System.assert(newTestCases.workorders.size() > 0);
		
		for(WorkOrderWrapper wo : newTestCases.workorders){
			wo.latitude = 47.4242424242;
			wo.longitude = -80.2424242424;
			wo.addressTypes = 'street_address';
		}
		
		newTestCases.saveGeocodeRecords();
		
		//create a geozone link list
       		List<GeoZoneLink__c> links = new List<GeoZoneLink__c>();
       
       		List<Case> cList = [SELECT Geocode_Cache__c, ClientZipCode__r.ClientZone__c FROM Case WHERE Id IN: testcases];
       		for(Case c : cList){
       			System.assert(c.Geocode_Cache__c != null);
       			System.assert(c.ClientZipCode__r.ClientZone__c != null);
       			
       			//fill the links list with the links that should have just been created
       			for(GeoZoneLink__c gz : [SELECT Id, Geocode_Cache__c, ClientZone__c 
       								FROM GeoZoneLink__c
       								WHERE Geocode_Cache__c =: c.Geocode_Cache__c AND ClientZone__c =: c.ClientZipCode__r.ClientZone__c]){
       			links.add(gz);
       			}
	}
	
       		//check to see if the list has a size > 0, if it does, then a geozone link was created off a work order
       		System.Assert(links.size() > 0);
	 
	}
*/
}