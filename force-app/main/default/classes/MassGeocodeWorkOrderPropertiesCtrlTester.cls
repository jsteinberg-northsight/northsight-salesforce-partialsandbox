@isTest 
private class MassGeocodeWorkOrderPropertiesCtrlTester {

//    private static ClientZone__c newClientZone;
//    private static ClientZipCode__c txClientZip;
//    private static ClientZipCode__c ctClientZip;
//    private static Case partialCase;
//    private static Case sameAddressCase1;
//    private static Case sameAddressCase2;
//    private static Case existingPropertyCase;
//    private static Geocode_Cache__c newGeocodeCache;
//    private static Case[] insertAllCases;
//    
//    private static void generateClientZoneData(){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new client zone
//        newClientZone = new ClientZone__c(
//            Client_Name__c = 'SG',
//            Client_Department__c = 'REO',
//            Order_Type__c = 'Routine',
//            Name = 'SG-RGC:TX53'
//        );
//        insert newClientZone;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new client zip code
//        txClientZip = new ClientZipCode__c(
//            ClientZone__c = newClientZone.Id,
//            Zip_Code_Value__c = '78704'
//        );
//        insert txClientZip;
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new client zip code
//        ctClientZip = new ClientZipCode__c(
//            ClientZone__c = newClientZone.Id,
//            Zip_Code_Value__c = '60562'
//        );
//        insert ctClientZip;
//    }
//    
//    private static void buildTestCaseWithPartialAddress(){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new case
//        partialCase = new Case(
//            Street_Address__c = '515 Partial St',
//            State__c = 'CT',
//            City__c = 'Bridgeport',
//            Zip_Code__c = '60562',
//            Vendor_Code__c = 'TRYWZYM',
//            Client__c = 'SG',
//            Work_Order_Type__c = 'Routine',
//            Work_Ordered__c = 'Grass Recut',
//            Loan_Type__c = 'REO'
//        );
//    }
//    
//    private static void buildTestCasesWithSameAddress(){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new case
//        sameAddressCase1 = new Case(
//            Street_Address__c = '1234 Street',
//            State__c = 'TX',
//            City__c = 'Austin',
//            Zip_Code__c = '78704',
//            Vendor_Code__c = 'SRSRSR',
//            Client__c = 'SG',
//            Work_Order_Type__c = 'Routine',
//            Work_Ordered__c = 'Initial Grass Cut',
//            Loan_Type__c = 'REO'
//        );
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new case
//        sameAddressCase2 = new Case(
//            Street_Address__c = '1234 Street',
//            State__c = 'TX',
//            City__c = 'Austin',
//            Zip_Code__c = '78704',
//            Vendor_Code__c = 'RSRSRS',
//            Client__c = 'SG',
//            Work_Order_Type__c = 'Routine',
//            Work_Ordered__c = 'Grass Recut',
//            Loan_Type__c = 'REO'
//        );
//    }
//    
//    private static void buildTestCaseWithExistingProperty(){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new geocode_cache
//        newGeocodeCache = new Geocode_Cache__c(
//            Address_Hash__c = '400cedarstbrgprtct60562',
//            Formatted_Address__c = '400 CEDAR ST, BRIDGEPORT, CT 60562',
//            Formatted_Street_Address__c = '400 CEDAR ST',
//            Formatted_City__c = 'BRIDGEPORT',
//            Formatted_State__c = 'CT',
//            Formatted_Zip_Code__c = '60562',
//            Street_Address__c = '400 Cedar St',
//            City__c = 'Bridgeport',
//            State__c = 'CT',
//            Zip_Code__c = '60562',
//            Location__Latitude__s = 0,//location__latitude__s = 11.11111111,
//            Location__Longitude__s = 0//location__longitude__s = -111.22222222
//        );
//        insert newGeocodeCache;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new case
//        existingPropertyCase = new Case(
//            Street_Address__c = '400 Cedar St',
//            State__c = 'CT',
//            City__c = 'Bridgeport',
//            Zip_Code__c = '60562',
//            Vendor_Code__c = 'TRYWZYM',
//            Client__c = 'SG',
//            Work_Order_Type__c = 'Routine',
//            Work_Ordered__c = 'Grass Recut',
//            Loan_Type__c = 'REO'
//        );
//    }
//    
//    //--------------------------------------------------------
//    //PRE-TEST SETUP
//    //set the client zip codes on the cases
//    private static void setClientZipCodeOnCases(){
//        //create a map of string to id and insert the client zip codes into the map as Map<Zip_Code_Value__c, ClientZipCode__c>
//        Map<string, Id> czcMap = new Map<string, Id>();         
//        for(ClientZipCode__c czc : [SELECT Id, Zip_Code_Value__c FROM ClientZipCode__c WHERE Id =: txClientZip.Id OR Id =: ctClientZip.Id]){
//            czcMap.put(czc.Zip_Code_Value__c, czc.Id);
//        }
//        system.assert(czcMap.Size() > 0);
//        
//        //create a list for the test cases
//        List<Case> updateTestCases = new List<Case>();
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//        for(Case c : [SELECT Id, ClientZipCode__c, Zip_Code__c FROM Case WHERE Id IN: insertAllCases]){
//            //check the map if there is a matching zip code btw a case and a client zip code
//            c.Client_Name__c =  account.Id;
//            if(czcMap.containsKey(c.Zip_Code__c)){
//                //add the clientzipcode to the case
//                c.ClientZipCode__c = czcMap.get(c.Zip_Code__c);
//                system.assert(c.ClientZipCode__c != null);
//                updateTestCases.add(c);
//            }
//        }
//        update updateTestCases;
//    }
//    
//    //--------------------------------------------------------
//    //TESTS
//    //Unit tests for method testing and regression testing. 
//    //--------------------------------------------------------
//        
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - testing the standard functionality of the MassGeocodeWorkOrderPropertiesController
//    public static testmethod void test(){
//    /*******************************************************************************************************************/
//        generateClientZoneData();
//        buildTestCaseWithExistingProperty();
//        buildTestCasesWithSameAddress();
//        buildTestCaseWithPartialAddress();
//        
//        //Padmesh Soni (06/24/2014) - Support Case Record Type
//        //Query record of Record Type of Account and Case objects
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
//                                            AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true ORDER BY DeveloperName];
//        
//        //Padmesh Soni (06/24/2014) - Support Case Record Type
//        //Assert statement
//        System.assertEquals(1, recordTypes.size());
//        
//        //OrderAssignmentHelper.init() is only called once from trigger, so must insert all cases in one go
//        insertAllCases = new Case[]{};
//        insertAllCases.add(existingPropertyCase);
//        insertAllCases.add(sameAddressCase1);
//        insertAllCases.add(sameAddressCase2);
//        insertAllCases.add(partialCase);
//        
//        //Padmesh Soni (06/24/2014) - Support Case Record Type
//        //add new instance into list of "Support" record type Case object
//        insertAllCases.add(new Case(RecordTypeId = recordTypes[0].Id, Street_Address__c = '400 Cedar St', State__c = 'CT', City__c = 'Bridgeport', 
//                                        Zip_Code__c = '60562', Vendor_Code__c = 'TRYWZYM', Client__c = 'SG', Work_Order_Type__c = 'Routine', 
//                                        Work_Ordered__c = 'Grass Recut', Loan_Type__c = 'REO'));
//        insert insertAllCases;
//        
//        setClientZipCodeOnCases();
//        
//        //Begin Test
//        //initialize()
//        MassGeocodeWorkOrderPropertiesController newTestCases = new MassGeocodeWorkOrderPropertiesController();
//        newTestCases.init();
//        
//        system.assert(newTestCases.workorders.size() > 0);
//        
//        //Padmesh Soni (06/24/2014) - Support Case Record Type
//        //assert statement
//        system.assertEquals(4, newTestCases.workorders.size());
//        
//        //set lat/lng and addressType on WO wrappers
//        for(WorkOrderWrapper wo : newTestCases.workorders){
//            if(wo.addressHash == '515partialstbrgprtct60562'){
//                wo.latitude = 47.4242424242;
//                wo.longitude = -80.2424242424;
//                wo.addressTypes = 'partial';
//                wo.wo.County__c = 'Bridgeport';
//            }
//            else if(wo.addressHash == '400cedarstbrgprtct60562'){
//                wo.latitude = 41.180119;//47.4242424242
//                wo.longitude = -73.176236;//-80.2424242424
//                wo.addressTypes = 'street_address';
//                //wo.wo.County__c = 'Bridgeport';
//                newTestCases.propertiesJson = '{"properties":[{"address":"400 CEDAR ST, BRIDGEPORT, CT 60562","county":"Fairfield"}]}';
//            }
//            else{
//                wo.latitude = 0;
//                wo.longitude = 0;
//                wo.addressTypes = 's';
//                wo.wo.County__c = '';
//            }
//        }
//        
//        //save geocoded records
//        newTestCases.saveGeocodeRecords();
//        
//        //Code added - Padmesh Soni (1/12/2015) - CCP-15:Fill County on Property
//        //New field County__c added into query
//		//check that properties were inserted from the order assignment helper class
//        Geocode_Cache__c[] geoResults = [SELECT Last_Geocoded__c, Address_Hash__c, Quarantined__c, Partial_Match__c, Location__Latitude__s, 
//                                            Location__Longitude__s, Geo_System__c, Geo_Timestamp__c, County__c FROM Geocode_Cache__c];
//                                            
//        system.debug('*****'+geoResults+'*****');
//        system.assertEquals(3, geoResults.size());
//        
//        for(Geocode_Cache__c geo : geoResults) {
//            if(geo.Address_Hash__c == '515partialstbrgprtct60562') {
//                system.assertEquals(true, geo.Quarantined__c);
//                system.assertEquals(true, geo.Partial_Match__c);
//                system.assertNotEquals(null, geo.Last_Geocoded__c);
//                system.assertEquals('MassGeocodeWorkOrderPropertiesController.cls', geo.Geo_System__c);
//                system.assert(Datetime.now() >= geo.Geo_Timestamp__c);
//                system.assert(Datetime.now().addMinutes(-1) < geo.Geo_Timestamp__c);
//                
//                //Code added - Padmesh Soni (1/12/2015) - CCP-15:Fill County on Property
//                //assert statement
//                system.assertEquals('Bridgeport',geo.County__c);
//        		//system.assert(geo.County__c != '');
//        		//system.assert(geo.County__c != null);
//                
//            } else if(geo.Address_Hash__c == '400cedarstbrgprtct60562') {
//                //system.assert(false, 'propsToUpdate: ' + newTestCases.propsToUpdate); 
//                system.assertEquals(false, geo.Quarantined__c);
//                system.assert(geo.Location__Latitude__s != 0, 'geo.Location__Latitude__s: ' + geo.Location__Latitude__s);
//                system.assert(geo.Location__Longitude__s != 0);
//                system.assertNotEquals(geo.Location__Longitude__s, geo.Location__Latitude__s);      
//                system.assertNotEquals(null, geo.Last_Geocoded__c);
//                system.assertEquals(false, geo.Partial_Match__c);
//                system.assertEquals(false, geo.Quarantined__c);
//                system.assertEquals('MassGeocodeWorkOrderPropertiesController.cls', geo.Geo_System__c);
//                system.assert(Datetime.now() >= geo.Geo_Timestamp__c);
//                system.assert(Datetime.now().addMinutes(-1) < geo.Geo_Timestamp__c);
//                
//                //Code added - Padmesh Soni (1/12/2015) - CCP-15:Fill County on Property
//                //assert statement
//                system.assertEquals('Fairfield',geo.County__c);
//        		//system.assert(geo.County__c != '');
//        		//system.assert(geo.County__c != null);
//            } else {
//                system.assertEquals(false, geo.Quarantined__c);
//                system.assert(geo.Location__Latitude__s != null);
//                system.assert(geo.Location__Longitude__s != null);
//                system.assertEquals(null, geo.Last_Geocoded__c);
//                system.assertEquals(null, geo.Geo_System__c);
//                system.assertEquals(null, geo.Geo_Timestamp__c);
//            }
//        }
//        
//        //create a geozone link list
//            List<GeoZoneLink__c> links = new List<GeoZoneLink__c>();
//       
//            List<Case> cList = [SELECT Geocode_Cache__c, ClientZipCode__r.ClientZone__c FROM Case WHERE Id IN: insertAllCases];
//            for(Case c : cList){
//                System.assert(c.Geocode_Cache__c != null);
//                System.assert(c.ClientZipCode__r.ClientZone__c != null);
//                
//                //fill the links list with the links that should have just been created
//                for(GeoZoneLink__c gz : [SELECT Id, Geocode_Cache__c, ClientZone__c 
//                                    FROM GeoZoneLink__c
//                                    WHERE Geocode_Cache__c =: c.Geocode_Cache__c AND ClientZone__c =: c.ClientZipCode__r.ClientZone__c]){
//                links.add(gz);
//                }
//    }
//    
//            //check to see if the list has a size > 0, if it does, then a geozone link was created off a work order
//            System.Assert(links.size() > 0);
//     
//    }
}