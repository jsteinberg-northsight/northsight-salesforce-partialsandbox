@isTest
private class Test_DeleteAllImageLinks {
///**
// *	Purpose			:	This is used to test the functionality of DeleteAllImageLinks.
// *
// *	Created By		:	Padmesh Soni
// *
// *	Created Date	:	03/01/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - As per Delete All Pruvan Image Links assignment
// *	
// *	Code Coverage	:	
// **/
//    static testMethod void testDeleteImageLinks() {
//        
//        //Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client') AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(2, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accounts;
//		
//		Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//		insert property;
//		
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN,Client_Name__c = account.Id));
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1), Status = Constants.CASE_STATUS_OPEN,Client_Name__c = account.Id));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//    	//List to hold Case imageLinks
//    	List<ImageLinks__c> images = new List<ImageLinks__c>();
//    	
//		for(Integer i = 1; i < 1100; i++) {
//			images.add(new ImageLinks__c(Name='Test'+i, CaseId__c = workOrders[0].Id));
//		}
//		
//		//insert ImageLinks records
//		insert images;
//		
//		//Test starts here
//		Test.startTest();
//		
//		//Call webservice method
//		DeleteAllImageLinks.deleteAllLinks(workOrders[0].Id);
//		
//		//Call webservice method
//		DeleteAllImageLinks.deleteAllLinks(workOrders[1].Id);
//		
//		//Test stops here
//		Test.stopTest();
//		
//		//Quering imageLink records
//		images = [SELECT Id FROM ImageLinks__c WHERE CaseId__c =: workOrders[0].Id];
//		System.assertEquals(0, images.size());
//    }
}