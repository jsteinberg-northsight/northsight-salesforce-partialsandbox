@isTest(seeAllData=false)
private class Test_SchedulePruvanGetOrderLogsDeletion {
///**
// *  Purpose         :   This is used for testing and covering SchedulePruvanOrderResponseLogsDeletion scheduler.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/02/2014
// *
// *	Current Version	:	V1.0
// *	
// *	Coverage		:	100%
// **/
// 
//    //Test method added
//	static testmethod void zoneHistoryTest(){
//		
//		//create a cron expression test obj
//		string CRON_EXP = '0 0 0 * * ? *';
//		
//		//start the test
//		Test.startTest();
//		
//		//create a string of the jobId for a new schedule instance of the SchedulePruvanOrderResponseLogsDeletion class
//		string jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new SchedulePruvanOrderResponseLogsDeletion());
//		
//		//query the CronTrigger table selecting in CronTrigger fields where the Id = jobId
//		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id =: jobId];
//		system.assertEquals(CRON_EXP, ct.CronExpression);
//		
//		//assert that the job has not been triggered
//		system.assertEquals(0, ct.TimesTriggered);
//		
//		//assert when the next fire time will be
//		system.assert(ct.NextFireTime != null);
//		
//		//Test stops here
//		Test.stopTest();
//	}
}