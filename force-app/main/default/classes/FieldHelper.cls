/*
Field update helper for generic web-service field update interface.  
Resolves type ambiguity by checking the destination field type and casting appropriately.  
Some additional handling for special field types may be added in the future. 
*/
public without sharing class FieldHelper {
//---------------------------------------------------------------------------
//PUBLIC
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//Generic value setter.  handles type checking for most field types. 
//---------------------------------------------------------------------------
public static void setFieldValue(sObject obj, string fieldName, object fieldValue){
	sObjectField f = obj.getSobjectType().getDescribe().fields.getMap().get(fieldName);
	if(f == null){//if f is null, throw an error
		if(fieldName.RIGHT(3).toLowerCase() == '__c'){
			throw new FieldSetterException('Custom Field: ' + fieldName + ' does not exist.');	
		}
		else {
			return;
		}
	}
	type primitiveType = typeMap.get(f.getDescribe().getType());
	if(primitiveType==String.class){
		setStringFieldValue(obj,fieldName,fieldValue);
	}else if(primitiveType == Boolean.class){
		setBooleanFieldValue(obj,fieldName,fieldValue);
	}else if(primitiveType == Integer.class){
		setIntegerFieldValue(obj,fieldName,fieldValue);
	}else if(primitiveType == Decimal.class){
		setDecimalFieldValue(obj,fieldName,fieldValue);
	}else if(primitiveType == Double.class){
		setDoubleFieldValue(obj,fieldName,fieldValue);
	}else if(primitiveType == Id.class){
		setIdFieldValue(obj,fieldName,fieldValue);
	}else if(primitiveType == DateTime.class){
		setDateTimeFieldValue(obj,fieldName,fieldValue);
	}else if(primitiveType == Date.class){
		setDateFieldValue(obj,fieldName,fieldValue);
	}else{
		throw new FieldSetterException('Field is of an unsupported type:'+f.getDescribe().getType());
	}
	
}
//---------------------------------------------------------------------------
//PRIVATE
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//Custom Exception Type for low lever error reporting
//---------------------------------------------------------------------------
private class FieldSetterException extends Exception{}
//---------------------------------------------------------------------------
//Distplay Type to Apex Type map since I couldn't find a neat way to do this with lanquage features. 
//---------------------------------------------------------------------------
private static map<Schema.DisplayType,type> typeMap = new map<Schema.DisplayType,type>{
		Schema.DisplayType.Boolean=>Boolean.class,
		Schema.DisplayType.Integer=>Integer.class,
		Schema.DisplayType.Currency=>Decimal.class,
		Schema.DisplayType.Percent=>Decimal.class,
		Schema.DisplayType.Double=>Double.class,
		Schema.DisplayType.Date=>Date.class,
		Schema.DisplayType.DateTime=>DateTime.class,
		Schema.DisplayType.Time=>Time.class,
		Schema.DisplayType.Phone=>String.class,
		Schema.DisplayType.Email=>String.class,
		Schema.DisplayType.EncryptedString=>String.class,
		Schema.DisplayType.base64=>String.class,
		Schema.DisplayType.Combobox=>String.class,
		Schema.DisplayType.Id=>Id.class,
		Schema.DisplayType.Picklist=>String.class,
		Schema.DisplayType.String=>String.class, 
		Schema.DisplayType.TextArea=>String.class,
		Schema.DisplayType.URL=>String.class
		}; 
//---------------------------------------------------------------------------
//Interprets the value as true or false and sets the field. 
//---------------------------------------------------------------------------
private static void setBooleanFieldValue(sObject obj, string fieldName, object fieldValue){
		set<string> trueStrings = new set<string>{'y','yes','1','true','t'};
		string val = string.valueof(fieldValue);
		boolean bval = trueStrings.contains(val.toLowerCase().trim());
		obj.put(fieldName,bval);
}
//---------------------------------------------------------------------------
//Simply casts the value to a string and sets the field.  
//---------------------------------------------------------------------------
private static void setStringFieldValue(sObject obj, string fieldName, object fieldValue){
		
		//TypeException handling logic added with else logic into existing once - Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
		try{
				
			string val = string.valueof(fieldValue);
			obj.put(fieldName,val);
		} catch(TypeException e){ 
			
			//Set the Invalid Survey Data field
			setFieldInvalidSurveyData(obj, fieldName, fieldValue);
			obj.put(fieldName,'');
		}
}
//---------------------------------------------------------------------------
//Attempts to cast the value to an integer before setting the field. 
//---------------------------------------------------------------------------
private static void setIntegerFieldValue(sObject obj, string fieldName, object fieldValue){
		
		//TypeException handling logic added with else logic into existing once - Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
		try {
				
			Integer val = integer.valueof(fieldValue);
			obj.put(fieldName,val);
		} catch(TypeException e) {
			
			//Set the Invalid Survey Data field
			setFieldInvalidSurveyData(obj, fieldName, fieldValue);
			obj.put(fieldName, 0);
		}	
}
//---------------------------------------------------------------------------
//Attempts to cast the value to a decimal before setting the field. 
//---------------------------------------------------------------------------
private static void setDecimalFieldValue(sObject obj, string fieldName, object fieldValue){
		
		//TypeException handling logic added with else logic into existing once - Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
		try {
				
			Decimal val = System.Decimal.valueof(string.valueof(fieldValue).replace('$',''));//Need to change this to use system currency info. 
			obj.put(fieldName,val);
		} catch(TypeException e) {
			
			//Set the Invalid Survey Data field
			setFieldInvalidSurveyData(obj, fieldName, fieldValue);
			obj.put(fieldName, 0.0);
		}
}
//---------------------------------------------------------------------------
//Attempts to cast the value to an integer before setting the field. 
//---------------------------------------------------------------------------
private static void setDoubleFieldValue(sObject obj, string fieldName, object fieldValue){
		
		//TypeException handling logic added with else logic into existing once - Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
		try {
			
			Double val = Double.valueof(string.valueof(fieldValue));
			obj.put(fieldName,val);
		} catch(TypeException e) {
			
			//Set the Invalid Survey Data field 
			setFieldInvalidSurveyData(obj, fieldName, fieldValue);
			obj.put(fieldName, 0.0);
		}	
}
//-------------------------------------------------------------------------------
//Attempts to cast the value to a Sakesforce sObject Id before setting the field. 
//-------------------------------------------------------------------------------
private static void setIdFieldValue(sObject obj, string fieldName, object fieldValue){
		
		//TypeException handling logic added with else logic into existing once - Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
		try {
			
			Id val = Id.valueof(string.valueof(fieldValue));
			obj.put(fieldName,val);
		} catch(TypeException e) { 
			
			//Set the Invalid Survey Data field
			setFieldInvalidSurveyData(obj, fieldName, fieldValue);
		}
}
//---------------------------------------------------------------------------
//Attempts to cast the value to a DateTime object before setting the field. 
//---------------------------------------------------------------------------
private static void setDateTimeFieldValue(sObject obj, string fieldName, object fieldValue){
		
		//TypeException handling logic added with else logic into existing once - Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
		try {	
			
			DateTime val = DateTime.valueof(string.valueof(fieldValue));
			obj.put(fieldName,val);
		} catch(TypeException e) {
			
			//Set the Invalid Survey Data field
			setFieldInvalidSurveyData(obj, fieldName, fieldValue);
		}
}
//---------------------------------------------------------------------------
//Attempts to cast the value to a Date object before setting the field. 
//---------------------------------------------------------------------------
private static void setDateFieldValue(sObject obj, string fieldName, object fieldValue){
		
		//TypeException handling logic added with else logic into existing once - Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
		try {
				
			Date val = Date.valueof(string.valueof(fieldValue));
			obj.put(fieldName,val);
		} catch(TypeException e) { 
			
			//Set the Invalid Survey Data field  
			setFieldInvalidSurveyData(obj, fieldName, fieldValue);
		}
}

	/**
	 *	@descpriton		:	This method is used to setting Invalid Survey Data field on Work Order (Case).
	 *						Method added - Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
	 *
	 *	@param			:	Case workOrder, String fieldName, object fieldValue
	 *
	 *	@return			:	void 
	 **/
	private static void setFieldInvalidSurveyData(Sobject sObj, String fieldName, object fieldValue) {
		
		//blank string variable to hold value of Invalid Survey Data field
		String invSurveyData = '';
		
		//Check for sobject type of Case sobject 
		if(sObj.getSobjectType() == Case.getSobjectType() || sObj.getSobjectType() == Bid__c.getSobjectType()) {
		
			//Check if Invalid Survey Data is blank or null
			if(sObj.get('Invalid_Survey_Data__c') == null || sObj.get('Invalid_Survey_Data__c') == '') {
			
				//Getting formatted value for Invalid Survey Data field
				invSurveyData += 'Question ' + fieldName + ':' + 'Answer '+ fieldValue;
			}else {
				
				//Getting formatted value for Invalid Survey Data field    
				invSurveyData += sObj.get('Invalid_Survey_Data__c')== null? 'Question ' + fieldName + ':' + 'Answer '+ fieldValue : sObj.get('Invalid_Survey_Data__c')+ '\n' + 'Question ' + fieldName + ':' + 'Answer '+ fieldValue;
			}
			
			//putting value on field
			sObj.put('Invalid_Survey_Data__c', invSurveyData);
			sObj.put('Flag__c', true);
		}
	}
	
		public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}