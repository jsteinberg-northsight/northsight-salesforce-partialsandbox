public with sharing class WorkOrderAuditControllerHelper {
	private static String MISMATCH = 'Mismatch';
	public static String MISSING_PHOTOS = 'MissingPhotos';
	public static String ORDER_ISSUES = 'OrderIssues';
	
	public static List<Case> fetchWorkOrder(String workOrderNumber) {
		List<Case> workOrders = [SELECT
													Id,
													CaseNumber,
													Reference_Number__c,
													Order_Issues__c,
													Missing_Photos__c,
													Street_Address__c,
													Street_Add_Concat__c,
													Work_Ordered__c,
													Description,
													Vendor_Notes_To_Staff__c,
													Internal_Notes__c,
													Geocode_Cache__r.Notes__c,
													Geocode_Cache__r.Internal_Property_Notes__c,
													Geo_Coordinates__c,
													Photos_Audited_By__c,
													Missing_Photo_Options__c
												FROM
													Case
												WHERE
													CaseNumber =: workOrderNumber
												LIMIT 1];
		return workOrders;
	}
	
	public static List<WorkOrderAuditController.Option> fetchOrderIssueOptions(Case workOrder) {
		WorkOrderAuditController.Option newOption;
		Boolean isFirstAudit = (workOrder.Photos_Audited_By__c == null) ? true : false;
		List<WorkOrderAuditController.Option> orderIssueOptions = new List<WorkOrderAuditController.Option>();
		List<Schema.PicklistEntry> picklist = Case.Order_Issues__c.getDescribe().getPicklistValues();
		List<String> picklistValues = new List<String>();
		String optionChecked = 'No';
		
		for (Schema.PicklistEntry currentValue : picklist) {
			picklistValues.add(currentValue.getValue());
		}
		for (String currentValue : picklistValues) {
			optionChecked = 'No';
			if (workOrder.Order_Issues__c != null && workOrder.Order_Issues__c.contains(currentValue)) {
				optionChecked = 'Yes';
			}
			newOption = new WorkOrderAuditController.Option(optionChecked, currentValue, currentValue, isFirstAudit, ORDER_ISSUES);
			if (isFirstAudit) {
				newOption.checked = 'No';
			}
			orderIssueOptions.add(newOption);
		}
		return orderIssueOptions;
	}
	
	public static List<WorkOrderAuditController.Option> fetchMissingPhotoOptions(Case workOrder, Set<Integer> pOptionsRange) {
		String lawnOption;
		String poolOption;
		Boolean isFirstAudit = (workOrder.Photos_Audited_By__c == null) ? true : false;
		List<WorkOrderAuditController.Option> missingPhotoOptions = new List<WorkOrderAuditController.Option>();
		List<Schema.PicklistEntry> picklist = Case.Missing_Photos__c.getDescribe().getPicklistValues();
		List<String> picklistValues = new List<String>();
		String optionChecked = 'Yes';
		Boolean houseNumberMismatch = false;
		Boolean streetSignMismatch = false;
		Integer optionCounter = 0;
		String streetNumber;
		String streetAddress;
		List<String> addressChunks = (workOrder.Street_Address__c == null) ? new List<String>() : workOrder.Street_Address__c.trim().split(' ');
		
		streetAddress = '';
		if (!addressChunks.isEmpty()) {
			streetNumber = ' (' + addressChunks[0] + ')';
			addressChunks.remove(0);
			for (String currentChunk : addressChunks) {
				streetAddress += currentChunk + ' ';
			}
			streetAddress = streetAddress.trim();
			streetAddress = ' (' + streetAddress + ')';
		} else {
			streetNumber = '';
			streetAddress = '';
		}
		
		for (Schema.PicklistEntry currentValue : picklist) {
			if (pOptionsRange.contains(optionCounter)) {
				picklistValues.add(currentValue.getValue());
			}
			optionCounter++;
		}
		for (String currentValue : picklistValues) {
			optionChecked = 'Yes';
			if (workOrder.Missing_Photos__c != null && workOrder.Missing_Photos__c.contains(currentValue)) {
				optionChecked = 'No';
				if (currentValue == 'House number mismatch') {
					houseNumberMismatch = true;
				} else if (currentvalue == 'Street sign mismatch') {
					streetSignMismatch = true;
				}
			}
			if (currentValue.toUpperCase().contains(MISMATCH)) {
				continue;
			}
			if (currentValue == 'House number') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, currentValue + streetNumber, currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'Street sign') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, currentValue + streetAddress, currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'Before Front - From Street') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Full View - Front', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'Before Front - From House') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Both Side Yards', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'Before - Both Side Yards') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Full View - Back', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'Before Back - From House') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Grass Under 12"', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'Before Back - From Prop Line') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Shrubs/Trees Touching House', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'Action - Weed Wacker') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Weed Wacker', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'Action - Lawn Mower') {
				lawnOption = '';
				if (!String.isBlank(workOrder.Missing_Photo_Options__c) && workOrder.Missing_Photo_Options__c.contains(';')) {
					lawnOption = workOrder.Missing_Photo_Options__c.split(';')[0];
				}
				missingPhotoOptions.add(new WorkOrderAuditController.Option(lawnOption, 'Lawn Mower', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'Action - Weed Spray') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Weed Spray', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'After - Edging') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Full View - Front', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'After Front - From Street') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Both Side Yards', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'After Front - From House') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Full View - Back', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'After - Both Side Yards') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Hard Edging Complete', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'After Back - From House') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Clippings Removed', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'After Back - From Prop Line') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Ruler Shows Under 2 Inches', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'After - Measurements') {
				missingPhotoOptions.add(new WorkOrderAuditController.Option(optionChecked, 'Any Overgrowth Left', currentValue, isFirstAudit, MISSING_PHOTOS));
			} else if (currentValue == 'After - Pad Lock') {
				poolOption = 'N/A';
				if (!String.isBlank(workOrder.Missing_Photo_Options__c) && workOrder.Missing_Photo_Options__c.contains(';')) {
					poolOption = workOrder.Missing_Photo_Options__c.split(';')[1];
				}
				missingPhotoOptions.add(new WorkOrderAuditController.Option(poolOption, 'Pad Lock (if pool present)', currentValue, isFirstAudit, MISSING_PHOTOS));
			} 
		}
		
		if (houseNumberMismatch || streetSignMismatch) {
			for (WorkOrderAuditController.Option currentOption : missingPhotoOptions) {
				if (currentOption.optionLabel.contains('House number') && houseNumberMismatch) {
					currentOption.checked = MISMATCH;
				} else if (currentOption.optionLabel.contains('Street sign') && streetSignMismatch) {
					currentOption.checked = MISMATCH;
				}
			}
		}
		return missingPhotoOptions;
	}
	
	public static String createOptionString(List<WorkOrderAuditController.Option> pOptions, String pOptionType) {
		String optionString = '';
		
		for (WorkOrderAuditController.Option currentOption : pOptions) {
			if (pOptionType == MISSING_PHOTOS) {
				if (currentOption.checked == 'No') {
					optionString += currentOption.optionValue + '; ';
				} else if (currentOption.checked.toUpperCase() == MISMATCH) {
					optionString += currentOption.optionValue + ' mismatch; ';
				}
			} else {
				if (currentOption.checked == 'Yes') {
					optionString += currentOption.optionValue + '; ';
				}
			}
		}
		optionString = optionString.removeEnd('; ');
		return optionString;
	}
	
	public static List<S3.ListEntry> fetchWorkOrderImages(String workOrderNumber, String bucketName) {
		String s3Key = 'AKIAJXTY25P3AXZATXXQ';
		String s3Secret =  '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';
		Datetime now = Datetime.now();
		Integer maxNumberToList = 100; //Set the number of Objects to return for a specific Bucket
		String prefix = workOrderNumber; //Limits the response to keys that begin with the indicated prefix. You can use prefixes to separate a bucket into different sets of keys in a way similar to how a file system uses folders. This is an optional argument.
		String marker = null; //Indicates where in the bucket to begin listing. The list includes only keys that occur alphabetically after marker. This is convenient for pagination: To get the next page of results use the last key of the current page as the marker. The most keys you'd like to see in the response body. The server might return less than this number of keys, but will not return more. This is an optional argument.
		String delimiter = null; //Causes keys that contain the same string between the prefix and the first occurrence of the delimiter to be rolled up into a single result element in the CommonPrefixes collection. These rolled-up keys are not returned elsewhere in the response. 
		
		S3.AmazonS3 amazonS3 = new S3.AmazonS3(s3Key, s3Secret);
		List<S3.ListEntry> s3Images = (System.Test.isRunningTest()) ?
			null :
			amazonS3.ListBucket(
				bucketName,
				prefix,
				marker,
				maxNumberToList,
				delimiter,
				s3Key,
				now,
				amazonS3.signature('ListBucket',now),
				s3Secret).Contents;
		return s3Images;
	}
	
	public static String fetchGeoUrl(Case pWorkOrder) {
		String urlBegin;
		String urlEnd;
		
		urlBegin = 'http://maps.google.com/maps?f=q&hl=en&q=';
		urlEnd = '&om=1';
		return (pworkOrder.Geocode_Cache__c == null) ? 
							urlBegin + pWorkOrder.Street_Add_Concat__c + urlEnd :
							urlBegin + pWorkOrder.Geo_Coordinates__c + urlEnd;
	}
}