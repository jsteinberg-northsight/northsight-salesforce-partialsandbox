@isTest(seeAllData=false)
private class Test_Batch_MonthlyDataUsageReset {
///**
// *  Purpose         :   This is used for testing and covering Batch_MonthlyDataUsageReset class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   07/24/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Padmesh Soni(07/24/2014) - VWMSubmitUpdate
// *	
// *	Coverage		:	100%
// **/
// 	
// 	//Test method to test the functionality of Batch_MonthlyDataUsageReset
//    static testMethod void myUnitTest() {
//    	
//    	//List to hold test records of Proxy sobject
//    	List<Proxy__c> proxies = new List<Proxy__c>();
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now().addMonths(-4).addDays(-10)));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now(), Data_Usage_This_Month__c = 2));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now().addMonths(-3), Data_Usage_This_Month__c = 2));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now().addMonths(-1).addDays(-5)));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now().addMonths(-1).addDays(-18), Data_Usage_This_Month__c = 2));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now().addDays(10), Data_Usage_This_Month__c = 2));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now().addMonths(4), Data_Usage_This_Month__c = 2));
//    	
//    	//insert Proxy Sobject's list
//    	insert proxies;
//    	
//        //Test starts here
//        Test.startTest();
//        
//        //instance of Batch
//        Batch_MonthlyDataUsageReset bc = new Batch_MonthlyDataUsageReset();
//        
//        //Batch executes here
//        Database.executeBatch(bc, 200);
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //query result of Case
//        proxies = [SELECT OwnerId FROM Proxy__c WHERE Data_Usage_This_Month__c = 0];
//        	
//    	//assert statement
//        System.assertEquals(4, proxies.size());
//    }
}