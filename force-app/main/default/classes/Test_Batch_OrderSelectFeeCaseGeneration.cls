@isTest(seeAllData=false)
private class Test_Batch_OrderSelectFeeCaseGeneration {
//
///**
// *  Purpose         :   This is used for testing and covering Batch_OrderSelectFeeCaseGeneration class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   07/09/2014
// *
// *  Current Version :   V1.0
// *
// *  Revision Log    :   V1.0 - Created - Padmesh Soni(07/09/2014) - Order Fee Generation Scheduled process
// *  
// *  Coverage        :   
// **/
//    
//    private static List<Account> accounts;
//    private static List<Contact> contacts;
//    private static Profile newProfile;
//    private static List<User> users;
//    private static Group queues;
//    
//    private static void generateTestData(integer countData){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP 
//        //create a new user with the Customer Portal Manager Standard profile
//        newProfile = [SELECT Id FROM Profile WHERE Name = 'Customer Portal Manager Standard'];
//        
//        //System.runAs ( new User(Id = UserInfo.getUserId()) ) {
//            //create a new user with the profile
//            queues = new Group(Name='Queue',Type='Queue');
//            insert queues;
//            
//            //Queue sObject   
//            QueueSobject queueObj = new QueueSobject(QueueId = queues.Id, SobjectType = 'Case');
//            insert queueObj;
//        
//            //Loop through countData time
//            for(integer i = 1; i <= countData; i++) {
//             
//                //PRE-TEST SETUP create a new account
//                Account newAccount = new Account(
//                    Name = 'OrderSelectFeeCaseGeneration Test Account ' + i
//                );
//                accounts.add(newAccount);
//            }
//            
//            insert accounts;
//            
//            //Loop through countData time
//            for(integer i = 1; i <= countData; i++) {
//            
//                //--------------------------------------------------------
//                //PRE-TEST SETUP
//                //create a new contact
//                contacts.add(new Contact(LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active', RecordTypeId = '01240000000UbtfAAC',
//                                            Last_Submitted_Date_Time__c = DateTime.now().addDays(2)));
//            }   
//            
//            insert contacts;
//            
//            //Loop through countData time
//            for(integer i = 1; i <= countData; i++) {
//            
//                //create a new user with the profile
//                users.add(new User(alias = 'standt'+i, contactId = contacts[i-1].Id, email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8',
//                                        lastname = 'Testing'+i, languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//                                        timezonesidkey = 'America/Los_Angeles', username = 'standarduser'+i+'@test.com', isActive = true));
//            }
//            
//            insert users;
//        //}
//    }
//    
//    //Test method to test the functionality of Batch_OrderSelectFeeCaseGeneration
//    /*static testMethod void myUnitTest() {
//        
//        //initialize variables
//        accounts = new List<Account>();
//        contacts = new List<Contact>();
//        users = new List<User>();
//        
//        generateTestData(1);
//        
//        //Query record of Record Type of Account and Case objects
//        //Padmesh Soni (06/19/2014) - Support Case Record Type
//        //Query one more record type of Case Object "SUPPORT"    
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client'
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(3, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsOnOrders = new List<Account>();
//        accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsOnOrders;
//        
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account; 
//        
//        //List to hold Case records
//        List<Case> workOrders = new List<Case>();
//
//        workOrders.add(new Case(
//                                RecordTypeId = recordTypes[1].Id, 
//                                AccountId = accountsOnOrders[0].Id, 
//                                OwnerId = users[0].Id,
//                                Due_Date__c = Date.today(), 
//                                Status = AssignmentMapConstants.CASE_STATUS_OPEN, 
//                                Approval_Status__c = 'Not Started',
//                                Client__c = AssignmentMapConstants.CASE_CLIENT_SG, 
//                                Work_Order_Type__c = 'Routine',
//                                Department__c = 'Test', 
//                                Work_Ordered__c = 'Grass Recut', 
//                                Self_Assigned__c = true, 
//                                Assignment_Program__c = 'OrderSelect',
//                                Assigned_Date_Time__c = DateTime.now(), 
//                                Street_Address__c = '13950 N. Northsight Blvd.', 
//                                City__c = 'Scottsdale', 
//                                State__c = 'AZ', 
//                                Zip_Code__c = '85260',Client_Name__c = account.Id));
//        workOrders.add(new Case(
//                                RecordTypeId = recordTypes[1].Id, 
//                                AccountId = accountsOnOrders[0].Id, 
//                                OwnerId = users[0].Id, 
//                                Due_Date__c = Date.today().addDays(1), 
//                                Status = AssignmentMapConstants.CASE_STATUS_OPEN, 
//                                Approval_Status__c = 'Not Started',
//                                Client__c = AssignmentMapConstants.CASE_CLIENT_SG, 
//                                Work_Order_Type__c = 'Routine', 
//                                State__c = 'TX',
//                                Self_Assigned__c = true, 
//                                Assignment_Program__c = 'OrderSelect',
//                                Assigned_Date_Time__c = DateTime.now(),Client_Name__c = account.Id));
//        workOrders.add(new Case(
//                                RecordTypeId = recordTypes[1].Id, 
//                                AccountId = accountsOnOrders[1].Id, 
//                                OwnerId = users[0].Id, 
//                                Due_Date__c = Date.today(), 
//                                Status = AssignmentMapConstants.CASE_STATUS_OPEN, 
//                                Approval_Status__c = 'Not Started', 
//                                Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, 
//                                Work_Order_Type__c = 'Maintenance', 
//                                State__c = 'LA',
//                                Department__c = 'Test', 
//                                Self_Assigned__c = true, 
//                                Assignment_Program__c = 'OrderSelect',
//                                Assigned_Date_Time__c = DateTime.now(),Client_Name__c = account.Id));
//        workOrders.add(new Case(
//                                RecordTypeId = recordTypes[1].Id, 
//                                AccountId = accountsOnOrders[1].Id, 
//                                OwnerId = users[0].Id, 
//                                Due_Date__c = Date.today().addDays(1), 
//                                Status = AssignmentMapConstants.CASE_STATUS_OPEN, 
//                                Approval_Status__c = 'Not Started', 
//                                Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, 
//                                Work_Order_Type__c = 'Maintenance', 
//                                State__c = 'AR',
//                                Department__c = 'Test1', 
//                                Work_Ordered__c = 'Grass Initial Cut', 
//                                Self_Assigned__c = true, 
//                                Assignment_Program__c = 'OrderSelect',
//                                Assigned_Date_Time__c = DateTime.now(),Client_Name__c = account.Id));
//        workOrders.add(new Case(
//                                RecordTypeId = recordTypes[2].Id, 
//                                AccountId = accountsOnOrders[1].Id, 
//                                OwnerId = users[0].Id, 
//                                Due_Date__c = Date.today().addDays(1), 
//                                Status = AssignmentMapConstants.CASE_STATUS_OPEN, 
//                                Approval_Status__c = 'Not Started', 
//                                Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, 
//                                Work_Order_Type__c = 'Test', 
//                                State__c = 'AR',
//                                Department__c = 'Facebook', 
//                                Work_Ordered__c = 'Testing',
//                                Self_Assigned__c = true, 
//                                Assignment_Program__c = 'OrderSelect',
//                                Assigned_Date_Time__c = DateTime.now(),Client_Name__c = account.Id));
//        
//        //insert case records
//        insert workOrders;
//        
//        
//            
//        //Test starts here
//        Test.startTest();
//        Case testWorkOrder = [Select Id, Client_Name__c from Case where Id =: workOrders[0].Id limit 1 ];
//        System.Debug(workOrders[0].Client_Name__c);
//        System.assert(testWorkOrder.Client_Name__c !=null);
//       
//        //instance of Batch
//        Batch_OrderSelectFeeCaseGeneration bc = new Batch_OrderSelectFeeCaseGeneration();
//        
//        //Batch executes here
//        Database.executeBatch(bc, 200);
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //query result of Case
//        workOrders = [SELECT OwnerId FROM Case WHERE OwnerId =: bc.caseQueues[0].Id ];
//            
//        //assert statement
//        //System.assertEquals(5, workOrders.size());
//            
//        //query result of Case
//        workOrders = [SELECT Id FROM Case WHERE Client__c = 'FOLLOW'];
//        
//        //assert statement
//        //System.assertEquals(5, workOrders.size());
//    }*/
}