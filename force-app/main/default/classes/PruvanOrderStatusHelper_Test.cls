@isTest(SeeAllData=false)
/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/
*/
public with sharing class PruvanOrderStatusHelper_Test {
	
//	private static string workOrderStatusPayload;
//	
//	//Padmesh Soni (06/26/2014) - Support Case Record Type
//	//Formatted test class with optimization of codes
//    private static List<Case> workOrders;
//	
//	private static void generateTestCases() {
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//   		//create a new account
//		Account newAccount = new Account(Name = 'Tester Account');
//		insert newAccount;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new contact
//		Contact newContact = new Contact(LastName = 'Tester', AccountId = newAccount.Id, Pruvan_Username__c = 'NSBobTest');
//					newContact.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//	newContact.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//	newContact.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//	newContact.Shrub_Trim__c = 3;//Pricing...required 
//	newContact.REO_Grass_Cut__c = 3;//Pricing
//	newContact.Pay_Terms__c = 'Net 30';//Terms required
//	newContact.Trip_Charge__c = 25;//Pricing
//	newContact.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//	newContact.Date_Active__c = Date.Today();//Date Active must be set. 
//	newContact.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//	newContact.MailingStreet = '30 Turnberry Dr';
//	newContact.MailingCity = 'La Place';
//	newContact.MailingState = 'LA';
//	newContact.MailingPostalCode = '70068';
//	newContact.OtherStreet = '30 Turnberry Dr';
//	newContact.OtherCity = 'La Place';
//	newContact.OtherState = 'LA';
//	newContact.OtherPostalCode = '70068';
//	newContact.Other_Address_Geocode__Latitude__s = 30.00;
//	newContact.Other_Address_Geocode__Longitude__s = -90.00;
//		insert newContact;
//		
//		//Padmesh Soni (06/26/2014) - Support Case Record Type
//    	//Query record of Record Type of Case object
//		List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
//											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
//		
//		//Padmesh Soni (06/26/2014) - Support Case Record Type
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//    	//create a new test cases
//		workOrders.add(new Case(street_address__c = '1234 Street',state__c = 'TX', city__c = 'Austin', zip_code__c = '78704', Vendor_code__c = 'SRSRSR',
//        							Work_Order_Type__c = 'Routine', Client__c = 'NOT-SG', Loan_Type__c = 'REO', ContactId = newContact.Id,
//        							Work_Ordered__c = 'Grass Recut', Approval_Status__c = 'Not Started', Work_Completed__c = 'Yes', 
//        							Date_Serviced__c = Date.today(),Client_Name__c = account.Id));
//        workOrders.add(new Case(street_address__c = '1234 Street',state__c = 'TX', city__c = 'Austin', zip_code__c = '78704', Vendor_code__c = 'SRSRSR',
//        							Work_Order_Type__c = 'Routine', Client__c = 'NOT-SG', Loan_Type__c = 'REO', ContactId = newContact.Id,
//        							Work_Ordered__c = 'Grass Recut', Approval_Status__c = 'Not Started', Work_Completed__c = 'Yes', 
//        							Date_Serviced__c = Date.today(),Client_Name__c = account.Id));
//        workOrders.add(new Case(street_address__c = '1234 Street',state__c = 'TX', city__c = 'Austin', zip_code__c = '78704', Vendor_code__c = 'SRSRSR',
//        							Work_Order_Type__c = 'Routine', Client__c = 'NOT-SG', Loan_Type__c = 'REO', ContactId = newContact.Id,
//        							Work_Ordered__c = 'Grass Recut', Approval_Status__c = 'Not Started', Work_Completed__c = 'Yes', 
//        							Date_Serviced__c = Date.today(),Client_Name__c = account.Id));
//        workOrders.add(new Case(RecordTypeId = recordTypes[0].Id, street_address__c = '1234 Street',state__c = 'TX', city__c = 'Austin', zip_code__c = '78704', Vendor_code__c = 'SRSRSR',
//        							Work_Order_Type__c = 'Routine', Client__c = 'NOT-SG', Loan_Type__c = 'REO', ContactId = newContact.Id,
//        							Work_Ordered__c = 'Grass Recut', Approval_Status__c = 'Not Started', Work_Completed__c = 'Yes', 
//        							Date_Serviced__c = Date.today(),Client_Name__c = account.Id));
//        insert workOrders;
//	}
//	
//	private static void setPayload(){
//		
//		//Padmesh Soni (06/26/2014) - Support Case Record Type
//    	//Format the JSON string 
//		workOrderStatusPayload = '{"workOrders": [';
//		
//		//Padmesh Soni (06/26/2014) - Support Case Record Type
//    	//Loop through query result of Case
//		for(Case workOrder : [SELECT CaseNumber From Case Where Id IN: workOrders]) {
//			
//			//Format the JSON string 
//			workOrderStatusPayload += '{"workOrderNumber": "NSM::' + workOrder.CaseNumber + '","status": "Complete"},';
//		}
//		
//		//Padmesh Soni (06/26/2014) - Support Case Record Type
//    	//Check String is ending with comma character
//		if(workOrderStatusPayload.endsWith(',')) {
//			
//			//Format the JSON string 
//			workOrderStatusPayload = workOrderStatusPayload.substring(0, workOrderStatusPayload.lastIndexOf(','));
//			workOrderStatusPayload += ']}';
//		}
//	}
//	
//	//--------------------------------------------------------
//   	//TESTS
//   	//Unit tests for method testing and regression testing. 
//   	//--------------------------------------------------------
//   		
//   	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - test that the PruvanOrderStatusHelper is functioning correctly
//	public static testmethod void testPruvanOrderStatusHelper(){
//		
//		//initializae the list of Case
//		workOrders = new List<Case>();
//		
//		system.Test.startTest();
//		
//		generateTestCases();
//		setPayload();
//		
//		PruvanOrderStatusHelper.updateOrderStatus(workOrderStatusPayload);
//		
//		//Padmesh Soni (06/26/2014) - Support Case Record Type
//    	//Qurey result of Case records
//		workOrders = [SELECT Pruvan_Status__c From Case Where Id IN: workOrders ORDER BY CaseNumber];
//		
//		//Padmesh Soni (06/26/2014) - Support Case Record Type
//    	//assert statements
//		system.assertEquals('Complete', workOrders[0].Pruvan_Status__c);
//		system.assertEquals('Complete', workOrders[1].Pruvan_Status__c);
//		system.assertEquals('Complete', workOrders[2].Pruvan_Status__c);
//		system.assertEquals(null, workOrders[3].Pruvan_Status__c);
//		
//		system.Test.stopTest();
//	}
}