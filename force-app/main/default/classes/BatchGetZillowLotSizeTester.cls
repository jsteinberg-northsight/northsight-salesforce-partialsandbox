@isTest(SeeAllData=false)
public with sharing class BatchGetZillowLotSizeTester {
//
///**
// *  Purpose         :   This is a test class for BatchGetZillowLotSize class to test the functionality. 
// *
// *  Created By      :   
// *
// *  Created Date    :   
// *
// *  Current Version :   V1.1
// *
// *  Revision Log    :   V1.0 - Created
// *                      V1.1 - Modified - Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements - 5 test method added for cover the code
// *
// *	Code Coverage	:	93%   
// **/
// 	
// 	//Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements
//	//class variable to hold all Property records
//	private static List<Geocode_Cache__c> newProperties;
//	
//	//instance of Porperty
//	private static Geocode_Cache__c newProperty;
//	
//	//--------------------------------------------------------
//    //PRE-TEST SETUP
//    //create a new geocode_cache
//	private static void createNewProperty(integer counter){
//		
//		//Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements
//		//initialize instance of List
//		newProperties = new List<Geocode_Cache__c>();
//		
//		//Loop through till counter to create property instance
//		for(integer i=0; i < counter; i++) {
//	    	
//	    	//create a new instance of Geo Code Cache(Property)
//	        newProperty = new Geocode_Cache__c(Address_Hash__c = '2114bigelowavenseattlewa98109', Formatted_Address__c = '2114 BIGELOW AVE N, SEATTLE, WA 98109', 
//	        									Formatted_Street_Address__c = '2114 BIGELOW AVE N', Formatted_City__c = 'SEATTLE', Formatted_State__c = 'WA', 
//	        									Formatted_Zip_Code__c = '98109', Street_Address__c = '2114 Bigelow Ave N', City__c = 'Seattle', State__c = 'WA', 
//	        									Zip_Code__c = '98109', Location__Latitude__s = 47.637933, Location__Longitude__s = -122.347938, 
//	        									Last_Geocoded__c = Date.today());
//	        
//	        //insert Property here
//       		insert newProperty;
//       		
//	        //Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements
//			//add newProperty into list
//	        newProperties.add(new Geocode_Cache__c(Address_Hash__c = '2114bigelowavenseattlewa98109', Formatted_Address__c = '2114 BIGELOW AVE N, SEATTLE, WA 98109', 
//	        									Formatted_Street_Address__c = '2114 BIGELOW AVE N', Formatted_City__c = 'SEATTLE', Formatted_State__c = 'WA', 
//	        									Formatted_Zip_Code__c = '98109', Street_Address__c = '2114 Bigelow Ave N', City__c = 'Seattle', State__c = 'WA', 
//	        									Zip_Code__c = '98109', Location__Latitude__s = 47.637933, Location__Longitude__s = -122.347938, 
//	        									Last_Geocoded__c = Date.today()));
//		}
//		
//		//Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements
//		//Check if counter is more than one 
//		//then insert the list of Properties
//		if(counter > 1)
//			insert newProperties;
//	}
//	
//	//Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements
//	//Code commented becuase this code has be throw error that "Test method does not support web service callouts" 
//	//--------------------------------------------------------
//    //TESTS
//    //Unit tests for method testing and regression testing. 
//    //--------------------------------------------------------
//        
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - testing that the BatchGeocodeWorkOrderProperties class is functioning correctly
//	/**public static testmethod void testBatchGetZillowLotSize(){
//		createNewProperty(1);
//		
//		system.Test.startTest();
//			//create and run a new instance of the BatchGetZillowLotSize class
//	        BatchGetZillowLotSize testBatch = new BatchGetZillowLotSize();
//	        database.executeBatch(testBatch, 1);
//		system.Test.stopTest();
//		
//		/**assert the batch fields are being correctly handled
//		system.assertEquals('2114 BIGELOW AVE N', testBatch.streetAddressParameter);
//		system.assertEquals('SEATTLE', testBatch.cityParameter);
//		system.assertEquals('WA', testBatch.stateParameter);
//		system.assertEquals('98109', testBatch.zipCodeParameter);
//		system.assertEquals(1, testBatch.limitCounter);
//		
//		for(Geocode_Cache__c gc : [SELECT SqFt_of_Lot_Size__c, Zillow_Status__c FROM Geocode_Cache__c WHERE Id =: newProperty.Id]){
//			system.assertNotEquals(0, gc.SqFt_of_Lot_Size__c);
//			system.assertNotEquals('There was no Zillow response message', gc.Zillow_Status__c);
//			system.assertNotEquals('The request was successfully processed, but there was no return on the lot size', gc.Zillow_Status__c);
//			system.assertNotEquals('Zillow did not return a response', gc.Zillow_Status__c);
//			system.assertNotEquals('Please make sure all address fields are filled out correctly', gc.Zillow_Status__c);
//			system.assertEquals(4680, gc.SqFt_of_Lot_Size__c);
//			system.assertEquals('Code: 0, Request successfully processed', gc.Zillow_Status__c);
//		}
//	}**/
//	
//	//Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements
//	//Test method for unit test of BatchGetZillowLotSize with correct response from zillow
//	static testMethod void testBatchWithCorrectResponse() {
//		
//		//call generic method to creating Property records
//		createNewProperty(10);
//		
//        //create and run a new instance of the BatchGetZillowLotSize class
//        BatchGetZillowLotSize testBatch = new BatchGetZillowLotSize();
//        
//        //setting response body
//        testBatch.responseXMLString = ZillowControllerMockClass.getResponseBody();
//        
//		//Test Starts here
//        Test.startTest();
//        
//        //Execution of batch
//		Database.executeBatch(testBatch, 200);
//        
//        //Test stop here
//        Test.stopTest();
//        
//        //Loop through queried Property records
//        for(Geocode_Cache__c gc : [SELECT SqFt_of_Lot_Size__c, Zillow_Status__c FROM Geocode_Cache__c WHERE Id IN: newProperties]){
//        	
//			system.assertEquals(4680, gc.SqFt_of_Lot_Size__c);
//			system.assertEquals('Code: 0, Request successfully processed', gc.Zillow_Status__c);
//		}
//	}
//	
//	//Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements
//	//Test method for unit test of BatchGetZillowLotSize with No LotSize Response from zillow
//	static testMethod void testBatchWithNoLotSizeResponse() {
//		
//		//call generic method to creating Property records
//		createNewProperty(10);
//		
//		//initialize noOfResponse to 1
//		ZillowControllerMockClass.noOfResponse = 1;
//		
//		//create and run a new instance of the BatchGetZillowLotSize class
//        BatchGetZillowLotSize testBatch = new BatchGetZillowLotSize();
//        
//        //setting response body
//        testBatch.responseXMLString = ZillowControllerMockClass.getResponseBody();
//        
//		//Test Starts here
//        Test.startTest();
//        
//        //Execution of batch
//		Database.executeBatch(testBatch, 200);
//        
//        //Test stop here
//        Test.stopTest();
//        
//        //Loop through queried Property records
//        for(Geocode_Cache__c gc : [SELECT SqFt_of_Lot_Size__c, Zillow_Status__c FROM Geocode_Cache__c WHERE Id IN: newProperties]){
//        	
//			system.assertEquals(0, gc.SqFt_of_Lot_Size__c);
//			system.assertEquals('The request was successfully processed, but there was no return on the lot size', gc.Zillow_Status__c);
//		}
//	}
//	
//	//Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements
//	//Test method for unit test of BatchGetZillowLotSize With Wrong Response from zillow
//	static testMethod void testBatchWithWrongResponse() {
//		
//		//call generic method to creating Property records
//		createNewProperty(10);
//		
//		//initialize noOfResponse to 1
//		ZillowControllerMockClass.noOfResponse = 2;
//		
//		//create and run a new instance of the BatchGetZillowLotSize class
//        BatchGetZillowLotSize testBatch = new BatchGetZillowLotSize();
//        
//        //setting response body
//        testBatch.responseXMLString = ZillowControllerMockClass.getResponseBody();
//        
//		//Test Starts here
//        Test.startTest();
//        
//        //Execution of batch
//		Database.executeBatch(testBatch, 200);
//        
//        //Test stop here
//        Test.stopTest();
//        
//        //Loop through queried Property records
//        for(Geocode_Cache__c gc : [SELECT SqFt_of_Lot_Size__c, Zillow_Status__c FROM Geocode_Cache__c WHERE Id IN: newProperties]){
//        	
//			system.assertEquals(0, gc.SqFt_of_Lot_Size__c);
//			system.assertEquals('Code: 508, Error: no exact match found for input address', gc.Zillow_Status__c);
//		}
//	}
//	
//	//Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements
//	//Test method for unit test of BatchGetZillowLotSize with no addresses on property
//	static testMethod void testBatchNoAddresses() {
//		
//		//initialize instance of List
//		newProperties = new List<Geocode_Cache__c>();
//		
//		//Loop through till counter to create property instance
//		for(integer i=0; i < 10; i++) {
//	    	
//	    	//create a new instance of Geo Code Cache(Property)
//	        newProperties.add(new Geocode_Cache__c(Active__c = true));
//		}
//		
//		//insert newProperties here
//		insert newProperties;
//		
//		//create and run a new instance of the BatchGetZillowLotSize class
//        BatchGetZillowLotSize testBatch = new BatchGetZillowLotSize();
//        
//        //Test Starts here
//        Test.startTest();
//        
//        //Execution of batch
//		Database.executeBatch(testBatch, 200);
//        
//        //Test stop here
//        Test.stopTest();
//        
//        //Loop through queried Property records
//        for(Geocode_Cache__c gc : [SELECT SqFt_of_Lot_Size__c, Zillow_Status__c FROM Geocode_Cache__c WHERE Id IN: newProperties]){
//        	
//			system.assertEquals(null, gc.SqFt_of_Lot_Size__c);
//			system.assertEquals(null, gc.Zillow_Status__c);
//		}
//	}
//	
//	//Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements
//	//Test method for unit test of BatchGetZillowLotSize with no response from zillow
//	static testMethod void testBatchWithNoResponse() {
//		
//		//call generic method to creating Property records
//		createNewProperty(10);
//		
//		//create and run a new instance of the BatchGetZillowLotSize class
//        BatchGetZillowLotSize testBatch = new BatchGetZillowLotSize();
//        
//        //setting response body
//        testBatch.responseXMLString = null;
//        
//		//Test Starts here
//        Test.startTest();
//        
//        //Execution of batch
//		Database.executeBatch(testBatch, 200);
//        
//        //Test stop here
//        Test.stopTest();
//        
//        //Loop through queried Property records
//        for(Geocode_Cache__c gc : [SELECT SqFt_of_Lot_Size__c, Zillow_Status__c FROM Geocode_Cache__c WHERE Id IN: newProperties]){
//        	
//			system.assertEquals('Zillow did not return a response', gc.Zillow_Status__c);
//		}
//	}
}