@RestResource(urlMapping='/WeatherAlerts')
global without sharing class WeatherAlertHelper {
	@HttpPost
    global static valertResponse doPost() {
    	        //RestRequest instance  
        RestRequest req = RestContext.request;
        //Getting request body as String
        String alertData = req.requestBody.ToString();
        
        valertResponse result =  new valertResponse();
        
        
        try{       
        result.data = alertData;
        //Deserialize the input payload Summary Data String and get Root map 
        Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(alertData);
        
        result.data = alertData;
        result.headers = JSON.serialize(req.headers);
        string username = req.headers.get('Username');
        string password = req.headers.get('Password');
       	if(!(username == 'northsight' && password=='374172b12ea467c24a237a24')){
       		result.result = 'login failed';
       		result.data = null;
       		result.headers = null;
       		
       	}
       	//extract assets
       	object[] alerts = (object[])root.get('alerts');
		Geocode_Cache__c[] props = new Geocode_Cache__c[]{};
       	if(alerts!=null){
       	for(object o:alerts){
       		Map<String,Object> alertMap = (Map<String,Object>)o;
       		object[] assets = (object[])alertMap.get('assets');
       		for(object o2:assets){
       			Map<String,Object> assetMap = (Map<String,Object>)o2;
       			Geocode_Cache__c c = new Geocode_Cache__c();
       			c.VAlert_Id__c  = (String)assetMap.get('id');
       			object[] stations = (object[])assetMap.get('stations');
       			for(object o3:stations){
       				 
       				Map<string,Object> station = (Map<String,Object>)o3;
       				system.assert(station.get('type')!=null,'Type of alert cannot be null');
       				if(station.get('type')=='snowdepth'||station.get('type')=='snowcover'){
       					c.Last_Reported_Snow_Cover__c = (Decimal)alertMap.get('measurement_value');
       					c.Last_Snow_Report_Date__c = DateTime.Now();
       					props.add(c);
       				}
       				
       			}
       		} 
       	}
       		database.upsert(props,Geocode_Cache__c.VAlert_Id__c,false);
       	}else{
       		result.data  = 'No Alerts to process';
       	}
        }catch(Exception e){
       		result.data = e.getStackTraceString();
       	} 
       	//set fields 
        return result;
           
    }
    
    global class valertResponse{
    	global string result = 'success'; 
    	global string data = null;
    	global string headers = null;
    }
    
}