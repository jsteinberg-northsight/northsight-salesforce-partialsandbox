@isTest(seeAllData=false)
private class TestSubmitUpdate {
///**
// *  Purpose         :   This is used for testing and covering VWMSubmitUpdate class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   07/24/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Padmesh Soni(07/24/2014) - VWMSubmitUpdate
// *	
// *	Coverage		:	91%
// **/
// 	
// 	//Test method to test the functionality of VWMSubmitUpdate
//    static testMethod void testit(){
//    	Test.startTest();
//    	
//    	//Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client') AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(2, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accounts;
//		
//		//List to hold test records of Proxy sobject
//    	List<Proxy__c> proxies = new List<Proxy__c>();
//    	proxies.add(new Proxy__c(IP__c = '198.162.1.1', Port__c = 80, UserName__c = 'padmeshsoni', Password__c = 'sf#12345', State__c = 'RJ', 
//    								Requests_Today__c = 1, Last_Used__c = Datetime.now().addDays(2), Monthly_Data_Limit__c = 13000000, 
//    								Data_Usage_This_Month__c = 50));
//    	
//    	List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today(), Status = 'Open', 
//    								Client__c = 'SG',Client_Name__c = account.Id));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1), 
//    								Status = 'Open', Mobile__c = true,Client_Name__c = account.Id));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1), 
//    								Status = 'Open',Client_Name__c = account.Id));
//    	workOrders.add(new Case(AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1), 
//    								Status = 'Open', Client__c = 'SG', street_address__c = '1234 Street', Mobile_Version__c = 3.9,
//    								state__c = 'TX', city__c = 'Austin', zip_code__c = '78704', Vendor_code__c = 'SRSRSR',
//    								Work_Order_Type__c = 'Routine', Loan_Type__c = 'REO', Work_Ordered__c = 'Grass Recut',
//    								Pruvan_Status__c = 'Complete', Survey_Received__c = true, All_Images_Received__c = true, 
//    								Approval_Status__c = 'Not Started', Work_Completed__c = 'Yes', Date_Serviced__c = Date.today(),Client_Name__c = account.Id));
//    									
//    	//insert case records
//    	Database.SaveResult[] results = database.insert(workOrders,false);
//    	for(Case c: workOrders){
//    		//System.AssertNotEquals(Null,c.Id,string.valueOf(c));
//    	}
//    	
//    	//List to hold image Link records
//    	List<ImageLinks__c> imageLinks = new List<ImageLinks__c>();
//    	
//    	//Loop till 10 counters
//    	for(integer i = 0; i < 50; i++) {
//    		
//    		//populate imagelinks list
//	    	imageLinks.add(new ImageLinks__c(Pruvan_evidenceType__c= 'Survey', GPS_Timestamp__c= DateTime.now(), 
//	    										ImageUrl__c='https://s3.amazonaws.com/NorthsightTmp/02570223/52820869.jpg', CaseId__c= workOrders[0].Id, 
//	    										GPS_Location__Latitude__s = 30.087567, GPS_Location__Longitude__s=-90.485625, Pruvan_gpsAccuracy__c=2531, 
//	    										Survey_Question_Id__c='Trip_Charge_Reason__c', Survey_Id__c='GC23_001-v5', 
//	    										Notes__c ='This is a picture of the lawn.'+ i, Is_Summarized__c = true));
//	    	imageLinks.add(new ImageLinks__c(Pruvan_evidenceType__c= 'Survey', GPS_Timestamp__c= DateTime.now(), 
//	    										ImageUrl__c='https://s3.amazonaws.com/NorthsightTmp/02570223/52820869.jpg', CaseId__c= workOrders[1].Id, 
//	    										GPS_Location__Latitude__s = 30.087567, GPS_Location__Longitude__s=-90.485625, Pruvan_gpsAccuracy__c=2531, 
//	    										Survey_Question_Id__c='Trip_Charge_Reason__c', Survey_Id__c='GC23_001-v5', 
//	    										Notes__c = i + 'This is a picture of the lawn.', Is_Summarized__c = true));
//	    	imageLinks.add(new ImageLinks__c(Pruvan_evidenceType__c= 'Survey', GPS_Timestamp__c= DateTime.now(), 
//	    										ImageUrl__c='https://s3.amazonaws.com/NorthsightTmp/02570223/52820869.jpg', CaseId__c= workOrders[3].Id, 
//	    										GPS_Location__Latitude__s = 30.087567, GPS_Location__Longitude__s=-90.485625, Pruvan_gpsAccuracy__c=2531, 
//	    										Survey_Question_Id__c='Trip_Charge_Reason__c', Survey_Id__c='GC23_001-v5', 
//	    										Notes__c = i + 'This is a picture of the lawn.', Is_Summarized__c = true));
//    	}
//    	
//    	//insert Image Links here
//    	//insert imageLinks;
//    	
//    	//Test starts here
//    	
//    	
//    	//Variable to hold error message
//    	String errorMessage = 'Nothing sent.  Errors prior to send. ';
//    	
//    	//set current page paramenter
//    	Apexpages.currentPage().getParameters().put('Id', workOrders[0].Id);
//    	
//    	//Instance of VWMSubmitUpdate
//		VWMSubmitUpdate controller = new VWMSubmitUpdate();
//		controller.validate();//DON - Added validate to get page messages shown for errors in constructor
//		controller.sendRequest();
//		
//		//assert statment
//		System.assertEquals(errorMessage, controller.responseString);
//		
//		//insert Proxy sobject test records
//    	insert proxies;
//    	
//    	//set current page paramenter
//    	Apexpages.currentPage().getParameters().put('Id', workOrders[1].Id);
//    	
//    	//Instance of VWMSubmitUpdate
//		controller = new VWMSubmitUpdate();
//		controller.validate();//DON - Added validate to get page messages shown for errors in constructor
//		controller.sendRequest();
//		
//		//assert statment
//		System.assertEquals(errorMessage, controller.responseString);
//		
//    	//set current page paramenter
//    	Apexpages.currentPage().getParameters().put('Id', workOrders[2].Id);
//    	
//    	//Instance of VWMSubmitUpdate
//		controller = new VWMSubmitUpdate();
//		controller.validate();//DON - Added validate to get page messages shown for errors in constructor
//		controller.sendRequest();
//		
//		//assert statment
//		System.assertEquals(errorMessage, controller.responseString);
//		
//		//set current page paramenter
//    	Apexpages.currentPage().getParameters().put('Id', workOrders[3].Id);
//    	
//    	//Instance of VWMSubmitUpdate
//		controller = new VWMSubmitUpdate();	
//		controller.validate();//DON - Added validate to get page messages shown for errors in constructor
//		controller.sendRequest();
//		
//		//assert statements
//		//System.assertEquals(0,controller.errorCount);
//		
//		//Code added - Padmesh Soni(07/29/2014) - VWMSubmitUpdate (Suspend and Disable Proxies automatically)
//		//Loop through query iterative flow of Proxy sobject
//		for(Proxy__c proxy : [SELECT Requests_Today__c, Data_Usage_This_Month__c, Last_Failure__c, Fail_Count__c FROM Proxy__c WHERE Id IN: proxies]) {
//			
//			//assert statements
//			//System.assertEquals(Date.today(), proxy.Last_Failure__c.date());			
//			//System.assertEquals(1, proxy.Fail_Count__c);
//		}
//		
//		//Code added - Padmesh Soni(07/29/2014) - VWMSubmitUpdate (Suspend and Disable Proxies automatically)
//		//call method again as per the test data.
//		controller.sendRequest();
//		
//		//Loop through query iterative flow of Proxy sobject
//		for(Proxy__c proxy : [SELECT Requests_Today__c, Data_Usage_This_Month__c, Last_Failure__c, Fail_Count__c FROM Proxy__c WHERE Id IN: proxies]) {
//			
//			//assert statements
//			//System.assertEquals(3, proxy.Requests_Today__c);			
//			//System.assertEquals(100, proxy.Data_Usage_This_Month__c);
//			
//			//Code added - Padmesh Soni(07/29/2014) - VWMSubmitUpdate (Suspend and Disable Proxies automatically)
//			//assert statements
//			//System.assertEquals(0, proxy.Fail_Count__c);
//		}
//		
//		//call controller's method of getting update string
//		String updateStr = controller.getUpdateString();
//		
//		//assert statement
//		//System.assertNotEquals(null, updateStr);
//		
//		//instance of Id
//		Id a;
//		
//		//set current page paramenter
//    	Apexpages.currentPage().getParameters().put('Id', a);
//    	
//    	//Instance of VWMSubmitUpdate
//		controller = new VWMSubmitUpdate();	
//		
//		proxies[0].Last_Used__c = Datetime.now().addMonths(-3);
//		proxies[0].Last_Failure__c = Datetime.now().addMonths(-3);
//		update proxies;
//		
//		//set current page paramenter
//    	Apexpages.currentPage().getParameters().put('Id', workOrders[3].Id);
//    	
//    	//Instance of VWMSubmitUpdate
//		controller = new VWMSubmitUpdate();
//		controller.validate();
//		controller.sendRequest();
//		controller.sendRequest();
//		
//		for(Proxy__c proxy : [SELECT Requests_Today__c, Data_Usage_This_Month__c FROM Proxy__c WHERE Id IN: proxies]) {
//			//System.assertEquals(2, proxy.Requests_Today__c);			
//			//System.assertEquals(50, proxy.Data_Usage_This_Month__c);
//		}		
//		
//		//Test stops here
//		Test.stopTest();
//	}
}