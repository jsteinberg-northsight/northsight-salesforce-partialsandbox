@isTest(seeAllData=false)
private class Test_contactCommunicationController {
//
///**
// *  Purpose         :   This is used for testing and covering contactCommunicationController functionality.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/30/2014
// *
// *	Current Version	:	V1.1
// *
// *	Revision Log	:	V1.0 - Created - Padmesh Soni(06/30/2014) - Required Maintenance: Unit Test Improvements
// *
// *	Coverage		:	94% - V1.0
// **/
//
//    static testMethod void testContactCommunicationController() {
//        
//        //List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890'));
//    	insert accounts;
//		
//		//List to hold test records of Contact
//		List<Contact> contacts = new List<Contact>();
//		contacts.add(new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = accounts[0].Id));
//		insert contacts;
//		
//		//List to hold test records of Event
//		List<Event> events = new List<Event>();
//		events.add(new Event(ActivityDate = Date.today().addDays(-5), Subject = 'Call', StartDateTime = Datetime.now().addDays(-5), 
//								EndDateTime = Datetime.now(), WhoId = contacts[0].Id));
//		events.add(new Event(ActivityDate = Date.today().addDays(-2), Subject = 'Call', StartDateTime = Datetime.now().addDays(-2), 
//								EndDateTime = Datetime.now().addDays(2), WhoId = contacts[0].Id));
//		events.add(new Event(ActivityDate = Date.today(), Subject = 'Call', StartDateTime = Datetime.now(), 
//								EndDateTime = Datetime.now().addDays(10), WhoId = contacts[0].Id));
//		insert events;
//		
//		//List to hold test records of Event
//		List<Task> tasks = new List<Task>();
//		tasks.add(new Task(ActivityDate = Date.today().addDays(-5), Subject = 'Call', Type = 'Other', Status = 'In Progress', Priority = 'High', 
//								WhoId = contacts[0].Id, WhatId = accounts[0].Id));
//		tasks.add(new Task(ActivityDate = Date.today().addDays(-1), Subject = 'Call', Type = 'Other', Status = 'In Progress', Priority = 'Low', 
//								WhoId = contacts[0].Id, WhatId = accounts[0].Id));
//		tasks.add(new Task(ActivityDate = Date.today().addDays(1), Subject = 'Call', Type = 'Other', Status = 'In Progress', Priority = 'Normal', 
//								WhoId = contacts[0].Id, WhatId = accounts[0].Id));
//		insert tasks;
//		
//		//Test starts here
//    	Test.startTest();
//    	
//    	ApexPages.currentPage().getParameters().put('id', contacts[0].Id);
//    	
//		//Instance of controller
//		contactCommunicationController controller = new contactCommunicationController();
//		
//		//assert statement
//		System.assertEquals(6, controller.listClass.size());
//		
//		//Call controller's methods
//		controller.getEvents();
//		controller.getTasks();
//		    	
//    	//Test stops here
//    	Test.stopTest();
//    }
}