@isTest()
/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/
*/
public with sharing class PruvanUploadPicturesController_Test {
//	public static testmethod void testValidateController(){
//		/***********************************************************************************************************************************/
//		/*
//		this sections handles the creation of objects that will need to be used by some of the functions for testing purposes
//		*/
//		//create a new account
//		Account newAccount = new Account(
//			Name = 'Tester Account',
//			RecordTypeId = '01240000000Ubta'
//		);
//		insert newAccount;
//		//assert that the account exists
//		system.assert(newAccount.Id != null);
//	
//		//create a new contact
//		Contact newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest',
//			RecordTypeId = '01240000000UbtfAAC'
//		);
//		insert newContact;
//		//assert that the contact exists
//		system.assert(newContact.Id != null);
//		
//		//Create a new user with the Customer Portal Manager Standard profile
//		Profile newProfile = [SELECT Id FROM profile WHERE Name = 'Partner Community - Processors'];
//		system.assert(newProfile != null);
//		User newUser = new User(
//			alias = 'standt', 
//			contactId = newContact.id, 
//			email='standarduser@testorg.com',
//	  		emailencodingkey='UTF-8', 
//	  		lastname='Testing', 
//	  		languagelocalekey='en_US',
//	  		localesidkey='en_US', 
//	  		profileid = newProfile.Id,
//	  		timezonesidkey='America/Los_Angeles', username='standarduser@northsight.test',IsActive=true
//	  	);
//  		insert newUser;
//  		//assert that the user exists
//		system.assert(newUser.Id != null);
//	
//		//create a new geocode_cache__c
//		Geocode_Cache__c newGeocode = new Geocode_Cache__c(
//			address_hash__c = '400cedarstbrdgprtct60562',
//        	quarantined__c = false,
//        	location__latitude__s = 11.11111111,
//        	location__longitude__s = -111.22222222
//		);
//		insert newGeocode;
//		//assert that the geocode exists	
//		system.assert(newGeocode.Id != null);
//		
//		//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//		//Fetch record type of Account Sobject
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client' 
//													AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//		
//    	//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//    	//Create a test instance for Account sobject
//		Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//			
//		//create a new case
//		Case newCase = new Case(
//			Client__c = 'SG',
//			Due_Date__c = Date.today(),
//			Backyard_Serviced__c = 'Yes',
//			Excessive_Growth__c = 'No',
//			Debris_Removed__c = 'No',
//			Shrubs_Trimmed__c = 'Yes',
//			Work_Completed__c = 'No',
//			Vendor_Code__c = 'CHIGRS',
//			State__c = 'CT',
//			Status = 'Open',
//			City__c = 'Bridgeport',
//			Zip_Code__c = '60562',
//			Description = 'Work order for grass cutting',
//			OwnerId = newUser.Id,
//			ContactId = newContact.Id,
//			Street_Address__c = '400 Cedar st',
//			Geocode_Cache__c = newGeocode.Id,
//			Work_Ordered__c = 'Initial Grass Cut',
//			Work_Ordered_Text__c = 'Grass Cutting',
//			
//			//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//			//New field populated
//			Client_Name__c = account.Id
//		);
//		insert newCase;
//		//assert that the case exists
//		Case cs = [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id];
//		system.assert(cs.Id != null);
//		/***********************************************************************************************************************************/
//		//set the payload var to mimic the one that would be sent by Pruvan
//		string uploadPicturesPayload = '{"username":"pruvan","password":"8c8cd34c37d6813808baa358b18169c211cb47f3","key2":"'+cs.CaseNumber+'","uuid":"a7df9730-94a1-11e2-9e96-0800200c9a65","parentUuid":"c16a7080-94a1-11e2-9e96-0800200c9a66","timestamp":1377559122,"evidenceType":"before","fileType":"picture","fileName":"1363971679689.jpg","csrCertifiedPhoto":"1","csrCertifiedTime":"1","csrCertifiedLocation":"1","csrPictureCount":"","gpsLatitude":"30.508968","gpsLongitude":"-97.678362","gpsTimestamp":"1363971674","gpsAccuracy":"10.000000"}';
//		string uploadPicturespayload2 = '{"username": "pruvan", "password": "8c8cd34c37d6813808baa358b18169c211cb47f3", "key2": "' + cs.CaseNumber + '","uuid": "a7df9730-94a1-11e2-9e96-0800200c9a65","parentUuid": "c16a7080-94a1-11e2-9e96-0800200c9a66","timestamp": 1377559122,"evidenceType": "before","fileType": "survey","fileName": "1363971679689.jpg","csrCertifiedPhoto": "1","csrCertifiedTime": "1","csrCertifiedLocation": "1","csrPictureCount": "","gpsLatitude": "30.508968","gpsLongitude": "-97.678362","gpsTimestamp": "1363971674","gpsAccuracy": "10.000000","nsSurveyExtract": {"meta": {"parentId": "74283","surveyTemplateId": "sarcj07TEST::GC_001-v4","currentQuestionIndex": "12","surveyId": "021CB2EC-A10A-46D6-85C5-B0D527299819"},"answers": [{"id": "Work_Completed__c","answer": ["Yes"],"hint": ""}, {"id": "Backyard_Serviced__c","answer": ["Yes"],"hint": ""}, {"id": "Excessive_Growth__c","answer": ["No"],"hint": ""}, {"id": "Shrubs_Trimmed__c","answer": ["Yes"],"hint": ""}, {"id": "Debris_Removed__c","answer": ["No"],"hint": ""}]}}';
//		
//		//instantiate the page
//		PageReference pageRef = Page.PruvanUploadPictures;
//		
//		//set the page as the test starting point
//		Test.setCurrentPage(pageRef);
//		
//		//instantiate the controller
//		PruvanUploadPicturesController controller = new PruvanUploadPicturesController();
//		
//		//add the payload to the page parameters
//		ApexPages.currentPage().getParameters().put('payload', uploadPicturesPayload);
//		
//		//assert that we get an error before controller execution
//		system.assertEquals('{"error":"The web service call has failed to hit the proper web service to initiate execution."}', controller.getResponse());
//		
//		//fire off the controller's execute method
//		controller.execute();
//		
//		//assert that we get the expected value back from the controller
//		//system.assertEquals('{"status":true,"error":null}', controller.getResponse());
//		
//		
//		/*/testing a payload that has a fileType of survey with survey meta data and answers
//		//instantiate the controller
//		PruvanUploadPicturesController controller2 = new PruvanUploadPicturesController();
//		
//		//add the payload to the page parameters
//		ApexPages.currentPage().getParameters().put('payload', uploadPicturesPayload2);
//		
//		//assert that we get an error before controller execution
//		system.assertEquals('{"error":"The web service call has failed to hit the proper web service to initiate execution."}', controller2.getResponse());
//		
//		//fire off the controller's execute method
//		controller2.execute();
//		
//		//assert that we get the expected value back from the controller
//		system.assertEquals('{"status":true,"error":null}', controller2.getResponse());
//		
//		//select the test case survey field values that should have been filled and assert they equal the values we expect
//        for(Case surveyAnswer : [SELECT Id,
//										Work_Completed__c, 
//										Date_Serviced__c,
//										Backyard_Serviced__c,
//										Excessive_Growth__c,
//										Shrubs_Trimmed__c,
//										Debris_Removed__c,
//										Mobile__c
//								FROM Case
//								WHERE Id =: newCase.Id]){
//									
//			system.assertEquals('Yes', surveyAnswer.Work_Completed__c);
//        	system.assertEquals(Date.today(), surveyAnswer.Date_Serviced__c);
//        	system.assertEquals('Yes', surveyAnswer.Backyard_Serviced__c);
//        	system.assertEquals('No', surveyAnswer.Excessive_Growth__c);
//        	system.assertEquals('Yes', surveyAnswer.Shrubs_Trimmed__c);
//        	system.assertEquals('No', surveyAnswer.Debris_Removed__c);
//        	//system.assertEquals(true, selectedCase.Mobile__c);
//        }*/
//	}
}