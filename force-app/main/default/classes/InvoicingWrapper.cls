/**
 *  Purpose         :   This class is used as wrapper to store invoice details which will be used on Invoicing system. 
 *
 *  Created By      :   
 *
 *  Created Date    :   
 *  Current Version :   V1.1
 *
 *  Revision Log    :   V1.0 - Created - AC-6:Invoice Screen | Visual Force Page Prototype
 *						V1.1 - Modified - Padmesh Soni (1/6/2015) - AC-6:Invoice Screen | Visual Force Page Prototype
 *								Changes are:
 *									1. Code modified with appropriate comments.
 **/
public with sharing class InvoicingWrapper {
	//Class properties
	public string id{get;set;}
	public string invoiceNumber{get;set;}
	public decimal discount{get;set;}
	public decimal total{get;set;}
	public date paidDate{get;set;}
	public date created{get;set;}
	public string status{get;set;}
	public string priceBook{get;set;}
	public string client{get;set;}
	public string clientId{get;set;}
	public string vendor{get;set;}
	public string vendorId{get;set;}
	public date sent{get;set;}
	public decimal subTotal{get;set;}
	public String privateNotes{get; set;} 
	public String publicNotes {get; set;}
	public string workOrder {get;set;}
	public string recordType {get;set;}
	
	//Constructor definition
	public InvoicingWrapper(Invoice__c i){

		id = i.Id;
		created = i.CreatedDate.date();
		invoiceNumber = i.Name;
		discount = i.Discount__c;
		total = i.Invoice_Total__c;
		paidDate = i.Paid_Date__c;
		status = i.Status__c;
		priceBook = i.Price_Book__c;
		vendorId  = i.Pay_To__c;
		clientId = i.Receive_From__c;
		workOrder = i.Work_Order__c;
		recordType = i.RecordTypeId;
		if(i.Receive_From__r != null)
		client = i.Receive_From__r.Name;
		else
		client = '';
		if(i.Pay_To__r != null)
		vendor = i.Pay_To__r.Name;
		else
		vendor = '';
		sent = i.Sent__c;
		subTotal = i.Sub_Total__c.setScale(2);
		privateNotes = i.Private_Notes__c;
		publicNotes = i.Public_Notes__c;
	}
}