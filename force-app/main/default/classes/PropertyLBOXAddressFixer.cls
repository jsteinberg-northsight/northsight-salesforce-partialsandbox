/*
	This class is meant for new work orders that are not yet associated with any geocode cache records
	For each work order record it must:
	 1. See if the address is already cached, associate the new work order with it, 
	     and any further duplicate addresses
	 2. Create a new cache (up to 10 new per execute) if it's not already cached,
	      associate further duplicate addresses with it
*/
global without sharing class PropertyLBOXAddressFixer{// implements Database.Batchable<Geocode_Cache__c>, Database.Stateful, Database.AllowsCallouts{
	/*
	global Iterable<Geocode_Cache__c> start(Database.BatchableContext BC){
		return [select Street_Address__c, City__c, State__c, Zip_Code__c, Formatted_Address__c,Formatted_Street_Address__c, Formatted_City__c, Formatted_State__c, Formatted_Zip_Code__c,Manual_Location__c  from Geocode_Cache__c where Street_Address__c != null and Formatted_Street_Address__c = null order by createddate limit 50000];
	}
	
	global void execute(Database.BatchableContext BC, List<Geocode_Cache__c> properties){
		for(Geocode_Cache__c property:properties) {
			// Step 1: Hash the address
			//if(property.Manual_Location__c==false){
			//	property.Formatted_Address__c = GeoUtilities.formatAddress(property.Street_Address__c,property.City__c,property.State__c,property.Zip_Code__c);
			//}
			
			property.Address_hash__c = GeoUtilities.createAddressHash(GeoUtilities.formatAddress(property.Street_Address__c,property.City__c,property.State__c,property.Zip_Code__c));
			property.Formatted_Street_Address__c = GeoUtilities.capitalizeAll(GeoUtilities.preformatAddress(property.Street_Address__c));
			property.Formatted_City__c = property.City__c;
			property.Formatted_State__c = property.State__c;
			property.Formatted_Zip_Code__c = property.Zip_Code__c;
		}
		update properties;
	}	
	
	 
	global void finish(Database.BatchableContext BC){
		}

*/
}