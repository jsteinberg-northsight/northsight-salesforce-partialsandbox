@isTest(seeAllData=false)
private class Test_SearchMarkerJSONController {
///**
// *	Description		:	This is test class for SearchMarkerJSONController class to validate that SearchMarkerJSONController's functionality 
// *						is running well or not. 
// *
// *	Created By		:	Padmesh Soni	
// *
// *	Created Date	:	05/28/2014
// *
// *	Current Version	:	V1.1
// *
// *	Revisiion Logs	:	V1.0 - Created
// *
// *	Code Coverage	:	100%
// **/
// 	
// 	//Test method to test the funcationality of getSearchResult method
//    static testMethod void testGetSearchResult() {
//        
//        //List to hold Account records
//        List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Tester Account'));
//		
//		//insert account here
//		insert accounts;
//		
//		//List to hold Account records
//        List<Contact> contacts = new List<Contact>();
//		contacts.add(new Contact(LastName = 'Tester0', AccountId = accounts[0].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active',
//									Other_Address_Geocode__Latitude__s = 41.785896, Other_Address_Geocode__Longitude__s = -87.598936));
//		
//		//insert contact here
//		insert contacts;
//		
//        //Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: AssignmentMapConstants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: AssignmentMapConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: AssignmentMapConstants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: AssignmentMapConstants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: AssignmentMapConstants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client') AND (SobjectType =: AssignmentMapConstants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: AssignmentMapConstants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(2, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accountsOnOrders = new List<Account>();
//    	accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accountsOnOrders;
//		
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//		workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX',
//    								Scheduled_Date_Override__c = Date.today(), Work_Ordered__c = 'Grass Recut'));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX',
//    								Scheduled_Date_Override__c = Date.today().addDays(-6), Work_Ordered__c = 'Grass Recut'));
//    	//insert case records
//    	insert workOrders;
//		
//		//List to hold all Property records
//		List<Geocode_Cache__c> properties = new List<Geocode_Cache__c>();
//		
//		for(Geocode_Cache__c property : [SELECT Id, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c]) {
//			
//			//assign Geolocation values to fields
//			property.Location__Latitude__s = 50.09567;
//			property.Location__Longitude__s = -10.4532;
//			properties.add(property);
//		}
//		
//		System.assertEquals(1, properties.size());
//		update properties;
//		
//		//Initialize page reference of AssignmentMapSearchJSON page
//		PageReference p = Page.SearchMarkerJSON;
//		
//		//Test start here
//		Test.startTest();
//		
//		//Set URL parameter of Current page instance
//		ApexPages.currentPage().getParameters().put('term','0');
//		
//		//instance of SearchMarkerJSONController
//		SearchMarkerJSONController controller = new SearchMarkerJSONController();
//		
//		//Call method
//		controller.getSearchResult();
//		
//		//assert statement
//		System.assert(controller.searchJSONMarkers != null);
//		
//		//Test stops here
//		Test.stopTest();	
//	}
}