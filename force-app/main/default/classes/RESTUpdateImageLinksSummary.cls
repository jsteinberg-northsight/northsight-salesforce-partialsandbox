/**
 *  Purpose         :   This is UpdateImageLinksSummary Listner should accept JSON input and update the specified fields on the Work Orders(Case) 
 *                      specified.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   02/01/2014
 *
 *  Current Version :   V1.1
 *
 *  Revision Log    :   V1.0 - Created
 *                      V1.1 - Modified - Padmesh Soni (06/18/2014) - Support Case Record Type
 *
 **/
@RestResource(urlMapping='/RESTUpdateImageLinksSummary')
global class RESTUpdateImageLinksSummary {
    
    /**
     *  @descpriton     :   This HttpPost method is used to return parsed response of Image Link summary records to update the existing Image Link and
     *                      Work Order records.
     *
     *  @param          :
     *
     *  @return         :   void 
     **/
    @HttpPost
    global static void updateImageLinksSummary() {
        
        //RestRequest instance  
        RestRequest req = RestContext.request;
        
        //Getting request body as String
        String summaryData = req.requestBody.ToString();
        
        //Check if inputJSON request string is not null or blank
        if(!String.isBlank(summaryData)) {
            
            //Deserialize the input payload Summary Data String and get Root map 
            Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(summaryData);
            
            //Map to hold parent WorkOrder with their Object instance
            Map<String, Object> mapOrdersWithNumber = new Map<String, Object>();
            
            //Getting envelop root element Object list    
            List<Object> items = (List<Object>)root.get(Constants.CASE_WORKORDERS);
            
            //Loop through Object list
            for (Object item : items) {
                
                //Map with envelop tagName as Key and value of corresponding value
                Map<String, Object> caseRecord = (Map<String, Object>)item;
                
                //populate map of WorkOrder Number with Whole Item 
                mapOrdersWithNumber.put(String.valueOf(caseRecord.get(Constants.CASE_FIELD_CASENUMBER)), item);
            }
            
            //Map to getting all fields on Case
            Map<String, Schema.SObjectField> mapOrderFieldAPIName = Case.getSObjectType().getDescribe().Fields.getMap();
            
            //Padmesh Soni (06/18/2014) - Support Case Record Type
            //Query record of Record Type of Case object
            List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
                                                AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
            
            //Query the records of Cases related to input response
            //Padmesh Soni (06/18/2014) - Support Case Record Type
            //New RecordTypeId filter added into query 
            List<Case> workOrders = [SELECT Id, CaseNumber, Image_Count__c, Image_Latency__c, Mobile_Data_Off__c, ImageLinks_Summary__c, 
                                        All_Images_Received__c FROM Case WHERE RecordTypeId NOT IN: recordTypes 
                                        AND CaseNumber IN: mapOrdersWithNumber.keySet()];
            
            //List to hold workOrder's to be updated
            List<Case> workOrdersToUpdate = new List<Case>();
            
            //Loop through query record of Cases
            for(Case workOrder : workOrders) {
                
                //instance of envelop with their tag and values
                Object item = mapOrdersWithNumber.get(workOrder.CaseNumber);
                
                //map of envelop with their tag and values
                Map<String, Object> caseFieldWithValue = (Map<String, Object>)item;
                
                //Loop through field's API name list
                for(String apiName : caseFieldWithValue.keySet()){
                    
                    //Check if mapOrderFieldAPIName already contains api Name of fields as Key
                    if(mapOrderFieldAPIName.containsKey(apiName)) {
                        
                        //Check if field is updateable than process logic
                        if(mapOrderFieldAPIName.get(apiName).getDescribe().isUpdateable()) {
                            
                            //check for ImageLinks Summary field
                            if(apiName == Constants.CASE_FIELD_IMAGELINKS_SUMMARY) {
                                
                                //Serialize object into JSON content and generates indented content using the pretty-print format
                                String imgSummary = JSON.serializePretty(caseFieldWithValue.get(apiName));
                                
                                //put value on field 
                                workOrder.put(apiName, imgSummary);
                            } else {
                                
                                //put value on field 
                                workOrder.put(apiName, caseFieldWithValue.get(apiName));
                            }
                        }
                    }
                }
                
                //add into Work Order(Case) records
                workOrdersToUpdate.add(workOrder);
            }
            
            //Check for size of Work Orders list
            if(workOrdersToUpdate.size() > 0) {
                
                //Allow partial update
                Database.SaveResult[] results = Database.update(workOrdersToUpdate, false);
                
                //String to hold error result of DML operation
                String errorLog = '';
                
                //Create log
                errorLog += Util.createLog(results);
                
                //Getting response of REST request
                Restresponse res = RestContext.response;
                
                //Check Error result string doesn't contains blank string
                if(!String.isBlank(errorLog)) {
                    
                    //Getting response body of Errors as blob
                    res.responseBody = Blob.valueOf('{"Errors":"' + errorLog + '"}');
                    
                    //Body of email message
                    String body = '';
                    body += 'Hi '+ UserInfo.getFirstName() + ', <BR/>'
                            +'In updation of WorkOrders some errors occurred which are '+ errorLog;
                    
                    //Call email handler's sendEmail method to sending mail     
                    EmailHandler.sendEmail(UserInfo.getUserEmail(), 'Error into updation of Work Orders from RESTUpdateImageLinksSummary', body, body);
                } else {
                    
                    string response = '{"Errors":""}';
                
                    //Getting response body of Errors as blob  
                    res.responseBody = Blob.valueOf(response);
                    res.statusCode = 200;
                }
            }
        }
    }
    
    global static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
    
}