public with sharing class InvoiceLineWrapper {
	public string id{get;set;}
	public string productName{get;set;}
	public string productId{get;set;}
	public string productDescription{get;set;}
	public string description{get;set;}
	public decimal qty{get;set;}
	public decimal unitPrice{get;set;}
	public decimal originalPrice{get;set;}
	public String productCategory{get;set;}
	public decimal discount{get;set;}
	public string invoiceId;
	public string recordType;
	public string uom;
	public string pbeId;
	public string cpId;

	public InvoiceLineWrapper(Invoice_Line__c i){
		  id = i.Id;
		  invoiceId = i.Invoice__c;
		  productId = i.Product__c;
		  description = i.public_notes__c;
		  recordType = i.RecordTypeId;
		  pbeId = i.Price_Book_Entry__c;
		  cpId = i.Contact_Price__c;
		  originalPrice = i.Original_Price__c;
		  if(i.Product__r != null){
		  	productName = i.Product__r.Name;
		  	productDescription = i.Product__r.Description;
		  	productCategory = i.Product__r.Family;
		  }
		  else{
		  	productName = '';
		  	productDescription = '';
		  	productCategory = '';
		  }
		  qty = i.Quantity__c;
		  unitPrice = i.Unit_Price__c;
		  discount = i.Discount__c;
		  uom = i.UOM__c;
	}
}