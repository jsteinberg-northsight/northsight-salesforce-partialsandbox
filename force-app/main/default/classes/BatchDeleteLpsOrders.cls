global without sharing class BatchDeleteLpsOrders implements Database.Batchable<sObject>{
	//this method will create a list of LPS orders
	global Database.Querylocator start(Database.BatchableContext BC){
			return Database.getQueryLocator([Select Id From Case Where Client__c = 'LPS']);
	}
	
	//this method will be used to delete all LPS orders
	global void execute(Database.BatchableContext BC, List<Case> markedLogs){
		if(markedLogs.size() > 0){//if markedLogs > 0...
			Database.delete(markedLogs,false);//delete markedLogs
			//empty the recycle bin of the markedLogs
			Database.EmptyRecycleBinResult[] emptyRecycleBinResults = DataBase.emptyRecycleBin(markedLogs);
			for(Database.EmptyRecycleBinResult result : emptyRecycleBinResults){//for each empty bin result
				if(!result.isSuccess()){//if not a success..
					result.getErrors();//show the failure error
				}
			}
		}
	}
	
	global void finish(Database.BatchableContext BC){
		//section not needed
	}
}