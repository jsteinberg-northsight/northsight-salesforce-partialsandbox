@isTest(SeeAllData=false)
public with sharing class Batch_Move_Orders_to_PrePending_Tester {
//    private static Case caseOne;
//    private static Case caseTwo;
//    
//    //Padmesh Soni (06/19/2014) - Support Case Record Type
//    //New variable added
//    private static Case caseThree;
//    private static List<RecordType> recordTypes;
//    
//    //--------------------------------------------------------
//    //PRE-TEST SETUP
//    //create two test cases
//    private static void generateTestCases(){
//        //create a new account
//        Account newAccount = new Account(
//            Name = 'Tester Account'
//        );
//        insert newAccount;
//    
//        //create a new contact
//        Contact newContact = new Contact(
//            LastName = 'Tester',
//            AccountId = newAccount.Id,
//            Pruvan_Username__c = 'NSBobTest',
//            MailingStreet = '30 Turnberry Dr',
//            MailingCity = 'La Place',
//            MailingState = 'LA',
//            MailingPostalCode = '70068',
//            OtherStreet = '30 Turnberry Dr',
//            OtherCity = 'La Place',
//            OtherState = 'LA',
//            OtherPostalCode = '70068',
//            Other_Address_Geocode__Latitude__s = 30.00,
//            Other_Address_Geocode__Longitude__s = -90.00
//            
//        );
//        insert newContact;
//        
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account; 
//        
//        //create a new test case
//        caseOne = new Case();
//        caseOne.street_address__c = '301 Front St';
//        caseOne.state__c = 'AK';
//        caseOne.city__c = 'Nome';
//        caseOne.zip_code__c = '99762';
//        caseOne.Vendor_code__c = 'SRSRSR';
//        caseOne.Work_Order_Type__c = 'Routine';
//        caseOne.Client__c = 'NOT-SG';
//        caseOne.Loan_Type__c = 'REO';
//        caseOne.ContactId = newContact.Id;
//        caseOne.Work_Ordered__c = 'Grass Recut';
//        caseOne.Client_Name__c = account.Id;
//        
//        insert caseOne;
//        
//        //create a new test case
//        caseTwo = new Case();
//        caseTwo.street_address__c = '301 Front St';
//        caseTwo.state__c = 'AK';
//        caseTwo.city__c = 'Nome';
//        caseTwo.zip_code__c = '99762';
//        caseTwo.Vendor_code__c = 'SRSRSR';
//        caseTwo.Work_Order_Type__c = 'Routine';
//        caseTwo.Client__c = 'NOT-SG';
//        caseTwo.Loan_Type__c = 'REO';
//        caseTwo.ContactId = newContact.Id;
//        caseTwo.Work_Ordered__c = 'Initial Grass Cut';
//        caseTwo.Client_Name__c = account.Id;
//        
//        insert caseTwo;
//        
//        //Padmesh Soni (06/19/2014) - Support Case Record Type
//        //create a new test case
//        caseThree = new Case(RecordTypeId = recordTypes[0].Id);
//        caseThree.street_address__c = '301 Front St';
//        caseThree.state__c = 'AK';
//        caseThree.city__c = 'Nome';
//        caseThree.zip_code__c = '99762';
//        caseThree.Vendor_code__c = 'SRSRSR';
//        caseThree.Work_Order_Type__c = 'Routine';
//        caseThree.Client__c = 'NOT-SG';
//        caseThree.Loan_Type__c = 'REO';
//        caseThree.ContactId = newContact.Id;
//        caseThree.Work_Ordered__c = 'Initial Grass Cut';
//        caseThree.Client_Name__c = account.Id;
//        
//        insert caseThree;
//    }
//    
//    //--------------------------------------------------------
//    //TESTS
//    //Unit tests for method testing and regression testing. 
//    //--------------------------------------------------------
//        
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - set caseTwo to an almost completed state other than the pruvan status
//    //and set it so that it appears that it was updated one hour ago, then run the 
//    //Batch_Move_Orders_to_PrePending class and ensure that caseTwo was set to Pre-Pending
//    //while caseOne is still set as Not Started
//    static testMethod void testBatchClass() {
//        
//        //Padmesh Soni (06/19/2014) - Support Case Record Type
//        //Query record of Record Type of Account and Case objects
//        recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
//                            AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(1, recordTypes.size());
//        
//        generateTestCases();
//        
//        List<Case> wo = [SELECT Id, 
//                         Work_Completed__c, 
//                         Date_Serviced__c, 
//                         Survey_Received__c, 
//                         Survey_Received_Datetime__c, 
//                         All_Images_Received__c 
//                  FROM Case
//                  WHERE Id =: caseTwo.Id];
//        wo[0].Work_Completed__c = 'Yes';
//        wo[0].Date_Serviced__c = Date.today();
//        wo[0].Survey_Received__c = true;
//        wo[0].Survey_Received_Datetime__c = Datetime.now().addHours(-1);
//        wo[0].All_Images_Received__c = true;
//        //wo.Last_Status_Change__c = Datetime.now().addHours(-1);
//        
//        update wo;
//        
//        system.Test.startTest();
//            //create and run a new instance of the Batch_Move_Orders_to_PrePending class
//            Batch_Move_Orders_to_PrePending testbatch = new Batch_Move_Orders_to_PrePending();
//            database.executeBatch(testbatch, 2);
//        system.Test.stopTest();
//        
//        //Padmesh Soni (06/19/2014) - Support Case Record Type
//        //New filter criteria added for RecordTypeId
//        for(Case c : [SELECT Approval_Status__c FROM Case WHERE RecordTypeId NOT IN: recordTypes]){
//            if(c.Id == caseTwo.Id){
//                c.Approval_Status__c = 'Pre-Pending';
//            } else {
//                c.Approval_Status__c = 'Not Started';
//            }
//        }
//    }
//    
//    /*/--------------------------------------------------------
//    //TEST:
//    //Scenario - set caseTwo to an almost completed state other than the pruvan status
//    //and set it so that it appears that it was updated one hour ago but also have the status 
//    //change happen after the survey is received, then run the Batch_Move_Orders_to_PrePending 
//    //class and ensure that caseTwo and caseOne are still set as Not Started
//    static testMethod void testBatchClass2() {
//        generateTestCases();
//        
//        Case wo = [SELECT Id, 
//                         Work_Completed__c, 
//                         Date_Serviced__c, 
//                         Survey_Received__c, 
//                         Survey_Received_Datetime__c, 
//                         All_Images_Received__c,
//                         Approval_Status__c
//                  FROM Case
//                  WHERE Id =: caseTwo.Id];
//        wo.Work_Completed__c = 'Yes';
//        wo.Date_Serviced__c = Date.today();
//        wo.Survey_Received__c = true;
//        wo.Survey_Received_Datetime__c = Datetime.now().addHours(-1);
//        wo.All_Images_Received__c = true;
//        
//        update wo;
//        
//        wo.Approval_Status__c = 'Rejected';
//        
//        update wo;
//        
//        system.Test.startTest();
//            //create and run a new instance of the Batch_Move_Orders_to_PrePending class
//            Batch_Move_Orders_to_PrePending testbatch = new Batch_Move_Orders_to_PrePending();
//            database.executeBatch(testbatch, 200);
//        system.Test.stopTest();
//        
//        for(Case c : [SELECT Approval_Status__c FROM Case]){
//            c.Approval_Status__c = 'Not Started';
//        }
//    }*/
}