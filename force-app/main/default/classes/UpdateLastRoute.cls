public without sharing class UpdateLastRoute {

	static public void updateContacts(List<Route__c> routes, Map<Id, Route__c> routeMap) {
		Set<Id> users = new Set<Id>();
		Set<Id>contacts = new Set<Id>();
		Map<Id, Contact> userMap = new Map<Id, Contact>();
		Map<Id, Contact> contactMap;

		for (Route__c currentRoute : routes) {
			users.add(currentRoute.OwnerId);
		}

		for (User currentUser : [SELECT
		                                         ContactId
		                                     FROM
		                                         User
		                                     WHERE
		                                         Id IN: users]) {
			contacts.add(currentUser.ContactId);
		}

		contactMap = new Map<Id, Contact>([SELECT
		                                                        Last_Route_Date_Time__c,
		                                                        Last_Route__c
		                                                     FROM
		                                                         Contact
		                                                     WHERE
		                                                     Id IN: contacts]);
		for (User currentUser : [SELECT
		                                         ContactId
		                                     FROM
		                                         User
		                                     WHERE
		                                         Id IN: users]) {
			if (contactMap.containsKey(currentUser.ContactId)) {
				userMap.put(currentUser.Id, contactMap.get(currentUser.ContactId));
			}
		}
		for (Route__c currentRoute : routes) {
			if (userMap.containsKey(currentRoute.OwnerId)) {
				userMap.get(currentRoute.OwnerId).Last_Route_Date_Time__c = currentRoute.LastModifiedDate;
				userMap.get(currentRoute.OwnerId).Last_Route__c = currentRoute.Id;
			}
		}
		update userMap.values();
	}
}