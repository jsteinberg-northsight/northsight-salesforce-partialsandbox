public without sharing class PruvanDeltaCheckController {

@remoteAction 
public static object fetchResultSegment(string segment){
	pruvanPayload pp = new pruvanPayload();
	pp.NSPruvanResultSegment = segment;
	pp.timestamp = DateTime.now().getTime()/1000;
	string payload = JSON.serialize(pp);
	object jsonResult = JSON.deserializeUntyped(PruvanWorkOrderHelper.fetchPruvanWorkOrders(payload));
	return jsonResult;
} 

public class pruvanPayload{
	public string password = '8c8cd34c37d6813808baa358b18169c211cb47f3';
	public string username = 'pruvan';
	public long timestamp =  0;
    public string[] workOrders = new string[]{};
	public string NSPruvanResultSegment = '00'; 
} 
}