@isTest(seeAllData=false)
private class Test_ZillowAPIUtility {
///**
// *  Purpose         :   Test class for ZillowAPIUtility. 
// *
// *  Created By      :   Padmesh Soni    
// *
// *  Created Date    :   12/12/2013
// *
// *  Current Version :   V1.0
// *
// *  Revision Log    :   V1.0 - Created
// *                      V1.1 - Modified - New test method add to test "fetchListingFromZillow" method. - Padmesh Soni - 12/18/2013
// *
// *  Code Coverage   :   100%    
// **/
//    
//    //Method to test Fetching Listing from Zillow - Padmesh Soni - 12/18/2013
//    static testMethod void testFetchListingFromZillow() {
//        
//        //Insert Zillow Configuration Defaults values
//        Zillow_Configuration__c configDefault = new Zillow_Configuration__c();
//        configDefault.ZWS_Id__c = 'test';
//        insert configDefault;
//        
//        //Listing test record
//        Geocode_Cache__c property = new Geocode_Cache__c (Street_Address__c = '2114 Bigelow Ave', City__c = 'Seattle', State__c = 'WA',
//                                                        Zip_Code__c = '98105');
//        
//        //insert property
//        insert property;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Set Mock Callout class 
//        Test.setMock(HttpCalloutMock.class, new ZillowControllerMockClass());
//        
//        //Call Zillow API Utility method to fetch property from Zillow related to property or Property
//        ZillowResponseWrapper zillowResWrapper = ZillowAPIUtility.fetchListingFromZillow(property.Street_Address__c, property.City__c, property.State__c, property.Zip_Code__c);
//            
//        //Assert statements 
//        System.assertEquals('4680', zillowResWrapper.lotSizeSqFt);
//        System.assertEquals('47.637933', zillowResWrapper.latitude);
//        System.assertEquals('-122.347938', zillowResWrapper.longitude);
//        
//        //Test stops here
//        Test.stopTest();
//    }
}