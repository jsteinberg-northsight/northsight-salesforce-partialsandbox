@isTest(SeeAllData=true)
public class RHX_TEST_Catalog {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Catalog__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Catalog__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}