public with sharing class PropertySharingHelper {
 public static void sharePropertiesFromCases(Map<Id,Case>oldCases,List<Case>newCases){
 		//
 		Geocode_Cache__Share[] propertiesToShare = new Geocode_Cache__Share[]{}; 
 		set<String> sharingToDelete = new set<String>();
 		if(newCases==null || newCases.Size()==0){
 			System.assertNotEquals(null,oldCases,'No new cases and no new cases.  This method should not be called.');
 			System.assertNotEquals(0,oldCases.Size(),'No new cases and no new cases.  This method should not be called.');
 			for(Case c:oldCases.Values()){
 				sharingToDelete.add((string)c.Id+(string)c.OwnerId);
 			}
 			
 		}else{
 		for(Case c:newCases){
 			Case oldCase = oldCases.get(c.Id);

 		    if(c.Geocode_Cache__c==null){
				//Case Property  reference is being deleted.  Need to clear out old and and new sharing to be sure it's all clear.  
 				if(oldCase!=null&&oldCase.Geocode_Cache__c!=null){
					sharingToDelete.add((string)c.Id+(string)c.OwnerId);//delete sharing for current owner
 					sharingToDelete.add((string)oldCase.Id+(string)oldCase.OwnerId);//delete sharing for old owner
 				}
 			}else{
	 			//Property reference isn't being deleted. set up share record. 
	 			Geocode_Cache__Share share = new Geocode_Cache__Share();
	 			share.AccessLevel = 'Edit';
	 			share.ParentId = c.Geocode_Cache__c;
				share.UserOrGroupId = c.OwnerId;
				share.RowCause = (string)c.Id+(string)c.OwnerId;
				propertiesToShare.add(share);
	 			if(oldCase!=null){
	 				if((oldCase.OwnerId != c.OwnerId)||(oldCase.Geocode_Cache__c != c.Geocode_Cache__c)){
	 					sharingToDelete.add((string)oldCase.Id+(string)oldCase.OwnerId);
	 				}
	 			}
 		    }
 		}
 		}
 		//Delete 
 		if(sharingToDelete.size()>0){
 			try{
 				delete[select id from Geocode_Cache__Share where RowCause in:sharingToDelete];
 			}catch(Exception e){
 				//couldn't delete. 
 			}
 		}
 		//Add
 		if(propertiesToShare.size()>0){
 			insert propertiesToShare;
 		}
 }
}