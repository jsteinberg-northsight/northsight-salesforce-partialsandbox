@isTest(seeAllData=false)
private class PortalUpdater2ControllerTest {
//  private static @isTest void fetchOrders_Test() {
//    Case testCase = new Case(
//      Vendor_Code__c = 'TEST2',
//      Street_Address__c = '200 Street',
//      Date_Serviced__c = Date.today(),
//      Special_Review_Checkbox__c = false,
//      Work_Completed__c = 'Yes',
//      Approval_Status__c = 'In Process',
//      OwnerId = UserInfo.getUserId(),
//      In_Process_By__c = UserInfo.getUserId());
//    insert testCase;
//    List<Case> cases = PortalUpdater2ControllerHelper.fetchWorkOrderInprocess(UserInfo.getUserId());
//    system.assertEquals(1, cases.size(), 'Number of work orders in process');
//  }
//  
//  private static @isTest void processVendorCodeTest() {
//    Portal_Update_Setup__c setup1 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST1', Client__c = 'SG');
//    Portal_Update_Setup__c setup2 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST2', Client__c = 'CL');
//    List<Portal_Update_Setup__c> setupRecords = new List<Portal_Update_Setup__c>{ setup1, setup2 };
//    insert setupRecords;
//    
//    setupRecords = PortalUpdater2ControllerHelper.fetchVendorCodes();
//    system.assertEquals(2, setupRecords.size(), 'Number of vendor codes');
//  }
//  
//  private static @isTest void fetchHighestPriorityCode_Test() {
//    Case testCase = new Case(
//      Vendor_Code__c = 'TEST2',
//      Street_Address__c = '200 Street',
//      Date_Serviced__c = Date.today(),
//      Special_Review_Checkbox__c = false,
//      Work_Completed__c = 'Yes',
//      Approval_Status__c = 'Pending',
//      OwnerId = UserInfo.getUserId(),
//      In_Process_By__c = UserInfo.getUserId());
//    insert testCase;
//    
//    String vendorCode = PortalUpdater2ControllerHelper.fetchHighestPriorityVendorCode(new Set<String> { 'TEST1', 'TEST2' });
//    system.assertEquals('TEST2', vendorCode, 'Hightest priority vendor code');
//  }
//  
//  private static @isTest void fetchHighestPriorityOrders_Test() {
//    Case testCase = new Case(
//      Vendor_Code__c = 'TEST2',
//      Street_Address__c = '200 Street',
//      Date_Serviced__c = Date.today(),
//      Special_Review_Checkbox__c = false,
//      Work_Completed__c = 'Yes',
//      Approval_Status__c = 'Pending',
//      OwnerId = UserInfo.getUserId(),
//      In_Process_By__c = UserInfo.getUserId());
//    insert testCase;
//    
//    //Code modified - add "0" as second parameter on method calling - Padmesh Soni(02/13/2014) 
//    //As per Padmesh Assignment  02/12/2014 "Northsight Updater Portal Bug"
//    List<Case> testCases = PortalUpdater2ControllerHelper.fetchHighestPriorityWorkOrdersByVendorCode('TEST2', 0);
//    system.assertEquals(1, testCases.size(), 'Number of work orders by vendor code');
//  }
//  
//  private static @isTest void updateWorkOrders_Test() {
//  	List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//	insert account; 
//    Case testCase = new Case(
//      Vendor_Code__c = 'TEST2',
//      Street_Address__c = '200 Street',
//      Date_Serviced__c = Date.today(),
//      Special_Review_Checkbox__c = false,
//      Work_Completed__c = 'Yes',
//      Approval_Status__c = 'Pending',
//      OwnerId = UserInfo.getUserId(),
//      In_Process_By__c = UserInfo.getUserId(),Client_Name__c = account.Id);
//    insert testCase;
//    
//    List<Case> testCases = PortalUpdater2ControllerHelper.setWorkOrdersToInProcess(new Set<Id> { testCase.Id });
//    List<Case> updatedCases = [SELECT Id FROM Case WHERE Approval_Status__c = : PortalUpdater2ControllerHelper.IN_PROCESS];
//    system.assertEquals(updatedCases.size(), testCases.size(), 'Matching number of cases updated');
//  }
//  
//  private static @isTest void initializeNoCodes_Test() {
//    PortalUpdater2Controller controller = new PortalUpdater2Controller();
//    system.assertEquals(null, controller.inProcessOrders, 'No Orders Available');
//    system.assertEquals(PortalUpdater2Controller.NO_ORDERS_SELECTED, controller.errorText, 'No Orders Available');
//  }
//  
//  private static @isTest void refreshNoOrders_Test() {
//    PortalUpdater2Controller controller = new PortalUpdater2Controller();
//    controller.refreshWorkOrderList();
//    //system.assertEquals('(3)'+PortalUpdater2Controller.NO_ORDERS_SELECTED, controller.errorText, 'No Orders Selected');
//  }
//  
//  private static @isTest void refreshWithOrders_Test() {
//  	List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//	insert account; 
//    Case testCase = new Case(
//      Vendor_Code__c = 'TEST2',
//      Street_Address__c = '200 Street',
//      Date_Serviced__c = Date.today(),
//      Special_Review_Checkbox__c = false,
//      Work_Completed__c = 'Yes',
//      Approval_Status__c = 'In Process',
//      OwnerId = UserInfo.getUserId(),
//      In_Process_By__c = UserInfo.getUserId(),Client_Name__c = account.Id);
//    insert testCase;
//    
//    PortalUpdater2Controller controller = new PortalUpdater2Controller();
//    controller.refreshWorkOrderList();
//    system.assertEquals(1, controller.inProcessOrders.size(), 'Order count');
//  }
//  
//  private static @isTest void assignWorkOrdersNoCodes_Test() {
//    Case testCase = new Case(
//      Vendor_Code__c = 'TEST2',
//      Street_Address__c = '200 Street',
//      Date_Serviced__c = Date.today(),
//      Special_Review_Checkbox__c = false,
//      Work_Completed__c = 'Yes',
//      Approval_Status__c = 'Pending',
//      OwnerId = UserInfo.getUserId(),
//      In_Process_By__c = UserInfo.getUserId());
//    insert testCase;
//    
//    PortalUpdater2Controller controller = new PortalUpdater2Controller();
//    controller.assignHighestPriorityWorkOrders();
//    system.assertEquals(PortalUpdater2Controller.NO_VENDOR_CODES, controller.errorText, 'No Vendor Codes');
//  }
//  
//  private static @isTest void assignWorkOrdersNoOrders_Test() {
//    Portal_Update_Setup__c setup1 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST1', Client__c = 'SG');
//    Portal_Update_Setup__c setup2 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST2', Client__c = 'CL');
//    List<Portal_Update_Setup__c> setupRecords = new List<Portal_Update_Setup__c>{ setup1, setup2 };
//    insert setupRecords;
//    
//    PortalUpdater2Controller controller = new PortalUpdater2Controller();
//    controller.assignHighestPriorityWorkOrders();
//    system.assertEquals(PortalUpdater2Controller.NO_VENDOR_CODES, controller.errorText, 'No Orders');
//  }
//  
//  private static @isTest void assignWorkOrders_Test() {
//  	List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//    Case testCase = new Case(
//      Vendor_Code__c = 'TEST2',
//      Street_Address__c = '200 Street',
//      Date_Serviced__c = Date.today(),
//      Special_Review_Checkbox__c = false,
//      Work_Completed__c = 'Yes',
//      Approval_Status__c = 'Pending',
//      OwnerId = UserInfo.getUserId(),
//      In_Process_By__c = UserInfo.getUserId(),Client_Name__c = account.Id);
//    insert testCase;
//    Portal_Update_Setup__c setup1 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST1', Client__c = 'SG');
//    Portal_Update_Setup__c setup2 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST2', Client__c = 'CL');
//    List<Portal_Update_Setup__c> setupRecords = new List<Portal_Update_Setup__c>{ setup1, setup2 };
//    insert setupRecords;
//    
//    PortalUpdater2Controller controller = new PortalUpdater2Controller();
//    controller.assignHighestPriorityWorkOrders();
//    system.assertEquals(1, controller.inProcessOrders.size(), 'Order count');
//  }
//  
//  //Code added - New test method added - Padmesh Soni(02/13/2014) 
//  //As per Padmesh Assignment  02/12/2014 "Northsight Updater Portal Bug"
//  static testMethod void testUpdaterPortalBug() {
//    
//    //Padmesh Soni (06/23/2014) - Support Case Record Type
//    //Query record of Record Type of Account and Case objects
//      List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
//                        AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true ORDER BY DeveloperName];
//      
//      //Assert statement
//      System.assertEquals(1, recordTypes.size());
//      
//    //create a new account
//    Account newAccount = new Account(Name = 'Tester Account',RecordTypeId = '01240000000Ubta');
//    insert newAccount;
//    
//    //create a new contact
//    Contact newContact = new Contact(LastName = 'Tester', AccountId = newAccount.Id, Pruvan_Username__c = 'NSBobTest', RecordTypeId = '01240000000UbtfAAC');
//    insert newContact;
//    
//    //create a new user with the Customer Portal Manager Standard profile
//    Profile newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Processors'];
//    
//    //create a new user with the profile
//    User newUser = new User(alias = 'standt', contactId = newContact.Id, email = 'standarduser@testorg.com', emailencodingkey = 'UTF-8',
//                  lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//                  timezonesidkey = 'America/Los_Angeles', username = 'standarduser@northsight.test', isActive = true, User_Initials__c = 'Mr.' );
//    insert newUser;
//    
//    //List to hold test records of Work Order(Case)
//    List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//    List<Case> cases = new List<Case>();
//    cases.add(new Case(Vendor_Code__c = 'TEST2', Street_Address__c = '200 Street', Date_Serviced__c = Date.today(), 
//                Special_Review_Checkbox__c = false, Work_Completed__c = 'Yes', Approval_Status__c = 'Pending',
//                OwnerId = UserInfo.getUserId(), In_Process_By__c = UserInfo.getUserId(),Client_Name__c = account.Id));
//    cases.add(new Case(Vendor_Code__c = 'TEST2', Street_Address__c = '300 Street', Date_Serviced__c = Date.today(), 
//                Special_Review_Checkbox__c = false, Work_Completed__c = 'Yes', Approval_Status__c = 'Pending',
//                OwnerId = UserInfo.getUserId(), In_Process_By__c = UserInfo.getUserId(),Client_Name__c = account.Id));
//    cases.add(new Case(Vendor_Code__c = 'TEST2', Street_Address__c = '230 Street', Date_Serviced__c = Date.today(), 
//                Special_Review_Checkbox__c = false, Work_Completed__c = 'Yes', Approval_Status__c = 'Pending',
//                OwnerId = UserInfo.getUserId(), In_Process_By__c = UserInfo.getUserId(),Client_Name__c = account.Id));
//    
//    //Code modified - Padmesh Soni (06/23/2014) - Support Case Record Type
//      //New record added of "Support" record type
//      cases.add(new Case(RecordTypeId = recordTypes[0].Id, Vendor_Code__c = 'TEST2', Street_Address__c = '240 Street', Date_Serviced__c = Date.today(), 
//                Special_Review_Checkbox__c = false, Work_Completed__c = 'Yes', Approval_Status__c = 'Pending',
//                OwnerId = UserInfo.getUserId(), In_Process_By__c = UserInfo.getUserId()));
//    
//    //insert cases here
//    insert cases;
//    
//    //Portal Update Setup testing records
//    Portal_Update_Setup__c setup1 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST1', Client__c = 'SG');
//    Portal_Update_Setup__c setup2 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST2', Client__c = 'CL');
//    
//    //insert Portal Update Setups
//    List<Portal_Update_Setup__c> setupRecords = new List<Portal_Update_Setup__c>{ setup1, setup2 };
//    insert setupRecords;
//    
//    System.runAs(newUser) {
//    PageReference p = Page.PortalUpdater2;
//    Test.setCurrentPage(p);
//      //instance of PortalUpdater2Controller
//      PortalUpdater2Controller controller = new PortalUpdater2Controller();
//      
//      //assert statement
//      system.assertEquals(null, controller.inProcessOrders, 'Order count');
//      
//      //call controller methods
//      controller.assignHighestPriorityWorkOrders();
//      system.assertEquals(0, ApexPages.getMessages().size(), ApexPages.getMessages().size()>0?ApexPages.getMessages()[0].getSummary():'NONE');
//      //assert statement
//      //system.assertEquals(3, controller.inProcessOrders.size(), 'Order count');
//    }
//    
//    //Loop through query records and assert statements
//    //Code modified - Padmesh Soni (06/23/2014) - Support Case Record Type
//      //New filter criteria added for RecordTypeId
//      for(Case workOrder : [SELECT In_Process_By__c, Approval_Status__c FROM Case WHERE RecordTypeId NOT IN: recordTypes AND Id IN: cases]) {
//      
//      //assert statements
//      //System.assertEquals(newUser.Id, workOrder.In_Process_By__c);  
//      //System.assertEquals('In Process', workOrder.Approval_Status__c);
//    }
//  }
//  
//  //Code added - New test method added - Padmesh Soni(08/15/2014) - NS Communities Migration
//  static testMethod void testUpdaterWithPartnerCommunity() {
//    
//	//Query record of Record Type of Account and Case objects
//    List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
//                        AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true ORDER BY DeveloperName];
//      
//	//Assert statement
//	System.assertEquals(1, recordTypes.size());
//      
//    //create a new account
//    Account newAccount = new Account(Name = 'Tester Account',RecordTypeId = '01240000000Ubta');
//    insert newAccount;
//    
//    //create a new contact
//    Contact newContact = new Contact(LastName = 'Tester', AccountId = newAccount.Id, Pruvan_Username__c = 'NSBobTest', RecordTypeId = '01240000000UbtfAAC');
//    insert newContact;
//    
//    //create a new user with the Customer Portal Manager Standard profile
//    Profile newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Processors'];
//    
//    //create a new user with the profile
//    User newUser = new User(alias = 'standt', contactId = newContact.Id, email = 'standarduser@testorg.com', emailencodingkey = 'UTF-8',
//                  lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//                  timezonesidkey = 'America/Los_Angeles', username = 'standarduser@'+math.random()+'test.com', isActive = true, User_Initials__c = 'Mr.' );
//    insert newUser;
//    
//    //List to hold test records of Work Order(Case)
//    List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//	insert account; 
//    List<Case> cases = new List<Case>();
//    cases.add(new Case(Vendor_Code__c = 'TEST2', Street_Address__c = '200 Street', Date_Serviced__c = Date.today(), 
//                Special_Review_Checkbox__c = false, Work_Completed__c = 'Yes', Approval_Status__c = 'Pending',
//                OwnerId = UserInfo.getUserId(), In_Process_By__c = UserInfo.getUserId(),Client_Name__c = account.Id));
//    cases.add(new Case(Vendor_Code__c = 'TEST2', Street_Address__c = '300 Street', Date_Serviced__c = Date.today(), 
//                Special_Review_Checkbox__c = false, Work_Completed__c = 'Yes', Approval_Status__c = 'Pending',
//                OwnerId = UserInfo.getUserId(), In_Process_By__c = UserInfo.getUserId(),Client_Name__c = account.Id));
//    cases.add(new Case(Vendor_Code__c = 'TEST2', Street_Address__c = '230 Street', Date_Serviced__c = Date.today(), 
//                Special_Review_Checkbox__c = false, Work_Completed__c = 'Yes', Approval_Status__c = 'Pending',
//                OwnerId = UserInfo.getUserId(), In_Process_By__c = UserInfo.getUserId(),Client_Name__c = account.Id));
//    
//    //Code modified - Padmesh Soni (06/23/2014) - Support Case Record Type
//	//New record added of "Support" record type
//	cases.add(new Case(RecordTypeId = recordTypes[0].Id, Vendor_Code__c = 'TEST2', Street_Address__c = '240 Street', Date_Serviced__c = Date.today(), 
//							Special_Review_Checkbox__c = false, Work_Completed__c = 'Yes', Approval_Status__c = 'Pending',
//							OwnerId = UserInfo.getUserId(), In_Process_By__c = UserInfo.getUserId()));
//    
//    //insert cases here
//    insert cases;
//    
//    //Portal Update Setup testing records
//    Portal_Update_Setup__c setup1 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST1', Client__c = 'SG');
//    Portal_Update_Setup__c setup2 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST2', Client__c = 'CL');
//    
//    //insert Portal Update Setups
//    List<Portal_Update_Setup__c> setupRecords = new List<Portal_Update_Setup__c>{ setup1, setup2 };
//    insert setupRecords;
//    
//  system.assertNotEquals(0,[SELECT CaseNumber, Reference_Number__c, Row_Highlight__c, Work_Ordered__c FROM Case
//								WHERE RecordTypeId NOT IN: PortalUpdater2ControllerHelper.recordTypes].size());
//  //system.assertNotEquals(0,[SELECT CaseNumber, Reference_Number__c, Row_Highlight__c, Work_Ordered__c FROM Case
//	//							WHERE Approval_Status__c = :PortalUpdater2ControllerHelper.IN_PROCESS].size());
//  system.assertNotEquals(0,[SELECT CaseNumber, Reference_Number__c, Row_Highlight__c, Work_Ordered__c FROM Case
//								WHERE Date_Serviced1__c <= TODAY].size());
//  system.assertNotEquals(0,[SELECT CaseNumber, Reference_Number__c, Row_Highlight__c, Work_Ordered__c FROM Case
//								WHERE Special_Review_Checkbox__c = false].size());
//  system.assertNotEquals(0,[SELECT CaseNumber, Reference_Number__c, Row_Highlight__c, Work_Ordered__c FROM Case
//								WHERE Ready_For_Update__c != 'No'].size());
//  system.assertNotEquals(0,[SELECT CaseNumber, Reference_Number__c, Row_Highlight__c, Work_Ordered__c FROM Case
//								WHERE Vendor_Code__c = 'TEST2'].size());
//    System.runAs(newUser) {
//    
//      //instance of PortalUpdater2Controller
//      PortalUpdater2Controller controller = new PortalUpdater2Controller();
//      
//      //assert statement
//      system.assertEquals(null, controller.inProcessOrders, 'Order count');
//      
//      //call controller methods
//      
//      system.assertNotEquals(null,PortalUpdater2ControllerHelper.recordTypes);
//      system.assertNotEquals(0,PortalUpdater2ControllerHelper.recordTypes.size());
//      controller.assignHighestPriorityWorkOrders();
//      
//									
//      //assert statement
//      
//      system.assertEquals(0, ApexPages.getMessages().size(), ApexPages.getMessages().size()>0?ApexPages.getMessages()[0].getSummary():'NONE');
//      //system.assertEquals(3, controller.inProcessOrders.size(), 'Order count');
//      
//      //call controller methods
//      controller.refreshWorkOrderList();
//    }
//    
//    //Loop through query records and assert statements
//    //Code modified - Padmesh Soni (06/23/2014) - Support Case Record Type
//    //New filter criteria added for RecordTypeId
//    for(Case workOrder : [SELECT In_Process_By__c, Approval_Status__c FROM Case WHERE RecordTypeId NOT IN: recordTypes AND Id IN: cases]) {
//      
//    	//assert statements
//    	//System.assertEquals(newUser.Id, workOrder.In_Process_By__c);  
//    	//System.assertEquals('In Process', workOrder.Approval_Status__c);
//    }
//  }
}