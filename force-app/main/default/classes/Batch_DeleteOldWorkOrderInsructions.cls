/**
 *  Purpose         :   This Batch is used for delete ImageLinks__c  records that meet the following criteria.
 *							1. ImageLinks have been summarized
 *							2. Work Order is closed 
 *							3. ImageLinks record is older than 90 days. 
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   06/10/2014
 *
 *	Current Version	:	V1.0
 *
 *	Revision Log	:	V1.0 - Created - Padmesh Soni(06/10/2014) - Tasks 06/09/2014
 **/
global class Batch_DeleteOldWorkOrderInsructions implements Database.Batchable<SObject>, Database.Stateful {
	
	//String to hold errorLog on DML operations
	global String errorLog = '';
	global boolean runagain = false;
	//Start method
    global Database.Querylocator start(Database.BatchableContext BC) {
    	
    	//Date of 90 days ago from today 
    	DateTime dtm = DateTime.now().addDays(-60);
    	
    	//Check if test is running
    	if(Test.isRunningTest()) {
    		
    		//return query result of VWMSurvey records
	        return Database.getQueryLocator([select Id from  Work_Order_Instructions__c where CreatedDate<Last_N_Days:60 limit 1000]);
    	} else {
	    
	    	//return query result of VWMSurvey records
	        return Database.getQueryLocator([select Id from  Work_Order_Instructions__c where CreatedDate<Last_N_Days:60 and CreatedById = '00540000001dyX7' limit 1000]);
    	}
    }
    
    //Execute method
    global void execute(Database.BatchableContext BC, List<Work_Order_Instructions__c> woIns) {
    	runagain = false;
    	//Check for size of List
    	if(woIns.size() > 0){
    		runagain = true;
    		//Delete the records into list
			Database.delete(woIns, false);
			
			//empty the recycle bin of the markedLogs
			Database.EmptyRecycleBinResult[] emptyRecycleBinResults = DataBase.emptyRecycleBin(woIns);
			
			//Loop through empty bin result
			for(Database.EmptyRecycleBinResult result : emptyRecycleBinResults){
				
				//Check for success and create log
				if(!result.isSuccess())
					errorLog += result.getErrors();
			}
		}
    }
    
    // Finish method
    global void finish(Database.BatchableContext BC) {
		
		////Query result of Asynchronous jobs
    	//AsyncApexJob a = [SELECT Id, JobItemsProcessed, TotalJobItems, NumberOfErrors, CreatedBy.Email FROM AsyncApexJob
        //                    WHERE id = :BC.getJobId()];
//
  //      // Send email indicating the completion of this batch
   //     String emailMessage = 'Your batch job \"Batch_DeleteOldImageLinkFromCase\" has finished.  It processed job items'
    //                            + a.totalJobItems +
     //                           ' batches.  Of which, '
      //                          + a.JobItemsProcessed
      //                          + ' processed without any exceptions thrown and '
      //                          + a.numberOfErrors
      //                          + ' batches threw unhandled exceptions.' + '<BR/>' + errorLog;
//		
		//Send batch status to the person who executed this batch
        //EmailHandler.sendEmail(a.CreatedBy.Email, 'Batch_DeleteOldImageLinkFromCase has been completed', emailMessage, emailMessage);
        if(runagain){
        	database.executeBatch(new Batch_DeleteOldWorkOrderInsructions(),100);
        }
    }
    
}