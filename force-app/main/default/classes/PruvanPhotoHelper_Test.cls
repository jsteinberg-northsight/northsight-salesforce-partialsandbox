@isTest()
/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/
*/
public without sharing class PruvanPhotoHelper_Test {
//	private static Case newCase;
//	private static string photoHelperPayload;
//	private static string surveyHelperPayload;
//	
//	private static void generateTestCaseWithoutContact() {
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//   		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		//create a new test case
//		newCase = new Case();
//		newCase.street_address__c = '1234 Street';
//        newCase.state__c = 'TX';
//        newCase.city__c = 'Austin';
//        newCase.zip_code__c = '78704';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        newCase.Client__c = 'NOT-SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.Client_Name__c = account.Id;
//        insert newCase;
//	}
//	
//	private static void generateTestCaseWithContact() {
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//   		//create a new account
//		Account newAccount = new Account(
//			Name = 'Tester Account'
//		);
//		insert newAccount;
//	
//		//create a new contact
//		Contact newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest'
//		);
//					newContact.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//	newContact.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//	newContact.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//	newContact.Shrub_Trim__c = 3;//Pricing...required 
//	newContact.REO_Grass_Cut__c = 3;//Pricing
//	newContact.Pay_Terms__c = 'Net 30';//Terms required
//	newContact.Trip_Charge__c = 25;//Pricing
//	newContact.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//	newContact.Date_Active__c = Date.Today();//Date Active must be set. 
//	newContact.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//	newContact.MailingStreet = '30 Turnberry Dr';
//	newContact.MailingCity = 'La Place';
//	newContact.MailingState = 'LA';
//	newContact.MailingPostalCode = '70068';
//	newContact.OtherStreet = '30 Turnberry Dr';
//	newContact.OtherCity = 'La Place';
//	newContact.OtherState = 'LA';
//	newContact.OtherPostalCode = '70068';
//	newContact.Other_Address_Geocode__Latitude__s = 30.00;
//	newContact.Other_Address_Geocode__Longitude__s = -90.00;
//		insert newContact;
//		
//		//create a new test case
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//		newCase = new Case();
//		newCase.street_address__c = '1234 Street';
//        newCase.state__c = 'TX';
//        newCase.city__c = 'Austin';
//        newCase.zip_code__c = '78704';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        newCase.Client__c = 'NOT-SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'Grass Recut';
//        newCase.Client_Name__c = account.Id;
//        insert newCase;
//	}
//	
//	private static void setPayloads(){
//		for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//			photoHelperPayload = 	'{' +
//								    '"timestamp": 1377559122,' +
//								    '"serviceId": "32981757",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "before",' +
//								    '"fileType": "picture",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "d9aab2a9-eec6-470e-bf13-0cb74ca47e75",' +
//								    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//								    '"fileName": "1382536931021.jpg",' +
//								    '"gpsLatitude": "31.62020098900615",' +
//								    '"gpsLongitude": "-97.099915453764",' +
//								    '"gpsTimestamp": "1382536931",' +
//								    '"gpsAccuracy": "10.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706368.jpg",' +
//								    '"notes": "These are photo notes"' +
//									'}';
//									
//			surveyHelperPayload = 	'{' +
//								    '"timestamp": 1377559123,' +
//								    '"serviceId": "32786734",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "Survey",' +
//								    '"fileType": "survey",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//								    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ef",' +
//								    '"fileName": "survey_1382537334014.json",' +
//								    '"gpsLatitude": "42.01235463",' +
//								    '"gpsLongitude": "-87.89172686",' +
//								    '"gpsTimestamp": "1382537316",' +
//								    '"gpsAccuracy": "10.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"uploaderVersion": "mobile 3.9.1",' +
//								    '"deviceId": "ios-1B3F968D-5F14-47F0-95EC-89A15EFBA580",' +
//								    '"sdkVersion": "7.0.4",' +
//								    '"survey": {' +
//								        '"answers": [{' +
//								            '"answer": ["yes"],' +
//								            '"id": "Work_Completed__c",' +
//								            '"picIds": ["d9aab2a9-eec6-470e-bf13-0cb74ca47e75"],' +
//								            '"hint": "Select No for Trip Charge"' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Backyard_Serviced__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Excessive_Growth__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Shrubs_Trimmed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Debris_Removed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["Excessive Grass/Weed Height"],' +
//								            '"id": "Bid_Type__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["This is the bid description."],' +
//								            '"id": "Bid_Description__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["100"],' +
//								            '"id": "Bid_Cost__c",' +
//								            '"hint": ""' +
//								        '}],' +
//								        '"meta": {' +
//								            '"currentQuestionIndex": 18,' +
//								            '"surveyId": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//								            '"parentId": "37064308",' +
//								            '"surveyTemplateId": "sarcj07::GC_001-v4"' +
//								        '}' +
//								    '},' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706991.json"' +
//									'}';
//		}
//	}
//	
//	private static void setCsrPayload(){
//		for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//			photoHelperPayload = 	'{' +
//								    '"timestamp": 1377559122,' +
//								    '"serviceId": "32981757",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "before",' +
//								    '"fileType": "csr",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "d9aab2a9-eec6-470e-bf13-0cb74ca47e75",' +
//								    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//								    '"fileName": "1382536931021.jpg",' +
//								    '"gpsLatitude": "31.62020098900615",' +
//								    '"gpsLongitude": "-97.099915453764",' +
//								    '"gpsTimestamp": "1382536931",' +
//								    '"gpsAccuracy": "10.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706368.jpg"' +
//									'}';
//		}	
//	}
//	
//	private static void setInvalidPhotoPayloadNeg(){
//		for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//			photoHelperPayload = 	'{' +
//								    '"timestamp": 1377559122,' +
//								    '"serviceId": "32981757",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "before",' +
//								    '"fileType": "picture",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "d9aab2a9-eec6-470e-bf13-0cb74ca47e75",' +
//								    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//								    '"fileName": "1382536931021.jpg",' +
//								    '"gpsLatitude": "-91",' +
//								    '"gpsLongitude": "-181",' +
//								    '"gpsTimestamp": "1382536931",' +
//								    '"gpsAccuracy": "10.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706368.jpg"' +
//									'}';
//		}	
//	}
//	
//	private static void setInvalidPhotoPayloadPos(){
//		for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//			photoHelperPayload = 	'{' +
//								    '"timestamp": 1377559122,' +
//								    '"serviceId": "32981757",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "before",' +
//								    '"fileType": "picture",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "d9aab2a9-eec6-470e-bf13-0cb74ca47e75",' +
//								    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//								    '"fileName": "1382536931021.jpg",' +
//								    '"gpsLatitude": "91",' +
//								    '"gpsLongitude": "181",' +
//								    '"gpsTimestamp": "1382536931",' +
//								    '"gpsAccuracy": "10.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706368.jpg"' +
//									'}';
//		}	
//	}
//	
//	//--------------------------------------------------------
//   	//TESTS
//   	//Unit tests for method testing and regression testing. 
//   	//--------------------------------------------------------
//   		
//   	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - send a photo payload to the PruvanPhotoHelper
//   	//and assert that an image link with all the correct field data is generated
//	public static testmethod void photoTest(){
//       system.Test.startTest();
//       
//       generateTestCaseWithContact();
//       setPayloads();
//        
//        //assert that uploadPictures returns the following string with payload
//        //system.assertEquals('{"status":true,"error":null}', PruvanPhotoHelper.uploadPictures(photoHelperPayload));
//        
//        //format unix milliseconds into seconds and create a datetime var to check against the image link gps timestamp
//        integer secondsToAdd = 1382536931;
//		Datetime formattedTimeStamp = datetime.newinstance(0);
//		formattedTimeStamp = formattedTimeStamp.addSeconds(secondsToAdd);
//		
//		//get the case number of the work order
//		string caseNum;
//		
//		for(Case c : [select CaseNumber from Case where Id =: newCase.Id]){
//			caseNum = c.CaseNumber;
//		}
//        
//        //scale the latitude and longitude values to 15
//        Decimal latitude = 31.62020098900615;
//        Decimal longitude = -97.099915453764;
//        Decimal scaledLat = latitude.setScale(6);
//        Decimal scaledLong = longitude.setScale(6);
//        
//        //select the ImageLinks object created from the payload and assert the values we get back are equal to the strings we expect
//        for(ImageLinks__c imageLink : [SELECT Id,
//										Pruvan_uuid__c, 
//										Pruvan_parentUuid__c,
//										Pruvan_evidenceType__c,
//										Pruvan_fileType__c,
//										Original_fileName__c,
//										GPS_Location__latitude__s,
//										GPS_Location__longitude__s,
//										GPS_Timestamp__c,
//										Pruvan_gpsAccuracy__c,
//										ImageUrl__c,
//										Notes__c
//								FROM ImageLinks__c
//								WHERE CaseId__c =: newCase.Id]){
//        	
//        	system.assertEquals('d9aab2a9-eec6-470e-bf13-0cb74ca47e75', imageLink.Pruvan_uuid__c);
//        	system.assertEquals('7571f782-0ccc-484b-a708-df4a0f5176ee', imageLink.Pruvan_parentUuid__c);
//        	system.assertEquals('before', imageLink.Pruvan_evidenceType__c);
//        	system.assertEquals('picture', imageLink.Pruvan_fileType__c);
//        	system.assertEquals('1382536931021.jpg', imageLink.Original_fileName__c);
//        	system.assertEquals(scaledLat, imageLink.GPS_Location__latitude__s);
//        	system.assertEquals(scaledLong, imageLink.GPS_Location__longitude__s);
//        	system.assertEquals(formattedTimestamp, imageLink.GPS_Timestamp__c);
//        	system.assertEquals(10, imageLink.Pruvan_gpsAccuracy__c);
//        	system.assertEquals('https://allyearimages.s3.amazonaws.com/' + caseNum + '/46706368.jpg', imageLink.ImageUrl__c);
//        	system.assertEquals('These are photo notes', imageLink.Notes__c);
//        }
//        
//        system.Test.stopTest();
//	}
//	
//	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - send a photo payload to the PruvanPhotoHelper
//   	//and assert that an image link with all the correct field data is generated
//	public static testmethod void photoWithSurveyDataTest(){
//       system.Test.startTest();
//       
//       generateTestCaseWithContact();
//       setPayloads();
//        
//        //send both the photoHelperPayload and the surveyHelperPayload through the PruvanPhotoHelper
//        PruvanPhotoHelper.uploadPictures(photoHelperPayload);
//        PruvanPhotoHelper.uploadPictures(surveyHelperPayload);
//        
//        //format unix milliseconds into seconds and create a datetime var to check against the image link gps timestamp
//        integer secondsToAdd = 1382536931;
//		Datetime formattedTimeStamp = datetime.newinstance(0);
//		formattedTimeStamp = formattedTimeStamp.addSeconds(secondsToAdd);
//		
//		integer seconds = 1382537316;
//		Datetime timeStamp = datetime.newInstance(0);
//		timeStamp = timeStamp.addSeconds(seconds);
//		
//		//get the case number of the work order
//		string caseNum;
//		for(Case c : [select CaseNumber from Case where Id =: newCase.Id]){
//			caseNum = c.CaseNumber;
//		}
//        
//        //scale the latitude and longitude values to 15
//        Decimal latitude = 31.62020098900615;
//        Decimal longitude = -97.099915453764;
//        Decimal scaledLat = latitude.setScale(6);
//        Decimal scaledLong = longitude.setScale(6);
//        
//        //select the ImageLinks object created from the payload and assert the values we get back are equal to the strings we expect
//        for(ImageLinks__c imageLink : [SELECT Id,
//										Pruvan_uuid__c, 
//										Pruvan_parentUuid__c,
//										Pruvan_evidenceType__c,
//										Pruvan_fileType__c,
//										Original_fileName__c,
//										GPS_Location__latitude__s,
//										GPS_Location__longitude__s,
//										GPS_Timestamp__c,
//										Pruvan_gpsAccuracy__c,
//										ImageUrl__c,
//										Notes__c
//								FROM ImageLinks__c
//								WHERE CaseId__c =: newCase.Id]){
//        	if(imageLink.Pruvan_uuid__c == '7571f782-0ccc-484b-a708-df4a0f5176ee'){
//	        	system.assertEquals('7571f782-0ccc-484b-a708-df4a0f5176ef', imageLink.Pruvan_parentUuid__c);
//	        	system.assertEquals('survey', imageLink.Pruvan_fileType__c);
//	        	system.assertEquals('survey_1382537334014.json', imageLink.Original_fileName__c);
//	        	system.assertEquals(42.012355, imageLink.GPS_Location__latitude__s);
//	        	system.assertEquals(-87.891727, imageLink.GPS_Location__longitude__s);
//	        	system.assertEquals(timeStamp, imageLink.GPS_Timestamp__c);
//	        	system.assertEquals(10, imageLink.Pruvan_gpsAccuracy__c);
//	        	system.assertEquals('https://allyearimages.s3.amazonaws.com/' + caseNum + '/46706991.json', imageLink.ImageUrl__c);
//        	} else {
//        		system.assertEquals('d9aab2a9-eec6-470e-bf13-0cb74ca47e75', imageLink.Pruvan_uuid__c);
//	        	system.assertEquals('7571f782-0ccc-484b-a708-df4a0f5176ee', imageLink.Pruvan_parentUuid__c);
//	        	system.assertEquals('before', imageLink.Pruvan_evidenceType__c);
//	        	system.assertEquals('picture', imageLink.Pruvan_fileType__c);
//	        	system.assertEquals('1382536931021.jpg', imageLink.Original_fileName__c);
//	        	system.assertEquals(scaledLat, imageLink.GPS_Location__latitude__s);
//	        	system.assertEquals(scaledLong, imageLink.GPS_Location__longitude__s);
//	        	system.assertEquals(formattedTimestamp, imageLink.GPS_Timestamp__c);
//	        	system.assertEquals(10, imageLink.Pruvan_gpsAccuracy__c);
//	        	system.assertEquals('https://allyearimages.s3.amazonaws.com/' + caseNum + '/46706368.jpg', imageLink.ImageUrl__c);
//	        	system.assertEquals('These are photo notes', imageLink.Notes__c);
//        	}
//        }
//        
//        //select the test case survey field values, the approval status, and the mobile flag and assert they equal the values we expect
//        for(Case selectedCase : [SELECT Id,
//										Work_Completed__c, 
//										Date_Serviced__c,
//										Backyard_Serviced__c,
//										Excessive_Growth__c,
//										Shrubs_Trimmed__c,
//										Debris_Removed__c,
//										Bid_Type__c,
//										Bid_Description__c,
//										Bid_Cost__c
//								FROM Case
//								WHERE Id =: newCase.Id]){
//        	
//        	system.assertEquals('Yes', selectedCase.Work_Completed__c);
//        	system.assertEquals(Date.today(), selectedCase.Date_Serviced__c);
//        	system.assertEquals('Yes', selectedCase.Backyard_Serviced__c);
//        	system.assertEquals('No', selectedCase.Excessive_Growth__c);
//        	system.assertEquals('Yes', selectedCase.Shrubs_Trimmed__c);
//        	system.assertEquals('No', selectedCase.Debris_Removed__c);
//        	system.assertEquals('Excessive Grass/Weed Height', selectedCase.Bid_Type__c);
//        	system.assertEquals('This is the bid description.', selectedCase.Bid_Description__c);
//        	system.assertEquals(100.00, selectedCase.Bid_Cost__c);
//        }
//        
//        system.Test.stopTest();
//	}
//	
//	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - send a photo payload to the PruvanPhotoHelper
//   	//and assert that an image link with all the correct field data is generated
//	public static testmethod void csrPhotoTest(){
//       system.Test.startTest();
//       
//       generateTestCaseWithoutContact();
//       setCsrPayload();
//        
//       //assert that uploadPictures returns the following string with a csr payload
//       //system.assertEquals('{"status":true,"error":null}', PruvanPhotoHelper.uploadPictures(photoHelperPayload));
//        
//       system.Test.stopTest();
//	}
//	
//	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - create a case without a contact and assert
//   	//that none of the picture/survey data gets uploaded
//	public static testmethod void noContactTest(){
//       system.Test.startTest();
//       
//       generateTestCaseWithoutContact();
//       setPayloads();
//        
//       //select the test case survey field values, the approval status, and the mobile flag and assert they equal the values we expect
//        for(Case selectedCase : [SELECT Id,
//										Work_Completed__c, 
//										Date_Serviced__c,
//										Backyard_Serviced__c,
//										Excessive_Growth__c,
//										Shrubs_Trimmed__c,
//										Debris_Removed__c,
//										Bid_Type__c,
//										Bid_Description__c,
//										Bid_Cost__c
//								FROM Case
//								WHERE Id =: newCase.Id]){
//        	
//        	system.assertEquals('No', selectedCase.Work_Completed__c);
//        	system.assertEquals(null, selectedCase.Date_Serviced__c);
//        	system.assertEquals('Yes', selectedCase.Backyard_Serviced__c);
//        	system.assertEquals('No', selectedCase.Excessive_Growth__c);
//        	system.assertEquals('No', selectedCase.Shrubs_Trimmed__c);
//        	system.assertEquals('No', selectedCase.Debris_Removed__c);
//        	system.assertEquals(null, selectedCase.Bid_Type__c);
//        	system.assertEquals(null, selectedCase.Bid_Description__c);
//        	system.assertEquals(null, selectedCase.Bid_Cost__c);
//		}
//		
//		system.Test.stopTest();
//	}
//	
//	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - send a photo payload to the PruvanPhotoHelper
//   	//and assert that an image link with all the correct field data is generated
//	public static testmethod void invalidPhotoTestNeg(){
//       system.Test.startTest();
//       
//       generateTestCaseWithContact();
//       setInvalidPhotoPayloadNeg();
//        
//        //send the photoHelperPayload through the PruvanPhotoHelper
//        PruvanPhotoHelper.uploadPictures(photoHelperPayload);
//        
//        //format unix milliseconds into seconds and create a datetime var to check against the image link gps timestamp
//        integer secondsToAdd = 1382536931;
//		Datetime formattedTimeStamp = datetime.newinstance(0);
//		formattedTimeStamp = formattedTimeStamp.addSeconds(secondsToAdd);
//		
//		//get the case number of the work order
//		string caseNum;
//		for(Case c : [select CaseNumber from Case where Id =: newCase.Id]){
//			caseNum = c.CaseNumber;
//		}
//        
//        //select the ImageLinks object created from the payload and assert the values we get back are equal to the strings we expect
//        for(ImageLinks__c imageLink : [SELECT Id,
//										Pruvan_uuid__c, 
//										Pruvan_parentUuid__c,
//										Pruvan_evidenceType__c,
//										Pruvan_fileType__c,
//										Original_fileName__c,
//										GPS_Location__latitude__s,
//										GPS_Location__longitude__s,
//										GPS_Timestamp__c,
//										Pruvan_gpsAccuracy__c,
//										ImageUrl__c
//								FROM ImageLinks__c
//								WHERE CaseId__c =: newCase.Id]){
//        	
//        	system.assertEquals('d9aab2a9-eec6-470e-bf13-0cb74ca47e75', imageLink.Pruvan_uuid__c);
//        	system.assertEquals('7571f782-0ccc-484b-a708-df4a0f5176ee', imageLink.Pruvan_parentUuid__c);
//        	system.assertEquals('before', imageLink.Pruvan_evidenceType__c);
//        	system.assertEquals('picture', imageLink.Pruvan_fileType__c);
//        	system.assertEquals('1382536931021.jpg', imageLink.Original_fileName__c);
//        	system.assertEquals(0, imageLink.GPS_Location__latitude__s);
//        	system.assertEquals(0, imageLink.GPS_Location__longitude__s);
//        	system.assertEquals(formattedTimestamp, imageLink.GPS_Timestamp__c);
//        	system.assertEquals(10, imageLink.Pruvan_gpsAccuracy__c);
//        	system.assertEquals('https://allyearimages.s3.amazonaws.com/' + caseNum + '/46706368.jpg', imageLink.ImageUrl__c);
//        }
//	}
//	
//	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - send a photo payload to the PruvanPhotoHelper
//   	//and assert that an image link with all the correct field data is generated
//	public static testmethod void invalidPhotoTestPos(){
//       system.Test.startTest();
//       
//       generateTestCaseWithContact();
//       setInvalidPhotoPayloadPos();
//        
//        //send the photoHelperPayload through the PruvanPhotoHelper
//        PruvanPhotoHelper.uploadPictures(photoHelperPayload);
//        
//        //format unix milliseconds into seconds and create a datetime var to check against the image link gps timestamp
//        integer secondsToAdd = 1382536931;
//		Datetime formattedTimeStamp = datetime.newinstance(0);
//		formattedTimeStamp = formattedTimeStamp.addSeconds(secondsToAdd);
//		
//		//get the case number of the work order
//		string caseNum;
//		for(Case c : [select CaseNumber from Case where Id =: newCase.Id]){
//			caseNum = c.CaseNumber;
//		}
//        
//        //select the ImageLinks object created from the payload and assert the values we get back are equal to the strings we expect
//        for(ImageLinks__c imageLink : [SELECT Id,
//										Pruvan_uuid__c, 
//										Pruvan_parentUuid__c,
//										Pruvan_evidenceType__c,
//										Pruvan_fileType__c,
//										Original_fileName__c,
//										GPS_Location__latitude__s,
//										GPS_Location__longitude__s,
//										GPS_Timestamp__c,
//										Pruvan_gpsAccuracy__c,
//										ImageUrl__c
//								FROM ImageLinks__c
//								WHERE CaseId__c =: newCase.Id]){
//        	
//        	system.assertEquals('d9aab2a9-eec6-470e-bf13-0cb74ca47e75', imageLink.Pruvan_uuid__c);
//        	system.assertEquals('7571f782-0ccc-484b-a708-df4a0f5176ee', imageLink.Pruvan_parentUuid__c);
//        	system.assertEquals('before', imageLink.Pruvan_evidenceType__c);
//        	system.assertEquals('picture', imageLink.Pruvan_fileType__c);
//        	system.assertEquals('1382536931021.jpg', imageLink.Original_fileName__c);
//        	system.assertEquals(0, imageLink.GPS_Location__latitude__s);
//        	system.assertEquals(0, imageLink.GPS_Location__longitude__s);
//        	system.assertEquals(formattedTimestamp, imageLink.GPS_Timestamp__c);
//        	system.assertEquals(10, imageLink.Pruvan_gpsAccuracy__c);
//        	system.assertEquals('https://allyearimages.s3.amazonaws.com/' + caseNum + '/46706368.jpg', imageLink.ImageUrl__c);
//        }
//	}
}