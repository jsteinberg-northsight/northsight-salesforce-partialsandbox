global with sharing class MultiStatusController 
{
	static Set<string> ApprovalStatusSet;	
	static Set<string> OwnerNameSet;	
	private integer MaxRecords;
	private string printResultIdList;
	
	static
	{
		ApprovalStatusSet = new Set<string>{'Not Started','Rejected'};
		OwnerNameSet = new Set<string>{'CANCELED','REO Unassigned'};
	}
	
	class WorkOrderObject
	{
		public boolean Selectbox { get; set; }
		public Case WorkOrderObject { get; set; }
		
		public WorkOrderObject(Case aCase)
		{
			Selectbox = false;
			WorkOrderObject = aCase;
		}
	}
	
	public List<WorkOrderObject> WorkOrderList { get; set; }
	public string Mode { get; set; }
	public Status__c Status { get; set; }
	public string SelectedQuery { get; set; }
	public string HtmlDataId { get; set; }
	public string TheUserAddress { get; set; }
	public string Url { get; set; }
	
	public List<RecordType> recordTypes;
	
	//Constructor
	public MultiStatusController()
	{
		
		//Padmesh Soni (06/23/2014) - Support Case Record Type
		//Query record type of Case Object "SUPPORT"    
		recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true ORDER BY DeveloperName];
		
		//get the user's information
		User theUser = [select Street, City, State, PostalCode from User where id =: UserInfo.getUserId()];
		TheUserAddress = theUser.Street + ' ' + theUser.City + ', ' + theUser.State + ' ' + theUser.PostalCode;
		
		MaxRecords = 900;
		SelectedQuery = '1';
		Status = new Status__c();
		
		if (ApexPages.CurrentPage().getParameters().get('mode') == null)
			Mode = 'start';
		else
			Mode = ApexPages.CurrentPage().getParameters().get('mode');
			
		WorkOrderList = new List<WorkOrderObject>();
		
		if (ApexPages.CurrentPage().getParameters().get('ids') != null)
		{
			SelectedQuery = '4';   //get records by ID set
			//get id list for result print
			List<string> idList = ApexPages.CurrentPage().getParameters().get('ids').split(':');
			//system.debug(idList);
			Set<Id> idSet = new Set<Id>();

			for(string currentId : idList)
			{
				//system.debug(currentId);
				idSet.add(currentId);
			}
			
			doGetQuery(SelectedQuery, idSet);
		}
		else
		{
			doGetQuery(SelectedQuery, null);
		}
	}
	
	private void doGetQuery(string aQueryId, Set<Id> aList)
	{
		List<Case> caseList;
		
		if (Test.isRunningTest())
			
			//Padmesh Soni (06/23/2014) - Support Case Record Type
	    	//New filter criteria added for RecordTypeId
			caseList =	 [select 
								CaseNumber, 
								Scheduled_Date__c,
								Vendor_Status__c,
								Street_Address__c,
								City__c,
								State__c,
								Zip_Code__c,
								Work_Ordered__c,
								Client_Number__c,
								Notes__c,
								Status_Health__c,
								Status_Health_Text__c
							from 
								Case 
							where
								RecordTypeId NOT IN: recordTypes AND 
								RecordType.Name = 'REO' and
								Approval_Status__c IN: ApprovalStatusSet and
								Status != 'Canceled'
							order by
								Scheduled_Date__c
							limit :MaxRecords];
		else
		{
			if (aQueryId == '1')  //open
				
				//Padmesh Soni (06/23/2014) - Support Case Record Type
		    	//New filter criteria added for RecordTypeId
				caseList =  [select 
										CaseNumber, 
										Scheduled_Date__c,
										Vendor_Status__c,
										Street_Address__c,
										City__c,
										State__c,
										Zip_Code__c,
										Work_Ordered__c,
										Client_Number__c,
										Notes__c,
										Status_Health__c,
										Status_Health_Text__c
									from 
										Case 
									where 
										RecordTypeId NOT IN: recordTypes AND 
										City__c != null and
										RecordType.Name = 'REO' and
										Approval_Status__c IN: ApprovalStatusSet and
										Owner.Name NOT IN: OwnerNameSet and 
										Status != 'Canceled' and
										OwnerId =: UserInfo.getUserId()
									order by
										Scheduled_Date__c
									limit :MaxRecords];
					
			else if (aQueryId == '2') //submitted
				
				//Padmesh Soni (06/23/2014) - Support Case Record Type
		    	//New filter criteria added for RecordTypeId
				caseList = [select 
									CaseNumber, 
									Scheduled_Date__c,
									Vendor_Status__c,
									Street_Address__c,
									City__c,
									State__c,
									Zip_Code__c,
									Work_Ordered__c,
									Client_Number__c,
									Notes__c,
									Status_Health__c,
									Status_Health_Text__c
								from 
									Case 
								where 
									RecordTypeId NOT IN: recordTypes AND 
									RecordType.Name = 'REO' and
									Approval_Status__c NOT IN: ApprovalStatusSet and
									Owner.Name NOT IN: OwnerNameSet and
									Pre_Pending_Date_Time__c >= LAST_N_DAYS:30 and
									OwnerId =: UserInfo.getUserId()
								order by
									Scheduled_Date__c
								limit :MaxRecords];
			
			else if (aQueryId == '3')  //cancelled
				
				//Padmesh Soni (06/23/2014) - Support Case Record Type
		    	//New filter criteria added for RecordTypeId
				caseList = [select 
									CaseNumber, 
									Scheduled_Date__c,
									Vendor_Status__c,
									Street_Address__c,
									City__c,
									State__c,
									Zip_Code__c,
									Work_Ordered__c,
									Client_Number__c,
									Notes__c,
									Status_Health__c,
									Status_Health_Text__c
								from 
									Case 
								where 
									RecordTypeId NOT IN: recordTypes AND 
									RecordType.Name = 'REO' and
									Approval_Status__c IN: ApprovalStatusSet and
									Owner.Name NOT IN: OwnerNameSet and 
									Status = 'Canceled' and
									OwnerId =: UserInfo.getUserId()
								order by
									Scheduled_Date__c
								limit :MaxRecords];
			else if (aQueryId == '4')
				
				//Padmesh Soni (06/23/2014) - Support Case Record Type
		    	//New filter criteria added for RecordTypeId
				caseList = [select 
									CaseNumber, 
									Scheduled_Date__c,
									Vendor_Status__c,
									Street_Address__c,
									City__c,
									State__c,
									Zip_Code__c,
									Work_Ordered__c,
									Client_Number__c,
									Notes__c,
									Status_Health__c,
									Status_Health_Text__c
								from 
									Case 
								where 
									RecordTypeId NOT IN: recordTypes AND 
									Id IN: aList
								order by
									Scheduled_Date__c
								limit :MaxRecords];
			}
			
			if (caseList.size() > 0)
			{
				WorkOrderList = new List<WorkOrderObject>();
				for (Case currentCase : caseList)
				{
					WorkOrderObject newWorkOrder = new WorkOrderObject(currentCase);
					WorkOrderList.add(newWorkOrder);
				}
			}
			else
				WorkOrderList = null;	
	}
	
	public void LoadQuery()
	{
		WorkOrderList = new List<WorkOrderObject>();
		
		doGetQuery(SelectedQuery, null);
	}
	
	public pageReference Print()
	{
		//parse the URL and set retURL
        Url = Url.substring(0, Url.indexOf('/001/'));
        
        PageReference printPage;
        if (Mode == 'results')
        	printPage = new PageReference(Url + '/apex/multistatus?mode=print&ids=' + printResultIdList);
       	else
       	{
       		//if there is something selected, send only selected items
       		string selectedIds = '';
       		
       		for (integer i = 0; i <WorkOrderList.size(); i++)
       		{
       			if (WorkOrderList[i].SelectBox)
       				selectedIds = selectedIds + WorkOrderList[i].WorkOrderObject.Id + ':';
       		}
       		
       		//did we have something selected?
       		if (selectedIds.length() > 0)
       		{
       			//remove trailing :
       			selectedIds = selectedIds.substring(0, selectedIds.length() - 1);
       			printPage = new PageReference(Url + '/apex/multistatus?mode=print&ids=' + selectedIds);
       		}
       		else
       			printPage = new PageReference(Url + '/apex/multistatus?mode=print');	
       	}	
		return printPage;
	}
	
	public pageReference ShowStatus()
	{
		boolean OkToSetStatus = false;
		
		//Is anything selected?
		for (WorkOrderObject currentObject : WorkOrderList)
		{
			if (currentObject.Selectbox)
			{
				OkToSetStatus = true;
				Mode = 'status';
				break;
			}		
		}
		
		if (!OkToSetStatus)
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Nothing Selected'));
			
		return null;
	}
	
	public PageReference SaveStatus()
	{
		printResultIdList = '';
		
		try
		{
			integer updatedRecords = 0;
			integer insertedRecords = 0;
			
			Set<Id>WorkOrderIdList = new Set<Id>();
					
			//if work order is selected, add work order # to the list
			for (WorkOrderObject currentObject : WorkOrderList)
			{
				if (currentObject.Selectbox)
				{
					printResultIdList = printResultIdList + currentObject.WorkOrderObject.Id + ':';
					//add to list of selected work orders
					WorkOrderIdList.add(currentObject.WorkOrderObject.Id);  //add to list to get updated
				}	
			}
			
			//remove trailing |
			printResultIdList = printResultIdList.substring(0, printResultIdList.length() - 1);
			
			//get all statuses for selected work orders
			List<Status__c> StatusToUpdate = new List<Status__c>();
			
			/*  DONT UNCOMMENT
			if (WorkOrderIdList.size() > 0)
				StatusToUpdate = [select Expected_Completion_Date__c, Notes__c, Order_Status__c, Report_Reason__c, Case__r.Id from Status__c where Case__c IN: WorkOrderIdList];
			
			if (StatusToUpdate.size() > 0)
				updatedRecords = StatusToUpdate.size();
				
			//Loop thru all existing records
			for (Status__c currentStatus : StatusToUpdate)
			{
				currentStatus.Expected_Completion_Date__c = Status.Expected_Completion_Date__c;
				currentStatus.Notes__c = Status.Notes__c;
				currentStatus.Report_Reason__c = Status.Report_Reason__c;
				
				//remove Id from Set
				WorkOrderIdList.remove(currentStatus.Case__c);
			}
			*/
			
			//Do we need to create new status records? Check if there are remaining Ids and add new status records for them
			if (WorkOrderIdList.size() > 0)
			{
				insertedRecords = WorkOrderIdList.Size();
				
				//Loop thru ID set and add a new Status object
				for (Id currentId : WorkOrderIdList)
				{
					Status__c newStatus = new Status__c();
					newStatus.Expected_Upload_Date__c = Status.Expected_Upload_Date__c;
					newStatus.Explanation__c = Status.Explanation__c;
					newStatus.Delay_Reason__c = Status.Delay_Reason__c;
					newStatus.Order_Status__c = Status.Order_Status__c;
					newStatus.Case__c = currentId;
					
					StatusToUpdate.add(newStatus);
				}
			}
			
			upsert StatusToUpdate;
			
			//get list of selected work orders
			doGetQuery('4', WorkOrderIdList);

			Mode = 'results';
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Records Inserted: ' + string.ValueOf(insertedRecords)));
			
			//Clear status information
			Status = new Status__c();
		}
		catch(Exception E)
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, E.getMessage()));
		}
		
		return null;
	}
	
	public PageReference BackToHomePage()
	{
		return new PageReference('/home/home.jsp');
	}
	
	public PageReference Cancel()
	{
		Mode = 'start';
		
		return null;
	}
	
	@RemoteAction
    global static string SaveHtml(string aHtml)
    {
    	try
    	{
	    	//create parent record
	    	Listview_Html__c listViewObject = new Listview_Html__c();
	        insert listViewObject;
	        
	        List<Html_Batch__c> batchList = new List<Html_Batch__c>();
	        
	        integer counter = 0;
	        integer chunkSize = 30000;
	        string newChunk = '';
	        
	        //is html bigger than 30k?
	        if (aHtml.length() > chunkSize)
	        {
		        //break the html into pieces of 30k
		        while (aHtml.length() > chunkSize)
		        {
		        	//get a chunk
		        	newChunk = aHtml.substring(0, chunkSize);
		        	
		        	//remove chunk from HTML string
		        	aHtml = aHtml.substring(chunkSize, aHtml.length());
		        	
		        	//add chunk
		        	Html_Batch__c newBatch = new Html_Batch__c();
		        	newBatch.Listview_html__c = listViewObject.Id;
		        	newBatch.Html_Chunk__c = newChunk;
		        	newBatch.Order__c = counter;  //keep order so we can put it back together
		        	batchList.add(newBatch);
		        	
		        	counter++;
		        }
		        
		        //any remaining data?
		        if (aHtml.length() > 0)
		        {
		        	Html_Batch__c newBatch = new Html_Batch__c();
		        	newBatch.Listview_html__c = listViewObject.Id;
		        	newBatch.Html_Chunk__c = aHtml;
		        	newBatch.Order__c = counter;  //keep order so we can put it back together
		        	batchList.add(newBatch);
		        }        
	        }
	        else
	        {
	        	//only need one chunk
	        	Html_Batch__c newBatch = new Html_Batch__c();
	        	newBatch.Listview_html__c = listViewObject.Id;
	        	newBatch.Html_Chunk__c = aHtml;
	        	newBatch.Order__c = counter;  //keep order so we can put it back together
	        	
	        	batchList.add(newBatch);
	        }
	        
	        //insert chunks
	        insert batchList;
	        
			//return the parent Id
	        return listViewObject.Id;
    	}
    	catch(exception e)
    	{
    		return 'Error Saving Html';
    	}
    }
    
    public PageReference GoToExportPage()
    {
        PageReference exportPage = new PageReference('/apex/ExportToExcel?id=' + HtmlDataId + '&filename=WorkOrder');
        return exportPage;
    }
    
    	public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}