public with sharing class Bid_Image_Controller {

public List<ImageLinks__c> links;

public String currentWorkOrder {get;set;}

    public Bid_Image_Controller(ApexPages.StandardController controller) {
        //currentWorkOrder  = ApexPages.CurrentPage().getparameters().get('Work_Order__c');
        
   Bid__c[] bid = [ Select Work_Order__c from Bid__c where id = :System.currentPagereference().getParameters().get('id') ];
   currentWorkOrder=bid[0].Work_Order__c;
    }
public List<ImageLinks__c> getlinks()
{
	links = [Select id,ImageUrl__c From ImageLinks__c Where CaseId__c=:currentWorkOrder];
	return links;
}
    
}