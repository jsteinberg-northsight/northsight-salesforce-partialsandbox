/**
 *  Description     :   This trigger helper class is used for handling all pre and post DML operation functionality on User standard object.
 *
 *  Created By      :   
 *
 *  Created Date    :   
 *
 *  Current Version :   V1.4
 *
 *  Revisiion Logs  :   V1.0 - Created
 *                      V1.1 - Modified - Padmesh Soni(04/09/2014) - Added new method for Pre processing functionality added to validate Alias from 
 *                                        another existing user. 
 *                      V1.2 - Modified - Padmesh Soni(10/06/2014) - SM-232: Prevent parenthesis from being in names.
 *                      V1.3 - Modified - Padmesh Soni(10/21/2014) - VCP-32: Prevent certain Profiles from being selected.
 *						V1.4 - Modified - Padmesh Soni(10/29/2014) - VCP-37: Improve "profile blocker" trigger by putting the settings in Custom Settings.
 **/
public without sharing class UserTriggerHandler {
    
    //Code added - Padmesh Soni(10/29/2014) - VCP-37: Improve "profile blocker" trigger by putting the settings in Custom Settings.
    //Variable to hold Prevent Profiles Default custom setting values
    public static Prevent_Profiles_Default__c CONFIG_PROFILE_PREVENTED {

        get {
        
            //Get Prevent Profiles Default values
            Prevent_Profiles_Default__c configDefaults = Prevent_Profiles_Default__c.getOrgDefaults();
            
            //Check if Prevent Profiles Default is already there or not
            if(configDefaults == null) {
                configDefaults = new Prevent_Profiles_Default__c();
            }
            
            //Return Prevent Profiles Default
            return configDefaults;
        }
    }
    
    public static void onAfterInsert(List<User> newUsers, Map<Id, User> userMap) {
        // EXECUTE AFTER INSERT LOGIC
        UpdateContactWithUserId.setUserId(newUsers, userMap);
    }
    
    /**
     *  @descpriton     :   This method is used for validate current inserting or updating User's Alias is unique into org. 
     *                      Padmesh Soni(04/09/2014) - Added Method.
     *
     *  @param          :   List of new Users, Map of Old users
     *
     *  @return         :   void 
     **/
    public static void validateUniqueAlias(List<User> newUsers, Map<Id, User> oldUsersMap) {
        
        //Set to hold all User's alias
        Set<String> userAliases = new Set<String>();
        
        //Loop through newUsers
        for(User newUser : newUsers) {
            
            //Check for Unique Alias
            //DON-4/22/2015 updated for case insensitive duplicate check for compatibility with SOQL. 
            if(String.isNotBlank(newUser.Alias) && (oldUsersMap == null || (newUser.Alias.tolowercase() != oldUsersMap.get(newUser.Id).Alias.tolowercase()))) {
                
                //add user's alias into set
                userAliases.add(newUser.Alias);
            }
        }
        
        //Query result of User who are having Alias which is into set
        List<User> users = [SELECT Id, Alias, Name FROM User WHERE Alias IN: userAliases];
        
        //Check for size of User
        if(users.size() > 0) {
            
            //Map to hold all existing users who are having aliases from userAliases set
            Map<String, User> mapUserWithAlias = new Map<String, User>();
            
            //Loop through query result of Users
            for(User usr : users) {
                
                //populate the map
                mapUserWithAlias.put(usr.Alias, usr);
            }
            
            //Loop through newUsers or Trigger.new
            for(User newUser : newUsers) {
                
                //Checking if map of User with Alias having newUser's Alias as key  
                if(mapUserWithAlias.containsKey(newUser.Alias)) {
                    
                    //add error on top
                    newUser.Alias.addError(Label.Validate_Unique_Alias_Error);
                }
            }
        }
    }
    
    /**
     *  @descpriton     :   This method is used for validate current inserting or updating User's FirstName, LastName or Alias having parenthesis. 
     *                      New method added - Padmesh Soni(10/06/2014) - SM-232: Prevent parenthesis from being in names 
     *
     *  @param          :   List of new Users, Map of Old users
     *
     *  @return         :   void
     **/
    public static void validateParenthesisInAliasOrNames(List<User> newUsers, Map<Id, User> oldUsersMap) {
        
        //Set to hold all User's alias
        Set<String> userAliases = new Set<String>();
        
        //Loop through newUsers
        for(User newUser : newUsers) {
            
            //Check for Prevent parenthesis from being in FirstName, LastName or Alias 
            if(((String.isNotBlank(newUser.FirstName) && (newUser.FirstName.contains(')') || newUser.FirstName.contains('('))) 
            		|| (String.isNotBlank(newUser.LastName) && (newUser.LastName.contains(')') || newUser.LastName.contains('(')))
            		|| (String.isNotBlank(newUser.Alias) && (newUser.Alias.contains(')') || newUser.Alias.contains('(')))) 
            		&& (oldUsersMap == null || (newUser.FirstName != oldUsersMap.get(newUser.Id).FirstName 
            		|| newUser.LastName != oldUsersMap.get(newUser.Id).LastName 
            		|| newUser.Alias != oldUsersMap.get(newUser.Id).Alias))) {
                
                //add error on top
                newUser.addError(Label.NOT_VALID_PARENTHESIS_IN_NAME_OR_ALIAS);
            }
        }
    }
    
    /**						~~~~ Code added - Padmesh Soni(10/21/2014) - VCP-32:Prevent certain Profiles from being selected ~~~~
     *  @descpriton     :   This method is used for Disallow setting a user to any of these profiles:
     *							1. Partner Community User     
     *							2. DO NOT USE - Partner Community - Updaters Login 
     *							3. DO NOT USE - Partner Community - Vendors Login 
     *							4. DO NOT USE - Partner Community - Vendors OrderSelect Login 
     *							5. Partner Community Login User
     *
     *  @param          :   List of new Users, Map of Old users
     *
     *  @return         :   void
     **/
    public static void preventUsersProfile(List<User> newUsers, Map<Id, User> oldUsersMap) {
    	
    	//Code added - Padmesh Soni(10/29/2014) - VCP-37: Improve "profile blocker" trigger by putting the settings in Custom Settings.
    	//String to hold ZWS Id of Zillow
        Prevent_Profiles_Default__c configDefault = CONFIG_PROFILE_PREVENTED;
        
        //Set to hold all profile names which are need 
    	//to be prevent from being creation of these type users 
    	Set<String> excludedProfileNames = new Set<String>();
        
        //Code added - Padmesh Soni(10/29/2014) - VCP-37: Improve "profile blocker" trigger by putting the settings in Custom Settings.
    	//Check for not null or blank
        if(String.isNotBlank(configDefault.Excluded_Profiles__c)) {
	    	
	    	//Code modified - Padmesh Soni(10/29/2014) - VCP-37: Improve "profile blocker" trigger by putting the settings in Custom Settings.
    		//Loop through after spliting the value of Excluded Profiles field values
	        for(String preventProfile : configDefault.Excluded_Profiles__c.split(',')) {
	        	
	        	//Populate the set
	        	excludedProfileNames.add(preventProfile.trim());	
	        }
        }
        
    	//List to hold query result of Profiles
    	Map<Id, Profile> mapPreventedProfiles = new Map<Id, Profile>([SELECT Id FROM Profile WHERE Name IN: excludedProfileNames]);
    	
    	//Check for size of list
    	if(mapPreventedProfiles.size() > 0) {
    		
    		//Loop through newUsers(Trigger.new)
    		for(User newUser : newUsers) {
    			
    			//Check for user's Profile is related to prevented profiles
    			if(newUser.ProfileId != null && mapPreventedProfiles.containsKey(newUser.ProfileId) 
    					&& (oldUsersMap == null || newUser.ProfileId != oldUsersMap.get(newUser.Id).ProfileId)) {
    				
    				//add error on top
                	newUser.addError(Label.PROFILE_SELECTION_WARNING);			
    			}
    		}
    	}
    }
}