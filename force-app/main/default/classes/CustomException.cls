/**
 *	Description		:	This class is extending Exception and it is used in another org wide classes to exception handling. 
 *
 *	Created By		:	Padmesh Soni
 *
 *	Created Date	:	02/26/2014
 *
 *	Current Version	:	V1.1
 **/
public class CustomException extends Exception {

}