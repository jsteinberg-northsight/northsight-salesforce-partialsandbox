@isTest(SeeAllData=false)
public without sharing class BidSurveyHelperTester {
//    private static Case newCase;
//    private static string bidSurveyPayload;
//    private static string caseNum;
//    
//    //--------------------------------------------------------
//    //PRE-TEST SETUP
//    //create a new test case
//    private static void generateTestCase() {
//        Id clientRt = [select id from recordtype where sobjecttype = 'Account' and name = 'Client'].Id;
//        //create a new account
//        Account newAccount = new Account(
//            Name = 'Tester Account',
//            RecordTypeId = clientRt,
//            Status__c = 'Active'
//        );
//        insert newAccount;
//    
//        clientRt = [select id from recordtype where sobjecttype = 'Contact' and name = 'Client'].Id;
//        //create a new contact
//        Contact newContact = new Contact(
//            LastName = 'Tester',
//            AccountId = newAccount.Id,
//            Pruvan_Username__c = 'NSBobTest',
//            MailingStreet = '30 Turnberry Dr',
//            MailingCity = 'La Place',
//            MailingState = 'LA',
//            MailingPostalCode = '70068',
//            OtherStreet = '30 Turnberry Dr',
//            OtherCity = 'La Place',
//            OtherState = 'LA',
//            OtherPostalCode = '70068',
//            Other_Address_Geocode__Latitude__s = 30.00,
//            Other_Address_Geocode__Longitude__s = -90.00,
//            RecordTypeId = clientRt
//        );
//        insert newContact;
//        
//        //create a new test case
//        newCase = new Case();
//        newCase.street_address__c = '1234 Street';
//        newCase.state__c = 'TX';
//        newCase.city__c = 'Austin';
//        newCase.zip_code__c = '78704';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        newCase.Client__c = 'NOT-SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'Grass Recut';
//        newCase.Client_Name__c = newAccount.Id;
//        
//        insert newCase;
//    }
//    
//    //--------------------------------------------------------
//    //PRE-TEST SETUP
//    //create a test payload that creates one bid
//    private static void setPayloadForOneBid(){
//        for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//            bidSurveyPayload = '{"username":"pruvan","password":"8c8cd34c37d6813808baa358b18169c211cb47f3","evidenceType":"Survey","fileName":"survey_1382537334014.json","uuid":"6969","parentUuid":"37064308","gpsLatitude":"42.01235463","gpsLongitude":"-87.89172686","gpsTimestamp":"1382537316","gpsAccuracy":"10.0","fileType":"survey","key2":"' + c.CaseNumber + '","uploaderVersion":"mobile 3.9.1","deviceId":"ios-1B3F968D-5F14-47F0-95EC-89A15EFBA580","sdkVersion":"7.0.4","createdBySubUser":"nmbob","survey":{"answers":[{"answer":["Trimming - Shrubs"],"id":"BidSurvey:Mobile_Bid_Type__c","hint":""},{"answer":["bid description"],"id":"BidSurvey:Bid_Description__c","hint":""},{"answer":["50"],"id":"BidSurvey:Bid_Cost__c","hint":""},{"answer":[""],"id":"bid photos","picIds":["d9aab2a9-eec6-470e-bf13-0cb74c262015"],"hint":""},{"answer":["no"],"id":"place bid","hint":""}],"meta":{"currentQuestionIndex":18,"surveyId":"71e3d9ac-68b1-464d-a09e-9783223a612c","parentId":"37064308","surveyTemplateId":"sarcj07::BidSurvey_001-v1"},"imageLink":"https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706991.json"}}';
//        }
//    }
//    
//    //--------------------------------------------------------
//    //PRE-TEST SETUP
//    //create a test payload that creates two bids
//    private static void setPayloadForMultipleBids(){
//        for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//            caseNum = c.CaseNumber;
//            bidSurveyPayload = '{"username":"pruvan","password":"8c8cd34c37d6813808baa358b18169c211cb47f3","evidenceType":"Survey","fileName":"survey_1382537334014.json","uuid":"6969","parentUuid":"37064308","gpsLatitude":"42.01235463","gpsLongitude":"-87.89172686","gpsTimestamp":"1382537316","gpsAccuracy":"10.0","fileType":"survey","key2":"' + c.CaseNumber + '","uploaderVersion":"mobile 3.9.1","deviceId":"ios-1B3F968D-5F14-47F0-95EC-89A15EFBA580","sdkVersion":"7.0.4","createdBySubUser":"nmbob","survey":{"answers":[{"answer":["Trimming - Shrubs"],"id":"BidSurvey:Mobile_Bid_Type__c","hint":""},{"answer":["bid description"],"id":"BidSurvey:Bid_Description__c","hint":""},{"answer":["50"],"id":"BidSurvey:Bid_Cost__c","hint":""},{"answer":[""],"id":"bid photos","picIds":["6916"],"hint":""},{"answer":["yes"],"id":"place bid","hint":""},{"answer":["Trimming - Trees"],"id":"BidSurvey2:Mobile_Bid_Type__c","hint":""},{"answer":["bid description2"],"id":"BidSurvey2:Bid_Description__c","hint":""},{"answer":["25"],"id":"BidSurvey2:Bid_Cost__c","hint":""},{"answer":[""],"id":"bid photos2","picIds":["1669"],"hint":""},{"answer":["no"],"id":"place bid2","hint":""}],"meta":{"currentQuestionIndex":18,"surveyId":"71e3d9ac-68b1-464d-a09e-9783223a612c","parentId":"37064308","surveyTemplateId":"sarcj07::BidSurvey_001-v1"},"imageLink":"https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706991.json"}}';
//        }
//    }
//    
//    //--------------------------------------------------------
//    //TESTS
//    //Unit tests for method testing and regression testing. 
//    //--------------------------------------------------------
//        
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - use the single bid payload and test that a single bid is created
//    //when the payload is sent through the PruvanPhotoHelper down to the BidSurveyHelper
//    public static testmethod void oneBidTest(){
//        system.Test.startTest();
//        
//        generateTestCase();
//        setPayloadForOneBid();              
//        
//        //send the bidSurveyPayload
//        PruvanWebServices.doUploadPictures(bidSurveyPayload);
//         
//        //select back the bid that should have been created and assert that it was created with the appropriate data values
//        List<Bid__c> bidList = [SELECT Id,
//                                 Work_Order__c,
//                                 Mobile_Bid_Type__c,
//                                 Bid_Description__c,
//                                 Bid_Cost__c
//                          FROM Bid__c];
//        system.assertEquals(1, bidList.size());
//        //loop thru the list and assert the bids were created with the appropriate data values
//        for(Bid__c bid : bidList){
//            system.assertEquals(newCase.Id, bid.Work_Order__c);
//            system.assertEquals('Trimming - Shrubs', bid.Mobile_Bid_Type__c);
//            system.assertEquals('bid description', bid.Bid_Description__c);
//            system.assertEquals(50, bid.Bid_Cost__c);
//        }
//        
//        //select back the image links that should have been created
//        Case caseIdSelect = [SELECT Id FROM Case WHERE Id =: newCase.Id limit 1];
//        System.Debug('caseIdSelect::: '+caseIdSelect.Id);
//        List<ImageLinks__c> ilList = [SELECT Id,
//                                       CaseId__c,
//                                       Survey_Id__c,
//                                       Survey_Question_Id__c,
//                                       Pruvan_uuid__c
//                                FROM ImageLinks__c
//                                WHERE CaseId__c =: caseIdSelect.Id];
//        system.assertEquals(2, ilList.size());
//        //assert the image link was created with the appropriate values
//        Map<string,ImageLinks__c> ilMap = new Map<string,ImageLinks__c>();
//        for(ImageLinks__c iL : ilList){
//            ilMap.put(iL.Pruvan_uuid__c, iL);
//        }
//        
//        system.assert(ilMap.ContainsKey('d9aab2a9-eec6-470e-bf13-0cb74c262015'));
//        ImageLinks__c imageLink = ilMap.get('d9aab2a9-eec6-470e-bf13-0cb74c262015');
//        system.assertEquals(newCase.Id, imageLink.CaseId__c);
//        system.assertEquals('BidSurvey_001-v1', imageLink.Survey_Id__c);
//        system.assertEquals('bid photos', imageLink.Survey_Question_Id__c);
//        
//        system.Test.stopTest();
//    }
//    
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - use the single bid payload and test that a single bid is created
//    //when the payload is sent through the PruvanPhotoHelper down to the BidSurveyHelper
//    public static testmethod void multiBidTest(){
//        system.Test.startTest();
//        
//        generateTestCase();
//        setPayloadForMultipleBids();            
//        
//        //send the bidSurveyPayload
//        PruvanWebServices.doUploadPictures(bidSurveyPayload);
//        
//        //select back the bids that should have been created and assert that they were created with the appropriate data values
//        List<Bid__c> bidList = [SELECT Id,
//                                 Work_Order__c,
//                                 Mobile_Bid_Type__c,
//                                 Bid_Description__c,
//                                 Bid_Cost__c
//                          FROM Bid__c];
//        system.assertEquals(2, bidList.size());
//        //loop thru the list and assert the bids were created with the appropriate data values
//        for(Bid__c bid : bidList){
//            if(bid.Bid_Description__c == 'bid description'){
//                system.assertEquals(newCase.Id, bid.Work_Order__c);
//                system.assertEquals('Trimming - Shrubs', bid.Mobile_Bid_Type__c);
//                system.assertEquals('bid description', bid.Bid_Description__c);
//                system.assertEquals(50, bid.Bid_Cost__c);
//            } else {
//                system.assertEquals(newCase.Id, bid.Work_Order__c);
//                system.assertEquals('Trimming - Trees', bid.Mobile_Bid_Type__c);
//                system.assertEquals('bid description2', bid.Bid_Description__c);
//                system.assertEquals(25, bid.Bid_Cost__c);
//            }
//        }
// 
//        //select back the image links that should have been created
//        List<ImageLinks__c> ilList = [SELECT Id,
//                                       CaseId__c,
//                                       Survey_Id__c,
//                                       Survey_Question_Id__c,
//                                       Pruvan_uuid__c
//                                FROM ImageLinks__c
//                                WHERE CaseId__c =: newCase.Id];
//        system.assertEquals(3, ilList.size());
//        //assert the image links were created with the appropriate values
//        Map<string,ImageLinks__c> ilMap = new Map<string,ImageLinks__c>();
//        for(ImageLinks__c iL : ilList){
//            ilMap.put(iL.Pruvan_uuid__c, iL);
//        }
//            
//        system.assert(ilMap.ContainsKey('6916'));
//        ImageLinks__c imageLink = ilMap.get('6916');
//        system.assertEquals(newCase.Id, imageLink.CaseId__c);
//        system.assertEquals('BidSurvey_001-v1', imageLink.Survey_Id__c);
//        system.assertEquals('bid photos', imageLink.Survey_Question_Id__c);
//    
//        system.assert(ilMap.ContainsKey('1669'));
//        ImageLinks__c imageLink2 = ilMap.get('1669');
//        system.assertEquals(newCase.Id, imageLink2.CaseId__c);
//        system.assertEquals('BidSurvey_001-v1', imageLink2.Survey_Id__c);
//        system.assertEquals('bid photos2', imageLink2.Survey_Question_Id__c);
//        
//        system.Test.stopTest();
//    }
}