@isTest(SeeAllData=true)
public class RHX_TEST_ImageLinkRlt {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM ImageLinkRlt__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new ImageLinkRlt__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}