global class ScheduleBatch_InProcess_to_Pending implements Schedulable {
	global void execute(SchedulableContext ctx) {
		Batch_InProcess_to_Pending b = new Batch_InProcess_to_Pending();
		
		Database.executeBatch(b, 200);
	}
	
	//Scheduler test method
    static testMethod void testScheduleBatch_InProcess_to_Pending() {
        
        //Test starts here
		Test.startTest();
        
        // Schedule the test job at midnight Sept. 3rd. 2022
        String jobId = System.schedule('Test_Sched_Batch_AttachmentCleanupOnCases', '0 0 0 3 9 ? 2022', new ScheduleBatch_InProcess_to_Pending());
        
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same
        System.assertEquals('0 0 0 3 9 ? 2022', ct.CronExpression);
        
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        
        //Test stops here
		Test.stopTest();
    }
}