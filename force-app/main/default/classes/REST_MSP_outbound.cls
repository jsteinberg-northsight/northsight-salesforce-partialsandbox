@RestResource(urlMapping='/integrations/MSP/outbound/*')
global without sharing class REST_MSP_outbound {
	
	static final string PASSWORD='2b0d09b1-f369-4854-8285-1e6ed8a23b52';
	
	
	static final string FETCH_TYPE_FIRST = 'fetchFirst';
	static final string FETCH_TYPE_NEXT = 'fetchNext';
	static final string FETCH_TYPE_SUCCESS = 'fetchSuccess';
	static final string FETCH_TYPE_FAIL= 'fetchFail';
	static final string FETCH_TYPE_LIST = 'fetchList';
	
	static final set<string> validFetches = new set<string>{FETCH_TYPE_FIRST, FETCH_TYPE_NEXT, FETCH_TYPE_SUCCESS, FETCH_TYPE_FAIL, FETCH_TYPE_LIST};
	
	static final integer RECORDS_PER_CHUNK = 250;
	
	static final string MSP_FILENAME_PREFIX = 'L0001';
	
	class Payload{
		boolean more{get;set;}
		id batchID{get;set;}
		INT_MSP_Result__c[] results {get;set;}
		WaitingAccount[] accountsWithResultsWaiting {get;set;}
		string filename{get;set;}
	}
	
	class WaitingAccount{
		id accountId {get;set;}
		string filename {get;set;}
		string MSPclientCode {get;set;}
	}	
	
	@HttpPost //using a POST here so as not to hvae the password in the URL
	global static void doPost() {
		
		RestRequest req = RestContext.request;
		string paramBatchID=req.headers.get('batchID');
		string paramPassword= req.headers.get('password');
		string paramFetchType=req.headers.get('fetchType');
		string paramLog=req.headers.get('log');
		string paramAcctId= req.headers.get('accountID');
		
		id batchID = string.isEmpty(paramBatchID) ? null : (id)paramBatchID;
		id acctId= string.isEmpty(paramAcctId) ? null : (id)paramAcctId;
		boolean batchWasGiven = batchID!=null;
		boolean acctIdWasGiven = paramAcctId!=null;
		
		if (paramPassword!=PASSWORD){
			RestContext.response.statuscode=400;
			RestContext.response.responsebody=blob.valueof('bad password');
		}
		else if(!validFetches.contains(paramFetchType)){
			RestContext.response.statuscode=400;
			RestContext.response.responsebody=blob.valueof('bad fetchType param');
		}
		else if(
			
			((paramFetchType==FETCH_TYPE_NEXT || paramFetchType==FETCH_TYPE_FIRST) && !acctIdWasGiven) ||
			(paramFetchType==FETCH_TYPE_FIRST && batchWasGiven) || //a batchid would not be provided on first request
			(paramFetchType==FETCH_TYPE_NEXT && !batchWasGiven) ||  
			((paramFetchType==FETCH_TYPE_SUCCESS || paramFetchType==FETCH_TYPE_FAIL)  && !batchWasGiven)
		)
		{
			
			RestContext.response.statuscode=400;
			RestContext.response.responsebody=blob.valueof('bad parameter combo');
		}		
		else	
			try{
				
				if(paramFetchType==FETCH_TYPE_SUCCESS || paramFetchType==FETCH_TYPE_FAIL){
					INT_MSP_Outbound_Batch__c batch =
						[select id, Transmission_Result__c, log__c
						from INT_MSP_Outbound_Batch__c
						where id=:batchID
						for update
						];
					
					system.assert(batch!=null);
					
					batch.Transmission_Result__c= paramFetchType==FETCH_TYPE_SUCCESS ? 'Success' : 'Failure';
					addLineToBatchLog(batch, 'result from caller: ' + batch.Transmission_Result__c);
					addLineToBatchLog(batch, 'log sent from caller: ' + paramLog);
					update batch;
				}
				else if(paramFetchType==FETCH_TYPE_LIST){
					Payload payload = new Payload();
					//id[] acctIds = new id[]{};
					WaitingAccount[] waitingAccounts= new WaitingAccount[]{};
					
					
					AggregateResult[] results =
					  [select Related_Work_Order__r.Account.id
				        from INT_MSP_Result__c
				        where
				        	(Transmission_Status__c != 'Sent' AND 
							status__c='Ready' AND
							Test_Record__c = false) 
                       		OR
                       		INT_MSP_Result_Batch__r.Transmission_Result__c != 'Success'
				        group by Related_Work_Order__r.Account.id];
				            
			            
				            
				            
					for (AggregateResult ar : results){
						id tmpID=(id)ar.get('id');
						string cliCode= [select MSP_Client_Code__c from account where id=:tmpID].MSP_Client_Code__c;
					    WaitingAccount waitAcct = new WaitingAccount();
					    waitAcct.accountID=tmpID;
					    waitAcct.filename=MSP_FILENAME_PREFIX +  cliCode;
					    waitAcct.MSPclientCode = cliCode;
					    waitingAccounts.add(waitAcct);
					}
									    
				    payload.accountsWithResultsWaiting=waitingAccounts;
				        
					RestContext.response.responsebody=blob.valueof(json.serialize(payload));
				}
				else //FETCH_TYPE_FIRST or FETCH_TYPE_NEXT
				{	
					Payload payload = new Payload();
					INT_MSP_Result__c[] resultsChunkToSend;			
					
					if(doResultsForSendingExist()){
						INT_MSP_Outbound_Batch__c batch=fetchOrCreateBatchToUse(batchID,acctId);
						addlinetobatchlog(batch, 'batch selected for upload');
						batchID=batch.id;					
						payload.batchID = batchID;
						
						if (paramFetchType==FETCH_TYPE_FIRST){
							resetStatusOfChildrenToUnsent(batchID);
							associateReadyResultsToBatch(batchID, acctID);
						}
						
						integer numResultsReady = fetchCountOfResultsReadyForSend(batchID);
						addLineToBatchLog(batch, 'Results Ready for Send: ' + numResultsReady);
						payload.more = (numResultsReady > RECORDS_PER_CHUNK);	
						resultsChunkToSend=fetchChunkofReadyResults(batchID);
						addLineToBatchLog(batch, 'Results Sent in Chunk: ' + resultsChunkToSend.size());
						setResultsTransmissionStatusesToSent(resultsChunkToSend);
						payload.results=resultsChunkToSend;
						
						update batch; // for the log entries
					
						
					}
					else{
						payload.more=false;
						payload.results=new INT_MSP_Result__c[]{};
						
					}
					RestContext.response.responsebody=blob.valueof(json.serialize(payload));
					
				}
				
				
				
								
				
			}
			catch(exception e){	
				RestContext.response.statuscode=500;
				RestContext.response.responsebody=blob.valueof(e.getMessage());
				sendErrorNotificationEmail(e);
			}
			
			
	}

	
	static boolean doResultsForSendingExist(){
		if (
				[select id 
				from INT_MSP_Outbound_Batch__c 
				where Transmission_Result__c != 'Success'].size()>0) return true;
		
		else if (fetchCountOfResultsReadyForSend(null)>0) return true;
		else return false;
	
	}
	
	public static void tmp_fetchCountOfResultsReadyForSend(){
		system.debug(fetchCountOfResultsReadyForSend(null));
	}
	
	static void addLineToBatchLog(INT_MSP_Outbound_Batch__c batch, string log){
		batch.log__c=(string.isblank(batch.log__c) ? '' : batch.log__c) +'\r\n'+ datetime.now() + '\t' +  log;
		
	}
	
	static void setResultsTransmissionStatusesToSent(INT_MSP_Result__c[] results /*modified*/ ){
		for (INT_MSP_Result__c res : results) res.Transmission_Status__c='Sent';
		update results;
	}
	
	static INT_MSP_Result__c[] fetchChunkofReadyResults(id batchID){
		
		INT_MSP_Result__c[] ret =[
				SELECT ACTION_NEEDED_BOARD_SECURE__c,ACTION_NEEDED_CHANGE_LOCKS__c,ACTION_NEEDED_CUT_GRASS__c,ACTION_NEEDED_DRAIN_POOL__c,ACTION_NEEDED_OTHER__c,ACTION_NEEDED_REMOVE_DEBRIS__c,ACTION_NEEDED_REPLACE_GLASS__c,ACTION_NEEDED_WINTERIZE__c,BROKER_NAME__c,BROKER_TELEPHONE__c,CARD_LEFT_AT_PROPERTY__c,COMMENTS_1__c,COMMENTS_2__c,CONSTRUCTION_TYPE_OTHER__c,CONSTRUCTION_TYPE__c,CORRECT_ALTERNATE_PHONE_LOCATION_TAG__c,CORRECT_ALTERNATE_PHONE_NUMBER_1__c,CORRECT_ALTERNATE_PHONE_NUMBER_2__c,CORRECT_PRIMARY_PHONE_LOCATION_TAG__c,CORRECT_PRIMARY_PHONE_NUMBER__c,CreatedById,CreatedDate,DELINQUENCY_REASON_CODE__c,DELINQUENCY_REASON_OTHER__c,DEPENDENT_COUNT__c,DISCUSSED_RELIEF_MEASURES_CODE__c,ESTIMATED_VALUE_CODE__c,EXTERNAL_PROPERTY_CONDITION__c,FEES_DUE_BY_INSPECTION_TYPE_A__c,FEES_DUE_BY_INSPECTION_TYPE_B__c,FEES_DUE_BY_INSPECTION_TYPE_C__c,FEES_DUE_BY_INSPECTION_TYPE_D__c,FEES_DUE_BY_INSPECTION_TYPE_E__c,FEES_DUE_BY_INSPECTION_TYPE_F__c,FEES_DUE_BY_INSPECTION_TYPE_G__c,FEES_DUE_BY_INSPECTION_TYPE_H__c,FEES_DUE_BY_INSPECTION_TYPE_I__c,FEES_DUE_BY_INSPECTION_TYPE_J__c,FEES_DUE_BY_INSPECTION_TYPE_K__c,FEES_DUE_CANCELLATIONS__c,FEES_DUE_OTHER__c,FEES_DUE_PHOTO__c,FEES_DUE_TOTAL__c,FIELD_SERVICING_COMPANY__c,FIRST_KNOWN_VACANCY_DATE__c,FIRST_TIME_VACANT__c,Id,INCOME_FLAG_CODE__c,INCOME_FREQUENCY__c,INCOME_NAME__c,INCOME_NUMBER__c,INCOME_PAYMENT_AMOUNT__c,INCOME_TYPE__c,INSPECTION_ADDRESS__c,INSPECTION_COMPLETE_DATE__c,INSPECTION_REQUEST_DATE__c,INSPECTION_REQUEST_SOURCE__c,INSPECTION_TYPE__c,INSPECTOR_NAME_CODE__c,INTERVIEWEE_ATTITUDE_CODE__c,INTERVIEWEE_CODE__c,INTERVIEWEE_OTHER__c,INT_MSP_Inbound_Child__c,INT_MSP_Result_Batch__c,IN_MILITARY_SERVICE__c,IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,LOAN_NUMBER__c,MAINTENANCE_RECOMMENDED__c,MAP_REFERENCE_NUMBER_MAILING__c,MAP_REFERENCE_NUMBER_PROPERTY__c,MORTGAGE_COMPANY__c,Name,NEIGHBORHOOD_CONDITION_CODE__c,OBLIGATION_BALANCE__c,OBLIGATION_DELINQUENT_PAYMENT_COUNT__c,OBLIGATION_NAME__c,OBLIGATION_NUMBER__c,OBLIGATION_PAYMENT_FREQUENCY_CODE__c,OBLIGATION_PAYMENT__c,OBLIGATION_TYPE_CODE__c,OCCUPANCY_CODE__c,OCCUPANCY_VERIFIED_OTHER__c,OCCUPANCY_VERIFIED__c,OFFICE_REGION__c,OTHER_PROPERTY_CONDITION_PROBLEM__c,OwnerId,PERSONAL_PROPERTY_ON_SITE__c,PERSON_INTERVIEWED__c,PHONE_VERIFICATION_TAG__c,PHOTOS_FORWARDED__c,POOL_ON_SITE__c,POOL_SECURED__c,PROMISE_TO_PAY_AMOUNT__c,PROMISE_TO_PAY_DATE__c,PROMISE_TO_PAY_NUMBER_OF_PAYMENTS__c,PROPERTY_CONDITION_PROBLEM_CODE__c,PROPERTY_FOR_SALE__c,PROPERTY_TYPE__c,RECORD_CODE__c,RECORD_ID__c,Related_Work_Order__c,RELIEF_MEASURES_ASSIGNMENT__c,RELIEF_MEASURES_COUNSELING__c,RELIEF_MEASURES_DEED_IN_LIEU__c,RELIEF_MEASURES_FORBEARANCE__c,RELIEF_MEASURES_SALE__c,RENTER_S_NAME__c,RENT_AMOUNT__c,RENT_DELINQUENT__c,RENT_PAID_TO__c,RENT_PAYEE_ADDRESS_1__c,RENT_PAYEE_ADDRESS_2__c,REQUESTOR_CODE__c,SERVICE_BUREAU__c,SystemModstamp,TENURE_MONTHS__c,TENURE_YEARS__c,TOTAL_CFS_RECORDS__c,TOTAL_FEES_DUE_FOR_INSPECTIONS_TYPE_A__c,TOTAL_FEES_DUE_FOR_INSPECTIONS_TYPE_B__c,TOTAL_FEES_DUE_FOR_INSPECTIONS_TYPE_C__c,TOTAL_FEES_DUE_FOR_INSPECTIONS_TYPE_D__c,TOTAL_FEES_DUE_FOR_INSPECTIONS_TYPE_E__c,TOTAL_FEES_DUE_FOR_INSPECTIONS_TYPE_F__c,TOTAL_FEES_DUE_FOR_INSPECTIONS_TYPE_G__c,TOTAL_FEES_DUE_FOR_INSPECTIONS_TYPE_H__c,TOTAL_FEES_DUE_FOR_INSPECTIONS_TYPE_I__c,TOTAL_FEES_DUE_FOR_INSPECTIONS_TYPE_J__c,TOTAL_FEES_DUE_FOR_INSPECTIONS_TYPE_K__c,TOTAL_FEE_RECORDS__c,TOTAL_PIR_RECORDS__c,UTILITIES_ON_ELECTRIC__c,UTILITIES_ON_GAS__c,UTILITIES_ON_WATER__c,VERSION_NUMBER__c,WORK_ORDER_COMPLETION_DATE__c,Number_of_Photos_Taken__c
				from INT_MSP_Result__c
				where 
					INT_MSP_Result_Batch__c=:batchID AND
					Transmission_Status__c != 'Sent'
				limit :RECORDS_PER_CHUNK
				for update /*for update clause is critical to preventing accidental simultaneous calls to endpoint from double submitting to fiserv*/
			];
		return ret;
	}
	
	static integer fetchCountOfResultsReadyForSend(id batchID){
		integer ret;
		
		AggregateResult[] ar  = 
			[SELECT count(id) cnt
			FROM INT_MSP_Result__c 
			where
				INT_MSP_Result_Batch__c=:batchID AND
				Transmission_Status__c != 'Sent' AND 
				status__c='Ready' AND
				Test_Record__c = false
			];
		
		ret = (integer)ar[0].get('cnt'); 
		return ret;				
	}
	
	static void sendErrorNotificationEmail(exception e){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		
		// Strings to hold the email addresses to which you are sending the email.
		String[] toAddresses = new String[] {'software@northsight.com'}; 
		mail.setToAddresses(toAddresses);
		mail.setReplyTo('DONOTREPLY@northsight.com');
		mail.setSenderDisplayName('MSP Outbound Integration');
		mail.setSubject('MSP Outbound Integration Error');		
		mail.setPlainTextBody(e.getmessage() + e.getstacktracestring());
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	

	static void associateReadyResultsToBatch(id batchID, id accountID){
		INT_MSP_Result__c[] readyResults = 
			[select id, INT_MSP_Result_Batch__c
			from INT_MSP_Result__c
			where 
				status__c='Ready' AND
				Test_Record__c = false AND
				INT_MSP_Result_Batch__c=null AND
				related_Work_Order__r.Account.id=:accountID
			for update
			];
		
		for (INT_MSP_Result__c r : readyResults) r.INT_MSP_Result_Batch__c=batchID;
		
		update readyResults;
	}
	
	/*
		It will probably rarely if ever happen, but there could be an situation where a batch was large enough
		that we send in chunks.  in this scenario, we need to mark the result as "sent" to keep track of the chunks.
		if, say, there was an error in the external service and the full send to fiserv never happened, we could have
		result records that say sent but the parent batch will have its transmission status as unsent b/c the confirmation
		call never came through from the external service.  	
	*/
	static void resetStatusOfChildrenToUnsent(id batchID){
		
		INT_MSP_Result__c[] children =
			[select id,transmission_status__c 
			from INT_MSP_Result__c 
			where 
				INT_MSP_Result_Batch__c=:batchID
			for update];
		
		for (INT_MSP_Result__c child : children){
			child.transmission_status__c='Unsent';
		}
		
		update children;
		 
	}
	
	static INT_MSP_Outbound_Batch__c fetchOrCreateBatchToUse(id batchID, id acctId){
		INT_MSP_Outbound_Batch__c ret;
		
		if (batchID == null){
			INT_MSP_Outbound_Batch__c[] availableBatches= 
				[select id, Transmission_Result__c, log__c 
				from INT_MSP_Outbound_Batch__c 
				where
					Transmission_Result__c != 'Success' AND
					Account__c= :acctId];
			
			system.assert(availableBatches.size()<=1, 'There should be only 1 failed or unsent batch at a time');
			
			if (availableBatches.isEmpty()){
				ret = new INT_MSP_Outbound_Batch__c(account__c=acctId);	
				insert ret;
			}
			else{
				ret=availableBatches[0];
			}			
		}
		else
		{
			ret= 
				[select id, Transmission_Result__c,log__c 
				from INT_MSP_Outbound_Batch__c 
				where id=:batchID];
		
			system.assert(ret!=null, 'Batch ' + batchID + ' not found.');
			system.assert(ret.Transmission_Result__c != 'Success', 'batch already processed');
				
		}
		return ret;	
	}
	
	
	public static void SandboxTesting(){
		delete [select id from INT_MSP_Outbound_Batch__c];
		
		INT_MSP_Result__c[] results = [select Number_of_Photos_Taken__c, transmission_status__c, status__c from INT_MSP_Result__c];
		for (INT_MSP_Result__c res : results){
			res.transmission_status__c='Unsent';
			res.status__c='Ready';
			
		}
		
		update results;
	}
	
	global static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		
		
	}
    
}