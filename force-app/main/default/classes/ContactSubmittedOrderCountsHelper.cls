/**
 *  Purpose         :   This is helper class for Case trigger OrderAssignmentPostProcess. 
 *						Its having functionality to update Contact's Submitted Count Today field which is associated with Case record.
 *
 *  Modified By      :   Padmesh Soni
 *
 *  Modifed Date    :   09/10/2014
 *
 *  Current Version :   V1.1
 *
 *  Revision Log    :   V1.0 - Created
 *						V1.1 - Modified - Padmesh Soni(08/07/2014) - OAM-75 Submitted Today/Yesterday Not Updating
 **/
public without sharing class ContactSubmittedOrderCountsHelper {
	
	public static boolean BYPASS_SUBMIT_COUNT_UPDATES = true;
	public static final String ORDER_APPROVAL_STATUS_PRE_PENDING = 'Pre-Pending';
	public static final String ORDER_APPROVAL_STATUS_NOT_STARTED = 'Not Started';
	public static final String ORDER_APPROVAL_STATUS_REJECTED = 'Rejected';
	
	//Variable to hold recordtype list
	public static Map<Id, RecordType> recordTypesToExclude = new Map<Id, RecordType>([SELECT Id, Name FROM RecordType 
																						WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
																						AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true]);
	
	/**
	 *		---- Code added - Padmesh Soni (09/09/2014) - OAM-75 Submitted Today/Yesterday Not Updating -----
	 *	@descpriton		:	This method is used for updating Submitted Count Today field on Contact record with which Case is associated.
	 *
	 *	@param			:	List<Case> newOrders: Trigger.new, Map<Id, Case> oldMapOrders: Trigger.oldMap
	 *
	 *	@return			:	void
	 **/
	/**public static void populateContactSubmittedCount(List<Case> newOrders, Map<Id, Case> oldMapOrders) {
		
		//Set To hold contact ids which are to be updated
		Set<Id> contactIds = new Set<Id>();
		
		//Set to hold all user Ids if Case record's owner is changed
		Set<Id> oldVendorIds = new Set<Id>();
		
		//List to hold Contacts to be updated
		List<Contact> contactsToUpdated = new List<Contact>();
		
		//Loop through Trigger.new list
		for(Case newOrder : newOrders) {
			
			//Check for Approval Status is Changed to "Pre-Pending"
			if((newOrder.Approval_Status__c == ORDER_APPROVAL_STATUS_PRE_PENDING 
				|| oldMapOrders.get(newOrder.Id).Approval_Status__c == ORDER_APPROVAL_STATUS_PRE_PENDING)
				&& newOrder.Approval_Status__c != oldMapOrders.get(newOrder.Id).Approval_Status__c) {
				
				//add contact ids into set
				contactIds.add(newOrder.ContactId);
			}
		}
		
		//Check for size
		if(contactIds.size() > 0) {
			
			//List to hold Aggregate result of Case
			List<AggregateResult> contactSubmittedOrders = [SELECT ContactId, Count(Id) submittedOrders FROM Case WHERE RecordTypeId NOT IN: recordTypesToExclude 
																AND Status != 'Canceled' AND Approval_Status__c NOT IN ('Not Started','Rejected') 
																AND LastModifiedDate = TODAY AND ContactId IN: contactIds 
																AND DAY_ONLY(Pre_Pending_Date_Time__c) = TODAY GROUP BY ContactId];
			
			if(contactSubmittedOrders.size() > 0) {
				
				//Loop through aggregate results
				for(AggregateResult ar : contactSubmittedOrders){
					
					//Get Contact Id from aggregate result
					Id conId = (Id)ar.get('ContactId');
					
					//Getting Submitted Order Count Today
					Integer submittedCount = (Integer)ar.get('submittedOrders');
					
					//Check for not null
					if(conId != null && contactIds.contains(conId)){
						
						//add Contact into list to be updated
						contactsToUpdated.add(new Contact(Id = conId, Submitted_Count_Today__c = submittedCount));
						
						//remove the processed Contact from the set 
						contactIds.remove(conId);
					}
				}
				
				//Check for remaining Contacts which are not associated with 
				//any Case update to or from "Pre-Pending" Approval Status and 
				//not getting Aggregate counts in aggregate result then update 
				//all Contact's Submitted Count Today with 0  
				if(contactIds.size() > 0) {
					
					//Loop through set of ContactIds
					for(Id conId : contactIds) {
					
						//add Contact into list to be updated
						contactsToUpdated.add(new Contact(Id = conId, Submitted_Count_Today__c = 0));
					}	
				}
			} else {
				
				//Loop through set of ContactIds
				for(Id conId : contactIds) {
				
					//add Contact into list to be updated
					contactsToUpdated.add(new Contact(Id = conId, Submitted_Count_Today__c = 0));
				}
			}
		}
		
		//Check for size of list
		if(contactsToUpdated.size() > 0) {
			database.update(contactsToUpdated,false);
		}
	}**/
	
	public static void populateContactSubmittedCount(List<Case> newOrders, Map<Id, Case> oldMapOrders) {
		
		//Set To hold contact ids which are to be updated
		Set<Id> contactIds = new Set<Id>();
		
		//Map to hold Contacts to be updated
		Map<Id, Contact> contactsToUpdated = new Map<Id, Contact>();
		
		//Loop through Trigger.new list
		for(Case newOrder : newOrders) {
			
			//Check for recordtype that is not related to excluded RecordTypes
			if(!recordTypesToExclude.containsKey(newOrder.RecordTypeId)) {
					
				//Check for Approval Status is Changed to "Pre-Pending"
				if(newOrder.Approval_Status__c == ORDER_APPROVAL_STATUS_PRE_PENDING 
					&& (oldMapOrders.get(newOrder.Id).Approval_Status__c == ORDER_APPROVAL_STATUS_NOT_STARTED 
							|| oldMapOrders.get(newOrder.Id).Approval_Status__c == ORDER_APPROVAL_STATUS_REJECTED)
					&& newOrder.Approval_Status__c != oldMapOrders.get(newOrder.Id).Approval_Status__c) {
					
					//add contact ids into set
					contactIds.add(newOrder.ContactId);
				}
			}
		}
		
		//Check for size
		if(contactIds.size() > 0) {
			
			Map<Id, Contact> submittedToVendors = new Map<Id, Contact>([SELECT Id, Last_Submitted_Date_Time__c, Submitted_Count_Today__c FROM Contact WHERE Id IN:contactIds]);
			
			//Check for size
			if(submittedToVendors.size() > 0) {
				
				//Loop through Trigger.new list
				for(Case newOrder : newOrders) {
					
					//Check for recordtype that is not related to excluded RecordTypes
					if(!recordTypesToExclude.containsKey(newOrder.RecordTypeId)) {
					
						//Check for Approval Status is Changed to "Pre-Pending" form "Not Started" or "Rejected"
						if(newOrder.Approval_Status__c == ORDER_APPROVAL_STATUS_PRE_PENDING 
							&& (oldMapOrders.get(newOrder.Id).Approval_Status__c == ORDER_APPROVAL_STATUS_NOT_STARTED 
									|| oldMapOrders.get(newOrder.Id).Approval_Status__c == ORDER_APPROVAL_STATUS_REJECTED)
							&& newOrder.Approval_Status__c != oldMapOrders.get(newOrder.Id).Approval_Status__c) {
							
							//Check for Case's ContactId is alreaday exists into map of submittedToVendors and Last Submitted on today
							if(submittedToVendors.containsKey(newOrder.ContactId)
								&& submittedToVendors.get(newOrder.ContactId).Last_Submitted_Date_Time__c != null 
								&& submittedToVendors.get(newOrder.ContactId).Last_Submitted_Date_Time__c.date() == Date.today()) {
								
								//Check contact's Id is already a key of map
								if(contactsToUpdated.containsKey(newOrder.ContactId)) {
									
									//counter incremented by 1
									contactsToUpdated.get(newOrder.ContactId).Submitted_Count_Today__c = contactsToUpdated.get(newOrder.ContactId).Submitted_Count_Today__c==null?1:contactsToUpdated.get(newOrder.ContactId).Submitted_Count_Today__c + 1;
								} else {
									
									//populate the map at initial level
									contactsToUpdated.put(newOrder.ContactId, new Contact(Id= newOrder.ContactId, Submitted_Count_Today__c = submittedToVendors.get(newOrder.ContactId).Submitted_Count_Today__c==null?1:submittedToVendors.get(newOrder.ContactId).Submitted_Count_Today__c+1));
								}
								
								//System.debug('Step 1::::'+ contactsToUpdated);
							} 
							//Check for Case's ContactId is alreaday exists into map of submittedToVendors
							//and Last Submitted on yesterday and count today's not zero
							else if(submittedToVendors.containsKey(newOrder.ContactId)
										&& submittedToVendors.get(newOrder.ContactId).Last_Submitted_Date_Time__c != null 
										&& submittedToVendors.get(newOrder.ContactId).Last_Submitted_Date_Time__c.date() == Date.today().addDays(-1)
										&& submittedToVendors.get(newOrder.ContactId).Submitted_Count_Today__c != 0) {
								
								//Check contact's Id is already a key of map
								if(contactsToUpdated.containsKey(newOrder.ContactId)) {
									
									//counter incremented by 1
									contactsToUpdated.get(newOrder.ContactId).Submitted_Count_Today__c += 1;
								} else {
									
									//populate the map at initial level
									contactsToUpdated.put(newOrder.ContactId, new Contact(Id= newOrder.ContactId, Submitted_Count_Today__c = 1, 
																							Submitted_Count_Yesterday__c = submittedToVendors.get(newOrder.ContactId).Submitted_Count_Today__c));
								}
								
								//System.debug('Step 2::::'+ contactsToUpdated);
							} 
							//Check for Case's ContactId is alreaday exists into map of submittedToVendors
							//and Last Submitted before yesterday and count today's not zero
							else if(submittedToVendors.containsKey(newOrder.ContactId)
										&& submittedToVendors.get(newOrder.ContactId).Last_Submitted_Date_Time__c != null 
										&& submittedToVendors.get(newOrder.ContactId).Last_Submitted_Date_Time__c.date() < Date.today().addDays(-1)
										&& submittedToVendors.get(newOrder.ContactId).Submitted_Count_Today__c != 0) {
								
								//Check contact's Id is already a key of map
								if(contactsToUpdated.containsKey(newOrder.ContactId)) {
									
									//counter incremented by 1
									contactsToUpdated.get(newOrder.ContactId).Submitted_Count_Today__c += 1;
								} else {
									
									//populate the map at initial level
									contactsToUpdated.put(newOrder.ContactId, new Contact(Id= newOrder.ContactId, Submitted_Count_Today__c = 1, 
																							Submitted_Count_Yesterday__c = 0));
								}
								
								//System.debug('Step 3::::'+ contactsToUpdated);
							}
						}
					}
				}
			}
		}

		//Check for size of list
		if(contactsToUpdated.size() > 0) {
		
			//Loop through contactsToUpdated keyset
			for(Id contactId : contactsToUpdated.keySet()) {
				
				//reset the Last Submitted Date/Time on contact by now
				contactsToUpdated.get(contactId).Last_Submitted_Date_Time__c = Datetime.now();
			}
			
			//update the Contacts
			database.update(contactsToUpdated.values(),false);
			BYPASS_SUBMIT_COUNT_UPDATES = false;
		}
	}
}