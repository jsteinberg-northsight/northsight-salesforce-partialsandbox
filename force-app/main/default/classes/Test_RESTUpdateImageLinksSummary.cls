@isTest(seeAllData=false)
private class Test_RESTUpdateImageLinksSummary {
///**
// *  Purpose         :   This is used for testing and covering RESTUpdateImageLinksSummary functionality.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   02/05/2014
// *
// *  Current Version :   V1.2
// *
// *  Revision Log    :   V1.0 - Created
// *                      V1.1 - Modified - Padmesh Soni (06/18/2014) - Support Case Record Type
// *                      V1.2 - Modified - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
// *
// *  Coverage        :   90% - V1.0
// *                      89% - V1.1
// *                      100% - V1.2
// **/
//    
//    //Test method to test Update ImageLinksSummary on WorkOrder
//    static testMethod void testUpdateImageLinksSummary() {
//        
//        //Query record of Record Type of Account and Case objects
//        //Padmesh Soni (06/18/2014) - Support Case Record Type
//        //Query one more record type of Case Object "SUPPORT"    
//        /*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client'
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(3, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accounts = new List<Account>();
//        accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accounts;
//        
//        //Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//		//Fetch record type of Account Sobject
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client' 
//													AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//		
//    	//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//    	//Create a test instance for Account sobject
//		Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//        //List to hold Case records
//        List<Case> workOrders = new List<Case>();
//        workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today(), 
//        							Status = Constants.CASE_STATUS_OPEN,
//        							
//        							//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//    								//new field populated 
//									Client_Name__c = account.Id));
//        workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1), 
//        							Status = Constants.CASE_STATUS_OPEN,
//        							
//        							//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//    								//new field populated 
//									Client_Name__c = account.Id));
//        workOrders.add(new Case(RecordTypeId = recordTypes[2].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(2), 
//        							Status = Constants.CASE_STATUS_OPEN,
//        							
//        							//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//    								//new field populated 
//									Client_Name__c = account.Id));
//        
//        //insert case records
//        insert workOrders;
//        
//        //Query result of Case(Work Orders)
//        workOrders = [SELECT CaseNumber FROM Case WHERE Id IN: workOrders];
//        
//        //JSON formatted string
//        //Padmesh Soni (06/18/2014) - Support Case Record Type
//        //Append string with new records
//        String summaryData = '{'
//                                +'"WorkOrders": ['
//                                    +'{'
//                                            +'"WorkOrderNumber": "' + workOrders[0].CaseNumber + '",'
//                                            +'"Image_Count__c": 1,'
//                                            +'"Image_Latency__c": 1,'
//                                            +'"Mobile_Data_Off__c": false,'
//                                            +'"ImageLinks_Summary__c": ['
//                                                +'{'
//                                                    +'"Id": 2,'
//                                                    +'"ImageUrl__c": "http://nspdl.elasticbeanstalk.com/viewImage/12122222/234234234.jpg",'
//                                                    +'"Survey_Id__c": "GC22_001-v7",'
//                                                    +'"Survey_Question_Id__c": "Number_of_Bags_Removed__c",'
//                                                    +'"Pruvan_EvidenceType__c": "Survey",'
//                                                    +'"GPS_Timestamp__c": "2014-01-31T13:05:24Z",'
//                                                    +'"GPS_Location_Latitude__c": 23.253321,'
//                                                    +'"GPS_Location_Longitude__c": -100.030122,'
//                                                    +'"Pruvan_gpsAccuracy__c": 7,'
//                                                    +'"Distance_from_Property_in_Ft__c": 24.5,'
//                                                    +'"Notes":"This is a picture of the lawn."'
//                                                +'}'
//                                            +'],'
//                                            +'"All_Images_Received__c": true'
//                                    +'},'
//                                    +'{'
//                                        +'"WorkOrderNumber": "'+ workOrders[1].CaseNumber + '",'
//                                        +'"Image_Count__c": 1,'
//                                        +'"Image_Latency__c": 1,'
//                                        +'"Mobile_Data_Off__c": false,'
//                                        +'"ImageLinks_Summary__c": ['
//                                            +'{'
//                                                +'"Id": 3,'
//                                                +'"ImageUrl__c": "http://nspdl.elasticbeanstalk.com/viewImage/12122223/234234235.jpg",'
//                                                +'"Survey_Id__c": "GC22_001-v7",'
//                                                +'"Survey_Question_Id__c": "Number_of_Bags_Removed__c",'
//                                                +'"Pruvan_EvidenceType__c": "Survey",'
//                                                +'"GPS_Timestamp__c": "2014-01-31T13:05:24Z",'
//                                                +'"GPS_Location_Latitude__c": 23.253321,'
//                                                +'"GPS_Location_Longitude__c": -100.030122,'
//                                                +'"Pruvan_gpsAccuracy__c": 7,'
//                                                +'"Distance_from_Property_in_Ft__c": 24.5,'
//                                                +'"Notes":"This is a picture of the lawn."'
//                                            +'}'
//                                        +'],'
//                                        +'"All_Images_Received__c": true'
//                                    +'},'
//                                    +'{'
//                                        +'"WorkOrderNumber": "'+ workOrders[2].CaseNumber + '",'
//                                        +'"Image_Count__c": 2,'
//                                        +'"Image_Latency__c": 2,'
//                                        +'"Mobile_Data_Off__c": false,'
//                                        +'"ImageLinks_Summary__c": ['
//                                            +'{'
//                                                +'"Id": 3,'
//                                                +'"ImageUrl__c": "http://nspdl.elasticbeanstalk.com/viewImage/12122223/234234235.jpg",'
//                                                +'"Survey_Id__c": "GC22_001-v7",'
//                                                +'"Survey_Question_Id__c": "Number_of_Bags_Removed__c",'
//                                                +'"Pruvan_EvidenceType__c": "Survey",'
//                                                +'"GPS_Timestamp__c": "2014-01-31T13:05:24Z",'
//                                                +'"GPS_Location_Latitude__c": 23.253321,'
//                                                +'"GPS_Location_Longitude__c": -100.030122,'
//                                                +'"Pruvan_gpsAccuracy__c": 7,'
//                                                +'"Distance_from_Property_in_Ft__c": 24.5,'
//                                                +'"Notes":"This is a picture of the lawn."'
//                                            +'},'
//                                            +'{'
//                                                +'"Id": 4,'
//                                                +'"ImageUrl__c": "http://nspdl.elasticbeanstalk.com/viewImage/12122223/234234236.jpg",'
//                                                +'"Survey_Id__c": "GC22_001-v7",'
//                                                +'"Survey_Question_Id__c": "Number_of_Bags_Removed__c",'
//                                                +'"Pruvan_EvidenceType__c": "Survey",'
//                                                +'"GPS_Timestamp__c": "2014-01-31T13:05:24Z",'
//                                                +'"GPS_Location_Latitude__c": 23.253321,'
//                                                +'"GPS_Location_Longitude__c": -100.030122,'
//                                                +'"Pruvan_gpsAccuracy__c": 7,'
//                                                +'"Distance_from_Property_in_Ft__c": 24.5,'
//                                                +'"Notes":"This is a picture of the lawn."'
//                                            +'}'
//                                        +'],'
//                                        +'"All_Images_Received__c": true'
//                                    +'}'
//                                +']'
//                            +'}';
//        
//        //Test starts here
//        Test.startTest();
//        
//        // set up the request object
//        RestContext.request = new RestRequest();
//        
//        //Setting request URI of RestContext
//        RestContext.request.requestURI = '/RESTUpdateImageLinksSummary';
//        
//        //Setting body of RestContext
//        RestContext.request.requestBody = Blob.valueOf(summaryData);
//        
//        // set up the response object
//        RestContext.response = new Restresponse();
//        
//        string response = '{"Errors":""}';
//                
//        //Setting response body of Errors as blob  
//        RestContext.response.responseBody = Blob.valueOf(response);
//        RestContext.response.statusCode = 200;
//
//        //Call webservice method of REST class
//        RESTUpdateImageLinksSummary.updateImageLinksSummary();
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //Query result of Case(Work Orders)
//        //Padmesh Soni (06/18/2014) - Support Case Record Type
//        //Query record of Record Type of Case object
//        workOrders = [SELECT ImageLinks_Summary__c FROM Case WHERE RecordTypeId !=: recordTypes[2].Id AND Id IN: workOrders];
//        
//        //Loop through work Order queried records
//        for(Case workOrder : workOrders) {
//            
//            //assert statements here
//            System.assert(workOrder.ImageLinks_Summary__c != null);
//        }
//    }
}