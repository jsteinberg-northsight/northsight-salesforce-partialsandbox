@isTest(seeAllData=false)
private class Test_BatchDeletePruvanGetWorkOrderLogs {
//
///**
// *  Purpose         :   This is used for testing and covering BatchDeleteImageLinks class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/06/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Required Maintenance: Unit Test Improvements
// *	
// *	Coverage		:	85%
// **/
// 	
//    //test method added to check the functionality with code coverage
//    static testMethod void myUnitTest() {
//        
//        //List to hold test records of Pruvan_GetWorkOrder_Log__c
//        List<Pruvan_GetWorkOrder_Log__c> pruvanGetWorkOrderLogs = new List<Pruvan_GetWorkOrder_Log__c>();
//        
//        //Loop through till count 10
//        for(integer i=0; i < 10; i++) {
//        	
//        	//add new instance of Pruvan_GetWorkOrder_Log__c into list
//        	pruvanGetWorkOrderLogs.add(new Pruvan_GetWorkOrder_Log__c(Name = 'Test'+ i, Log_Data__c = 'Testing is going on.'));
//        }
//        
//        //insert Pruvan Work Order logs here
//        insert pruvanGetWorkOrderLogs;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Execute batch here
//        Database.executeBatch(new BatchDeletePruvanGetWorkOrderLogs(), 200);
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //Query result of Pruvan_GetWorkOrder_Log__c records
//        pruvanGetWorkOrderLogs = [SELECT Id FROM Pruvan_GetWorkOrder_Log__c];
//        
//        //Assert statement here
//        System.assertEquals(0, pruvanGetWorkOrderLogs.size());
//    }
}