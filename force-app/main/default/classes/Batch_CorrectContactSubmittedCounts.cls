/**
 *  Purpose         :   This is batch for correcting Submitted Count today and yesterday on Contact.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   09/24/2014
 *
 *  Current Version :   V1.1
 *
 *  Revision Log    :   V1.0 - Created - OAM-75: Submitted Today/Yesterday Not Updating
 **/
global class Batch_CorrectContactSubmittedCounts implements Database.Batchable<SObject> {
    
    //Variable to hold recordtype list
    global static Map<Id, RecordType> recordTypesToExclude = new Map<Id, RecordType>([SELECT Id, Name FROM RecordType 
                                                                                        WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
                                                                                        AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true]);
    
    //Start method
    global Database.Querylocator start(Database.BatchableContext BC) {
        
        //return query result
        return Database.getQueryLocator([SELECT Id, Last_Submitted_Date_Time__c, Submitted_Count_Today__c, Submitted_Count_Yesterday__c FROM Contact
                                            WHERE (DAY_ONLY(Last_Submitted_Date_Time__c) < TODAY AND Submitted_Count_Today__c != 0)
                                            OR (DAY_ONLY(Last_Submitted_Date_Time__c) = TODAY AND Submitted_Count_Today__c = 0)
                                            OR (DAY_ONLY(Last_Submitted_Date_Time__c) = YESTERDAY AND Submitted_Count_Yesterday__c = 0)
                                            OR (DAY_ONLY(Last_Submitted_Date_Time__c) < YESTERDAY AND Submitted_Count_Yesterday__c !=0)]);
    }
    
    //Execute method
    global void execute(Database.BatchableContext BC, List<Contact> contacts) {
        
        //Sets to hold contact Ids
        Set<Id> contactForTodayCount = new Set<Id>();
        Set<Id> contactForYesterdayCount = new Set<Id>();
        
        //Loop through batch queried records
        for(Contact conRec : contacts) {
            
            contactForYesterdayCount.add(conRec.Id);
        }
        
        contactForTodayCount.addAll(contactForYesterdayCount);
        
        //List to hold contact to be updated
        Map<Id, Contact> contactsToUpdated = new Map<Id, Contact>();
        
        //List to hold Aggregate result of Case
        List<AggregateResult> yesterdaySubmittedOrders = [SELECT ContactId, Count(Id) submittedOrders FROM Case WHERE 
                                                            RecordTypeId NOT IN: recordTypesToExclude.keyset()
                                                            AND Status != 'Canceled' AND Approval_Status__c NOT IN ('Not Started','Rejected') 
                                                            AND ContactId IN: contactForYesterdayCount 
                                                            AND DAY_ONLY(Pre_Pending_Date_Time__c) = YESTERDAY 
                                                            GROUP BY ContactId ];
        
        //List to hold Aggregate result of Case
        List<AggregateResult> todaySubmittedOrders = [SELECT ContactId, Count(Id) submittedOrders FROM Case WHERE 
                                                            RecordTypeId NOT IN: recordTypesToExclude.keyset()
                                                            AND Status != 'Canceled' AND Approval_Status__c NOT IN ('Not Started','Rejected') 
                                                            AND ContactId IN: contactForTodayCount 
                                                            AND DAY_ONLY(Pre_Pending_Date_Time__c) = TODAY 
                                                            GROUP BY ContactId ];
        
        //Check for size
        if(yesterdaySubmittedOrders.size() > 0) {
            
            //Loop through aggregate results
            for(AggregateResult ar : yesterdaySubmittedOrders){
                
                //Get Contact Id from aggregate result
                Id conId = (Id)ar.get('ContactId');
                
                //Getting Submitted Order Count yesterday
                Integer submittedCount = (Integer)ar.get('submittedOrders');
                
                //Check for not null
                if(conId != null && contactForYesterdayCount.contains(conId)){
                    
                    //add Contact into list to be updated
	                contactsToUpdated.put(conId, new Contact(Id = conId, Submitted_Count_Yesterday__c = submittedCount));
                    
                    //remove the processed Contact from the set 
                    contactForYesterdayCount.remove(conId);
                }
            }
            
            //Check for remaining Contacts which are not associated with 
            //any Case update to or from "Pre-Pending" Approval Status and 
            //not getting Aggregate counts in aggregate result then update 
            //all Contact's Submitted Count Today with 0  
            if(contactForYesterdayCount.size() > 0) {
                
                //Loop through set of contactForYesterdayCount
                for(Id conId : contactForYesterdayCount) {
                
                    //add Contact into list to be updated
	                contactsToUpdated.put(conId, new Contact(Id = conId, Submitted_Count_Yesterday__c = 0));
                }   
            }
        } else {
            
            //Loop through set of contactForYesterdayCount
            for(Id conId : contactForYesterdayCount) {
            
                //add Contact into list to be updated
                contactsToUpdated.put(conId, new Contact(Id = conId, Submitted_Count_Yesterday__c = 0));
            }
        }
        
        //Check for size
        if(todaySubmittedOrders.size() > 0) {
            
            //Loop through aggregate results
            for(AggregateResult ar : todaySubmittedOrders){
                
                //Get Contact Id from aggregate result
                Id conId = (Id)ar.get('ContactId');
                
                //Getting Submitted Order Count Today
                Integer submittedCount = (Integer)ar.get('submittedOrders');
                
                //Check for not null
                if(conId != null && contactForTodayCount.contains(conId)){
                    
                    //Check if contact is already processed into previous loop of yesterday counts
                	if(contactsToUpdated.containsKey(conId)) {
	                	
	                    contactsToUpdated.get(conId).Submitted_Count_Today__c = submittedCount;
                	}else {
                    
	                    //add Contact into list to be updated
	                    contactsToUpdated.put(conId, new Contact(Id = conId, Submitted_Count_Today__c = submittedCount));
	                }
	                
                    //remove the processed Contact from the set 
                    contactForTodayCount.remove(conId);
                }
            }
            
            //Check for remaining Contacts which are not associated with 
            //any Case update to or from "Pre-Pending" Approval Status and 
            //not getting Aggregate counts in aggregate result then update 
            //all Contact's Submitted Count Today with 0  
            if(contactForTodayCount.size() > 0) {
                
                //Loop through set of contactForTodayCount
                for(Id conId : contactForTodayCount) {
                	
                	//Check if contact is already processed into previous loop of yesterday counts
                	if(contactsToUpdated.containsKey(conId)) {
	                	
	                    contactsToUpdated.get(conId).Submitted_Count_Today__c = 0;
                	}else {
                    
	                    //add Contact into list to be updated
	                    contactsToUpdated.put(conId, new Contact(Id = conId, Submitted_Count_Today__c = 0));
	                }
                }   
            }
        } else {
            
            //Loop through set of contactForTodayCount
            for(Id conId : contactForTodayCount) {
            	
            	//Check if contact is already processed into previous loop of yesterday counts
                if(contactsToUpdated.containsKey(conId)) {
                	
                    contactsToUpdated.get(conId).Submitted_Count_Today__c = 0;
            	}else {
                
                    //add Contact into list to be updated
                    contactsToUpdated.put(conId, new Contact(Id = conId, Submitted_Count_Today__c = 0));
                }
            }
        }
        
        //Check for size of contact list
        if(contactsToUpdated.size() > 0)
            database.update(contactsToUpdated.values(), false);
    }
    
    // Finish method
    global void finish(Database.BatchableContext BC) {
        
    }
}