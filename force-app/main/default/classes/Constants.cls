/**
 *  Purpose         :   This class is to hold the constant values.
 *
 *  Create By       :   Padmesh Soni
 *
 *  Created Date    :   12/11/2013
 *
 *  Current Version :   V1.0
 **/
public with sharing class Constants {
    
    //Variable to hold Zillow Configuration defaults custom setting values
    public static Zillow_Configuration__c CONFIG_DEFAULT {

        get {
        
            //Get Zillow Configuration Defaults values
            Zillow_Configuration__c configDefaults = Zillow_Configuration__c.getOrgDefaults();
            
            //Check if Zillow Configuration Defaults is already there or not
            if(configDefaults == null) {
                configDefaults = new Zillow_Configuration__c();
            }
            
            //Return Zillow Configuration Defaults
            return configDefaults;
        }
    }
    
    //Constants values for Account sobject
    public static final String ACCOUNT_SOBJECTTYPE = 'Account';
    //public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER = 'Customer';//Customer no longer exists as a DeveloperName in production
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER = 'Client';//Client replaces Customer as the DeveloperName to use
    
    //Constants values for Attachment sobject
    public static final String CASE_SOBJECTTYPE = 'Case';
    public static final String CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER = 'NEW_ORDER';
    public static final String CASE_STATUS_OPEN = 'Open';
    
    //Constants values for Work Order(Case) sobject
    public static final String CASE_WORKORDERS = 'WorkOrders';
    public static final String CASE_FIELD_CASENUMBER = 'WorkOrderNumber';
    public static final String CASE_FIELD_IMAGELINKS_SUMMARY = 'ImageLinks_Summary__c';
    public static final String CASE_CLIENT_SG = 'SG';
    
    //Padmesh Soni (06/17/2014) - Support Case Record Type
    //New constant added
    public static final String CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT = 'SUPPORT';
    
    //Padmesh Soni (06/19/2014) - Support Case Record Type
    //New constant added for list of Record Type which are associated with Customers
    public static final List<String> CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES = new List<String>{'SUPPORT'};
    
    //Constants values for ImageLink object
    public static final String IMAGELINK_FIELD_GPS_TIMESTAMP = 'GPS_Timestamp__c';
    
    //Constants values for Client Commitment object
    public static final String CLIENT_COMMITMENT_SYNC_STATUS_UNSENT = 'Unsent';
    public static final String CLIENT_COMMITMENT_SYNC_STATUS_IN_PROGRESS = 'In Progress';
    public static final String CLIENT_COMMITMENT_SYNC_STATUS_SENT = 'Sent';
    public static final String CLIENT_COMMITMENT_SYNC_STATUS_FAILED = 'Failed';
}