@isTest(seeAllData=false)
private class Test_ActivatePortalUser {
///**
// *  Description     :   This is test class for testing of ActivatePortalUser class functionality. 
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   10/30/2014
// *
// *  Current Version :   V1.0
// *
// *  Revisiion Logs  :   V1.0 - Created
// *      
// *  Code Coverage   :   V1.0 - 100%
// **/
// 
//    //Method to test functionality of ActivationPortalUser class
//    static /*testMethod*/ void testActivationUser() {
//        
//        //query result of Profile
//        List<Profile> profiles = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' OR Name = 'Partner Community - Vendors' ORDER BY Name];
//        
//        //assert statement
//        System.assertEquals(2, profiles.size());
//        
//        //List to hold all account
//        List<Account> accounts = new List<Account>();
//        //populate list of Accounts
//        accounts.add(new Account(Name = 'Test Vendor Community',
//            RecordTypeId = '0124000000011Q2'));
//        
//        //Loop through countData time
//        for(integer i = 1; i <= 2; i++) {
//            
//            //populate list of Accounts
//            accounts.add(new Account(Name = 'Test Account'+i,RecordTypeId = '0124000000011Q2'));
//        }
//        
//        insert accounts;
//        
//        accounts[0].IsPartner = true;
//        update accounts[0];
//        
//        List<Contact> contacts = new List<Contact>();
//        Id vendorRT = [SELECT Id FROM RecordType WHERE Name = 'Vendor' AND SobjectType = 'Contact'].Id;
//        //Loop through countData time
//        for(integer i = 0; i < 3; i++) {
//        
//            //create a new contact
//            contacts.add(new Contact(LastName = 'Tester'+i, AccountId = accounts[i].Id, Pruvan_Username__c = 'NSBobTest', Status__c = '3 - Active',
//                                        Grass_Cut_Cap__c = 3, Order_Count__c = 0, Email = 'test'+i+'@'+Math.random()+'.com',
//                                        MailingStreet = '301 Front St',
//                                        MailingCity = 'Nome',
//                                        MailingState = 'AK',
//                                        MailingPostalCode = '99762',
//                                        OtherStreet = '301 Front St',
//                                        OtherCity = 'Nome',
//                                        OtherState = 'AK',
//                                        OtherPostalCode = '99762',
//                                        Date_Active__c = Date.today(),                                        
//                                        Activated_By__c = 'z-Other',
//                                        Other_Address_Geocode__Latitude__s = 64.497669,
//                                        Other_Address_Geocode__Longitude__s = -165.41005,
//                                        REO_Grass_Cut__c = 25.00,
//                                        Pay_Terms__c = 'Net 30',
//                                        Trip_Charge__c = 15.00,
//                                        Shrub_Trim__c = 0.00,
//                                        RecordTypeId = vendorRt));
//        }   
//        contacts.add(new Contact(LastName = 'Tester', AccountId = accounts[2].Id, Pruvan_Username__c = 'NSBobTest', Status__c = '3 - Active',
//                                        Grass_Cut_Cap__c = 3, Order_Count__c = 0,
//                                        MailingStreet = '301 Front St',
//                                        MailingCity = 'Nome',
//                                        MailingState = 'AK',
//                                        MailingPostalCode = '99762',
//                                        OtherStreet = '301 Front St',
//                                        OtherCity = 'Nome',
//                                        OtherState = 'AK',
//                                        OtherPostalCode = '99762',
//                                        Date_Active__c = Date.today(),                                        
//                                        Activated_By__c = 'z-Other',
//                                        Other_Address_Geocode__Latitude__s = 64.497669,
//                                        Other_Address_Geocode__Longitude__s = -165.41005,
//                                        REO_Grass_Cut__c = 25.00,
//                                        Pay_Terms__c = 'Net 30',
//                                        Trip_Charge__c = 15.00,
//                                        Shrub_Trim__c = 0.00,
//                                        RecordTypeId = vendorRt));
//        insert contacts;
//        
//        //create a new user with the profile
//        User usr = new User(alias = 'standt', contactId = contacts[0].Id, email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8',
//                                lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = profiles[0].Id,
//                                timezonesidkey = 'America/Los_Angeles', username = 'stand'+Math.random() +'@test.com', isActive = true);
//        insert usr;
//        
//        //Test starts here
//        Test.startTest();
//        
//        ApexPages.StandardController sc = new ApexPages.StandardController(contacts[0]);
//        
//        try {
//            ActivatePortalUser controller = new ActivatePortalUser(sc);
//        }catch(Exception e) {
//            
//            System.assertEquals(e.getMessage(), Label.ACTIVATE_PORTAL_USER_WARNING_CONTACT.replace('USRPROFILE', profiles[0].Name));    
//        }
//        
//        sc = new ApexPages.StandardController(contacts[1]);
//        ActivatePortalUser controller = new ActivatePortalUser(sc);
//        controller.changeContactAccount();
//        
//        System.runAs(usr) {
//            controller.activateNormalVendorUser();
//        }
//        
//        List<User> users = [SELECT Id FROM User WHERE Contact.AccountId =: accounts[0].Id];
//        System.assertEquals(2, users.size());
//        
//        sc = new ApexPages.StandardController(contacts[2]);
//        controller = new ActivatePortalUser(sc);
//        controller.changeContactAccount();
//        
//        System.runAs(usr) {
//            controller.activateOrderSelectVendorUser();
//        }
//        
//        users = [SELECT Id FROM User WHERE Contact.AccountId =: accounts[0].Id];
//        //System.assertEquals(3, users.size());
//        
//        sc = new ApexPages.StandardController(contacts[2]);
//        controller = new ActivatePortalUser(sc);
//        
//        System.runAs(usr) {
//            
//            try {
//                
//                controller.activateOrderSelectVendorUser();
//            }catch(Exception e) {
//                System.assertEquals('', e.getMessage());    
//            }
//        }
//        
//        System.runAs(usr) {
//            
//            try {
//                
//                controller.activateNormalVendorUser();
//            }catch(Exception e) {
//                System.assertEquals('', e.getMessage());    
//            }
//        }
//        
//        //Test stops here
//        Test.stopTest();
//    }
}