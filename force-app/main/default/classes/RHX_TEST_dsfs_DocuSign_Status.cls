@isTest(SeeAllData=true)
public class RHX_TEST_dsfs_DocuSign_Status {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM dsfs__DocuSign_Status__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new dsfs__DocuSign_Status__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}