@isTest(seeAllData=false)
private class Test_WorkOrderExportPrint {
///**
// *  Purpose         :   This is used for testing and covering WorkOrderExportPrint class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/18/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Padmesh Soni(06/18/2014) - Support Case Record Type
// *	
// *	Coverage		:	100% - V1.0
// **/
// 
// 	//Test method is used to test the functionality and coverage of code.
//    static testMethod void myUnitTest() {
//    	
//    	//Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client'
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(3, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	//insert Account records here
//    	insert accounts;
//		
//		//Create a test record for Property
//		Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//		insert property;
//		
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Status = Constants.CASE_STATUS_OPEN));
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, 
//    								Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN, Scheduled_Date_Override__c = Date.today()));
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[2].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1), Status = Constants.CASE_STATUS_OPEN));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//    	//Test starts here
//    	Test.startTest();
//    	
//    	//Page instance
//    	PageReference p = Page.WorkOrderPrint;
//    	
//    	//put the parameters
//		p.getParameters().put('wo_list',workOrders[0].Id+','+workOrders[1].Id+','+workOrders[2].Id);
//		
//		//Set the current page
//		System.test.setCurrentPage(p);
//		
//		//Create an instance of controller
//		WorkOrderExportPrint controller = new WorkOrderExportPrint();
//		
//		//assert statement here
//		System.assertEquals(2, controller.export_list.size());
//		
//		//Test stops here
//		Test.stopTest();
//    }
}