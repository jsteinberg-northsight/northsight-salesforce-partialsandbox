public without sharing class InspectionReportController {
	public String accountId { get; set; }
	public Account accountRecord { get; set; }
	public Case dateFilters { get; set; }
	public List<String> workOrdered { get; set; }
	public List<SelectOption> workOrderedItems { get; set; }
	public transient Map<String, WorkOrderObject> workOrders { get; set; }
	public Integer workOrdersSize { get; set; }
	public String csvOutput { get; set; }
	public List<String> csvColumnHeaders { get; set; }
	public String reportName { get; set; }

	public class WorkOrderObject {
		public Case priorInspection { get; set; }
		public Case currentInspection { get; set; }
		public Id propertyId { get; set; }
		public Integer daysBetweenInspections { get; set; }

		public WorkOrderObject(Case pPriorInspection, Case pCurrentInspection, Id pPropertyId) {
			priorInspection = pPriorInspection;
			currentInspection = pCurrentInspection;
			propertyId = pPropertyId;
			daysBetweenInspections = 0;
		}
	}

	public InspectionReportController() {
		//Property parameter must be included
		accountId = ApexPages.currentPage().getParameters().get('accountId');
		if (!isValidAccountRecord(accountId)) {
			accountId = null;
			return;
		}
		accountRecord = getAccountRecord(accountId);
		dateFilters = setDateRangeFilter(
			new Case(
				Client_Accepted_Date__c = Date.today().addMonths(-1)
			)
		);
		workOrdered = new List<String>();
		workOrderedItems = getWorkOrderedItems();

		csvColumnHeaders = new List<String>{'Loan #','Prior Inspection Type', 'Prior Inspection Number', 'Prior Inspection Date', 'Current Inspection Type', 'Current Inspection Number', 'Current Inspection Date', 'Days Between'};
		workOrders = null;
		workOrdersSize = 0;
		csvOutput = '';
	}

	private static List<SelectOption> getWorkOrderedItems() {
		List<SelectOption> options = new List<SelectOption>();
		for (Schema.PicklistEntry currentPicklistValue : Schema.Case.Work_Ordered__c.getDescribe().getPicklistValues()) {
			options.add(new SelectOption(currentPicklistValue.getValue(), currentPicklistValue.getValue()));
		}
		return options;
	}

	public void updateDateRange() {
		dateFilters = setDateRangeFilter(dateFilters);
	}

	@TestVisible
	private static Account getAccountRecord(String pAccountId) {
		if (String.isBlank(pAccountId)) {
			return null;
		}

		return [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, Client_Code__c FROM Account WHERE Id = :pAccountId];
	}

	@TestVisible
	private static Case setDateRangeFilter(Case pDateFilters) {
		Case dateFilters = new Case();
		Integer numberOfDaysInMonth = Date.daysInMonth(
			pDateFilters.Client_Accepted_Date__c.year(),
			pDateFilters.Client_Accepted_Date__c.month()
		);

		//Use as Start Date Filter just to get the Datepicker
		dateFilters.Client_Accepted_Date__c = Date.newInstance(
			pDateFilters.Client_Accepted_Date__c.year(),
			pDateFilters.Client_Accepted_Date__c.month(),
			1
		);
		//User as End Date Filter just to get the Datepicker
		dateFilters.Client_Canceled_Date__c = Date.newInstance(
			pDateFilters.Client_Accepted_Date__c.year(),
			pDateFilters.Client_Accepted_Date__c.month(),
			numberOfDaysInMonth
		);
		return dateFilters;
	}

	@TestVisible
	private static Boolean isValidAccountRecord(String pAccountId) {
		try {
			if (String.isBlank(pAccountId)) {
				ApexPages.addMessage(
					new ApexPages.message(
						ApexPages.severity.ERROR,
						'Account Id Required'
					)
				);
				return false;
			}

			Id accountId = pAccountId;
			if (accountId.getSobjectType() != Schema.Account.SObjectType) {
				ApexPages.addMessage(
					new ApexPages.message(
						ApexPages.severity.ERROR,
						'Invalid Account Id'
					)
				);
				return false;
			}
			return true;
		} catch (Exception e) {
			ApexPages.addMessage(
				new ApexPages.message(
					ApexPages.severity.ERROR,
					'There was a problem with the parameter for Account Id'
				)
			);
			return false;
		}
	}

	public void getWorkOrdersButton() {
		List<Case> cases = getWorkOrders(
				accountId,
				dateFilters.Client_Accepted_Date__c,
				dateFilters.Client_Canceled_Date__c,
				workOrdered
		);
		workOrders = processWorkOrders(
			cases,
			dateFilters.Client_Accepted_Date__c,
			dateFilters.Client_Canceled_Date__c
		);
		cases = null;
		system.debug(workOrders.size());
		workOrdersSize = workOrders.size();
	}

	@TestVisible
	private static List<Case> getWorkOrders(String pAccountId, Date pStartDate, Date pEndDate, List<String> pWorkOrderList) {
		if (String.isBlank(pAccountId) || pStartDate == null || pEndDate == null || pWorkOrderList == null) {
			return null;
		}

		return [SELECT
		            Id,
		            Date_Serviced__c,
		            Work_Ordered__c,
		            Reference_Number__c,
		            Geocode_Cache__c,
		            Geocode_Cache__r.Loan_Number__c
		        FROM
		            Case
		        WHERE
		        	Geocode_Cache__r.Client_Name__c = :pAccountId AND
		            Date_Serviced__c <= :pEndDate AND
		            Work_Ordered__c IN :pWorkOrderList
		        ORDER BY
		            Geocode_Cache__r.Loan_Number__c ASC,
		            Date_Serviced__c DESC];
	}

	@TestVisible
	private static Map<String, WorkOrderObject> processWorkOrders(List<Case> pWorkOrders, Date pStartDate, Date pEndDate) {
		Map<String, WorkOrderObject> workOrders = new Map<String, WorkOrderObject>();
		if (pWorkOrders == null || pWorkOrders.isEmpty()) {
			return workOrders;
		}

		for (Case currentWorkOrder : pWorkOrders) {
			//If no loan, or loan already has previous and current set, skip the record
			if (String.isBlank(currentWorkOrder.Geocode_Cache__r.Loan_Number__c) ||
				(workOrders.containsKey(currentWorkOrder.Geocode_Cache__r.Loan_Number__c) &&
				workOrders.get(currentWorkOrder.Geocode_Cache__r.Loan_Number__c).priorInspection.Id != null &&
				workOrders.get(currentWorkOrder.Geocode_Cache__r.Loan_Number__c).currentInspection.Id != null)) {
				continue;
			}

			if (!workOrders.containsKey(currentWorkOrder.Geocode_Cache__r.Loan_Number__c)) {
				//first work order must be in target date range
				if (currentWorkOrder.Date_Serviced__c >= pStartDate && currentWorkOrder.Date_Serviced__c <= pEndDate) {
					workOrders.put(
						currentWorkOrder.Geocode_Cache__r.Loan_Number__c,
						new WorkOrderObject(
							new Case(),
							currentWorkOrder,
							currentWorkOrder.Geocode_Cache__c
						)
					);
					continue;
				}
			}

			//check if first work order assigned, but missing 2nd
			if (workOrders.containsKey(currentWorkOrder.Geocode_Cache__r.Loan_Number__c) && workOrders.get(currentWorkOrder.Geocode_Cache__r.Loan_Number__c).priorInspection.Id == null) {
				workOrders.get(currentWorkOrder.Geocode_Cache__r.Loan_Number__c).priorInspection = currentWorkOrder;
				Date startDate = workOrders.get(currentWorkOrder.Geocode_Cache__r.Loan_Number__c).priorInspection.Date_Serviced__c;
				Date endDate = workOrders.get(currentWorkOrder.Geocode_Cache__r.Loan_Number__c).currentInspection.Date_Serviced__c;
				workOrders.get(currentWorkOrder.Geocode_Cache__r.Loan_Number__c).daysBetweenInspections = startDate.daysBetween(endDate);
			}
		}
		return workOrders;
	}

	public PageReference exportCsvButton() {
		String day = String.valueOf(dateFilters.Client_Accepted_Date__c.day());
		day = (day.length() == 1) ? '0' + day : day;
		String month = String.valueOf(dateFilters.Client_Accepted_Date__c.month());
		month = (month.length() == 1) ? '0' + month : month;
		String year = String.valueOf(dateFilters.Client_Accepted_Date__c.year());
		reportName = accountRecord.Client_Code__c + '_' + month + day + year + '_InspectionReport.csv';
		PageReference exportCsv = Page.InspectionReportCsv;
		getWorkOrdersButton();
		csvOutput = convertWorkOrdersToCsv(workOrders, csvColumnHeaders);
		workOrders = null;
		exportCsv.setRedirect(false);
		return exportCsv;
	}

	@TestVisible
	private static String convertWorkOrdersToCsv(Map<String, WorkOrderObject> pWorkOrders, List<String> pCsvHeaders) {
		if (pWorkOrders == null || pWorkOrders.isEmpty()) {
			return '';
		}

		String csvOutput = String.join(pCsvHeaders, ',');
		csvOutput += '\r\n';
		for (String currentKey : pWorkOrders.keySet()) {
			csvOutput += currentKey + ',';
			csvOutput += (pWorkOrders.get(currentKey).priorInspection.Id == null) ? ',' : pWorkOrders.get(currentKey).priorInspection.Work_Ordered__c + ',';
			csvOutput += (pWorkOrders.get(currentKey).priorInspection.Id == null) ? ',' : pWorkOrders.get(currentKey).priorInspection.Reference_Number__c + ',';
			csvOutput += (pWorkOrders.get(currentKey).priorInspection.Id == null) ? ',' : pWorkOrders.get(currentKey).priorInspection.Date_Serviced__c.format() + ',';
			csvOutput += pWorkOrders.get(currentKey).currentInspection.Work_Ordered__c + ',';
			csvOutput += pWorkOrders.get(currentKey).currentInspection.Reference_Number__c + ',';
			csvOutput += pWorkOrders.get(currentKey).currentInspection.Date_Serviced__c.format() + ',';
			csvOutput += (pWorkOrders.get(currentKey).priorInspection.Id == null) ? '' : pWorkOrders.get(currentKey).daysBetweenInspections + '';
			csvOutput += '\r\n';
		}
		return csvOutput;
	}

	public PageReference backButton() {
		PageReference property = new PageReference('/' + accountId);
		return property;
	}

	public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
	}
}