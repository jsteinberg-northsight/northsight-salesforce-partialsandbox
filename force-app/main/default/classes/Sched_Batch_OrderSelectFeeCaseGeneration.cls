/**
 *  Purpose         :   This scheduler is used to schedule Batch_DeleteOldImageLinkFromCase class.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   07/09/2014
 *
 *  Current Version :   V1.0
 *
 *	Revision Log	:	V1.0 - Created - Padmesh Soni(07/09/2014) - Order Fee Generation Scheduled process
 **/
global class Sched_Batch_OrderSelectFeeCaseGeneration implements Schedulable {
	
	//execute method to execute the logic of batch processing 
    global void execute(SchedulableContext ctx) {
        
        //Batch executes here
        Database.executeBatch(new Batch_OrderSelectFeeCaseGeneration(), 200);
    }
}