public class RequestTriggerController {

    public static void setPropertyId(List<Request__c> pRequests) {
        if (pRequests == null || pRequests.isEmpty()) {
            return;
        }
        
        Map<String, Id> propertyOrLoan = new Map<String, Id>();
        for (Request__c currentRequest : pRequests) {
            if (!String.isBlank(currentRequest.Property_Number_or_Loan_Number__c)) {
            	propertyOrLoan.put(currentRequest.Property_Number_or_Loan_Number__c, null);
            }
        }
        if (propertyOrLoan.isEmpty()) {
            return;
        }
        
        propertyOrLoan = getPropertyByToken(propertyOrLoan);
        if (propertyOrLoan.isEmpty()) {
            return;
        }
        
        for (Request__c currentRequest : pRequests) {
            if (propertyOrLoan.containsKey(currentRequest.Property_Number_or_Loan_Number__c)) {
                currentRequest.Property__c = propertyOrLoan.get(currentRequest.Property_Number_or_Loan_Number__c);
            }
        }
    }
  
    private static Map<String, Id> getPropertyByToken(Map<String, Id> pPropertyOrLoan) {
        if (pPropertyOrLoan == null || pPropertyOrLoan.isEmpty()) {
            return new Map<String, Id>();
        }
        system.debug(pPropertyOrLoan.keySet());
        for (Geocode_Cache__c currentProperty : [SELECT
                                                Id,
                                                Name,
                                                Loan_Number__c
                                            FROM
                                                Geocode_Cache__c
                                            WHERE
                                                Name IN :pPropertyOrLoan.keySet() OR
                                                Loan_Number__c IN :pPropertyOrLoan.keySet()]) {
            if (pPropertyOrLoan.containsKey(currentProperty.Name)) {
            	pPropertyOrLoan.put(currentProperty.Name, currentProperty.Id);
                continue;
            }
            if (pPropertyOrLoan.containsKey(currentProperty.Loan_Number__c)) {
            	pPropertyOrLoan.put(currentProperty.Loan_Number__c, currentProperty.Id);
            }                                                                                
    	}
        return pPropertyOrLoan;
    }
}