@isTest(seeAllData=false)
private class Test_ClientOrderGridController {
//
///**
// *  Purpose         :   This is used to test controller for ClientOrderGridController class. 
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   11/26/2014
// *
// *  Current Version :   V1.0
// *
// *  Revision Log    :   V1.0 - Created - CCP-8: GridView | SlickGrid testing and provisioning
// *
// *	Code Coverage	:	V1.0 - 100%
// **/
//	
//	private static List<Account> accounts;
//    private static List<Contact> contacts;
//    private static Profile newProfile;
//    
//    private static void generateTestData(integer countData){
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP 
//        //create a new user with the Customer Portal Manager Standard profile
//        newProfile = [SELECT Id FROM Profile WHERE Name = 'Customer Portal Manager Standard'];
//        
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            /******** PRE-TEST SETUP create a new account *********/
//            accounts.add(new Account(Name = 'Tester Account' + i));
//        }
//        
//        insert accounts;
//        
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            //--------------------------------------------------------
//            //PRE-TEST SETUP
//            //create a new contact
//            contacts.add(new Contact(LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active'));
//        }   
//        
//        insert contacts;
//    }
//    
//	//Test method to test the functionality of ClientOrderGridController class
//    static testMethod void unitTesting() {
//        
//        //initialize variables
//        accounts = new List<Account>();
//        contacts = new List<Contact>();
//        
//        generateTestData(1);
//        
//        //Query record of Record Type of Account and Case objects
//        /*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client'
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(3, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsOnOrders = new List<Account>();
//        accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsOnOrders;
//        
//        Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//        insert property;
//        
//        //List to hold Case records
//        List<Case> workOrders = new List<Case>();
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX',
//                                    Department__c = 'Test', Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual'));
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX'));
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'LA',
//                                    Department__c = 'Test', Assignment_Program__c = 'Mannual'));
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'AR',
//                                    Department__c = 'Test1', Work_Ordered__c = 'Grass Initial Cut', Assignment_Program__c = 'OrderSelect'));
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[2].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Test', State__c = 'AR',
//                                    Department__c = 'Facebook', Work_Ordered__c = 'Testing', Assignment_Program__c = 'Test Prog'));
//        
//        //insert case records
//        insert workOrders;
//        
//        //Test starts here
//        Test.startTest();
//        
//        ClientOrderGridController controller = new ClientOrderGridController();
//        
//        //assert statement
//        System.assert(String.isNotBlank(controller.woSerialized));
//        //System.assert(String.isNotBlank(controller.pickListState));
//        System.assert(String.isNotBlank(controller.baseURL));
//        
//        String orderTable = '';
//        
//        for(integer i = 1; i < ClientOrderGridController.CHUNK_SIZE; i++ ) {
//        	
//        	orderTable += 'T';
//        }
//        
//        //Call SaveHTML method
//        String listViewObjectId = ClientOrderGridController.SaveHtml(orderTable);
//        
//        //assert statement
//        System.assert(String.isNotBlank(listViewObjectId));
//        
//        //Call SaveHTML method
//        Pagereference pgRef = controller.GoToExportPage();
//        
//        orderTable = '';
//        
//        for(integer i = 0; i <= ClientOrderGridController.CHUNK_SIZE + ClientOrderGridController.CHUNK_SIZE; i++ ) {
//        	
//        	orderTable += 'Test'+ i;
//        }
//        
//        //Call SaveHTML method
//        listViewObjectId = ClientOrderGridController.SaveHtml(orderTable);
//        
//        String testStr;
//        
//        try {
//        	
//        	//Call SaveHTML method
//        	listViewObjectId = ClientOrderGridController.SaveHtml(testStr);
//        } catch(Exception e) {
//        	
//        	System.assert(e.getMessage().contains(Label.ORDER_GRID_SAVING_ERROR));
//        }
//        
//        //assert statement
//        System.assert(String.isNotBlank(listViewObjectId));
//        
//        //Test stops here
//        Test.stopTest();
//    }
}