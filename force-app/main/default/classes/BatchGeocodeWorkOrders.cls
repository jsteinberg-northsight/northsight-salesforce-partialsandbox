/*
	This class is meant for new work orders that are not yet associated with any geocode cache records
	For each work order record it must:
	 1. See if the address is already cached, associate the new work order with it, 
	     and any further duplicate addresses
	 2. Create a new cache (up to 10 new per execute) if it's not already cached,
	      associate further duplicate addresses with it
*/

global without sharing class BatchGeocodeWorkOrders implements Database.Batchable<Case>, Database.Stateful, Database.AllowsCallouts{
	/*
	private List<Case> ordersList;
	
	// This is our working set of work orders that need to be updated
	private List<Case> ordersToUpdate;
	
	// This is the list of geocode cache records we will be referencing, saving, or updating
	private List<GeoCode_Cache__c> GeoCodeCache;
	
	// This is a helper class to use to track what orders (by list index) belong with what geo caches (by list index)
	private class detail {
		public detail(integer firstIndex, Integer gcidx) {
			orderIndexes = new List<Integer> {firstIndex};
			GeoCodeIndex = gcidx;
		}
		public list<integer> orderIndexes {get; set;}
		public Integer GeoCodeIndex {get; private set;}
		//public boolean GCExists {get; private set;}
	}
	// We index the detail objects by address hash for easy reference
	private Map<string, detail> AddressMapGeoCache;
	// Flag set if we encounter items to quarantine
	private boolean quarantined;
	// Flag to cancel any further processing
	private boolean isOverLimit;
	private integer callouts = 0;
	private integer orderCount = 0;
	private integer ordersProcessed = 0;
	*/
	global Iterable<Case> start(Database.BatchableContext BC){
		/*
		system.debug('!!! Start !!!');
	
		// Initialize limit flag
		isOverLimit = false;
	
		// Google imposes a limit of 2500 records per day. Don't try to do more than that // Non closed or canceled / Existing after 10/1/12
		ordersList = [Select id,ContactId,Automatic_Reassignment__c, geocode_cache__c, street_address__c, city__c, state__c, zip_code__c , Trip_Charge_Reason__c,Vendor_Notes_To_Staff__c from case where (status!='Closed' and status!='Canceled') and createdDate > 2012-10-01T00:00:00Z  and geocode_cache__c = null order by due_date__c nulls last limit 2500];	
		orderCount = ordersList.size();
		GeoCodeCache = new List<Geocode_cache__c> {};
		ordersToUpdate = new List<Case> {};
		AddressMapGeoCache = new Map<string, detail> {};
		quarantined = false; 
		
		system.debug('!!!!! OrdersList Size: ' + ordersList.size());
		
		return ordersList;
		*/
		return null;
	}
	
	global void execute(Database.BatchableContext BC, List<Case> orders){
		/*
		system.debug('!!! Execute :: orders: ' + orders);
		
		// In here we are going to process up to 10 orders at a time
		boolean tmpBool;
		string tmpHash;
		detail tempDetail;
		geocode_cache__c[] tmpCache;
		Integer tempOrderIndex;
		Integer tempCacheIndex;
		map<string, string> tmpCacheData;
		geocode_cache__c tmpCacheRec;
		for(Case ord : orders) {
			// Step 1: Hash the address
			string addrConcat = GeoUtilities.formatAddress(ord.Street_Address__c,ord.City__c,ord.State__c,ord.Zip_Code__c);
			tmpHash = GeoUtilities.createAddressHash(addrConcat);
			
			// Step 2: add to the orderstoupdate list
			tempOrderIndex = ordersToUpdate.size();
			if(ord.Trip_Charge_Reason__c == 'Other' && ord.Vendor_Notes_To_Staff__c == null){
				ord.Vendor_Notes_To_Staff__c = 'NONE';
			}
			ordersToUpdate.add(ord);
			
			// Step 3: Is the current address already processed?
			//   We know if the hash address is already added as an index in the tracker map
			//   If so, go ahead and update the map, adding in the index of the order to associate with the 
			//    indexed GeoCode Cache record. 
			// no callout needed
			if(AddressMapGeoCache.containsKey(tmpHash)) {
				AddressMapGeoCache.get(tmpHash).OrderIndexes.add(tempOrderIndex);
			}
			// Step 4: Check for an existing GeoCache in the database
			//   If we find one, create a record of it in the tracker map for later reference
			//   ELSE do a callout and retrieve geo data and create a new geo cache object
			else {
				tmpCache = [select id,quarantined__c,flag__c from geocode_cache__c where address_hash__c = :tmpHash];
				if(tmpCache.size() > 0 ) {
					
					//Add the geocache to the working list
					tempCacheIndex = GeoCodeCache.size();
					GeoCodeCache.add(tmpCache[0]);
					AddressMapGeoCache.put(tmpHash, new detail(tempOrderIndex, tempCacheIndex));
					
				} else {
					// Skip callouts if we're over limit
					if(isOverLimit) {
						system.debug('!!! Over Query Limit :: Skipping case set');
						continue;//skip the rest and loop to the next case. 
					}
					// Callout
					callouts++;
					tmpCacheData = GeoUtilities.getGeoData(addrConcat);
					// Stop batch execution if we're over limit
					if(tmpCacheData.get('status')=='OVER_QUERY_LIMIT') {
						system.debug('OVER QUERY LIMIT!! HALTING!!!');
						isOverLimit = true;
						continue;//skip the rest and loop to the next case.
					}
					
					// doing it this way since I can't cast string to boolean -- thanks salesforce!!
					if(tmpCacheData.get('partial')=='1') {
						tmpBool = true;
						quarantined = true;
					} else {
						tmpBool = false;
					}
					
					tmpCacheRec = new geocode_cache__c (
    											address_hash__c = tmphash,
    											formatted_address__c = addrConcat,
    											Formatted_Street_Address__c = GeoUtilities.capitalizeAll(GeoUtilities.preformatAddress(ord.Street_Address__c)),
												Formatted_City__c = ord.City__c,
												Formatted_State__c = ord.State__c,
												Formatted_Zip_Code__c = ord.Zip_Code__c,
    											Street_Address__c = ord.Street_Address__c,
												City__c = ord.City__c,
												State__c = ord.State__c,
												Zip_Code__c = ord.Zip_Code__c,
    											location__latitude__s = decimal.valueOf(tmpCacheData.get('lat')),
    											location__longitude__s = decimal.valueOf(tmpCacheData.get('lng')),
    											last_geocoded__c = date.today(),
    											manual_location__c = false,
    											partial_match__c = tmpBool,
    											geocode_status__c = tmpCacheData.get('status'),
    											quarantined__c = tmpBool
    										);
					tempCacheIndex = GeoCodeCache.size();
					// Add the new geocode cache record to the working list
					GeoCodeCache.add(tmpCacheRec);
				
					AddressMapGeoCache.put(tmpHash, new detail(tempOrderIndex, tempCacheIndex));
      				
				}
			} 
		}	
		*/
	}
	 
	global void finish(Database.BatchableContext BC){
		/*
		system.debug('!!! Finish :: Orderstoupdate size: ' + ordersToUpdate.size());
		
		// Step 1:  Save the GeoCodeCache list
		database.upsert(GeoCodeCache,false);
		
		// Step 2: Use the tracker map to update the ordersToUpdate list
		Set<string> hashkeys = AddressMapGeoCache.keySet();
		detail tmpdetail;
		// Loop through the master hash indexed address list
		for(string key : hashkeys) {
			// get the current detail record
			tmpdetail = AddressMapGeoCache.get(key);
			// Loop through the list of work order indexes, add the geocodecache index to the ordersToUpdate record
			for(integer idx : tmpdetail.orderIndexes) {
				ordersToUpdate[idx].GeoCode_cache__c = GeoCodeCache[tmpdetail.GeoCodeIndex].id;
				ordersProcessed++;
				if(GeoCodeCache[tmpdetail.GeoCodeIndex].quarantined__c==true){
						ordersToUpdate[idx].Approval_Status__c = 'Quarantined';
					}
				if(GeoCodeCache[tmpdetail.GeoCodeIndex].flag__c==true){
						ordersToUpdate[idx].Approval_Status__c = 'Quarantined-Flagged';
				}
				if(ordersToUpdate[idx].ContactId == null && ordersToUpdate[idx].Approval_Status__c!='Quarantined' && ordersToUpdate[idx].Approval_Status__c!='Quarantined-Flagged'){
					ordersToUpdate[idx].Automatic_Reassignment__c = true;
				}
			}
		}
		
		system.debug('!!! Address Geocache Map : ' + AddressMapGeoCache);
		
		// Step 3: Update the work orders
		database.upsert(ordersToUpdate,false);
		
		/*******************************************************************************************************************/
		/*
		//create a list for case IDs
		List<ID> caseIDs = new List<ID>();
		//add case IDs from ordersToUpdate into caseIDs
		for(Case cID : ordersToUpdate){
			caseIDs.add(cID.Id);
		}
		//call the generateLinks method in the generateGeoLinksOffWorkOrders class sending a list of order IDs
		//the list of order IDs will be used to create geozone links
		generateGeoLinksOffWorkOrders.generateLinks(caseIDs);
		/*******************************************************************************************************************/
		/*
		if(System.Test.isRunningTest()){
			//test to see if the call to generateLinks method creates a geozone link off a work order
       		/*******************************************************************************************************************/
       		//create a geozone link list
       		/*List<GeoZoneLink__c> links = new List<GeoZoneLink__c>();
       
       		//fill the links list with the links that should have just been created
       		for(GeoZoneLink__c gz : [SELECT Id, Geocode_Cache__c, ClientZone__c 
       								FROM GeoZoneLink__c]){
       			System.debug(gz.Geocode_Cache__c);
       			System.debug(gz.ClientZone__c);
       			links.add(gz);
       		}
       		
       		//check to see if the list has a size > 0, if it does, then a geozone link was created off a work order
       		//System.Assert(links.size() > 0);
       		/*******************************************************************************************************************/
		/*}
		
		insert new Geocode_Process_Log__c(Callouts__c = callouts, Orders_Processed__c = ordersProcessed, Work_Order_Count__c = orderCount, Exceeded_Limit__c = isOverLimit);
		
		// Step 4: Notify -- if the quarantined flag is set
		if(quarantined) {
			string[] ids = new string[] {};
			string[] emails = new string[] {};
			group[] g = [select id from Group where Name='Quarantine_Managers'];
			if(g.size()>0) {
				groupmember[] gm = [select userorgroupid from groupmember where groupid=: g[0].id];
				for(groupmember grm : gm) {
					ids.add(grm.userorgroupid);
				}
				user[] u = [select email from user where id IN :ids];
				for(user ur : u) {
					emails.add(ur.email);
				}
			
				if(emails.size()>0) {
					emailtemplate et = [select id from emailtemplate where developername=: 'WO_Quarantine_Notification'];
					 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					 mail.setToAddresses(emails);
					 mail.setTemplateId(et.id);
					 mail.saveAsActivity = false;
					 mail.setTargetObjectId(u[0].id);
					 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
					 
					 system.debug('sent notification email!!');
				}
			}
		}*/
	}
	
	global static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}