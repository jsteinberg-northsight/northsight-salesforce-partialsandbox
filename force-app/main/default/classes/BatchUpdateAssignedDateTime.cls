global without sharing class BatchUpdateAssignedDateTime{// implements Database.Batchable<sObject>{
	/*
	//this method will create a list of cases that need to have their assigned date time updated
	global Database.Querylocator start(Database.BatchableContext BC){
		
		//Padmesh Soni (06/19/2014) - Support Case Record Type
		//Query record of Record Type of Case object
		List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
											
		//return a list of cases that need to have their assigned date time updated
		//Padmesh Soni (06/19/2014) - Support Case Record Type
    	//New filter criteria added for RecordTypeId
	    return Database.getQueryLocator([SELECT Id, 
												Assigned_Date_Time__c 
										 FROM Case 
										 WHERE RecordTypeId NOT IN: recordTypes 
										 AND Contact.Pruvan_Username__c != null 
										 AND Serviceable__c = 'Yes' 
										 AND Pruvan_Verify__c = false 
										 AND Status = 'Open' 
										 AND Geocode_Cache__r.Formatted_Street_Address__c != null 
										 AND Geocode_Cache__r.Formatted_City__c != null 
										 AND Geocode_Cache__r.Formatted_State__c != null 
										 AND Geocode_Cache__r.Formatted_Zip_Code__c != null 
										 AND (Geocode_Cache__r.quarantined__c != true OR Override_Quarantine__c = true) 
										 AND Geocode_Cache__c != null 
										 AND (RecordType.Name = 'REO' OR RecordType.Name = 'MAID' OR RecordType.Name = 'INSP' OR RecordType.Name = 'INSPM')]);
	}
	
	//this method will be used to update the cases' assigned date time values
	global void execute(Database.BatchableContext BC, List<Case> caseList){
		if(caseList != null && caseList.size() > 0){
			for(Case c : caseList){
				c.Assigned_Date_Time__c = Datetime.now();
			}
			
			update caseList;
		}
	}
	
	global void finish(Database.BatchableContext BC){
		//section not needed
	}
	*/
}