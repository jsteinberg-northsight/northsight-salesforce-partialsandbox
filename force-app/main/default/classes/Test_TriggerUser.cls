@isTest(seeAllData=false)
private class Test_TriggerUser {
///**
// *	Description		:	This is test class for testing of pre-post functionality on User trigger. 
// *
// *	Created By		:	Padmesh Soni
// *
// *	Created Date	:	04/09/2014
// *
// *	Current Version	:	V1.3
// *
// *	Revisiion Logs	:	V1.0 - Created 
// *                      V1.1 - Modified - Padmesh Soni(10/06/2014) - SM-232: Prevent parenthesis from being in names
// *                      V1.2 - Modified - Padmesh Soni(10/21/2014) - VCP-32:Prevent certain Profiles from being selected
// *						V1.3 - Modified - Padmesh Soni(10/29/2014) - VCP-37: Improve "profile blocker" trigger by putting the settings in Custom Settings.
// *		
// *	Code Coverage	:	V1.0 - 100%
// *						V1.1 - 100%
// *						V1.2 - 100%
// *						V1.3 - 100%
// **/
//	
//	//create a new user with the System Administrator profile
//    private static Profile sysAdminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
//         
//	//Test method to test validateUniqueAlias method functionality
//    static testMethod void testValidateUniqueAlias() {
// 		
// 		//List to hold all account
//        List<Account> accounts = new List<Account>();
//        
//        //Loop through countData time
//        for(integer i = 1; i <= 2; i++) {
//        
//            /******** PRE-TEST SETUP create a new account *********/
//            accounts.add(new Account(Name = 'Tester Account' + i));
//        }
//        
//        insert accounts;
//        
//        List<Contact> contacts = new List<Contact>();
//        
//        //Loop through countData time
//        for(integer i = 1; i <= 2; i++) {
//        
//            //--------------------------------------------------------
//            //PRE-TEST SETUP
//            //create a new contact
//            contacts.add(new Contact(LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active',
//            							Grass_Cut_Cap__c = 3, Order_Count__c = 0));
//        }   
//        
//        insert contacts;
//        
//        //create a new user with the profile
//        User usr = new User(alias = 'standt', email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8',
//                                lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = sysAdminProfile.Id,
//                                timezonesidkey = 'America/Los_Angeles', username = Math.random()+'@test.com', isActive = true);
//        
//        insert usr;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Create test user
//        User usr1 = new User(alias = 'standt', email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8',
//                                lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = sysAdminProfile.Id,
//                                timezonesidkey = 'America/Los_Angeles', username = Math.random()+'@test.com', isActive = true);
//        
//        try {
//        	
//        	//create user
//        	insert usr1;
//        } catch(Exception e) {
//        	
//	        //assert statement
//	        System.assert(e.getMessage().contains(Label.Validate_Unique_Alias_Error));
//        }
//        
//        //Blank update usr for black testing
//        update usr;
//        
//        //Test stops here
//        Test.stopTest();       
//    }
//    
//    //Code added - Padmesh Soni(10/06/2014) - SM-232: Prevent parenthesis from being in names
//	//Test method to test validateParenthesisInAliasOrNames method functionality
//    static testMethod void test_validateParenthesisInAliasOrNames() {
// 		
// 		//List to hold all account
//        List<Account> accounts = new List<Account>();
//        
//        //Loop through countData time
//        for(integer i = 1; i < 2; i++) {
//        
//            /******** PRE-TEST SETUP create a new account *********/
//            accounts.add(new Account(Name = 'Tester Account' + i));
//        }
//        
//        insert accounts;
//        
//        List<Contact> contacts = new List<Contact>();
//        
//        //Loop through countData time
//        for(integer i = 1; i < 2; i++) {
//        
//            //--------------------------------------------------------
//            //PRE-TEST SETUP
//            //create a new contact
//            contacts.add(new Contact(LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active',
//            							Grass_Cut_Cap__c = 3, Order_Count__c = 0));
//        }   
//        
//        insert contacts;
//        
//        //create a new user with the profile
//        User usr = new User(alias = 'stand(t', email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8',
//                                lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = sysAdminProfile.Id,
//                                timezonesidkey = 'America/Los_Angeles', username = Math.random()+'@test.com', isActive = true);
//        
//        //Test starts here
//        Test.startTest();
//        
//        try {
//        	
//        	insert usr;
//        } catch(Exception e) {
//        	
//	        //assert statement
//	        System.assert(e.getMessage().contains(Label.NOT_VALID_PARENTHESIS_IN_NAME_OR_ALIAS));
//        }
//        
//        //Create test user
//        usr = new User(alias = 'standt', email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8',
//                                lastname = 'Testi()ng', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = sysAdminProfile.Id,
//                                timezonesidkey = 'America/Los_Angeles', username = Math.random()+'@test.com', isActive = true);
//        
//        try {
//        	
//        	//create user
//        	insert usr;
//        } catch(Exception e) {
//        	
//	        //assert statement
//	        System.assert(e.getMessage().contains(Label.NOT_VALID_PARENTHESIS_IN_NAME_OR_ALIAS));
//        }
//        
//        
//        //Create test user
//        usr = new User(alias = 'standt', email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8', FirstName = 'Test(Testing)',
//                                lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = sysAdminProfile.Id,
//                                timezonesidkey = 'America/Los_Angeles', username = Math.random()+'@test.com', isActive = true);
//        
//        try {
//        	
//        	//create user
//        	insert usr;
//        } catch(Exception e) {
//        	
//	        //assert statement
//	        System.assert(e.getMessage().contains(Label.NOT_VALID_PARENTHESIS_IN_NAME_OR_ALIAS));
//        }
//        
//        
//        //Create test user
//        usr = new User(alias = 'standt', email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8', FirstName = 'Test',
//                                lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = sysAdminProfile.Id,
//                                timezonesidkey = 'America/Los_Angeles', username = Math.random()+'@test.com', isActive = true);
//        
//    	//create user
//    	insert usr;
//    	
//    	try {
//        	
//        	usr.Alias = 'test(T)';
//    	
//    		//update user
//        	update usr;
//        } catch(Exception e) {
//        	
//	        //assert statement
//	        System.assert(e.getMessage().contains(Label.NOT_VALID_PARENTHESIS_IN_NAME_OR_ALIAS));
//        }
//        
//        //Test stops here
//        Test.stopTest();       
//    }
//    
//    //Code added - Padmesh Soni(10/21/2014) - VCP-32:Prevent certain Profiles from being selected.
//	//Test method to test validateParenthesisInAliasOrNames method functionality
//    static testMethod void test_preventUsersProfile() {
// 		
// 		//Code added - Padmesh Soni(10/29/2014) - VCP-37: Improve "profile blocker" trigger by putting the settings in Custom Settings.
//    	//Insert Prevent Profiles Default values
//        Prevent_Profiles_Default__c configDefault = new Prevent_Profiles_Default__c();
//        configDefault.Excluded_Profiles__c = 'Partner Community User, DO NOT USE - Partner Community - Updaters Login, DO NOT USE - Partner Community - Vendors Login, DO NOT USE - Partner Community - Vendors OrderSelect Login, Partner Community Login User';
//        insert configDefault;
//        
// 		//Set to hold all profile names which are need 
//    	//to be prevent from being creation of these type users 
//    	Set<String> excludedProfileNames = new Set<String>{ 'Partner Community User',
//															'Partner Community Login User'};
//    	
//    	//List to hold query result of Profiles
//    	List<Profile> preventedProfiles = [SELECT Id FROM Profile WHERE Name IN: excludedProfileNames];
//    	
//    	//assert statement
//    	//System.assertEquals(2, preventedProfiles.size());
//    	
// 		//List to hold all account
//        List<Account> accounts = new List<Account>();
//        
//        //Loop through countData time
//        for(integer i = 1; i < 2; i++) {
//        
//            /******** PRE-TEST SETUP create a new account *********/
//            accounts.add(new Account(Name = 'Tester Account' + i));
//        }
//        
//        insert accounts;
//        
//        List<Contact> contacts = new List<Contact>();
//        
//        //Loop through countData time
//        for(integer i = 1; i < 2; i++) {
//        
//            //--------------------------------------------------------
//            //PRE-TEST SETUP
//            //create a new contact
//            contacts.add(new Contact(LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active',
//            							Grass_Cut_Cap__c = 3, Order_Count__c = 0));
//        }   
//        
//        insert contacts;
//        
//        //create a new user with the profile
//        User usr = new User(alias = 'stand(t', email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8', contactId = contacts[0].Id, 
//                                lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = preventedProfiles[0].Id,
//                                timezonesidkey = 'America/Los_Angeles', username = Math.random()+'@test.com', isActive = true);
//        
//        //Test starts here
//        Test.startTest();
//        
//        try {
//        	
//        	insert usr;
//        } catch(Exception e) {
//        	
//	        //assert statement
//	        System.assert(e.getMessage().contains(Label.PROFILE_SELECTION_WARNING));
//        }
//        
//        //Create test user
//        usr = new User(alias = 'standt', email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8', contactId = contacts[0].Id,
//                                lastname = 'Testi()ng', languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = preventedProfiles[0].Id,
//                                timezonesidkey = 'America/Los_Angeles', username = Math.random()+'@test.com', isActive = true);
//        
//        try {
//        	
//        	//create user
//        	insert usr;
//        } catch(Exception e) {
//        	
//	        //assert statement
//	        System.assert(e.getMessage().contains(Label.PROFILE_SELECTION_WARNING));
//        }
//        
//        
//        //Create test user
//        usr = new User(alias = 'standt', email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8', FirstName = 'Test(Testing)', 
//        					contactId = contacts[0].Id, lastname = 'Testing', languagelocalekey = 'en_US', localesidkey = 'en_US', 
//        					profileId = preventedProfiles[1].Id,timezonesidkey = 'America/Los_Angeles', username = Math.random()+'@test.com', 
//        					isActive = true);
//        
//        try {
//        	
//        	//create user
//        	insert usr;
//        } catch(Exception e) {
//        	
//	        //assert statement
//	        System.assert(e.getMessage().contains(Label.PROFILE_SELECTION_WARNING));
//        }
//        
//        //Test stops here
//        Test.stopTest();       
//    }
}