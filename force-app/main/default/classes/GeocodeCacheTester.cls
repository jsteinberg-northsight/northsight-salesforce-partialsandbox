@isTest(SeeAllData=false)
private class GeocodeCacheTester {
//
//    static testMethod void myUnitTest() {
//		//ACCOUNTS
//		Account a = new Account(Name='Test Account');
//		insert a;
//		Profile p = [select id from profile where name='Customer Portal Manager Standard'];
//		
//		//CONTACTS
//		Contact con = new Contact(LastName='TEST', FirstName='Billy');
//		con.AccountId = a.id;
//		con.Maintenance_Cap__c = 100;
//		con.Grass_Cut_Cap__c = 100;
//		con.Inspections_Cap__c = 100;
//		con.Shrub_Trim__c = 3;
//		con.REO_Grass_Cut__c = 3;
//		con.Pay_Terms__c = 'Net 30';
//		con.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//		con.Date_Active__c = Date.Today();
//		con.Status__c = '3 - Active';
//		con.MailingStreet = '30 Turnberry Dr';
//		con.MailingCity = 'La Place';
//		con.MailingState = 'LA';
//		con.MailingPostalCode = '70068';
//		con.OtherStreet = '30 Turnberry Dr';
//		con.OtherCity = 'La Place';
//		con.OtherState = 'LA';
//		con.OtherPostalCode = '70068';
//		con.Trip_Charge__c = 25;
//		con.Other_Address_Geocode__Latitude__s = 30.00;
//		con.Other_Address_Geocode__Longitude__s = -90.00;
//		insert con;
//	
//        //PROPERTIES
//        Map<Id, Geocode_Cache__c> geos = new Map<Id, Geocode_Cache__c>();
//		Geocode_Cache__c geo1 = new Geocode_Cache__c(Last_Inspected_Date__c = date.today(), Last_Completed_Date_Serviced__c = date.today(), Last_Completed_Client_Order_Number__c = '1234', Client__c = 'Jimi Hendrix', Customer__c = 'The muffin man', Loan_Type__c = 'CONVENTIONAL', Last_Completed_Serviced_By__c = con.Id, Last_Completed_Work_Ordered__c = 'Work Order 1', Last_Completed_Work_Order_ID__c = '1234567890');
//		Geocode_Cache__c geo2 = new Geocode_Cache__c();
//		
//		insert new Geocode_Cache__c[]{geo1, geo2};
//		
//		geos.put(geo1.Id, geo1);
//		geos.put(geo2.Id, geo2);
//		
//		//RECORD TYPE
//		RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name =:'INSP' limit 1];
//		
//		//update geocode_cache set Create_Inspection__c to true
//		geo1.Create_Inspection__c = true;
//		geo2.Create_Inspection__c = false;
//		update geos.values();
//		
//		//requery 
//		geos = new map<Id, Geocode_Cache__c>([SELECT Id, Create_Inspection__c FROM Geocode_Cache__c WHERE Id IN :geos.keySet()]); 
//		Case[] cases = [SELECT Id, Geocode_Cache__c, Last_Service_Date__c, Last_Crew_First_Name__c, Last_Work_Ordered__c, Last_Work_Order_ID__c, Date_Inspected__c, Last_Work_Order_Number__c, Client__c, Customer__c, Loan_Type__c, RecordTypeId, Work_Ordered__c, Vendor_Code__c, Last_Crew_Name_Lookup__c FROM Case WHERE Geocode_Cache__c IN :geos.keySet()];
//		
//		//assert to see if cases were created
//		system.assert(cases != null);
//		for(Case c : cases){
//			if(c.Geocode_Cache__c == geo1.Id){
//				//system.assert(c.Date_Inspected__c != null);
//				system.assertEquals(c.Last_Work_Order_Number__c, geo1.Last_Completed_Client_Order_Number__c);
//				system.assertEquals(c.Client__c, geo1.Client__c);
//				system.assertEquals(c.Customer__c, geo1.Customer__c);
//				system.assertEquals(c.Loan_Type__c, geo1.Loan_Type__c);
//				system.assertEquals(c.Last_Work_Ordered__c, geo1.Last_Completed_Work_Ordered__c);
//				system.assertEquals(c.Last_Work_Order_ID__c, geo1.Last_Completed_Work_Order_ID__c);
//				system.assertEquals(c.Last_Crew_Name_Lookup__c, geo1.Last_Completed_Serviced_By__c);
//				system.assertEquals(c.Last_Crew_First_Name__c, con.FirstName);
//				system.assertEquals(c.Last_Service_Date__c, geo1.Last_Completed_Date_Serviced__c);
//				
//			}
//		}
//		
//		geo1 = geos.get(geo1.Id);
//		//assert to see if Create_Inspection__c was changed to false
//		system.assertEquals(false, geo1.Create_Inspection__c);
//    }
}