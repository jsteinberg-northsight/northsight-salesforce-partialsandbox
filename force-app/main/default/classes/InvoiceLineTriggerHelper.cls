public with sharing class InvoiceLineTriggerHelper {
	
	//20160815 - Tyler Hudson
	//function will modify the invoicelines parameter such that the invoiceline.property field 
	//will refer to the same property that the invoice.property refers to.
	
	public static void setInvoiceLineFieldsFromParentInvoice (/*OUT*/ Invoice_line__c[] lines){
		map<id,invoice__c> invoices = fetchInvoices(extractInvoiceIDs(lines));
		for(invoice_line__c line:lines){
			line.property__c=invoices.get(line.invoice__c).property__c;
			line.InvoiceStatus__c=invoices.get(line.invoice__c).status__c;
		}
	}	
	
	static map<id,invoice__c> fetchInvoices(set<id> invoiceIDs){
		return new map<id,invoice__c>([select id, property__c,status__c from invoice__c where id IN :invoiceIDs]); 
	} 
	
	static set<id> extractInvoiceIDs(Invoice_line__c[] lines){
		set<id> ret = new set<id>();
		for (invoice_line__c line : lines){
			ret.add(line.invoice__c);
		}		
		return ret;
	}
	
	public static void testbypass(){		
		integer i = 0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
						i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
						i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;


	}  
    
}