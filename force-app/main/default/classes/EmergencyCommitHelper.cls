public without sharing class EmergencyCommitHelper {
	private static List<Case> cases;
	private static List<Client_Commitment__c> emergencyCommitments;
	
	//initialize the cases list and emergencyCommitments list
	public static void initialize(List<Case> triggerNew){
		cases = triggerNew != null ? triggerNew : new List<Case>();
		
		emergencyCommitments = new List<Client_Commitment__c>();
	}
	
	//this method checks the list of cases to see if client commitments are needed on them
	public static void createEmergencyCommitments(){
		if(cases.size() > 0){
			for(Case c : cases){
				
				//System.debug('Commit_By_Date__c ::::'+ c.Commit_By_Date__c);
				//System.debug('Client_Commit_Date__c ::::'+ c.Client_Commit_Date__c);
				//System.debug('Client__c ::::'+ c.Client__c);
				
				//this is the filter criteria for creating a client commitment on a case
				if(c.Commit_By_Date__c < Date.today() && (c.Client_Commit_Date__c < Date.today() || c.Client_Commit_Date__c == null) && c.Client__c == 'SG'){
					Client_Commitment__c newCommit = new Client_Commitment__c(
						Work_Order__c = c.Id,
						Commitment_Date__c = Date.today().addDays(3),
						Current_Status_of_Order__c = 'Pending Field Complete',
						Root_Cause_of_Delay__c = 'Delay in Receiving Work',
						Public_Comments__c = '',
						Internal_Comments__c = ''
					);
					emergencyCommitments.add(newCommit);
				}
			}
		}
	}
	
	//finalize by inserting any client commitments that may have been created
	public static void finalize(){
		if(emergencyCommitments.size() > 0){
			insert emergencyCommitments;
		}
	}
}