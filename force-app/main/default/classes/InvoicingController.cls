/**
 *  Purpose         :   This class is controller for Invoicing page which is used to generate Invoices and inclusion of their Invoice Line Items.
 *
 *  Created By      :
 *
 *  Created Date    :   12/9/2014
 *
 *  Current Version :   V1.10
 *
 *  Revision Log    :   V1.0 - Created - AC-6:Invoice Screen | Visual Force Page Prototype
 *                                              V1.1 - Modified - Padmesh Soni (12/19/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
 *                                                              Changes are:
 *                                                                      1. Code modified with appropriate comments.
 *                                                                      2. Two new methods added.
 *                                              V1.2 - Modified - Padmesh Soni (12/22/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
 *                                              V1.3 - Modified - Padmesh Soni (12/29/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
 *                                              V1.4 - Modified - Padmesh Soni (12/30/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
 *                                              V1.5 - Modified - Padmesh Soni (12/31/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
 *                                              V1.6 - Modified - Padmesh Soni (1/1/2015) - AC-6:Invoice Screen | Visual Force Page Prototype
 *                                              V1.7 - Modified - Padmesh Soni (1/2/2015) - AC-6:Invoice Screen | Visual Force Page Prototype
 *                                              V1.8 - Modified - Padmesh Soni (1/5/2015) - AC-6:Invoice Screen | Visual Force Page Prototype
 *                                              V1.9 - Modified - Padmesh Soni (1/6/2015) - AC-6:Invoice Screen | Visual Force Page Prototype
 *                                              V1.10 - Modified - Padmesh Soni (2/5/2015) - AC-12: Invoice Line Item Combo Box improvements
 **/
public with sharing class InvoicingController {
    //Class properties
    public String workOrderId{get;set;}
    public Case workOrder{get;set;}
    public String invoiceSerializedPayable{
        get;set;
    }

    public String invoiceSerializedReceivable{get; set;}
    public integer ARTotal{get;set;}
    public integer APTotal{get;set;}
    public Id APLineTypeId{get;set;}
    public Id ARLineTypeId{get;set;}
    public Id APInvoiceTypeId{get;set;}
    public Id ARInvoiceTypeId{get;set;}
	public String domain { get; set; }

	//Code added - Padmesh Soni (1/16/2015) - AC-9: Default AP Pricebook
	//New variable to hold Vendor's payable invoice Id
	public Id newVendorPayableInv {get; set;}
    public Boolean canCreateInvoice { get; set; }

    //Standard Controller constructor defintion
    public InvoicingController(ApexPages.Standardcontroller SC) {
        domain = URL.getSalesforceBaseUrl().getHost();
        canCreateInvoice = (Schema.sObjectType.Invoice__c.isCreateable() && Schema.sObjectType.Invoice__c.isUpdateable()) ? true : false;

        APLineTypeId = [select Id from RecordType where SobjectType = 'Invoice_Line__c' and DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AP limit 1].Id;
        ARLineTypeId = [select Id from RecordType where SobjectType = 'Invoice_Line__c' and DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR limit 1].Id;
        APInvoiceTypeId = [select Id from RecordType where SobjectType = 'Invoice__c' and DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AP limit 1].Id;
        ARInvoiceTypeId = [select Id from RecordType where SobjectType = 'Invoice__c' and DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR limit 1].Id;

        //Getting Standard Id
        if(String.isNotBlank(SC.getId()))
            workOrderId = SC.getId();

		//Code modified - Padmesh Soni (2/5/2015) - AC-12: Invoice Line Item Combo Box improvements.
		//New field "Geocode_Cache__r.SqFt_of_Lot_Size__c" added into query
        //Code modified - Padmesh Soni (1/16/2015) - AC-9: Default AP Pricebook
		//New field "Contact.Vendor_Price_Book__c" added into query
        //Code modified - Padmesh Soni (12/22/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
        //As per new changeset doucment: 141218 Invoice Changeset
        //New fields Geocode_Cache__r.Name, Geocode_Cache__r.Id, Receive_From__r.Name, Receive_From__r.Id added into query
        //Query result of Case of Standard Set Controller
        workOrder = [SELECT Id, CaseNumber, Geocode_Cache__r.Name, AccountId, Geocode_Cache__r.Id, Client_Name__r.Name, Client_Name__r.Id,
        				Contact.Vendor_Price_Book__c, Geocode_Cache__r.SqFt_of_Lot_Size__c
						FROM Case where id =: workOrderId limit 1 FOR UPDATE];

        if (workOrder.Contact.Vendor_Price_Book__c != null) {
        	Pricebook2[] vendorPB = [SELECT
                                         Id,
                                         Name
                                     FROM
                                         Pricebook2
                                     WHERE
                                         Id = :workOrder.Contact.Vendor_Price_Book__c AND
                                         RecordType.DeveloperName = 'Payable' AND
                                         IsActive = true];
        	if (vendorPB.size() > 0) {
        		newVendorPayableInv = vendorPB[0].Id;
            }
        }

        //Query result of Aggregation results for recievable Invoice sobject records
        AggregateResult aResultAR = [SELECT COUNT(Id) recordCount FROM Invoice__c WHERE Work_Order__c =: workOrderId
                                                                        AND RecordType.DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR];

        //Query result of Aggregation results for payable Invoice sobject records
        AggregateResult aResultAP = [SELECT COUNT(Id) recordCount FROM Invoice__c WHERE Work_Order__c =: workOrderId
                                                                        AND RecordType.DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AP];

        //Getting total number of recievable Invoices
        ARTotal = (integer) aResultAR.get('recordCount');

        //Getting total number of payable Invoices
        APTotal = (integer) aResultAP.get('recordCount');

        try{

                //Query result of Payable Invoices
            Invoice__c[] invoicesPayable = [SELECT Id,CreatedDate, Name, Work_Order__c, RecordTypeId, Discount__c, Invoice_Total__c, Paid_Date__c, Status__c, Sub_Total__c,
                                                                                Price_Book__c, Payment_Release_Date__c, Receive_From__c,
                                                                                Receive_From__r.Name,Pay_To__c,Pay_To__r.Name, Sent__c,  Private_Notes__c,Public_Notes__c
                                                                                FROM Invoice__c WHERE Work_Order__c =: workOrderId
                                                                                AND RecordType.DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AP];

            //New instance of wrapper to hold list of InvoicingWrapper
            List<InvoicingWrapper> listWrap = new List<InvoicingWrapper>();

            //Loop through queried Invoices
            for (Invoice__c inv : invoicesPayable) {

                //populate list of InvoiceWrapper
                listWrap.Add(new InvoicingWrapper(inv));
            }

            //Serialize the list of Wrapper
            invoiceSerializedPayable = JSON.serialize(listWrap);

            //return JSON.serialize(invoices);

            //Serialized the Sobject list into JSON String
            //return JSON.serialize(invoices);

            //Query result of Invoices receivables
            Invoice__c[] invoicesReceivable = [SELECT Id, name, CreatedDate, Work_Order__c, RecordTypeId, Discount__c, Invoice_Total__c, Paid_Date__c, Status__c, Sub_Total__c,
                                                                                        Price_Book__c, Payment_Release_Date__c, Receive_From__c, Sent__c ,
                                                                                        Pay_To__c, Pay_To__r.Name, Receive_From__r.Name, Private_Notes__c,Public_Notes__c FROM Invoice__c
                                                                                        WHERE Work_Order__c =: workOrderId
                                                                                        AND RecordType.DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR];

            //Serialized the Sobject list into JSON String
            List<InvoicingWrapper> listWrap2 = new List<InvoicingWrapper>();

            //Loop through queried Invoices
            for (Invoice__c inv : invoicesReceivable) {

                //populate list of InvoiceWrapper
                listWrap2.Add(new  InvoicingWrapper(inv));
            }

            //Serialize the list of Wrapper
            invoiceSerializedReceivable =  JSON.serialize(listWrap2);
        }catch(Exception ex){

            //Instance of Apex page message to show User friendly message on page
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
    }

    /**
     *  @descpriton     :   This method is used to getting InvoiceLineWrapper instance list to load the lines
     *
     *  @param          :   Id invoiceId
     *
     *  @return         :   List<InvoiceLineWrapper>
     **/
    @RemoteAction
    public static List<InvoiceLineWrapper> loadLines(Id invoiceId){

        //Query result of Invoice Line records
        Invoice_Line__c[] invoiceLine = [SELECT Id,Original_Price__c, Contact_Price__c, Price_Book_Entry__c, Work_Order__c, public_notes__c, Product__c ,Product__r.Name,Product__r.Description,Quantity__c,Unit_Price__c,Discount__c,
                                                                                Product__r.Family,Invoice__c, Invoice__r.Name,RecordTypeId, UOM__c FROM Invoice_Line__c WHERE Invoice__c =: invoiceId];

        //New instance of InvoiceLineWrapper
        List<InvoiceLineWrapper> listWrap = new List<InvoiceLineWrapper>();

        //Loop through InvoiceLine queried results
        for (Invoice_Line__c inv : invoiceLine) {

            //populate list of InvoiceLineWrapper
            listWrap.Add(new InvoiceLineWrapper(inv));
        }
        return listWrap;
    }

    /**
     *  @descpriton     :   This method is used to get InvoicingUtilities.iuOption pirce book picklist options
     *
     *  @param          :   Id WOId
     *
     *  @return         :   List<InvoicingUtilities.iuOption>
     **/
    @RemoteAction
    public static List<InvoicingUtilities.iuOption> getPriceBooks(Id WOId) {
        //return price books
        return InvoicingUtilities.getPriceBooks(WOId);
    }

    /**
     *  @descpriton     :   This method is used to getting recievable InvoicingUtilities.iuOption picklist options instance list.
     *
     *  @param          :   Id WOId
     *
     *  @return         :   List<InvoicingUtilities.iuOption>
     **/
    @RemoteAction
    public static List<InvoicingUtilities.iuOption> getARPriceBooks(Id WOId){

        //returning recievable pricebooks iuOption
        return InvoicingUtilities.getARPriceBooks(WOId);
    }

    /**
     *  @descpriton     :   This method is used to getting payable InvoicingUtilities.iuOption picklist options instance list.
     *
     *  @param          :   Id WOId
     *
     *  @return         :   List<InvoicingUtilities.iuOption>
     **/
    @RemoteAction
    public static List<InvoicingUtilities.iuOption> getAPPriceBooks(Id WOId){

        //returning payable pricebooks iuOption
        return InvoicingUtilities.getAPPriceBooks(WOId);
    }

    /**
     *  @descpriton     :   This method is used to getting prodcut Categories InvoicingUtilities.iuOption picklist options instance list.
     *
     *  @param          :   Product Id
     *
     *  @return         :   List<InvoicingUtilities.iuOption>
     **/
    @RemoteAction
    public static List<InvoicingUtilities.iuOption> getProductCategories(Id pbID){

        //returning product categories iuOption
        return InvoicingUtilities.getProductCategories(pbId);
    }

    /**
     *  @descpriton     :   This method is used to getting prodcuts InvoicingUtilities.iuOption picklist options instance list.
     *
     *  @param          :   Product Id
     *
     *  @return         :   List<InvoicingUtilities.iuOption>
     **/
    @RemoteAction
    public static List<InvoicingUtilities.iuOption> getProducts(Id PBId){

        //returning product iuOption
        return  InvoicingUtilities.getProducts(PBId);
    }

    /**
     *  @descpriton     :   This method is used to getting status options on Invoice sobject.
     *
     *  @param          :
     *
     *  @return         :   List<InvoicingUtilities.iuOption>
     **/
    @RemoteAction
    public static List<InvoicingUtilities.iuOption> getStatusValues() {

        //returning product iuOption
        return  InvoicingUtilities.getInvoiceStatus();
    }

        /**
     *  @descpriton     :   This method is used to getting vendor prodcuts InvoicingUtilities.iuOption picklist options instance list.
     *
     *  @param          :   Id vendorId
     *
     *  @return         :   List<InvoicingUtilities.iuOption>
     **/
    @RemoteAction
    public static List<InvoicingUtilities.iuOption> getVendorProducts(Id vendorId){

        //returning product iuOption
        return  InvoicingUtilities.getVendorProducts(vendorId);
    }

    /**                                 ~~~~~~ Code added - Padmesh Soni (12/19/2014) - AC-6:Invoice Screen | Visual Force Page Prototype ~~~~~~~
     *  @descpriton             :       This method is used to save the invoices into database with using of JSON String of InvoiceWrapper.
     *
     *  @param                  :       String invoicesJSON
     *
     *  @return                 :       void
     **/
    @RemoteAction
    public static string saveInvoice(String invoicesJSON){

        //Code modified - Padmesh Soni (12/29/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
        //replace the deserializeStrict to deserializeUntyped reason is
        //InvoiceWrapper class is having date element for sent date and it's not getting parsed
        //when the user tried to updated sent date and got the JSON parsing exception
        //Deserialized string into InvoiceWrapper class element
        Map<String, Object> wrapItem;
        try{
                wrapItem = (Map<String, Object>) JSON.deserializeUntyped(invoicesJSON);

        }catch(Exception ex){
                return 'Invalid';
        }


        //List to hold invoices to be upserted
        Invoice__c invoiceToBeUpserted;

        //Loop through list of deserialized InvoiceWrapper list
                //Check for not null & blank
        if(String.isNotBlank(String.valueOf(wrapItem.get('id')))) {

            //Code added - Padmesh Soni (12/29/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
            //sent date string typecasted into date format
            Date sentDate;
            try{sentDate = Date.parse(String.valueOf(wrapItem.get('sent')));}catch(Exception e){}
                //Code modified - Padmesh Soni (12/29/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
                //set the values as per map processed
                //populate the Invoice list
                invoiceToBeUpserted = new Invoice__c(Id = (Id)wrapItem.get('id'), Discount__c = (decimal)wrapItem.get('discount'),
                                                        Sent__c = sentDate, Status__c = String.valueOf(wrapItem.get('status')),
                                                        Price_Book__c = (Id)wrapItem.get('priceBook'), Private_Notes__c = String.valueOf(wrapItem.get('privateNotes')),
                                                        Public_Notes__c = String.valueOf(wrapItem.get('publicNotes')));
        } else {

	        Date sentDate;

	        //Code added - Padmesh Soni (12/29/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
	        //sent date string typecasted into date format
	        if(wrapItem.get('sent') != null)
	                        sentDate = date.parse(String.valueOf(wrapItem.get('sent')));

            //Code modified - Padmesh Soni (12/29/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
            //set the values as per map processed
            //populate the Invoice list
            invoiceToBeUpserted = new Invoice__c(Discount__c = (decimal)wrapItem.get('discount'),
									                Sent__c = sentDate, Status__c = String.valueOf(wrapItem.get('status')),
									                Price_Book__c = (Id)wrapItem.get('priceBook'), Private_Notes__c = String.valueOf(wrapItem.get('privateNotes')),
									                Public_Notes__c = String.valueOf(wrapItem.get('publicNotes')), Work_Order__c = String.valueOf(wrapItem.get('workOrder')),
									                recordTypeId = (Id)wrapItem.get('recordType'));

            if(String.isNotBlank(String.valueOf(wrapItem.get('client')))) {
                invoiceToBeUpserted.Receive_From__c = String.valueOf(wrapItem.get('client'));
            }

            if(String.isNotBlank(String.valueOf(wrapItem.get('vendorId')))) {
                invoiceToBeUpserted.Pay_To__c = String.valueOf(wrapItem.get('vendorId'));
            }
        }

        upsert invoiceToBeUpserted;
        return invoiceToBeUpserted.Id;

    }

    /**                                 ~~~~~~ Code added - Padmesh Soni (12/19/2014) - AC-6:Invoice Screen | Visual Force Page Prototype ~~~~~~~
     *  @descpriton             :       This method is used to save the invoice lines into database with using of JSON String of InvoiceWrapper.
     *
     *  @param                  :       String invoicesJSON
     *
     *  @return                 :       void
     **/
    @RemoteAction
    public static integer saveInvoiceLine(String invoiceLinesJSON){

        //Variable to hold recordtype Id
        /*
        Id recordTypeId;

        //Check for invoice record type and populate the recordTypeId with respective to the Invoice record type
        if(invoiceLineRecordType == InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR)
            recordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Invoice_Line__c'
                                AND DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR limit 1].Id;
        else
            recordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Invoice_Line__c'
                                AND DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AP limit 1].Id;
                */
        //Deserialized string into InvoiceWrapper class element
        List<InvoiceLineWrapper> invoiceLineWrapperItems = (List<InvoiceLineWrapper>)JSON.deserializeStrict(invoiceLinesJSON, List<InvoiceLineWrapper>.class);

        //List to hold invoices to be upserted
        List<Invoice_Line__c> invoiceLinesToBeUpserted = new List<Invoice_Line__c>();

        //Loop through list of deserialized InvoiceWrapper list
        for(InvoiceLineWrapper wrapItem : invoiceLineWrapperItems) {

            //Check for not null & blank
            if(String.isNotBlank(wrapItem.id)) {

                //populate the Invoice Line list
                invoiceLinesToBeUpserted.add(new Invoice_Line__c(Id = wrapItem.id, Discount__c = wrapItem.discount, Quantity__c = wrapItem.qty,
                                                                    Unit_Price__c = wrapItem.unitPrice,Product__c = wrapItem.productId,
                                                                    public_notes__c = wrapItem.description, UOM__c = wrapItem.uom,
                                                                    original_Price__c = wrapItem.originalPrice, Contact_Price__c = wrapItem.cpId,
                                                                    Price_Book_Entry__c = wrapItem.pbeId));
            } else {

                //populate the Invoice Line list
                invoiceLinesToBeUpserted.add(new Invoice_Line__c(Discount__c = wrapItem.discount, Quantity__c = wrapItem.qty,
                                                                        Unit_Price__c = wrapItem.unitPrice,Product__c = wrapItem.productId,
                                                                        public_notes__c = wrapItem.description,
                                                                        RecordTypeId = wrapItem.recordType, Invoice__c = wrapItem.invoiceId, UOM__c = wrapItem.uom ,
                                                                        original_Price__c = wrapItem.originalPrice, Contact_Price__c = wrapItem.cpId,
                                                                    	Price_Book_Entry__c = wrapItem.pbeId));
            }

        }

        //variable to hold to be upserted line item list
        integer upsertedReocrdCount = 0;

        //Check for size of list
        if(invoiceLinesToBeUpserted.size() > 0) {

            //perform dml operation
            upsert invoiceLinesToBeUpserted;

            //store the list size
            upsertedReocrdCount = invoiceLinesToBeUpserted.size();
        }
        return upsertedReocrdCount;
    }

    /**
     *  @descpriton     :   This method is used to refresh the Payable invoice section.
     *
     *  @param          :
     *
     *  @return         :   void
     **/
    public void refreshAP(){

        //Query result of Aggregation results for payable Invoice sobject records
        AggregateResult aResultAP = [SELECT COUNT(Id) recordCount FROM Invoice__c WHERE Work_Order__c =: workOrderId
                                                                        AND RecordType.DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AP];

        //Getting total number of payable Invoices
        APTotal = (integer) aResultAP.get('recordCount');

        try{

                //Query result of Payable Invoices
            Invoice__c[] invoicesPayable = [SELECT Id, CreatedDate,Name, Work_Order__c, Discount__c, Invoice_Total__c, Paid_Date__c, Status__c, Sub_Total__c,
                                                                                Price_Book__c, Payment_Release_Date__c, Receive_From__c,
                                                                                Receive_From__r.Name,Pay_To__c,Pay_To__r.Name, Sent__c,  Private_Notes__c,Public_Notes__c
                                                                                FROM Invoice__c WHERE Work_Order__c =: workOrderId
                                                                                AND RecordType.DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AP];

            //New instance of wrapper to hold list of InvoicingWrapper
            List<InvoicingWrapper> listWrap = new List<InvoicingWrapper>();

            //Loop through queried Invoices
            for (Invoice__c inv : invoicesPayable) {

                //populate list of InvoiceWrapper
                listWrap.Add(new InvoicingWrapper(inv));
            }

            //Serialize the list of Wrapper
            invoiceSerializedPayable = JSON.serialize(listWrap);
        }catch(Exception ex){

            //Instance of Apex page message to show User friendly message on page
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
    }

    /**
     *  @descpriton     :   This method is used to refresh the Payable invoice section.
     *
     *  @param          :
     *
     *  @return         :   void
     **/
    public void refreshAR(){

        //Query result of Aggregation results for recievable Invoice sobject records
        AggregateResult aResultAR = [SELECT COUNT(Id) recordCount FROM Invoice__c WHERE Work_Order__c =: workOrderId
                                                                        AND RecordType.DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR];

        //Getting total number of recievable Invoices
        ARTotal = (integer) aResultAR.get('recordCount');

        try{

            //Query result of Invoices receivables
            Invoice__c[] invoicesReceivable = [SELECT Id,CreatedDate, name, Work_Order__c, Discount__c, Invoice_Total__c, Paid_Date__c, Status__c, Sub_Total__c,
                                                                                        Price_Book__c, Payment_Release_Date__c, Receive_From__c, Sent__c ,
                                                                                        Pay_To__c, Pay_To__r.Name, Receive_From__r.Name, Private_Notes__c,Public_Notes__c FROM Invoice__c
                                                                                        WHERE Work_Order__c =: workOrderId
                                                                                        AND RecordType.DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR];

            //Serialized the Sobject list into JSON String
            List<InvoicingWrapper> listWrap2 = new List<InvoicingWrapper>();

            //Loop through queried Invoices
            for (Invoice__c inv : invoicesReceivable) {

                //populate list of InvoiceWrapper
                listWrap2.Add(new  InvoicingWrapper(inv));
            }

            //Serialize the list of Wrapper
            invoiceSerializedReceivable =  JSON.serialize(listWrap2);
        }catch(Exception ex){

            //Instance of Apex page message to show User friendly message on page
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
    }

    /**
     *  @descpriton     :   This method is used delete invoice line item.
     *
     *  @param          :   String lineId
     *
     *  @return         :   String
     **/
    @RemoteAction
    public static String deleteInvoiceLine(String lineId){

        String msg = '';
        if(String.isNotBlank(lineId) && lineId instanceOf id){

            Invoice_Line__c lineItem = new Invoice_Line__c(Id = lineId);

            try {
                    delete lineItem;
            } catch(Exception e){

                    msg = e.getMessage();
            }
            return msg;
        } else {

            msg = 'This is not a valid Invoice Line.';

            return msg;
	    }
    }

    /**
     *  @descpriton     :   This method is used delete invoice.
     *
     *  @param          :   String invoiceId
     *
     *  @return         :   String
     **/
    @RemoteAction
    public static String deleteInvoice(String invoiceId){

        String msg = '';
        if(String.isNotBlank(invoiceId) && invoiceId instanceOf id){

            Invoice__c lineItem = new Invoice__c(Id = invoiceId);

            try {
				delete lineItem;
            } catch(Exception e){

				msg = e.getMessage();
            }
            return msg;
        } else {

            msg = 'This is not a valid Invoice Line.';

            return msg;
	    }
    }
    
    	public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}