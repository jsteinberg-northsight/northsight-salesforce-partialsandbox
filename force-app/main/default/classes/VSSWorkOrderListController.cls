/**
 *  Description     :   This is controller for VSSWorkOrderListJSON page.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   03/29/2014
 *
 *  Current Version :   V1.0
 *
 *  Revisiion Logs  :   V1.0 - Created
 *						V1.1 - Modified - Padmesh Soni (05/13/2014) - 140509 OrderSelect Dynamic pricing mod(Self Service Assignment)
 *                      
 **/
public without sharing class VSSWorkOrderListController {
    
    //Class properties
    private double minLat = 0.0;
    private double minLon = 0.0;
    private double maxLat = 0.0;
    private double maxLon = 0.0;
    private double contactLat = 0.0;
    private double contactLon = 0.0;
    private double contactServiceRange = 0.0;
    public boolean notAV = false;
    
    public string outputJSON{get; private set;}
    
    //Padmesh Soni (06/20/2014) - Support Case Record Type
	//Variable to hold list of RecordTypes of Case object
	List<RecordType> recordTypes;
	String idInClause;
	
	//Constructor definition 
    public VSSWorkOrderListController(){
        
        //Padmesh Soni (06/20/2014) - Support Case Record Type
		//Query record of Record Type of Case object
		recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
		
		//Padmesh Soni (06/20/2014) - Support Case Record Type
		//initialize string
		idInClause = '(\'';
		
		//Padmesh Soni (06/20/2014) - Support Case Record Type
		//Loop through RecordTypes query result
		for (RecordType recordType : recordTypes){
		
			//populate Ids in clause variable
		    idInClause += recordType.Id + '\',\'';
		}
		
		//Padmesh Soni (06/20/2014) - Support Case Record Type
		//Format string with removing last comma
		idInClause  = idInClause.substring(0,idInClause.length()-2);
		idInClause += ')';
    }
    
    /**
     *  @descpriton     :   This method is used to generate JSON string for particular URL parameters.
     *
     *  @param          :   
     *
     *  @return         :   void 
     **/
    public void doSearch(){
        
        //Instance of PageReference 
        PageReference p = ApexPages.currentPage();
        
        //Map to hold all parameters from Page URL
        Map<string, string> params = p.getParameters();
        
        //Query result of OS queues
        Group osGroup = [SELECT Id, Name FROM Group WHERE Name =: VSOAM_Constants.CASE_QUEUE_NAME_OS AND Type =: VSOAM_Constants.GROUP_TYPE_QUEUE];
        
        //Variable to hold Dynamic SOQL string
        String SOQL = '';
        
        //Create SOQL string for dynamic quering records  
        //Padmesh Soni (05/13/2014) - 140509 OrderSelect Dynamic pricing mod(Self Service Assignment)
        //New field "SSA_Map_Icon_Data__c" added into SOQL string 
        SOQL = 'SELECT Id, CaseNumber, OwnerId, Owner.Name, Owner.Alias, Geocode_cache__r.Formatted_Address__c, stop_color__c, Work_Ordered__c,'
                    + ' Geocode_Cache__r.Location__Latitude__s, Geocode_Cache__r.Location__Longitude__s, Serviceable__c, Routing_Priority__c, Self_Service_Available__c,' 
                    + ' Scheduled_Date__c, Status, Client_Number__c, Client__c, notes__c, Description, Geocode_Cache__r.notes__c, Work_Order_Type__c, '
                    + ' SSA_Map_Icon_Data__c FROM Case WHERE RecordTypeId NOT IN '+ idInClause + ' AND Routing_Serviceable__c = \'Yes\'';
        
        if(!Test.isRunningTest())
        	SOQL += 'and OwnerId =  \''+ osGroup.Id + '\'';   
        
        /***** Distance from Contact's home address(Other Address) within service range*****/
        try {
            
            //Getting User's contact's latitude from URL parameter minLat
            contactLat = double.valueOf(params.get(VSOAM_Constants.URL_PARAM_CONTACT_LAT));
            contactLon = double.valueOf(params.get(VSOAM_Constants.URL_PARAM_CONTACT_LON));
            contactServiceRange = double.valueOf(params.get(VSOAM_Constants.URL_PARAM_CONTACT_SERVICE_RANGE));
            
            //add filter into query String
            SOQL += ' AND DISTANCE(Geocode_Cache__r.Location__c, GEOLOCATION(' + contactLat + ',' + contactLon + '), \'mi\') < '+ contactServiceRange;
        } catch(Exception ex) {
        
        	ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
			ApexPages.addMessage(errorMsg);
        }
        
        /***** MAP EXTENTS *****/
        try {
            
            //Getting minimum latitude from URL parameter minLat
            minLat = double.valueOf(params.get(VSOAM_Constants.URL_PARAM_MINLAT));
            
            //add filter into query String
            SOQL += ' AND Geocode_Cache__r.Location__Latitude__s >=' + minLat;
        } catch(Exception ex) {
        
        	ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
			ApexPages.addMessage(errorMsg);
		}
        
        try {
            
            //Getting minimum latitude from URL parameter minLon
            minLon = double.valueOf(params.get(VSOAM_Constants.URL_PARAM_MINLON));
            
            //add filter into query String
            SOQL += ' AND Geocode_Cache__r.Location__Longitude__s >=' + minLon;
        } catch(Exception ex) {
        
        	ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
			ApexPages.addMessage(errorMsg);
		}
        
        try {
            
            //Getting minimum latitude from URL parameter maxLat
            maxLat = double.valueOf(params.get(VSOAM_Constants.URL_PARAM_MAXLAT));
            
            //add filter into query String
            SOQL += ' AND Geocode_Cache__r.Location__Latitude__s <=' + maxLat;
        } catch(Exception ex) {
        
        	ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
			ApexPages.addMessage(errorMsg);
		}
        
        try {
            
            //Getting minimum latitude from URL parameter maxLon
            maxLon = double.valueOf(params.get(VSOAM_Constants.URL_PARAM_MAXLON));
            
            //add filter into query String
            SOQL += ' AND Geocode_Cache__r.Location__Longitude__s <=' + maxLon;
        } catch(Exception ex) {
        
        	ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
			ApexPages.addMessage(errorMsg);
		}
        
        //Initialize list of OutputWrapper instance 
        List<outputWrapper> outPut = new List<outputWrapper>();
        
        //SOQL concatenate
        SOQL += ' ORDER BY Self_Service_Available__c DESC';
        
        //System.debug('SOQL:::'+ SOQL);
        
        //Removing leading ane trailing spaces
        SOQL = SOQL.trim();
        
        List<Case> ordersList = Database.query(SOQL);
        
        //Loop through queried records of Work Orders
        for(Case workOrder : ordersList) {
            
            //Wrapper class instancce
            OutputWrapper ow = new OutputWrapper();
            
            //Check for Self Service Available is false
            if(ordersList[0].Self_Service_Available__c == false) {
            	notAV = true;
            	break;
            }
            
            //assign value to Wrapper instance attributes 
            ow.Id = workOrder.Id;
            ow.ownerId = workOrder.OwnerId;
            ow.WON = workOrder.CaseNumber;
            ow.lat = workOrder.Geocode_Cache__r.Location__Latitude__s;
            ow.lon = workOrder.Geocode_Cache__r.Location__Longitude__s;
            ow.client = workOrder.Client__c;
            ow.ownerAlias = workOrder.Owner.Alias;
            ow.color = workOrder.stop_color__c;
            ow.ownerName = workOrder.Owner.Name;
            
            //Padmesh Soni (05/13/2014) - 140509 OrderSelect Dynamic pricing mod(Self Service Assignment)
	        //Assign field value to Wrapper element  
	        ow.SSAMapIcon = workOrder.SSA_Map_Icon_Data__c;
            
            outPut.add(ow);
        }
        
        if(notAV) {
        	
        	//Wrapper class instancce
            OutputWrapper ow = new OutputWrapper();
            ow.isAvailable = 'UnAvailable';
            outPut.add(ow);
        }
        
        outputJSON = JSON.serialize(outPut);
    }
    
    //Wrapper class
    private class OutputWrapper{
        
        public string isAvailable;
        public double lat;
        public double lon;
        public string WON;
        public string Id;
        public string ownerId;
        public string ownerAlias;
        public String ownerName;
        public string client;
        public string color;
        
        //Padmesh Soni (05/13/2014) - 140509 OrderSelect Dynamic pricing mod(Self Service Assignment)
        //New class variable to hold Pricing of Work Order as Wrapper element 
        public String SSAMapIcon;
    }
    
    public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}