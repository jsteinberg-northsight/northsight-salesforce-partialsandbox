@isTest
private class mapToStatusTriggerTest {
	
//    static testMethod void myUnitTest() {
//        RouteWrapper r = new RouteWrapper();
//		Case[] cases = new Case[]{};
//		
//		//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//		//Fetch record type of Account Sobject
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client' 
//													AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//		
//    	//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//    	//Create a test instance for Account sobject
//		Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//		for(Integer x=0;x<3;x++){
//			cases.add(new Case(Vendor_Code__c='AYSGRS', Street_Address__c='addr'+x,City__c=new string[]{'Slidell','Lacombe','Mandeville'}[x],
//									State__c='LA',Zip_Code__c = '7046'+x,Work_Ordered_Text__c = 'GRASS CUT', 
//									General_Order_Type_Notes__c = 'MUST BE COMPLETED BY 3:00PM',
//									
//									//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//    								//new field populated 
//									Client_Name__c = account.Id));
//		}
//		
//		upsert cases;
//		cases = [select Id,
//						CaseNumber,
//						Geocode_Cache__r.Formatted_Address__c,
//						Display_Street__c,
//						Display_City__c,
//						Display_State__c,
//						Display_Zip_Code__c,
//						Street_Address__c,
//						City__c,
//						State__c,
//						Zip_Code__c,
//						Client_Number__c,
//						Status_Health_Text__c,
//						Vendor_Status__c,
//						Scheduled_Date__c,
//						Work_Ordered_Text__c,
//						Notes__c,
//						Description 
//				 FROM Case];
//		for(Case c:cases){
//			r.stops.add(new RouteStopWrapper(c));
//		}
//		r.label = 'MyRoute';
//		r.EstCompletionDate = Date.Today().Format();
//		r.save();
//		Id[] ids = new Id[]{cases[0].Id,cases[1].Id,cases[2].Id};
//		Status__c[] statuses = [select Id,Expected_Upload_Date__c, Order_Status__c, Auto_Created__c, Route_Id__c, Route_Stop__c from Status__c where Case__c in :Ids];
//		System.Assert(statuses.Size()==3);
//		System.Assert(statuses[0].Order_Status__c=='On Route');
//		System.Assert(statuses[1].Order_Status__c=='On Route');
//		System.Assert(statuses[2].Order_Status__c=='On Route');
//		System.Assert(statuses[0].Auto_Created__c==true);
//		System.Assert(statuses[1].Auto_Created__c==true);
//		System.Assert(statuses[2].Auto_Created__c==true);
//		System.Assert(statuses[0].Expected_Upload_Date__c==Date.Today());
//		System.Assert(statuses[1].Expected_Upload_Date__c==Date.Today());
//		System.Assert(statuses[2].Expected_Upload_Date__c==Date.Today());
//		r.stops[0].innerStop().Work_Order__c = cases[2].Id;
//		r.stops[1].innerStop().Work_Order__c = cases[0].Id;
//		r.stops[2].innerStop().Work_Order__c = cases[1].Id;
//		update new Route_Stop__c[]{r.stops[0].innerStop(),r.stops[1].innerStop(),r.stops[2].innerStop()};
//		statuses = [select Id,Expected_Upload_Date__c, Order_Status__c, Auto_Created__c, Route_Id__c, Route_Stop__c from Status__c where Case__c in :Ids];
//		System.Assert(statuses.Size()==3);
//		System.Assert(statuses[0].Order_Status__c=='On Route');
//		System.Assert(statuses[1].Order_Status__c=='On Route');
//		System.Assert(statuses[2].Order_Status__c=='On Route');
//		System.Assert(statuses[0].Auto_Created__c==true);
//		System.Assert(statuses[1].Auto_Created__c==true);
//		System.Assert(statuses[2].Auto_Created__c==true);
//		System.Assert(statuses[0].Expected_Upload_Date__c==Date.Today());
//		System.Assert(statuses[1].Expected_Upload_Date__c==Date.Today());
//		System.Assert(statuses[2].Expected_Upload_Date__c==Date.Today());
//		delete r.stops[2].innerStop();
//		statuses = [select Id,Expected_Upload_Date__c, Order_Status__c, Auto_Created__c, Route_Id__c, Route_Stop__c from Status__c where Case__c in :Ids];
//		System.Assert(statuses.Size()==2);
//		System.Assert(statuses[0].Order_Status__c=='On Route');
//		System.Assert(statuses[1].Order_Status__c=='On Route');
//		System.Assert(statuses[0].Auto_Created__c==true);
//		System.Assert(statuses[1].Auto_Created__c==true);
//		System.Assert(statuses[0].Expected_Upload_Date__c==Date.Today());
//		System.Assert(statuses[1].Expected_Upload_Date__c==Date.Today());
//    }
}