@isTest
global class GenericHttpMock implements HttpCalloutMock {
/**
 *  Purpose         :   This is used for setting mock response of generic webservice.
 **/
 
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setBody('true');
        res.setStatus('OK');
        res.setStatusCode(200);
        return res;
    }
    
       
}