@isTest(seeAllData=false)
private class Test_BatchDeletePruvanOrderResponseLogs {
///**
// *  Purpose         :   This is used for testing and covering BatchDeletePruvanOrderResponseLogs class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/06/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Required Maintenance: Unit Test Improvements
// *	
// *	Coverage		:	85%
// **/
// 	
//    //test method added to check the functionality with code coverage
//    static testMethod void myUnitTest() {
//        
//        //List to hold test records of Pruvan_GetWorkOrder_Log__c
//        List<Pruvan_GetWorkOrderResponse_Log__c> pruvanGetWorkOrderRespLogs = new List<Pruvan_GetWorkOrderResponse_Log__c>();
//        
//        //Loop through till count 10
//        for(integer i=0; i < 10; i++) {
//        	
//        	//add new instance of Pruvan_GetWorkOrder_Log__c into list
//        	pruvanGetWorkOrderRespLogs.add(new Pruvan_GetWorkOrderResponse_Log__c(Name = 'Test'+ i, Log_Data__c = 'Testing is going on.'));
//        }
//        
//        //insert Pruvan Work Order logs here
//        insert pruvanGetWorkOrderRespLogs;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Execute batch here
//        Database.executeBatch(new BatchDeletePruvanOrderResponseLogs(), 200);
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //Query result of Pruvan_GetWorkOrder_Log__c records
//        pruvanGetWorkOrderRespLogs = [SELECT Id FROM Pruvan_GetWorkOrderResponse_Log__c];
//        
//        //Assert statement here
//        System.assertEquals(0, pruvanGetWorkOrderRespLogs.size());
//    }
}