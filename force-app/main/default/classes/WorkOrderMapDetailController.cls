/**
 *	Description		:	This is controller for AssignmentMapSearchJSON page.
 *
 *	Created By		:	Padmesh Soni
 *
 *	Created Date	:	05/06/2014
 *
 *	Current Version	:	V1.1
 *
 *	Revisiion Logs	:	V1.0 - Created - Padmesh Soni (05/06/2014) - Enhancement of functionality on OAM project. 
 *								1. OAM-44: The Vendor's name should be clickable into their contact record.
 **/
public with sharing class WorkOrderMapDetailController {
	
	public Case workOrder {get; set;}
	public String vendorContactId {get; set;}
	
	public WorkOrderMapDetailController(ApexPages.StandardController SC) {
		
		workOrder = (Case)SC.getRecord();
		vendorContactId = '';
		inIt();
	}
	
	public void inIt() {
		
		if(workOrder.OwnerId.getSObjectType().getDescribe().getName() == 'User') {
			
			//List to hold querying User record
			List<User> vendors = [SELECT Id, ContactId From User WHERE Id =:workOrder.OwnerId];
			
			if(vendors[0].ContactId != null) {
				
				vendorContactId = vendors[0].ContactId;
			}
		} 
	}
}