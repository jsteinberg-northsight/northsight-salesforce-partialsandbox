@isTest(seeAllData=false)
private class Test_VendorSelfServeMapController {
///**
// *  Purpose         :   This is used for testing and covering VendorSelfServeMapController.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   04/01/2014
// *
// *  Current Version :   V1.2
// *
// *  Revision Log    :   V1.0 - Created
// *                      V1.1 - Modified - Padmesh Soni (08/28/2014) - SM-183 Last Login isn't updated on Contact. 
// *                      V1.2 - Modified - Padmesh Soni (1/29/2015) - SM-272:Update reassignment fee trigger to include the Client_Name__c field
// * 
// *  Coverage        :   96% - V1.0
// *                      93% - V1.1
// *                      97% - V1.2
// **/
//    
//    private static List<Account> accounts;
//    private static List<Contact> contacts;
//    private static Profile newProfile;
//    private static List<User> users;
//    private static Group queues;
//    
//    //Method to create all test method
//    private static void generateTestData(integer countData){
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP 
//        //create a new user with the Customer Portal Manager Standard profile
//        //newProfile = [SELECT Id FROM Profile WHERE Name = 'Customer Portal OrderSelect'];
//        newProfile = [SELECT Id FROM Profile WHERE Name =: VSOAM_Constants.PROFILE_NAME_PARTNER_COMMUNITY_VENDORS];
//        
//        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
//            //create a new user with the profile   
//            queues = new Group(Name='Queue',Type='Queue');
//            insert queues;
//            
//            //Queue sObject   
//            QueueSobject queueObj = new QueueSobject(QueueId = queues.Id, SobjectType = 'Case');
//            insert queueObj;
//        }
//        //Loop through countData time
//        for(integer i = 0; i <= countData; i++) {
//        
//            /******** PRE-TEST SETUP create a new account *********/
//            accounts.add(new Account(Name = 'Tester Account' + i,RecordTypeId='0124000000011Q2'));
//        }
//        
//        insert accounts;
//        
//        //Loop through countData time
//   //Loop through countData time
//        for(integer i = 0; i <= countData; i++) {
//        
//            //--------------------------------------------------------
//            //PRE-TEST SETUP
//            //create a new contact
//            //Processor Record Type ID: 01240000000UbtfAAC
//            Contact newContact = new Contact(LastName = 'Tester'+i, AccountId = accounts[0].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active', RecordTypeId = '01240000000UbtfAAC');
//                        newContact.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//                        newContact.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//                        newContact.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//                        newContact.Shrub_Trim__c = 3;//Pricing...required 
//                        newContact.REO_Grass_Cut__c = 3;//Pricing
//                        newContact.Pay_Terms__c = 'Net 30';//Terms required
//                        newContact.Trip_Charge__c = 25;//Pricing
//                        newContact.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//                        newContact.Date_Active__c = Date.Today();//Date Active must be set. 
//                        newContact.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//                        newContact.MailingStreet = '30 Turnberry Dr';
//                        newContact.MailingCity = 'La Place';
//                        newContact.MailingState = 'LA';
//                        newContact.MailingPostalCode = '70068';
//                        newContact.OtherStreet = '30 Turnberry Dr';
//                        newContact.OtherCity = 'La Place';
//                        newContact.OtherState = 'LA';
//                        newContact.OtherPostalCode = '70068';
//                        newContact.Other_Address_Geocode__Latitude__s = 30.00;
//                        newContact.Other_Address_Geocode__Longitude__s = -90.00;
//                        contacts.add(newContact);
//        }   
//        
//        insert contacts;
//        
//        //Loop through countData time
//        for(integer i = 0; i <= countData; i++) {
//        
//            if(i==0) {
//                //create a new user with the profile
//                users.add(new User(alias = 'standt'+i, contactId = contacts[i].Id, email = Math.random()+Math.random()+Math.random()+i+'@northsighttestorg.com', emailencodingkey = 'UTF-8',
//                                        lastname = 'Testing'+i, languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//                                        timezonesidkey = 'Australia/Perth', username = Math.random()+Math.random()+Math.random()+i+'@northsighttestorg.com', isActive = true));
//            } else {
//                
//                //create a new user with the profile 
//                users.add(new User(alias = 'standt'+i, contactId = contacts[i].Id, email = Math.random()+Math.random()+Math.random()+i+'@northsighttestorg.com', emailencodingkey = 'UTF-8',
//                                        lastname = 'Testing'+i, languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//                                        timezonesidkey = 'America/Los_Angeles', username = Math.random()+Math.random()+Math.random()+i+'@northsighttestorg.com', isActive = true));
//            }
//        }
//        
//        insert users;
//    }
//    
//    //Test method is used to test functionality of VendorSelfServeMapController
//    static testMethod void testVendorSelfServeMapController() {
//        
//        //initialize variables
//        accounts = new List<Account>();
//        contacts = new List<Contact>();
//        users = new List<User>();
//        
//        generateTestData(1);
//        
//        //Query record of Record Type of Account and Case objects
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: VSOAM_Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: VSOAM_Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: VSOAM_Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: VSOAM_Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(2, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsOnOrders = new List<Account>();
//        accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsOnOrders;
//        
//        Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//        insert property;
//        
//        //List to hold Case records
//        List<Case> wOrders = new List<Case>();
//        wOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today(), Status = VSOAM_Constants.CASE_STATUS_OPEN, 
//                                    Approval_Status__c = VSOAM_Constants.CASE_APPROVAL_STATUS_REJECTED,
//                                    Client__c = VSOAM_Constants.CASE_CLIENT_SG, Work_Order_Type__c = VSOAM_Constants.CASE_WORK_ORDER_TYPE_ROUTINE, 
//                                    State__c = VSOAM_Constants.CASE_STATE_TX));
//        wOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = VSOAM_Constants.CASE_STATUS_OPEN, 
//                                    Approval_Status__c = VSOAM_Constants.CASE_APPROVAL_STATUS_REJECTED,
//                                    Client__c = VSOAM_Constants.CASE_CLIENT_SG, Work_Order_Type__c = VSOAM_Constants.CASE_WORK_ORDER_TYPE_ROUTINE, 
//                                    State__c = VSOAM_Constants.CASE_STATE_TX));
//        wOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today(), Status = VSOAM_Constants.CASE_STATUS_OPEN, 
//                                    Approval_Status__c = VSOAM_Constants.CASE_APPROVAL_STATUS_REJECTED, 
//                                    Client__c = VSOAM_Constants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = VSOAM_Constants.CASE_WORK_ORDER_TYPE_MAINTENANCE, 
//                                    State__c = VSOAM_Constants.CASE_STATE_LA));
//        wOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = VSOAM_Constants.CASE_STATUS_OPEN, 
//                                    Approval_Status__c = VSOAM_Constants.CASE_APPROVAL_STATUS_REJECTED, 
//                                    Client__c = VSOAM_Constants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = VSOAM_Constants.CASE_WORK_ORDER_TYPE_MAINTENANCE, 
//                                    State__c = VSOAM_Constants.CASE_STATE_AR));
//        
//        //insert case records
//        insert wOrders;
//        
//        //Query result of OS queues
//        Group osGroup = [SELECT Id, Name FROM Group WHERE Name =: VSOAM_Constants.CASE_QUEUE_NAME_OS];
//        
//        List<Case> workOrders = new List<Case>();
//        
//        //Loop through case list
//        for(Case wo : wOrders) {
//            
//            //Code modified - Padmesh Soni (1/29/2015) - SM-272:Update reassignment fee trigger to include the Client_Name__c field
//            //New field "Client_Name__c" populated
//            workOrders.add(new Case(Id = wo.Id, OwnerId = osGroup.Id, Client_Name__c = accountsOnOrders[0].Id));    
//        }
//        
//        update workOrders;
//        
//        //Test start here
//        Test.startTest();
//        
//        //Initialize controller of AssignmentMap 
//        VendorSelfServeMapController controllerExt = new VendorSelfServeMapController();
//        
//        //assert statement 
//        System.assertEquals(false, VendorSelfServeMapController.isAvailable);
//        
//        //initialize array of string
//        String[] woIds = new String[workOrders.size()];
//        
//        //query user's record of currently inserted user
//        users = [SELECT Id, Name, Contact.Order_Count__c, Contact.Grass_Cut_Cap__c FROM User WHERE Id IN: users ORDER BY Name];
//        
//        
//        //quering result of case records
//        workOrders = [SELECT Id, CaseNumber FROM Case];
//        
//        //assert statement
//        System.assertEquals(4, workOrders.size());
//        
//        String tempJSON = '';
//        
//        //Loop through case list
//        for(Case wo : workOrders) {
//            
//            //create JSON string of CaseNumbers
//            tempJSON +=  wo.CaseNumber + ',';
//        }
//        
//        //Loop through case list
//        for(integer i=0; i<workOrders.size(); i++) {
//            
//            woIds[i] = workOrders[i].Id;
//        }
//        
//        integer wOSize = workOrders.size();
//        
//        //test as current test user
//        System.runAs(users[0]) {
//        
//            //Initialize controller of AssignmentMap 
//            VendorSelfServeMapController controller = new VendorSelfServeMapController();
//            
//            //assert statement
//            System.assertEquals(users[0].Name, controller.currentUser.Name);
//            
//            //Code added - Padmesh Soni (08/28/2014) - SM-183 Last Login isn't updated on Contact
//            //Call checkIp method to cover the new code of main class
//            controller.checkIp();
//            
//            //call refreshUser method
//            controller.refreshUser();
//            
//            //call closepopup method
//            controller.showPopup();
//            
//            //assert statement
//            System.assertEquals(true, controller.displayPopup);
//            
//            //call closepopup method
//            controller.closePopup();
//            
//            //assert statement
//            System.assertEquals(false, controller.displayPopup);
//            
//            //initialize with blank
//            controller.ordersJSON = tempJSON;
//            
//            //Checking for string endswith comma (,) character
//            if(controller.ordersJSON.endsWith(',')) {
//                
//                //format the OrdersJson string of controller
//                controller.ordersJSON = controller.ordersJSON.substring(0, controller.ordersJSON.lastIndexOf(','));
//            }
//            
//            //call controller's claimed record
//            controller.getOrdersToBeClaimed();
//            
//            //assert statements
//            System.assertEquals(wOSize, controller.selCounter);
//            
//            try {
//                
//                //call remoteaction method
//                VendorSelfServeMapController.checkAssignWorkOrders(woIds);
//            } catch(Exception e) {
//                
//                //Total orders to be assigned to user
//                integer totalOrdersToBeAssign = woIds.size() + integer.valueOf(users[0].Contact.Order_Count__c);
//                
//                //Total removable orders for User's contact
//                integer limitAssign = totalOrdersToBeAssign - integer.valueOf(users[0].Contact.Grass_Cut_Cap__c);
//                
//                //assert statement
//                System.assertEquals('You currently have ' + users[0].Contact.Order_Count__c + ' orders assigned to you. Your grass cut cap is '+
//                                users[0].Contact.Grass_Cut_Cap__c +', but you attempted to assign yourself an additional ' + woIds.size() + 
//                                ' orders. Please deselect '+ limitAssign + ' orders and try again.', e.getMessage());
//            }
//        }
//        
//        contacts[0].Grass_Cut_Cap__c = 5;
//        update contacts[0];
//        
//        workOrders = [SELECT Id, CaseNumber FROM Case];
//            
//        //assert statement
//        System.assertEquals(4, workOrders.size());
//        
//        //Loop through case list
//        for(integer i=0; i<workOrders.size(); i++) {
//            
//            //populate array of Cases Id
//            woIds[i] = workOrders[i].Id;
//        }
//        
//        //test as current test user
//        System.runAs(users[0]) {
//        
//            //call remoteaction method
//            VendorSelfServeMapController.checkAssignWorkOrders(woIds);
//            
//            //querying records of Case
//            for(Case workOrder : [SELECT Id, CaseNumber, OwnerId FROM Case WHERE Id IN: woIds]) {
//                
//                System.assertEquals(users[0].Id, workOrder.OwnerId);
//            }
//        }
//        
//        //querying records of Case
//        workOrders = [SELECT Id, CaseNumber, OwnerId FROM Case];
//        
//        //initalize blank value to JSON string
//        tempJSON = '';
//        
//        //Loop through case list
//        for(Case wo : workOrders) {
//            
//            //create JSON string of CaseNumbers
//            tempJSON +=  wo.CaseNumber + ',';
//        }
//        
//        //update the Case's Owner to current user
//        Case newCase = new Case(Id= workOrders[0].Id, OwnerId = UserInfo.getUserId());
//        update newCase;
//        
//        controllerExt = new VendorSelfServeMapController();
//        
//        //create JSON string of CaseNumbers     
//        controllerExt.ordersJSON = tempJSON;
//        
//        //Checking for string endswith comma (,) character
//        if(controllerExt.ordersJSON.endsWith(',')) {
//            
//            //format the OrdersJson string of controller
//            controllerExt.ordersJSON = controllerExt.ordersJSON.substring(0, controllerExt.ordersJSON.lastIndexOf(','));
//        }
//        
//        //call remoteaction method
//        controllerExt.getOrdersToBeClaimed();
//        
//        //assert statement
//        //System.assertEquals(true, VendorSelfServeMapController.checkAvailable);
//        
//        //initialize new Orders list
//        List<Case> newOrders = new List<Case>();
//        
//        //Loop through new Orders
//        for(Case ordr : [SELECT Id, OwnerId FROM Case WHERE Id IN: workOrders]) {
//            
//            //add order into list to update
//            newOrders.add(new Case(Id = ordr.Id, OwnerId = Userinfo.getUserId()));
//        }
//        
//        //update the newOrders
//        update newOrders;
//        
//        //call remoteaction method
//        controllerExt.getOrdersToBeClaimed();
//        
//        System.assert(controllerExt.infoMessage != '');
//        
//        //initialize with blank array
//        woIds = new String[]{};
//        
//        System.runAs(users[0]) {
//            
//            try {
//                
//                //call remoteaction method
//                VendorSelfServeMapController.checkAssignWorkOrders(woIds);
//            } catch(Exception e) {
//                
//                //assert statement
//                System.assertEquals(Label.VOAMP_Not_Selected, e.getMessage());
//            }
//        }
//        
//        //run as another time zone users
//        System.runAs(users[1]) {
//         
//            VendorSelfServeMapController controller = new VendorSelfServeMapController();
//            controller.refreshUser();
//        }
//        
//        try {
//            
//            //call remoteaction method
//            VendorSelfServeMapController.checkAssignWorkOrders(woIds);
//        } catch(Exception e) {
//            
//            //assert statement
//            System.assertEquals(Label.VOAM_NO_Access, e.getMessage());
//        }
//        
//        //Test stops here
//        Test.stopTest();
//    }
}