/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
global class ReCaptchaMockRespGenerator implements HttpCalloutMock {
/**
 *  Purpose         :   This is used for setting mock response of ReCaptcha webservice.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   09/09/2014
 *
 *	Current Version	:	V1.0
 *
 *	Revision Log	:	V1.0 - Created - SM-204: Customize self-register apex
 **/
 
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setBody('true');
        res.setStatus('OK');
        res.setStatusCode(200);
        return res;
    }
}