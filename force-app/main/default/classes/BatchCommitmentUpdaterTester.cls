@isTest(SeeAllData=false)
public without sharing class BatchCommitmentUpdaterTester {
//    private static Case newCase;
//    private static Case newCase2;
//    private static Case newCase3;
//    private static Datetime oneHour;
//    private static Work_Order_Setup__c wos;
//    
//    //Code added - Declare new Case record instance- Padmesh Soni(02/12/2014) 
//    //As per "Padmesh Assignment - 2/10/2014 11th Hour Client Commitment Batch" 
//    private static Case newCase4;
//    private static List<Case> workOrders;
//        
//    private static void generateTestCases() {
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new account
//        Account newAccount = new Account(
//            Name = 'Tester Account'
//        );
//        insert newAccount;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new contact
//        Contact newContact = new Contact(
//            LastName = 'Tester',
//            AccountId = newAccount.Id,
//            Pruvan_Username__c = 'NSBobTest',
//            MailingStreet = '30 Turnberry Dr',
//            MailingCity = 'La Place',
//            MailingState = 'LA',
//            MailingPostalCode = '70068',
//            OtherStreet = '30 Turnberry Dr',
//            OtherCity = 'La Place',
//            OtherState = 'LA',
//            OtherPostalCode = '70068',
//            Other_Address_Geocode__Latitude__s = 30.00,
//            Other_Address_Geocode__Longitude__s = -90.00
//        );
//        insert newContact;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create work order setup object
//        wos = new Work_Order_Setup__c(
//            Work_Ordered_Text__c = 'Grass Cutting', 
//            Work_Ordered__c = 'Grass Recut', 
//            Order_Type__c = 'Routine', 
//            Client__c = 'NOT-SG'
//        );
//        insert wos;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //set oneHour = Datetime.now - 3hours
//        oneHour = Datetime.now().addHours(-1);
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new test case
//        newCase = new Case();
//        newCase.street_address__c = '1234 Street';
//        newCase.state__c = 'TX';
//        newCase.city__c = 'Austin';
//        newCase.zip_code__c = '78704';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        
//        //Code modified - Modified value "Client__c =: Constants.CASE_CLIENT_SG" into Case record - Padmesh Soni(02/12/2014) 
//        //As per "Padmesh Assignment - 2/10/2014 11th Hour Client Commitment Batch"
//        newCase.Client__c = Constants.CASE_CLIENT_SG;
//        
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'Grass Recut';
//        newCase.Approval_Status__c = 'Approved';
//        newCase.Due_Date__c = Date.today();
//        newCase.Client_Commit_Date__c = Date.today();
//        newCase.Last_Open_Verification_DateTime__c = oneHour;
//        newCase.Work_Completed__c = 'Yes';
//        newCase.Date_Serviced__c = Date.today();
//        newCase.Updated_Date_Time__c = Datetime.now();
//        
//        insert newCase;
//        
//        //Query record of Record Type of Account and Case objects
//        //Padmesh Soni (06/19/2014) - Support Case Record Type
//        //Query one more record type of Case Object "SUPPORT"    
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT
//                                            AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true ORDER BY DeveloperName];
//        
//        workOrders = new List<Case>();
//        
//        //Padmesh Soni (06/19/2014) - Support Case Record Type
//        //New test record created
//        workOrders.add(new Case(RecordTypeId = recordTypes[0].Id, street_address__c = '1234 Street', state__c = 'TX', city__c = 'Austin',zip_code__c = '78704',
//                                    Vendor_code__c = 'SRSRSR', Work_Order_Type__c = 'Routine', Client__c = Constants.CASE_CLIENT_SG,
//                                    Loan_Type__c = 'REO',ContactId = newContact.Id, Work_Ordered__c = 'Grass Recut', Approval_Status__c = 'Approved',
//                                    Due_Date__c = Date.today(), Client_Commit_Date__c = Date.today(), Last_Open_Verification_DateTime__c = oneHour, 
//                                    Work_Completed__c = 'Yes', Date_Serviced__c = Date.today(), Updated_Date_Time__c = Datetime.now()));
//        
//        insert workOrders;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new test case
//        newCase2 = new Case();
//        newCase2.street_address__c = '1234 Street';
//        newCase2.state__c = 'TX';
//        newCase2.city__c = 'Austin';
//        newCase2.zip_code__c = '78704';
//        newCase2.Vendor_code__c = 'SRSRSR';
//        newCase2.Work_Order_Type__c = 'Routine';
//        
//        //Code modified - Modified value "Client__c =: Constants.CASE_CLIENT_SG" into Case record - Padmesh Soni(02/12/2014) 
//        //As per "Padmesh Assignment - 2/10/2014 11th Hour Client Commitment Batch"
//        newCase2.Client__c = Constants.CASE_CLIENT_SG;
//        
//        newCase2.Loan_Type__c = 'REO';
//        newCase2.ContactId = newContact.Id;
//        newCase2.Work_Ordered__c = 'Grass Recut';
//        newCase2.Approval_Status__c = 'Pending';
//        newCase2.Due_Date__c = Date.today();
//        newCase2.Client_Commit_Date__c = Date.today();
//        newCase2.Last_Open_Verification__c = Date.today();
//        newCase2.Work_Completed__c = 'Yes';
//        newCase2.Date_Serviced__c = Date.today();
//        newCase2.Status = 'Rush';
//        
//        insert newCase2;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new test case
//        newCase3 = new Case();
//        newCase3.street_address__c = '1234 Street';
//        newCase3.state__c = 'TX';
//        newCase3.city__c = 'Austin';
//        newCase3.zip_code__c = '78704';
//        newCase3.Vendor_code__c = 'SRSRSR';
//        newCase3.Work_Order_Type__c = 'Routine';
//        
//        //Code modified with modified value "Client__c =: Constants.CASE_CLIENT_SG" into Case record - Padmesh Soni(02/12/2014) 
//        //As per "Padmesh Assignment - 2/10/2014 11th Hour Client Commitment Batch"
//        newCase3.Client__c = Constants.CASE_CLIENT_SG;
//        
//        newCase3.Loan_Type__c = 'REO';
//        newCase3.ContactId = newContact.Id;
//        newCase3.Work_Ordered__c = 'Grass Recut';
//        newCase3.Approval_Status__c = 'Not Started';
//        newCase3.Due_Date__c = Date.today();
//        newCase3.Client_Commit_Date__c = Date.today();
//        newCase3.Last_Open_Verification__c = Date.today();
//        
//        insert newCase3;
//        
//        
//        //Code added - Creating new Case record which is not pick by batch to process- Padmesh Soni(02/12/2014) 
//        //As per "Padmesh Assignment - 2/10/2014 11th Hour Client Commitment Batch"
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new test case
//        newCase4 = new Case();
//        newCase4.street_address__c = '1234 Street';
//        newCase4.state__c = 'TX';
//        newCase4.city__c = 'Austin';
//        newCase4.zip_code__c = '78704';
//        newCase4.Vendor_code__c = 'SRSRSR';
//        newCase4.Work_Order_Type__c = 'Routine';
//        newCase4.Client__c = 'Not-SG';
//        newCase4.Loan_Type__c = 'REO';
//        newCase4.ContactId = newContact.Id;
//        newCase4.Work_Ordered__c = 'Grass Recut';
//        newCase4.Approval_Status__c = 'Not Started';
//        newCase4.Due_Date__c = Date.today();
//        newCase4.Client_Commit_Date__c = Date.today();
//        newCase4.Last_Open_Verification__c = Date.today().addDays(1);
//        
//        insert newCase4;
//        /*
//        Map<Id, Case> approvedOrders = new Map<Id, Case>([SELECT Id, Updated_Date_Time__c FROM Case WHERE Due_Date__c <=: currentDay AND Approval_Status__c = 'Approved' AND Last_Open_Verification_DateTime__c >: twoHours AND (Client_Commit_Date__c = null OR Client_Commit_Date__c <=: currentDay)]);
//        
//        Map<Id, Case> pendingOrders = new Map<Id, Case>([SELECT Id, Date_Serviced1__c FROM Case WHERE Due_Date__c <=: currentDay AND Approval_Status__c = 'Pending' AND Special_Review__c = 'No' AND Last_Open_Verification__c =: currentDay AND (Client_Commit_Date__c = null OR Client_Commit_Date__c <=: currentDay)]);
//        
//        Map<Id, Case> allOtherOrders = new Map<Id, Case>([SELECT Id FROM Case WHERE Id NOT IN : casesToAvoid AND Due_Date__c <=: currentDay AND Approval_Status__c != 'Approved' AND Last_Open_Verification__c =: currentDay AND (Client_Commit_Date__c = null OR Client_Commit_Date__c <=: currentDay)]);
//        */
//        Date currentDay = Date.today();
//        Datetime twoHours = Datetime.now().addHours(-2);
//        Set<Id> casesToAvoid = new Set<Id>();
//        
//        Case approvedCase = [SELECT Id, Updated_Date_Time__c FROM Case WHERE Commit_By_Date__c <=: currentDay AND Approval_Status__c = 'Approved' AND Last_Open_Verification_DateTime__c >: twoHours AND (Client_Commit_Date__c = null OR Client_Commit_Date__c <=: currentDay) AND Id =: newCase.Id];
//            casesToAvoid.add(approvedCase.Id);
//            system.assertEquals(approvedCase.Id, newCase.Id);
//        Case pendingCase = [SELECT Id, Date_Serviced1__c FROM Case WHERE Commit_By_Date__c <=: currentDay AND Approval_Status__c = 'Pending' AND Special_Review__c = 'No' AND Last_Open_Verification__c =: currentDay AND (Client_Commit_Date__c = null OR Client_Commit_Date__c <=: currentDay) AND Id =: newCase2.Id];
//            casesToAvoid.add(pendingCase.Id);
//            system.assertEquals(pendingCase.Id, newCase2.Id);
//        Case otherCase = [SELECT Id FROM Case WHERE Id NOT IN : casesToAvoid AND Commit_By_Date__c <=: currentDay AND Approval_Status__c != 'Approved' AND Last_Open_Verification__c =: currentDay AND (Client_Commit_Date__c = null OR Client_Commit_Date__c <=: currentDay) AND Id =: newCase3.Id];
//            system.assertEquals(otherCase.Id, newCase3.Id);
//    }
//    
//    private static void createCommitments(){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create new client commitments
//        Client_Commitment__c approvedClientCommit = new Client_Commitment__c(
//            Work_Order__c = newCase.Id,
//            Root_Cause_of_Delay__c = 'Manually created approved commit',
//            Current_Status_of_Order__c = 'Manually created approved commit',
//            Public_Comments__c = 'Manually created approved commit',
//            Autogen__c = false,
//            Sync_Status__c = 'Sent',
//            Commitment_Date__c = Date.today().addDays(4)
//        );
//        insert approvedClientCommit;
//
//        Client_Commitment__c pendingClientCommit = new Client_Commitment__c(
//            Work_Order__c = newCase2.Id,
//            Root_Cause_of_Delay__c = 'Manually created pending commit',
//            Current_Status_of_Order__c = 'Manually created pending commit',
//            Public_Comments__c = 'Manually created pending commit',
//            Autogen__c = false,
//            Sync_Status__c = 'Unsent',
//            Commitment_Date__c = Date.today().addDays(4)
//        );
//        insert pendingClientCommit;
//
//        Client_Commitment__c otherClientCommit = new Client_Commitment__c(
//            Work_Order__c = newCase3.Id,
//            Root_Cause_of_Delay__c = 'Manually created other commit',
//            Current_Status_of_Order__c = 'Manually created other commit',
//            Autogen__c = false,
//            Sync_Status__c = 'In Progress',
//            Commitment_Date__c = Date.today().addDays(4)
//        );
//        insert otherClientCommit;
//    }
//    
//    //--------------------------------------------------------
//    //TESTS
//    //Unit tests for method testing and regression testing. 
//    //--------------------------------------------------------
//        
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - test that the BatchCommitmentUpdater is functioning correctly
//    public static testmethod void testBatchCommitmentUpdater(){
//        generateTestCases();
//        
//        //run the BatchCommitmentUpdater
//        system.Test.startTest();
//        BatchCommitmentUpdater testbatch = new BatchCommitmentUpdater();
//        database.executeBatch(testbatch, 200);
//        system.Test.stopTest();
//        
//        Case approvedCase = [SELECT Updated_Date_Time__c FROM Case WHERE Id =: newCase.Id];//select back the updated date time field from newCase
//        Date dateOfDateTime = approvedCase.Updated_Date_Time__c.date();//pull out the date part of Updated_Date_Time__c
//        //format the dateOfDateTime as MM/DD/YYYY
//        String approvedFormattedDate = string.valueOf(dateOfDateTime.month()) +'/'+ string.valueOf(dateOfDateTime.day()) +'/'+ string.valueOf(dateOfDateTime.year());
//        
//        Case pendingCase = [SELECT Date_Serviced1__c FROM Case WHERE Id =: newCase2.Id];//select back the date serviced1 field from newCase2
//        //format Date_Serviced1__c as MM/DD/YYYY
//        String pendingFormattedDate = string.valueOf(pendingCase.Date_Serviced1__c.month()) +'/'+ string.valueOf(pendingCase.Date_Serviced1__c.day()) +'/'+ string.valueOf(pendingCase.Date_Serviced1__c.year());
//        
//        //select the client commitments that should have been created off the test orders in the batch
//        List<Client_Commitment__c> testCommitments = [Select Work_Order__c, Commitment_Date__c, Current_Status_of_Order__c, Root_Cause_of_Delay__c, Public_Comments__c, Autogen__c From Client_Commitment__c];
//        system.assertEquals(3, testCommitments.size());//assert that there should be 3 client commitments for the 3 test orders
//        
//        for(Client_Commitment__c cc : testCommitments){//loop thru the client commitments...
//            if(cc.Work_Order__c == newCase.Id){//if the work order on the current client commitment = newCase.Id
//                system.assertEquals(Date.today().addDays(2), cc.Commitment_Date__c);
//                system.assertEquals('Delay in Receiving Work', cc.Root_Cause_of_Delay__c);
//                system.assertEquals('Field Completed Pending Update', cc.Current_Status_of_Order__c);
//                system.assertEquals('Work order has been complete and was updated at ' + approvedFormattedDate + '.', cc.Public_Comments__c);
//                system.assertEquals(true, cc.Autogen__c);   
//            }
//            else if(cc.Work_Order__c == newCase2.Id){//if the work order on the current client commitment = newCase2.Id
//                system.assertEquals(Date.today().addDays(2), cc.Commitment_Date__c);
//                system.assertEquals('Field Capacity', cc.Root_Cause_of_Delay__c);
//                system.assertEquals('Field Completed Pending Update', cc.Current_Status_of_Order__c);
//                system.assertEquals('This work order was completed on ' + pendingFormattedDate + ' and will be updated soon.', cc.Public_Comments__c);
//                system.assertEquals(true, cc.Autogen__c);   
//            }
//            else{//if the work order on the current client commitment = newCase3.Id
//                system.assertEquals(Date.today().addDays(2), cc.Commitment_Date__c);
//                system.assertEquals('Field Capacity', cc.Root_Cause_of_Delay__c);
//                system.assertEquals('Pending Field Complete', cc.Current_Status_of_Order__c);
//                system.assertEquals(null, cc.Public_Comments__c);
//                system.assertEquals(true, cc.Autogen__c);
//            }
//        }
//    }
//    
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - test that the BatchCommitmentUpdater will not create commits where there are already existing commits
//    public static testmethod void testUpdaterWithExistingCommits(){
//        generateTestCases();
//        createCommitments();
//        
//        //run the BatchCommitmentUpdater
//        system.Test.startTest();
//        BatchCommitmentUpdater testbatch = new BatchCommitmentUpdater();
//        database.executeBatch(testbatch, 200);
//        system.Test.stopTest();
//        
//        //select the client commitments that should have been created off the test orders in the batch
//        List<Client_Commitment__c> testCommitments = [Select Work_Order__c, Commitment_Date__c, Current_Status_of_Order__c, Root_Cause_of_Delay__c, Public_Comments__c, Autogen__c, Sync_Status__c From Client_Commitment__c];
//        system.assertEquals(3, testCommitments.size());//assert that there should be 3 client commitments for the 3 test orders
//        
//        for(Client_Commitment__c cc : testCommitments){//loop thru the client commitments...
//            if(cc.Work_Order__c == newCase.Id){//if the work order on the current client commitment = newCase.Id
//                system.assertEquals('Manually created approved commit', cc.Root_Cause_of_Delay__c);
//                system.assertEquals('Manually created approved commit', cc.Current_Status_of_Order__c);
//                system.assertEquals('Manually created approved commit', cc.Public_Comments__c);
//                system.assertEquals(false, cc.Autogen__c);
//                system.assertEquals('Sent', cc.Sync_Status__c); 
//                system.assertEquals(Date.today().addDays(4), cc.Commitment_Date__c);
//            }
//            else if(cc.Work_Order__c == newCase2.Id){//if the work order on the current client commitment = newCase2.Id
//                system.assertEquals('Manually created pending commit', cc.Root_Cause_of_Delay__c);
//                system.assertEquals('Manually created pending commit', cc.Current_Status_of_Order__c);
//                system.assertEquals('Manually created pending commit', cc.Public_Comments__c);
//                system.assertEquals(false, cc.Autogen__c);
//                system.assertEquals('Unsent', cc.Sync_Status__c);
//                system.assertEquals(Date.today().addDays(4), cc.Commitment_Date__c);
//            }
//            else{//if the work order on the current client commitment = newCase3.Id
//                system.assertEquals('Manually created other commit', cc.Root_Cause_of_Delay__c);
//                system.assertEquals('Manually created other commit', cc.Current_Status_of_Order__c);
//                system.assertEquals(null, cc.Public_Comments__c);
//                system.assertEquals(false, cc.Autogen__c);
//                system.assertEquals('In Progress', cc.Sync_Status__c);  
//                system.assertEquals(Date.today().addDays(4), cc.Commitment_Date__c);
//            }
//        }
//    }
}