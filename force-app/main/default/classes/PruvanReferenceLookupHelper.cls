public without sharing class PruvanReferenceLookupHelper {
	private static Map<string,object> referenceRequest;
	private static Map<string,object> referenceResponse;
	private static string caseNum;
	private static string responseJSON;
	
	public static string fetchReferenceLookup(string input){
		//get the request data into a map of string to object var
		referenceRequest = (map<string,object>)JSON.deserializeUntyped(input);
		
		//get the reference number from the request map into a string var
		caseNum = (string)referenceRequest.get('workOrderNumber');
		List<String> formatCaseNumber = caseNum.split('::');
		caseNum = formatCaseNumber[formatCaseNumber.size()-1];
		
		//instantiate the response map
		referenceResponse = new Map<string, object>();
		try{
			//attempt to fill the response map with the reference number and a blank error
			referenceResponse.put('referenceNumber', workOrderReferenceLookup());
			referenceResponse.put('error', '');
		}
		catch(Exception e){
			//fill the response map with the error message if the try fails
			referenceResponse.put('error', e.getMessage()+' '+e.getStackTraceString()+' : '+caseNum);
		}
		
		//return the response map as a Json string
		return responseJSON = JSON.serialize(referenceResponse);
	}
	
	
	//return the ref. # of a work order based on the case number
	private static string workOrderReferenceLookup(){
		
		//Padmesh Soni (06/26/2014) - Support Case Record Type
		//Query record of Record Type of Case object
		List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
														AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
		
        //Padmesh Soni (06/26/2014) - Support Case Record Type
    	//New filter criteria added for RecordTypeId
	    Case caseRecord = [SELECT Reference_Number__c FROM Case WHERE RecordTypeId NOT IN: recordTypes AND CaseNumber =: caseNum LIMIT 1];
											
		return caseRecord.Reference_Number__c;
	}
}