public with sharing class WorkOrderPrintDetail {
    private Case mCase;
    //private Case fCase;
    
    public List<Work_Order_Instructions__c> instructions{get;set;}
    
    public WorkOrderPrintDetail(ApexPages.StandardController stdController)
    {
        this.mCase = (Case)stdController.getRecord();
        instructions = [select Instruction_Type__c, Item_Instructions__c, Action__c from Work_Order_Instructions__c where Case__c = :this.mCase.Id and RecordType.DeveloperName = 'Vendor_Instructions' ORDER BY Sort__c ASC NULLS FIRST, Line__c ASC NULLS FIRST];
    }
}