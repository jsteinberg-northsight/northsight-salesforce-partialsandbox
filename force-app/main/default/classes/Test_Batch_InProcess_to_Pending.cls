@isTest(seeAllData=false)
private class Test_Batch_InProcess_to_Pending {
///**
// *  Purpose         :   This is used for testing and covering Batch_InProcess_to_Pending functionality.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/12/2014
// *
// *  Current Version :   V1.1
// *
// *  Revision Log    :   V1.0 - Created - Padmesh Soni(06/12/2014) - Required Maintenance: Unit Test Improvements
// *                      V1.1 - Modified - Padmesh Soni (06/18/2014) - Support Case Record Type
// *
// *  Coverage        :   100% - V1.0
// *                      100% - V1.1
// **/
//    
//    static testMethod void myUnitTest() {
//        
//        //Query record of Record Type of Account and Case objects
//        //Padmesh Soni (06/18/2014) - Support Case Record Type
//        //Query one more record type of Case Object "SUPPORT"    
//        /*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client' 
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(3, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsToInsert = new List<Account>();
//        accountsToInsert.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsToInsert.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsToInsert;
//        
//        //List to hold Case records
//        List<Case> workOrders = new List<Case>();
//        workOrders.add(new Case(Approval_Status__c = 'In Process', RecordTypeId = recordTypes[1].Id, Last_Status_Change__c = DateTime.now().addHours(-3),
//                                    AccountId = accountsToInsert[0].Id, Due_Date__c = Date.today().addDays(-2), Status = Constants.CASE_STATUS_OPEN,
//                                    Work_Completed__c = 'Yes', Work_Ordered__c = 'Intial Grass Cut'));
//        workOrders.add(new Case(Approval_Status__c = 'In Process', RecordTypeId = recordTypes[1].Id, Last_Status_Change__c = DateTime.now().addHours(-3),
//                                    AccountId = accountsToInsert[0].Id, Due_Date__c = Date.today().addDays(-2), Status = Constants.CASE_STATUS_OPEN,
//                                    Work_Completed__c = 'Yes', Work_Ordered__c = 'Intial Grass Cut', Auto_Bounce_Count__c = null));
//        workOrders.add(new Case(Approval_Status__c = 'In Process', RecordTypeId = recordTypes[1].Id, Last_Status_Change__c = DateTime.now().addHours(-5),
//                                    AccountId = accountsToInsert[0].Id, Due_Date__c = Date.today().addDays(-11), Status = Constants.CASE_STATUS_OPEN,
//                                    Work_Completed__c = 'Trip Charge'));
//        workOrders.add(new Case(Approval_Status__c = 'In Process', RecordTypeId = recordTypes[1].Id, Last_Status_Change__c = DateTime.now().addHours(-5),
//                                    AccountId = accountsToInsert[0].Id, Due_Date__c = Date.today().addDays(-11), Status = Constants.CASE_STATUS_OPEN,
//                                    Work_Completed__c = 'Trip Charge', Auto_Bounce_Count__c = null));
//        workOrders.add(new Case(Approval_Status__c = 'In Process', RecordTypeId = recordTypes[2].Id, Last_Status_Change__c = DateTime.now().addHours(-4),
//                                    AccountId = accountsToInsert[0].Id, Due_Date__c = Date.today().addDays(-11), Status = Constants.CASE_STATUS_OPEN,
//                                    Work_Completed__c = 'Trip Charge', Auto_Bounce_Count__c = null));
//        
//        //insert case records
//        insert workOrders;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Batch instance
//        Batch_InProcess_to_Pending batchJob = new Batch_InProcess_to_Pending();
//        
//        //Batch executes here
//        Database.executeBatch(batchJob, 200);
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //Loop through query result of Case records
//        //Padmesh Soni (06/18/2014) - Support Case Record Type
//        //New filter criteria added for RecordTypeId
//        for(Case wOrder : [SELECT Auto_Bounce_Count__c FROM Case WHERE  RecordTypeId NOT IN: recordTypes AND Id IN: workOrders]) {
//            
//            //assert statement here
//            System.assertEquals(1, wOrder.Auto_Bounce_Count__c);
//        }
//    }
//    
//    //TEST METHOD
//    //Scenario -- testing that a grass cut order with a last status change more than an hour old updates to pending
//    //more than 2 hours old and not a grass cut order updates to pending
//    //more than 5 hours old and not a grass cut order and department is Repairs updates to pending
//    
//    static testMethod void testingOrderTypes() {
//        
//        //Query record of Record Type of Account and Case objects
//        //Padmesh Soni (06/18/2014) - Support Case Record Type
//        //Query one more record type of Case Object "SUPPORT"    
//        /*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client' 
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(3, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsToInsert = new List<Account>();
//        accountsToInsert.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsToInsert.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsToInsert;
//        
//        //List to hold Case records
//        List<Case> workOrders = new List<Case>();
//        workOrders.add(new Case(Street_Address__c = '301 Front St',
//                                City__c = 'Nome',
//                                State__c = 'AK',
//                                Zip_Code__c = '99762',
//                                Approval_Status__c = 'In Process', 
//                                RecordTypeId = recordTypes[1].Id, 
//                                Last_Status_Change__c = DateTime.now().addHours(-1),
//                                AccountId = accountsToInsert[0].Id, 
//                                Due_Date__c = Date.today(), 
//                                Status = Constants.CASE_STATUS_OPEN,
//                                Work_Completed__c = 'Yes', 
//                                Work_Ordered__c = 'Intial Grass Cut',
//                                Department__c = 'Preservation'));
//        workOrders.add(new Case(Street_Address__c = '302 Front St',
//                                City__c = 'Nome',
//                                State__c = 'AK',
//                                Zip_Code__c = '99762',
//                                Approval_Status__c = 'In Process', 
//                                RecordTypeId = recordTypes[1].Id, 
//                                Last_Status_Change__c = DateTime.now().addHours(-1).addMinutes(1),
//                                AccountId = accountsToInsert[0].Id, 
//                                Due_Date__c = Date.today(), 
//                                Status = Constants.CASE_STATUS_OPEN,
//                                Work_Completed__c = 'Yes', 
//                                Work_Ordered__c = 'Intial Grass Cut', 
//                                Auto_Bounce_Count__c = null,
//                                Department__c = 'Preservation'));
//        workOrders.add(new Case(Street_Address__c = '303 Front St',
//                                City__c = 'Nome',
//                                State__c = 'AK',
//                                Zip_Code__c = '99762',
//                                Approval_Status__c = 'In Process', 
//                                RecordTypeId = recordTypes[1].Id, 
//                                Last_Status_Change__c = DateTime.now().addHours(-2),
//                                AccountId = accountsToInsert[0].Id, 
//                                Due_Date__c = Date.today(), 
//                                Status = Constants.CASE_STATUS_OPEN,
//                                Work_Completed__c = 'Trip Charge',
//                                Department__c = 'Inspections'));
//        workOrders.add(new Case(Street_Address__c = '304 Front St',
//                                City__c = 'Nome',
//                                State__c = 'AK',
//                                Zip_Code__c = '99762',
//                                Approval_Status__c = 'In Process', 
//                                RecordTypeId = recordTypes[1].Id, 
//                                Last_Status_Change__c = DateTime.now().addHours(-2).addMinutes(1),
//                                AccountId = accountsToInsert[0].Id, 
//                                Due_Date__c = Date.today(), 
//                                Status = Constants.CASE_STATUS_OPEN,
//                                Work_Completed__c = 'Trip Charge', 
//                                Auto_Bounce_Count__c = null,
//                                Department__c = 'Inspections'));
//        workOrders.add(new Case(Street_Address__c = '305 Front St',
//                                City__c = 'Nome',
//                                State__c = 'AK',
//                                Zip_Code__c = '99762',
//                                Approval_Status__c = 'In Process', 
//                                RecordTypeId = recordTypes[2].Id, 
//                                Last_Status_Change__c = DateTime.now().addHours(-5),
//                                AccountId = accountsToInsert[0].Id, 
//                                Due_Date__c = Date.today(), 
//                                Status = Constants.CASE_STATUS_OPEN,
//                                Work_Completed__c = 'Trip Charge', 
//                                Auto_Bounce_Count__c = null,
//                                Department__c = 'Repairs'));
//        workOrders.add(new Case(Street_Address__c = '306 Front St',
//                                City__c = 'Nome',
//                                State__c = 'AK',
//                                Zip_Code__c = '99762',
//                                Approval_Status__c = 'In Process', 
//                                RecordTypeId = recordTypes[2].Id, 
//                                Last_Status_Change__c = DateTime.now().addHours(-5).addMinutes(1),
//                                AccountId = accountsToInsert[0].Id, 
//                                Due_Date__c = Date.today(), 
//                                Status = Constants.CASE_STATUS_OPEN,
//                                Work_Completed__c = 'Trip Charge', 
//                                Auto_Bounce_Count__c = null,
//                                Department__c = 'Repairs'));
//        
//        //insert case records
//        insert workOrders;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Batch instance
//        Batch_InProcess_to_Pending batchJob = new Batch_InProcess_to_Pending();
//        
//        //Batch executes here
//        Database.executeBatch(batchJob, 200);
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //Loop through query result of Case records
//        //Padmesh Soni (06/18/2014) - Support Case Record Type
//        //New filter criteria added for RecordTypeId
//        for(Case wOrder : [SELECT Auto_Bounce_Count__c, Street_Address__c FROM Case WHERE  RecordTypeId NOT IN: recordTypes AND Id IN: workOrders]) {
//            if(wOrder.Street_Address__c == '301 Front St'){
//                //assert statement here
//                System.assertEquals(null, wOrder.Auto_Bounce_Count__c);
//            } else if(wOrder.Street_Address__c == '302 Front St'){
//                //assert statement here
//                System.assertEquals(1, wOrder.Auto_Bounce_Count__c);
//            } else if(wOrder.Street_Address__c == '303 Front St'){
//                //assert statement here
//                System.assertEquals(null, wOrder.Auto_Bounce_Count__c);
//            } else if(wOrder.Street_Address__c == '304 Front St'){
//                //assert statement here
//                System.assertEquals(1, wOrder.Auto_Bounce_Count__c);
//            } else if(wOrder.Street_Address__c == '305 Front St'){
//                //assert statement here
//                System.assertEquals(null, wOrder.Auto_Bounce_Count__c);
//            } else if(wOrder.Street_Address__c == '306 Front St'){
//                //assert statement here
//                System.assertEquals(1, wOrder.Auto_Bounce_Count__c);
//            }
//        }
//    }
}