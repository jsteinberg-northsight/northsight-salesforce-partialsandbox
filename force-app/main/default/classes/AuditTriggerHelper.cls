public with sharing class AuditTriggerHelper {
	//-----------------------------------------------------------------------------------------
	//Maps for data lookup 
	//-----------------------------------------------------------------------------------------
	// changed to newList because trigger.newMap does not exist for trigger before insert
	public static List<Audit__c> newList;
	public static Map<id,Audit__c> oldMap;
	public static Map<id, Audit__c> newMap; 
	public static Map<id, Order_Import__c> OrderImportsToUpdate;
	//-------------------------------------------------------------------
	//Public Methods
	//-------------------------------------------------------------------
	//-------------------------------------------------------------------
	/*Populate the maps we will need for lookups. 
	  All related data is loaded into maps in this function to facilitate optimal "bulkified" code.  
	*/
	public static void initialize(List<Audit__c> triggerNew, Map<id,Audit__c> triggerOldMap, Map<id, Audit__c> triggerNewMap){
		/*alias trigger values*/
		newList = triggerNew!=null?triggerNew:new List<Audit__c>();
		oldMap = triggerOldMap!=null?triggerOldMap:new Map<id,Audit__c>();
		newMap = triggerNewMap!=null?triggerNewMap:new Map<id, Audit__c>();
		orderImportsToUpdate = new  Map<id, Order_Import__c>();
		
	}


	public static void updateOrderImportFields(){
		//pick the audit list
		List<Audit__c> auditList = trigger.isDelete?oldMap.Values():newList;
		//loop over the audit list
		for(Audit__c a:auditList){
			orderImportsToUpdate.put(a.Order_Import__c, new Order_Import__c(Id = a.Order_Import__c,Last_Audit_Id__c = a.Id ));
		}
		
	}
	public static void finalize(){
		if(OrderImportsToUpdate.size()>0){
			update orderImportsToUpdate.Values();
		}
	}

private static testmethod void  testme(){
	Order_Import__c oi = new Order_Import__c();
	oi.Active__c = true;
	oi.Client__c = 'SG';
	insert oi;
	
	Audit__c a = new Audit__c(Order_Import__c=oi.Id);
	insert a;
	
	Order_Import__c oiCheck = [select Id,Last_Audit_Id__c from Order_Import__c where id = :oi.Id];
	system.assertEquals(a.Id, oiCheck.Last_Audit_Id__c,'The Last Audit ID field should be equal to the most recent audit record id.');
	
	
}

}