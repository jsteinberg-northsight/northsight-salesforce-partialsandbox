@isTest(seeAllData=false)
private class Test_BatchDeletePruvanPhotoLogs {
///**
// *  Purpose         :   This is used for testing and covering BatchDeletePruvanPhotoLogs class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/06/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Required Maintenance: Unit Test Improvements
// *	
// *	Coverage		:	83%
// **/
// 	
//    //test method added to check the functionality with code coverage
//    static testMethod void myUnitTest() {
//        
//        //List to hold test records of Pruvan_UploadPictures_Log__c
//        List<Pruvan_UploadPictures_Log__c> pruvanUploadPicturesLogs = new List<Pruvan_UploadPictures_Log__c>();
//        
//        //Loop through till count 10
//        for(integer i=0; i < 10; i++) {
//        	
//        	//add new instance of Pruvan_UploadPictures_Log__c into list
//        	pruvanUploadPicturesLogs.add(new Pruvan_UploadPictures_Log__c(Name = 'Test'+ i, Log_Data__c = 'Testing is going on.'));
//        }
//        
//        //insert Pruvan Work Order logs here
//        insert pruvanUploadPicturesLogs;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Execute batch here
//        Database.executeBatch(new BatchDeletePruvanPhotoLogs(), 200);
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //Query result of Pruvan_UploadPictures_Log__c records
//        pruvanUploadPicturesLogs = [SELECT Id FROM Pruvan_UploadPictures_Log__c];
//        
//        //Assert statement here
//        System.assertEquals(0, pruvanUploadPicturesLogs.size());
//    }
}