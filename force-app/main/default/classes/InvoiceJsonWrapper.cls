public with sharing class InvoiceJsonWrapper {
	
	public string id;
	public string client;
	public string sent;
	public string priceBook;
	public string status;
	public decimal subTotal;
	public decimal total;
	public string vendor;
	public string vendorId;
	public string privateNotes;
	public string publicNotes;
	public string invoiceNumber;
	public string workOrder;
	public string recordType;

}