@isTest
private class PortalUpdaterControllerTest {
	
//	static testmethod void processVendorCodeTest() {
//		Portal_Update_Setup__c setup1 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST1', Client__c = 'client1');
//		Portal_Update_Setup__c setup2 = new Portal_Update_Setup__c(Vendor_Code__c = 'TEST2', Client__c = 'client2');
//		List<Portal_Update_Setup__c> setupRecords = new List<Portal_Update_Setup__c>{ setup1, setup2 };
//		
//		PortalUpdaterController controller = new PortalUpdaterController();
//		Set<String> codes = controller.processVendorCodes(setupRecords);
//		system.assertEquals(2, codes.size(), 'Number of codes');
//		system.assertEquals(true, codes.contains('TEST1'), 'Code exists');
//	}
//	
//	static testmethod void processWorkOrderTest() {
//		List<Case> workOrders = new List<Case>{ new Case(
//			Vendor_Code__c = 'TEST2',
//			Street_Address__c = '200 Street',
//			Date_Serviced__c = Date.today(),
//			Special_Review_Checkbox__c = false,
//			Work_Completed__c = 'Yes',
//			Approval_Status__c = 'Pending',
//			OwnerId = UserInfo.getUserId()) };
//		
//		PortalUpdaterController controller = new PortalUpdaterController();
//		List<PortalUpdaterController.Vendor> vendorList = controller.processWorkOrders(workOrders);
//		system.assertEquals(1, vendorList.size(), 'Number of unique vendors');
//	}
//	
//	static testmethod void workOrderInProcessTest() {
//		PortalUpdaterController controller = new PortalUpdaterController();
//		PortalUpdaterController.InProcessWorkOrder currentInProcessOrder = controller.checkForWorkOrderInProcess(
//																												new List<Case>{ new Case(
//																																			Vendor_Code__c = 'TEST2',
//																																			Street_Address__c = '200 Street',
//																																			Date_Serviced__c = Date.today(),
//																																			Special_Review_Checkbox__c = false,
//																																			Work_Completed__c = 'Yes',
//																																			Approval_Status__c = 'Pending',
//																																			OwnerId = UserInfo.getUserId()) });
//		system.assertEquals('Current In-Process Work Order: ', currentInProcessOrder.inProcessText, 'Text when user has in-process work order');
//		system.assertEquals('TEST2', currentInProcessOrder.workOrder.Vendor_Code__c, 'Vendor code of work order in process');
//	}
//	
//	static testmethod void noWorkOrderInProcessTest() {
//		PortalUpdaterController controller = new PortalUpdaterController();
//		PortalUpdaterController.InProcessWorkOrder currentInProcessOrder = controller.checkForWorkOrderInProcess(new List<Case>());
//		system.assertEquals('', currentInProcessOrder.inProcessText, 'Text when user has no in-process work order');
//		system.assertEquals(null, currentInProcessOrder.workOrder, 'Work order when no in-process work order found');
//	}
//	
//	static testmethod void noWorkOrderTest() {
//		PortalUpdaterController controller = new PortalUpdaterController();
//		system.assertEquals(new List<PortalUpdaterController.Vendor>(), controller.vendorList, 'No work orders');
//	}
}