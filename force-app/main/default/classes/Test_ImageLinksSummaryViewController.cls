@isTest(seeAllData=false)
private class Test_ImageLinksSummaryViewController {
//
///**
// *  Purpose         :   This is used for testing and covering ImageLinksSummaryViewController functionality.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   02/05/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created
// *
// *	Coverage		:	100%	
// **/
// 
//	//Test method to used testing ImageLinksSummaryView functionailty
//	static testMethod void testImageLinksSummaryView() {
//        
//        //Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client') AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(2, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accounts;
//		
//		Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//		insert property;
//		
//		//List to hold image Link records  
//    	List<ImageLinks__c> imageLinks = new List<ImageLinks__c>();
//    	imageLinks.add(new ImageLinks__c(Pruvan_evidenceType__c= 'Survey', GPS_Timestamp__c= DateTime.now(), 
//    										ImageUrl__c='https://s3.amazonaws.com/NorthsightTmp/02570223/52820869.jpg',
//    										GPS_Location__Latitude__s = 30.087567, GPS_Location__Longitude__s=-90.485625, Pruvan_gpsAccuracy__c=2531, 
//    										Survey_Question_Id__c='Trip_Charge_Reason__c', Survey_Id__c='GC23_001-v5'));
//    	imageLinks.add(new ImageLinks__c(Pruvan_evidenceType__c= 'Survey', GPS_Timestamp__c= DateTime.now(), 
//    										ImageUrl__c='https://s3.amazonaws.com/NorthsightTmp/02570223/52820869.jpg',
//    										GPS_Location__Latitude__s = 30.087567, GPS_Location__Longitude__s=-90.485625, Pruvan_gpsAccuracy__c=2531, 
//    										Survey_Question_Id__c='Trip_Charge_Reason__c', Survey_Id__c='GC23_001-v5', Notes__c = 'This is a picture of the lawn.'));
//    	
//    	String summaryData = JSON.serializePretty(imageLinks);
//    	
//    	String summaryData2 = '['
//										+'{'
//										    +'"Id": 2,'
//										    +'"ImageUrl__c": "http://nspdl.elasticbeanstalk.com/viewImage/12122222/234234234.jpg",'
//										    +'"Survey_Id__c": "GC22_001-v7",'
//										    +'"Survey_Question_Id__c": "Number_of_Bags_Removed__c",'
//										    +'"Pruvan_EvidenceType__c": "Survey",'
//										    +'"GPS_Timestamp__c": "2014-01-31T13:05:24Z",'
//										    +'"GPS_Location_Latitude__c": 23.253321,'
//										    +'"GPS_Location_Longitude__c": -100.030122,'
//										    +'"Pruvan_gpsAccuracy__c": 7,'
//										    +'"Distance_from_Property_in_Ft__c": 24.5,'
//										    +'"Notes__c":"This is a picture of the lawn."'
//										+'}'
//								    +']';
//								
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today(), 
//    								Status = Constants.CASE_STATUS_OPEN, ImageLinks_Summary__c = summaryData2));
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1), 
//    								Status = Constants.CASE_STATUS_OPEN, ImageLinks_Summary__c = 'Test'));
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(2), 
//    								Status = Constants.CASE_STATUS_OPEN, ImageLinks_Summary__c = summaryData));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//    	//Test starts here
//    	Test.startTest();
//    	
//    	//Initialize standard contorller of Property object
//    	ApexPages.StandardController stdController = new ApexPages.standardController(workOrders[0]);
//    	
//    	//Instance of PropertyWorkOrdersListController
//    	ImageLinksSummaryViewController controller = new ImageLinksSummaryViewController(stdController);
//    	
//    	//assert statements
//    	System.assertEquals(1, controller.imageLinks.size());
//    	
//    	//Initialize standard contorller of Property object
//    	stdController = new ApexPages.standardController(workOrders[1]);
//    	
//    	//Instance of PropertyWorkOrdersListController
//    	controller = new ImageLinksSummaryViewController(stdController);
//    	
//    	//assert statements
//    	System.assert(ApexPages.getMessages() != null);
//    	
//    	//Initialize standard contorller of Property object
//    	stdController = new ApexPages.standardController(workOrders[2]);
//    	
//    	//Instance of PropertyWorkOrdersListController
//    	controller = new ImageLinksSummaryViewController(stdController);
//    	
//    	//assert statements
//    	System.assertEquals(2, controller.imageLinks.size());
//    	
//    	//Test stops here
//    	Test.stopTest();
//    }
}