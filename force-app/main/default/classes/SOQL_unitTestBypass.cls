@isTest
public class SOQL_unitTestBypass {

    public static testmethod void runUnitTestBypassers(){
        //in order to deploy SOQL_X_invocable class, add unittestbypass call below
        SOQL_Account_Invocable.unitTestBypass();
        SOQL_FindVendorFromApplication_Invocable.unitTestByPass();
        SOQL_Bid_Invocable.unitTestBypass();
        SOQL_Bid_Item_Invocable.unitTestBypass();
        SOQL_Case_Invocable.unitTestBypass();
        SOQL_Compliance_Item_Invocable.unitTestBypass();
        SOQL_Contact_Invocable.unitTestBypass();
        SOQL_ImageLinks_Invocable.unitTestBypass();
        SOQL_Invoice_Invocable.unitTestBypass();
        SOQL_Invoice_Line_Invocable.unitTestBypass();
        SOQL_PAD_Invocable.unitTestBypass();
        SOQL_PAR_Invocable.unitTestBypass();
        SOQL_Payment_Batch_Invocable.unitTestBypass();
        SOQL_Payment_Dispute_Invocable.unitTestBypass();
        SOQL_Payment_Dispute_Review_Invocable.unitTestBypass();
        SOQL_Payment_Invocable.unitTestBypass();
        SOQL_Payment_Line_Invocable.unitTestBypass();
        SOQL_Payments_Received_Invocable.unitTestBypass();
        SOQL_Products_Invocable.unitTestBypass();
        SOQL_Property_Invocable.unitTestBypass();
        SOQL_Property_Service_Invocable.unitTestBypass();
        SOQL_Property_Task_Invocable.unitTestBypass();
        SOQL_Recurring_Service_Invocable.unitTestBypass();
        SOQL_Vendor_Application_Invocable.unitTestBypass();
        SOQL_Work_Code_Invocable.unitTestBypass();
        SOQL_Work_Code_Mapping_Invocable.unitTestBypass();
        SOQL_Work_Code_Survey_Invocable.unitTestBypass();
        SOQL_Work_Instruction_Invocable.unitTestBypass();
        SOQL_Work_Order_Assignment_Invocable.unitTestBypass();
        SOQL_Work_Order_Instructions_Invocable.unitTestBypass();
        SOQL_Work_Order_Setup_Invocable.unitTestBypass();
        SOQL_Work_Order_Survey_Invocable.unitTestBypass();
        SOQL_Work_Survey_Invocable.unitTestBypass();
    }
    
}