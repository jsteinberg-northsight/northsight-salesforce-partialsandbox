@isTest
private class Test_RequestHelper {
    //GLOBALS
    //--------------------------------------------------
    private static User contextUser;
    //--------------------------------------------------
    
    //TEST UTILITY METHODS
    //------------------------------------------------------------------------------------------------------
    //creates a user with the 'Partner Community - Clients' profile
    private static void createUser() {
        Id userContactId = createContact();
        Id userProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Vendors' limit 1].Id;
        
        contextUser = new User(
            Alias = 'pcClient', 
            Email='clientStandT@testorg.com', 
            EmailEncodingKey='UTF-8', 
            LastName='clientRequest_Testing', 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
            ProfileId = userProfile, 
            TimeZoneSidKey='America/Los_Angeles', 
            UserName='clientStandT@testorg.com',
            isActive = true,
            ContactId = userContactId
        );
        insert contextUser;
    }
    
    //creates a user that doesn't have the 'Partner Community - Clients' profile
    private static void createNonClientCommunityUser() {
        Id userContactId = createContact();
        Id userProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Vendors' limit 1].Id;
        
        contextUser = new User(
            Alias = 'pcClient', 
            Email='clientStandT@testorg.com', 
            EmailEncodingKey='UTF-8', 
            LastName='clientRequest_Testing', 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
            ProfileId = userProfile, 
            TimeZoneSidKey='America/Los_Angeles', 
            UserName='clientStandT@testorg.com',
            isActive = true,
            ContactId = userContactId
        );
        insert contextUser;
    }
    
    //creates a contact
    private static Id createContact() {
        Id contactAccountId = createAccount();
        Id vendorRT = [SELECT Id FROM RecordType WHERE Name = 'Vendor' AND SobjectType = 'Contact'].Id;
        
        Contact newContact = new Contact(
            LastName = 'Tester', 
            AccountId = contactAccountId, 
            MailingStreet = '301 Front St',
            MailingCity = 'Nome',
            MailingState = 'AK',
            MailingPostalCode = '99762',
            OtherStreet = '301 Front St',
            OtherCity = 'Nome',
            OtherState = 'AK',
            OtherPostalCode = '99762',
            Status__c = '3 - Active',
            Date_Active__c = Date.today(),            
            Activated_By__c = 'z-Other',
            Other_Address_Geocode__Latitude__s = 64.497669,
            Other_Address_Geocode__Longitude__s = -165.41005,
            REO_Grass_Cut__c = 25.00,
            Pay_Terms__c = 'Net 30',
            Trip_Charge__c = 15.00,
            Shrub_Trim__c = 0.00,
            RecordTypeId = vendorRt
        );
        insert newContact;
        
        return newContact.Id;
    }
    
    //creates an account
    private static Id createAccount() {
        Account newAccount = new Account(
            Name = 'Partner Community Vendors Account',
            RecordTypeId ='0124000000011Q2'
        );
        insert newAccount;
        
        return newAccount.Id;
    }
    
    //creates a request
    private static void createRequest() {
        Request__c newRequest = new Request__c(
            Status__c = 'New',
            Type__c = 'New Preservation Orders',
            Last_Status_Change_Date_Time__c = DateTime.now(),
            Description__c = 'New Client Request Test'
        );
        insert newRequest;
    }
    //------------------------------------------------------------------------------------------------------
    
    //UNIT TESTS
    //------------------------------------------------------------------------------------------------------
//    static /*testMethod*/ void simpleRequestTest() {
//        createUser();
//        System.runAs(contextUser) {
//            createRequest();
//            List<Request__c> requests = [SELECT Client_Name__c FROM Request__c];
//                System.assertEquals(1, requests.size());
//            Id userAccountId = [SELECT AccountId FROM User WHERE Id =: contextUser.Id].AccountId;
//            for(Request__c request : requests) {
//            //  System.assertEquals(userAccountId, request.Client_Name__c);
//            }
//        }
//    }
//    
//    static /*testMethod*/ void simpleRequestTest_NonClientCommunityUser() {
//        createNonClientCommunityUser();
//        System.runAs(contextUser) {
//            createRequest();
//            List<Request__c> requests = [SELECT Client_Name__c FROM Request__c];
//                System.assertEquals(1, requests.size());
//            Id userAccountId = [SELECT AccountId FROM User WHERE Id =: contextUser.Id].AccountId;
//            for(Request__c request : requests) {
//                System.assertNotEquals(userAccountId, request.Client_Name__c);
//                System.assertEquals(null, request.Client_Name__c);
//            }
//        }
//    }
    //------------------------------------------------------------------------------------------------------
}