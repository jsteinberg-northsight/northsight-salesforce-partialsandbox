@isTest(seeAllData=false)
private class Test_AssignmentMapUtility {
///**
// *  Purpose         :   This is used for testing and covering AssignmentMapUtility.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   09/17/2014
// *
// *	Current Version	:	V1.2
// *
// *	Revision Log	:	V1.0 - Created - Padmesh Soni (09/17/2014) - OAM-78: Add  the ability to be able to plot all vendors on the map not just 
// *											 the ones with assigned work orders.
// *						V1.1 - Modified - Padmesh Soni (09/25/2014) - OAM-78: Add  the ability to be able to plot all vendors on the map not just 
// *											 the ones with assigned work orders.
// *						V1.2 - Modified - Padmesh Soni (09/25/2014) - OAM-78: Add  the ability to be able to plot all vendors on the map not just 
// *											 the ones with assigned work orders.
// * 
// *	Coverage		:	100% - V1.0
// *						100% - V1.1
// *						100% - V1.2
// **/
// 
//	private static List<Account> accounts;
//	private static List<Contact> contacts;
//	private static Profile newProfile;
//	private static List<User> users;
//	private static Group queues;
//	
//	private static void generateTestData(integer countData){
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP 
//		//create a new user with the Customer Portal Manager Standard profile
//		newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Vendors'];
//		
//		//Loop through countData time
//		for(integer i = 1; i <= countData; i++) {
//		
//			/******** PRE-TEST SETUP create a new account *********/
//			accounts.add(new Account(Name = 'Tester Account' + i,RecordTypeId = '0124000000011Q2'));
//		}
//		
//		insert accounts;
//		
//		List<RecordType> recordTypes = [SELECT Id FROM RecordType WHERE DeveloperName = 'Vendor' AND SobjectType = 'Contact' AND IsActive = true];
//		
//		System.assertEquals(1, recordTypes.size());
//		
//		//Loop through countData time
//		for(integer i = 1; i <= countData; i++) {
//		
//			//--------------------------------------------------------
//	   		//PRE-TEST SETUP
//			//create a new contact
//			contacts.add(new Contact(RecordTypeId = recordTypes[0].Id, LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest'+i, 
//										Status__c = '3 - Active', Date_Active__c = Date.today(), OtherStreet = '123 Main Street', OtherCity = 'Midland',
//										OtherState = 'TX', OtherPostalCode = '123344', MailingStreet = '123 Main Street', MailingCity = 'Midland', 
//										MailingState = 'TX', MailingPostalCode = '123344', Shrub_Trim__c = 23, REO_Grass_Cut__c = 12,
//										Pay_Terms__c = 'Net 3', Trip_Charge__c = 10, 
//										Other_Address_Geocode__Latitude__s = 15.34444, Other_Address_Geocode__Longitude__s = -68.34444));
//		}
//		
//		//Code added - Padmesh Soni (10/03/2014) - OAM-78: Add  the ability to be able to plot all vendors 
//		//on the map not just the ones with assigned work orders. 
//		contacts.add(new Contact(RecordTypeId = recordTypes[0].Id, LastName = 'Tester', AccountId = accounts[0].Id, Pruvan_Username__c = 'NSBobTest', 
//										Status__c = '3 - Active', Date_Active__c = Date.today(), OtherStreet = '123 Main Street', OtherCity = 'Midland',
//										OtherState = 'TX', OtherPostalCode = '123344', MailingStreet = '123 Main Street', MailingCity = 'Midland', 
//										MailingState = 'TX', MailingPostalCode = '123344', Shrub_Trim__c = 23, REO_Grass_Cut__c = 12,
//										Pay_Terms__c = 'Net 3', Trip_Charge__c = 10, 
//										Other_Address_Geocode__Latitude__s = 15.34444, Other_Address_Geocode__Longitude__s = -68.34444));
//		insert contacts;
//		
//		//Loop through countData time
//		for(integer i = 1; i <= countData; i++) {
//		
//			//create a new user with the profile
//			users.add(new User(alias = 'standt'+i, contactId = contacts[i-1].Id, email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8',
//									lastname = 'Testing'+i, languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//									timezonesidkey = 'America/Los_Angeles', username = 'NS_stdUser00x'+i+'@test.com', isActive = true));
//		}
//		
//		insert users;
//	}
//	
//    //Test method is used to test functionality of getVendorsToPlot method
//    static testMethod void testGetVendorsToPlot() {
//    	
//    	//initialize variables
//    	accounts = new List<Account>();
//		contacts = new List<Contact>();
//		users = new List<User>();
//		
//		//Create testing data
//		generateTestData(3);
//		
//		//Test starts here 
//		Test.startTest();
//		
//		//Code added - Padmesh Soni (09/25/2014) -  - OAM-78: Add  the ability to be able to plot all vendors on the map not just
//		//the ones with assigned work orders.
//		//A list to include contact status
//		List<String> contactStatus = new List<String>{'3 - Active'};
//		
//		//query result of Contact records
//		contacts = [SELECT Other_Address_Geocode__Latitude__s, Other_Address_Geocode__Longitude__s FROM Contact];
//		
//		//Range of map boundaries
//		double minLat = contacts[0].Other_Address_Geocode__Latitude__s;
//		double minLon = contacts[0].Other_Address_Geocode__Longitude__s;
//		double maxLat = contacts[0].Other_Address_Geocode__Latitude__s;
//		double maxLon = contacts[0].Other_Address_Geocode__Longitude__s;
//		
//		//Instance of PageReference 
//		PageReference p = ApexPages.currentPage();
//		
//		//Map to hold all parameters from Page URL
//		Map<string, string> params = p.getParameters();
//		params.put(AssignmentMapConstants.URL_PARAM_MINLAT, String.valueOf(minLat));
//		params.put(AssignmentMapConstants.URL_PARAM_MINLON, String.valueOf(minLon));
//		params.put(AssignmentMapConstants.URL_PARAM_MAXLAT, String.valueOf(maxLat));
//		params.put(AssignmentMapConstants.URL_PARAM_MAXLON, String.valueOf(maxLon));
//		params.put('vendorStatusSelected', '3 - Active');
//		
//		AssignmentMapUtility controller = new AssignmentMapUtility();
//		
//		//Call method
//		controller.getVendorsToPlot();
//		
//		//assert statement
//		System.assertNotEquals(null, controller.vendorsOnMapJSON);
//		
//		String excludeVendors = '';
//		String excludeConIds = '';
//		
//		//Loop through inserted Users
//		for(User usr : users) {
//			
//			//populate set of Vendors Ids
//			excludeVendors +=usr.Id + ',';
//		}
//		
//		//Loop through inserted Users
//		for(Contact con : contacts) {
//			
//			//populate set of Vendors Ids
//			excludeConIds +=con.Id + ',';
//		}
//		
//		//Code added - Padmesh Soni (10/03/2014) - OAM-78: Add  the ability to be able to plot all vendors 
//		//on the map not just the ones with assigned work orders. 
//		//query result of Contact records
//		contacts = [SELECT Other_Address_Geocode__Latitude__s, Other_Address_Geocode__Longitude__s FROM Contact WHERE LastName = 'Tester'];
//		
//		params.put('excludeVendors', excludeVendors);
//		params.put('excludeContacts', excludeConIds+contacts[0].Id);
//		
//		controller = new AssignmentMapUtility();
//		
//		//Call method
//		controller.getVendorsToPlot();
//		
//		//assert statement
//		System.assertEquals('', controller.vendorsOnMapJSON);
//		
//		//Test stops here
//		Test.stopTest();
//	}
}