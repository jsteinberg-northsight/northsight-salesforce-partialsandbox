@isTest
public with sharing class CaseFormulaTester {
//
//	private static Account 			newAccount;
//	private static Contact 			newContact;
//	private static Profile 			newProfile;
//	private static User    			newUser;
//	private static Geocode_Cache__c newGeocode;
//	private static RecordType   	rt;
//	private static Case				newCase;
//	
//private static void generateTestData(){
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new account
//		newAccount = new Account(
//			Name = 'Tester Account',
//			RecordTypeId = '01240000000Ubta'
//		);
//		insert newAccount;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new contact
//		newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest',
//			RecordTypeId = '01240000000UbtfAAC'
//			
//		);
//		insert newContact;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new user with the Customer Portal Manager Standard profile
//		newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Processors'];
//		//create a new user with the profile
//		newUser = new User(
//			alias = 'standt',
//			contactId = newContact.Id,
//			email = 'standarduser@testorg.com',
//			emailencodingkey = 'UTF-8',
//			lastname = 'Testing',
//			languagelocalekey = 'en_US',
//			localesidkey = 'en_US',
//			profileId = newProfile.Id,
//			timezonesidkey = 'America/Los_Angeles', username = 'standarduser@northsight.test', isActive = true
//		);
//		insert newUser;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new property
//		newGeocode = new Geocode_Cache__c(
//			address_hash__c = '400cedarstbrdgprtct60562',
//			quarantined__c = false,
//			location__latitude__s = 30.099307,//11.11111111
//			location__longitude__s = -81.75276,//-111.22222222
//			Street_Address__c = '400 Cedar st',
//			Zip_Code__c = '60562',
//			City__c = 'Bridgeport',
//			State__c = 'CT',
//			Formatted_Street_Address__c = '400 Cedar st',
//			Formatted_Zip_Code__c = '60562',
//			Formatted_City__c = 'Bridgeport',
//			Formatted_State__c = 'CT',
//			sqft_of_lot_size__c = 4340
//		);
//		insert newGeocode;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create work orders with the REO record type
//		rt = [SELECT Id, Name FROM RecordType WHERE Name =: 'REO' LIMIT 1];
//		//create a work order with the record type
//		
//		newCase = new Case(
//			Client__c = 'SG',
//			Due_Date__c = Date.today(),
//			Scheduled_Date_Override__c = Date.today().adddays(2),
//			Backyard_Serviced__c = 'Yes',
//			Excessive_Growth__c = 'No',
//			Debris_Removed__c = 'No',
//			Shrubs_Trimmed__c = 'No',
//			Vendor_Code__c = 'FOLLOW',
//			State__c = 'CT',
//			Status = 'Open',
//			City__c = 'Bridgeport',
//			Zip_Code__c = '60562',
//			Description = 'Work order for grass cutting',
//			OwnerId = newUser.Id,
//			ContactId = newContact.Id,
//			Street_Address__c = '400 Cedar st',
//			Geocode_Cache__c = newGeocode.Id,
//			//Work_Ordered__c = 'Initial Grass Cut',
//			Work_Ordered__c = 'Maid Service',
//			Work_Ordered_Text__c = 'Grass Cutting',
//			Approval_Status__c = 'Not Started',
//			RecordTypeId = rt.Id,
//			Canceled__c = False
//		);
//		insert newCase;
//	}
//	
//	public static testmethod void testFiveMinsApart(){
//		system.Test.startTest();
//		generateTestData();
//		system.debug('Scheduled_ Date_ Override__ c: '+newCase.Scheduled_Date_Override__c);
//		Case testCase = [SELECT Scheduled_Date_Override__c, Scheduled_Date__c, Routing_Serviceable__c, Approval_Status__c FROM Case WHERE Id=: newCase.Id];
//		
//		system.debug('Scheduled Date c: '+testCase.Scheduled_Date__c);
//		system.debug('Routing Serviceable: '+testCase.Routing_Serviceable__c);
//		system.debug('Approval _Status_c: '+testCase.Approval_Status__c);
//		
//		system.assertEquals('Yes', testCase.Routing_Serviceable__c);
//		system.Test.stopTest();
//	}
}