/**
 *	Purpose			:	This class is to store all generic methods to used on org wide level.
 *
 *	Created By		:	Padmesh Soni
 *
 *	Created Date	:	02/04/2014
 *
 *	Version			:	V1.0
 **/
public class Util {
	
	//Variable to hold boolean value which is used for by passing triggers 
	//if this flag is true and doesn't do anything further
	public static boolean ByPassAllTriggers = false;
		
	/**
	 *	@description	:	Method for create log of DML insert or update opertion.
	 *
	 *	@args			:	Database.SaveResult[]
	 *
	 *	@return			:	String
	 **/
	public Static String createLog(Database.SaveResult[] results) {

		//String to hold Log message
		String errorLog = '';

		//Iterate through each returned result
		for(Database.SaveResult result : results) {

			//Check for success
			if (!result.isSuccess()) {

				//call errorHTMLLog for getting Errors in HTML format
				errorLog = errorHTMLLog(result.getErrors());
			}
		}

		//return logmessage
		return errorLog;
	}

	/**
	 *	@description	:	Method for create log of DML Delete opertion.
	 *
	 *	@args			:	Database.DeleteResult[]
	 *
	 *	@return			:	String
	 **/
    public Static String createLog(Database.DeleteResult[] deleteResults) {
        
        //String to hold Log message
        String logMessage = '';
        
        //Iterate through each returned result
        for(Database.DeleteResult deleteResult : deleteResults) {
			
			//Check for success
          	if (!deleteResult.isSuccess()) {
          	
	            //Operation failed, so get all errors
	            for(Database.Error err : deleteResult.getErrors()) {
                    
	                //create log message 
	                logMessage += err.getStatusCode() + '&nbsp;' + err.getMessage() + '<br>';
            	}
          	}
		}
        
        //return logmessage
        return logMessage;
    }
    
	/**
	 *	@description	:	Method for create log of DML upsert opertion.
	 *
	 *	@args			:	Database.UpsertResult[]
	 *
	 *	@return			:	String
	 **/
    //Method for create log of DML upsert opertion
    public Static String createLog(Database.UpsertResult[] upsertResults) {
        
        //String to hold Log message
        String logMessage = '';
        
        //Iterate through each returned result
        for(Database.UpsertResult upsertResult : upsertResults) {
			
			//Check for success
          	if (!upsertResult.isSuccess()) {
          	
	            //Operation failed, so get all errors
	            for(Database.Error err : upsertResult.getErrors()) {
                    
	                //create log message 
	                logMessage += err.getStatusCode() + '&nbsp;' + err.getMessage() + '<br>';
            	}
          	}
		}
        
        //return logmessage
        return logMessage;
    }
    
    /**
	 *	@description	:	Method for create log of DML opertion errors in HTML format.
	 *
	 *	@args			:	Database.Errors[]
	 *
	 *	@return			:	String
	 **/
	public Static String errorHTMLLog(Database.Error [] errors) {

		//String to hold Log message
		String errorHTMLLogs = '';

		//Operation failed, so get all errors
		for(Database.Error err : errors) {

			//create log message
			errorHTMLLogs += '<tr><td>' + err.getStatusCode() + '</td><td>' + err.getMessage() + '</td></tr>';
		}

		//Check if error log is not null
		if(errorHTMLLogs != '')
			errorHTMLLogs = '<table border="1"><tr><th>StatusCode</th><th>Message</th></tr>' + errorHTMLLogs + '</table>';

		//return logmessage
		return errorHTMLLogs;
	}
}