global without sharing class BatchMigateVendor{// implements Database.Batchable<sObject> { 
	/*global database.Querylocator start(Database.BatchableContext BC){
		return database.getQueryLocator([select id, Street,City,State,Country,PostalCode, AboutMe,  ReceivesAdminInfoEmails, Alias, ForecastEnabled, CallCenterId, MobilePhone, DigestFrequency, CompanyName, ContactId, DefaultGroupNotificationFrequency,DelegatedApproverId, Department, Division, Email, EmailEncodingKey, SenderEmail, SenderName, Signature,EmployeeNumber,Extension,Fax, ReceivesInfoEmails, LanguageLocaleKey ,LocaleSidKey,ManagerId, Name, CommunityNickname, Phone, ProfileId, UserRoleId,FederationIdentifier, StayInTouchNote, StayInTouchSignature,StayInTouchSubject,TimeZoneSidKey,Title,Username, is4sf__is_customer_id__c, is4sf__date__c, skyplatform__Do_Not_Log_Calls__c, echosign_dev1__EchoSign_Allow_Delegated_Sending__c, echosign_dev1__EchoSign_Email_Verified__c, GC_Price__c, is4sf__is_password__c,is4sf__is_username__c,is4sf__is_domino_license__c,is4sf__is_reach_license__c,License_Type__c, skyplatform__ShoreTel_Sky_Profile_Type__c, SSA_Pricing__c, Today_15__c , is4sf__is_user_id__c, User_Initials__c, OrderAssignmentID__c, Profile.Name from User where MigrateToCommunity__c = true and IsActive=false and Contact.Account.Name !='Vendor Community']);
	} 
	public Account vendorCommunityAccount;
	public Profile OSCommunityProfile;
	public Profile VCommunityProfile; 
	public UserRole VCPURole;
	global void execute(Database.BatchableContext BC, List<User> markedVendors){
		vendorCommunityAccount = [select Id from Account where Name = 'Vendor Community' limit 1];
		VCommunityProfile = [select Id from Profile where Name = 'Partner Community - Vendors' limit 1];
		OSCommunityProfile  = [select Id from Profile where Name = 'Partner Community - Vendors OrderSelect' limit 1];
		VCPURole = [Select Id from UserRole where Name = 'Vendor Community Partner User' limit 1];
		Contact[] vendorContactsToUpdate = new Contact[]{};
		Map<id,User> newUsers = new Map<id,User>();
		for(User ven:markedVendors){
			newUsers.put(ven.Id,createCommunitiesUser(ven));
			vendorContactsToUpdate.add(changeContactAccount(ven));
			//disablePortalUser(ven);
			//alterPortalUserAlias(ven);
		}
		//update markedVendors;
		update vendorContactsToUpdate;
		insert newUsers.values();
///*	
//		Case[] casesToUpdate = new Case[]{};
//		Route__c[]routesToUpdate = new Route__c[]{};
//		for(User u:markedVendors){
//			
//			Id newId = newUsers.get(u.Id).Id;
//			for(Case c:[select Id,OwnerId from Case where Status!='Closed' and OwnerId =:u.Id]){
//				c.OwnerId = newId;
//				casesToUpdate.add(c);
//			}
//			for(Route__c r:[select Id,OwnerId from Route__c where OwnerId =:u.Id]){
//				r.OwnerId = newId;
//				routesToUpdate.add(r);
//			}
//		}
//		
//		update casesToUpdate;
//		update routesToUpdate;
//
	}
	global void finish(Database.BatchableContext BC){
		
	}
	public void disablePortalUser(User ven){
		ven.IsActive = false;
		ven.IsPortalEnabled = false;
	}
	public void alterPortalUserAlias(User ven){
		ven.Alias = '_'+ven.Alias;
	}
	public Contact changeContactAccount(User ven){
		Contact vendorContact = new Contact(Id = ven.ContactId);
		vendorContact.AccountId =  vendorCommunityAccount.Id;
		return vendorContact;
	}
	public User createCommunitiesUser(User ven){
		User newUser = ven.Clone(false,false,false,false);
		setProfile(ven,newUser);
		setRole(newUser);
		uncheckCRMOptions(newUser);
		return newUser;
	}
	public void uncheckCRMOptions(User newUser){
		//Fields not available.
		//Receive Salesforce CRM Content Email Alerts
		//Receive Salesforce CRM Content Alerts as Daily Digest
	}
	public void setRole(User newUser){
		newUser.UserRoleId = VCPURole.Id;
	}
	public void setProfile(User ven,User newUser){
	     //Any Customer Portal that is not Order Select use Partner Community Vendor
		 if(ven.Profile.Name.contains('Order Select')){
		 	newUser.ProfileId = OSCommunityProfile.Id;
		 }else{
		 //Customer Portal Vendor that is Order Select use Partner Community Order Select
		 	newUser.ProfileId = VCommunityProfile.Id;
		 }
	}
	*/
}