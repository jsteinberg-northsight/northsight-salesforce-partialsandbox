@isTest(seeAllData=false)
private class Test_ZillowController {
///**
// *  Purpose         :   Test class for ZillowController. 
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   12/12/2013
// *
// *  Current Version :   V1.0
// *
// *  Revision Log    :   V1.0 - Created
// *                      V1.1 - Modified - Existing method modify to test the getting latitude and longitude from Zillow of Listing. - Padmesh Soni - 12/18/2013
// *
// *  Code Coverage   :   100%    
// **/
//    
//    //Method to test ZillowController request for Lot size of Listing
//    static testMethod void testListingZillowLotSize() {
//        
//        //Insert Zillow Configuration Defaults values
//        Zillow_Configuration__c configDefault = new Zillow_Configuration__c();
//        configDefault.ZWS_Id__c = 'test';
//        insert configDefault;
//        
//        //List to hold test records of Listing object
//        List<Geocode_Cache__c> properties = new List<Geocode_Cache__c>();
//        
//        //Looping for adding 10 new Property records - Padmesh Soni - 12/12/2013
//        //Looping for adding 5 new Listing records - Padmesh Soni - 12/18/2013 
//        for(Integer i = 1; i <= 5; i++) {
//            
//            //add new instance of property record into list of propertys
//            properties.add(new Geocode_Cache__c(Street_Address__c = '2114 Bigelow Ave'+ i, City__c = 'Seattle', State__c = 'WA', 
//                                                Zip_Code__c = '9810'+i));
//        }
//        
//        //insert property records
//        insert properties;
//        
//        //Test Starts here
//        Test.startTest();
//        
//        //Set Mock Callout class 
//        Test.setMock(HttpCalloutMock.class, new ZillowControllerMockClass());
//        
//        //Constructor calling
//        ZillowController controller = new ZillowController();
//        
//        //Loop through ZillowController's List of Wrapper class instance 
//        for(ZillowController.LotSizeWrapper lotWrap : controller.lotSizeWrapperList) {
//            
//            //Assert for results
//            System.assertEquals(4680, lotWrap.lotSizeFromZillow);
//            
//            //Assert statements added for taking response of Latitude and Longitude - Padmesh Soni - 12/18/2013 
//            System.assertEquals(47.637933, lotWrap.latitudeVal);
//            System.assertEquals(-122.347938, lotWrap.longitudeVal);
//        }
//        
//        //Test stop here
//        Test.stopTest();
//    }
}