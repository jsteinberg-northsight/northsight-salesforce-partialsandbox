public class ExternalAPIFieldsController {

    public Contact loggedInContact { get; set; }
    public Boolean enabledStatus { get; set; }
    public Boolean disabledStatus { get; set; }
    
    public PageReference saveContact() {
        Id UserId = UserInfo.getUserId();
        User currentUser =  [SELECT ContactId FROM User WHERE Id =:UserId];
        loggedInContact = [SELECT Id, EXTAPI_Key__c, EXTAPI_Enabled__c FROM Contact WHERE Id =: currentUser.ContactId];
        
        loggedInContact.EXTAPI_Enabled__c = true;
        loggedInContact.EXTAPI_Disabled_By__c = null;
        loggedInContact.EXTAPI_Disabled_Date_Time__c = null;
        update loggedInContact ;
        
        PageReference tempPage = ApexPages.currentPage();            
        tempPage.setRedirect(true);
        return tempPage;
    }
    
    public ExternalAPIFieldsController() {
        Id UserId = UserInfo.getUserId();
        User currentUser =  [SELECT ContactId FROM User WHERE Id =:UserId];
        loggedInContact = [SELECT Id, EXTAPI_Key__c, EXTAPI_Enabled__c FROM Contact WHERE Id =: currentUser.ContactId];
        enabledStatus = loggedInContact.EXTAPI_Enabled__c;
        disabledStatus = !enabledStatus;
    }
}