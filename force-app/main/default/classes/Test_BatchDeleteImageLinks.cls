@isTest(seeAllData=false)
private class Test_BatchDeleteImageLinks {
///**
// *  Purpose         :   This is used for testing and covering BatchDeleteImageLinks class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/06/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Required Maintenance: Unit Test Improvements
// *	
// *	Coverage		:	100%
// **/
// 	
//    //test method added to check the functionality with code coverage
//    static testMethod void myUnitTest() {
//    	
//    	//Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: AssignmentMapConstants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: AssignmentMapConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: AssignmentMapConstants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: AssignmentMapConstants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: AssignmentMapConstants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client') AND (SobjectType =: AssignmentMapConstants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: AssignmentMapConstants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(2, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accountsOnOrders = new List<Account>();
//    	accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	
//    	
//    	List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'LA',
//    								Department__c = 'Test', Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual', 
//    								Updated_Date_Time__c = DateTime.now(),Client_Name__c = account.Id));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'LA', 
//    								Updated_Date_Time__c = DateTime.now().addHours(2),Client_Name__c = account.Id));
//    	
//    	//insert Order here
//    	insert workOrders;
//    	
//    	//List to hold ImageLinks records
//    	List<ImageLinks__c> imageLinks = new List<ImageLinks__c>();
//    	
//    	//Loop through counter till 10
//    	for(integer i=0; i < 10; i++) {
//    		
//    		if(i <5) {
//    			
//    			//add record instace into list
//    			imageLinks.add(new ImageLinks__c(Name = 'Test'+i, CaseId__c = workOrders[0].Id));
//    		} else {
//    			
//    			//add record instace into list
//    			imageLinks.add(new ImageLinks__c(Name = 'Test'+i, CaseId__c = workOrders[1].Id));
//    		}
//    	}
//    	
//    	//insert Image Link here
//    	insert imageLinks;
//    	
//    	//Test starts here
//    	Test.startTest();
//    	
//    	//Execute the batch here
//    	Database.executeBatch(new BatchDeleteImageLinks(), 200);
//    	
//    	//Test stops here
//    	Test.stopTest();
//    	
//    	//Query result of Image Link records after Batch execution
//    	imageLinks = [SELECT Id FROM ImageLinks__c];
//    	
//    	//Assert statements
//    	System.assertEquals(0, imageLinks.size());
//    }
}