/*
    This class is meant for new work orders that are not yet associated with any geocode cache records
    For each work order record it must:
     1. See if the address is already cached, associate the new work order with it, 
         and any further duplicate addresses
     2. Create a new cache (up to 10 new per execute) if it's not already cached,
          associate further duplicate addresses with it
*/
global without sharing class BatchGeocodeWorkOrderProperties implements Database.Batchable<Case>, Database.Stateful, Database.AllowsCallouts{
    private List<Case> ordersList;
    
    // This is our working set of work orders that need to be updated
    private List<Case> ordersToUpdate;
    
    // This is the list of geocode cache records we will be referencing, saving, or updating
    //private List<GeoCode_Cache__c> GeoCodeCache;
    private map<string,geocode_cache__c> GeoCodeCache;
    
    // This is a helper class to use to track what orders (by list index) belong with what geo caches (by list index)
    /*private class detail {
        public detail(integer firstIndex, Integer gcidx) {
            orderIndexes = new List<Integer> {firstIndex};
            GeoCodeIndex = gcidx;
        }
        public list<integer> orderIndexes {get; set;}
        public Integer GeoCodeIndex {get; private set;}
        //public boolean GCExists {get; private set;}
    }*/
    private class detail {
    	public detail(case c, geocode_cache__c g, string googleAPIjson){
    	    orders = new Case[]{c};
            property = g;
            googleJSON=googleAPIjson;
    	}
    	
        public detail(Case c, Geocode_Cache__c g) {
    	    orders = new Case[]{c};
            property = g;
        }
        public list<Case> orders {get; set;}
        public Geocode_Cache__c property {get; private set;}
        public string googleJSON {get; set;}
    }
    // We index the detail objects by address hash for easy reference
    private Map<string, detail> AddressMapGeoCache;
    // Flag set if we encounter items to quarantine
    private boolean quarantined;
    // Flag to cancel any further processing
    private boolean isOverLimit;
    private integer callouts = 0;
    private integer orderCount = 0;
    private integer ordersProcessed = 0;
    //error log created with failed upserts
    private string systemProcessLogBody;
    
    
    
    global Iterable<Case> start(Database.BatchableContext BC){
         
        //system.debug('!!! Start !!!'); 
    
        // Initialize limit flag
        isOverLimit = false;
        
        //Padmesh Soni (06/18/2014) - Support Case Record Type
        //Query record of Record Type of Case object
        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
                                            AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
        
        // Google imposes a limit of 2500 records per day. Don't try to do more than that // Non closed or canceled / Existing after 10/1/12
        //Padmesh Soni (06/18/2014) - Support Case Record Type
        //New filter criteria added into SOQL of RecordTypeId
        if(Test.isRunningTest()){//if the unit test is running use this query
            ordersList = [Select id, ContactId, Automatic_Reassignment__c, geocode_cache__c, street_address__c, city__c, state__c, 
                                zip_code__c , Trip_Charge_Reason__c, Vendor_Notes_To_Staff__c from Case 
                                WHERE RecordTypeId NOT IN: recordTypes 
                                AND (status!='Closed' and status!='Canceled') 
                                AND createdDate > 2012-10-01T00:00:00Z 
                                AND (
                                        (
                                            (Geocode_Cache__r.Location__Latitude__s = 0 or Geocode_Cache__r.Location__Latitude__s = null) and
                                            (Geocode_Cache__r.Location__Longitude__s = 0 or Geocode_Cache__r.Location__Longitude__s = null)
                                        )
                                        or Geocode_Cache__r.Last_Geocoded__c = null
                                        or geocode_cache__c = null
                                    ) 
                                order by due_date__c nulls last limit 2500];
        } else {//use the actual query
            /*ordersList = [Select id, ContactId, Automatic_Reassignment__c, geocode_cache__c, street_address__c, city__c, state__c, 
                                zip_code__c , Trip_Charge_Reason__c, Vendor_Notes_To_Staff__c from Case 
                                WHERE RecordTypeId NOT IN: recordTypes AND (status!='Closed' and status!='Canceled') 
                                and createdDate > 2012-10-01T00:00:00Z and ((Geocode_Cache__r.Location__Latitude__s = 0 
                                and Geocode_Cache__r.Location__Longitude__s = 0 and Geocode_Cache__r.Last_Geocoded__c = null) 
                                or geocode_cache__c = null) and Override_Quarantine__c != true order by due_date__c nulls last limit 2500];*/
              ordersList = [Select id, ContactId, Automatic_Reassignment__c, geocode_cache__c, street_address__c, city__c, state__c, 
                                zip_code__c , Trip_Charge_Reason__c, Vendor_Notes_To_Staff__c from Case 
                                WHERE RecordTypeId NOT IN: recordTypes 
                                AND (status!='Closed' and status!='Canceled') 
                                AND createdDate > 2012-10-01T00:00:00Z 
                                AND (
                                        (
                                            (Geocode_Cache__r.Location__Latitude__s = 0 or Geocode_Cache__r.Location__Latitude__s = null) and
                                            (Geocode_Cache__r.Location__Longitude__s = 0 or Geocode_Cache__r.Location__Longitude__s = null)
                                        )
                                        /*TBH:20170404-removing this or Geocode_Cache__r.Last_Geocoded__c = null*/
                                        or geocode_cache__c = null
                                    )
                                AND Override_Quarantine__c != true 
                                order by due_date__c nulls last limit 2500];
        }   
        
        orderCount = ordersList.size();
        //GeoCodeCache = new List<Geocode_cache__c>();
        GeoCodeCache = new map<string,geocode_cache__c>();
        ordersToUpdate = new List<Case>();
        AddressMapGeoCache = new Map<string, detail>();
        quarantined = false;
        systemProcessLogBody = '';
        
        //system.debug('!!!!! OrdersList Size: ' + ordersList.size());
        
        //return ordersList;
        return ordersList;
    }
    
    
    
    
    global void execute(Database.BatchableContext BC, List<Case> orders){
        
        //system.debug('!!! Execute :: orders: ' + orders);
        
        // In here we are going to process up to 10 orders at a time
        string tmpHash;
        detail tempDetail;
        geocode_cache__c[] tmpCache;
        
        for(Case ord : orders) {
            // Step 1: Hash the address
            string addrConcat = GeoUtilities.formatAddress(ord.Street_Address__c,ord.City__c,ord.State__c,ord.Zip_Code__c);
            tmpHash = GeoUtilities.createAddressHash(addrConcat);
            
            // Step 2: add to the orderstoupdate list
            if(ord.Trip_Charge_Reason__c == 'Other' && ord.Vendor_Notes_To_Staff__c == null){
                ord.Vendor_Notes_To_Staff__c = 'NONE';
            }
            ordersToUpdate.add(ord);
            
            // Step 3: Is the current address already processed?
            //   We know if the hash address is already added as an index in the tracker map
            //   If so, go ahead and update the map, adding in the index of the order to associate with the 
            //    indexed GeoCode Cache record. 
            // no callout needed
            if(AddressMapGeoCache.containsKey(tmpHash)) {
                AddressMapGeoCache.get(tmpHash).orders.add(ord);
            }
            // Step 4: Check for an existing GeoCache in the database
            //   If we find one, create a record of it in the tracker map for later reference
            //   ELSE do a callout and retrieve geo data and create a new geo cache object
            else {
                tmpCache = [select id, address_hash__c, 
                                   formatted_street_address__c, street_address__c, 
                                   formatted_city__c, city__c, 
                                   formatted_state__c, state__c, 
                                   formatted_zip_code__c, zip_code__c, 
                                   quarantined__c, flag__c, 
                                   location__latitude__s, location__longitude__s,
                                   geo_system__c, geo_timestamp__c,
                                   last_geocoded__c,
                                   Url_Used__c
                            from geocode_cache__c where address_hash__c = :tmpHash];
                if(tmpCache.size() > 0) {//if there is a geocode record
                    /*******************************************************/
                    /*/if the unit test is running
                    if(system.Test.isRunningTest()){//because we can't make callouts, set the location data
                        if(tmpCache[0].address_hash__c == '400cedarstbrdgprtct60562'){
                            Map<string,string> cacheData = GeoUtilities.getGeoData(addrConcat);
                            tmpCache[0].location__latitude__s = 11.11111111;
                            tmpCache[0].location__longitude__s = -111.22222222;
                            tmpCache[0].geo_system__c = 'BatchGeocodeWorkOrderProperties.cls';
                            tmpCache[0].last_geocoded__c = Date.today();
                            tmpCache[0].geo_timestamp__c = Datetime.now();
                            tmpCache[0].Url_Used__c = cacheData.get('url');
                            update tmpCache;
                        }
                    }*/
                    /*******************************************************/
                    //if lat. and long. are 0 or null
                    if(
                        (
                            (tmpCache[0].Location__Latitude__s == 0 || tmpCache[0].Location__Latitude__s == null) && 
                            (tmpCache[0].Location__Longitude__s == 0 || tmpCache[0].Location__Longitude__s == null)
                        )
                        || tmpCache[0].last_geocoded__c == null
                      ){
                        GeoCodeCache.put(tmpHash, tmpCache[0]);//Add the geocache to the working list
                        geocodeRecord(addrConcat, tmphash, ord);//geocode the record
                    }
                    else{   
                        AddressMapGeoCache.put(tmpHash, new detail(ord, tmpCache[0]));//add the good geocode_cache to the AddressMapGeoCache map
                    }
                } else {//if no geocode record
                    // Skip callouts if we're over limit
                    if(isOverLimit) {
                        //system.debug('!!! Over Query Limit :: Skipping case set');
                        continue;//skip the rest and loop to the next case. 
                    }
                    geocodeRecord(addrConcat, tmphash, ord);//geocode a new record
                }
            } 
        }   
    }
    
    
    
    private void geocodeRecord(string addrConcat, string tmphash, Case ord){
        boolean tmpBool;
        geocode_cache__c tmpCacheRec;
        map<string,string> tmpCacheData;
        detail tmpdetail;
        GeoUtilities.geoUtilOutParam out = new GeoUtilities.geoUtilOutParam(); 

        // Callout
        callouts++;
        tmpCacheData = GeoUtilities.getGeoData(addrConcat,out);
        // Stop batch execution if we're over limit
        if(tmpCacheData.get('status')=='OVER_QUERY_LIMIT') {
            //system.debug('OVER QUERY LIMIT!! HALTING!!!');
            isOverLimit = true;
            return;//skip the rest and loop to the next case.
        }
        
        // doing it this way since I can't cast string to boolean -- thanks salesforce!!
        if(tmpCacheData.get('partial')=='1') {
            tmpBool = true;
            quarantined = true;
        } else {
            tmpBool = false;
        }
        
        if(GeoCodeCache.containsKey(tmphash)){//if the GeoCodeCache map has the tmphash key, get the geocode record
            tmpCacheRec = GeoCodeCache.get(tmphash);
        }
        else{//if no key, make a new geocode record
            tmpCacheRec = new geocode_cache__c (
                                        address_hash__c = tmphash,
                                        formatted_address__c = addrConcat,
                                        Formatted_Street_Address__c = GeoUtilities.capitalizeAll(GeoUtilities.preformatAddress(ord.Street_Address__c)),
                                        Formatted_City__c = ord.City__c,
                                        Formatted_State__c = ord.State__c,
                                        Formatted_Zip_Code__c = GeoUtilities.preformatZip(ord.Zip_Code__c),
                                        Street_Address__c = ord.Street_Address__c,
                                        City__c = ord.City__c,
                                        State__c = ord.State__c,
                                        Zip_Code__c = ord.Zip_Code__c
                                    );
        }
        tmpCacheRec.location__latitude__s = decimal.valueOf(tmpCacheData.get('lat'));
        tmpCacheRec.location__longitude__s = decimal.valueOf(tmpCacheData.get('lng'));
        tmpCacheRec.last_geocoded__c = date.today();
        tmpCacheRec.manual_location__c = false;
        tmpCacheRec.partial_match__c = tmpBool;
        tmpCacheRec.geocode_status__c = tmpCacheData.get('status');
        tmpCacheRec.quarantined__c = tmpBool;
        tmpCacheRec.geo_system__c = 'BatchGeocodeWorkOrderProperties.cls';
        tmpCacheRec.geo_timestamp__c = Datetime.now();
        tmpCacheRec.Url_Used__c = tmpCacheData.get('url');
        
        //Code added - Padmesh Soni (12/11/2014) - CCP-15: Fill County on Property
        //assign county value
        if(tmpCacheData.containsKey('administrative_area_level_2'))
            tmpCacheRec.County__c = tmpCacheData.get('administrative_area_level_2');
            
        /********************************************************************/
        /*/if the unit test is running
        if(system.Test.isRunningTest()){//because we can't make callouts, quarantine this property
            if(tmpCacheRec.Address_hash__c == '515partialstbrdgprtct60562'){
                tmpCacheRec.Partial_Match__c = true;
                tmpCacheRec.quarantined__c = true;
                update tmpCacheRec;
            }
        }*/
        /********************************************************************/
        // Add the new geocode cache record to the working list
        GeoCodeCache.put(tmphash, tmpCacheRec);
    
        AddressMapGeoCache.put(tmpHash, new detail(ord, tmpCacheRec, out.stringValue));
    }
    
    
    
    
    global void finish(Database.BatchableContext BC){
        system.debug('!!! Finish :: Orderstoupdate size: ' + ordersToUpdate.size());
        
        // Step 1:  Save the GeoCodeCache list
        //database.upsert(GeoCodeCache,false);
        if(GeoCodeCache.size() > 0){
            //get the results of the database.upsert
            Database.Upsertresult[] propertyDmlResults = database.upsert(GeoCodeCache.values(),false);
            for(Database.Upsertresult sr : propertyDmlResults){
                if(!sr.isSuccess()){//get the errors, if any, into the systemProcessLogBody string
                    for(Database.Error e : sr.getErrors()){
                        systemProcessLogBody += 'Property Upsert Exception' + '\r\n' + 
                                                '    Error Message: ' + e.getMessage() + '\r\n' +
                                                '    Status Code: ' + e.getStatusCode() + '\r\n' +
                                                '    Field(s): ' + e.getFields() + '\r\n' + '\r\n';
                    }
                }
            }
        }
        
        // Step 2: Use the tracker map to update the ordersToUpdate list
        Set<string> hashkeys = AddressMapGeoCache.keySet();
        detail tmpdetail;
        // Loop through the master hash indexed address list
        for(string key : hashkeys) {
            // get the current detail record
            tmpdetail = AddressMapGeoCache.get(key);
            // Loop through the list of work order indexes, add the geocodecache index to the ordersToUpdate record
            /*for(integer idx : tmpdetail.orderIndexes) {
                ordersToUpdate[idx].GeoCode_cache__c = GeoCodeCache[tmpdetail.GeoCodeIndex].id;
                ordersProcessed++;
                if(GeoCodeCache[tmpdetail.GeoCodeIndex].quarantined__c==true){
                        ordersToUpdate[idx].Approval_Status__c = 'Quarantined';
                    }
                if(GeoCodeCache[tmpdetail.GeoCodeIndex].flag__c==true){
                        ordersToUpdate[idx].Approval_Status__c = 'Quarantined-Flagged';
                }
                if(ordersToUpdate[idx].ContactId == null && ordersToUpdate[idx].Approval_Status__c!='Quarantined' && ordersToUpdate[idx].Approval_Status__c!='Quarantined-Flagged'){
                    ordersToUpdate[idx].Automatic_Reassignment__c = true;
                }
            }*/
            for(Case c : tmpdetail.orders) {
                c.GeoCode_cache__c = tmpdetail.property.id;
                ordersProcessed++;
                if(tmpdetail.property.quarantined__c==true){
                        c.Approval_Status__c = 'Quarantined';
                    }
                if(tmpdetail.property.flag__c==true){
                        c.Approval_Status__c = 'Quarantined-Flagged';
                }
                if(c.ContactId == null && c.Approval_Status__c!='Quarantined' && c.Approval_Status__c!='Quarantined-Flagged'){
                    c.Automatic_Reassignment__c = true;
                }
            }
        }
        
        //system.debug('!!! Address Geocache Map : ' + AddressMapGeoCache);
        
        // Step 3: Update the work orders
        //database.upsert(ordersToUpdate,false);
        if(ordersToUpdate.size() > 0){
            //get the results of the database.upsert
            Database.Upsertresult[] orderDmlResults = database.upsert(ordersToUpdate,false);
            for(Database.Upsertresult sr : orderDmlResults){
                if(!sr.isSuccess()){//get the errors, if any, into the systemProcessLogBody string
                    for(Database.Error e : sr.getErrors()){
                        systemProcessLogBody += 'Work Order Upsert Exception' + '\r\n' + 
                                                '    Error Message: ' + e.getMessage() + '\r\n' +
                                                '    Status Code: ' + e.getStatusCode() + '\r\n' +
                                                '    Field(s): ' + e.getFields() + '\r\n' + '\r\n';
                    }
                }
            }
        }
        /*******************************************************************************************************************/
        //create a list for case IDs
        List<ID> caseIDs = new List<ID>();
        //add case IDs from ordersToUpdate into caseIDs
        for(Case cID : ordersToUpdate){
            caseIDs.add(cID.Id);
        }
        
                
        insert new Geocode_Process_Log__c(Callouts__c = callouts, Orders_Processed__c = ordersProcessed, Work_Order_Count__c = orderCount, Exceeded_Limit__c = isOverLimit);
        
        // Step 4: Notify -- if the quarantined flag is set
        if(quarantined) {
            string[] ids = new string[] {};
            string[] emails = new string[] {};
            group[] g = [select id from Group where Name='Quarantine_Managers'];
            if(g.size()>0) {
                groupmember[] gm = [select userorgroupid from groupmember where groupid=: g[0].id];
                for(groupmember grm : gm) {
                    ids.add(grm.userorgroupid);
                }
                user[] u = [select email from user where id IN :ids];
                for(user ur : u) {
                    emails.add(ur.email);
                }
            
                if(emails.size()>0) {
                    emailtemplate et = [select id from emailtemplate where developername=: 'WO_Quarantine_Notification'];
                     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                     mail.setToAddresses(emails);
                     mail.setTemplateId(et.id);
                     mail.saveAsActivity = false;
                     mail.setTargetObjectId(u[0].id);
                     Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                     
                     //system.debug('sent notification email!!');
                }
            }
        }
        
        //Step 5: Create a new system alert if there were any dml errors
        if(systemProcessLogBody != null && systemProcessLogBody != ''){
            System_Alerts__c newSA = new System_Alerts__c(
                Subject_Line__c = 'Geocoder Alert',
                Process_Name__c = 'Geocoder',
                Body__c = 'The Geocoder Batch Process (BatchGeocodeWorkOrderProperties.cls) has encountered Salesforce DML errors:' + '\r\n'
                            + '\r\n' + systemProcessLogBody
            );
            insert newSA;
        }
        
        
        saveGoogleAPI(AddressMapGeoCache);  //20170314 - capturing google api data.
        
    }
    
    
    /*
    	20170313 - Tyler H: this routine is responsible for saving the googleAPI integration information as a child record to the property (geocodecache object)
    */
    static void saveGoogleAPI(Map<string, detail> AddressMapGeoCache){
    	try{
    	
	    	//AddressMapGeoCache Select i.ward__c, i.transit_station__c, i.train_station__c, i.subpremise__c, i.sublocality_level_5__c, i.sublocality_level_4__c, i.sublocality_level_3__c, i.sublocality_level_2__c, i.sublocality_level_1__c, i.sublocality__c, i.street_number__c, i.street_address__c, i.route__c, i.room__c, i.premise__c, i.postal_town__c, i.postal_code_suffix__c, i.postal_code__c, i.post_box__c, i.political__c, i.point_of_interest__c, i.parking__c, i.park__c, i.neighborhood__c, i.natural_feature__c, i.locality__c, i.intersection__c, i.country__c, i.colloquial_area__c, i.bus_station__c, i.airport__c, i.administrative_area_level_5__c, i.administrative_area_level_4__c, i.administrative_area_level_3__c, i.administrative_area_level_2__c, i.administrative_area_level_1__c, i.SystemModstamp, i.Related_Property__c, i.Name, i.LastModifiedDate, i.LastModifiedById, i.LastActivityDate, i.IsDeleted, i.Id, i.CreatedDate, i.CreatedById From INT_GoogleInbound__c i
	    	INT_GoogleInbound__c[] apiResults = new INT_GoogleInbound__c[]{};
	    	
	    	for (string key : AddressMapGeoCache.keyset()){
	    		string json = AddressMapGeoCache.get(key).googleJSON;
	    		id propID =  AddressMapGeoCache.get(key).property.id;
	    		if (!string.isblank(json))
	    			apiResults.add(saveGoogleAPI_setFlds(json,propID));
	    	}
	    	database.insert(apiResults, false);
    	}
    	catch (Exception e){
    		sendErrEmail(e);
    	}
    }
    
    
    static void sendErrEmail(Exception e){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new String[] {'software@northsight.com'});
		mail.setSenderDisplayName('NSM SFDC Error Alert');
		mail.setSubject('BatchGeoCodeWorkOrderProperties Error');
		mail.setUseSignature(false);
		mail.setPlainTextBody(e.getMessage() + e.getStackTraceString() );		
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    static INT_GoogleInbound__c saveGoogleAPI_setFlds(string json, id propertyID){
    	map<string,string> keyVals = GeoUtilities.convertGoogleAPIjsonToMap(json);
    	    	
    	INT_GoogleInbound__c ret = new INT_GoogleInbound__c();
    	ret.Related_Property__c = propertyID;
    	ret.rawJSON__c=json;
    	ret.status__c=keyVals.get('status');
    	
    	if(keyVals.get('status')!='OK') return ret;
    	
    	ret.administrative_area_level_1__c = keyVals.get('administrative_area_level_1');
		ret.administrative_area_level_2__c = keyVals.get('administrative_area_level_2');
		ret.administrative_area_level_3__c = keyVals.get('administrative_area_level_3');
		ret.administrative_area_level_4__c = keyVals.get('administrative_area_level_4'); 
		ret.administrative_area_level_5__c = keyVals.get('administrative_area_level_5');
		ret.airport__c = keyVals.get('airport');
		ret.bus_station__c = keyVals.get('bus_station');
		ret.colloquial_area__c = keyVals.get('colloquial_area');
		ret.country__c = keyVals.get('country');
		ret.intersection__c = keyVals.get('intersection');
		ret.locality__c = keyVals.get('locality');
		ret.natural_feature__c = keyVals.get('natural_feature');
		ret.neighborhood__c = keyVals.get('neighborhood');
		ret.park__c = keyVals.get('park');
		ret.parking__c = keyVals.get('parking');
		ret.point_of_interest__c = keyVals.get('point_of_interest');
		ret.political__c = keyVals.get('political');
		ret.post_box__c = keyVals.get('post_box');
		ret.postal_code__c = keyVals.get('postal_code');
		ret.postal_town__c = keyVals.get('postal_town');
		ret.premise__c = keyVals.get('premise');
		ret.room__c = keyVals.get('room');
		ret.route__c = keyVals.get('route');
		ret.street_address__c = keyVals.get('street_address');
		ret.street_number__c = keyVals.get('street_number');
		ret.sublocality__c = keyVals.get('sublocality');
		ret.sublocality_level_1__c = keyVals.get('sublocality_level_1');
		ret.sublocality_level_2__c = keyVals.get('sublocality_level_2');
		ret.sublocality_level_3__c = keyVals.get('sublocality_level_3');
		ret.sublocality_level_4__c = keyVals.get('sublocality_level_4');
		ret.sublocality_level_5__c = keyVals.get('sublocality_level_5');
		ret.subpremise__c = keyVals.get('subpremise');
		ret.train_station__c = keyVals.get('train_station');
		ret.transit_station__c = keyVals.get('transit_station');
		ret.ward__c = keyVals.get('ward');
		ret.result_type__c = keyVals.get('result_type');
		ret.place_id__c=keyVals.get('place_id');
		ret.formatted_address__c=keyVals.get('formatted_address');
		ret.latitude__c=keyVals.get('lat');
		ret.longitude__c=keyVals.get('lng');
		ret.location_type__c=keyVals.get('location_type');
		ret.place_id__c=keyVals.get('place_id');
		ret.result_type__c=keyVals.get('result_type');
		ret.postal_code_suffix__c=keyVals.get('postal_code_suffix');
		
		
    	return ret;
    }
    
	global static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}    
    
	
}