public without sharing class TestDataFactory {
	
	static Work_Code__c workCode_grassCutREO=genGrassCutREOworkCode();
 	static Work_Code_Mapping__c workCodeMapping_grassCutREO=genWorkCodeMapping(workCode_grassCutREO);
 	static Work_Survey__c workSurvey=genWorkSurvey();
 	static Work_Code_Survey__c workCodeSurvey=genWorkCodeSurvey(workSurvey,workCode_grassCutREO);
 	static Work_Order_Instructions__c workOrderInstructions;
 	static case newCase;
 	static account newAccount;
	static contact newContact; 	
 	
 	
 	static void genWorkOrderSurvey(id workSurveyID, id workOrderID){
 	 	Work_Order_Survey__c ret = new Work_Order_Survey__c(
    		Comments__c = 'Comment 1',
    		Make_Survey_Required__c = true,
    		Service_Name__c = 'service 1',
    		Status__c = 'Active',
    		Survey_Number__c = workSurveyID,
    		Work_Order_Number__c = workOrderID
    	);
    	
    	insert ret;
    	 
 	}    
 	
 	static Work_Order_Instructions__c genWorkOrderInstructions(id caseID){ 		
		Work_Order_Instructions__c ret = new Work_Order_Instructions__c(Instruction_Type__c = 'ACCESS UNKNOWN',Item_Instructions__c = 'Testing is going on.', Line__c =1 , Case__c = caseID, Action__c ='Bid');
		
		insert ret;
		return ret; 
 	}
 	
 	static Work_Code_Survey__c genWorkCodeSurvey(Work_Survey__c ws, Work_Code__c wc){
 	        Work_Code_Survey__c ret = new Work_Code_Survey__c(
	            Comments__c = 'Work Code Survey Comments 1',
	            Make_Survey_Required__c = true,
	            Survey_Number__c = ws.Id,
	            Work_Code_Name__c = wc.Id
        	);
        	insert ret;
        	return ret;
 	}
 	
 	static Work_Survey__c genWorkSurvey(){
		Work_Survey__c ret= new Work_Survey__c(
			Comments__c = 'Work Survey Comments1',
			Description__c = 'Work Survey Description1',
			Survey_External_ID__c = 'Work Survey External ID1',
			Service_Name__c = 'Service Name1');
		insert ret;
		return ret;
 	}

	public static void create1(){
		user joshSarchet = [select id from user where id='00540000001K1jO'];
		system.runas(joshSarchet)
		{
			

			insertStates();
			
			newAccount = new Account(
				Name = 'Test Vendor Account',
				Account_Supervisor__c=joshSarchet.Id,
				RecordTypeID='0124000000011Q2' //vendor
			);
			insert newAccount;

			newContact = new Contact(
				FirstName='Testy',
				LastName = 'Tester',
				AccountId = newAccount.Id,
				Pruvan_Username__c = 'NSBobTest',
				MailingStreet = '30 Turnberry Dr',
				MailingCity = 'La Place',
				MailingState = 'LA',
				MailingPostalCode = '70068',
				OtherStreet = '30 Turnberry Dr',
				OtherCity = 'La Place',
				OtherState = 'LA',
				OtherPostalCode = '70068',
				Other_Address_Geocode__Latitude__s = 30.00,
				Other_Address_Geocode__Longitude__s = -90.00,
				recordtypeid='01240000000UPSr'
			);
			insert newContact;
			
			
			
			Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community - Vendors']; 
			User user_vendor = new User(Alias = 'nsmvend', Email='vendor@northsighttest.com', 
			EmailEncodingKey='UTF-8', LastName='Vendorman', LanguageLocaleKey='en_US', 
			LocaleSidKey='en_US', ProfileId = p.Id, contactid=newContact.id,
			TimeZoneSidKey='America/Los_Angeles', UserName='vendor@northsighttest.com');

			insert user_vendor;
  
	

	
			//create a new test case
			List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
				AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
											
			Account account =  new Account(Name = 'Test Client Account',Client_Code__c='XXX',recordTypeId=accountRecordTypes[0].Id, Account_Supervisor__c=joshSarchet.Id);
			insert account; 
			
			newCase = new Case();
			newCase.street_address__c = '1234 Street';
			newCase.state__c = 'TX';
			newCase.city__c = 'Austin';
			newCase.zip_code__c = '78704';
			newCase.Vendor_code__c = 'SRSRSR';
			newCase.Work_Order_Type__c = 'Unknown';
			newCase.Client__c = 'NOT-SG';
			newCase.Loan_Type__c = 'REO';
			newCase.ContactId = newContact.Id;
			newCase.Work_Ordered__c = 'Grass Recut';
			newCase.Work_Completed__c = 'Yes';
			newCase.Client_Name__c = account.Id;
			newCase.due_date__c=Date.today();
			newcase.Client__c='XXX';
			newcase.Date_Serviced__c = Date.newInstance(2013,1,1);
			newcase.Pre_Pending_Date_Time__c = Datetime.newInstance(2013,1,1);
			newcase.Approval_Status__c='Not Started';
			
			insert newCase;
			
			insert new Status__c(case__c = NewCase.Id, Expected_Upload_Date__c = Date.today(), Delay_Reason__c='Nothing', Order_Status__c = 'Done');
			workOrderInstructions=genWorkOrderInstructions(newCase.id);
			
			genWorkOrderSurvey(workSurvey.id, newCase.id );
			
			newCase.Approval_Status__c='Approved';
			update newCase;
			
			genTaskOnCase(newCase,user_vendor);
			
			genClientCommitments(newCase);
			
			newCase = [SELECT Id,Geocode_Cache__c
					FROM Case
					WHERE Id = :newCase.Id];
			
			Geocode_Cache__c[] newProperty=[select id from Geocode_Cache__c where id=:newCase.Geocode_Cache__c];
			
			if (newProperty.size()>0){
				update newProperty;
				genPropertyRequest(newProperty[0]);
			}
		 
			
			

		}
	}
	
	
	public static void create2(){
		user joshSarchet = [select id from user where id='00540000001K1jO'];
		system.runas(joshSarchet)
		{
		
			
			insert new ImageLinks__c(
				CaseId__c = newCase.Id,
				GPS_Timestamp__c = datetime.newInstance(0).addSeconds(1385540760),
				Pruvan_uuid__c = '3'
			);
			
			
			insertPaymentDispute(newCase);
			
			Id receivableInv = [SELECT Id FROM RecordType WHERE Name = 'Receivable' AND SobjectType = 'Invoice__c'].Id;
			Date currTime = Date.today();
			Invoice__c inv1 = new Invoice__c(
				Work_Order__c = newCase.Id,
				Paid_Date__c = currTime,
				Status__c = 'Open', Sent__c =  currTime,
				Receive_From__c = newAccount.Id,//'0014000000IsTotAAF',
				RecordTypeId  = receivableInv//'012f00000008rWJ'
			);
			insert inv1;
			inv1 = [SELECT Id, Paid_Date__c, Work_Order__c,Name, Status__c, Receive_From__c, RecordTypeId
					FROM Invoice__c
					WHERE Id = :inv1.Id];
			
			Product2 newProduct = new Product2();
			newProduct.name ='Trip Charge';
			newProduct.Family = 'Service Fee';
			newProduct.IsActive = true;
			insert newProduct;
			
			
			Id receivableInvLine = [SELECT Id FROM RecordType WHERE Name = 'Receivable' AND SobjectType = 'Invoice_Line__c'].Id;
			Id tripCharge = [SELECT Id FROM Product2 WHERE Name = 'Trip Charge' limit 1].Id;
			Invoice_Line__c invL = new Invoice_Line__c(
				Product__c= tripCharge,//'01t40000004sJnVAAU',
				Quantity__c = 24,
				Unit_Price__c = 45, Discount__c =  40,
				Invoice__c = inv1.Id,
				RecordTypeId  = receivableInvLine//'012f00000008rWT'//AR
			);
			
			insert invL;
		}//end runAs
	}
	
	static void insertPaymentDispute(case c){
		Payment_Dispute__c testDispute = new Payment_Dispute__c(Work_Order_Number__c = c.Id, Dispute_Reason__c = 'test');
		insert testDispute;	
	}
	
	static void insertStates(){
		
			string jsonstring='[{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9moAAC"},"FHA_Initial_Cut_1_10000sqf__c":70.00,"FHA_Initial_Cut_10001_20000sqf__c":90.00,"FHA_Recut_1_10000_sqf__c":65.00,"FHA_Recut_10001_20000_sqft__c":85.00,"Name":"AL","Id":"a1r33000000F9moAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9mpAAC"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"AK","Id":"a1r33000000F9mpAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9mqAAC"},"FHA_Initial_Cut_1_10000sqf__c":75.00,"FHA_Initial_Cut_10001_20000sqf__c":95.00,"FHA_Recut_1_10000_sqf__c":70.00,"FHA_Recut_10001_20000_sqft__c":90.00,"Name":"AZ","Id":"a1r33000000F9mqAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9mrAAC"},"FHA_Initial_Cut_1_10000sqf__c":70.00,"FHA_Initial_Cut_10001_20000sqf__c":90.00,"FHA_Recut_1_10000_sqf__c":65.00,"FHA_Recut_10001_20000_sqft__c":85.00,"Name":"AR","Id":"a1r33000000F9mrAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9msAAC"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"CA","Id":"a1r33000000F9msAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9mtAAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"CO","Id":"a1r33000000F9mtAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9muAAC"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"CT","Id":"a1r33000000F9muAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9mvAAC"},"FHA_Initial_Cut_1_10000sqf__c":95.00,"FHA_Initial_Cut_10001_20000sqf__c":125.00,"FHA_Recut_1_10000_sqf__c":90.00,"FHA_Recut_10001_20000_sqft__c":120.00,"Name":"DE","Id":"a1r33000000F9mvAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9mwAAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"FL","Id":"a1r33000000F9mwAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9mxAAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"GA","Id":"a1r33000000F9mxAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9myAAC"},"FHA_Initial_Cut_1_10000sqf__c":110.00,"FHA_Initial_Cut_10001_20000sqf__c":130.00,"FHA_Recut_1_10000_sqf__c":105.00,"FHA_Recut_10001_20000_sqft__c":125.00,"Name":"HI","Id":"a1r33000000F9myAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9mzAAC"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"ID","Id":"a1r33000000F9mzAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9n0AAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"IL","Id":"a1r33000000F9n0AAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9n1AAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"IN","Id":"a1r33000000F9n1AAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9n2AAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"IA","Id":"a1r33000000F9n2AAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9n3AAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"KS","Id":"a1r33000000F9n3AAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9n4AAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"KY","Id":"a1r33000000F9n4AAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9n5AAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"LA","Id":"a1r33000000F9n5AAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9n6AAC"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"ME","Id":"a1r33000000F9n6AAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9n7AAC"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"MD","Id":"a1r33000000F9n7AAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9n8AAC"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"MA","Id":"a1r33000000F9n8AAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9n9AAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"MI","Id":"a1r33000000F9n9AAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nAAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"MN","Id":"a1r33000000F9nAAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nBAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"MS","Id":"a1r33000000F9nBAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nCAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"MO","Id":"a1r33000000F9nCAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nDAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"MT","Id":"a1r33000000F9nDAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nEAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"NE","Id":"a1r33000000F9nEAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nFAAS"},"FHA_Initial_Cut_1_10000sqf__c":90.00,"FHA_Initial_Cut_10001_20000sqf__c":110.00,"FHA_Recut_1_10000_sqf__c":85.00,"FHA_Recut_10001_20000_sqft__c":105.00,"Name":"NV","Id":"a1r33000000F9nFAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nGAAS"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"NH","Id":"a1r33000000F9nGAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nHAAS"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"NJ","Id":"a1r33000000F9nHAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nIAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"NM","Id":"a1r33000000F9nIAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nJAAS"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"NY","Id":"a1r33000000F9nJAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nKAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"NC","Id":"a1r33000000F9nKAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nLAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"ND","Id":"a1r33000000F9nLAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nMAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"OH","Id":"a1r33000000F9nMAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nNAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"OK","Id":"a1r33000000F9nNAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nOAAS"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"OR","Id":"a1r33000000F9nOAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nPAAS"},"FHA_Initial_Cut_1_10000sqf__c":95.00,"FHA_Initial_Cut_10001_20000sqf__c":115.00,"FHA_Recut_1_10000_sqf__c":90.00,"FHA_Recut_10001_20000_sqft__c":110.00,"Name":"PA","Id":"a1r33000000F9nPAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nQAAS"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"RI","Id":"a1r33000000F9nQAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nRAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"SC","Id":"a1r33000000F9nRAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nSAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"SD","Id":"a1r33000000F9nSAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nTAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"TN","Id":"a1r33000000F9nTAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nUAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"TX","Id":"a1r33000000F9nUAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nVAAS"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"UT","Id":"a1r33000000F9nVAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nWAAS"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"VT","Id":"a1r33000000F9nWAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nXAAS"},"FHA_Initial_Cut_1_10000sqf__c":95.00,"FHA_Initial_Cut_10001_20000sqf__c":115.00,"FHA_Recut_1_10000_sqf__c":90.00,"FHA_Recut_10001_20000_sqft__c":110.00,"Name":"VA","Id":"a1r33000000F9nXAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nYAAS"},"FHA_Initial_Cut_1_10000sqf__c":100.00,"FHA_Initial_Cut_10001_20000sqf__c":120.00,"FHA_Recut_1_10000_sqf__c":95.00,"FHA_Recut_10001_20000_sqft__c":115.00,"Name":"WA","Id":"a1r33000000F9nYAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nZAAS"},"FHA_Initial_Cut_1_10000sqf__c":95.00,"FHA_Initial_Cut_10001_20000sqf__c":115.00,"FHA_Recut_1_10000_sqf__c":90.00,"FHA_Recut_10001_20000_sqft__c":110.00,"Name":"WV","Id":"a1r33000000F9nZAAS"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9naAAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"WI","Id":"a1r33000000F9naAAC"},{"attributes":{"type":"State__c","url":"/services/data/v38.0/sobjects/State__c/a1r33000000F9nbAAC"},"FHA_Initial_Cut_1_10000sqf__c":85.00,"FHA_Initial_Cut_10001_20000sqf__c":105.00,"FHA_Recut_1_10000_sqf__c":80.00,"FHA_Recut_10001_20000_sqft__c":100.00,"Name":"WY","Id":"a1r33000000F9nbAAC"}]';
			state__c[] states = (state__c[]) JSON.deserialize(jsonstring, state__c[].class);
			for (state__c s : states) s.id=null;
			
			insert states;
	}
	
	static Work_Code__c genGrassCutREOworkCode() {
	    Work_Code__c ret = new Work_Code__c(
            Name = 'GCREO01',
            Work_Code_Description__c = 'Grass Cut REO',
            Review_Needed__c =true,
            Work_Ordered__c = 'Grass Cut', 
            Department__c='Maintenance'
        );
        insert ret;
        return ret;
	}
	
	static Work_Code_Mapping__c genWorkCodeMapping(Work_Code__c code){
		//create new work code mapping
		Work_Code_Mapping__c ret = new Work_Code_Mapping__c(
		 City__c = 'Dallas', 
		 Client__c = 'SG', 
		 Client_Special_1__c = 'Test Client1', 
		 Client_Special_2__c = 'Test Client2', 
		 Client_Special_3__c = 'Test Client3', 
		 Customer__c = 'Test Customer', 
		 Loan_Type__c = 'REO', 
		 Mapping_Priority__c = 300, 
		 State__c = 'CA', 
		 Vendor_Code__c = '123', 
		 Work_Code__c = code.Id, 
		 Work_Ordered_Text__c = 'Test WO Data', 
		 Zip_Code__c = '12345'
		);
		insert ret;
		return ret;
	}
	
	static void genClientCommitments(case c){
		Client_Commitment__c ret = new Client_Commitment__c( 
			Work_Order__c=c.Id, Internal_Comments__c='the internal comments', Commitment_Date__c=Date.today(), Current_Status_of_Order__c='Not Started', Root_Cause_of_Delay__c='Field Capacity', Public_Comments__c='the public comments'
		);
					
		insert ret; 
	}
	
	static void genPropertyRequest(Geocode_Cache__c prop){
        Request__c request = new Request__c(
        	Property_Number_or_Loan_Number__c = '12345',
            Type__c = 'test',
            Property__c=prop.id
        );
        insert request;	
	}
	
	
	static void genTaskOnCase(case c, user u){
	
		Task newTask = new Task(
			OwnerId = u.Id,
			WhatId = c.Id,
			Type = 'Other',
			Subject = 'Subject: s',
			Description = 'Comments, Comments, Comments',
			Status = 'Not Started',
			Priority = 'High'
		);
		insert newTask;	
	
	}
	
	public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				
		
	
	
	}
	
}