@isTest(seeAllData=false)
private class Test_InvoicingController {
//
///**
// *  Purpose         :   This class is controller for Invoicing page which is used to generate Invoices and inclusion of their Invoice Line Items. 
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   12/30/2014
// *
// *  Current Version :   V1.4
// *
// *  Revision Log    :   V1.0 - Created - AC-6:Invoice Screen | Visual Force Page Prototype
// **/
// 
//    static testMethod void test_Invoicing() {
//        
//        //Query record of Record Type of Account and Case objects
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//                                            OR DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR 
//                                            OR DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AP) 
//                                            AND (SobjectType =: Constants.CASE_SOBJECTTYPE OR SobjectType = 'Invoice__c' 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) 
//                                            AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(4, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsToInsert = new List<Account>();
//        accountsToInsert.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsToInsert.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsToInsert;
//        
//        //List to hold Invoice Line records
//        List<Pricebook2> priceBooks = new List<Pricebook2>();
//        priceBooks.add(new Pricebook2(Name = 'Test PB 1', Client__c = accountsToInsert[0].Id, isActive=true));
//        priceBooks.add(new Pricebook2(Name = 'Test PB 2', Client__c = accountsToInsert[0].Id, isActive=true));
//        priceBooks.add(new Pricebook2(Name = 'Test PB 3', Client__c = accountsToInsert[0].Id, isActive=true));
//        priceBooks.add(new Pricebook2(Name = 'Test PB 4', Client__c = accountsToInsert[1].Id, isActive=true));
//        
//        insert priceBooks;
//        
//        //List to hold Case records
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//		
//        List<Case> workOrders = new List<Case>();
//        workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, Client__c = AssignmentMapConstants.CASE_CLIENT_SG,
//                                    AccountId = accountsToInsert[0].Id, Due_Date__c = Date.today().addDays(-2), Status = Constants.CASE_STATUS_OPEN,
//                                    Street_Address__c = '301 Front St', City__c = 'Nome', State__c = 'AK', Zip_Code__c = '99762', Vendor_Code__c = 'CHIGRS',
//                                    Work_Ordered__c = 'Initial Grass Cut',Client_Name__c = account.Id));
//        workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, Client__c = AssignmentMapConstants.CASE_CLIENT_SG,
//                                    AccountId = accountsToInsert[1].Id, Due_Date__c = Date.today().addDays(-2), Status = Constants.CASE_STATUS_OPEN,
//                                    Street_Address__c = '301 Front St', City__c = 'Nome', State__c = 'AK', Zip_Code__c = '99762', Vendor_Code__c = 'CHIGRS',
//                                    Work_Ordered__c = 'Initial Grass Cut',Client_Name__c = account.Id));
//        
//        //insert case records
//        insert workOrders;
//        
//        //List to hold Invoice Line records
//        List<Product2> products = new List<Product2>();
//        products.add(new Product2(Name = 'Test 1', Family = 'Cleaning', IsActive = true));
//        products.add(new Product2(Name = 'Test 2', Family = 'Code Violation', IsActive = true));
//        products.add(new Product2(Name = 'Test 3', Family = 'Code Development', IsActive = true));
//        products.add(new Product2(Name = 'Test 4', Family = 'Coding Issues', IsActive = true));
//        
//        //insert product here
//        insert products;
//        
//        // This is available irrespective of the state of SeeAllData.
//        Id pricebookId = Test.getStandardPricebookId();
//        
//        // 1. Insert a price book entry for the standard price book.
//        // Standard price book entries require the standard price book ID we got earlier.
//        PricebookEntry standardPrice = new PricebookEntry(
//            Pricebook2Id = pricebookId, Product2Id = products[0].Id,
//            UnitPrice = 10000, IsActive = true);
//        insert standardPrice;
//        
//        //List to hold Invoice Line records
//        List<PricebookEntry> pbEntries = new List<PricebookEntry>();
//        //pbEntries.add(new PricebookEntry(PriceBook2Id = pricebookId, Product2Id = products[0].Id, UnitPrice = 100, IsActive = true, UseStandardPrice = false));
//        pbEntries.add(new PricebookEntry(PriceBook2Id = pricebookId, Product2Id = products[1].Id, UnitPrice = 100, IsActive = true, UseStandardPrice = false));
//        pbEntries.add(new PricebookEntry(PriceBook2Id = pricebookId, Product2Id = products[2].Id, UnitPrice = 100, IsActive = true, UseStandardPrice = false));
//        pbEntries.add(new PricebookEntry(PriceBook2Id = pricebookId, Product2Id = products[3].Id, UnitPrice = 100, IsActive = true, UseStandardPrice = false));
//        
//        //insert product here
//        insert pbEntries;
//        
//        //List to hold Invoice records
//        List<Invoice__c> invoices = new List<Invoice__c>();
//        invoices.add(new Invoice__c(Work_Order__c = workOrders[0].Id, RecordTypeId = recordTypes[2].Id));
//        invoices.add(new Invoice__c(Work_Order__c = workOrders[0].Id, RecordTypeId = recordTypes[3].Id));
//        
//        //insert invoices here
//        insert invoices;
//        
//        List<RecordType> lineRecordTypes = [SELECT Id FROM RecordType WHERE (DeveloperName = 'Receivable' OR DeveloperName = 'Payable') 
//        										AND SobjectType = 'Invoice_Line__c' ORDER BY DeveloperName];
//		
//		System.assertEquals(2, lineRecordTypes.size());
//		
//        //List to hold Invoice Line records
//        List<Invoice_Line__c> invoiceLines = new List<Invoice_Line__c>();
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[0].Id, Invoice__c = invoices[0].Id, Product__c = products[0].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[0].Id, Invoice__c = invoices[0].Id, Product__c = products[0].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[0].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[0].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[0].Id, Invoice__c = invoices[0].Id, Product__c = products[1].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[1].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[2].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[2].Id, Quantity__c = 1, Unit_Price__c = 500));
//        
//        //insert line items of invoices
//        insert invoiceLines;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Instance for Case standard controller
//        ApexPages.StandardController sc = new ApexPages.StandardController(workOrders[0]);
//        
//        //Instance of controller InvoicingController
//        InvoicingController controller = new InvoicingController(sc);
//        
//        //call controller's method to load invoice line records
//        List<InvoiceLineWrapper> listLineWrap = InvoicingController.loadLines(invoices[0].Id);
//        
//        //assert statements
//        System.assert(listLineWrap.size() == 3);
//        
//        //call controller's method to load PriceBooks records
//        List<InvoicingUtilities.iuOption> pbOptions = InvoicingController.getPriceBooks(workOrders[0].Id);
//        
//        //assert statements
//        //System.assertEquals(4, pbOptions.size());
//        System.assertEquals(1, pbOptions.size());
//        
//        //call controller's method to load PriceBooks records
//        List<InvoicingUtilities.iuOption> pbOptions1 = InvoicingController.getARPriceBooks(workOrders[0].Id);
//        List<InvoicingUtilities.iuOption> pbOptions2 = InvoicingController.getAPPriceBooks(workOrders[0].Id);
//        
//        
//        List<InvoicingUtilities.iuOption> productCategories = InvoicingController.getProductCategories(pricebookId);
//        
//        //assert statements
//        System.assertEquals(5, productCategories.size());
//        
//        //Test stops here
//        Test.stopTest();
//    }
}