/**
 *  Purpose         :   This Batch is used for OrderSelect fee Case generation & reassginment Scheduled process. 
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   07/09/2014
 *
 *	Current Version	:	V1.0
 *
 *	Revision Log	:	V1.0 - Created - Padmesh Soni(07/09/2014) - Order Fee Generation Scheduled process
 **/
global class Batch_OrderSelectFeeCaseGeneration implements Database.Batchable<SObject>, Database.Stateful {
	
	//Query result of OS queues
    global List<Group> caseQueues;
    
    global Batch_OrderSelectFeeCaseGeneration() {
    	
    	//Case Queue query result
    	caseQueues = [SELECT Id, Name FROM Group WHERE Name =: VSOAM_Constants.CASE_QUEUE_NAME_OS AND Type =: VSOAM_Constants.GROUP_TYPE_QUEUE];
    }
    
	//Start method
    global Database.Querylocator start(Database.BatchableContext BC) {
    	
    	//return the query result of Case records which are having Risk is greater than Zero
    	return Database.getQueryLocator([SELECT OwnerId,Geocode_Cache__c, CaseNumber, Street_Add_Concat__c, Risk__c, Risk_Type__c, Date_Serviced__c, Due_Date__c, Client_Name__c
    										FROM Case WHERE Risk__c < 0]);	
    }
    
    //Execute method
    global void execute(Database.BatchableContext BC, List<Case> orignatingOrders) {
    	
    	//List to hold Case records to be inserted
    	List<Case> newOrdersToUpsert = new List<Case>();
    	
    	//Loop through Context result from start method
    	for(Case originatingOrder : orignatingOrders) {
    		
    		//Create a new instance of Case record
    		Case newOrder = new Case(   Client_Name__c = originatingOrder.Client_Name__c,
    		                            Description = 'Work Order #:'+ originatingOrder.CaseNumber + ' -- Address:' + originatingOrder.Street_Add_Concat__c,
    									Vendor_Payout_Standard__c = originatingOrder.Risk__c, Work_Ordered__c = originatingOrder.Risk_Type__c,
    									Approval_Status__c = 'Approved', Street_Address__c = '13950 N. Northsight Blvd.', City__c = 'Scottsdale',
    									State__c = 'AZ', Zip_Code__c = '85260', Release_Payment__c = true, Client__c = 'FOLLOW', Vendor_Code__c = 'FOLLOW', AccountId = '0014000000RajWk', RecordTypeId = '01240000000UPMo',
    									Geocode_Cache__c = originatingOrder.Geocode_Cache__c,
    									Pre_Pending_Date_Time__c = DateTime.newInstanceGmt(2014, 1, 1, 12, 0, 0), Work_Completed__c = 'Yes', 
    									Date_Serviced__c = Date.today(), Due_Date__c = Date.today(), OwnerId = originatingOrder.OwnerId);
    		
    		//Check for Risk Type is 'Reassignment Fee'
    		if(originatingOrder.Risk_Type__c == 'Reassignment Fee') {
    			
    			//assign 'OS' Group/Queue Id as OwnerId
    			newOrdersToUpsert.add(new Case(Id = originatingOrder.Id, OwnerId = caseQueues[0].Id));
    		}
    		
    		//populate newOrdersToInsert list
    		newOrdersToUpsert.add(newOrder);
    	}
    	
    	//Check for size and insert it
    	if(newOrdersToUpsert.size() > 0)
    		upsert newOrdersToUpsert;
    }
    
    //Finish method
    global void finish(Database.BatchableContext BC) {
    	
    }	
}