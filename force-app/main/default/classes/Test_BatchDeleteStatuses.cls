@isTest(seeAllData=false)
private class Test_BatchDeleteStatuses {
//
///**
// *  Purpose         :   This is used for testing and covering BatchDeleteStatuses class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/10/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Padmesh Soni(06/10/2014) - Tasks 06/09/2014
// *	
// *	Coverage		:	87%
// **/
// 	
//    //test method added to check the functionality with code coverage
//    static testMethod void myUnitTest() {
//        
//        //Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client') AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(2, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accounts;
//		
//		Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//		insert property;
//		
//		
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(Status = 'Closed', Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today(),Client_Name__c = account.Id));
//    	workOrders.add(new Case(Status = 'Closed', Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1),Client_Name__c = account.Id));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//        //List to hold test records of Status__c
//        List<Status__c> statusRecords = new List<Status__c>();
//        
//        //Loop through till count 10
//        for(integer i=0; i < 10; i++) {
//        	
//        	//add new instance of Status__c into list
//        	statusRecords.add(new Status__c(Case__c = workOrders[0].Id, Delay_Reason__c = 'Assigned Late', Order_Status__c = 'On Route',
//        										Expected_Upload_Date__c = Date.today().addDays(i)));
//        	
//        	//add new instance of Status__c into list
//        	statusRecords.add(new Status__c(Case__c = workOrders[1].Id, Delay_Reason__c = 'Vehicle Breakdown', Order_Status__c = 'To Be Updated (Completed)',
//        										Expected_Upload_Date__c = Date.today().addDays(i - 1)));
//        }
//        
//        //insert VWM Surveys here
//        insert statusRecords;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Execute batch here
//        Database.executeBatch(new BatchDeleteStatuses(), 200);
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //Query result of Status__c records
//        statusRecords = [SELECT Id FROM Status__c];
//        
//        //Assert statement here
//        System.assertEquals(0, statusRecords.size());
//    }
}