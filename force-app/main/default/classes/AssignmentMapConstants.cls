/**
 *  Purpose         :   This class is to hold the constant values for Work Order Assigment Map functionality.
 *
 *  Create By       :   Padmesh Soni
 *
 *  Created Date    :   12/11/2013
 *
 *  Current Version :   V1.2
 *
 *	Revision Log	:	V1.0 - Created
 *						V1.1 - Modified - Padmesh Soni (05/08/2014) - 140503 OAM Date Widgets (1) - Enhancement of functionality on OAM project.
 *											Changes are:
 *												1. OAM-40: Better Date Filter UI (Full specifications on all date filters.)
 *						V1.2 - Modified - Padmesh Soni (09/17/2014) - OAM-78: Add  the ability to be able to plot all vendors on the map not just 
 *											 the ones with assigned work orders.
 *											 Changes are: New method "getNewVendorsToPlotOnMap" added.
 *                      V1.3 - Modified - Don Kotter (03/26/2015) - OAM-90 : Add Vendor Support Team Filter. 
 **/
public with sharing class AssignmentMapConstants {
	
	//Org wide boolean flag constants
	public static final boolean BOOL_ON = true;
    public static final boolean BOOL_OFF = false;
    
	//Constants related to Account Object
	public static final String ACCOUNT_SOBJECTTYPE = 'Account';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER = 'Customer';
    
    //Constants for URL parameters 
    public static final String URL_PARAM_MINLAT = 'minLat';
    public static final String URL_PARAM_MINLON = 'minLon';
    public static final String URL_PARAM_MAXLAT = 'maxLat';
    public static final String URL_PARAM_MAXLON = 'maxLon';
    public static final String URL_PARAM_TOGGLESTATE = 'toggleState';
    public static final String URL_PARAM_STATEPARAM = 'stateParam';
    public static final String URL_PARAM_FILTERCLIENTS = 'filterClients';
    public static final String URL_PARAM_FILTERASSIGNPROG = 'filterAssignProg';
    public static final String URL_PARAM_WORKORDERTYPES = 'workOrderTypes';
    public static final String URL_PARAM_WRKORDEREDTYPE = 'wrkOrderedType';
    public static final String URL_PARAM_DEPARTMENT = 'department';
    public static final String URL_PARAM_STOP_COLOR = 'stopColor';
    public static final String URL_PARAM_DUESTARTDATE = 'dueStartDate';
    public static final String URL_PARAM_DUESTOPDATE = 'dueStopDate';
    public static final String URL_PARAM_SCHEDULESTARTDATE = 'scheduleStartDate';
    public static final String URL_PARAM_SCHEDULESTOPDATE = 'scheduleStopDate';
    public static final String URL_PARAM_ASSIGNEDSTARTDATE = 'assignedStartDate';
    public static final String URL_PARAM_ASSIGNEDSTOPDATE = 'assignedStopDate';
    public static final String URL_PARAM_CREATEDSTARTDATE = 'createdStartDate';
    public static final String URL_PARAM_CREATEDSTOPDATE = 'createdStopDate';
    public static final String URL_PARAM_VENDORSUPPORTTEAM = 'vendorSupportTeam';
    
    //Constants related to Case Object
    public static final String CASE_QUEUE_NAME_UA = 'UA';
    public static final String CASE_SOBJECTTYPE = 'Case';
    public static final String CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER = 'NEW_ORDER';
    public static final String CASE_STATUS_OPEN = 'Open';
    public static final String CASE_CLIENT_SG = 'SG';
    public static final String CASE_CLIENT_NOT_SG = 'NOT-SG';
    public static final String CASE_WORK_ORDER_TYPE_ROUTINE = 'Routine';
    public static final String CASE_WORK_ORDER_TYPE_MAINTENANCE = 'Maintenance';
    public static final String CASE_APPROVAL_STATUS_REJECTED = 'Rejected';
    public static final String CASE_QUEUE_NAME_OS = 'OS';
    public static final String CASE_ROUTING_SERVICEABLE_YES = 'Yes';
    public static final String CASE_STATE_TX = 'TX';
    public static final String CASE_STATE_LA = 'LA';
    public static final String CASE_STATE_AR = 'AR';
    
    //Sobject type name of User object
    public static final String SOBJECT_NAME_USER = 'User';
    
    //OAM-48: Self Assigned filter - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes
    //New Url parameter String initialized
	public static final String URL_PARAM_FILTERSELFASSIGN = 'filterSelfAssign';
	
	//OAM-40: Better Date Filter UI - Padmesh Soni (05/08/2014) - 140503 OAM Date Widgets (1)
	//New constants added
	public static final String LATE = 'late';
    public static final String TODAYS = 'today';
    public static final String TODAYLATE = 'todayLate';
    public static final String CURRENTSRVC = 'CurrentSrvc';
    public static final String LAST2DAYS = 'last2Days';
    
    //Code added - Padmesh Soni (09/25/2014) - OAM-78: Add  the ability to be able to plot all vendors on the map
    //Variable to hold Profile's user license of string 
    //of which users should be display on map 
 	public static final List<String> USER_LICENSE_OF_VENDORS_ON_MAP = new List<String> {'Customer Portal Manager Standard',
		    																			 'Partner Community Login',
		    																			 'Partner Community'};
}