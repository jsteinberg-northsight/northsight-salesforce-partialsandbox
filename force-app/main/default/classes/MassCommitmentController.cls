public without sharing class MassCommitmentController {
	/******************************************************************************************/
	//error vars
	public string status_error{get;set;}
	public string status_error_fields{get;set;}
	/******************************************************************************************/
	// The list of work orders selected by the user
	public transient WorkOrderWrapper[] work_orders {get; private set;}
	/******************************************************************************************/
	//pagination vars
	public integer counter;  //keeps track of the offset
	public integer list_size; //sets the page size or number of rows
	public integer total_size; //used to show user the total size of the list
	/******************************************************************************************/
	//client commitment var
	public Client_Commitment__c tmpClientCommitment {get; set;}
	/******************************************************************************************/
	//sort vars
	public string sort_order;
	public string sort_direction {get; private set;}
	public string sort_field {get; set;}
	public string last_sort_field;
	public string orderByClause;
	/******************************************************************************************/
	//dynamic query vars
	public string queryOption { get; set; } 
	public string stateQueryOption { get; set; } 
	public Date dueDateQueryOptionBegin { get; set; } 
	public Date dueDateQueryOptionEnd { get; set; }
	public Date commitDateQueryOptionBegin { get; set; }
	public Date commitDateQueryOptionEnd { get; set; }
	/******************************************************************************************/
	//static set vars
	static Set<string> ApprovalStatusSet;	
	static Set<string> OwnerNameSet;
	static {
		ApprovalStatusSet = new Set<string>{'Not Started','Rejected'};
		OwnerNameSet = new Set<string>{'CANCELED','REO Unassigned'};
	}
	
	//Padmesh Soni (06/23/2014) - Support Case Record Type
	//Variable to hold list of RecordTypes of Case object
	List<RecordType> recordTypes;
	String idInClause;
	
	public Case tempCase {get;set;}
	/*public List<SelectOption> statePLValues {get; private set;}
	public static List<SelectOption> getStatePLValues(){
		//List<SelectOption> statePLValues = new List<SelectOption>();
		
		Schema.Describefieldresult dfr = Case.State__c.getDescribe();
		
		for(Schema.Picklistentry ple : dfr.getPicklistValues()){
			statePLValues.add(new SelectOption(ple.getValue(), ple.getLabel(), !ple.isActive()));
		}
		
		return statePLValues;
	}*/
	/*******************************************************************************************************************************************************************************/
	public MassCommitmentController(){
		counter = 0;
		list_size = 20;
		total_size = 0;
		status_error_fields = '{}';
		tmpClientCommitment = new Client_Commitment__c();
		sort_order = 'Up'; // not used until the first sort
		orderByClause = ' ORDER BY Scheduled_Date__c';
		tempCase = new Case();
		
		//Padmesh Soni (06/23/2014) - Support Case Record Type
		//Query record of Record Type of Case object
		recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
		
		//Padmesh Soni (06/23/2014) - Support Case Record Type
		//initialize string
		idInClause = '(\'';
		
		//Padmesh Soni (06/23/2014) - Support Case Record Type
		//Loop through RecordTypes query result
		for (RecordType recordType : recordTypes){
		
			//populate Ids in clause variable
		    idInClause += recordType.Id + '\',\'';
		}
		
		//Padmesh Soni (06/23/2014) - Support Case Record Type
		//Format string with removing last comma
		idInClause  = idInClause.substring(0,idInClause.length()-2);
		idInClause += ')';
	}
	
	/*******************************************************************************************************************************************************************************/
	public void loadQuery() {//function that will be used to load the queries
		try{
			// Get relevant work orders
			transient case[] caseList;
			caseList = buildDynamicQuery(tempCase.State__c, dueDateQueryOptionBegin, dueDateQueryOptionEnd, commitDateQueryOptionBegin, commitDateQueryOptionEnd);
			system.assertNotEquals(null, caseList);			
			// Initialize wrapper list
			work_orders = new WorkOrderWrapper[] {};
			// Populate the work order class list	
			//  Also build a JSON string to make address more easily available to JS		 
			for(case c : caseList) {
				if(c.Assigned_Date_Time__c != null){
					/*
					datetime l = c.Assigned_Date_Time__c;
					Date d = l.dateGmt();
	        		Time t = l.timeGmt();        		
	         		c.Assigned_Date_Time__c = Datetime.newInstance(d,t);
					*/
					//this is here to display the correct date on the page,
					TimeZone tz = UserInfo.getTimeZone();
					DateTime localTime = c.Assigned_Date_Time__c.AddSeconds(tz.getOffset(c.Assigned_Date_Time__c)/1000);
					c.Assigned_Date_Time__c = localtime;
				}
				work_orders.add(new WorkOrderWrapper(c));
			}
			system.assertNotEquals(null, work_orders);
		} catch(Exception e) {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.error, e.getMessage()+'*****'+e.getStackTraceString()));
		}
	}
	/*******************************************************************************************************************************************************************************/
	public PageReference createCommits() {//function used to create client commitments
			try{
				loadQuery();//reload the query
				Set<Id>orderIdSet = new Set<Id>();//create a set for holding work order IDs
				for (WorkOrderWrapper currentOrder : work_orders){//loop thru list of work orders in work_orders
					if (currentOrder.selected){//if the current order is selected
						orderIdSet.add(currentOrder.wo.Id); //add the current order id to orderIdSet
					}	
				}
				//create a list for holding client commitment objects to be created
				List<Client_Commitment__c> clientCommitmentsToInsert = new List<Client_Commitment__c>();
				if (orderIdSet.size() > 0){//if there are order IDs
					for (Id currentOrderId : orderIdSet){//loop thru the orderIdSet
						Client_Commitment__c newClientCommitment = new Client_Commitment__c(//create a new client commitment object
							Commitment_Date__c = tmpClientCommitment.Commitment_Date__c,
							Current_Status_of_Order__c = tmpClientCommitment.Current_Status_of_Order__c,
							Root_Cause_of_Delay__c = tmpClientCommitment.Root_Cause_of_Delay__c,
							Public_Comments__c = tmpClientCommitment.Public_Comments__c,
							Internal_Comments__c = tmpClientCommitment.Internal_Comments__c,
							Work_Order__c = currentOrderId
						);
						clientCommitmentsToInsert.add(newClientCommitment);//add the new client commitment to the clientCommitmentsToInsert list
					}
				}
				//do an insert of the values in clientCommitmentsToInsert list and catch the save results in a list called srList
				Database.SaveResult[] srList = database.insert(clientCommitmentsToInsert,false);
					status_error = '';//instantiate status_error
					set<string> errorSet = new set<string>();//create a string set named errorSet for adding error messages to
					set<string> fieldSet = new set<string>();//create a string set named fieldSet for adding the fields errors occurred on
				for(Database.Saveresult sr : srList){//loop thru the save results in srList
					if(!sr.isSuccess()){//if the save result was not successful
						for(Database.Error e : sr.getErrors()){//loop thru the save result errors
							string m = e.getMessage();//add the error message to a string named m
							fieldSet.addAll(e.getFields());//add the fields to fieldSet
							errorSet.add(m);//add the m error message var to errorSet
						}
					}
				}
				status_error_fields = Json.serializePretty(fieldSet);//add the fieldSet to status_error_fields
				status_error = string.join(new List<string>(errorSet),'\r\n');//add the errorSet to a new list joined with other lists of errorSets to status_error
				tmpClientCommitment = new Client_Commitment__c();//instantiate tmpClientCommitment as a new client commitment
				//loadQuery();//reload the query
			} catch(Exception e) {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.error, e.getMessage()+'*****'+e.getStackTraceString()));
			}
			return null;
	}
	/*******************************************************************************************************************************************************************************/
	public PageReference WOSort() {//function used to sort the orders table
		try{
			if(sort_field == last_sort_field){//If the same field is being sorted again, reverse direction
				if(sort_direction=='Up') {//if the sort direction was Up
					sort_direction = 'Down';//make the sort direction Down
				} 
				else {//if the sort direction was Down
					sort_direction = 'Up';//make the sort direction Up
				}
			} 
			else {//if the same field is not being sorted again
				sort_direction = 'Up';//set the sort direction to Up
			}
			WorkOrderWrapper.sortField = sort_field;//set the WorkOrderWrapper.sortField equal to that of the sort_field var
			WorkOrderWrapper.sortDirection = sort_direction;//set the WorkOrderWrapper.sortDirection equal to that of the sort_direction var
			orderByClause = ' ORDER BY ' + sort_field;
			if(sort_direction == 'Down'){
				orderByClause += ' desc';
			}
			loadQuery();//load the query
			/*if(work_orders!=null){//if we have orders in the work_orders list
				work_orders.sort();//sort the work_orders list
			}*/
			last_sort_field = sort_field;//set last_sort_field equal to the sort_field var
		} catch(Exception e) {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.error, e.getMessage()+'*****'+e.getStackTraceString()));
		}
		return null;
	}
	/*******************************************************************************************************************************************************************************/
	/*private string format_date(datetime d) {//function for formatting datetime fields
		if(d==null) {//if the datetime is null
			return '';//return blank
		} else {//if we have a datetime
			return d.month() + '/' + d.day() + '/' + d.year();//return the formatted date
		}
	}*/
	/*******************************************************************************************************************************************************************************/
	//*****************************************************************************************************************************
	//****************************************QUERY BUILDING***********************************************************************
	//*****************************************************************************************************************************
	private SObject[] buildDynamicQuery(String state, Date dueDateBegin, Date dueDateEnd, Date commitDateBegin, Date commitDateEnd){//function for building queries dynamically
 		SObject[] result = new SObject[]{};//the result sObject list will hold the query results
 		//define our base query
 		string selectStatement = 'select Id, OwnerId, Zip_Code__c, Effective_Commit_By_Date__c, Last_Order_Status__c , Contact_Name__c, Customer_Name__c, Approval_Status__c, Scheduled_Date__c, Work_Ordered__c, Work_Ordered_Text__c, Street_Address__c, City__c, Due_Date__c, Assigned_Date_Time__c, Status_Health_Text__c, Status_Health__c, Vendor_Status__c, Status, State__c, notes__c, Description, Contact.Name, Last_Expected_Upload_Date__c, Client_Number__c, Commit_By_Date__c, CaseNumber, Stop_Color__c, Routing_Serviceable__c, Routing_Priority__c, Display_Street__c, Display_City__c, Display_State__c, Display_Zip_Code__c, Client_Commit_Date__c, Client_Commit_Count__c from Case ';
		string selectIdCountStatement = 'select count(Id) caseCount from Case ';//used to get the count of results in a separate query using the same clauses except no limits/offset
 		
 		//WHERE Clauses
 		//the base where clause
 			//all work order where 
 				//last open verfication is today or yesterday
 				//client is always SG
 				//approval status = Not Started OR Rejected
				//and client commit date value = False
 		//Padmesh Soni (06/23/2014) - Support Case Record Type
    	//New filter criteria added for RecordTypeId
		string mainWhereClause = 'WHERE RecordTypeId NOT IN '+ idInClause +' AND Approval_Status__c IN: ApprovalStatusSet AND Id NOT IN (SELECT Work_Order__c FROM Client_Commitment__c WHERE Sync_Status__c = \'In Progress\' OR Sync_Status__c = \'Unsent\') AND (Last_Open_Verification__c = TODAY OR Last_Open_Verification__c = YESTERDAY) AND Client__c =\'SG\' AND isClosed = false AND Status != \'Closed\'';
 		/*******************************************************************************************************************************************************************************/
 		/*
 		this section instantiates the variables that are used for creating the other where clause filters
 			by state,
 			by the due date begin/end
 			by the commit by begin/end date
 		*/
 		string stateClause = '';
 		string dueDateBeginClause = '';
 		string dueDateEndClause = '';
 		string commitByBeginClause = '';
 		string commitByEndClause = '';
 		/*******************************************************************************************************************************************************************************/
 		string limitAndOffset = ' LIMIT :list_size OFFSET :counter';//this is the limit clause
 		/*******************************************************************************************************************************************************************************/
 		//Checking the filter field values to see if we need to fill the other where clause filter variables
 		/*
 		state check
 		*/
 		if(state != null && state != '--None--'){
 			stateClause = ' and State__c = \'' + state + '\'' ;
 		}
 		/*
 		due dates check
 		*/
 		if(dueDateBegin != null){
 			//Put the due date into a SOQL readable format.
			dueDateBeginClause = ' and Due_Date__c >= ' + normalizeDateForSOQL(dueDateBegin);
 			
 		}
 		if(dueDateEnd != null){
 			//Put the due date into a SOQL readable format. 
			dueDateEndClause = ' and Due_Date__c <= ' + normalizeDateForSOQL(dueDateEnd);
 		}
 		/*
 		commit by dates check
 		*/
 		if(commitDateBegin != null){
 			//Put the commit by date into a SOQL readable format. 			
 			commitByBeginClause = ' and Effective_Commit_By_Date__c >= ' + normalizeDateForSOQL(commitDateBegin);
 		}
 		if(commitDateEnd != null){
 			//Put the commit by date into a SOQL readable format.
 			commitByEndClause = ' and Effective_Commit_By_Date__c <= ' + normalizeDateForSOQL(commitDateEnd);
 		}
 		/*******************************************************************************************************************************************************************************/
 		//system.assert(false, selectStatement + stateClause + dueDateClause + commitByClause + orderByAndLimitClause );
 		//String myString = selectStatement + mainWhereClause + stateClause + dueDateEndClause + dueDateBeginClause + commitByEndClause + commitByBeginClause + orderByAndLimitClause;
		//ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,myString);
		//ApexPages.addMessage(myMsg);
		AggregateResult[] caseList;//create a list to hold results of a query with no limits or offsets
		caseList = Database.query(selectIdCountStatement + mainWhereClause + stateClause + dueDateEndClause + dueDateBeginClause + commitByEndClause + commitByBeginClause);
		if(caseList != null ){//get the size of the result set
			for(AggregateResult r : caseList){
				total_size = (integer)r.get('caseCount');	
			}
		} else {
			total_size = 0;
		}
		//The reason the majority of the query is commented out is because I wanted to work with a larger data set, it is safe to un-comment the code. 
 		return Database.query(selectStatement + mainWhereClause + stateClause + dueDateEndClause + dueDateBeginClause + commitByEndClause + commitByBeginClause + orderByClause + limitAndOffset);
 	}
	//*****************************************************************************************************************************
	//****************************************END QUERY BUILDING***********************************************************************
	//*****************************************************************************************************************************	
	/*******************************************************************************************************************************************************************************/
	private string normalizeDateForSOQL(date myDate){//function for normalizing dates for SOQL queries
		Time tm = Time.newInstance(0,0,0,0);
 		Datetime myDateGMT = DateTime.newInstanceGMT(mydate, tm);
 		string myDateFormat = myDateGMT.formatGMT('yyyy-MM-dd');
		return myDateFormat;
	}
	/*******************************************************************************************************************************************************************************/
	/*Pagination section for the page buttons*/
	public PageReference Beginning() { //user clicked beginning
		counter = 0;
		loadQuery();
		return null;
	}
	public PageReference Previous() { //user clicked previous button
		counter = counter - list_size;
		loadQuery();
		return null;
	}
	public PageReference Next() { //user clicked next button
		counter = counter + list_size;
		loadQuery();
		return null;
	}
	public PageReference End() { //user clicked end
		integer tmpMod = math.mod(total_size, list_size);
		if(tmpMod > 0){
			counter = total_size - tmpMod;
		} else {
			counter = total_size - list_size;
		}
		loadQuery();
		return null;
	}
	public Boolean getDisablePrevious(){//this will disable the previous and beginning buttons
		if(counter > 0){
			 return false;
		} else {
			return true;
		}
	}
	public Boolean getDisableNext(){//this will disable the next and end buttons
		if(counter + list_size < total_size){ 
			return false;
		} else {
			return true;
		}
	}
	public Integer getPageNumber(){//returns the current page number
		if(total_size != null && total_size != 0){
			return counter/list_size + 1;
		} else {
			return 0;
		}
	}
	public Integer getTotalPages(){//returns the total number of pages
		if(math.mod(total_size, list_size) > 0){
			return total_size/list_size + 1;
		} else {
			return (total_size/list_size);
		}
	}
	
		public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}