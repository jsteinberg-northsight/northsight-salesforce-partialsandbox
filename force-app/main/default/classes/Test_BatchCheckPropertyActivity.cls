@isTest(seeAllData=false)
private class Test_BatchCheckPropertyActivity {
///**
// *  Purpose         :   This is used for testing and covering BatchCheckPropertyActivity class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/06/2014
// *
// *	Current Version	:	V1.1
// *
// *	Revision Log	:	V1.0 - Created - Required Maintenance: Unit Test Improvements
// *						V1.1 - Modified - Padmesh Soni (06/19/2014) - Support Case Record Type
// *	
// *	Coverage		:	100% - V1.0
// *						100% - V1.1
// **/
// 	
// 	//test method added to check the functionality with code coverage
//    static testMethod void myUnitTest() {
//        
//        //Query record of Record Type of Account and Case objects
//    	//Padmesh Soni (06/19/2014) - Support Case Record Type
//		//Query one more record type of Case Object "SUPPORT"    
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client'
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(3, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accountsOnOrders = new List<Account>();
//    	accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accountsOnOrders;
//		
//		//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX',
//    								Department__c = 'Test', Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual', 
//    								Updated_Date_Time__c = DateTime.now()));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX', 
//    								Updated_Date_Time__c = DateTime.now().addHours(2)));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'LA',
//    								Department__c = 'Test', Assignment_Program__c = 'Mannual', Updated_Date_Time__c = DateTime.now(), Vendor_Code__c = 'FOLLOW'));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'AR',
//    								Department__c = 'Test1', Work_Ordered__c = 'Grass Initial Cut', Assignment_Program__c = 'OrderSelect', 
//    								Updated_Date_Time__c = DateTime.now().addHours(5), Vendor_Code__c = 'FOLLOW'));
//    	
//    	//Padmesh Soni (06/19/2014) - Support Case Record Type
//    	//New record created for Customer support recordtypes
//		workOrders.add(new Case(RecordTypeId = recordTypes[2].Id, AccountId = accountsOnOrders[1].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'AZ',
//    								Department__c = 'Test1', Work_Ordered__c = 'Grass Initial Cut', Assignment_Program__c = 'OrderSelect', 
//    								Updated_Date_Time__c = DateTime.now().addHours(5), Vendor_Code__c = 'FOLLOW'));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//    	//Qurey result of Property
//    	List<Geocode_Cache__c> properties = [SELECT Name, Active__c FROM Geocode_Cache__c WHERE State__c IN ('TX', 'AZ')];
//    	properties[0].Active__c = false;
//    	properties[1].Active__c = false;
//    	update properties;
//    	
//    	//Test starts here
//    	Test.startTest();
//    	
//    	//Execute the batch
//    	Database.executeBatch(new BatchCheckPropertyActivity(), 200);
//    	
//    	//Test stops here
//    	Test.stopTest();
//    	
//    	//Qurey result of Property
//    	properties = [SELECT Name, Active__c FROM Geocode_Cache__c ORDER BY Active__c];
//    	
//    	//Assert statement
//    	//Padmesh Soni (06/19/2014) - Support Case Record Type
//    	//assert statement updated from 3 to 4
//		System.assertEquals(4, properties.size());
//    	System.assertEquals(false, properties[0].Active__c);
//    	System.assertEquals(false, properties[1].Active__c);
//    	System.assertEquals(false, properties[2].Active__c);
//    	System.assertEquals(true, properties[3].Active__c);
//    }
}