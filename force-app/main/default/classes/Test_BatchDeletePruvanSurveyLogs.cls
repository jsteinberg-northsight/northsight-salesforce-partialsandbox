@isTest(seeAllData=false)
private class Test_BatchDeletePruvanSurveyLogs {
///**
// *  Purpose         :   This is used for testing and covering BatchDeleteSurveyResponseLogs. 
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   05/02/2014
// *
// *	Current Version	:	V1.0 - Padmesh Soni (05/02/2014) - Required Maintenance: Unit Test Improvements
// *
// *	Coverage		:	85%
// **/
// 	
// 	//Test method to test the functionality of Batch
//    static testMethod void testBatchDeleteSurveyResponseLogs() {
//        
//        //List to hold Pruvan GetWorkOrderResponse Log records
//        List<Pruvan_SurveyValidate_Log__c> pruvanSurveyValidLogs = new List<Pruvan_SurveyValidate_Log__c>();
//        pruvanSurveyValidLogs.add(new Pruvan_SurveyValidate_Log__c(Name = 'Test Log 1', Log_Data__c = 'Testin is going on for Deletion of first record.'));
//        pruvanSurveyValidLogs.add(new Pruvan_SurveyValidate_Log__c(Name = 'Test Log 2', Log_Data__c = 'Testin is going on for Deletion of Second record.'));
//        
//        //insert Pruvan Survay Response logs
//        insert pruvanSurveyValidLogs;
//        
//        //Batch instance
//        BatchDeletePruvanSurveyLogs b = new BatchDeletePruvanSurveyLogs();
//		
//        //Test starts here
//        Test.startTest();
//        
//        //Execute batch here
//		Database.executeBatch(b, 200);
//		
//        //Test stops here
//        Test.stopTest();
//        
//        //Query result of Pruvan_SurveyResponse_Log__c object
//        pruvanSurveyValidLogs = [SELECT Id FROM Pruvan_SurveyValidate_Log__c WHERE Id IN: pruvanSurveyValidLogs];
//        
//        //assert statement here
//        System.assertEquals(0, pruvanSurveyValidLogs.size());
//        
//    }
}