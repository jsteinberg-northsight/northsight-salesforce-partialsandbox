/**
 *	Description		:	This is controller for VF_ControlSchedulers visualforce page. 
 *
 *	Created By		:	Padmesh Soni
 *
 *	Created Date	:	06/13/2014
 *
 *	Current Version	:	V1.0
 *
 *	Revisiion Logs	:	V1.0 - Created
 **/
public with sharing class SchedulerPauseRestartController {
	
	//Class properties
	public List<ScheduledWrapper> scheduleWrapList {get; set;}
	
	//Constructor definition
	public SchedulerPauseRestartController() {
		
		//initialize ScheduledWrapper list
		scheduleWrapList = new List<ScheduledWrapper>();
	}
	
	/**
	 *	@description	:	This method is to get all existing scheduled jobs which are scheduled already in Org. Format it as list into Wrapper and 
	 *						show those result on page.
	 *
	 *	@param			:
	 *
	 *	@return			:	void
	 **/
	public void getScheduledJobs() {
		
		//List to hold query result of Scheduled_Job__c records
		Map<String, Scheduled_Job__c> mapScheduledJob = new Map<String, Scheduled_Job__c>();
		
		//Loop through query result of Scheduled Job
		for(Scheduled_Job__c scheduleJob : [SELECT Id, Name, Job_Name__c, Cron_Expression__c, JobId__c, Schedulable_Class__c, Status__c FROM Scheduled_Job__c]) {
			
			mapScheduledJob.put(scheduleJob.Job_Name__c, scheduleJob);
		}
		
		//List to hold Scheduled_Job__c records to be updated
		List<Scheduled_Job__c> scheduledJobToUpdate = new List<Scheduled_Job__c>();
		
		//List to hold query result of CronTrigger records
		map<string,CronTrigger> cronMap = new map<string,CronTrigger> (); 
		for(CronTrigger cronTrigger : [Select State, StartTime, OwnerId, Id, CronJobDetailId, CronExpression, CronJobDetail.JobType, 
												CronJobDetail.Name From CronTrigger WHERE CronJobDetail.JobType = '7' AND State = 'WAITING']) {
			cronMap.put(cronTrigger.CronJobDetail.Name,cronTrigger);
		}
		
													
			//Check if JobName is exist in 	mapScheduledJob as key
			for(string k : mapScheduledJob.KeySet()) {
				Scheduled_Job__c sched = mapScheduledJob.get(k);
				//Create a instance of ScheduledWrapper and add this to scheduleWrapList
				ScheduledWrapper schedWrap = new ScheduledWrapper(); 
				schedWrap.scheduledJob = sched;
				schedWrap.isScheduled = false;
				scheduleWrapList.add(schedWrap);
				
				//update the Scheduled Job fields
				if(cronMap.containsKey(k)){
					
					sched.Cron_Expression__c = cronMap.get(k).CronExpression;
					sched.JobId__c = cronMap.get(k).Id;
					sched.Status__c = 'Scheduled';
					scheduledJobToUpdate.add(sched);
				}else{
					//sched.Cron_Expression__c = cronMap.get(k).CronExpression;
					sched.JobId__c = Null;
					sched.Status__c = 'Stopped';
					scheduledJobToUpdate.add(sched);
				}
			}								
		
		 
		//Check for size
		if(scheduleWrapList.size() > 0) {
			if(scheduledJobToUpdate.size() > 0)
				update scheduledJobToUpdate;
		} else {
			
			//String to hold error message
			String errorMessage = 'No jobs scheduled at this moment. Try again later...';
			
			//add message on Page to show user friendly message
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, errorMessage));
		}
	}
	
	/**
	 *	@description	:	This method is to abort the Scheduled jobs which are selected by User on UI.
	 *
	 *	@param			:
	 *
	 *	@return			:	void
	 **/
	public void abortJobs() {
		
		List<Scheduled_Job__c> schedJobs = new List<Scheduled_Job__c>();
		
		//Loop through scheduleWrapList
		for(ScheduledWrapper scheduleWrap : scheduleWrapList) {
			
			//Check if user have selected Checkbox in UI
			if(scheduleWrap.isScheduled && scheduleWrap.scheduledJob.Status__c == 'Scheduled') {
				
				//abort the selected job
				System.abortJob(scheduleWrap.scheduledJob.JobId__c);
				scheduleWrap.scheduledJob.Status__c = 'Stopped';
				schedJobs.add(scheduleWrap.scheduledJob);
			}
		}
		
		//Check for size
		if(schedJobs.size() > 0)
			update schedJobs;
	}
	
	/**
	 *	@description	:	This method is to schedule the Scheduled jobs which are selected by User on UI.
	 *
	 *	@param			:
	 *
	 *	@return			:	void
	 **/
	public void doScheduleJobs() {
		
		List<Scheduled_Job__c> schedJobs = new List<Scheduled_Job__c>();
	
		//Loop through scheduleWrapList
		for(ScheduledWrapper scheduleWrap : scheduleWrapList) {
			
			//Check if user have selected Checkbox in UI
			if(scheduleWrap.isScheduled && scheduleWrap.scheduledJob.Status__c == 'Stopped') {
				
				//Create a type instance
				Type t = Type.forName(scheduleWrap.scheduledJob.Schedulable_Class__c);
				
				//Schedule the selected job
				System.schedule(scheduleWrap.scheduledJob.Job_Name__c, scheduleWrap.scheduledJob.Cron_Expression__c, (Schedulable)t.newInstance());
			}
		}
		
		//List to hold query result of Scheduled_Job__c records
		Map<String, Scheduled_Job__c> mapScheduledJob = new Map<String, Scheduled_Job__c>();
		
		//Loop through query result of Scheduled Job
		for(Scheduled_Job__c scheduleJob : [SELECT Id, Name, Job_Name__c, Cron_Expression__c, JobId__c, Schedulable_Class__c, Status__c FROM Scheduled_Job__c]) {
			
			//populate the map
			mapScheduledJob.put(scheduleJob.Job_Name__c, scheduleJob);
		}
		
		//List to hold query result of CronTrigger records
		for(CronTrigger cronTrigger : [Select State, StartTime, OwnerId, Id, CronJobDetailId, CronExpression, CronJobDetail.JobType, 
												CronJobDetail.Name From CronTrigger WHERE CronJobDetail.JobType = '7' AND State = 'WAITING']) {
													
			//Check if JobName is exist in 	mapScheduledJob as key
			if(mapScheduledJob.containsKey(cronTrigger.CronJobDetail.Name)) {
				
				//update the Scheduled Job fields
				mapScheduledJob.get(cronTrigger.CronJobDetail.Name).Cron_Expression__c = cronTrigger.CronExpression;
				mapScheduledJob.get(cronTrigger.CronJobDetail.Name).JobId__c = cronTrigger.Id;
				mapScheduledJob.get(cronTrigger.CronJobDetail.Name).Status__c = 'Scheduled';
			}
		}
		
		//Loop through scheduleWrapList
		for(ScheduledWrapper scheduleWrap : scheduleWrapList) {
			
			//Check if user have selected Checkbox in UI
			if(mapScheduledJob.containsKey(scheduleWrap.scheduledJob.Job_Name__c)) {
				
				//populate values on the instance of Scheduled Job record
				scheduleWrap.scheduledJob.JobId__c = mapScheduledJob.get(scheduleWrap.scheduledJob.Job_Name__c).JobId__c;
				scheduleWrap.scheduledJob.Status__c = mapScheduledJob.get(scheduleWrap.scheduledJob.Job_Name__c).Status__c;
				scheduleWrap.scheduledJob.Cron_Expression__c = mapScheduledJob.get(scheduleWrap.scheduledJob.Job_Name__c).Cron_Expression__c;
				schedJobs.add(scheduleWrap.scheduledJob);
			}
		}
				
		//Check for size
		if(schedJobs.size() > 0)
			update schedJobs;
	}
	
	//Wrapper class to hold a custom list to store some 
	//different values and use this list to view on page
	public class ScheduledWrapper {
		
		public Scheduled_Job__c scheduledJob {get; set;}
		public boolean isScheduled {get; set;}
	}
	
public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}	
}