@isTest
private class TestBatchCommitmentTimeout {
//    private static void generateTestCases() {
//        DateTime threeHours;
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new account
//        Account newAccount = new Account(
//            Name = 'Tester Account'
//        );
//        insert newAccount;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new contact
//        Contact newContact = new Contact(
//            LastName = 'Tester',
//            AccountId = newAccount.Id,
//            Pruvan_Username__c = 'NSBobTest',
//            MailingStreet = '30 Turnberry Dr',
//            MailingCity = 'La Place',
//            MailingState = 'LA',
//            MailingPostalCode = '70068',
//            OtherStreet = '30 Turnberry Dr',
//            OtherCity = 'La Place',
//            OtherState = 'LA',
//            OtherPostalCode = '70068',
//            Other_Address_Geocode__Latitude__s = 30.00,
//            Other_Address_Geocode__Longitude__s = -90.00
//        );
//        insert newContact;
//        Case newCase,newCase2,newCase3;
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //set threeHours = Datetime.now - 3hours
//        threeHours = Datetime.now();
//        threeHours = threeHours.addHours(-1);
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new test case
//        newCase = new Case();
//        newCase.street_address__c = '1234 Street';
//        newCase.state__c = 'TX';
//        newCase.city__c = 'Austin';
//        newCase.zip_code__c = '78704';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        newCase.Client__c = 'NOT-SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'Grass Recut';
//        newCase.Approval_Status__c = 'Not Started';
//        newCase.Due_Date__c = Date.today();
//        newCase.Client_Commit_Date__c = Date.today();
//        newCase.Last_Open_Verification_DateTime__c = threeHours;
//        
//        insert newCase;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new test case
//        newCase2 = new Case();
//        newCase2.street_address__c = '1234 Street';
//        newCase2.state__c = 'TX';
//        newCase2.city__c = 'Austin';
//        newCase2.zip_code__c = '78704';
//        newCase2.Vendor_code__c = 'SRSRSR';
//        newCase2.Work_Order_Type__c = 'Routine';
//        newCase2.Client__c = 'NOT-SG';
//        newCase2.Loan_Type__c = 'REO';
//        newCase2.ContactId = newContact.Id;
//        newCase2.Work_Ordered__c = 'Grass Recut';
//        newCase2.Approval_Status__c = 'Not Started';
//        newCase2.Due_Date__c = Date.today();
//        newCase2.Client_Commit_Date__c = Date.today();
//        newCase2.Last_Open_Verification_DateTime__c = threeHours;
//        
//        insert newCase2;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new test case
//        newCase3 = new Case();
//        newCase3.street_address__c = '1234 Street';
//        newCase3.state__c = 'TX';
//        newCase3.city__c = 'Austin';
//        newCase3.zip_code__c = '78704';
//        newCase3.Vendor_code__c = 'SRSRSR';
//        newCase3.Work_Order_Type__c = 'Routine';
//        newCase3.Client__c = 'NOT-SG';
//        newCase3.Loan_Type__c = 'REO';
//        newCase3.ContactId = newContact.Id;
//        newCase3.Work_Ordered__c = 'Grass Recut';
//        newCase3.Approval_Status__c = 'Not Started';
//        newCase3.Due_Date__c = Date.today();
//        newCase3.Client_Commit_Date__c = Date.today();
//        newCase3.Last_Open_Verification_DateTime__c = threeHours;
//        
//        insert newCase3;
//        
//        //Padmesh Soni (06/18/2014) - Support Case Record Type
//        //Query record of Record Type of Case object
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
//                                            AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
//        
//        //Assert statement
//        System.assertEquals(1, recordTypes.size());
//        
//        //Padmesh Soni (06/18/2014) - Support Case Record Type
//        //New filter added into query of RecordTypeId
//        for(Case c : [Select Due_Date__c, Client_Commit_Date__c, Last_Open_Verification_DateTime__c From Case WHERE RecordTypeId NOT IN: recordTypes]){
//            system.assertEquals(Date.today(), c.Due_Date__c);
//            system.assertEquals(Date.today(), c.Client_Commit_Date__c);
//            system.assertEquals(threeHours, c.Last_Open_Verification_DateTime__c);
//        }
//    }
//    
//    
//    
//    static testMethod void myUnitTest() {
//        generateTestCases();
//        
//        Case[] cases = [select id from Case];
//        Client_Commitment__c[] comms = new Client_Commitment__c[]{};
//        
//        for(Case c:cases){
//            comms.add(new Client_Commitment__c(Work_Order__c=c.Id,Commitment_Date__c=Date.today(),Current_Status_of_Order__c = 'Not Started',
//                    Root_Cause_of_Delay__c = 'Delay In Receiving Work',
//                    Internal_Comments__c = '',
//                    Public_Comments__c = ''));
//        }
//        
//        comms[0].Sync_Status__c ='In Progress';
//        comms[0].Last_Sync_Attempt__c = DateTime.now().addHours(-1);
//        comms[0].Attempt_Count__c = 3;
//        
//        comms[1].Sync_Status__c ='In Progress';
//        date mydate = date.parse('1/1/2013');
//        comms[1].Last_Sync_Attempt__c = DateTime.now().addHours(-1);
//        comms[1].Commitment_Date__c = mydate;
//        comms[1].Attempt_Count__c = 1;
//        
//        insert comms;
//        System.Test.startTest();
//        BatchCommitmentTimeout t = new BatchCommitmentTimeout();
//        Database.executeBatch(t,100);
//        System.Test.stopTest();
//        //System.assertEquals(2,[select id from Client_Commitment__c where Sync_Status__c = 'Failed'].size());
//    }
}