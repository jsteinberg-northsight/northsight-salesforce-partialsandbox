@isTest()
/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/
*/
public without sharing class PruvanWebServices_Test {
//	public static testmethod void pruvanWebServicesTest1(){
//		/***********************************************************************************************************************************/
//		/*
//		this sections handles the creation of objects that will need to be used by some of the functions for testing purposes
//		*/
//		//create a new account
//		Account newAccount = new Account(
//			Name = 'Tester Account',
//			RecordTypeId = '01240000000Ubta'
//		);
//		insert newAccount;
//		//assert that the account exists
//		Account a = [SELECT Id FROM Account WHERE Id =: newAccount.Id];
//		system.assert(a != null);
//	
//		//create a new contact
//		Contact newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest',
//			RecordTypeId = '01240000000UbtfAAC'
//		);
//		insert newContact;
//		//assert that the contact exists
//		Contact c = [SELECT Id FROM Contact WHERE Id =: newContact.Id];
//		system.assert(c != null);
//		
//		//Create a new user with the Customer Portal Manager Standard profile
//		Profile newProfile = [SELECT Id FROM profile WHERE Name = 'Partner Community - Processors'];
//		system.assert(newProfile != null);
//		User newUser = new User(
//			alias = 'standt', 
//			contactId = newContact.id, 
//			email='standarduser@testorg.com',
//	  		emailencodingkey='UTF-8', 
//	  		lastname='Testing', 
//	  		languagelocalekey='en_US',
//	  		localesidkey='en_US', 
//	  		profileid = newProfile.Id,
//	  		timezonesidkey='America/Los_Angeles', username='standarduser@northsight.test',IsActive=true
//	  	);
//  		insert newUser;
//  		//assert that the user exists
//		User u = [SELECT Id FROM User WHERE Id =: newUser.Id];
//		system.assert(u != null);
//	
//		//create a new geocode_cache__c
//		Geocode_Cache__c newGeocode = new Geocode_Cache__c(
//			address_hash__c = '400cedarstbrdgprtct60562',
//			quarantined__c = false,
//			location__latitude__s = 11.11111111,
//			location__longitude__s = -111.22222222,
//			Street_Address__c = '400 Cedar st',
//			Zip_Code__c = '60562',
//			City__c = 'Bridgeport',
//			State__c = 'CT',
//			Formatted_Street_Address__c = '400 Cedar st',
//			Formatted_Zip_Code__c = '60562',
//			Formatted_City__c = 'Bridgeport',
//			Formatted_State__c = 'CT'
//		);
//		insert newGeocode;
//		//assert that the geocode exists
//		Geocode_Cache__c g = [SELECT Id FROM Geocode_Cache__c WHERE Id =: newGeocode.Id];		
//		system.assert(g != null);
//	
//	
//		//create a new case
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account;
//		
//		Case newCase = new Case(
//			Client__c = 'SG',
//			Due_Date__c = Date.today(),
//			Backyard_Serviced__c = 'Yes',
//			Excessive_Growth__c = 'No',
//			Debris_Removed__c = 'No',
//			Shrubs_Trimmed__c = 'Yes',
//			Work_Completed__c = 'No',
//			Vendor_Code__c = 'CHIGRS',
//			State__c = 'CT',
//			Status = 'Open',
//			City__c = 'Bridgeport',
//			Zip_Code__c = '60562',
//			Description = 'Work order for grass cutting',
//			OwnerId = newUser.Id,
//			ContactId = newContact.Id,
//			Street_Address__c = '400 Cedar st',
//			Geocode_Cache__c = newGeocode.Id,
//			Work_Ordered__c = 'Initial Grass Cut',
//			Work_Ordered_Text__c = 'Grass Cutting',
//			Client_Name__c = account.Id
//		);
//		insert newCase;
//		
//		//create a new case
//		Case newCase2 = new Case(
//			Client__c = 'NOT-SG',
//			Due_Date__c = Date.today(),
//			Backyard_Serviced__c = 'Yes',
//			Excessive_Growth__c = 'No',
//			Debris_Removed__c = 'No',
//			Shrubs_Trimmed__c = 'Yes',
//			Work_Completed__c = 'Yes',
//			Date_Serviced__c = Date.today(),
//			Vendor_Code__c = 'CHIGRS',
//			State__c = 'CT',
//			Status = 'Open',
//			City__c = 'Bridgeport',
//			Zip_Code__c = '60562',
//			Description = 'Work order for grass cutting',
//			OwnerId = newUser.Id,
//			ContactId = newContact.Id,
//			Street_Address__c = '400 Cedar st',
//			Geocode_Cache__c = newGeocode.Id,
//			Work_Ordered__c = 'Initial Grass Cut',
//			Work_Ordered_Text__c = 'Grass Cutting',
//			Approval_Status__c = 'In Process',
//			Client_Name__c = account.Id
//		);
//		insert newCase2;
//		//assert that the case exists
//		Case cs = [SELECT Id, CaseNumber, Reference_Number__c, Contact.Pruvan_Username__c FROM Case WHERE Id =: newCase.Id];
//		system.assert(cs != null);
//		//get the last digit of the CaseNumber for the getWorkOrders call
//		String lastDigit = cs.CaseNumber.RIGHT(1);
//		
//		Case cs2 = [SELECT CaseNumber FROM Case WHERE Id =: newCase2.Id];
//		/***********************************************************************************************************************************/
//		/*
//		this sections handles the creation of payload strings to mimic the different payloads that would be sent by Pruvan
//		*/
//		string validatePayload = '{"username":"pruvan22","password":"8c8cd34c37d6813808baa358b18169c211cb47f3","timestamp":1377887299}';
//		string getWorkOrdersPayload = '{"username":"pruvan23","password":"8c8cd34c37d6813808baa358b18169c211cb47f3","timestamp":"","workOrders":[], "NSPruvanResultSegment": "' + lastDigit + '"}';
//		string uploadPicturesPayload = '{"username":"pruvan99","password":"8c8cd34c37d6813808baa358b18169c211cb47f3","key2":"'+ cs.CaseNumber +'","uuid":"a7df9730-94a1-11e2-9e96-0800200c9a65","parentUuid":"c16a7080-94a1-11e2-9e96-0800200c9a66","timestamp":1377559122,"evidenceType":"before","fileType":"picture","fileName":"1363971679689.jpg","csrCertifiedPhoto":"1","csrCertifiedTime":"1","csrCertifiedLocation":"1","csrPictureCount":"","gpsLatitude":"30.508968","gpsLongitude":"-97.678362","gpsTimestamp":"1363971674","gpsAccuracy":"10.000000"}';	
//		string surveyValidatePayload = '{"uuId":"9f307fb6-4c21-4d15-aabe-6c3fd9ea399d","username":"sarcj07TEST","key2":"NSTST::'+ cs.CaseNumber +'","key3":"45022","key4":"InitialGrassCut","key5":null,"fileType":"survey","template":"sarcj07TEST::GC_001-v4","uploaderVersion":"mobile 3.8.7","srCertifiedPhoto":"1","srCertifiedTime":"1","srCertifiedLocation":null,"srPictureCount":null,"gpsLatitude":"30.0966739","gpsLongitude":"-90.4925435","gpsTimestamp":"1377811452","gpsAccuracy":"3013.0","timestamp":1377811542,"meta":{"parentId":"74283","surveyTemplateId":"sarcj07TEST::GC_001-v4","currentQuestionIndex":"12","surveyId":"021CB2EC-A10A-46D6-85C5-B0D527299819"},"answers":[{"id":"Work_Completed__c","answer":["Yes"],"hint":""},{"id":"Backyard_Serviced__c","answer":["Yes"],"hint":""},{"id":"Excessive_Growth__c","answer":["No"],"hint":""},{"id":"Shrubs_Trimmed__c","answer":["Yes"],"hint":""},{"id":"Debris_Removed__c","answer":["No"],"hint":""}]}';
//		string referencePayload = '{"username":"pruvan41","password":"8c8cd34c37d6813808baa358b18169c211cb47f3","workOrderNumber":"'+ cs.CaseNumber +'"}';
//		string workOrderStatusPayload = '{"workOrders": [{"workOrderNumber": "NSM::' + cs2.CaseNumber + '","status": "Complete"}]}';
//
//		/*
//		this sections handles the creation of payload strings that will cause errors
//		*/
//		string failValidatePayload = '{"username":"valid-username","password":"87e467e680038e7822a9bc083cbeac2959280f57","timestamp":1377887299}';
//		/***********************************************************************************************************************************/
//		//assert that we get the expected response using validatePayload sent to doValidate
//		system.assertEquals('{"error":"","validated":true}', PruvanWebservices.doValidate(validatePayload));
//		//assert that we get the expected response using failValidatePayload sent to doValidate
//		system.assertEquals('{"error":"invalid username or password","validated":false}', PruvanWebservices.doValidate(failValidatePayload));
//		
//		//assert that the response from the doGetWorkOrders method does not return an empty workOrders list
//		//system.assertNotEquals('{"error":"","workOrders":[]}', PruvanWebservices.doGetWorkOrders(getWorkOrdersPayload));
//		
//		//assert that we get the expected response using uploadPicturesPayload sent to doUploadPictures
//		//system.assertEquals('{"status":true,"error":null}', PruvanWebservices.doUploadPictures(uploadPicturesPayload));
//		
//		//assert that we get the expected response using surveyValidatePayload sent to doSurveyValidate
//		//system.assertEquals('{"meta":{"surveyTemplateId":"sarcj07TEST::GC_001-v4","surveyId":"021CB2EC-A10A-46D6-85C5-B0D527299819","currentQuestionIndex":"12","parentId":"74283"},"error":null}', PruvanWebservices.doSurveyValidate(surveyValidatePayload));
//		
//		//assert that we get the expected response using the referencePayload sent to doReferenceLookup
//		//system.assertEquals('{"referenceNumber":"'+ cs.Reference_Number__c +'","error":""}', PruvanWebservices.doReferenceLookup(referencePayload));
//		
//		//assert that we get the expected response using the workOrderStatusPayload sent to doWorkOrderStatus
//		system.assertEquals('{"error":null}', PruvanWebservices.doWorkOrderStatus(workOrderStatusPayload));
//	}
//	
//	public static testmethod void pruvanWebServicesTest2(){
//		string getWorkOrdersPayload = '{"username":"pruvan","password":"8c8cd34c37d6813808baa358b18169c211cb47f3","timestamp":1387469824,"workOrders":[]}';
//		//assert that we will get an empty list of workOrders when we call doGetWorkOrders without having any existing work orders
//		//system.assertEquals('{"error":"","workOrders":[]}', PruvanWebservices.doGetWorkOrders(getWorkOrdersPayload));
//	}
}