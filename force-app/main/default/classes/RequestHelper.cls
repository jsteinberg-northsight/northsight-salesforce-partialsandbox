public without sharing class RequestHelper {
	//GLOBALS
	//----------------------------------------------------------------
	private static Id partnerCommunityClientsProfile;
	//----------------------------------------------------------------
	
	//initializes the partnerCommunityClientsProfile variable with the 'Partner Community - Clients' profile Id
	public static void initialize() {
		partnerCommunityClientsProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Clients' limit 1].Id;
	}
	
	//returns true if the context user has the 'Partner Community - Clients' profile, returns false otherwise
	public static boolean userIsCommunityUser(Id userProfileId) {
		if(userProfileId == partnerCommunityClientsProfile) {
			return true;
		}
		return false;
	}
	
	//sets the Client_Name__c field with the AccountId of the context user for all incoming requests
	public static void setClientName(List<Request__c> incomingRequests, Id userId) {
		Id userAccountId = getAccountId(userId);
		for(Request__c request : incomingRequests) {
			request.Client_Name__c = userAccountId;
		}
	}
	
	//returns the AccountId of the context user
	private static Id getAccountId(Id userId) {
		return [SELECT AccountId FROM User WHERE Id =: userId limit 1].AccountId;
	}
}