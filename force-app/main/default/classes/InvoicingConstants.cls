/**
 *  Purpose         :   This class is used to hold all related constants for Invoicing functionality. 
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   12/19/2014
 *
 *  Current Version :   V1.0
 *
 *  Revision Log    :   V1.0 - Created - AC-6:Invoice Screen | Visual Force Page Prototype
 **/
public with sharing class InvoicingConstants {
	
	//Constants for Invoice sobject records
	public static final String INVOICE_RECORD_TYPE_DEVELOPERNAME_AP = 'Payable';
	public static final String INVOICE_RECORD_TYPE_DEVELOPERNAME_AR = 'Receivable';
}