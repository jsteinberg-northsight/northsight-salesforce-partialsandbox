/**
 *	Purpose			:	This is used to delete all ImageLink records of Work Orders. 
 *
 *	Created By		:	Padmesh Soni
 *
 *	Created Date	:	03/01/2014
 *
 *	Current Version	:	V1.0
 *
 *	Revision Log	:	V1.0 - Created - As per Delete All Pruvan Image Links assignment  
 **/
global class DeleteAllImageLinks {
	
	/**
	 *	@descpriton		:	This method is used to delete all ImageLinks records associated on WorkOrder.
	 *
	 *	@param			:	Work Order(Case) Id
	 *
	 *	@return			:	String
	 **/
	webservice static String deleteAllLinks(String caseId) {
		
		//List to hold Image Links records
        List<ImageLinks__c> listToBeDelete = new List<ImageLinks__c>();
        
        //Loop through ImageLinks
        for(ImageLinks__c imageLink : [Select Id From ImageLinks__c Where CaseId__c =: caseId]) {
        	    
            //Populate the list        
            listToBeDelete.add(imageLink);
        }
  		
  		try {
  			
			//Check size of list and delete Links
			if(listToBeDelete.size() > 0)
				delete listToBeDelete;
			else
				throw new CustomException(Label.DAL_NotFound);
			
			return Label.DAL_Success;
  		} catch(Exception e) {
  			
  			return e.getMessage();
  		}
    }
}