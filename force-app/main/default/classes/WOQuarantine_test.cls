@isTest
public with sharing class WOQuarantine_test {
//	static Map<String, Geocode_Cache__c> test; //map<addrHash, property>
//	
//	static void insertObjectsForQuarantineTest(){
//		// create a couple test geocodes
//        test = new Map<String, Geocode_Cache__c>();
//    
//        /*
//        test.put('1600amphitheatrepkwymtnvwca94043', new geocode_cache__c(  address_hash__c='1600amphitheatrepkwymtnvwca94043',
//                                        formatted_address__c='1600 Amphitheatre Parkway, Mountain View, CA 94043', 
//                                        location__latitude__s=40.424267080, 
//                                        location__longitude__s=-122.08542120,
//                                        quarantined__c=true,
//                                        partial_match__c=true,
//                                        last_geocoded__c = date.today(),
//                                        flag__c = false
//                                        ));
//                                        
//        test.put('2009s1ststaustintx78704', new geocode_cache__c(  address_hash__c='2009s1ststaustintx78704',
//                                        formatted_address__c='2009 S. 1st St, Austin, TX 78704', 
//                                        location__latitude__s=0, 
//                                        location__longitude__s=0,
//                                        quarantined__c=true,
//                                        partial_match__c=true,
//                                        last_geocoded__c = date.today()
//                                        ));
//        
//        test.put('3824southwaydraustintx78704', new geocode_cache__c(  address_hash__c='3824southwaydraustintx78704',
//                                        formatted_address__c='3824 Southway dr, Austin, TX 78704',
//                                        location__latitude__s=0,
//                                        location__longitude__s=0,
//                                        quarantined__c=true,
//                                        partial_match__c=true,
//                                        last_geocoded__c = date.today()
//                                        ));                             
//        insert test.values();
//        */
//        
//        //create cases. Properties will be automatically linked via orderassignment helper.
//        
//        //System.Debug('USER ID: '+UserInfo.getUserId());
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//        List<Case> caselist = new List<Case>();
//        Case NewCase = new Case();
//        NewCase.Street_Address__c = '1600 Amphitheatre Parkway';
//        NewCase.City__c = 'Mountain View';
//        NewCase.State__c = 'CA';
//        NewCase.Zip_Code__c = '94043';
//        NewCase.Vendor_Code__c = 'SRSRSR';
//        /*NewCase.Approval_Status__c = 'Quarantined';*/
//        /*NewCase.Geocode_Cache__c = test[0].id;*/
//        NewCase.Client_Name__c = account.Id;
//        caselist.add(NewCase);
//        
//        Case NewCase2 = new Case();
//        NewCase2.Street_Address__c = '2009 S. 1st St';
//        NewCase.City__c = 'Austin';
//        NewCase.State__c = 'TX';
//        NewCase2.Zip_Code__c = '78704';
//        NewCase2.Vendor_Code__c = 'SRSRSR'; 
//        /*NewCase2.Approval_Status__c = 'Quarantined';*/
//        /*NewCase2.Geocode_Cache__c = test[1].id;*/
//        NewCase2.Client_Name__c = account.Id;
//        caselist.add(NewCase2);
//        
//        Case NewCase3 = new Case();
//        NewCase3.Street_Address__c = '3824 Southway dr';
//        NewCase.City__c = 'Austin';
//        NewCase.State__c = 'TX';
//        NewCase3.Zip_Code__c = '78704';
//        NewCase3.Vendor_Code__c = 'SRSRSR';
//        /*NewCase3.Approval_Status__c = 'Quarantined';*/
//        /*NewCase3.Geocode_Cache__c = test[2].id;*/
//        NewCase3.Client_Name__c = account.Id;
//        caselist.add(NewCase3);
//        
//        insert caselist;
//        
//        //update properties to be quarantined
//        Geocode_Cache__c[] geoProps = [SELECT Id, address_hash__c, formatted_address__c, location__latitude__s, location__longitude__s, quarantined__c, partial_match__c, last_geocoded__c FROM Geocode_Cache__c WHERE Id IN (SELECT Geocode_Cache__c FROM Case WHERE Id IN: caselist)];
//		for(Geocode_Cache__c g : geoProps){
//			g.quarantined__c = true;
//			g.partial_match__c = true;
//			g.last_geocoded__c = date.today();
//			test.put(g.address_hash__c, g);
//			
//		}
//		update test.values();
//		
//	}
//	
//	static void insertObjectsForSortingTest(){
//		test = new Map<String, Geocode_Cache__c>();
//        
//        test.put('3824southwaydraustintx78704', new geocode_cache__c(  address_hash__c='3824southwaydraustintx78704',
//                                        formatted_address__c='3824 Southway dr, Austin, TX 78704',
//                                        location__latitude__s=0,
//                                        location__longitude__s=0,
//                                        quarantined__c=true,
//                                        partial_match__c=true,
//                                        last_geocoded__c = date.today()
//                                        ));
//                                                                                
//        test.put('2009s1ststaustintx78704', new geocode_cache__c(  address_hash__c='2009s1ststaustintx78704',
//                                        formatted_address__c='2009 S. 1st St, Austin, TX 78704', 
//                                        location__latitude__s=0, 
//                                        location__longitude__s=0,
//                                        quarantined__c=true,
//                                        partial_match__c=true,
//                                        last_geocoded__c = date.today()
//                                        ));
//                                        
//        test.put('1600amphitheatrepkwymtnvwca94043', new geocode_cache__c(  address_hash__c='1600amphitheatrepkwymtnvwca94043',
//                                        formatted_address__c='1600 Amphitheatre Parkway, Mountain View, CA 94043', 
//                                        location__latitude__s=40.424267080, 
//                                        location__longitude__s=-122.08542120,
//                                        quarantined__c=true,
//                                        partial_match__c=true,
//                                        last_geocoded__c = date.today()
//                                        ));
//                                        
//        insert test.values();
//        
//        //---------------------------------------------------
//		//PRE-TEST SETUP
//		//create cases
//        List<Case> caselist = new List<Case>();
//        Case NewCase = new Case();
//        NewCase.Street_Address__c = '3824 Southway dr';
//        NewCase.City__c = 'Austin';
//        NewCase.State__c = 'TX';
//        NewCase.Zip_Code__c = '78704';
//        NewCase.Vendor_Code__c = 'SRSRSR';
//        /*NewCase.Approval_Status__c = 'Quarantined';*/
//        NewCase.Due_Date__c = Date.today();
//        /*NewCase.Geocode_Cache__c = test[2].id;*/
//        caselist.add(NewCase);
//        
//        Case NewCase2 = new Case();
//        NewCase2.Street_Address__c = '2009 S. 1st St';
//        NewCase.City__c = 'Austin';
//        NewCase.State__c = 'TX';
//        NewCase2.Zip_Code__c = '78704';
//        NewCase2.Vendor_Code__c = 'SRSRSR';
//        /*NewCase2.Approval_Status__c = 'Quarantined';*/
//        NewCase2.Due_Date__c = Date.today().addDays(1);
//        /*NewCase2.Geocode_Cache__c = test[1].id;*/
//        caselist.add(NewCase2);
//        
//        Case NewCase3 = new Case();
//        NewCase3.Street_Address__c = '1600 Amphitheatre Parkway';
//        NewCase.City__c = 'Mountain View';
//        NewCase.State__c = 'CA';
//        NewCase3.Zip_Code__c = '94043';
//        NewCase3.Vendor_Code__c = 'SRSRSR';
//        /*NewCase3.Approval_Status__c = 'Quarantined';*/
//        NewCase3.Due_Date__c = Date.today().addDays(2);
//        /*NewCase3.Geocode_Cache__c = test[0].id;*/
//        caselist.add(NewCase3);
//        
//        insert caselist;
//	}
//	
//	//--------------------------------------------------------
//    //TESTS
//    //Unit tests for method testing and regression testing. 
//    //--------------------------------------------------------
//	
//	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - test the standard functionality of the WOQuarantineController
//    public static testmethod void TestWOQuarantineController() {
//        
//        //insert cases and update properties to quarantined
//        insertObjectsForQuarantineTest();
//        
//        // save a list of ids for future use
//        string[] test_ids = new string[] {};
//        for(geocode_cache__c g: test.values()) {
//            test_ids.add(g.id);
//        }
//        
//    
//        // instantiate the class
//        WOQuarantineController testcls = new WOQuarantineController();
//        
//        //system.debug('List size: ' + testcls.geocode_list.size());
//        //system.assert(false, test.keyset());
//        // Do an update on the first item - testing manual setting
//        testcls.geocode_param_id = test.get('1600amphitheatrepkwyaustintx94043').id;
//        testcls.param_lat = '47.4242424242';
//        testcls.param_lng = '-80.2424242424';
//        testcls.param_street = '1600 Amphitheatre Parkway';
//        testcls.param_city = 'Mountain View';
//        testcls.param_state = 'CA';
//        testcls.param_zip = '94043';
//        
//        
//        // Test the updater
//        testcls.updateWO();
//        
//        /*
// 		geocode_cache__c g = [select location__latitude__s,location__longitude__s,partial_match__c,manual_location__c from Geocode_Cache__c where address_hash__c='1600amphitheatrepkwymtnvwca94043'];
//        system.debug('test method debug latitutde ' + g.location__latitude__s);
//        */
//        // Verify that the update function change the values of the first record
//        system.assert(testcls.geocode_list.size() > 0);
//        for(WOQuarantineController.cacheWrapper gc : testcls.geocode_list){
//        	if(gc.geo.Id == test.get('1600amphitheatrepkwyaustintx94043').Id){
//		        system.assertEquals(47.4242424242, gc.geo.location__latitude__s);
//		        system.assertEquals(-80.2424242424, gc.geo.location__longitude__s);
//		        system.assertEquals(false, gc.geo.partial_match__c);
//		        system.assertEquals(true, gc.geo.manual_location__c);
//		        system.assertEquals('WOQuarantineController.cls', gc.geo.Geo_System__c);
//        		system.assert(Datetime.now() >= gc.geo.Geo_Timestamp__c);
//        		system.assert(Datetime.now().addMinutes(-1) < gc.geo.Geo_Timestamp__c);
//        		system.assertEquals('http://northsight-maprelay-env.elasticbeanstalk.com/?address=1600+Amphitheatre+Parkway%2C+Austin%2C+Tx+94043&sensor=false', gc.geo.Url_Used__c);
//        		
//        		//Code added - Padmesh Soni (1/12/2015) - CCP-15:Fill County on Property
//        		//Assert statement for county
//		        system.assertEquals('Santa Calra', gc.geo.County__c);
//        	}
//        }
//        
//        // Test regeocode function
//        testcls.geocode_param_id = test.get('1600amphitheatrepkwyaustintx94043').id +':'+ test.get('2009s1ststnullnull78704').id +':'+ test.get('3824southwaydrnullnull78704').id;
//        //system.debug('testcls.geocode_list: ' + testcls.geocode_list);
//        testcls.geocodeItem();
//        List<id> idList = new List<Id>();
//        idList.add(test.get('1600amphitheatrepkwyaustintx94043').id);
//        idList.add(test.get('2009s1ststnullnull78704').id);
//        idList.add(test.get('3824southwaydrnullnull78704').id);
//        map<id,geocode_cache__c> cacheList = new Map<id,geocode_cache__c>([select id,location__latitude__s,location__longitude__s,partial_match__c,manual_location__c from Geocode_Cache__c where id in :idList]);
//        
//        // The lat lng values are fixed in a test scenario from the geocoder
//        //system.debug('!!!!!!!!!! Assertion value: ' + cacheList.get(test.get('1600amphitheatrepkwyaustintx94043').id).location__latitude__s);
//        system.assertEquals(37.42291810, cacheList.get(test.get('3824southwaydrnullnull78704').id).location__latitude__s);
//        system.assertEquals(-122.08542120, cacheList.get(test.get('3824southwaydrnullnull78704').id).location__longitude__s);
//        system.assertEquals(false, cacheList.get(test.get('3824southwaydrnullnull78704').id).partial_match__c);
//        system.assertEquals(false, cacheList.get(test.get('3824southwaydrnullnull78704').id).manual_location__c);
//        
//        system.assertEquals(37.42291810, cacheList.get(test.get('2009s1ststnullnull78704').id).location__latitude__s);
//        system.assertEquals(-122.08542120, cacheList.get(test.get('2009s1ststnullnull78704').id).location__longitude__s);
//        system.assertEquals(false, cacheList.get(test.get('2009s1ststnullnull78704').id).partial_match__c);
//        
//        /*/ Test release single
//        testcls.geocode_param_id = test.get('1600amphitheatrepkwyaustintx94043').id;
//        testcls.releaseItem();*/
//
//        for(Id geoId : idList){
//        	testcls.geocode_param_id = geoId;
//        	testcls.releaseItem();
//        }
//
//        // Test release multiple
//        //testcls.geocode_list[0].isSelected = true;
//        //testcls.geocode_list[1].isSelected = true;
//        //testcls.releaseSelectedItems();
//        
//        geocode_cache__c[] posttest = [select geocode_status__c,
//                                                 partial_match__c,
//                                                 quarantined__c,
//                                                 geo_system__c,
//                                                 geo_timestamp__c from geocode_cache__c where id IN :test_ids order by createdDate desc];
//
//        system.assert(posttest[0].partial_match__c==false);
//        system.assert(posttest[1].partial_match__c==false);
//        system.assert(posttest[2].partial_match__c==false);
//        
//        system.assert(posttest[0].quarantined__c==false);
//        system.assert(posttest[1].quarantined__c==false);
//        system.assert(posttest[2].quarantined__c==false);
//        
//        system.assertEquals('WOQuarantineController.cls', posttest[0].Geo_System__c);
//        system.assertEquals('WOQuarantineController.cls', posttest[1].Geo_System__c);
//        system.assertEquals('WOQuarantineController.cls', posttest[2].Geo_System__c);
//        
//        system.assert(Datetime.now() >= posttest[0].Geo_Timestamp__c);
//        system.assert(Datetime.now().addMinutes(-1) < posttest[0].Geo_Timestamp__c);
//        system.assert(Datetime.now() >= posttest[1].Geo_Timestamp__c);
//        system.assert(Datetime.now().addMinutes(-1) < posttest[1].Geo_Timestamp__c);
//        system.assert(Datetime.now() >= posttest[2].Geo_Timestamp__c);
//        system.assert(Datetime.now().addMinutes(-1) < posttest[2].Geo_Timestamp__c);
//        
//        /*6-11-2013 the following tests test work orders to see if it is not: closed, canceled, or assigned*/
//        /*if the work order is none of the above and it is removed from quarantine then it is reassigned*/
//        //geocode_cache__c[] test = new geocode_cache__c[] {};
//        
//        //List<Case> wo = new List<Case>();
//        
//        /***************************************************************************************************/
//    }
//    
//    //--------------------------------------------------------
//   	//TEST:
//   	//Scenario - tests that geocodes are sorted according to their work order's due dates (ASC)
//    public static testmethod void testSorting(){
//    	//---------------------------------------------------
//		//PRE-TEST SETUP
//		//delete pre-existing test data
//		List<Geocode_Cache__c> gc = [select Id from Geocode_Cache__c];
//		delete gc;
//		List<Case> c = [select Id from Case];
//		delete c;
//		
//		//---------------------------------------------------
//		//PRE-TEST SETUP
//		//create geocodes
//    	insertObjectsForSortingTest();
//        
//        //instantiate the WOQuarantineController
//        WOQuarantineController testcls = new WOQuarantineController();
//
//        integer index = 0;
//        for(WOQuarantineController.cacheWrapper rec:testcls.geocode_list){
//        	if(index == 0){
//        		System.AssertEquals(test.get('3824southwaydraustintx78704').id, rec.geo.Id);
//        	}
//        	else if(index == 1){
//        		System.AssertEquals(test.get('2009s1ststaustintx78704').id, rec.geo.Id);
//        	}
//        	else{
//        		System.AssertEquals(test.get('1600amphitheatrepkwymtnvwca94043').id, rec.geo.Id);
//        	}
//        	index++;
//        }
//    }
}