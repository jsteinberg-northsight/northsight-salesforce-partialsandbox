public without sharing class Task1TriggerController {

	public static void GenerateTaskCodeItems(List<Task1__c> pTask1s) {
		if (pTask1s == null || pTask1s.isEmpty()) {
			return;
		}

		Set<Id> taskCodeIds = getTaskCodeIds(pTask1s);
		if (taskCodeIds.isEmpty()) {
			return;
		}

		Map<Id, List<Task_Code_Item__c>> taskCodeItemsByTaskCode = getCodeItems(taskCodeIds);
		if (taskCodeItemsByTaskCode.isEmpty()) {
			return;
		}

		createTaskCodeItems(pTask1s, taskCodeItemsByTaskCode);
	}

	private static Set<Id> getTaskCodeIds(List<Task1__c> pTask1s) {
		Set<Id> taskCodeIds = new Set<Id>();
		if (pTask1s == null || pTask1s.isEmpty()) {
			return taskCodeIds;
		}

		for (Task1__c currentTask1 : pTask1s) {
			if (currentTask1.Task_Code__c != null && currentTask1.Auto_Fill_Task_Items__c) {
				taskCodeIds.add(currentTask1.Task_Code__c);
			}
		}
		return taskCodeIds;
	}

	private static Map<Id, List<Task_Code_Item__c>> getCodeItems(Set<Id> pTaskCodeIds) {
		Map<Id, List<Task_Code_Item__c>> taskCodeItemsByTaskCode = new Map<Id, List<Task_Code_Item__c>>();
		if (pTaskCodeIds == null || pTaskCodeIds.isEmpty()) {
			return taskCodeItemsByTaskCode;
		}

		for (Task_Code_Item__c currentItem : [SELECT
		                                          Id,
		                                          Name,
		                                          Task_Code__c,
		                                          Task_Item_Name__c ,
		                                          Task_Item_Help_Text__c,
		                                          Default_Sort__c ,
		                                          Related_Article__c
		                                      FROM
		                                          Task_Code_Item__c
		                                      WHERE
		                                          Task_Code__c IN :pTaskCodeIds]) {
			if (currentItem.Task_Code__c == null) {
				continue;
			}
			if (!taskCodeItemsByTaskCode.containsKey(currentItem.Task_Code__c)) {
				taskCodeItemsByTaskCode.put(
					currentItem.Task_Code__c,
					new List<Task_Code_Item__c>()
				);
			}
			taskCodeItemsByTaskCode.get(currentItem.Task_Code__c).add(currentItem);
		}
		return taskCodeItemsByTaskCode;
	}

	private static void createTaskCodeItems(List<Task1__c> pTask1s, Map<Id, List<Task_Code_Item__c>> pTaskCodeItemsByTaskCode) {
		if (pTask1s == null || pTask1s.isEmpty() || pTaskCodeItemsByTaskCode == null || pTaskCodeItemsByTaskCode.isEmpty()) {
			return;
		}

		list<Task_Item__c> newTaskItems = new List<Task_Item__c>();
		Task_Code_Item__c tempCodeItem;
		for (Task1__c currentTask : pTask1s) {
			if (currentTask.Task_Code__c != null && currentTask.Auto_Fill_Task_Items__c && pTaskCodeItemsByTaskCode.containsKey(currentTask.Task_Code__c)) {
				for (Task_Code_Item__c currentCodeItem: pTaskCodeItemsByTaskCode.get(currentTask.Task_Code__c)) {
					newTaskItems.add(
						new Task_Item__c(
							Task__c = currentTask.Id,
							Task_Item_Name__c = currentCodeItem.Task_Item_Name__c,
							Task_Item_Help_Text__c = currentCodeItem.Task_Item_Help_Text__c,
							Sort__c = currentCodeItem.Default_Sort__c,
							Related_Article__c = currentCodeItem.Related_Article__c,
							Task_Item_Template__c = currentCodeItem.Id,
                            Status__c = 'Open'
						)
					);
				}
			}
		}
		if (!newTaskItems.isEmpty()) {
			insert newTaskItems;
		}
	}
}