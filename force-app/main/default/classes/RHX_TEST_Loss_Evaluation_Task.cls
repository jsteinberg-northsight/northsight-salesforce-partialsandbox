@isTest(SeeAllData=true)
public class RHX_TEST_Loss_Evaluation_Task {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Loss_Evaluation_Task__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Loss_Evaluation_Task__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}