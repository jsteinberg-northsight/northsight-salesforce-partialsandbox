@isTest(seeAllData=false)
private class Test_BatchCreateTestOrders {
//
///**
// *  Purpose         :   This is used for testing and covering BatchCreateTestOrders class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/06/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Required Maintenance: Unit Test Improvements
// *	
// *	Coverage		:	100%
// **/
//    
//    //test method added to check the functionality with code coverage
//    static testMethod void myUnitTest() {
//        /*
//    	//Test starts here
//    	Test.startTest();
//    	
//    	//Execute the batch
//    	Database.executeBatch(new BatchCreateTestOrders(), 200);
//    	
//    	//Test stops here
//    	Test.stopTest();
//    	
//    	//Query result of Cases
//    	List<Case> workOrders = [SELECT Id FROM Case];
//    	
//    	//Assert statement here
//    	System.assertEquals(10, workOrders.size());
//    */
//    }
// 
}