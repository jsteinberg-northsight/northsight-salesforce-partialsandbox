/**
 *  Purpose         :   This Batch is used for delete some records only one time for resolving Data Limit Exceeded.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   08/07/2014
 *
 *  Current Version :   V1.0
 *
 *  Revision Log    :   V1.0 - Created - Padmesh Soni(08/07/2014) - Clear Data
 **/
global class Batch_ClearDataOneTime  implements Database.Batchable<SObject> {
    
    //Start method
    global Database.Querylocator start(Database.BatchableContext BC) {
        
        //return query result of VWMSurvey records
        return Database.getQueryLocator([SELECT Id FROM Order_Import_History__c]);
    }
    
    //Execute method
    global void execute(Database.BatchableContext BC, List<Sobject> sobjectsList) {
        
        //Check for size of List
        if(sobjectsList.size() > 0){
            
            //Delete the records into list
            Database.delete(sobjectsList, false);
            
            //empty the recycle bin of the markedLogs
            Database.EmptyRecycleBinResult[] emptyRecycleBinResults = DataBase.emptyRecycleBin(sobjectsList);
        }
    }
    
    // Finish method
    global void finish(Database.BatchableContext BC) {
        
    }
}