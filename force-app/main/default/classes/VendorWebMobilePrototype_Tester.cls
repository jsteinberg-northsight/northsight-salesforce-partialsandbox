@isTest(SeeAllData=false)
public with sharing class VendorWebMobilePrototype_Tester {
//    public static testmethod void test_VendorWebMobilePrototype(){
//        system.Test.startTest();
//        
//        VendorWebMobilePrototype.initialize();
//        system.assertEquals('https://vwmbridge-env.elasticbeanstalk.com/getWorkOrderList?', VendorWebMobilePrototype.calloutUrl);
//        system.assertNotEquals(null, VendorWebMobilePrototype.request);
//        system.assertNotEquals(null, VendorWebMobilePrototype.objDataList);
//        system.assertNotEquals(null, VendorWebMobilePrototype.innerDataMap);
//        system.assertNotEquals(null, VendorWebMobilePrototype.outerDataMap);
//        system.assertNotEquals(null, VendorWebMobilePrototype.newCases);
//        
//        List<Case> caseList = [SELECT Reference_Number__c, 
//                                      Loan_Type__c, 
//                                      Street_Address__c, 
//                                      City__c, 
//                                      State__c, 
//                                      Zip_Code__c, 
//                                      Work_Ordered_Text__c, 
//                                      SG_WorkOrderId__c, 
//                                      SG_DepartmentCode__c, 
//                                      SG_ContractorId__c
//                               FROM Case];
//        system.assertEquals(2, caseList.size());
//        for(Case c : caseList){
//            if(c.Street_Address__c.contains('11911')){
//                system.assertEquals('147695847', c.Reference_Number__c);
//                system.assertEquals('REO', c.Loan_Type__c);
//                system.assertEquals('11911 N PROSPECT POINT', c.Street_Address__c);
//                system.assertEquals('TUCSON', c.City__c);
//                system.assertEquals('AZ', c.State__c);
//                system.assertEquals('85737', c.Zip_Code__c);
//                system.assertEquals('REO Grass Cut/Lawn Care', c.Work_Ordered_Text__c);
//                system.assertEquals('154643216750', c.SG_WorkOrderId__c);
//                system.assertEquals(6, c.SG_DepartmentCode__c);
//                system.assertEquals('140228335800', c.SG_ContractorId__c);
//            } else {
//                system.assertEquals('147481723', c.Reference_Number__c);
//                system.assertEquals('REO', c.Loan_Type__c);
//                system.assertEquals('2530 W CAMINO DE LA CATER', c.Street_Address__c);
//                system.assertEquals('TUCSON', c.City__c);
//                system.assertEquals('AZ', c.State__c);
//                system.assertEquals('85742', c.Zip_Code__c);
//                system.assertEquals('REO Grass Cut/Lawn Care', c.Work_Ordered_Text__c);
//                system.assertEquals('154046335600', c.SG_WorkOrderId__c);
//                system.assertEquals(6, c.SG_DepartmentCode__c);
//                system.assertEquals('140228335800', c.SG_ContractorId__c);
//            }
//        }
//        
//        system.Test.stopTest();
//    }
}