@isTest(seeAllData=false)
private class Test_Sched_Batch_AttachmentCleanupOnCase {
/**
 *  Purpose         :   This is used for testing and covering Sched_Batch_AttachmentCleanupOnCases scheduler.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   01/28/2014
 *
 *	Current Version	:	V1.0
 *	
 *	Coverage		:	100%
 **/	
//	//Scheduler test method
//    static testMethod void testSchedBatchAttachmentCleanupOnCase() {
//        
//        //Test starts here
//		Test.startTest();
//        
//        // Schedule the test job at midnight Sept. 3rd. 2022
//        String jobId = System.schedule('Test_Sched_Batch_AttachmentCleanupOnCases', '0 0 0 3 9 ? 2022', new Sched_Batch_AttachmentCleanupOnCases());
//        
//        // Get the information from the CronTrigger API object
//        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
//        
//        // Verify the expressions are the same
//        System.assertEquals('0 0 0 3 9 ? 2022', ct.CronExpression);
//        
//        // Verify the job has not run
//        System.assertEquals(0, ct.TimesTriggered);
//        
//        //Test stops here
//		Test.stopTest();
//    }
}