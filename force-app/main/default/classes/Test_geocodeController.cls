@isTest
public class Test_geocodeController {
//
//    public static testmethod void test(){
//        //create a client zone and client zip code test obj for NewCase test obj
//        /*******************************************************************************************************************/
//        //create a new client zone test obj
//        ClientZone__c cz = new ClientZone__c();
//        cz.Client_Name__c = 'SG';
//        cz.Order_Type__c = 'Routine';
//        insert cz;
//        
//        //create a new client zip code test obj
//        ClientZipCode__c zc = new ClientZipCode__c();
//        zc.ClientZone__c = cz.Id;
//        zc.Zip_Code_Value__c = '90311';
//        insert zc;
//        /*******************************************************************************************************************/
//        Id clientRt = [select id from recordtype where sobjecttype = 'Account' and name = 'Client'].Id;
//        Account newAccount = new Account(
//            Name = 'geocodeController',
//            Status__c = 'Active',
//            RecordTypeId = clientRt
//        );
//        insert newAccount;
//        
//        Case NewCase = new Case();
//            NewCase.Zip_Code__c = '90311';
//            NewCase.Vendor_Code__c = 'SRSRSR';
//            NewCase.Street_Address__c = 'new street';
//            NewCase.Work_Order_Type__c = 'Routine';
//            NewCase.Client__c = 'SG';
//            NewCase.Client_Name__c = newAccount.Id;
//        insert NewCase;
//        
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account; 
//        
//        Case updateNewCase = new Case(Id = NewCase.Id);
//        updateNewCase.Client_Name__c = account.Id;
//        //link the case to the client zip code
//        updateNewCase.ClientZipCode__c = zc.Id;
//        update updateNewCase;
//        
//        for(Case c : [SELECT ClientZipCode__c FROM Case WHERE Id =: NewCase.Id]){
//            system.assert(c.ClientZipCode__c != null);
//        }
//            
//         newCase = [select geocode_cache__r.notes__c, Street_Add_Concat__c, geocode_cache__r.location__latitude__s,geocode_cache__r.location__longitude__s,geocode_cache__r.Formatted_Address__c, geocode_cache__r.Formatted_Street_Address__c, geocode_cache__r.Formatted_City__c, geocode_cache__r.Formatted_State__c, geocode_cache__r.Formatted_Zip_Code__c, Street_Address__c, City__c, State__c, Zip_Code__c from Case where id = :newCase.id];
//         geocodeController controller = new geocodeController(new ApexPages.StandardController(NewCase));
//         controller.param_street = '1600 Amphitheatre Parkway';
//         controller.param_city = 'Mountain View';
//         controller.param_state = 'CA';
//         controller.param_zip = '94043';   
//         controller.param_lat = '47.4242424242';
//         controller.param_lng = '-80.2424242424';
//         controller.saveAndRelease();
//           
//           //test to see if the call to generateLinks method creates a geozone link off a work order
//           /*******************************************************************************************************************/
//           //create a geozone link list
//           List<GeoZoneLink__c> links = new List<GeoZoneLink__c>();
//           //create a new case object with the geocode cache and clientzone from the case that has the Id of newCase
//           Case newWO = [SELECT Geocode_Cache__c, ClientZipCode__r.ClientZone__c FROM Case WHERE Id =: newCase.Id];
//           System.assert(newWO.Geocode_Cache__c != null);
//           System.assert(newWO.ClientZipCode__r.ClientZone__c != null);
//           //select the geozone link object that has a geocode cache and clientzone equal to newWO's geocode cache and clientzone
//           for(GeoZoneLink__c gz : [SELECT Geocode_Cache__c, ClientZone__c FROM GeoZoneLink__c WHERE Geocode_Cache__c =: newWO.Geocode_Cache__c AND ClientZone__c =: newWO.ClientZipCode__r.ClientZone__c]){
//                //add the link to the list
//                links.add(gz);
//           }
//           //check to see if the list has a size > 0, if it does, then a geozone link was created off the newCase case object
//           System.Assert(links.size() > 0);
//           /*******************************************************************************************************************/
//           
//           controller.cancel();
//    }
}