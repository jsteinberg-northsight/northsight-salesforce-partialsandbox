@isTest(seeAllData=false)
private class Test_ScheduleBatchGeocodeWorkOrderProp {
///**
// *  Purpose         :   This is used for testing and covering ScheduleBatchGeocodeWorkOrderProperties scheduler.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/04/2013
// *
// *	Current Version	:	V1.0 - Created - Required Maintenance: Unit Test Improvements
// *	
// *	Coverage		:	100%
// **/
// 	
// 	  private static ClientZone__c newClientZone;
//    private static ClientZipCode__c txClientZip;
//    private static ClientZipCode__c ctClientZip;
//    private static Case partialCase;
//    private static Geocode_Cache__c newGeocodeCache;
//    private static Case[] insertAllCases;
//    
//    //Padmesh Soni (04/29/2014) - Required Maintenance: Unit Test Improvements 
//    //Test record created for Group
//    private static Group qurantineGrp;
//    
//    //Padmesh Soni (04/29/2014) - Required Maintenance: Unit Test Improvements 
//    //New generic method created
//    private static void createGroup() {
//    	
//    	qurantineGrp = new Group(Name = 'Quarantine_Managers', DeveloperName = 'QuarantineManagers');
//        insert qurantineGrp;
//    }
//    
//    private static void generateClientZoneData(){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new client zone
//        newClientZone = new ClientZone__c(
//            Client_Name__c = 'SG',
//            Client_Department__c = 'REO',
//            Order_Type__c = 'Routine',
//            Name = 'SG-RGC:TX53'
//        );
//        insert newClientZone;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new client zip code
//        txClientZip = new ClientZipCode__c(
//            ClientZone__c = newClientZone.Id,
//            Zip_Code_Value__c = '78704'
//        );
//        insert txClientZip;
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new client zip code
//        ctClientZip = new ClientZipCode__c(
//            ClientZone__c = newClientZone.Id,
//            Zip_Code_Value__c = '60562'
//        );
//        insert ctClientZip;
//    }
//    
//    private static void buildTestCaseWithPartialAddress(){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new case
//        partialCase = new Case(
//            Street_Address__c = '515 Partial St',
//            State__c = 'CT',
//            City__c = 'Bridgeport',
//            Zip_Code__c = '60562',
//            Vendor_Code__c = 'TRYWZYM',
//            Client__c = 'SG',
//            Work_Order_Type__c = 'Routine',
//            Work_Ordered__c = 'Grass Recut',
//            Loan_Type__c = 'REO'
//        );
//        //insert partialCase;
//    }
//    
//    //Test method added
//	static testmethod void myUnitTest(){
//		
//		generateClientZoneData();
//    	buildTestCaseWithPartialAddress();
//    	
//    	insert partialCase;
//    	//set the case's override_quarantine to true
//    	List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//		
//    	Case newCase = [SELECT Id, Override_Quarantine__c FROM Case WHERE Id =: partialCase.Id];
//    	system.assertEquals(false, newCase.Override_Quarantine__c);
//    	newCase.Override_Quarantine__c = true;
//    	newCase.Client_Name__c = account.Id;
//    	update newCase;
//    	
//    	//set the property to be quarantined
//    	Geocode_Cache__c newGC = [SELECT Id, quarantined__c from Geocode_Cache__c where address_hash__c = '515partialstbrgprtct60562'];
//    	system.assertEquals(false, newGC.quarantined__c);
//    	newGC.quarantined__c = true;
//    	update newGC;
//    	
//		//create a cron expression test obj
//		string CRON_EXP = '0 0 0 * * ? *';
//		
//		//start the test
//		Test.startTest();
//		
//		//create a string of the jobId for a new schedule instance of the ScheduleBatchGeocodeWorkOrderProperties class
//		string jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new ScheduleBatchGeocodeWorkOrderProperties());
//		
//		//query the CronTrigger table selecting in CronTrigger fields where the Id = jobId
//		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id =: jobId];
//		system.assertEquals(CRON_EXP, ct.CronExpression);
//		
//		//assert that the job has not been triggered
//		system.assertEquals(0, ct.TimesTriggered);
//		
//		//assert when the next fire time will be
//		system.assert(ct.NextFireTime != null);
//		
//		//Test stops here
//		Test.stopTest();
//	}
}