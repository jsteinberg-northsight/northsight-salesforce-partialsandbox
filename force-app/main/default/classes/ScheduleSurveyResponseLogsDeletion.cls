global class ScheduleSurveyResponseLogsDeletion implements Schedulable{
	global void execute(SchedulableContext ctx) {
		BatchDeleteSurveyResponseLogs b = new BatchDeleteSurveyResponseLogs();
		
		Database.executeBatch(b, 200);
	}
	
	
	//Padmesh Soni (05/02/2014) - Required Maintenance: Unit Test Improvements
    //Test method added
	static testmethod void zoneHistoryTest(){
		
		//create a cron expression test obj
		string CRON_EXP = '0 0 0 * * ? *';
		
		//start the test
		Test.startTest();
		
		//create a string of the jobId for a new schedule instance of the ZoneHistorySnapshot class
		string jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new ScheduleSurveyResponseLogsDeletion());
		
		//query the CronTrigger table selecting in CronTrigger fields where the Id = jobId
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id =: jobId];
		system.assertEquals(CRON_EXP, ct.CronExpression);
		
		//assert that the job has not been triggered
		system.assertEquals(0, ct.TimesTriggered);
		
		//assert when the next fire time will be
		system.assert(ct.NextFireTime != null);
		
		//Test stops here
		Test.stopTest();
	}
}