@isTest(SeeAllData=true)
public class RHX_TEST_EXT_File_Rlt {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM EXT_File_Rlt__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new EXT_File_Rlt__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}