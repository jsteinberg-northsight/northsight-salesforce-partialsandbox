@isTest
private class Test_BatchCheckGeocoder {
///**
// *  Purpose         :   This is used for testing and covering BatchDeleteSurveyResponseLogs. 
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   05/02/2014
// *
// *	Current Version	:	V1.0 - Padmesh Soni (05/02/2014) - Required Maintenance: Unit Test Improvements
// *
// *	Coverage		:	100%
// **/
// 	
// 	//Test method to test batch functionality
//    static testMethod void myUnitTest() {
//        
//         //List to hold Pruvan Geocode_Process_Log__c Log records
//        List<Geocode_Process_Log__c> geoProcessLogs = new List<Geocode_Process_Log__c>();
//        geoProcessLogs.add(new Geocode_Process_Log__c(Exceeded_Limit__c = true));
//        geoProcessLogs.add(new Geocode_Process_Log__c(Exceeded_Limit__c = true));
//        
//        //insert Pruvan Survay Response logs
//        insert geoProcessLogs;
//        
//        //Test starts here
//        Test.startTest();
//        
//        BatchCheckGeocoder bc = new BatchCheckGeocoder();
//        bc.start();
//        
//        //Query the result of System alerts object
//        List<System_Alerts__c> sysAlerts = [SELECT Id FROM System_Alerts__c];
//        
//        //assert statements
//        System.assertEquals(1, sysAlerts.size());
//        
//        //Test stops here
//        Test.stopTest();
//    }
}