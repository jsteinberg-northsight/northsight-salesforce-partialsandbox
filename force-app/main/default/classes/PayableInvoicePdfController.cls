/**
 *  Purpose         :   This class is controller for invoices pdf pages. 
 *
 *  Created By      :   
 *
 *  Created Date    :   03/02/2015
 *
 *  Current Version :   V1.0
 *
 *  Revision Log    :   V1.0 - Created - :Invoice Screen | Visual Force Page Prototype
 **/
public with sharing class PayableInvoicePdfController {
	public Invoice__c invoice;
	public String invoiceId;
	public Case caseInvoice;
	public String address{get; set;}
	public String checkLong{get;set;} 
	public PayableInvoicePdfController(ApexPages.Standardcontroller SC){
		//WO {!Invoice__c.Work_Order__r.CaseNumber} Payable {!Invoice__c.Name}
		if(String.isNotBlank(SC.getId()))
            invoiceId = SC.getId();
            
        invoice = [Select Id, Name, Work_Order__c,Pay_To__r.MailingStreet from Invoice__c where Id =: invoiceId limit 1];
        caseInvoice = [Select Id, CaseNumber from Case where Id =: invoice.Work_Order__c limit 1];
		String fileName = 'WO '+caseInvoice.CaseNumber+' Payable '+invoice.Name+'.pdf' ;
		address = invoice.Pay_To__r.MailingStreet;
		checkLong = '&nbsp;';
		if(address.length()>=24)//Two lines
			checkLong = '&nbsp;<br/>&nbsp;';
			
		Apexpages.currentPage().getHeaders().put( 'content-disposition', 'inline; filename=' + fileName );
	}

}