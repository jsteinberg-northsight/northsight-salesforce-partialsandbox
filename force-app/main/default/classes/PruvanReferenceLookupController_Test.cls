@isTest()
/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/
*/
public with sharing class PruvanReferenceLookupController_Test {
//	public static testmethod void testReferenceLookupController(){
//		/***********************************************************************************************************************************/
//		/*
//		this sections handles the creation of objects that will need to be used by some of the functions for testing purposes
//		*/
//		//create a new account
//		Account newAccount = new Account(
//			Name = 'Tester Account',
//			RecordTypeId = '0124000000011Q2'
//		);
//		insert newAccount;
//		//assert that the account exists
//		Account a = [SELECT Id FROM Account WHERE Id =: newAccount.Id];
//		system.assert(a != null);
//	
//		//create a new contact
//		Contact newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest'
//		);
//					newContact.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//	newContact.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//	newContact.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//	newContact.Shrub_Trim__c = 3;//Pricing...required 
//	newContact.REO_Grass_Cut__c = 3;//Pricing
//	newContact.Pay_Terms__c = 'Net 30';//Terms required
//	newContact.Trip_Charge__c = 25;//Pricing
//	newContact.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//	newContact.Date_Active__c = Date.Today();//Date Active must be set. 
//	newContact.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//	newContact.MailingStreet = '30 Turnberry Dr';
//	newContact.MailingCity = 'La Place';
//	newContact.MailingState = 'LA';
//	newContact.MailingPostalCode = '70068';
//	newContact.OtherStreet = '30 Turnberry Dr';
//	newContact.OtherCity = 'La Place';
//	newContact.OtherState = 'LA';
//	newContact.OtherPostalCode = '70068';
//	newContact.Other_Address_Geocode__Latitude__s = 30.00;
//	newContact.Other_Address_Geocode__Longitude__s = -90.00;
//		insert newContact;
//		//assert that the contact exists
//		Contact c = [SELECT Id FROM Contact WHERE Id =: newContact.Id];
//		system.assert(c != null);
//		
//		//Create a new user with the Customer Portal Manager Standard profile
//		Profile newProfile = [SELECT Id FROM profile WHERE Name = 'Partner Community - Vendors'];
//		system.assert(newProfile != null);
//		User newUser = new User(
//			alias = 'standt', 
//			contactId = newContact.id, 
//			email='standarduser@testorg.com',
//	  		emailencodingkey='UTF-8', 
//	  		lastname='Testing', 
//	  		languagelocalekey='en_US',
//	  		localesidkey='en_US', 
//	  		profileid = newProfile.Id,
//	  		timezonesidkey='America/Los_Angeles', username='standarduser@northsight.test',IsActive=true
//	  	);
//  		insert newUser;
//  		//assert that the user exists
//		User u = [SELECT Id FROM User WHERE Id =: newUser.Id];
//		system.assert(u != null);
//	
//		//create a new geocode_cache__c
//		Geocode_Cache__c newGeocode = new Geocode_Cache__c(
//			address_hash__c = '400cedarstbrdgprtct60562',
//        	quarantined__c = false,
//        	location__latitude__s = 11.11111111,
//        	location__longitude__s = -111.22222222
//		);
//		insert newGeocode;
//		//assert that the geocode exists
//		Geocode_Cache__c g = [SELECT Id FROM Geocode_Cache__c WHERE Id =: newGeocode.Id];		
//		system.assert(g != null);
//	
//	
//		//create a new case
//		Case newCase = new Case(
//			Client__c = 'SG',
//			Due_Date__c = Date.today(),
//			Backyard_Serviced__c = 'Yes',
//			Excessive_Growth__c = 'No',
//			Debris_Removed__c = 'No',
//			Shrubs_Trimmed__c = 'Yes',
//			Work_Completed__c = 'No',
//			Vendor_Code__c = 'CHIGRS',
//			State__c = 'CT',
//			Status = 'Open',
//			City__c = 'Bridgeport',
//			Zip_Code__c = '60562',
//			Description = 'Work order for grass cutting',
//			OwnerId = newUser.Id,
//			ContactId = newContact.Id,
//			Street_Address__c = '400 Cedar st',
//			Geocode_Cache__c = newGeocode.Id,
//			Work_Ordered__c = 'Initial Grass Cut',
//			Work_Ordered_Text__c = 'Grass Cutting',
//			Reference_Number__c = '99999999'
//		);
//		insert newCase;
//		//assert that the case exists
//		Case cs = [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id];
//		system.assert(cs != null);
//		/***********************************************************************************************************************************/
//		//set the payload var to mimic the one that would be sent by Pruvan
//		string referencePayload = '{"username":"pruvan","password":"8c8cd34c37d6813808baa358b18169c211cb47f3","workOrderNumber":"'+ cs.CaseNumber +'"}';
//		
//		//instantiate the page
//		PageReference pageRef = Page.PruvanNorthsightReferenceLookup;
//		
//		//set the page as the test starting point
//		Test.setCurrentPage(pageRef);
//		
//		//instantiate the controller
//		PruvanReferenceLookupController controller = new PruvanReferenceLookupController();
//		
//		//add the payload to the page parameters
//		ApexPages.currentPage().getParameters().put('payload', referencePayload);
//		
//		//assert that we get an error before controller execution
//		system.assertEquals('{"error":"The web service call has failed to hit the proper web service to initiate execution."}', controller.getResponse());
//		
//		//fire off the controller's execute method
//		controller.execute();
//		
//		//assert that we get the expected value back from the controller
//		//system.assertEquals('{"referenceNumber":"99999999","error":""}', controller.getResponse());
//	}
}