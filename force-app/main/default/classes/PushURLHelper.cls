global without sharing class PushURLHelper {
    public static string retrieveURL(String ExternalSystem){
        String urlResult;
        //Salesforce method of determining environment
        if (!IsProductionOrg()) {
            //URL method of determining full or dev sandbox
            String[] hostArray = URL.getSalesforceBaseUrl().getHost().split('\\.');
            if (!hostArray[0].containsignorecase('--full') && hostArray[1].containsignorecase('cs'))
                //retrieve URL from PushURLs custom setting where external system name matches
                urlResult = [SELECT DevURL__c FROM PushURLs__c WHERE ExternalSystem__c =: ExternalSystem LIMIT 1].DevURL__c;
            else if (hostArray[0].containsignorecase('--full') && hostArray[1].containsignorecase('cs'))
                urlResult = [SELECT SandboxURL__c FROM PushURLs__c WHERE ExternalSystem__c =: ExternalSystem LIMIT 1].SandboxURL__c ;
        }   
        else //retrieve production URL from PushURLs custom setting where external system name matches
            urlResult = [SELECT ProductionURL__c FROM PushURLs__c WHERE ExternalSystem__c =: ExternalSystem LIMIT 1].ProductionURL__c ;
            
        return urlResult;
    }
    
    public static Boolean IsProductionOrg() { // Method to check of environement is Production ORG or not
        Organization org = [SELECT IsSandbox FROM Organization WHERE Id =: UserInfo.getOrganizationId()];
        return (org.IsSandbox == true) ? false : true;
    }
}