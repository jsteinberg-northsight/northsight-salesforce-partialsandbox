@isTest
private class UpdateContactWithUserIdTest {
//	
//	static testMethod void addUserTest() {
//		Account testAccount = new Account(Name='testAccount',RecordTypeId='0124000000011Q2');
//		insert testAccount;
//		Contact con = new Contact(LastName='test', AccountId=testAccount.Id, RecordTypeId = '01240000000UPSr');
//			con.AccountId = testAccount.id;
//	//Caps - Number of orders that can be assigned at once (open + all completed this week)
//	con.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//	con.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//	con.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//	con.Shrub_Trim__c = 3;//Pricing...required 
//	con.REO_Grass_Cut__c = 3;//Pricing
//	con.Pay_Terms__c = 'Net 30';//Terms required
//	con.Trip_Charge__c = 25;//Pricing
//	con.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//	con.Date_Active__c = Date.Today();//Date Active must be set. 
//	con.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//	con.MailingStreet = '30 Turnberry Dr';
//	con.MailingCity = 'La Place';
//	con.MailingState = 'LA';
//	con.MailingPostalCode = '70068';
//	con.OtherStreet = '30 Turnberry Dr';
//	con.OtherCity = 'La Place';
//	con.OtherState = 'LA';
//	con.OtherPostalCode = '70068';
//	con.Other_Address_Geocode__Latitude__s = 30.00;
//	con.Other_Address_Geocode__Longitude__s = -90.00;
//		insert con;
//		Profile testProfile = [SELECT Id FROM Profile WHERE Name='Partner Community - Vendors'];
//		User testUser = new User(Alias = 'standt1',
//			ContactId = con.Id,
//			Email='UpdateContactWithUserIdTest@testorg.com',
//			EmailEncodingKey='UTF-8',
//			LastName='Testing1',
//			LanguageLocaleKey='en_US', 
//			LocaleSidKey='en_US',
//			ProfileId = testProfile.Id,
//			TimeZoneSidKey='America/Los_Angeles',
//			UserName='UpdateContactWithUserIdTest@testorg.com');
//		test.startTest();
//		insert testUser;
//		test.stopTest();
//		
//		con = [SELECT Portal_User_Id__c FROM Contact WHERE Id =: con.Id];
//		system.assertEquals(testUser.Id, con.Portal_User_Id__c, 'User Id copied to Contact record');
//	}
//	
//	static testMethod void addUserWithoutContactIdTest() {
//		Account testAccount = new Account(Name='testAccount',RecordTypeId='0124000000011Q2');
//		insert testAccount;
//		Profile testProfile = [SELECT Id FROM Profile WHERE Name='Standard User'];
//		User testUser = new User(Alias = 'standt2',
//			Email='standarduser2@testorg.com',
//			EmailEncodingKey='UTF-8',
//			LastName='Testing2',
//			LanguageLocaleKey='en_US', 
//			LocaleSidKey='en_US',
//			ProfileId = testProfile.Id,
//			TimeZoneSidKey='America/Los_Angeles',
//			UserName='randomName@sandbox.org');
//		insert testUser;
//		
//		system.assertEquals(null, testUser.ContactId, 'Null contact reference');
//	}
//	
//	static testMethod void bulkUserTest() {
//		List<Contact> contacts = new List<Contact>();
//		List<User> users = new List<User>();
//		Profile testProfile = [SELECT Id FROM Profile WHERE Name='Partner Community - Vendors'];
//		Account testAccount = new Account(Name='testAccount',RecordTypeId='0124000000011Q2');
//		insert testAccount;
//		
//		for(Integer i = 0; i < 200; i++) {
//			Contact con = new Contact(LastName='test' + i, AccountId=testAccount.Id, RecordTypeId = '01240000000UPSr');
//						con.AccountId = testAccount.id;
//	//Caps - Number of orders that can be assigned at once (open + all completed this week)
//	con.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//	con.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//	con.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//	con.Shrub_Trim__c = 3;//Pricing...required 
//	con.REO_Grass_Cut__c = 3;//Pricing
//	con.Pay_Terms__c = 'Net 30';//Terms required
//	con.Trip_Charge__c = 25;//Pricing
//	con.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//	con.Date_Active__c = Date.Today();//Date Active must be set. 
//	con.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//	con.MailingStreet = '30 Turnberry Dr';
//	con.MailingCity = 'La Place';
//	con.MailingState = 'LA';
//	con.MailingPostalCode = '70068';
//	con.OtherStreet = '30 Turnberry Dr';
//	con.OtherCity = 'La Place';
//	con.OtherState = 'LA';
//	con.OtherPostalCode = '70068';
//	con.Other_Address_Geocode__Latitude__s = 30.00;
//	con.Other_Address_Geocode__Longitude__s = -90.00;
//			contacts.add(con);
//		}
//		insert contacts;
//		Integer counter = 0;
//		for(Contact currentContact : contacts) {
//			User testUser = new User(Alias = 'A' + counter,
//				ContactId = currentContact.Id,
//				Email='standarduser@testorg.com' + counter,
//				EmailEncodingKey='UTF-8',
//				LastName='Testing' + counter,
//				LanguageLocaleKey='en_US', 
//				LocaleSidKey='en_US',
//				ProfileId = testProfile.Id,
//				TimeZoneSidKey='America/Los_Angeles',
//				UserName='standarduser@testorg.com' + counter);
//			users.add(testUser);
//			counter++;
//		}
//		test.startTest();
//		insert users;
//		test.stopTest();
//		
//		Integer contactCount = [SELECT Count() FROM Contact WHERE Portal_User_Id__c != null];
//		system.assertEquals(200, contactCount, 'Number of Contact records');
//	}
}