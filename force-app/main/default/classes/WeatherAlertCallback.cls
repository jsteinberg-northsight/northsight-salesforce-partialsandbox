@RestResource(urlMapping='/WeatherAlertCallback')
global without sharing class WeatherAlertCallback {
	@HttpPost
    global static void doPost() {
    	string assetUrl = 'https://northsight.v-alert.co/api/assets';
    	map<string,object> creds = WeatherAssetHelper.login();
    	//RestRequest instance  
        RestRequest req = RestContext.request;
        //Getting request body as String
        String alertData = req.requestBody.ToString();
               
        //Deserialize the input payload Summary Data String and get Root map 
        Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(alertData);
        //fetch batch id
        string batchId = (string)root.get('batchId');
        
         //call out to API to get valert property Ids
        Http h = new Http();
		HttpRequest r = new HttpRequest();
		r.setEndpoint(assetUrl+'?batchId='+batchId);
		r.setMethod('GET');
		r.setHeader('Content-Type','application/json');
		r.setHeader('Accept','application/json');
		r.setHeader('X-Auth-Token',(string)creds.get('authToken'));
		r.setHeader('X-User-Id',(string)creds.get('userId'));
		
		r.setTimeout(120000);
		HttpResponse res = h.send(r);
		string response = res.getBody();
		try{
        map<string,object> data = (map<string,object>)JSON.deserializeUntyped(response);
        object[] assets = (object[])data.get('assets');
  
        map<string,string> propIdMap = new map<string,string>();
        for(object o:assets){
        	map<string,object> asset = (map<string,object>) o; 
        	propIdMap.put((string)asset.get('PropertyNumber'),(string)asset.get('id'));
        }
       Geocode_Cache__c[] updateProps = [select Id,Name, VAlert_Id__c from Geocode_Cache__c where Name in : propIdMap.Keyset()];
        for(Geocode_Cache__c p:updateProps){
        	p.VAlert_Id__c = propIdMap.get(p.Name);
        }
        upsert updateProps;
        RestResponse restRes = RestContext.response;
		restRes.responseBody  = Blob.valueOf(response);
		}catch(Exception e){
    	RestResponse restRes = RestContext.response;
		restRes.responseBody  = Blob.valueOf(e.getMessage());
		}
		
    }

}