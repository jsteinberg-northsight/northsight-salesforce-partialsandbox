/**
 *  Description     :   This is controller for AssignmentMapSearchJSON page.
 *
 *  Created By      :   
 *
 *  Created Date    :   02/18/2014
 *
 *  Current Version :   V1.5
 *
 *  Revisiion Logs  :   V1.0 - Created
 *                      V1.1 - Modified - Padmesh Soni (02/21/2014) - Code added for enhanced the functionality and convert the static query into 
 *                                          dynamic query. - Northsight Work Order Assignment Map
 *                      V1.2 - Modified - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes - Enhancement of functionality on OAM project.
 *                      V1.3 - Modified - Padmesh Soni (05/08/2014) - 140503 OAM Date Widgets (1) - Enhancement of functionality on OAM project.
 *                                          Changes are:
 *                                              1. OAM-40: Better Date Filter UI (Full specifications on all date filters.)
 *                      V1.4 - Modified - Padmesh Soni (05/30/2014) - OAM Tasks 05/28/2014
 *                                           Changes are:
 *                                              1.Additional Task - When we open up the OAM, by default, no "W.O. Types" should be selected. 
 *                                                 A work order without a type cannot be assigned and should not be mapped. The test data is in error.
 *                      V1.5 - Modified - Padmesh Soni (06/19/2014) - Support Case Record Type
 *                                           Changes are: New condition and query filter added of new RecordType excluding of Case object to process.
 *
 **/
public with sharing class AssignmentMapSearchDaemon {
    
    //Class properties 
    private set<string> filterClients = new set<string>();
    private set<string> workOrderTypes = new set<string>();
    private set<string> filterStates = new set<string>();
    private set<String> workOrderedParam = new Set<String>();
    private set<string> vendorSupportTeamParam = new Set<String>();
    private set<String> filterDepartment = new Set<String>();
    private set<String> filterAssignProg = new Set<String>();
    private boolean showLateOrders = true;
    private boolean showDueOrders = true;
    private boolean showEarlyOrders = true;
    private boolean showNoRoutingOrders;
    private double minLat = 0.0;
    private double minLon = 0.0;
    private double maxLat = 0.0;
    private double maxLon = 0.0;
    private String isUA;
    private Date dueStartDate;
    private Date dueStopDate;
    private Date scheduleStartDate;
    private Date scheduleStopDate;
    private String createdStartDate;
    private String createdStopDate;
    private String assignedStartDate;
    private String assignedStopDate;
    private boolean isStopColor;
    
    public string outputJSON {get; private set;}
    
    //Padmesh Soni (06/19/2014) - Support Case Record Type
    //Variable to hold list of RecordTypes of Case object
    List<RecordType> recordTypes;
    String idInClause;
    
    //Constructor definition
    public AssignmentMapSearchDaemon(){
        
        isStopColor = false;
        
        //Padmesh Soni (06/19/2014) - Support Case Record Type
        //Query record of Record Type of Case object
        recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
                                            AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
        
        //Padmesh Soni (06/19/2014) - Support Case Record Type
        //initialize string
        idInClause = '(\'';
        
        //Padmesh Soni (06/19/2014) - Support Case Record Type
        //Loop through RecordTypes query result
        for (RecordType recordType : recordTypes){
        
            //populate Ids in clause variable
            idInClause += recordType.Id + '\',\'';
        }
        
        //Padmesh Soni (06/19/2014) - Support Case Record Type
        //Format string with removing last comma
        idInClause  = idInClause.substring(0,idInClause.length()-2);
        idInClause += ')';
    }
    
    /**
     *  @descpriton     :   This method is used to generate JSON string for particular URL parameters.
     *
     *  @param          :   
     *
     *  @return         :   void 
     **/
    public void doSearch(){
        
        //Instance of PageReference 
        PageReference p = ApexPages.currentPage();
        
        //Map to hold all parameters from Page URL
        Map<string, string> params = p.getParameters();
        
        //Variable to hold Dynamic SOQL string
        String SOQL = '';
        
        //Create SOQL string for dynamic quering records
        //OAM-44 Change Assignment Program - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes
        //Add "Assignment_Program__c" into SOQL
        SOQL = 'SELECT Id, CaseNumber, OwnerId, Owner.Name, Owner.FirstName, Owner.LastName, Owner.Alias, Geocode_cache__r.Formatted_Address__c, Due_Date_Stop_Color__c, stop_color__c, Work_Ordered__c,'
                    + ' Geocode_Cache__r.Location__Latitude__s, Geocode_Cache__r.Location__Longitude__s, Serviceable__c, Routing_Priority__c, Due_Date__c, Assigned_Date_Time__c,' 
                    + ' Scheduled_Date__c, Status, Client_Number__c, Client__c, notes__c, Description, Geocode_Cache__r.notes__c, Work_Order_Type__c, Assignment_Program__c'
                    + ' FROM Case WHERE RecordTypeId NOT IN '+ idInClause + ' AND Serviceable__c = \'Yes\' ';
        
        String aggregateSOQL = SOQL.replace(SOQL.subString(0, SOQL.indexOf('FROM')), 'SELECT Count(Id) total, Work_Ordered__c wOrdered ');
        
        /***** MAP EXTENTS *****/
        try {
            
            //Getting minimum latitude from URL parameter minLat
            minLat = double.valueOf(params.get(AssignmentMapConstants.URL_PARAM_MINLAT));
            
            //add filter into query String 
            SOQL += ' AND Geocode_Cache__r.Location__Latitude__s >=' + minLat;
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        try {
            
            //Getting minimum latitude from URL parameter minLon
            minLon = double.valueOf(params.get(AssignmentMapConstants.URL_PARAM_MINLON));
            
            //add filter into query String
            SOQL += ' AND Geocode_Cache__r.Location__Longitude__s >=' + minLon;
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        try {
            
            //Getting minimum latitude from URL parameter maxLat
            maxLat = double.valueOf(params.get(AssignmentMapConstants.URL_PARAM_MAXLAT));
            
            //add filter into query String
            SOQL += ' AND Geocode_Cache__r.Location__Latitude__s <=' + maxLat;
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        try {
            
            //Getting minimum latitude from URL parameter maxLon
            maxLon = double.valueOf(params.get(AssignmentMapConstants.URL_PARAM_MAXLON));
            
            //add filter into query String
            SOQL += ' AND Geocode_Cache__r.Location__Longitude__s <=' + maxLon;
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        //Getting minimum latitude from URL parameter maxLon
        String stopColorOption = String.valueOf(params.get(AssignmentMapConstants.URL_PARAM_STOP_COLOR));
        
        if(stopColorOption == 'Due Date') {
            
            //assigning false
            isStopColor = AssignmentMapConstants.BOOL_OFF;
        } else if(stopColorOption == 'Schedule Date') {
            
            //assigning true
            isStopColor = AssignmentMapConstants.BOOL_ON;
        }
        
        /****** UA Filters ******/ 
        try {
            
            //Getting UA picklist value from URL parameter UA
            isUA = String.valueOf(params.get(AssignmentMapConstants.CASE_QUEUE_NAME_UA));
            
            if(isUA == 'only') {
                
                //add filter into query String
                SOQL += ' AND Owner.Name = \'UA\' ';
                aggregateSOQL += ' AND Owner.Name = \'UA\' ';
            } else if(isUA == 'no') {
                
                //add filter into query String
                SOQL += ' AND Owner.Name != \'UA\' ';
                aggregateSOQL += ' AND Owner.Name != \'UA\' ';
            } else if(isUA == 'yes') {
                
            }
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        /***** State Filters *****/
        try {
            
            if(params.get(AssignmentMapConstants.URL_PARAM_TOGGLESTATE) == 'true') {
            
                //Check for null of filter states param values    
                if(params.get(AssignmentMapConstants.URL_PARAM_STATEPARAM) != '') {
                    
                    //Populate the set of State values for Work Order
                    filterStates = new set<string>(params.get(AssignmentMapConstants.URL_PARAM_STATEPARAM).split(';'));
                    
                    //Check if set is not empty and then add filter into query String  
                    if(filterStates.size() > 0) {
                        
                        //String to hold state
                        String state = '';
                        
                        for(String str : filterStates)
                            state += '\'' + str + '\',';
                
                        state = state.lastIndexOf(',') > 0 ? '(' + state.substring(0,state.lastIndexOf(',')) + ')' : state ;
                                
                        //Dynamic SOQL adding filter String 
                        SOQL += ' AND State__c IN ' + state;
                        aggregateSOQL += ' AND State__c IN ' + state;
                    }
                }
            }
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        /***** Client Filters *****/
        try {
            
            //Check for null of filterClients param values
            if(params.get(AssignmentMapConstants.URL_PARAM_FILTERCLIENTS) != '') {
                
                //Populate the set of Client values for Work Order
                filterClients = new set<string>(params.get(AssignmentMapConstants.URL_PARAM_FILTERCLIENTS).split(';'));
                
                //Check if set is not empty and then add filter into query String
                if(filterClients.size() > 0) {
                    
                    //String to hold client types
                    String client = '';
                    
                    for(String str : filterClients)
                        client += '\'' + str + '\',';
            
                    client = client.lastIndexOf(',') > 0 ? '(' + client.substring(0,client.lastIndexOf(',')) + ')' : client ;
                            
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Client__c IN ' + client;
                    aggregateSOQL += ' AND Client__c IN ' + client;
                }   
            }
                
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        /***** Work Order Type Filters *****/
        //try {
            
            //System.debug('W.O. Types ::::::'+ params.get(AssignmentMapConstants.URL_PARAM_WORKORDERTYPES));
            //Check for null of filterClients param values
            workOrderTypes = new set<string>();
            if(params.get(AssignmentMapConstants.URL_PARAM_WORKORDERTYPES) != '') {
                
                //Populate the set of Client values for Work Order
                workOrderTypes = new set<string>(params.get(AssignmentMapConstants.URL_PARAM_WORKORDERTYPES).split(';'));
            }
                //System.debug('W.O. Types ::::::'+ workOrderTypes);
            
                //Check if set is not empty and then add filter into query String     
                if(workOrderTypes.size() > 0) {
                    
                    String orderType = '';
                    
                    for(String str : workOrderTypes)
                        orderType += '\'' + str + '\',';
            
                    orderType = orderType.lastIndexOf(',') > 0 ? '(' + orderType.substring(0,orderType.lastIndexOf(',')) + ')' : orderType ;
                    
                    //Dynamic SOQL adding filter String 
                    //Padmesh Soni (05/30/2014) - Additional Task - When we open up the OAM, by default, no "W.O. Types" should be selected. 
                    SOQL += ' AND Work_Order_Type__c IN ' + orderType + ' AND (Work_Order_Type__c != null OR Work_Order_Type__c != \'\')';
                    aggregateSOQL += ' AND Work_Order_Type__c IN ' + orderType;
                } else {
                    
                    //Padmesh Soni (05/30/2014) - Additional Task - When we open up the OAM, by default, no "W.O. Types" should be selected. 
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Work_Order_Type__c = null';
                    aggregateSOQL += ' AND Work_Order_Type__c = null';
                }
            
      //  } catch(Exception ex) {
            
        //     ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
          //   ApexPages.addMessage(errorMsg);
        //}
    
        /***** Department Filters *****/
        try {
            
            //Check for null of filterClients param values
            if(params.get(AssignmentMapConstants.URL_PARAM_DEPARTMENT) != '') {
                
                //Populate the set of Client values for Work Order
                filterDepartment = new set<string>(params.get(AssignmentMapConstants.URL_PARAM_DEPARTMENT).split(';'));
                
                //Check if set is not empty and then add filter into query String
                if(filterDepartment.size() > 0) {
                    
                    //String to hold client types
                    String department = '';
                    
                    for(String str : filterDepartment)
                        department += '\'' + str + '\',';
            
                    department = department.lastIndexOf(',') > 0 ? '(' + department.substring(0,department.lastIndexOf(',')) + ')' : department ;
                            
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Department__c IN ' + department;
                    aggregateSOQL += ' AND Department__c IN ' + department;
                }   
            }   
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        /***** Assignment Program Filters *****/
        try {
            
            //Check for null of filterClients param values
            if(params.get(AssignmentMapConstants.URL_PARAM_FILTERASSIGNPROG) != '') {
                
                //Populate the set of Client values for Work Order
                filterAssignProg = new set<string>(params.get(AssignmentMapConstants.URL_PARAM_FILTERASSIGNPROG).split(';'));
                
                //Check if set is not empty and then add filter into query String
                if(filterAssignProg.size() > 0) {
                    
                    //String to hold client types
                    String assignProg = '';
                    
                    for(String str : filterAssignProg)
                        assignProg += '\'' + str + '\',';
            
                    assignProg = assignProg.lastIndexOf(',') > 0 ? '(' + assignProg.substring(0,assignProg.lastIndexOf(',')) + ')' : assignProg ;
                            
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Assignment_Program__c IN ' + assignProg;
                    aggregateSOQL += ' AND Assignment_Program__c IN ' + assignProg;
                }   
            }   
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        /***** Due Date Filters *****/
        try {
            
            //Check for null of dueStartDate param values
            if(params.get(AssignmentMapConstants.URL_PARAM_DUESTARTDATE) != '') {
                
                //OAM-40: Better Date Filter UI - Padmesh Soni (05/08/2014) - 140503 OAM Date Widgets (1)
                //Checking for different param values on Due Date criteria line of code added 362 -380
                String dueStartDateParm = params.get(AssignmentMapConstants.URL_PARAM_DUESTARTDATE);
                
                if(dueStartDateParm == AssignmentMapConstants.LATE) {
                    
                    //Dynamic SOQL adding filter String
                    SOQL += ' AND Due_Date__c < TODAY';
                    aggregateSOQL += ' AND Due_Date__c < TODAY';
                } else if(dueStartDateParm == AssignmentMapConstants.TODAYS) {
                    
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Due_Date__c = TODAY';
                    aggregateSOQL += ' AND Due_Date__c = TODAY';
                } else if(dueStartDateParm == AssignmentMapConstants.TODAYLATE) {
                    
                    //Dynamic SOQL adding filter String
                    SOQL += ' AND Due_Date__c <= TODAY';
                    aggregateSOQL += ' AND Due_Date__c <= TODAY';
                } else if(dueStartDateParm == AssignmentMapConstants.LAST2DAYS) {
                    
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Due_Date__c = LAST_N_DAYS:'+ 2;
                    aggregateSOQL += ' AND Due_Date__c = LAST_N_DAYS:'+ 2;
                } else {
                
                    //Populate the dueStartDate range
                    dueStartDate = Date.parse(String.valueOf(params.get(AssignmentMapConstants.URL_PARAM_DUESTARTDATE)));
                    
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Due_Date__c >= '+ String.valueOf(dueStartDate);
                    aggregateSOQL += ' AND Due_Date__c >= '+ String.valueOf(dueStartDate);
                }
            }   
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        try {
                    
            //Check for null of dueStartDate param values
            if(params.get(AssignmentMapConstants.URL_PARAM_DUESTOPDATE) != '') {
                
                //Populate the dueStartDate range
                dueStopDate = Date.parse(String.valueOf(params.get(AssignmentMapConstants.URL_PARAM_DUESTOPDATE)));
                
                //Dynamic SOQL adding filter String 
                SOQL += ' AND Due_Date__c <= '+ String.valueOf(dueStopDate);
                aggregateSOQL += ' AND Due_Date__c <= '+ String.valueOf(dueStopDate);
            }   
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        try {
            
            //Check for null of dueStartDate param values
            if(params.get(AssignmentMapConstants.URL_PARAM_SCHEDULESTARTDATE) != '') {
                
                //OAM-40: Better Date Filter UI - Padmesh Soni (05/08/2014) - 140503 OAM Date Widgets (1)
                //Checking for different param values on Schedule Date criteria line of code added 419 - 438
                //populate a string to hold URL param value
                String schedStartDateParm = params.get(AssignmentMapConstants.URL_PARAM_SCHEDULESTARTDATE);
                
                //Check for Url parameter values
                if(schedStartDateParm == AssignmentMapConstants.LATE) {
                    
                    //Dynamic SOQL adding filter String
                    SOQL += ' AND Scheduled_Date__c < TODAY';
                    aggregateSOQL += ' AND Scheduled_Date__c < TODAY';
                } else if(schedStartDateParm == AssignmentMapConstants.TODAYS) {
                    
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Scheduled_Date__c = TODAY';
                    aggregateSOQL += ' AND Scheduled_Date__c = TODAY';
                } else if(schedStartDateParm == AssignmentMapConstants.TODAYLATE) {
                    
                    //Dynamic SOQL adding filter String
                    SOQL += ' AND Scheduled_Date__c <= TODAY';
                    aggregateSOQL += ' AND Scheduled_Date__c <= TODAY';
                } else if(schedStartDateParm == AssignmentMapConstants.CURRENTSRVC) {
                    
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Routing_Serviceable__c = \'Yes\' ';
                    aggregateSOQL += ' AND Routing_Serviceable__c = \'Yes\' ';
                } else { 
                
                    //Populate the dueStartDate range
                    scheduleStartDate = Date.parse(String.valueOf(params.get(AssignmentMapConstants.URL_PARAM_SCHEDULESTARTDATE)));
                    
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Scheduled_Date__c >= '+ String.valueOf(scheduleStartDate);
                    aggregateSOQL += ' AND Scheduled_Date__c >= '+ String.valueOf(scheduleStartDate);
                }
            }
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        try {
                
            //Check for null of dueStartDate param values
            if(params.get(AssignmentMapConstants.URL_PARAM_SCHEDULESTOPDATE) != '') {
                
                //Populate the dueStartDate range
                scheduleStopDate = Date.parse(String.valueOf(params.get(AssignmentMapConstants.URL_PARAM_SCHEDULESTOPDATE)));
                
                //Dynamic SOQL adding filter String 
                SOQL += ' AND Scheduled_Date__c <= '+ String.valueOf(scheduleStopDate);
                aggregateSOQL += ' AND Scheduled_Date__c <= '+ String.valueOf(scheduleStopDate);
            }
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        string suffix = 'T00:00:00-05:00';
        
        try {
            
            //Check for null of createdStartDate param values   
            if(params.get(AssignmentMapConstants.URL_PARAM_CREATEDSTARTDATE) != '') {
                
                //OAM-40: Better Date Filter UI - Padmesh Soni (05/08/2014) - 140503 OAM Date Widgets (1)
                //Checking for different param values on Opened Date criteria line of code added 481 - 487
                //Populate the createdStartDateParam string
                String createdStartDateParam = params.get(AssignmentMapConstants.URL_PARAM_CREATEDSTARTDATE);
                
                if(createdStartDateParam == AssignmentMapConstants.TODAYS) {
                    
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND CreatedDate = TODAY';
                    aggregateSOQL += ' AND CreatedDate = TODAY';
                } else {
                    
                    //Populate the createdStartDate range
                    createdStartDate = DateTime.parse(String.valueOf(params.get(AssignmentMapConstants.URL_PARAM_CREATEDSTARTDATE))).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
                    
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND CreatedDate >= '+ createdStartDate;
                    aggregateSOQL += ' AND CreatedDate >= '+ createdStartDate;
                }
            }   
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        try {
            
            //Check for null of createdStopDate param values
            if(params.get(AssignmentMapConstants.URL_PARAM_CREATEDSTOPDATE) != '') {
                
                //Populate the createdStopDate range
                createdStopDate = DateTime.parse(String.valueOf(params.get(AssignmentMapConstants.URL_PARAM_CREATEDSTOPDATE))).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
                
                //Dynamic SOQL adding filter String 
                SOQL += ' AND CreatedDate <= '+ createdStopDate;
                aggregateSOQL += ' AND CreatedDate <= '+ createdStopDate;
            }   
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        try {
            
            //Check for null of assignedStartDate param values   
            if(params.get(AssignmentMapConstants.URL_PARAM_ASSIGNEDSTARTDATE) != '') {
                
                //OAM-40: Better Date Filter UI - Padmesh Soni (05/08/2014) - 140503 OAM Date Widgets (1)
                //Checking for different param values on Assigned Date criteria line of code added 526 - 534
                //Populate the createdStartDateParam string
                String AssignedDateParam = params.get(AssignmentMapConstants.URL_PARAM_ASSIGNEDSTARTDATE);
                
                //Check if string is equal to Today
                if(AssignedDateParam == AssignmentMapConstants.TODAYS) {
                    
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Assigned_Date_Time__c = TODAY';
                    aggregateSOQL += ' AND Assigned_Date_Time__c = TODAY';
                } else {
                    
                    //Populate the assignedStartDate range
                    assignedStartDate = DateTime.parse(String.valueOf(params.get(AssignmentMapConstants.URL_PARAM_ASSIGNEDSTARTDATE))).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
                    
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Assigned_Date_Time__c >= '+ assignedStartDate;
                    aggregateSOQL += ' AND Assigned_Date_Time__c >= '+ assignedStartDate;
                }
            }   
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        try {
            
            //Check for null of dueStartDate param values
            if(params.get(AssignmentMapConstants.URL_PARAM_ASSIGNEDSTOPDATE) != '') {
                
                //Populate the dueStartDate range
                assignedStopDate = DateTime.parse(String.valueOf(params.get(AssignmentMapConstants.URL_PARAM_ASSIGNEDSTOPDATE))).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
                
                //Dynamic SOQL adding filter String 
                SOQL += ' AND Assigned_Date_Time__c <= '+ assignedStopDate;
                aggregateSOQL += ' AND Assigned_Date_Time__c <= '+ assignedStopDate;
            }   
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        //OAM-48: Self Assigned filter - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes
        /***** Self Assigned Filters *****/
        try {
            
            if(params.get(AssignmentMapConstants.URL_PARAM_FILTERSELFASSIGN) != 'both') {
            
                //Check for null of filter states param values
                if(params.get(AssignmentMapConstants.URL_PARAM_FILTERSELFASSIGN) != '') {
                    
                    //Populate the set of State values for Work Order
                    String isSelfAssign = params.get(AssignmentMapConstants.URL_PARAM_FILTERSELFASSIGN);
                    
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Self_Assigned__c = ' + isSelfAssign;
                    aggregateSOQL += ' AND Self_Assigned__c = ' + isSelfAssign;
                }
            }
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        aggregateSOQL += ' GROUP BY Work_Ordered__c';
        
        /***** Work Ordered Filters *****/
        try {
            
            //Check for null of filterClients param values
            if(params.get(AssignmentMapConstants.URL_PARAM_WRKORDEREDTYPE) != '') {
                
                //Populate the set of Client values for Work Order
                workOrderedParam = new set<string>(params.get(AssignmentMapConstants.URL_PARAM_WRKORDEREDTYPE).split(';'));
                
                //Check if set is not empty and then add filter into query String
                if(workOrderedParam.size() > 0) {
                    
                    //String to hold client types
                    String workOrdered = '';
                    
                    for(String str : workOrderedParam)
                        workOrdered += '\'' + str + '\',';
            
                    workOrdered = workOrdered.lastIndexOf(',') > 0 ? '(' + workOrdered.substring(0,workOrdered.lastIndexOf(',')) + ')' : workOrdered ;
                            
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Work_Ordered__c IN ' + workOrdered;
                }   
            }
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        //-----------------------------------------------------------------------------------------------------------
        //Vendor Support Team Filter
                try {
            
            //Check for null of filterClients param values
            if(params.get(AssignmentMapConstants.URL_PARAM_VENDORSUPPORTTEAM) != '') {
                
                //Populate the set of vendor support team values for Work Order
                vendorSupportTeamParam = new set<string>(params.get(AssignmentMapConstants.URL_PARAM_VENDORSUPPORTTEAM).split(';'));
                
                //Check if set is not empty and then add filter into query String
                if(vendorSupportTeamParam.size() > 0) {
                    
                    //String to hold client types
                    String vsp = '';
                    
                    for(String str : vendorSupportTeamParam)
                        vsp += '\'' + str + '\',';
            
                    vsp = vsp.lastIndexOf(',') > 0 ? '(' + vsp.substring(0,vsp.lastIndexOf(',')) + ')' : vsp ;
                            
                    //Dynamic SOQL adding filter String 
                    SOQL += ' AND Vendor_Support_Team__c IN ' + vsp;
                }   
            }
        } catch(Exception ex) {
            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
        }
            
        //Initialize list of OutputWrapper instance 
        List<outputWrapper> outPut = new List<outputWrapper>();
        
        //System.debug('SOQL:::'+ SOQL);
        
        //Removing leading ane trailing spaces
        SOQL = SOQL.trim();
        
        aggregateSOQL = aggregateSOQL.trim();
        
        String elementJSON = '';
        
        for(AggregateResult aggregatedWO : Database.query(aggregateSOQL)) {
            
            String ordered = (String) aggregatedWO.get('wOrdered');
            Integer noOfOrderd = (Integer) aggregatedWO.get('total');
            elementJSON += ordered +' : '+ noOfOrderd +', ';
        }
        if(elementJSON != '') {
            elementJSON = elementJSON.subString(0, elementJSON.lastIndexOf(','));
        }
        
        //System.debug('elementJSON :::'+ elementJSON);
        
        integer processCounter = 0;
        
        //Loop through queried records of Work Orders
        for(Case workOrder : Database.query(SOQL)) {
            
            //Wrapper class instancce
            OutputWrapper ow = new OutputWrapper();
            ow.Id = workOrder.Id;
            ow.ownerId = workOrder.OwnerId;
            ow.WON = workOrder.CaseNumber;
            ow.lat = workOrder.Geocode_Cache__r.Location__Latitude__s;
            ow.lon = workOrder.Geocode_Cache__r.Location__Longitude__s;
            ow.client = workOrder.Client__c;
            
            //Check if Work Order's Owner is User Sobject Type
            if(workOrder.ownerId.getSObjectType().getDescribe().getName() == AssignmentMapConstants.SOBJECT_NAME_USER) {
                
                //Getting initial letter of Owner's FirstName and LastName
                ow.ownerInitials = (workOrder.Owner.FirstName != null? workOrder.Owner.FirstName.subString(0, 1): '') + workOrder.Owner.LastName.subString(0, 1);
            }
            
            if(processCounter == 0) {
                ow.WorkOrderedPicklist = elementJSON;
                processCounter++;
            }
            
            //Check for Stop Color options
            if(isStopColor)
                ow.color = workOrder.stop_color__c;
            else
                ow.color = workOrder.Due_Date_Stop_Color__c;
                
            ow.ownerName = workOrder.Owner.Name;
            ow.ownerAlias = workOrder.Owner.Alias;
            ow.workOrdered = workOrder.Work_Ordered__c;
            
            //Padmesh Soni (05/30/2014) - Additional Task - When we open up the OAM, by default, no "W.O. Types" should be selected.
            //Assigning new property with Work Order Type to Wrapper instance
            ow.workOrderType = workOrder.Work_Order_Type__c;
            
            outPut.add(ow);
        }
        outputJSON = JSON.serialize(outPut);
    }
    
    //Wrapper class
    private class OutputWrapper{
        
        public double lat;
        public double lon;
        public string WON;
        public string Id;
        public string ownerId;
        public string ownerAlias;
        public String ownerName;
        public string client;
        public string color;
        public String ownerInitials;
        public String workOrdered;
        public String WorkOrderedPicklist;
        
        //Padmesh Soni (05/30/2014) - Additional Task - When we open up the OAM, by default, no "W.O. Types" should be selected.
        //Create a new property to store Work Order Type field value 
        public String workOrderType;
    }
    
	
		public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
	}
}