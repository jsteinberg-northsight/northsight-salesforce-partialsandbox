@isTest()
private class BatchGeocodeWorkOrders_Test {
///*
//    static testMethod void testBatchClass() {
//        
//    /*******************************************************************************************************************/
//    /*
//    //create a new client zone test obj
//	ClientZone__c cz = new ClientZone__c();
//	cz.Client_Name__c = 'SG';
//	cz.Order_Type__c = 'Routine';
//	insert cz;
//	//create new client zip code test objs
//	ClientZipCode__c[] zcList = new ClientZipCode__c[] {};
//	
//	zcList.add(new ClientZipCode__c(
//		ClientZone__c = cz.Id,
//		Zip_Code_Value__c = '78704'
//	));
//	
//	zcList.add(new ClientZipCode__c(
//		ClientZone__c = cz.Id,
//		Zip_Code_Value__c = '60562'
//	));
//	
//	insert zcList;
//    /*******************************************************************************************************************/
//    /*
//        case[] testcases = new case[] {};
//        
//        testcases.add(new case(
//        						street_address__c = '1234 Street',
//        						state__c = 'TX',
//        						city__c = 'Austin',
//        						zip_code__c = '78704',
//        						Vendor_code__c = 'SRSRSR',
//        						Work_Order_Type__c = 'Routine',
//        						Client__c = 'SG'
//        ));
//        
//        // Test case where the same address is already processed
//        testcases.add(new case(
//        						street_address__c = '1234 Street',
//        						state__c = 'TX',
//        						city__c = 'Austin',
//        						zip_code__c = '78704',
//        						Vendor_code__c = 'SRSRSR',
//        						Work_Order_Type__c = 'Routine',
//        						Client__c = 'SG'
//        ));
//        
//        // test case where address is already in the geocode table
//        case ctest = new case(
//        						street_address__c = '400 Cedar st',
//        						state__c = 'CT',
//        						city__c = 'Bridgeport',
//        						zip_code__c = '60562',
//        						Vendor_code__c = 'TRYWZYM',
//        						Work_Order_Type__c = 'Routine',
//        						Client__c = 'SG'
//        );
//        testcases.add(ctest);
//        // Test case where the address is partial
//        testcases.add(new case(
//        						street_address__c = '515 partial st',
//        						state__c = 'CT',
//        						city__c = 'Bridgeport',
//        						zip_code__c = '60562',
//        						Vendor_code__c = 'TRYWZYM',
//        						Work_Order_Type__c = 'Routine',
//        						Client__c = 'SG'
//        ));
//        
//        
//        insert testcases;
//        
//        //create a map of string to id and insert the client zip codes into the map as Map<Zip_Code_Value__c, ClientZipCode__c>
//        Map<string, Id> czcMap = new Map<string, Id>();         
//        for(ClientZipCode__c czc : [SELECT Id, Zip_Code_Value__c FROM ClientZipCode__c WHERE Id IN: zcList]){
//        	czcMap.put(czc.Zip_Code_Value__c, czc.Id);
//        }
//        
//        //create a list for the test cases
//        List<Case> updateTestCases = new List<Case>();
//        for(Case c : [SELECT Id, ClientZipCode__c, Zip_Code__c FROM Case WHERE Id IN: testcases]){
//        	//check the map if there is a matching zip code btw a case and a client zip code
//        	if(czcMap.containsKey(c.Zip_Code__c)){
//        		//add the clientzipcode to the case
//        		c.ClientZipCode__c = czcMap.get(c.Zip_Code__c);
//        		system.assert(c.ClientZipCode__c != null);
//        		updateTestCases.add(c);
//        	}
//        }
//        update updateTestCases;
//        
//        // For testing if an address is already geocoded
//        geocode_cache__c testgeo = new geocode_cache__c(
//        						address_hash__c='400cedarstbrdgprtct60562',
//        						quarantined__c=false,
//        						location__latitude__s = 11.11111111,
//        						location__longitude__s = -111.22222222
//        						);
//        						
//        insert testgeo;
//        
//     
//        Test.StartTest();
//        BatchGeocodeWorkOrders testbatch = new BatchGeocodeWorkOrders();
//        database.executeBatch(testbatch, 10);
//        test.stoptest();
//        
//        system.assertEquals('400cedarstbrdgprtct60562',[select address_hash__c from Geocode_Cache__c where Id in (select geocode_cache__c from Case where Id = :ctest.Id)].Address_Hash__c);
//        // Analyze result
//        geocode_cache__c[] resultgeo = [select address_hash__c, quarantined__c, partial_match__c, location__latitude__s, location__longitude__s from geocode_cache__c];
//        
//        system.debug('*****'+resultgeo+'*****');
//        
//        // Should have 3 geocode cache records 
//        system.assertEquals(3, resultgeo.size());
//        
//        for(geocode_cache__c geo : resultgeo) {
//        	
//        	if(geo.address_hash__c == '515partialstbrdgprtct60562') {
//        		system.assert(geo.quarantined__c==true);
//        		system.assert(geo.partial_match__c==true);
//        	} else {
//        		system.assert(geo.quarantined__c==false);
//        		system.assert(geo.location__latitude__s != null);
//        		system.assert(geo.location__longitude__s != null);
//        		system.assert(geo.location__longitude__s != geo.location__latitude__s);		
//        	}
//        }
//    }*/
}