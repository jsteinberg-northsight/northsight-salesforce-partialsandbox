@isTest
private class InspectionReportControllerTest {
//	private static Id accountId;
//
//	@TestSetup
//	private static void createData() {
//		RecordType clientType = [SELECT Id FROM RecordType WHERE sobjectType = 'Account' AND DeveloperName = 'Client'];
//
//		Account testAccount = new Account(
//			Name = 'test',
//			RecordTypeId = clientType.Id
//		);
//		insert testAccount;
//		accountId = testAccount.Id;
//
//		Case currentInspection = new Case(
//			Date_Serviced__c = Date.today(),
//			Work_Ordered__c = 'Occupancy Inspection',
//			Work_Completed__c = 'Yes',
//			Street_Address__c = 'address',
//			City__c = 'city',
//			State__c = 'state',
//			Zip_Code__c = 'code',
//			Vendor_Code__c = 'AA',
//			Loan__c = 'loan'
//		);
//		insert currentInspection;
//
//		Case priorInspection = new Case(
//			Date_Serviced__c = Date.today().addDays(-1),
//			Work_Ordered__c = 'Occupancy Inspection',
//			Work_Completed__c = 'Yes',
//			Street_Address__c = 'address',
//			City__c = 'city',
//			State__c = 'state',
//			Zip_Code__c = 'code',
//			Vendor_Code__c = 'AA',
//			Loan__c = 'loan'
//		);
//		insert priorInspection;
//
//		Geocode_Cache__c property = [SELECT Id, Zip_Code__c, Client_Name__c FROM Geocode_Cache__c];
//		property.Client_Name__c = testAccount.Id;
//		update property;
//
//		currentInspection.Geocode_Cache__c = property.Id;
//		currentInspection.Client_Name__c = testAccount.Id;
//		update currentInspection;
//		priorInspection.Geocode_Cache__c = property.Id;
//		priorInspection.Client_Name__c = testAccount.Id;
//		update priorInspection;
//	}
//
//	@isTest
//	private static void no_parameter_error_test() {
//		Test.setCurrentPage(Page.InspectionReport);
//		InspectionReportController controller = new InspectionReportController();
//		System.assertEquals('Account Id Required', ApexPages.getMessages()[0].getDetail(), 'Error message for no id');
//	}
//
//	@isTest
//	private static void invalid_id_parameter_error_test() {
//		PageReference report = Page.InspectionReport;
//		Test.setCurrentPage(report);
//		report.getParameters().put('accountId', UserInfo.getUserId());
//		InspectionReportController controller = new InspectionReportController();
//		System.assertEquals('Invalid Account Id', ApexPages.getMessages()[0].getDetail(), 'Error message for invalid id');
//	}
//
//	@isTest
//	private static void invalid_text_in_parameter_error_test() {
//		PageReference report = Page.InspectionReport;
//		Test.setCurrentPage(report);
//		report.getParameters().put('accountId', 'bad');
//		InspectionReportController controller = new InspectionReportController();
//		System.assertEquals('There was a problem with the parameter for Account Id', ApexPages.getMessages()[0].getDetail(), 'Error message for invalid id text');
//	}
//
//	@isTest
//	private static void valid_parameter_init_test() {
//		Account account = [SELECT Id FROM Account];
//		Geocode_Cache__c property = [SELECT Id, Zip_Code__c FROM Geocode_Cache__c];
//		PageReference report = Page.InspectionReport;
//		Test.setCurrentPage(report);
//		report.getParameters().put('accountId', account.Id);
//		InspectionReportController controller = new InspectionReportController();
//		System.assertEquals(account.Id, controller.accountRecord.Id, 'account record set');
//	}
//
//	@isTest
//	private static void get_work_orders_test() {
//		Account account = [SELECT Id FROM Account];
//		Geocode_Cache__c property = [SELECT Id, Zip_Code__c, Client_Name__c FROM Geocode_Cache__c];
//		List<Case> inspections = [SELECT Id FROM Case ORDER BY Date_Serviced__c DESC];
//		PageReference report = Page.InspectionReport;
//		Test.setCurrentPage(report);
//		report.getParameters().put('accountId', account.Id);
//		InspectionReportController controller = new InspectionReportController();
//		controller.dateFilters.Client_Accepted_Date__c = Date.today().addDays(-10);
//		controller.dateFilters.Client_Canceled_Date__c = Date.today();
//		controller.getWorkOrdersButton();
//		System.assertEquals(1, controller.workOrders.size(), 'work orders found');
//		System.assertEquals(inspections[1].Id, controller.workOrders.get('loan').priorInspection.Id, 'prior inspection set');
//		System.assertEquals(inspections[0].Id, controller.workOrders.get('loan').currentInspection.Id, 'current inspection set');
//	}
//
//	@isTest
//	private static void export_test() {
//		Account account = [SELECT Id FROM Account];
//		Geocode_Cache__c property = [SELECT Id, Zip_Code__c, Client_Name__c FROM Geocode_Cache__c];
//		PageReference report = Page.InspectionReport;
//		Test.setCurrentPage(report);
//		report.getParameters().put('accountId', account.Id);
//		InspectionReportController controller = new InspectionReportController();
//		controller.dateFilters.Client_Accepted_Date__c = Date.today().addDays(-10);
//		controller.dateFilters.Client_Canceled_Date__c = Date.today();
//		controller.getWorkOrdersButton();
//		controller.exportCsvButton();
//		System.assertEquals(true, controller.csvOutput.contains('Loan #'), 'csv output');
//	}
}