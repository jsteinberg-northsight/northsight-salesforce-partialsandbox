public class NSWhereAmI {
	//A wrapper class for the Organization object. 
	private static Organization org = null;
	//Populate the org
    private static Organization fetchOrg(){
    	//Only fetch the org once per thread. 
    	if(org == null){
	        // Organization object
	        Organization[] orgs = new Organization[]{};
	        orgs = [select Id, InstanceName,IsSandbox,Name,OrganizationType FROM Organization limit 1];
	        // Check to make sure we have a result
	        if(orgs.size()==1)
	            org = orgs[0];
		}
		return org;
    }
    // Get the Edition as a string 
    public Static String getEdition(){
		Organization o = fetchOrg();
		return o.OrganizationType;
    }
    // Are we in sandbox? 
    public Static Boolean isSandbox(){
		Organization o = fetchOrg();
		return o.isSandbox;		
    }

}