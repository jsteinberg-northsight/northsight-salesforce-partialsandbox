@isTest
private class Test_BatchDeletePhotoResponseLogs {

///**
// *  Purpose         :   This is used for testing and covering BatchDeletePhotoResponseLogs. 
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   05/02/2014
// *
// *	Current Version	:	V1.0 - Padmesh Soni (05/02/2014) - Required Maintenance: Unit Test Improvements
// *
// *	Coverage		:	83%
// **/
// 	
// 	//Test method to test the functionality of Batch
//    static testMethod void testBatchDeletePhotoResponseLogs() {
//        
//        //List to hold Pruvan Pruvan PhotoResponse Log records
//        List<Pruvan_PhotoResponse_Log__c> pruvanPhotoResLogs = new List<Pruvan_PhotoResponse_Log__c>();
//        pruvanPhotoResLogs.add(new Pruvan_PhotoResponse_Log__c(Name = 'Test Log 1', Log_Data__c = 'Testin is going on for Deletion of first record.'));
//        pruvanPhotoResLogs.add(new Pruvan_PhotoResponse_Log__c(Name = 'Test Log 2', Log_Data__c = 'Testin is going on for Deletion of Second record.'));
//        
//        //insert Pruvan PhotoResponse Logs
//        insert pruvanPhotoResLogs;
//        
//        //Batch instance
//        BatchDeletePhotoResponseLogs b = new BatchDeletePhotoResponseLogs();
//		
//        //Test starts here
//        Test.startTest();
//        
//        //Execute batch here
//		Database.executeBatch(b, 200);
//		
//        //Test stops here
//        Test.stopTest();
//        
//        //Query result of Pruvan PhotoResponse Log object
//        pruvanPhotoResLogs = [SELECT Id FROM Pruvan_PhotoResponse_Log__c WHERE Id IN: pruvanPhotoResLogs];
//        
//        //assert statement here
//        System.assertEquals(0, pruvanPhotoResLogs.size());
//        
//    }
}