/**
 *  Purpose         :   This class is wrapper class for showing Work Order Instruction on WO page layout. 
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   1/9/2015
 *
 *  Current Version :   V1.0
 *
 *  Revision Log    :   V1.0 - Created
 **/
public with sharing class WOClientInstruction {
	
    
    //class properties
    public Work_Order_Instructions__c woInstruction {get; set;}
    public boolean isAlreadyCopied {get; set;}
    public String formattedDate {get; set;}
    public String formattedCreatedDate {get; set;}
    
    //Constructor definition
    public WOClientInstruction(Work_Order_Instructions__c woInstruction, boolean isAlreadyCopied) {
        
        //assign values
        //New parameter assignment
        this.woInstruction = woInstruction;
        this.isAlreadyCopied = isAlreadyCopied;
        this.formattedDate = woInstruction.CreatedDate.format('MM/dd/yy hh:mma').replace('AM', 'a').replace('PM', 'p');
        this.formattedCreatedDate = woInstruction.CreatedDate.format('dd-MMM h:mma').replace('AM', 'a').replace('PM', 'p');
    }
}