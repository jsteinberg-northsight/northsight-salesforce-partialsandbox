global class WeatherAlertCalloutMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
System.debug('ENDPOINT:'+req.getEndpoint());
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
        if(req.getEndpoint() == 'https://northsight.v-alert.co/api/login'){
	        res.setBody('{"status":"success","data":{"authToken":"testtoken","userId":"testuser"}}');
        }
        else{
        	res.setBody('{}');
        }
        return res;
     }
}