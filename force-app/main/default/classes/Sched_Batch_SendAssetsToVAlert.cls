global class Sched_Batch_SendAssetsToVAlert implements Schedulable{
    
    //execute method to execute the logic of batch processing
    global void execute(SchedulableContext ctx) {
        
        //Batch instance
        Batch_SendAssetsToVAlert batchJob = new Batch_SendAssetsToVAlert();
        
        //Batch executes here
        Database.executeBatch(batchJob, 200);
    }

}