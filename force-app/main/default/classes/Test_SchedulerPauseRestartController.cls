@isTest(seeAllData=false)
private class Test_SchedulerPauseRestartController {
///**
// *	Description		:	This is test class for SchedulerPauseRestartController class to test the functionality. 
// *
// *	Created By		:	Padmesh Soni
// *
// *	Created Date	:	06/16/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Logs	:	V1.0 - Created
// *
// *	Code Coverage	:	100% - V1.0
// **/
// 	
// 	//Test method to test and cover the functionality
//    static testMethod void myUnitTest() {
//    	
//    	//List to hold Scheduled Job object's test records
//        List<Scheduled_Job__c> scheduledJobs = new List<Scheduled_Job__c>();
//        scheduledJobs.add(new Scheduled_Job__c(Job_Name__c = 'Populate ImageLink Summary 1', Schedulable_Class__c = 'Sched_Batch_PopulateImageLinkSummaryCase'));
//        scheduledJobs.add(new Scheduled_Job__c(Job_Name__c = 'Delete Late VWM Surveys 1', Schedulable_Class__c = 'Sched_Batch_DeleteVWMSurveyOnOrder'));
//        scheduledJobs.add(new Scheduled_Job__c(Job_Name__c = 'Delete Old Image Link 1', Schedulable_Class__c = 'Sched_Batch_DeleteOldImageLinkFromCase'));
//        
//        //insert Scheduled Job here
//        insert scheduledJobs;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Schedule the jobs
//        System.schedule('Populate ImageLink Summary 1', '0 0 0 ? * 1', new Sched_Batch_PopulateImageLinkSummaryCase());
//        System.schedule('Delete Late VWM Surveys 1', '0 0 10 ? * 1', new Sched_Batch_DeleteVWMSurveyOnOrder());
//        System.schedule('Delete Old Image Link 1', '0 0 10 ? * 1', new Sched_Batch_DeleteOldImageLinkFromCase());
//        
//        //instance of SchedulerPauseRestartController class
//        SchedulerPauseRestartController controller = new SchedulerPauseRestartController();
//        
//        //call the getScheduledJob method
//        controller.getScheduledJobs();
//        
//        //assert statements here
//        System.assert(controller.scheduleWrapList != null);
//        
//        //Loop through Scheduled Job querying records
//        for(Scheduled_Job__c schedJob : [SELECT Id, Status__c FROM Scheduled_Job__c WHERE Id IN: scheduledJobs ORDER By Job_Name__c]) {
//        	
//        	//assert statements here
//        	System.assertEquals('Scheduled', schedJob.Status__c); 
//        }
//        
//        //populate the parameter with UI checboxes true by user
//        controller.scheduleWrapList[0].isScheduled = true;
//        controller.scheduleWrapList[1].isScheduled = true;
//        controller.scheduleWrapList[2].isScheduled = true;
//        
//        //abort the jobs
//        controller.abortJobs();
//        
//        //Loop through Scheduled Job querying records
//        for(Scheduled_Job__c schedJob : [SELECT Id, Status__c FROM Scheduled_Job__c WHERE Id IN: scheduledJobs ORDER By Job_Name__c]) {
//        	
//        	//assert statements here
//        	System.assertEquals('Stopped', schedJob.Status__c); 
//        }
//        
//        controller.doScheduleJobs();
//        
//        //Loop through Scheduled Job querying records
//        for(Scheduled_Job__c schedJob : [SELECT Id, Status__c FROM Scheduled_Job__c WHERE Id IN: scheduledJobs ORDER By Job_Name__c]) {
//        
//        	//assert statements here
//        	System.assertEquals('Scheduled', schedJob.Status__c);
//        }
//        
//        //populate the parameter with UI checboxes true by user
//        controller.scheduleWrapList[0].isScheduled = true;
//        controller.scheduleWrapList[1].isScheduled = true;
//        controller.scheduleWrapList[2].isScheduled = true;
//        
//        //abort the jobs
//        controller.abortJobs();
//        
//        //Loop through Scheduled Job querying records
//        for(Scheduled_Job__c schedJob : [SELECT Id, Status__c FROM Scheduled_Job__c WHERE Id IN: scheduledJobs ORDER By Job_Name__c]) {
//        	
//        	//assert statements here
//        	System.assertEquals('Stopped', schedJob.Status__c); 
//        }
//        
//        //initialize Constructor again and run the getScheduledJobs method again
//        controller = new SchedulerPauseRestartController();
//        controller.getScheduledJobs();
//        
//        //Test stops here
//        Test.stopTest();
//    }
}