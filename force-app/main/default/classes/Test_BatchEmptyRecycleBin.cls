@isTest(seeAllData=false)
private class Test_BatchEmptyRecycleBin {
//
///**
// *  Purpose         :   This is used for testing and covering BatchEmptyRecycleBin class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/10/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Padmesh Soni(06/10/2014) - Required Maintenance: Unit Test Improvements
// *	
// *	Coverage		:	88%
// **/
// 	
//    //test method added to check the functionality with code coverage
//    static testMethod void myUnitTest() {
//        
//        //Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client') AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(2, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accounts;
//		
//		Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//		insert property;
//		
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(Status = 'Closed', Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today()));
//    	workOrders.add(new Case(Status = 'Closed', Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1)));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//        //List to hold test records of Assignment_Log__c
//        List<Assignment_Log__c> assignmentLogs = new List<Assignment_Log__c>();
//        
//        //Loop through till count 10
//        for(integer i=0; i < 10; i++) {
//        	
//        	//add new instance of Assignment_Log__c into list
//        	assignmentLogs.add(new Assignment_Log__c(Work_Order__c = workOrders[0].Id));
//        	
//        	//add new instance of Assignment_Log__c into list
//        	assignmentLogs.add(new Assignment_Log__c(Work_Order__c = workOrders[1].Id));
//        }
//        
//        //insert Assignment Logs here
//        insert assignmentLogs;
//        
//        //Delete Assignment Logs here
//        delete assignmentLogs;
//        
//        //Query result from Assignment Log records which are deleted	
//        assignmentLogs = [Select Id From Assignment_Log__c Where IsDeleted = true ALL ROWS];
//        
//        //assert statement here
//        System.assertEquals(20, assignmentLogs.size());
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Execute batch here
//        Database.executeBatch(new BatchEmptyRecycleBin(), 200);
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //Query result of Assignment Logs records
//        assignmentLogs = [SELECT Id FROM Assignment_Log__c];
//        
//        //Assert statement here
//        System.assertEquals(0, assignmentLogs.size());
//    }
}