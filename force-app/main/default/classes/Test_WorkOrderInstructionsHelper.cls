@isTest(SeeAllData=false)
public with sharing class Test_WorkOrderInstructionsHelper {
//    //Globals
//    //****************************************************************
//    private static List<RecordType> recordTypes;
//    private static Account newAccount;
//    private static Work_Code__c newWorkCode;
//    private static Work_Code_Mapping__c newWorkCodeMapping;
//    private static Case newWorkOrder;
//    private static Work_Instruction__c[] newWorkInstruction;
//    private static Work_Code_Instruction__c[] newWorkCodeInstruction;
//    //****************************************************************
//    
//    //PRE-TEST SETUP
//    //***********************************************************************************************
//    private static void generateTestData() {
//        //retrieve record types of account and case objects
//        recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: AssignmentMapConstants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                        OR DeveloperName = 'Client') AND (SobjectType =: AssignmentMapConstants.CASE_SOBJECTTYPE 
//                        OR SobjectType =: AssignmentMapConstants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        //Assert statement
//        System.assertEquals(2, recordTypes.size());
//        
//        //create new account
//        newAccount = new Account(
//            Name = 'Test Account 1',
//            Phone = '1234567890',
//            RecordTypeId = recordTypes[0].Id
//        );
//        insert newAccount;
//        
//        //create new work code
//        newWorkCode = new Work_Code__c(
//            Name = 'GCREO01',
//            Work_Code_Description__c = 'Grass Cut REO',
//            Review_Needed__c =true,
//            Work_Ordered__c = 'Grass Cut', 
//            Department__c='Maintenance'
//        );
//        insert newWorkCode;
//        
//        //create new work code mapping
//        newWorkCodeMapping = new Work_Code_Mapping__c(
//            City__c = 'Dallas', 
//            Client__c = 'SG', 
//            Client_Special_1__c = 'Test Client1', 
//            Client_Special_2__c = 'Test Client2', 
//            Client_Special_3__c = 'Test Client3', 
//            Customer__c = 'Test Customer', 
//            Loan_Type__c = 'REO', 
//            Mapping_Priority__c = 300, 
//            State__c = 'CA', 
//            Vendor_Code__c = '123', 
//            Work_Code__c = newWorkCode.Id, 
//            Work_Ordered_Text__c = 'Test WO Data', 
//            Zip_Code__c = '12345',
//            Client_Name__c = newAccount.Id
//        );
//        insert newWorkCodeMapping;
//        
//        //create a new work survey
//        newWorkInstruction = new Work_Instruction__c[]{
//        	new Work_Instruction__c(
//            Instruction_Type__c = 'Other',
//            Item_Instructions__c = 'Item Instructions1'),
//            new Work_Instruction__c(
//            Instruction_Type__c = 'Other',
//            Item_Instructions__c = 'Item Instructions2')};
//        insert newWorkInstruction;
//        
//        //create a new work code survey
//        newWorkCodeInstruction = new Work_Code_Instruction__c[]{
//        new Work_Code_Instruction__c(
//            Sort__c = 1,
//            Work_Instruction_Number__c = newWorkInstruction[0].Id,
//            Work_Code_Name__c = newWorkCode.Id
//        ),
//        new Work_Code_Instruction__c(
//            Sort__c = 2,
//            Work_Instruction_Number__c = newWorkInstruction[1].Id,
//            Work_Code_Name__c = newWorkCode.Id
//        )};
//        insert newWorkCodeInstruction;
//        
//        //create new work order
//        newWorkOrder = new Case(
//            RecordTypeId = recordTypes[1].Id, 
//            AccountId = newAccount.Id, 
//            Due_Date__c = Date.today(), 
//            Status = AssignmentMapConstants.CASE_STATUS_OPEN, 
//            Approval_Status__c = 'Rejected',
//            Client__c = AssignmentMapConstants.CASE_CLIENT_SG, 
//            Work_Order_Type__c = 'Routine', 
//            Department__c = 'Test', 
//            Work_Ordered__c = 'Grass Recut', 
//            Assignment_Program__c = 'Mannual', 
//            City__c = 'Dallas', 
//            Client_Special_1__c = 'Test Client1', 
//            Client_Special_2__c = 'Test Client2', 
//            Client_Special_3__c = 'Test Client3', 
//            Customer__c = 'Test Customer', 
//            Loan_Type__c = 'REO', 
//            State__c = 'CA', 
//            Vendor_Code__c = '123', 
//            Work_Ordered_Text__c = 'Test WO Data', 
//            Zip_Code__c = '12345',
//            Client_Name__c = newAccount.Id
//        );
//        insert newWorkOrder;
//    }
//    
//    //TEST METHODS
//    //***********************************************************************************************
//    
//    //UNIT TEST
//    public static testmethod void workOrderInstructionsHelper_UnitTest() {
//        system.test.startTest();
//        
//        generateTestData();
//        
//        List<Work_Order_Instructions__c> workOrderInstructions = [SELECT Instruction_Type__c,
//                                                                         Item_Instructions__c,
//                                                                         Sort__c,
//                                                                         Case__c,
//                                                                         Work_Instructions_Number__c,
//                                                                         Work_Code_Instruction_Entry__c
//                                                                  FROM Work_Order_Instructions__c  order by sort__c];
//        system.assertEquals(2, workOrderInstructions.size());
//        
//            system.assertEquals('Other', workOrderInstructions[0].Instruction_Type__c);
//            system.assertEquals('Item Instructions1', workOrderInstructions[0].Item_Instructions__c);
//            system.assertEquals(1, workOrderInstructions[0].Sort__c);
//            system.assertEquals(newWorkInstruction[0].Id, workOrderInstructions[0].Work_Instructions_Number__c);
//            system.assertEquals(newWorkCodeInstruction[0].Id, workOrderInstructions[0].Work_Code_Instruction_Entry__c);
//            system.assertEquals(newWorkOrder.Id, workOrderInstructions[0].Case__c);
//        
//                    system.assertEquals('Other', workOrderInstructions[1].Instruction_Type__c);
//            system.assertEquals('Item Instructions2', workOrderInstructions[1].Item_Instructions__c);
//            system.assertEquals(2, workOrderInstructions[1].Sort__c);
//            system.assertEquals(newWorkInstruction[1].Id, workOrderInstructions[1].Work_Instructions_Number__c);
//            system.assertEquals(newWorkCodeInstruction[1].Id, workOrderInstructions[1].Work_Code_Instruction_Entry__c);
//            system.assertEquals(newWorkOrder.Id, workOrderInstructions[1].Case__c);
//        
//        system.test.stopTest();
//    }
}