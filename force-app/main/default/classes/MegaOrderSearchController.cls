/**
 *	Description		:	This is controller for MegaOrderSearch page.
 *
 *	Created By		:	Padmesh Soni
 *
 *	Created Date	:	10/06/2014
 *
 *	Current Version	:	V1.2
 *
 *	Revisiion Logs	:	V1.0 - Created - Padmesh Soni (10/06/2014) - VCP-26: Create a VF component that allows Search on only Work Order records.
 *						V1.1 - Modified - Padmesh Soni (10/13/2014) - VCP-30: Community Search: Approved Only User Flag
 *						V1.2 - Modified - Padmesh Soni (10/14/2014) - VCP-31: On new Community Work Order Search, apply filters based on Contact field
 *																		Also removed work of VCP-30
 **/
public with sharing class MegaOrderSearchController {
	
	//Class properties
	public String searchElement {get; set;}
	public integer listSize {get; set;}
	public transient List<Sobject> workOrders {get; set;}
	
	//Constructor definition	
	public MegaOrderSearchController() {
		
		//initialize counter
		listSize = 0;
		
		try {
			
			if(Apexpages.currentPage().getParameters().containsKey('srchTxt')) {
			
				//Getting url parameter if its exists
				searchElement = Apexpages.currentPage().getParameters().get('srchTxt');
				
				//Check for url parameter's existance
				if(String.isNotBlank(searchElement) && searchElement.length() >= 2) {
					
					//call method
					searchIt();
				} else {
					
					//throw a user friendly message on page
					throw new CustomException(Label.SEARCH_TERM_LIMITS);
				}
			}
		} catch(Exception e) {
			
			Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, e.getMessage()));
		}
	}
	
	/**
	 *	@descpriton		:	This method is used to getting search results of 
	 *
	 *	@param			:	
	 *
	 *	@return			:	void 
	 **/
	public void searchIt() {
		
		//initialize counter
		listSize = 0;
		
		//Scoping block of code into try-catch block 
		//to handle exception with providing user friendly message on page
		try {
			
			//Check for url parameter's existance
			if(String.isBlank(searchElement) || searchElement.length() < 2) {
				
				//throw a user friendly message on page
				throw new CustomException(Label.SEARCH_TERM_LIMITS);
			} else {
			
				//Getting field set of Case sobject
				List<Schema.FieldSetMember> megaSearchFieldSet = SObjectType.Case.FieldSets.Mega_Search.getFields();
				
				//Code added - Padmesh Soni (10/13/2014) - VCP-30: Community Search: Approved Only User Flag
    			//Query result of users
				List<User>	users = [SELECT Limit_Community_Work_Order_Search__c FROM User WHERE Id =: Userinfo.getUserId()];
				
				//Check if last character of string is not an asterick
				if(searchElement.lastIndexOf('*') != searchElement.length() - 1)
					searchElement = searchElement + '*';
				
				//Check if last character of string is an asterick
				if(searchElement.length() == 2 && searchElement.lastIndexOf('*') == searchElement.length() - 1) {
					
					//remove the end asterick character
					searchElement = searchElement.removeEnd('*');
					
					//throw a user friendly message on page
					throw new CustomException(Label.SEARCH_TERM_LIMITS);
				} else {
					
					//Generate SOSL string
					String searchquery='FIND \''+ searchElement +'\' IN ALL FIELDS RETURNING Case('; 
					
					//Loop through field set member list
					List<String> allFields = new List<String>();
					for(Schema.FieldSetMember f : megaSearchFieldSet) {
						//append string into SOSL
			            allFields.add(f.getFieldPath());
			        }
			        searchQuery += String.join(allFields, ',');
			        //append Id on SOSL query string 
			        searchquery += ',Id ' ;
			        
			        //Code commented - Padmesh Soni (10/14/2014) - VCP-31: On new Community Work Order Search, apply filters based on Contact field
    				//Code removed because VCP-30 is replaced by VCP-31
			        //Code added - Padmesh Soni (10/13/2014) - VCP-30: Community Search: Approved Only User Flag
    				//Check for user's flag Search only Approved Work Orders
			        /**if(users.size() > 0 && users[0].Search_only_Approved_Work_Orders__c) {
			        	
			        	//append the Where clause
			        	searchquery += ' WHERE Approval_Status__c = \'Approved\'';
			        }**/
			        
			        //Code added - Padmesh Soni (10/14/2014) - VCP-31: On new Community Work Order Search, apply filters based on Contact field
    				//Check for user's Limit_Community_Work_Order_Search__c field is not blank or null
			        if(users.size() > 0 && String.isNotBlank(users[0].Limit_Community_Work_Order_Search__c)) {
			        	
			        	//String to hold selected picklist values as comma seprated for SOSL
			        	String limitApprovalStatus = '(';
			        	
			        	//Loop through picklist values by spliting colon
			        	for(String limitSearchStatus : users[0].Limit_Community_Work_Order_Search__c.split(';')) {
			        		
			        		//append the value
			        		limitApprovalStatus += '\''+ limitSearchStatus + '\', ';	
			        	}
			        	
			        	//triming and removing the last comma of string
			        	limitApprovalStatus = limitApprovalStatus.removeEnd(', ').trim() + ')';
			        	
			        	//append the Where clause
			        	searchquery += ' WHERE Approval_Status__c IN '+ limitApprovalStatus;
			        }
			        
			        searchquery += '), Geocode_Cache__c(Name)';
			        
			        //List to hold sobject's list to store Contact and Case records retrived from SOSL
					List<List<Sobject>> tempWorkOrdersList = search.query(searchquery);
					
					//initialize the instance of workOrders list
					workOrders = new List<Sobject>();
					
					//Loop through till 10 times
					for(integer i = 0; i < 10; i++) {
						
						//Check for size of list and populate the list to be display on page
						if(tempWorkOrdersList[0].size() >= i+1)
							workOrders.add(tempWorkOrdersList[0][i]);
					}	
					
					//only add "Property" records if under the 10 record limit
					if (workOrders.size() < 10) {
						workOrders.addAll(
							getCasesByPropertyName(
								tempWorkOrdersList,
								10 - workOrders.size(),
								String.join(allFields, ',')
							)
						);
					}
					//system.debug(tempWorkOrdersList[0]);
					//system.debug(tempWorkOrdersList[1]);
					
					listSize = getCaseCount(tempWorkOrdersList);
					//Check for size of list if its 
					//zero then throw a user friendly message on page
					if(listSize == 0) {
						
						//removing last asterick character from string
						searchElement = searchElement.removeEnd('*');
						
						//throw an exception
						throw new CustomException(Label.NO_RESULTS);
					}
				}
			}
				
			//removing last asterick character from string
			searchElement = searchElement.removeEnd('*');
		} catch(Exception e) {
			
			//removing last asterick character from string
			searchElement = searchElement.removeEnd('*');
			
			Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, e.getMessage()));
		}
	}
	
	private static List<sObject> getCasesByPropertyName(List<List<sObject>> pSearchResults, Integer pMaxRecordsToAdd, String pFields) {
		List<Case> cases = new List<Case>();
		if (pMaxRecordsToAdd == null || pMaxRecordsToAdd < 1 || pSearchResults == null || pSearchResults.isEmpty()) {
			return cases;
		}
		
		//get property list from SOSL query
		for (List<sObject> currentList : pSearchResults) {
			if (currentList.isEmpty() || currentList[0].getSobjectType() != Geocode_Cache__c.getSobjectType()) {
				continue;
			}
			Set<Id> propertyIds = new Set<Id>();
			for (sObject currentObject : currentList) {
				propertyIds.add((Id) currentObject.get('Id'));
			}
			return Database.query('SELECT ' + pFields + ' FROM Case WHERE Geocode_Cache__c IN :propertyIds');
		}
		return cases;
	}
	
	private static Integer getCaseCount(List<List<sObject>> pSearchResults) {
		Integer counter = 0;
		if (pSearchResults == null || pSearchResults.isEmpty()) {
			return counter;
		}
		
		//get property list from SOSL query
		for (List<sObject> currentList : pSearchResults) {
			if (!currentList.isEmpty() && currentList[0].getSobjectType() == Case.getSobjectType()) {
				counter += currentList.size();
			}
			if (!currentList.isEmpty() && currentList[0].getSobjectType() == Geocode_Cache__c.getSobjectType()) {
				Set<Id> propertyIds = new Set<Id>();
				for (sObject currentObject : currentList) {
					propertyIds.add((Id) currentObject.get('Id'));
				}
				List<Case> cases = [SELECT Id FROM Case WHERE Geocode_Cache__c IN :propertyIds];
				counter += cases.size();
			}
		}
		return counter;
	}
	
	public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}