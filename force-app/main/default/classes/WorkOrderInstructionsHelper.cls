public without sharing class WorkOrderInstructionsHelper {
    //globals
    //************************************************************
    private static List<Case> casesAfterInsert;
    private static List<Work_Order_Instructions__c> workOrderInstructions;
    private static Map<Id,Work_Code_Instruction__c[]> workCodeInstructionMap;
    private static RecordType recType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Vendor_Instructions' limit 1];
    //************************************************************
    
    //initialize method to instantiate globals
    public static void initialize(List<Case> triggerNew) {
        if(triggerNew != null && triggerNew.size() > 0)
            casesAfterInsert = triggerNew;
        workOrderInstructions = new List<Work_Order_Instructions__c>();
        workCodeInstructionMap = new Map<Id,Work_Code_Instruction__c[]>();
        getWorkCodeInstructions();
    }
    
    //gets the work code instruction records using the casesAfterInsert data and add them to the workCodeInstructionMap
    private static void getWorkCodeInstructions() {
        Set<Id> workCodeIds = new Set<Id>();
        if(casesAfterInsert != null && casesAfterInsert.size() > 0) {
            for(Case c : casesAfterInsert) {
                if(!workCodeIds.Contains(c.Work_Code__c))
                    workCodeIds.Add(c.Work_Code__c);
            }
        }
        
        if(workCodeIds != null && workCodeIds.size() > 0) {
            for(Work_Code_Instruction__c workCodeInstruction : [SELECT Id,
                                                             Instruction_Type__c,
                                                             Item_Instructions__c,
                                                             Sort__c,
                                                             Action__c,
                                                             Work_Code_Name__c,
                                                             Work_Instruction_Number__c
                                                      FROM Work_Code_Instruction__c 
                                                      WHERE Work_Code_Name__c IN: workCodeIds]) 
            {
                if(!workCodeInstructionMap.ContainsKey(workCodeInstruction.Work_Code_Name__c)){
                    workCodeInstructionMap.put(workCodeInstruction.Work_Code_Name__c,new Work_Code_Instruction__c[]{} );
                }
                workCodeInstructionMap.get(workCodeInstruction.Work_Code_Name__c).add(workCodeInstruction);
            }
        }
        createworkOrderInstructions();
    }
    
    //creates the work order instructions records using the caseAfterInsert and workCodeInstructionMap data, then adds the created records to workOrderInstructions list for insert
    private static void createworkOrderInstructions() {
        if(casesAfterInsert != null && casesAfterInsert.size() > 0) {
            if(workCodeInstructionMap != null && workCodeInstructionMap.size() > 0) {
                for(Case c : casesAfterInsert) {
                    if(workCodeInstructionMap.ContainsKey(c.Work_Code__c)) {
                        for(Work_Code_Instruction__c tempWorkCodeInstruction : workCodeInstructionMap.get(c.Work_Code__c)){
                        Work_Order_Instructions__c workOrderInstruction = new Work_Order_Instructions__c(
                            Case__c = c.Id,
                            RecordTypeId = recType.Id,
                            Work_Code_Instruction_Entry__c = tempWorkCodeInstruction.Id,
                            Instruction_Type__c = tempWorkCodeInstruction.Instruction_Type__c,
                            Item_Instructions__c = tempWorkCodeInstruction.Item_Instructions__c,
                            Action__c = tempWorkCodeInstruction.Action__c,
                            Sort__c = tempWorkCodeInstruction.Sort__c,
                            Work_Instructions_Number__c = tempWorkCodeInstruction.Work_Instruction_Number__c,
                            HashKey__c = ''+c.Id+tempWorkCodeInstruction.Work_Instruction_Number__c
                        );
                        workOrderInstructions.Add(workOrderInstruction);
                        }
                    }
                }
            }
        }
    }
    
    //inserts the list of work order instructions records, if any
    public static void finalize() {
        if(workOrderInstructions != null && workOrderInstructions.size() > 0)
            upsert workOrderInstructions HashKey__c;
    }
}