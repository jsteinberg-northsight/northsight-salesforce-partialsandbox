public with sharing class SendSGphotoDirectPhotosController {
    /*
    	Example json payload sent to PHP Server
    		{
			  "payload": {
			    "username": "JS3GRMOH01",
			    "password": "9ycoXy85",
			    "platform": "android",
			    "workOrderReferenceNum": "189754868",
			    "proxySalesforceId": "",
			    "proxyIP": "",
			    "proxyUser": "",
			    "proxyPassword": "",
			    "dateServiced": "",
			    "imageLinks": [
			      {
			        "imageLinkSalesforceId": "a0M3300000PuaF6EAJ",
			        "originalFileName": "5003300000ur1lpAAA",
			        "imageURL": "https://allyearimages.s3.amazonaws.com/178426810/500556069.jpg"
			      },
			      {
			        "imageLinkSalesforceId": "a0M3300000PuaGPEAZ",
			        "originalFileName": "5003300000ur1lpAAA",
			        "imageURL": "https://allyearimages.s3.amazonaws.com/178426810/500556548.jpg"
			      }
			    ]
			  }
			}
	*/
	
    private Case workOrder;
    private final String ENDPOINT = 'http://testnorthsight.nerdwebhost.com';
    
    public String displayMessage {get; set;}
    
    public boolean disableSendButton {get; set;}
    public Integer numOfImagesToSend {get; set;}
    public Integer numOfImagesSuccessfullySent {get; set;}
    public Integer numOfImagesFailedToSend {get; set;}
    
    private Proxy__c proxy;
    
    public SendSGphotoDirectPhotosController(ApexPages.StandardController stdController){
    	this.workOrder = (Case)stdController.getRecord();
    	
    	
    	if(this.workOrder.Id != null){
    		this.workOrder = fetchWorkOrder(this.workOrder.Id);
    		
    		//select proxy now, validate() will throw error if no proxy is available now
    		proxy = NSProxyHelper.selectProxy();
    		
    		if(validate()){
		    	//check if there are photos to upload and disable send button if not
		    	ImageLinks__c[] imageLinkRecs = fetchUnsentImageLinkRecords();
		    	if(imageLinkRecs.size() <= 0){
		    		refreshPhotoUploadStatus();
		    		
		    		disableSendButton = true;
		    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No photos to send.'));
		    	}
		    	else{
		    		disableSendButton = false;
		    	}
    		}
    		else{
    			disableSendButton = true;
    		}
    	}
    	else{
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Work order not found.'));
    	}
    }
    
    public void startImageUploadingProcess(){
    	try{
	    	//select proxy again to ensure the proxy is still available from the time the user opened the page to the time it took the user to click the send button
	    	proxy = NSProxyHelper.selectProxy();
	    	
	    	//perform validation check again to ensure proxy was selected and that nothing else changed from the time user opened page to when they clicked send button
	    	if(validate()){
		    	//query image link records
		    	ImageLinks__c[] imageLinks = fetchUnsentImageLinkRecords();
		    	
		    	if(imageLinks.size() > 0){
		    		
		    		String payload = buildPayloadJSONstring(imageLinks);
		    		
		    		
		    		/*EXAMPLE Response returned from php server:
		    			{
			    			"success":true,
			    			"error":null
		    			}
		    		*/
		    		
			    	String response = sendHTTPrequest(payload);
			    		//String response = '{"success":true, "error": "HTTP callout is disabled in SF controller"}'; //example response for testing purposes
			    	
			    	if(response != ''){
			    		Map<String, object> parsedResponse = (Map<String, object>)JSON.deserializeUntyped(response);
			    		
			    		if(parsedResponse.containsKey('success')){
				    		//verify if response is success/failure
				    		if(boolean.valueOf(parsedResponse.get('success')) == true){
				    			//display success message to user
				    			displayMessage = 'Connection was successful! response: ' + response; /*TODO: remove response from print on page once done testing*/
				    			
				    			//set proxy fail count to 0
				    			proxy.Fail_Count__c = 0;
				    		}
				    		else{
				    			//display failure message to user
				    			displayMessage = 'FAILURE occurred. Error response: ';
				    			
				    			if(parsedResponse.containsKey('error')){
				    				displayMessage += (string)parsedResponse.get('error');
				    				
				    				if((string)parsedResponse.get('error') == 'Curl-Error: Received HTTP code 407 from proxy after CONNECT'){
				    					//update values to Failure property of Proxy
				                        NSProxyHelper.proxyError(proxy.Id);
				    				}
			                    }
			                    else{
			                    	//couldnt read error message from response, so just print entire response on page
			                    	displayMessage += response;
			                    }
				    		}
			    		}
			    		else{ //unable to parse response. Display full response on page
			    			displayMessage = 'Failed to read response. Here is the full response received: ' + response;
			    		}
			    		
			    		//NOTE: Proxy must be updated after callout is made due to salesforce limitations.
			    		//Update the proxy record. This should help to prevent multiple users from selecting the same proxy at the same time.
				    	//Check for Last Used's month is in less than present month
			            if(proxy.Last_Used__c != null && proxy.Last_Used__c.month() < Date.today().month() && proxy.Last_Used__c.year() <= Date.today().year()){
			                proxy.Data_Usage_This_Month__c = 0;
			            }
			            /*//Check for Last Used's date is in less than present date
			            if(proxy.Last_Used__c != null && proxy.Last_Used__c.date() < Date.today()){
			                proxy.Requests_Today__c = 0;
			            }*/
			             
			            //assign present time to Last used
			            proxy.Last_Used__c = Datetime.now();
			    		
			    		//update proxy info
				        if(proxy.Requests_Today__c == null){
				            proxy.Requests_Today__c = 0;
				        }
				        proxy.Requests_Today__c += 1;
				        
			            update proxy;
			    	}
			    	else{
			    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Failed to receive a response from server.'));
			    	}
			    	displayMessage += '<br/><br/>Payload:<br/> ' + payload; /*TODO: remove when done testing*/
		    	}
		    	else{
		    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No photos to send.'));
		    	}
	    	}
    	}
    	catch(Exception e){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Error message: ' + e.getMessage() + '   ~~stack trace: ' + e.getStackTraceString()));
    	}
    }
    
    public void refreshPhotoUploadStatus(){
	    ImageLinks__c[] imgRecs = fetchAllImageLinkRecordsForWorkOrder();
	    
	    numOfImagesToSend = 0;
	    numOfImagesSuccessfullySent = 0;
	    numOfImagesFailedToSend = 0;
	    
	    for(ImageLinks__c i : imgRecs){
	    	if(i.Client_Transmit_Status__c == 'Unsent' || i.Client_Transmit_Status__c == ''){
	    		numOfImagesToSend++;
	    	}
	    	else if(i.Client_Transmit_Status__c == 'Failed'){
	    		numOfImagesFailedToSend++;
	    	}
	    	else if(i.Client_Transmit_Status__c == 'Sent'){
	    		numOfImagesSuccessfullySent++;
	    	}
	    }
	}
	
	private String buildPayloadJSONstring(ImageLinks__c[] imageLinks){
		//build payload json
    	Map<String, object> temp = new Map<String, object>();
    	
    	//if sub vendor code is blank on WO, then get user and pass from order import object
    	if(string.isBlank(workOrder.Sub_Vendor_Code__c)){
    		temp.put('username', workOrder.Import_Login__c);
    		temp.put('password', workOrder.Import_Password__c); 
    	}
    	else{ //otherwise get user and pass from order import subvendor obj
    		temp.put('username', workOrder.Sub_Vendor_Code__c);
    		
    		//get pass from sub vendor record
    		temp.put('password', fetchSubVendorPass(workOrder.Sub_Vendor_Code__c));
    	}
    	
    	//misc fields
    	String platform = workOrder.Vendor_Mobile_Device__c=='iOS'?'iphone':'android';
    	temp.put('platform', platform);
    	temp.put('workOrderReferenceNum', workOrder.Reference_Number__c);
		temp.put('dateServiced', DateTime.newInstance(workOrder.Date_Serviced1__c,Time.newInstance(0,0,0,0)).format('MM/dd/yyyy'));
    	
    	//proxy info
    	temp.put('proxySalesforceId', proxy.Id);
    	temp.put('proxyIP', proxy.IP__c+':'+string.valueOf(proxy.Port__c));
    	temp.put('proxyUser', proxy.Username__c);
    	temp.put('proxyPassword', proxy.Password__c);
    	
    	ImageLinksWrapper[] imageWrappers = buildImageLinkWrappers(imageLinks);
    	
    	temp.put('imageLinks', imageWrappers);
    	
    	Map<String, Object> payload = new Map<String, Object>();
    	payload.put('payload', temp);
    	
    	return JSON.serialize(payload);
	}
    
    private String sendHTTPrequest(String payloadJSON) {
		// Instantiate a new http object
		Http h = new Http();
		
		HttpRequest req = new HttpRequest();
		req.setEndpoint(ENDPOINT);
		req.setMethod('POST');
		
		req.setBody(payloadJSON);
		req.setTimeout(50000); //set to 50 sec. Default is 10 but was receiving timeout errors
		// Send the request, and return a response
		HttpResponse res = h.send(req);
		return res.getBody();
	}
	
	private ImageLinks__c[] fetchUnsentImageLinkRecords(){
		return [SELECT Id, ImageUrl__c, Original_fileName__c, Client_Transmit_Status__c
				FROM ImageLinks__c
				WHERE CaseId__c =: workOrder.Id AND
					  Client_Transmit_Status__c !=: 'Sent' AND
					  Client_Transmit_Status__c !=: 'In Process'];
	}
	
	private ImageLinks__c[] fetchAllImageLinkRecordsForWorkOrder(){
		return [SELECT Id, ImageUrl__c, Original_fileName__c, Client_Transmit_Status__c
				FROM ImageLinks__c
				WHERE CaseId__c =: workOrder.Id];
	}
	
	private Case fetchWorkOrder(Id woId){
		return [SELECT Id, Reference_Number__c, Vendor_Code__c, Sub_Vendor_Code__c, Vendor_Mobile_Device__c, Import_Login__c, Import_Password__c, Date_Serviced1__c, Client__c, Mobile__c
				FROM Case
				WHERE Id =: woId];
	}
	
	public class ImageLinksWrapper{
    	public String imageLinkSalesforceId {get; set;}
		public String imageURL {get; set;}
		public String originalFileName {get; set;}
    }
	
    private ImageLinksWrapper[] buildImageLinkWrappers(ImageLinks__c[] workOrderImageLinkRecords){
    	ImageLinksWrapper[] imageWrappers = new ImageLinksWrapper[]{};
    	
    	for(ImageLinks__c i : workOrderImageLinkRecords){
    		ImageLinksWrapper wrap = new ImageLinksWrapper();
    		
    		wrap.imageLinkSalesforceId = i.Id;
    		wrap.imageURL = i.ImageUrl__c;
    		wrap.originalFileName = i.Original_fileName__c;
    		
    		imageWrappers.add(wrap);
    	}
    	
    	return imageWrappers;
    }
    
    private String fetchSubVendorPass(string subvendorcode){
    	system.assert(!string.isblank(subvendorcode), 'subvendor code is blank!');
    	
    	Order_Import_Subvendor__c[] creds = [select id, vendorPass__c from Order_Import_Subvendor__c where vendorCode__c=:subVendorCode];
    	system.assert(creds.size()==1, 'Duplicate subvendors found!');
    	system.assert(!string.isblank(creds[0].vendorPass__c), 'Subvendor password entry is blank!');
    	
    	return creds[0].vendorPass__c;
    }
    
    private boolean validate(){
    	boolean ret = true;
    	
        //Check for proxy records list size
        if(proxy == null){
            ret = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, 'No Proxies available.\r\n'));
        }
        
        //Check for client is not equal to SG
        if(workOrder.Client__c != 'SG'){
            ret = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, 'Client must be SG\r\n'));
        }
        
        //Check for mobile is false
        if(!workOrder.Mobile__c){
            ret = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, 'Work order data must be submitted by mobile application prior to sending  with this button.\r\n'));
        }
        
        //Check for date service is null
        if(workOrder.Date_Serviced1__c==null){
            ret = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, 'Date Serviced: must not be null.\r\n'));
        }
        
        return ret;
    }
    
    public static void testByPass(){
    	Integer i = 0;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    }
}