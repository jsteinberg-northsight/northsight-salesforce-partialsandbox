@isTest(SeeAllData=false)
public with sharing class ImageLinksTester {
//
//    private static Account          newAccount;
//    private static Contact          newContact;
//    private static Profile          newProfile;
//    private static User             newUser;
//    private static Geocode_Cache__c newGeocode;
//    private static RecordType       rt;
//    private static Case             newCase;
//    private static string photoHelperPayload;
//    private static string surveyHelperPayload;
//    /*************************************************************************************************************************************************************************/
//    /*
//    this sections will create the necessary objects needed for the case object to be tested on
//    */
//    private static void generateTestData(){
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new account
//        newAccount = new Account(
//            Name = 'Tester Account',
//            RecordTypeId ='01240000000Ubta'
//            
//        );
//        insert newAccount;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new contact
//        newContact = new Contact(
//            LastName = 'Tester',
//            AccountId = newAccount.Id,
//            Pruvan_Username__c = 'NSBobTest',
//            RecordTypeId = '01240000000UbtfAAC'
//        );
//        insert newContact;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new user with the Customer Portal Manager Standard profile
//        newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Processors'];
//        //create a new user with the profile
//        newUser = new User(
//            alias = 'standt',
//            contactId = newContact.Id,
//            email = 'standarduser@testorg.com',
//            emailencodingkey = 'UTF-8',
//            lastname = 'Testing',
//            languagelocalekey = 'en_US',
//            localesidkey = 'en_US',
//            profileId = newProfile.Id,
//            timezonesidkey = 'America/Los_Angeles', username = 'standarduser@northsight.test', isActive = true
//        );
//        insert newUser;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new property
//        newGeocode = new Geocode_Cache__c(
//            address_hash__c = '400cedarstbrdgprtct60562',
//            quarantined__c = false,
//            location__latitude__s = 30.099307,//11.11111111
//            location__longitude__s = -81.75276,//-111.22222222
//            Street_Address__c = '400 Cedar st',
//            Zip_Code__c = '60562',
//            City__c = 'Bridgeport',
//            State__c = 'CT',
//            Formatted_Street_Address__c = '400 Cedar st',
//            Formatted_Zip_Code__c = '60562',
//            Formatted_City__c = 'Bridgeport',
//            Formatted_State__c = 'CT',
//            sqft_of_lot_size__c = 19660
//        );
//        insert newGeocode;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create work orders with the REO record type
//        rt = [SELECT Id, Name FROM RecordType WHERE Name =: 'REO' LIMIT 1];
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//        //create a work order with the record type
//        newCase = new Case(
//            Client__c = 'NOT-SG',
//            Due_Date__c = Date.today(),
//            Backyard_Serviced__c = 'Yes',
//            Excessive_Growth__c = 'No',
//            Debris_Removed__c = 'No',
//            Shrubs_Trimmed__c = 'No',
//            Vendor_Code__c = 'CHIGRS',
//            State__c = 'CT',
//            Status = 'Open',
//            City__c = 'Bridgeport',
//            Zip_Code__c = '60562',
//            Description = 'Work order for grass cutting',
//            OwnerId = newUser.Id,
//            ContactId = newContact.Id,
//            Street_Address__c = '400 Cedar st',
//            Geocode_Cache__c = newGeocode.Id,
//            Work_Ordered__c = 'Initial Grass Cut',
//            Work_Ordered_Text__c = 'Grass Cutting',
//            Approval_Status__c = 'Not Started',
//            RecordTypeId = rt.Id,
//            Client_Name__c = account.Id
//        );
//        insert newCase;
//    }
//    
//    private static void setPayloads(){
//        for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//            photoHelperPayload =    '{' +
//                                    '"timestamp": 1377559122,' +
//                                    '"serviceId": "32981757",' +
//                                    '"workOrderId": "37246450",' +
//                                    '"evidenceType": "Survey",' +
//                                    '"fileType": "picture",' +
//                                    '"csrCertifiedPhoto": "1",' +
//                                    '"csrCertifiedTime": "1",' +
//                                    '"csrCertifiedLocation": null,' +
//                                    '"csrPictureCount": null,' +
//                                    '"uuid": "d9aab2a9-eec6-470e-bf13-0cb74ca47e75",' +
//                                    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//                                    '"fileName": "1382536931021.jpg",' +
//                                    '"gpsLatitude": "31.62020098900615",' +
//                                    '"gpsLongitude": "-97.099915453764",' +
//                                    '"gpsTimestamp": "1382536931",' +
//                                    '"gpsAccuracy": "2.0",' +
//                                    '"key2": "' + c.CaseNumber + '",' +
//                                    '"key4": "Grass Recut",' +
//                                    '"username": "pruvan",' +
//                                    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//                                    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706368.jpg"' +
//                                    '}';
//                                    
//            surveyHelperPayload =   '{' +
//                                    '"timestamp": 1377559123,' +
//                                    '"serviceId": "32786734",' +
//                                    '"workOrderId": "37064308",' +
//                                    '"evidenceType": "Survey",' +
//                                    '"fileType": "survey",' +
//                                    '"csrCertifiedPhoto": "1",' +
//                                    '"csrCertifiedTime": "1",' +
//                                    '"csrCertifiedLocation": null,' +
//                                    '"csrPictureCount": null,' +
//                                    '"uuid": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//                                    '"parentUuid": "37064308",' +
//                                    '"fileName": "survey_1382537334014.json",' +
//                                    '"gpsLatitude": "42.01235463",' +
//                                    '"gpsLongitude": "-87.89172686",' +
//                                    '"gpsTimestamp": "1382537316",' +
//                                    '"gpsAccuracy": "10.0",' +
//                                    '"key2": "' + c.CaseNumber + '",' +
//                                    '"key4": "Grass Recut",' +
//                                    '"username": "pruvan",' +
//                                    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//                                    '"nsSurveyExtract": {' +
//                                        '"answers": [{' +
//                                            '"answer": ["yes"],' +
//                                            '"id": "Work_Completed__c",' +
//                                            '"picIds": ["d9aab2a9-eec6-470e-bf13-0cb74ca47e75"],' +
//                                            '"hint": "Select No for Trip Charge"' +
//                                        '}, {' +
//                                            '"answer": ["yes"],' +
//                                            '"id": "Backyard_Serviced__c",' +
//                                            '"hint": ""' +
//                                        '}, {' +
//                                            '"answer": ["no"],' +
//                                            '"id": "Excessive_Growth__c",' +
//                                            '"hint": ""' +
//                                        '}, {' +
//                                            '"answer": ["yes"],' +
//                                            '"id": "Shrubs_Trimmed__c",' +
//                                            '"hint": ""' +
//                                        '}, {' +
//                                            '"answer": ["no"],' +
//                                            '"id": "Debris_Removed__c",' +
//                                            '"hint": ""' +
//                                        '}, {' +
//                                            '"answer": ["Excessive Grass/Weed Height"],' +
//                                            '"id": "Bid_Type__c",' +
//                                            '"hint": ""' +
//                                        '}, {' +
//                                            '"answer": ["This is the bid description."],' +
//                                            '"id": "Bid_Description__c",' +
//                                            '"hint": ""' +
//                                        '}, {' +
//                                            '"answer": ["100"],' +
//                                            '"id": "Bid_Cost__c",' +
//                                            '"hint": ""' +
//                                        '}],' +
//                                        '"meta": {' +
//                                            '"currentQuestionIndex": 18,' +
//                                            '"surveyId": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//                                            '"parentId": "37064308",' +
//                                            '"surveyTemplateId": "sarcj07::GC_001-v4"' +
//                                        '}' +
//                                    '},' +
//                                    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706991.json"' +
//                                    '}';
//        }
//    }
//    /*************************************************************************************************************************************************************************/
//    //--------------------------------------------------------
//    //TESTS
//    //Unit tests for method testing and regression testing. 
//    //--------------------------------------------------------
//        
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - create two image links with the second one created 5 minutes later
//    //and then assert that the 'Minutes At Property' field on the case shows 5 for 5 mins
//    public static testmethod void testFiveMinsApart(){
//        system.Test.startTest();
//        generateTestData();
//        //update newCase to be completed and serviced today
//            //newCase.Work_Completed__c = 'Yes';
//            //newCase.Date_Serviced__c = Date.today();
//            //update newCase;
//        //create a list for two image links 5 mins apart
//        List<ImageLinks__c> fiveMinsApartList = new List<ImageLinks__c>();
//        //create two image links 5 minutes apart
//        ImageLinks__c beforePhoto = new ImageLinks__c(
//            CaseId__c = newCase.Id,
//            GPS_Timestamp__c = Datetime.newInstance(0).addSeconds(1388998800),
//            Pruvan_uuid__c = '1'
//        );
//        fiveMinsApartList.add(beforePhoto);
//        ImageLinks__c afterPhoto = new ImageLinks__c(
//            CaseId__c = newCase.Id,
//            GPS_Timestamp__c = Datetime.newInstance(0).addSeconds(1388999100),
//            Pruvan_uuid__c = '2'
//        );
//        fiveMinsApartList.add(afterPhoto);
//    
//        insert fiveMinsApartList;
//        //assert that the two image links were created 5 minutes apart
//        for(Case c : [SELECT Minutes_At_Property__c FROM Case WHERE Id =: newCase.Id]){
//            system.assertEquals(5, c.Minutes_At_Property__c);
//        }
//        system.Test.stopTest();
//    }
//        
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - create four image links: two for the current day and two for the day after
//        //the first two will be created 5 mins apart
//        //the next two will also be created 5 mins apart
//        //assert that with all four image links, the total 'Minutes At Property' field should be 10
//    public static testmethod void testDiffDays(){
//        system.Test.startTest();
//        generateTestData();
//        //update newCase to be completed and serviced today
//            //newCase.Work_Completed__c = 'Yes';
//            //newCase.Date_Serviced__c = Date.today();
//            //update newCase;
//        //create a list for image links created on separate days
//        List<ImageLinks__c> diffDaysList = new List<ImageLinks__c>();
//        //create four image links
//            //two 5 minutes apart on the same day
//            //two 5 minutes apart on the next day
//        ImageLinks__c photo1 = new ImageLinks__c(
//            CaseId__c = newCase.Id,
//            GPS_Timestamp__c = datetime.newInstance(0).addSeconds(1385483160),
//            Pruvan_uuid__c = '1'
//        );
//        diffDaysList.add(photo1);
//        
//        ImageLinks__c photo12 = new ImageLinks__c(
//            CaseId__c = newCase.Id,
//            GPS_Timestamp__c = datetime.newInstance(0).addSeconds(1385483400),
//            Pruvan_uuid__c = '12'
//        );
//        diffDaysList.add(photo12);
//        
//        ImageLinks__c photo13 = new ImageLinks__c(
//            CaseId__c = newCase.Id,
//            GPS_Timestamp__c = datetime.newInstance(0).addSeconds(1385484300),
//            Pruvan_uuid__c = '13'
//        );
//        diffDaysList.add(photo13);
//        
//        ImageLinks__c photo2 = new ImageLinks__c(
//            CaseId__c = newCase.Id,
//            GPS_Timestamp__c = datetime.newInstance(0).addSeconds(1385486520),
//            Pruvan_uuid__c = '2'
//        );
//        diffDaysList.add(photo2);
//        
//        
//        
//        
//        ImageLinks__c photo3 = new ImageLinks__c(
//            CaseId__c = newCase.Id,
//            GPS_Timestamp__c = datetime.newInstance(0).addSeconds(1385540760),
//            Pruvan_uuid__c = '3'
//        );
//        diffDaysList.add(photo3);
//        
//        ImageLinks__c photo32 = new ImageLinks__c(
//            CaseId__c = newCase.Id,
//            GPS_Timestamp__c = datetime.newInstance(0).addSeconds(1385559000),
//            Pruvan_uuid__c = '32'
//        );
//        diffDaysList.add(photo32);
//        
//        ImageLinks__c photo33 = new ImageLinks__c(
//            CaseId__c = newCase.Id,
//            GPS_Timestamp__c = datetime.newInstance(0).addSeconds(1385555400),
//            Pruvan_uuid__c = '33'
//        );
//        diffDaysList.add(photo33);
//        
//        ImageLinks__c photo4 = new ImageLinks__c(
//            CaseId__c = newCase.Id,
//            GPS_Timestamp__c = datetime.newInstance(0).addSeconds(1385563620),
//            Pruvan_uuid__c = '4'
//        );
//        diffDaysList.add(photo4);
//        
//        insert diffDaysList;
//        //assert that the two image links were created 5 minutes apart
//        for(Case c : [SELECT Minutes_At_Property__c FROM Case WHERE Id =: newCase.Id]){
//            system.assertEquals(437, c.Minutes_At_Property__c);
//        }
//        system.Test.stopTest();
//    }
//        
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - create one image link and assert that the 'Minutes At Property' field is 0
//    public static testmethod void testOneImageLink(){
//        system.Test.startTest();
//        generateTestData();
//        //update newCase to be completed and serviced today
//            //newCase.Work_Completed__c = 'Yes';
//            //newCase.Date_Serviced__c = Date.today();
//            //update newCase;
//        //create a single image link
//        ImageLinks__c singlePhoto = new ImageLinks__c(
//            CaseId__c = newCase.Id,
//            GPS_Timestamp__c = Datetime.now(),
//            Pruvan_uuid__c = '1'
//        );
//        insert singlePhoto;
//        //assert that the two image links were created 5 minutes apart
//        for(Case c : [SELECT Minutes_At_Property__c FROM Case WHERE Id =: newCase.Id]){
//            system.assertEquals(0, c.Minutes_At_Property__c);
//        }
//        system.Test.stopTest();
//    }
//    
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - create image links with a URL and verify the work order's approval status is set to 'Pre-Pending' and the mobile flag is set
//    public static testmethod void testHasURL(){
//        system.Test.startTest();
//        generateTestData();
//        setPayloads();
//
//        //send the payloads, the one for the photo helper will not activate the survey helper
//        //and the survey helper payload will only finish in the PruvanSurveyHelper class
//        
//        PruvanPhotoHelper.uploadPictures(photoHelperPayload);
//        PruvanPhotoHelper.uploadPictures(surveyHelperPayload);
//        
//        //assert that the work order's approval status has been set to 'Pre-Pending' and the mobile flag has been set
//        for(Case c : [SELECT All_Images_Received__c FROM Case WHERE Id =: newCase.Id]){
//            system.assertEquals(true, c.All_Images_Received__c);
//        }
//        system.Test.stopTest();
//    }
//    
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - create image links without a URL and verify the work order's approval status != 'Pre-Pending' and the mobile flag is not set
//    public static testmethod void testNoURL(){
//        system.Test.startTest();
//        generateTestData();
//        setPayloads();
//        
//        PruvanPhotoHelper.uploadPictures(surveyHelperPayload);//send the payload
//        
//        //assert that the work order's approval status != 'Pre-Pending' and the mobile flag has been set
//        for(Case c : [SELECT All_Images_Received__c FROM Case WHERE Id =: newCase.Id]){
//            system.assertEquals(false, c.All_Images_Received__c);
//        }
//        system.Test.stopTest();
//    }
//    
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - create image links that are more than 650ft away from property and assert that the case associated with the links
//    //has its location_gps_verified field updated to 'No'
//    public static testmethod void testInvalidDistance(){
//        system.Test.startTest();
//        generateTestData();
//        
//        //create image links that are over 650ft away from the property
//        List<ImageLinks__c> imagesToInsert = new List<ImageLinks__c>();
//        for(integer i = 1; i < 7; i++){
//            integer addTen = 10;
//            integer seconds = 1385483160 + addTen;
//            ImageLinks__c photo = new ImageLinks__c(
//                CaseId__c = newCase.Id,
//                GPS_Timestamp__c = datetime.newInstance(0).addSeconds(seconds),
//                Pruvan_uuid__c = string.valueOf(i),
//                GPS_Location__Latitude__s = 30.137721,
//                GPS_Location__Longitude__s = -81.773646,
//                Pruvan_gpsAccuracy__c = i
//            );
//            imagesToInsert.add(photo);
//            addTen += 10;
//        }
//        insert imagesToInsert;
//        
//        Case testCase = [SELECT Location_GPS_Verified__c FROM Case WHERE Id=: newCase.Id];
//        system.assertEquals('No', testCase.Location_GPS_Verified__c);
//        
//        system.Test.stopTest();
//    }
//    
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - create image links that are more than 650ft away from property and assert that the case associated with the links
//    //has its location_gps_verified field updated to 'No'
//    public static testmethod void testValidDistance(){
//        system.Test.startTest();
//        generateTestData();
//        
//        //create image links that are over 650ft away from the property
//        List<ImageLinks__c> imagesToInsert = new List<ImageLinks__c>();
//        for(integer i = 1; i < 7; i++){
//            integer addTen = 10;
//            integer seconds = 1385483160 + addTen;
//            if(i == 6){
//                ImageLinks__c photo = new ImageLinks__c(
//                    CaseId__c = newCase.Id,
//                    GPS_Timestamp__c = datetime.newInstance(0).addSeconds(seconds),
//                    Pruvan_uuid__c = string.valueOf(i),
//                    GPS_Location__Latitude__s = 30.137721,
//                    GPS_Location__Longitude__s = -81.773646,
//                    Pruvan_gpsAccuracy__c = i
//                );
//                imagesToInsert.add(photo);
//            } else {
//                ImageLinks__c photo = new ImageLinks__c(
//                    CaseId__c = newCase.Id,
//                    GPS_Timestamp__c = datetime.newInstance(0).addSeconds(seconds),
//                    Pruvan_uuid__c = string.valueOf(i),
//                    GPS_Location__Latitude__s = 30.099307,
//                    GPS_Location__Longitude__s = -81.75276,
//                    Pruvan_gpsAccuracy__c = i
//                );
//                imagesToInsert.add(photo);
//            }
//            addTen += 10;
//        }
//        insert imagesToInsert;
//        
//        Case testCase = [SELECT Location_GPS_Verified__c FROM Case WHERE Id=: newCase.Id];
//        system.assertEquals('Yes', testCase.Location_GPS_Verified__c);
//        
//        system.Test.stopTest();
//    }
//    
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - create image links that have no gps accuracy, in which case
//    //the location GPS verified field should be 'No'
//    public static testmethod void testNoAccuracy(){
//        system.Test.startTest();
//        generateTestData();
//        
//        //create image links that are over 650ft away from the property
//        List<ImageLinks__c> imagesToInsert = new List<ImageLinks__c>();
//        for(integer i = 1; i < 7; i++){
//            integer addTen = 10;
//            integer seconds = 1385483160 + addTen;
//            if(i == 6){
//                ImageLinks__c photo = new ImageLinks__c(
//                    CaseId__c = newCase.Id,
//                    GPS_Timestamp__c = datetime.newInstance(0).addSeconds(seconds),
//                    Pruvan_uuid__c = string.valueOf(i),
//                    GPS_Location__Latitude__s = 30.137721,
//                    GPS_Location__Longitude__s = -81.773646
//                );
//                imagesToInsert.add(photo);
//            } else {
//                ImageLinks__c photo = new ImageLinks__c(
//                    CaseId__c = newCase.Id,
//                    GPS_Timestamp__c = datetime.newInstance(0).addSeconds(seconds),
//                    Pruvan_uuid__c = string.valueOf(i),
//                    GPS_Location__Latitude__s = 30.099307,
//                    GPS_Location__Longitude__s = -81.75276
//                );
//                imagesToInsert.add(photo);
//            }
//            addTen += 10;
//        }
//        insert imagesToInsert;
//        
//        Case testCase = [SELECT Location_GPS_Verified__c FROM Case WHERE Id=: newCase.Id];
//        system.assertEquals('No', testCase.Location_GPS_Verified__c);
//        
//        system.Test.stopTest();
//    }
}