@isTest(seeAllData=false) 
private class propertyGeocodeControllerTester {
///**
// *  Purpose         :   This is used for testing and covering propertyGeocodeController.
// *
// *  Modified By		:   Padmesh Soni
// *
// *  Modified Date	:   1/12/2015
// *
// *  Current Version :   V1.0
// *
// *  Revision Log    :   V1.0 - Modified - CCP-15:Fill County on Property
// *								Changes are:
// *                                  1. New feature of filling County__c field with getting result from Google map of 
// *                                  	attribute "administrative_area_level_2".
// *                                  2. Code fromatted with the appropriate code comments.
// *
// *  Coverage        :   88% - V1.0
// **/
// 
//	private static Geocode_Cache__c newProperty;
//    
//    //--------------------------------------------------------
//    //PRE-TEST SETUP
//    //create a new property
//    private static void insertProperty(){
//        newProperty = new Geocode_Cache__c(
//            Address_Hash__c = '400cedarstbrdgprtct60562',
//            Formatted_Address__c = '400 CEDAR ST, BRIDGEPORT, CT 60562',
//            Formatted_Street_Address__c = '400 CEDAR ST',
//            Formatted_City__c = 'BRIDGEPORT',
//            Formatted_State__c = 'CT',
//            Formatted_Zip_Code__c = '60562',
//            Street_Address__c = '400 Cedar St',
//            City__c = 'Bridgeport',
//            State__c = 'CT',
//            Zip_Code__c = '60562',
//            Location__Latitude__s = 0,
//            Location__Longitude__s = 0
//        );
//        insert newProperty;
//    }
//    
//    //--------------------------------------------------------
//    //PRE-TEST SETUP
//    //change the property fields
//    private static void changePropertyFields(propertyGeocodeController controller){
//        controller.param_lat = '29.9798741';
//        controller.param_lng = '-90.36946109999997';
//        controller.param_street = '56 Belle Helene Dr.';
//        controller.param_city = 'Destrehan';
//        controller.param_state = 'LA';
//        controller.param_zip = '70047';
//        controller.param_notes = 'ss';
//        
//        //Code added - Padmesh Soni (1/12/2015) - CCP-15:Fill County on Property
//        //Set class attribute as test data
//		controller.param_county = 'Destrehan';
//    }
//    
//    //--------------------------------------------------------
//    //PRE-TEST SETUP
//    //create a zillow property
//    private static void insertZillowProperty(){
//        newProperty = new Geocode_Cache__c(
//            Address_Hash__c = '2114bigelowaven',
//            Formatted_Address__c = '2114 BIGELOW AVE N, SEATTLE, WA 98109',
//            Formatted_Street_Address__c = '2114 BIGELOW AVE N',
//            Formatted_City__c = 'SEATTLE',
//            Formatted_State__c = 'WA',
//            Formatted_Zip_Code__c = '98109',
//            Street_Address__c = '2114 Bigelow Ave N',
//            City__c = 'Seattle',
//            State__c = 'WA',
//            Zip_Code__c = '98109',
//            Location__Latitude__s = 0,
//            Location__Longitude__s = 0
//        );
//        insert newProperty;
//    }
//    
//    //--------------------------------------------------------
//    //TESTS
//    //Unit tests for method testing and regression testing. 
//    //--------------------------------------------------------
//        
//    /*/--------------------------------------------------------
//    //TEST:
//    //Scenario - testing the standard functionality of the MassGeocodeWorkOrderPropertiesController
//    static testMethod void myUnitTest() {
//        insertProperty();
//        
//        //instantiate page
//        PageReference pageRef = Page.PropertyGeocoderMap;
//        Test.setCurrentPage(pageRef);
//        ApexPages.StandardController stdController = new ApexPages.standardController(newProperty);
//        propertyGeocodeController controller = new propertyGeocodeController(stdController);
//        
//        //change controller property variables
//        //changePropertyFields(controller);
//        
//        //perform controller property update
//        controller.saveAndRelease();
//        
//        //asserts
//        //query and assert field updates
//        Geocode_Cache__c testProperty = [SELECT Id, Last_Geocoded__c, Formatted_Street_Address__c, Formatted_City__c, Formatted_State__c, Formatted_Zip_Code__c, Location__Latitude__s, Location__Longitude__s, Notes__c, Geocoder_Process__c
//                           FROM Geocode_Cache__c
//                           WHERE Id =: newProperty.Id];
//                           
//        system.assertEquals(controller.property.Id, newProperty.Id);
//        system.assertEquals(testProperty.Formatted_Street_Address__c, '56 Belle Helene Dr.');
//        system.assertEquals(testProperty.Formatted_City__c, 'Destrehan');
//        system.assertEquals(testProperty.Formatted_State__c, 'LA');
//        system.assertEquals(testProperty.Formatted_Zip_Code__c, '70047');
//        system.assertEquals(testProperty.Location__Latitude__s, 29.9798741);
//        system.assertEquals(testProperty.Location__Longitude__s, -90.36946109999997);
//        system.assertEquals(testProperty.Notes__c, 'ss');
//        system.assertEquals(testProperty.Last_Geocoded__c, date.today());
//        system.assertEquals('Geocoder', testProperty.Geocoder_Process__c);
//    }*/
//    
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - testing the zillow functionality
//    static testMethod void zillowTest() {
//        insertZillowProperty();
//        
//        //instantiate page
//        PageReference pageRef = Page.PropertyGeocoderMap;
//        Test.setCurrentPage(pageRef);
//        
//        //Code added - Padmesh Soni - 03/06/2014 - As per Debugging the Geocode Button Subsystem assignment
//        //Test starts here 
//        Test.startTest();
//        
//        //Code added - Padmesh Soni - 03/06/2014 - As per Debugging the Geocode Button Subsystem assignment
//        //Code for setting mock response into testing
//        //Set Mock Callout class 
//        Test.setMock(HttpCalloutMock.class, new ZillowControllerMockClass());
//        
//        ApexPages.StandardController stdController = new ApexPages.standardController(newProperty);
//        propertyGeocodeController controller = new propertyGeocodeController(stdController);
//        
//        //change controller property variables
//        controller.param_lat = '47.637933';
//        controller.param_lng = '-122.347938';
//        controller.param_street = '2114 Bigelow Ave N';
//        controller.param_city = 'Seattle';
//        controller.param_state = 'WA';
//        controller.param_zip = '98109';
//        controller.param_notes = 'Notes';
//        
//        //perform controller property update
//        controller.saveAndRelease();
//        
//        //Code added - Padmesh Soni - 03/06/2014 - As per Debugging the Geocode Button Subsystem assignment
//        //Test stops here
//        Test.stopTest();
//        
//        //asserts
//        system.assertNotEquals(null, controller.zillowResWrapper);
//        system.assertEquals(4680, controller.zillowLotSize);
//        system.assertEquals('Code: 0, Request successfully processed', controller.zillowStatus);
//        
//        for(Geocode_Cache__c gc : [SELECT Id, SqFt_of_Lot_Size__c, Zillow_Status__c, Geo_System__c, Geo_Timestamp__c FROM Geocode_Cache__c WHERE Id =: newProperty.Id]){
//            system.assertEquals(4680, gc.SqFt_of_Lot_Size__c);
//            system.assertEquals('Code: 0, Request successfully processed', gc.Zillow_Status__c);
//            system.assertEquals('propertyGeocodeController.cls', gc.Geo_System__c);
//            system.assert(Datetime.now() >= gc.Geo_Timestamp__c);
//            system.assert(Datetime.now().addMinutes(-1) < gc.Geo_Timestamp__c);
//            delete gc;
//        }
//    }
//    
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - testing the zillow functionality when there is a null address
//    static testMethod void zillowTestNullAddress() {
//        insertZillowProperty();
//        
//        for(Geocode_Cache__c gc : [SELECT Street_Address__c FROM Geocode_Cache__c WHERE Id =: newProperty.Id]){
//            gc.Street_Address__c = null;
//            update gc;
//        }
//        
//        //instantiate page
//        PageReference pageRef = Page.PropertyGeocoderMap;
//        Test.setCurrentPage(pageRef);
//        
//        //Code added - Padmesh Soni - 03/06/2014 - As per Debugging the Geocode Button Subsystem assignment
//        //Test starts here 
//        Test.startTest();
//        
//        //Code added - Padmesh Soni - 03/06/2014 - As per Debugging the Geocode Button Subsystem assignment
//        //Setting response number of Mock response
//        ZillowControllerMockClass.noOfResponse = 3;
//        
//        //Code for setting mock response into testing
//        //Set Mock Callout class 
//        Test.setMock(HttpCalloutMock.class, new ZillowControllerMockClass());
//        
//        ApexPages.StandardController stdController = new ApexPages.standardController(newProperty);
//        propertyGeocodeController controller = new propertyGeocodeController(stdController);
//        string emptyAddress = null;
//        //change controller property variables
//        controller.param_lat = '47.637933';
//        controller.param_lng = '-122.347938';
//        controller.param_street = null;
//        controller.param_city = 'Seattle';
//        controller.param_state = 'WA';
//        controller.param_zip = '98109';
//        controller.param_notes = 'Notes';
//        
//        controller.getZillowLotSize(emptyAddress, 'Seattle', 'WA', '98109');
//        //perform controller property update
//        controller.saveAndRelease();
//        
//        //Code added - Padmesh Soni - 03/06/2014 - As per Debugging the Geocode Button Subsystem assignment
//        //Test stops here
//        Test.stopTest();
//        
//        //asserts
//        system.assertNotEquals(null, controller.zillowResWrapper);
//        system.assertEquals(0, controller.zillowLotSize);
//        system.assertEquals('Please make sure all address fields are filled out correctly', controller.zillowStatus);
//     	
//     	for(Geocode_Cache__c gc : [SELECT SqFt_of_Lot_Size__c, Zillow_Status__c, Geo_System__c, Geo_Timestamp__c FROM Geocode_Cache__c WHERE Id =: newProperty.Id]){
//            system.assertEquals(0, gc.SqFt_of_Lot_Size__c);
//            system.assertEquals('Please make sure all address fields are filled out correctly', gc.Zillow_Status__c);
//            system.assertEquals('propertyGeocodeController.cls', gc.Geo_System__c);
//            system.assert(Datetime.now() >= gc.Geo_Timestamp__c);
//            system.assert(Datetime.now().addMinutes(-1) < gc.Geo_Timestamp__c);
//        }
//    }
//    
//    //Code added - Padmesh Soni - 03/06/2014 - As per Debugging the Geocode Button Subsystem assignment
//    //Scenario - testing the standard functionality of the MassGeocodeWorkOrderPropertiesController
//    static testMethod void testZillowWithNoLotSize() {
//     
//        insertProperty();
//        
//        //instantiate page
//        PageReference pageRef = Page.PropertyGeocoderMap;
//        Test.setCurrentPage(pageRef);
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Set mock response of no. 1
//        ZillowControllerMockClass.noOfResponse = 1;
//        
//        //Code for setting mock response into testing
//        //Set Mock Callout class 
//        Test.setMock(HttpCalloutMock.class, new ZillowControllerMockClass());
//        
//        ApexPages.StandardController stdController = new ApexPages.standardController(newProperty);
//        propertyGeocodeController controller = new propertyGeocodeController(stdController);
//        
//        //change controller property variables
//        changePropertyFields(controller);
//        
//    	//perform controller property update
//        controller.saveAndRelease();
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //Query result of Property(Geocode_Cache__c)
//        Geocode_Cache__c property = [SELECT Zillow_Status__c, Geo_System__c, Geo_Timestamp__c, County__c FROM Geocode_Cache__c WHERE Id =: newProperty.Id];
//        system.assertEquals('The request was successfully processed, but there was no return on the lot size', property.Zillow_Status__c);
//        system.assertEquals('propertyGeocodeController.cls', property.Geo_System__c);
//        system.assert(Datetime.now() >= property.Geo_Timestamp__c);
//        system.assert(Datetime.now().addMinutes(-1) < property.Geo_Timestamp__c);
//        
//        //Code added - Padmesh Soni (1/12/2015) - CCP-15:Fill County on Property
//		//Assert for county value on Property
//		system.assertEquals('Destrehan', property.County__c);
//    }   
//    
//    //Code added - Padmesh Soni - 03/06/2014 - As per Debugging the Geocode Button Subsystem assignment
//    //Scenario - testing the standard functionality of the MassGeocodeWorkOrderPropertiesController
//    static testMethod void testZillowWithErrorCode() {
//     
//        insertProperty();
//        
//        //instantiate page
//        PageReference pageRef = Page.PropertyGeocoderMap;
//        Test.setCurrentPage(pageRef);
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Set mock response of no. 1
//        ZillowControllerMockClass.noOfResponse = 2;
//        
//        //Code for setting mock response into testing
//        //Set Mock Callout class 
//        Test.setMock(HttpCalloutMock.class, new ZillowControllerMockClass());
//        
//        ApexPages.StandardController stdController = new ApexPages.standardController(newProperty);
//        propertyGeocodeController controller = new propertyGeocodeController(stdController);
//        
//        //change controller property variables
//        changePropertyFields(controller);
//        
//    	//perform controller property update
//        controller.saveAndRelease();
//        
//        //Test stops here
//        Test.stopTest();
//        
//        Geocode_Cache__c property = [SELECT Zillow_Status__c, Geo_System__c, Geo_Timestamp__c FROM Geocode_Cache__c WHERE Id =: newProperty.Id];
//        system.assertEquals('Code: 508, Error: no exact match found for input address', property.Zillow_Status__c);
//        system.assertEquals('propertyGeocodeController.cls', property.Geo_System__c);
//        system.assert(Datetime.now() >= property.Geo_Timestamp__c);
//        system.assert(Datetime.now().addMinutes(-1) < property.Geo_Timestamp__c);
//    }
}