global class ScheduleBatchDeleteStatuses implements Schedulable{
    global void execute(SchedulableContext ctx) {
        BatchDeleteStatuses b = new BatchDeleteStatuses();
        Database.executeBatch(b, 200);
    }
}