@isTest(SeeAllData=true)
public class RHX_TEST_Property_Bill {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Property_Bill__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Property_Bill__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}