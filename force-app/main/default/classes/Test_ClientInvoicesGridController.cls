@isTest(seeAllData=false)
private class Test_ClientInvoicesGridController {
//
///**
// *  Purpose         :   This is used for testing and covering ClientInvoicesGridController functionality.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   01/22/2014
// *
// *  Current Version :   V1.1
// *
// *  Revision Log    :   V1.0 - Created
// *                      V1.1 - Modified - Padmesh Soni (1/23/2014) - SM-269- Invoices: Client page
// * 
// *  Coverage        :   50% - V1.0
// *                      100% - V1.1
// **/
//    
//    private static List<Account> accounts;
//    private static List<Contact> contacts;
//    private static Profile newProfile;
//    private static List<User> users;
//    private static Group queues;
//    
//    //Method to create all test method
//    private static void generateTestData(integer countData){
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP 
//        //create a new user with the Customer Portal Manager Standard profile
//        //newProfile = [SELECT Id FROM Profile WHERE Name = 'Customer Portal OrderSelect'];
//        newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Vendors'];
//        
//        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
//            //create a new user with the profile   
//            queues = new Group(Name='Queue',Type='Queue');
//            insert queues;
//            
//            //Queue sObject   
//            QueueSobject queueObj = new QueueSobject(QueueId = queues.Id, SobjectType = 'Case');
//            insert queueObj;
//        }
//        //Loop through countData time
//        for(integer i = 0; i < countData; i++) {
//        
//            /******** PRE-TEST SETUP create a new account *********/
//            accounts.add(new Account(Name = 'Tester Account' + i,RecordTypeId='0124000000011Q2'));
//        }
//        
//        insert accounts;
//        
//        //Loop through countData time
//        for(integer i = 0; i < countData; i++) {
//        
//            //--------------------------------------------------------
//            //PRE-TEST SETUP
//            //create a new contact
//            Contact newContact = new Contact(LastName = 'Tester'+i, AccountId = accounts[i].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active', RecordTypeId = '01240000000UbtfAAC');
//            newContact.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//            newContact.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//            newContact.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//            newContact.Shrub_Trim__c = 3;//Pricing...required 
//            newContact.REO_Grass_Cut__c = 3;//Pricing
//            newContact.Pay_Terms__c = 'Net 30';//Terms required
//            newContact.Trip_Charge__c = 25;//Pricing
//            newContact.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//            newContact.Date_Active__c = Date.Today();//Date Active must be set. 
//            newContact.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//            newContact.MailingStreet = '30 Turnberry Dr';
//            newContact.MailingCity = 'La Place';
//            newContact.MailingState = 'LA';
//            newContact.MailingPostalCode = '70068';
//            newContact.OtherStreet = '30 Turnberry Dr';
//            newContact.OtherCity = 'La Place';
//            newContact.OtherState = 'LA';
//            newContact.OtherPostalCode = '70068';
//            newContact.Other_Address_Geocode__Latitude__s = 30.00;
//            newContact.Other_Address_Geocode__Longitude__s = -90.00;
//            contacts.add(newContact);
//        }   
//        
//        insert contacts;
//        
//        //Loop through countData time
//        for(integer i = 0; i < countData; i++) {
//        
//            //create a new user with the profile
//            users.add(new User(alias = 'standt'+i, contactId = contacts[i].Id, email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8',
//                                    lastname = 'Testing'+i, languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//                                    timezonesidkey = 'Australia/Perth', username = 'standarduser'+i+'@test.com', isActive = true));
//        
//        }
//        
//        insert users;
//    }
//    
//    //Test method is used to test functionality of VendorSelfServeMapController
//    static testMethod void testVendorSelfServeMapController() {
//        
//        //initialize variables
//        accounts = new List<Account>();
//        contacts = new List<Contact>();
//        users = new List<User>();
//        
//        generateTestData(1);
//        
//        //Query record of Record Type of Account and Case objects
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: VSOAM_Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client') AND (SobjectType =: VSOAM_Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: VSOAM_Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(2, recordTypes.size());
//        
//        //Query result of User
//        User user = [SELECT Contact.AccountId FROM User WHERE Id=: users[0].Id AND ContactId != null];
//        
//        //Test starts here
//        Test.startTest();
//    
//        //Instance of controller
//        ClientInvoicesGridController controller = new ClientInvoicesGridController();
//        
//        String urlBase = controller.baseURL;
//        
//        //assert statement
//        //System.assert(controller.rInvoiceSerialized == '');
//                //List of Account to store testing records
//            List<Account> accountsOnOrders = new List<Account>();
//            accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//            accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//            
//            insert accountsOnOrders;
//            
//                    
//            
//
//            
//            Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//            insert property;
//            
//            //List to hold Case records
//            List<Case> wOrders = new List<Case>();
//            wOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id,
//                                        Client_Name__c =  accountsOnOrders[0].Id,
//                                        Due_Date__c = Date.today(), Status = VSOAM_Constants.CASE_STATUS_OPEN, 
//                                        Approval_Status__c = VSOAM_Constants.CASE_APPROVAL_STATUS_REJECTED,
//                                        Client__c = VSOAM_Constants.CASE_CLIENT_SG, Work_Order_Type__c = VSOAM_Constants.CASE_WORK_ORDER_TYPE_ROUTINE, 
//                                        State__c = VSOAM_Constants.CASE_STATE_TX));
//            
//            //insert case records
//            insert wOrders;
//            
//            //Query result of RecordType
//            String ARInvoiceTypeId = [select Id from RecordType where SobjectType = 'Invoice__c' and DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR limit 1].Id;
//            
//            //Test records for Invoices
//            List<Invoice__c> invoices = new List<Invoice__c>();
//            invoices.add(new Invoice__c(Work_Order__c = wOrders[0].Id, RecordTypeId = ARInvoiceTypeId, Sent__c = Date.today().addDays(-3), 
//                                            Receive_From__c = user.Contact.AccountId));
//            invoices.add(new Invoice__c(Work_Order__c = wOrders[0].Id, RecordTypeId = ARInvoiceTypeId, Sent__c = Date.today().addDays(-3), 
//                                            Receive_From__c = user.Contact.AccountId));
//            invoices.add(new Invoice__c(Work_Order__c = wOrders[0].Id, RecordTypeId = ARInvoiceTypeId, Sent__c = Date.today().addDays(-3), 
//                                            Receive_From__c = user.Contact.AccountId));
//            invoices.add(new Invoice__c(Work_Order__c = wOrders[0].Id, RecordTypeId = ARInvoiceTypeId, Sent__c = Date.today().addDays(-3), 
//                                            Receive_From__c = user.Contact.AccountId));
//            
//            insert invoices;
//            
//            System.debug('QUery Result :::: ' + [SELECT Id, Name, Receive_From__c, RecordType.DeveloperName FROM Invoice__c] + ' User AccountId' + user.Contact.AccountId);
//        //Running as Partner community user
//        System.runAs(user) {
//
//            
//            //Instance of controller
//            controller = new ClientInvoicesGridController();
//            
//            //assert statement
//            System.assert(controller.rInvoiceSerialized != '');
//            
//            String invTable = '';
//            
//            for(integer i = 1; i < ClientInvoicesGridController.CHUNK_SIZE; i++ ) {
//                
//                invTable += 'T';
//            }
//            
//            //Call SaveHTML method
//            String listViewObjectId = ClientInvoicesGridController.SaveHtml(invTable);
//            
//            //assert statement
//            System.assert(String.isNotBlank(listViewObjectId));
//            
//            //Call SaveHTML method
//            Pagereference pgRef = controller.GoToExportPage();
//            
//            invTable = '';
//            
//            for(integer i = 0; i <= ClientInvoicesGridController.CHUNK_SIZE + ClientInvoicesGridController.CHUNK_SIZE; i++ ) {
//                
//                invTable += 'Test'+ i;
//            }
//            
//            //Call SaveHTML method
//            listViewObjectId = ClientInvoicesGridController.SaveHtml(invTable);
//            
//            String testStr;
//            
//            try {
//                
//                //Call SaveHTML method
//                listViewObjectId = ClientInvoicesGridController.SaveHtml(testStr);
//            } catch(Exception e) {
//                
//                System.assert(e.getMessage().contains(Label.ORDER_GRID_SAVING_ERROR));
//            }
//            
//            //assert statement
//            System.assert(String.isNotBlank(listViewObjectId));
//        }
//
//        //Test stops here
//        Test.stopTest();
//    }
}