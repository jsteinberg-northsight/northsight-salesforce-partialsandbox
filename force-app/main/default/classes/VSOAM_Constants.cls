/**
 *  Purpose         :   This class is to hold the constant values for Vendor Self Serving assignment map functionality.
 *
 *  Create By       :   Padmesh Soni
 *
 *  Created Date    :   04/03/2014
 *
 *  Current Version :   V1.1
 *
 *	Revision Log	:	V1.0 - Created
 *						V1.1 - Modified - Padmesh Soni (1/29/2015) - SM-272:Update reassignment fee trigger to include the Client_Name__c field 
 **/
public with sharing class VSOAM_Constants {
	
	/****** Constants for  Vendor Self Serving assignment map functionality ******/
    public static final String TIMEZONE_CST_ID_AMERICA_EL_SALVADOR = 'America/El_Salvador';
    public static final String TIMEZONE_CST_ID_AMERICA_CHICAGO = 'America/Chicago';
    public static final integer TZ_OFFSET_DIVISOR = 3600000;
    public static final String PROFILE_NAME_CUSTOMER_PORTAL_ORDERSELECT = 'Partner Community - Vendors OrderSelect';
    public static final String PROFILE_NAME_PARTNER_COMMUNITY_VENDORS = 'Partner Community - Vendors';
    public static final String PROFILE_NAME_PARTNER_COMMUNITY_VENDORS_LOGIN = 'Partner Community - Vendors';
    public static final String PROFILE_NAME_COMMUNITY_PLUS_USER = 'Community Plus User';
    public static final boolean BOOL_ON = true;
    public static final boolean BOOL_OFF = false;
    public static final String USER_ISACTIVE_ACTIVE = 'Active';
    
    public static final String URL_PARAM_MINLAT = 'minLat';
    public static final String URL_PARAM_MINLON = 'minLon';
    public static final String URL_PARAM_MAXLAT = 'maxLat';
    public static final String URL_PARAM_MAXLON = 'maxLon';
    public static final String URL_PARAM_CONTACT_LAT = 'contactLat';
    public static final String URL_PARAM_CONTACT_LON = 'contactLon';
    public static final String URL_PARAM_CONTACT_SERVICE_RANGE = 'conSrvcRange';
    public static final String CASE_WORK_ORDER_TYPE_ROUTINE = 'Routine';
    public static final String CASE_WORK_ORDER_TYPE_MAINTENANCE = 'Maintenance';
    
    public static final String CASE_APPROVAL_STATUS_REJECTED = 'Rejected';
    public static final String CASE_QUEUE_NAME_OS = 'OS';
    public static final String GROUP_TYPE_QUEUE = 'Queue';
    public static final String CASE_ROUTING_SERVICEABLE_YES = 'Yes';
    
    public static final String CASE_SOBJECTTYPE = 'Case';
    public static final String CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER = 'NEW_ORDER';
    public static final String CASE_STATUS_OPEN = 'Open';
    public static final String CASE_CLIENT_SG = 'SG';
    public static final String CASE_CLIENT_NOT_SG = 'NOT-SG';
    public static final String ACCOUNT_SOBJECTTYPE = 'Account';
    
    //Code modified - Padmesh Soni (1/29/2015) - SM-272:Update reassignment fee trigger to include the Client_Name__c field
    //Variable value updated from "Customer" -> "Client"
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER = 'Client';
    
    public static final String CASE_STATE_TX = 'TX';
    public static final String CASE_STATE_LA = 'LA';
    public static final String CASE_STATE_AR = 'AR';
    
    public static final String DECIMAL_POINT = '\\.';
    public static final String MINUS = '-';
    public static final String DML_EXCEPTION_UNABLE_TO_LOCK_ROW = 'UNABLE_TO_LOCK_ROW';
    
    public static final String VENDOR_CONTACT_ORDER_COUNT = '{!VendorContactOrderCount}';
    public static final String VENDOR_CONTACT_GRASS_CUT_CAP = '{!VendorContactGrassCutCap}';
    public static final String ORDER_SIZE = '{!OrderSize}';
    public static final String ORDER_ASSIGN_LIMIT = '{!limitAssign}';
    public static final String CASE_SERVICEABLE_YES = 'Yes';
}