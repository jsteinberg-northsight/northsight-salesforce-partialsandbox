@isTest()
public without sharing class FieldSetterTest {
//	//--------------------------------------------------------------------------
//	//TESTS
//	//--------------------------------------------------------------------------
//	//---------------------------------------------------------------------------
//	//Interprets the value as true or false and sets the field. 
//	//---------------------------------------------------------------------------
//	private static testmethod void testSetBooleanFieldValue(){
//			Case c = new Case();
//			FieldHelper.setFieldValue(c, 'IsVisibleInSelfService', 'Y');
//			system.assert(c.IsVisibleInSelfService);
//			FieldHelper.setFieldValue(c, 'IsVisibleInSelfService', 'YES');
//			system.assert(c.IsVisibleInSelfService);
//			FieldHelper.setFieldValue(c, 'IsVisibleInSelfService', '1');
//			system.assert(c.IsVisibleInSelfService);
//			FieldHelper.setFieldValue(c, 'IsVisibleInSelfService', 'T');
//			system.assert(c.IsVisibleInSelfService);
//			FieldHelper.setFieldValue(c, 'IsVisibleInSelfService', 'True');
//			system.assert(c.IsVisibleInSelfService);
//			FieldHelper.setFieldValue(c, 'IsVisibleInSelfService', 'N');
//			system.assert(!c.IsVisibleInSelfService);
//			FieldHelper.setFieldValue(c, 'IsVisibleInSelfService', 'NO');
//			system.assert(!c.IsVisibleInSelfService);
//			FieldHelper.setFieldValue(c, 'IsVisibleInSelfService', '0');
//			system.assert(!c.IsVisibleInSelfService);
//			FieldHelper.setFieldValue(c, 'IsVisibleInSelfService', 'F');
//			system.assert(!c.IsVisibleInSelfService);
//			FieldHelper.setFieldValue(c, 'IsVisibleInSelfService', 'FALSE');
//			system.assert(!c.IsVisibleInSelfService);
//	}
//	//---------------------------------------------------------------------------
//	//Simply casts the value to a string and sets the field.  
//	//---------------------------------------------------------------------------
//	private static testmethod void testSetStringFieldValue(){
//		Contact c = new Contact();	
//		object val = 'Test';
//		FieldHelper.setFieldValue(c, 'LastName', val);
//		system.assertEquals(c.LastName,(string)val);
//	}
//	//---------------------------------------------------------------------------
//	//Attempts to cast the value to an integer before setting the field. 
//	//---------------------------------------------------------------------------
//	private static testmethod void testSetIntegerFieldValue(){
//		Case c = new Case();
//		object val = 1;
//		FieldHelper.setFieldValue(c, 'Auto_Bounce_Count__c', val);
//		system.assertEquals((integer)c.Auto_Bounce_Count__c, (integer)val);
//	}
//	//---------------------------------------------------------------------------
//	//Attempts to cast the value to a decimal before setting the field. 
//	//---------------------------------------------------------------------------
//	private static testmethod void testSetDecimalFieldValue(){
//		Account a = new Account();
//		object val = '$14.00';
//		Decimal compareVal = 14.00;
//		FieldHelper.setFieldValue(a, 'AnnualRevenue', val);
//		system.assertEquals(a.AnnualRevenue, compareVal);
//	}
//	//---------------------------------------------------------------------------
//	//Attempts to cast the value to an integer before setting the field. 
//	//---------------------------------------------------------------------------
//	private static testmethod void testSetDoubleFieldValue(){
//		Case c = new Case();
//		object val = 1.00;
//		FieldHelper.setFieldValue(c, 'Auto_Bounce_Count__c', val);
//		system.assertEquals((double)c.Auto_Bounce_Count__c, (double)val);
//	}
//	//-------------------------------------------------------------------------------
//	//Attempts to cast the value to a Sakesforce sObject Id before setting the field. 
//	//-------------------------------------------------------------------------------
//	private static testmethod void testSetIdFieldValue(){
//		Case newCase = new Case(
//			street_address__c = '1234 Street',
//	        state__c = 'TX',
//	        city__c = 'Austin',
//	        zip_code__c = '78704',
//	        Vendor_code__c = 'SRSRSR',
//	        Work_Order_Type__c = 'Routine',
//	        Client__c = 'NOT-SG',
//	        Loan_Type__c = 'REO'
//	    );
//	    insert newCase;
//	    
//	    Case c = new Case();
//	    object val = newCase.Id;
//	    FieldHelper.setFieldValue(c, 'Id', val);
//	    system.assertEquals(c.Id, (Id)val);
//	}
//	//---------------------------------------------------------------------------
//	//Attempts to cast the value to a DateTime object before setting the field. 
//	//---------------------------------------------------------------------------
//	private static testmethod void testSetDateTimeFieldValue(){
//		Geocode_Cache__c g = new Geocode_Cache__c();
//		object val = '2013-10-25 00:00:00';
//		FieldHelper.setFieldValue(g, 'Last_Order_Updated_Date_Time__c', val);
//		system.assertEquals(g.Last_Order_Updated_Date_Time__c, Datetime.valueOf(string.valueOf(val)));
//	}
//	//---------------------------------------------------------------------------
//	//Attempts to cast the value to a Date object before setting the field. 
//	//---------------------------------------------------------------------------
//	private static testmethod void testSetDateFieldValue(){
//		Case c = new Case();
//		object val = Date.today();
//		FieldHelper.setFieldValue(c, 'Date_Serviced__c', val);
//		system.assertEquals(c.Date_Serviced__c, (date)val);
//	}
}