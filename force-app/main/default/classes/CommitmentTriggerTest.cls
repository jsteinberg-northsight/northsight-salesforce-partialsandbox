@isTest
private class CommitmentTriggerTest {
//	private static Account newAccount;
//	private static Contact newContact;
//	private static Case newCase;
//	private static Case newCase2;
//	private static Case newCase3;
//	private static List<Case> casesToInsert = new List<Case>();
//	private static Client_Commitment__c[] commits;
//	
//	private static void generateAccountAndContact(){
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//   		//create a new account
//		newAccount = new Account(
//			Name = 'Tester Account'
//		);
//		insert newAccount;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new contact
//		newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest',
//			MailingStreet = '30 Turnberry Dr',
//			MailingCity = 'La Place',
//			MailingState = 'LA',
//			MailingPostalCode = '70068',
//			OtherStreet = '30 Turnberry Dr',
//			OtherCity = 'La Place',
//			OtherState = 'LA',
//			OtherPostalCode = '70068',
//			Other_Address_Geocode__Latitude__s = 30.00,
//			Other_Address_Geocode__Longitude__s = -90.00
//		);
//		insert newContact;
//	}
//	
//	private static void generateCases() {
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new test case
//		newCase = new Case();
//		newCase.street_address__c = '2420 Ormond Blvd.';
//        newCase.state__c = 'LA';
//        newCase.city__c = 'Destrehan';
//        newCase.zip_code__c = '70047';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        newCase.Client__c = 'SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'REO Grass Cut/Lawn Care';
//        newCase.Approval_Status__c = 'Not Started';
//        newCase.Assigned_Date_Time__c = Date.today();
//        newCase.Due_Date__c = Date.today().addDays(-2);
//        newCase.Client_Commit_Date__c = Date.today().addDays(1);
//        newCase.Last_Open_Verification__c = Date.today();
//        
//        casesToInsert.add(newCase);
//        
//        //--------------------------------------------------------
//   		//PRE-TEST SETUP
//        //create a new test case
//		newCase2 = new Case();
//		newCase2.street_address__c = '1234 Street';
//        newCase2.state__c = 'TX';
//        newCase2.city__c = 'Austin';
//        newCase2.zip_code__c = '78704';
//        newCase2.Vendor_code__c = 'SRSRSR';
//        newCase2.Work_Order_Type__c = 'Routine';
//        newCase2.Client__c = 'SG';
//        newCase2.Loan_Type__c = 'REO';
//        newCase2.ContactId = newContact.Id;
//        newCase2.Work_Ordered__c = 'REO Grass Cut/Lawn Care';
//        newCase2.Approval_Status__c = 'Not Started';
//        newCase2.Assigned_Date_Time__c = Date.today();
//        newCase2.Due_Date__c = Date.today().addDays(-2);
//        newCase2.Client_Commit_Date__c = Date.today().addDays(1);
//        newCase2.Last_Open_Verification__c = Date.today();
//        
//        casesToInsert.add(newCase2);
//        
//        //--------------------------------------------------------
//   		//PRE-TEST SETUP
//        //create a new test case
//		newCase3 = new Case();
//		newCase3.street_address__c = '222 Peach Street';
//        newCase3.state__c = 'AR';
//        newCase3.city__c = 'Fayward';
//        newCase3.zip_code__c = '78704';
//        newCase3.Vendor_code__c = 'SRSRSR';
//        newCase3.Work_Order_Type__c = 'Routine';
//        newCase3.Client__c = 'SG';
//        newCase3.Loan_Type__c = 'REO';
//        newCase3.ContactId = newContact.Id;
//        newCase3.Work_Ordered__c = 'REO Grass Cut/Lawn Care';
//        newCase3.Approval_Status__c = 'Not Started';
//        newCase3.Assigned_Date_Time__c = Date.today();
//        newCase3.Due_Date__c = Date.today().addDays(-2);
//        newCase3.Client_Commit_Date__c = Date.today().addDays(1);
//        newCase3.Last_Open_Verification__c = Date.today();
//        
//        casesToInsert.add(newCase3);
//        
//        insert casesToInsert;
//	}
//	
//	private static void createClientCommitments(){
//		commits = new Client_Commitment__c[]{};
//		for(Case c : casesToInsert){
//			commits.add(new Client_Commitment__c(Work_Order__c=c.Id, Internal_Comments__c='the internal comments', Commitment_Date__c=Date.today(), Current_Status_of_Order__c='Not Started', Root_Cause_of_Delay__c='Field Capacity', Public_Comments__c='the public comments'));
//		}
//		insert commits;
//	}
//	
//	private static void createDuplicateClientCommitments(){
//		commits = new Client_Commitment__c[]{};
//		for(Integer i=0; i<2; i++){
//			commits.add(new Client_Commitment__c(Work_Order__c=newCase.Id, Internal_Comments__c='the internal comments', Commitment_Date__c=Date.today(), Current_Status_of_Order__c='Not Started', Root_Cause_of_Delay__c='Field Capacity', Public_Comments__c='the public comments'));
//		}
//		insert commits;
//	}
//	
//    static testMethod void positiveTest() {
//        	//inserts
//	        generateAccountAndContact();
//	        generateCases();
//	        createClientCommitments();
//	        
//	        //assert
//	        Client_Commitment__c[] clientCommits = [SELECT Id FROM Client_Commitment__c WHERE Work_Order__c IN: casesToInsert];
//	        system.assertEquals(3, clientCommits.Size());
//    }
//    
//    static testMethod void negativeTest() {
//    	try{
//    		//inserts
//    		generateAccountAndContact();
//        	generateCases();
//        	createDuplicateClientCommitments();
//    	}
//        catch(exception e){
//        	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage() + e.getStackTraceString());
//			ApexPages.addMessage(myMsg);
//			//assert the correct error message is returned
//			system.assertEquals(e.getMessage(), 'Insert failed. First exception on row 1; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Cannot add more than one new Commitment to the same Work Order.  Duplicate Commit found for Work Order: ' + newCase.Id + ': [Work_Order__c]');
//        }
//    }
}