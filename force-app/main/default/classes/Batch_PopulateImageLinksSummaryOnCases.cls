/**
 *  Purpose         :   This Batch is used for cleanup all Attachments related to Case object as parent with created date of one month ago.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   01/28/2014
 *
 *	Current Version	:	V1.2
 *
 *	Revision Log	:	V1.0 - Created
 *						V1.1 - Modified - Padmesh Soni(06/10/2014) - Summarize image links for any work orders that have not been summarized. 
 **/
global class Batch_PopulateImageLinksSummaryOnCases implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts {
	
	//String to hold errorLog on DML operations
	global String errorLog = '';
	global boolean runAgain;
	//Start method
    global Database.Querylocator start(Database.BatchableContext BC) {
    	
    	//return query result
    	//Query updated with adding filter criteria " Is_Summarized__c = false" - Padmesh Soni(06/10/2014) - 
    	//Summarize image links for any work orders that have not been summarized.
        return Database.getQueryLocator([select Id, CaseNumber,Latitude__c, Longitude__c,ImageLinks_Summary__c  from Case where LastModifiedDate = Last_N_Days:60 and Image_Summary_Current__c = false limit 1000]);
    }
    
    //Execute method
    global void execute(Database.BatchableContext BC, List<case>cases) {

    	//Build payload
    	string payload = buildPayload(cases);
    	//Send request
    			//HTTP Request
        HttpRequest request = new HttpRequest();
        
        //Set the EndPoint for the Request
        //request.setEndPoint('http://nsprelay-env.elasticbeanstalk.com/fetchImageLinks');
        request.setEndPoint('http://nspruvanrelay.elasticbeanstalk.com/fetchImageLinks');
        //Set the Method for the Request
        request.setMethod('POST');
        request.setTimeout(120000);
        //system.assertEquals('',payload);
        request.setBody('payload='+EncodingUtil.urlEncode(payload, 'UTF-8'));
        //HTTP Response
        Http http = new Http();
        HTTPResponse response = http.send(request);
    	
    	//Parse Response
    	string r = response.GetBody();
    	errorLog = r;
    	Map<id,List<ImageLinks__c>> links = new map<id,List<ImageLinks__c>>();
    	try{links = (Map<id,List<ImageLinks__c>>)JSON.deserialize(response.GetBody(),Map<id,List<ImageLinks__c>>.class);}catch(Exception e){}
    	//system.assertEquals('',response.GetBody());
    	//Calculate And Populate Fields
		Case[] casesForUpdate = new Case[]{};
		
		for(string i:links.KeySet()){
			if(i=='status'){
				continue;
			}
			integer imageCount=0;
			DateTime MinImageLinkDateTime;
			DateTime MaxImageLinkDateTime;
			Decimal MinAgeDuration;
			integer GPSOffCount = 0;
			integer MoreThan500ftCount;  
			if(links.get(i)!=null){
			for(ImageLinks__c link:links.get(i)){
				//Image Count    	
		    	imageCount++;
				//Min Image Link Date Time
				if(MinImageLinkDateTime == null || MinImageLinkDateTime > link.GPS_Timestamp__c){
					MinImageLinkDateTime = link.GPS_TIMESTAMP__C;
				}
				//Max Image Link Date Time
				if(MaxImageLinkDateTime == null || MaxImageLinkDateTime < link.GPS_Timestamp__c){
					MaxImageLinkDateTime = link.GPS_TIMESTAMP__C;
				}
				//Min Age Duration
				if(MinAgeDuration == null || MinAgeDuration > link.Age_Duration__c){
					MinAgeDuration = link.Age_Duration__c;
				}
				//GPS Off Count 
				if(link.Gps_Off__c == true){
					GPSOffCount++;
				}
				//Images More Than 500ft From Property
				if(link.DISTANCE_FROM_PROPERTY_IN_FT__C > 500){
					MoreThan500ftCount++;
				}
			}
			}
			Case c = new Case(Id = i,
							  Image_Count__c = imageCount, 
							  Image_Latency__c = MinAgeDuration,
							  MinImageLinkDateTime__c = MinImageLinkDateTime,
							  MaxImageLinkDateTime__c = MaxImageLinkDateTime,
							  Images_More_Than_500ft_From_Property__c = MoreThan500ftCount,
							  Gps_Off_Image_Count__c= GpsOffCount,
							  ImageLinks_Summary__c = json.serialize(links.get(i)),
							  Image_Summary_Current__c = true 
							  );
		   casesForUpdate.add(c);
		}
		//Update Cases 
		database.update(casesForUpdate,false);
    }
    public string buildPayload(Case[] cases){
    	List<woSend> wos = new List<woSend>();
    	for(Case c:cases){
    		woSend w = new woSend();
    		w.sfid = c.Id;
    		w.workOrderNumber = c.CaseNumber;
    		w.latitude = c.latitude__c;
    		w.longitude = c.longitude__c;
    		wos.add(w);
    	}
    	map<string,List<woSend>> p = new map<string,List<woSend>>();  
    	p.put('workOrders',wos);
    	return JSON.serialize(p);
    }
    class woSend{
    	public string sfid;
    	public string workOrderNumber;
    	public string latitude;
    	public string longitude;
    }
    // Finish method
    global void finish(Database.BatchableContext BC) {
    	/*
    	AsyncApexJob a = [SELECT Id, JobItemsProcessed, TotalJobItems, NumberOfErrors, CreatedBy.Email FROM AsyncApexJob
                            WHERE id = :BC.getJobId()];

        // Send email indicating the completion of this batch
        String emailMessage = 'Your batch job \"Batch_PopulateImageLinksSummaryOnCases\" has finished.  It processed job items'
                                + a.totalJobItems +
                                ' batches.  Of which, '
                                + a.JobItemsProcessed
                                + ' processed without any exceptions thrown and '
                                + a.numberOfErrors
                                + ' batches threw unhandled exceptions.' + '<BR/>' + errorLog;

		//Send batch status to the person who executed this batch
        EmailHandler.sendEmail(a.CreatedBy.Email, 'Batch_PopulateImageLinksSummaryOnCases has been completed', emailMessage, emailMessage);
        */
        
    }
    
    public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}	
}