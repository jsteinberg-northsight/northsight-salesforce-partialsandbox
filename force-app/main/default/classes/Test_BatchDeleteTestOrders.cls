@isTest(seeAllData=false)
private class Test_BatchDeleteTestOrders {
///**
// *  Purpose         :   This is used for testing and covering BatchDeleteTestOrders class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/06/2014
// *
// *	Current Version	:	V1.1
// *
// *	Revision Log	:	V1.0 - Created - Required Maintenance: Unit Test Improvements
// *						V1.1 - Modified - Padmesh Soni (06/18/2014) - Support Case Record Type	
// *	
// *	Coverage		:	100%
// **/
//    
//    //test method added to check the functionality with code coverage
//    static testMethod void myUnitTest() {
//        /*
//        //# between 69696000 - 69696999 to view under testOrders filter
//        integer refNum = 69696000;
//		
//		//Padmesh Soni (06/18/2014) - Support Case Record Type
//	    //Query record of Record Type of Case object
//		List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
//											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
//		
//		//Assert statement
//		System.assertEquals(1, recordTypes.size());
//		
//		//List to hold Case records
//        List<Case> testOrders = new List<Case>();
//        
//        //Loop through until count is less than 10	
//		for(integer i = 0; i < 9; i++){ 
//			testOrders.add(new Case(Street_Address__c = '2420 ormond blvd.', City__c = 'Destrehan', State__c = 'LA', Zip_Code__c = '7004'+i,
//			    						Due_Date__c = date.today().addDays(2), Client__c = 'NOT-SG', Vendor_Code__c = 'SRSRSR', Work_Ordered__c = 'Initial Grass Cut',
//									    Reference_Number__c = string.valueOf(refNum), OwnerId = '00540000001yas1AAA'));
//			
//			refNum++;
//		}
//		
//		//Padmesh Soni (06/18/2014) - Support Case Record Type
//	    //add one more Case record with "SUPPORT" recordtype
//		testOrders.add(new Case(RecordTypeId = recordTypes[0].Id, Street_Address__c = '2420 ormond blvd.', City__c = 'Destrehan', State__c = 'LA', Zip_Code__c = '70058',
//			    						Due_Date__c = date.today().addDays(2), Client__c = 'NOT-SG', Vendor_Code__c = 'SRSRSR', 
//			    						Work_Ordered__c = 'Initial Grass Cut', Reference_Number__c = string.valueOf(refNum), 
//			    						OwnerId = '00540000001yas1AAA'));
//			
//		//insert test records of Case sobject
//		insert testOrders;
//		
//    	//Test starts here
//    	Test.startTest();
//    	
//    	//Execute the batch
//    	Database.executeBatch(new BatchDeleteTestOrders(), 200);
//    	
//    	//Test stops here
//    	Test.stopTest();
//    	
//    	//Query result of Cases
//    	testOrders = [SELECT Id FROM Case];
//    	
//    	//Assert statement here
//    	System.assertEquals(1, testOrders.size());
//    */
//    }
//    
}