/**
 *  Purpose         :   This is a utility class to encapsulate all needed Zillow api calls. 
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   12/11/2013
 *
 *  Current Version :   V1.1
 *
 *  Revision Log    :   V1.0 - Created
 *                      V1.1 - Modified - New Method "fetchListingFromZillow" added to getting Listing(Property) details from Zillow - Padmesh Soni - 12/18/2013  
 **/
public with sharing class ZillowAPIUtility {
    
    /**
     *  @description    :   This method is used to fetch Listing from Zillow related to Listing or property
     *
     *  @param          :   Address, City, State, Zip/Postal Code 
     *
     *  @return         :   ZillowResponseWrapper 
     **/
     public static ZillowResponseWrapper fetchListingFromZillow(String address, String city, String state, String zipCode) {
        
        //String to hold ZWS Id of Zillow
        Zillow_Configuration__c configDefault = Constants.CONFIG_DEFAULT;
        
        //replace whitespaces with pluse 
        address = address.trim().replace(' ', '+');
        
        //Double to hold Lot Size
        Double latitudeVal;     
        Double longitudeVal;    
        
        //Get City State Zip from Listing Address fields
        String citystatezip = city + ' ' + state + ' ' +  zipCode;
        citystatezip = citystatezip.trim().replace(' ', '+');
        
		//HTTP Request
        HttpRequest request = new HttpRequest();
        
        //Set the EndPoint for the Request
        request.setEndPoint('http://www.zillow.com/webservice/GetDeepSearchResults.htm?zws-id=' + configDefault.ZWS_Id__c + '&address=' + address + '&citystatezip=' + citystatezip);
        
        //System.debug('Request ::::::'+ request.getEndpoint());
              
        //Set the Method for the Request
        request.setMethod('GET');
        
        //HTTP Response
        Http http = new Http();
        HTTPResponse response = http.send(request);
        
        //System.debug('response :::::'+ response.getBody());
        
        //Parse response and create a bean for it
        ZillowResponseWrapper zRWrapper = new ZillowResponseWrapper(response.getBodyDocument());
        
        //return lot size
        return zRWrapper;
     }
}