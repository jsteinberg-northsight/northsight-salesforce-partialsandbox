@isTest(SeeAllData=false)
public without sharing class MassCommitmentTester {
//	private static Account newAccount;
//	private static Contact newContact;
//	private static Case newCase;
//	private static Case newCase2;
//	private static Case newCase3;
//	private static Case newCase4;
//	private static List<Case> casesToInsert = new List<Case>();
//	private static Datetime oneHour;
//
//	//Padmesh Soni (06/23/2014) - Support Case Record Type
//	//Query record type of Case Object "SUPPORT"    
//	private static List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
//										AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true ORDER BY DeveloperName];
//	
//	private static void generateTestObjects(){
//		generateAccountAndContact();
//		generateAndInsertCases();
//	}
//	
//	private static void generateAndInsertCases(){
//		generateDefaultFilterCases();		
//		insert casesToInsert;
//	}
//
//	
//	private static void generateAccountAndContact(){
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//   		//create a new account
//		newAccount = new Account(
//			Name = 'Tester Account'
//		);
//		insert newAccount;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new contact
//		newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest',
//			MailingStreet = '30 Turnberry Dr',
//			MailingCity = 'La Place',
//			MailingState = 'LA',
//			MailingPostalCode = '70068',
//			OtherStreet = '30 Turnberry Dr',
//			OtherCity = 'La Place',
//			OtherState = 'LA',
//			OtherPostalCode = '70068',
//			Other_Address_Geocode__Latitude__s = 30.00,
//			Other_Address_Geocode__Longitude__s = -90.00
//		);
//		insert newContact;
//	}
//	
//	private static void generateDefaultFilterCases() {
//		
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//   		//set oneHour = Datetime.now - 3hours
//		oneHour = Datetime.now().addHours(-1);
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new test case
//		newCase = new Case();
//		newCase.street_address__c = '2420 Ormond Blvd.';
//        newCase.state__c = 'LA';
//        newCase.city__c = 'Destrehan';
//        newCase.zip_code__c = '70047';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        newCase.Client__c = 'SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'REO Grass Cut/Lawn Care';
//        newCase.Approval_Status__c = 'Not Started';
//        newCase.Assigned_Date_Time__c = Date.today();
//        newCase.Due_Date__c = Date.today().addDays(-2);
//        newCase.Client_Commit_Date__c = Date.today().addDays(1);
//        newCase.Last_Open_Verification__c = Date.today();
//        
//        casesToInsert.add(newCase);
//        
//        //--------------------------------------------------------
//   		//PRE-TEST SETUP
//        //create a new test case
//		newCase2 = new Case();
//		newCase2.street_address__c = '1234 Street';
//        newCase2.state__c = 'TX';
//        newCase2.city__c = 'Austin';
//        newCase2.zip_code__c = '78704';
//        newCase2.Vendor_code__c = 'SRSRSR';
//        newCase2.Work_Order_Type__c = 'Routine';
//        newCase2.Client__c = 'SG';
//        newCase2.Loan_Type__c = 'REO';
//        newCase2.ContactId = newContact.Id;
//        newCase2.Work_Ordered__c = 'REO Grass Cut/Lawn Care';
//        newCase2.Approval_Status__c = 'Not Started';
//        newCase2.Assigned_Date_Time__c = Date.today();
//        newCase2.Due_Date__c = Date.today().addDays(-2);
//        newCase2.Client_Commit_Date__c = Date.today().addDays(1);
//        newCase2.Last_Open_Verification__c = Date.today();
//        
//        casesToInsert.add(newCase2);
//        
//        //--------------------------------------------------------
//   		//PRE-TEST SETUP
//        //create a new test case
//		newCase3 = new Case();
//		newCase3.street_address__c = '222 Peach Street';
//        newCase3.state__c = 'AR';
//        newCase3.city__c = 'Fayward';
//        newCase3.zip_code__c = '78704';
//        newCase3.Vendor_code__c = 'SRSRSR';
//        newCase3.Work_Order_Type__c = 'Routine';
//        newCase3.Client__c = 'SG';
//        newCase3.Loan_Type__c = 'REO';
//        newCase3.ContactId = newContact.Id;
//        newCase3.Work_Ordered__c = 'REO Grass Cut/Lawn Care';
//        newCase3.Approval_Status__c = 'Not Started';
//        newCase3.Assigned_Date_Time__c = Date.today();
//        newCase3.Due_Date__c = Date.today().addDays(-2);
//        newCase3.Client_Commit_Date__c = Date.today().addDays(1);
//        newCase3.Last_Open_Verification__c = Date.today();
//        
//        casesToInsert.add(newCase3);
//        
//        //Padmesh Soni (06/23/2014) - Support Case Record Type
//		//create a new test case
//		newCase4 = new Case(RecordTypeId = recordTypes[0].Id);
//		newCase4.street_address__c = '222 Peach Street';
//        newCase4.state__c = 'AR';
//        newCase4.city__c = 'Fayward';
//        newCase4.zip_code__c = '78704';
//        newCase4.Vendor_code__c = 'SRSRSR';
//        newCase4.Work_Order_Type__c = 'Routine';
//        newCase4.Client__c = 'SG';
//        newCase4.Loan_Type__c = 'REO';
//        newCase4.ContactId = newContact.Id;
//        newCase4.Work_Ordered__c = 'REO Grass Cut/Lawn Care';
//        newCase4.Approval_Status__c = 'Not Started';
//        newCase4.Assigned_Date_Time__c = Date.today();
//        newCase4.Due_Date__c = Date.today().addDays(-2);
//        newCase4.Client_Commit_Date__c = Date.today().addDays(1);
//        newCase4.Last_Open_Verification__c = Date.today();
//        
//        casesToInsert.add(newCase4);
//        
//	}
//	
//	
//	//--------------------------------------------------------
//   	//TESTS
//   	//Unit tests for method testing and regression testing. 
//   	//--------------------------------------------------------
//   	public static testmethod void filterTesting(){
//   		system.Test.startTest();
//		//create account, contact, and test orders
//		generateTestObjects();
//		//create new page reference
//		Pagereference p = Page.MassCommitmentCreate;
//		//set the current page
//		Test.setCurrentPage(p);
//		//instantiate the class
//		MassCommitmentController mcc = new MassCommitmentController();		
//		
//		//****************************************BEGIN SEARCH TESTING*******************************************
//		// Load up default query
//		mcc.loadQuery();
//		
//		//assert that there are 3 orders in work_orders
//		system.assertEquals(3, mcc.work_orders.Size());
//		
//		//Checking with the filters
//		mcc.tempCase.State__c = 'LA';
//   		mcc.dueDateQueryOptionBegin = Date.today().addDays(-5);
//   		mcc.dueDateQueryOptionEnd = Date.today().addDays(1);
//   		mcc.commitDateQueryOptionBegin = Date.today().addDays(-5);
//   		mcc.commitDateQueryOptionEnd = Date.today().addDays(1);
//   		
//   		mcc.loadQuery();
//   		
//   		for(WorkOrderWrapper wo: mcc.work_orders){
//   			system.assertEquals('2420 Ormond Blvd.', wo.wo.Street_Address__c);
//   			system.assertEquals('LA', wo.wo.State__c);
//   			system.assertEquals('Destrehan', wo.wo.City__c);
//   			system.assertEquals('70047', wo.wo.Zip_Code__c);
//   			system.assertEquals('REO Grass Cut/Lawn Care', wo.wo.Work_Ordered__c);
//   			system.assertEquals(Date.today().addDays(-2), wo.wo.Due_Date__c);
//   			system.assertEquals(Date.today().addDays(1), wo.wo.Client_Commit_Date__c);
//   			system.assertEquals(newCase.Id, wo.wo.Id);
//   		}
//   		system.assertEquals(1, mcc.work_orders.size());
//
//		mcc.tempCase.State__c = 'TX';
//		
//		mcc.loadQuery();
//		
//		for(WorkOrderWrapper wo: mcc.work_orders){
//   			system.assertEquals('1234 Street', wo.wo.Street_Address__c);
//   			system.assertEquals('TX', wo.wo.State__c);
//   			system.assertEquals('Austin', wo.wo.City__c);
//   			system.assertEquals('78704', wo.wo.Zip_Code__c);
//   			system.assertEquals('REO Grass Cut/Lawn Care', wo.wo.Work_Ordered__c);
//   			system.assertEquals(Date.today().addDays(-2), wo.wo.Due_Date__c);
//   			system.assertEquals(Date.today().addDays(1), wo.wo.Client_Commit_Date__c);
//   			system.assertEquals(newCase2.Id, wo.wo.Id);
//   		}
//   		system.assertEquals(1, mcc.work_orders.size());
//   		
//		system.Test.stopTest();		
//   	}
//
//   	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - test that the BatchCommitmentUpdater is functioning correctly
//	public static testmethod void massCommitmentTest(){
//		system.Test.startTest();
//		//create account, contact, and test orders
//		generateTestObjects();
//		//create new page reference
//		Pagereference p = Page.MassCommitmentCreate;
//		//set the current page
//		Test.setCurrentPage(p);
//		//instantiate the class
//		MassCommitmentController mcc = new MassCommitmentController();		
//		
//		//check that the following variables are being instantiated
//		system.assertEquals(0, mcc.counter);
//		system.assertEquals(20, mcc.list_size);
//		system.assertEquals(0, mcc.total_size);
//		system.assertEquals('Up', mcc.sort_order);
//		system.assertEquals(' ORDER BY Scheduled_Date__c', mcc.orderByClause);
//		
//		//set page parameters with open order IDs
//		p.getParameters().put(newCase.Id,'');
//		p.getParameters().put(newCase2.Id,'');
//		p.getParameters().put(newCase3.Id,'');
//		
//		// Load up default query
//		mcc.loadQuery();
//		
//		system.assertEquals('{}', mcc.status_error_fields);
//		system.assertEquals(new Client_Commitment__c(), mcc.tmpClientCommitment);
//		
//		//assert that the work_orders list has orders in it, and is size 3
//		system.assert(mcc.work_orders.size() > 0);
//		system.assertEquals(3, mcc.work_orders.size());
//		
//				
//		//fill in the commit fields
//		mcc.tmpClientCommitment.Commitment_Date__c = Date.today();
//		mcc.tmpClientCommitment.Current_Status_of_Order__c = 'Completed, Pending Update';
//		mcc.tmpClientCommitment.Root_Cause_of_Delay__c = 'Delay in Receiving Work';
//		mcc.tmpClientCommitment.Public_Comments__c = 'Work order has been completed and was updated.';
//		mcc.tmpClientCommitment.Internal_Comments__c = 'Internal Comments';
//		
//		//call the createCommits method
//		mcc.createCommits();
//		
//		system.assertEquals('', mcc.status_error);//assert that there were no database.insert errors
//		
//		//select back the client commitments that should have been created into a list, and assert that the list has client commitments in it and is of size 3
//		List<Client_Commitment__c> ccList = [SELECT Commitment_Date__c, Current_Status_of_Order__c, Root_Cause_of_Delay__c, Public_Comments__c, Internal_Comments__c, Work_Order__c FROM Client_Commitment__c];
//		system.assert(ccList.size() > 0);
//		system.assertEquals(3, ccList.size());
//		
//		for(Client_Commitment__c cc : ccList){//loop thru ccList and assert the client commitment fields == the tmpClientCommitment fields
//				system.assert(cc.Work_Order__c != null);
//				system.assertEquals(Date.today(), cc.Commitment_Date__c);
//				system.assertEquals('Completed, Pending Update', cc.Current_Status_of_Order__c);
//				system.assertEquals('Delay in Receiving Work', cc.Root_Cause_of_Delay__c);
//				system.assertEquals('Work order has been completed and was updated.', cc.Public_Comments__c);
//				system.assertEquals('Internal Comments', cc.Internal_Comments__c);
//		}
//		
//		//clear the current work_orders list
//		mcc.work_orders.clear();
//		
//		system.Test.stopTest();
//		
//	}
//	
//	//--------------------------------------------------------
//   	//TESTS
//   	//Unit tests for method testing and regression testing. 
//   	//testing the sorting
//   	public static testmethod void sortTesting(){
//   		system.Test.startTest();
//		//create account, contact, and test orders
//		generateTestObjects();
//		//create new page reference
//		Pagereference p = Page.MassCommitmentCreate;
//		//set the current page
//		Test.setCurrentPage(p);
//		//instantiate the class
//		MassCommitmentController mcc = new MassCommitmentController();		
//		
//		//****************************************BEGIN SORT TESTING*******************************************
//		// Load up default query
//		mcc.loadQuery();
//		
//		mcc.sort_field = 'Due_Date__c';
//		system.assertNotEquals(mcc.last_sort_field, mcc.sort_field);
//		mcc.WOSort();
//		system.assertEquals('Up', mcc.sort_direction);
//		system.assertEquals(' ORDER BY Due_Date__c', mcc.orderByClause);
//		system.assertEquals(mcc.last_sort_field, mcc.sort_field);
//		mcc.WOSort();
//		system.assertEquals('Down', mcc.sort_direction);
//		mcc.WOSort();
//		system.assertEquals('Up', mcc.sort_direction);
//   		
//		system.Test.stopTest();		
//   	}
//   	
//   	//--------------------------------------------------------
//   	//TESTS
//   	//Unit tests for method testing and regression testing. 
//   	//testing the pagination
//   	public static testmethod void paginationTesting(){
//   		system.Test.startTest();
//		//create account, contact, and test orders
//		generateTestObjects();
//		//create new page reference
//		Pagereference p = Page.MassCommitmentCreate;
//		//set the current page
//		Test.setCurrentPage(p);
//		//instantiate the class
//		MassCommitmentController mcc = new MassCommitmentController();		
//		
//		//****************************************BEGIN PAGINATION TESTING*******************************************
//		// Load up default query
//		mcc.loadQuery();
//		
//		system.assertEquals(3, mcc.total_size);
//		system.assertEquals(true, mcc.getDisablePrevious());
//		system.assertEquals(true, mcc.getDisableNext());
//		system.assertEquals(1, mcc.getPageNumber());
//		system.assertEquals(1, mcc.getTotalPages());
//		
//		//Padmesh Soni (06/23/2014) - Support Case Record Type
//		//update with only method calling for code coverage
//		mcc.Beginning();
//   		mcc.Next();
//   		mcc.Previous();
//   		mcc.End();
//   		
//		system.Test.stopTest();		
//   	}
}