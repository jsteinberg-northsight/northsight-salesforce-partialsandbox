@isTest(seeAllData=false)
private class Test_WOInvoiceSummaryController {
///**
// *  Purpose         :   This is used for testing and covering WOInvoiceSummaryController.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   12/18/2014
// *
// *  Current Version :   V1.0
// *
// *  Revision Log    :   V1.0 - Created - AC-6:Invoice Screen | Visual Force Page Prototype
// *
// *	Code coverage	:	97% - V1.0
// **/
//	//Test method for functionality testing
//    static testMethod void testSummaryData() {
//
//		//Query record of Record Type of Account and Case objects
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//                                            OR DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AP OR DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType = 'Invoice__c' OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) 
//                                            AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(4, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsToInsert = new List<Account>();
//        accountsToInsert.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsToInsert.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsToInsert;
//        
//        //List to hold Case records
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//        List<Case> workOrders = new List<Case>();
//        workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, Client__c = AssignmentMapConstants.CASE_CLIENT_SG,
//                                    AccountId = accountsToInsert[0].Id, Due_Date__c = Date.today().addDays(-2), Status = Constants.CASE_STATUS_OPEN,
//                                    Street_Address__c = '301 Front St', City__c = 'Nome', State__c = 'AK', Zip_Code__c = '99762', Vendor_Code__c = 'CHIGRS',
//                                    Work_Ordered__c = 'Initial Grass Cut',Client_Name__c = account.Id));
//        
//        //insert case records
//        insert workOrders;
//        
//        //List to hold Invoice Line records
//        List<Product2> products = new List<Product2>();
//        products.add(new Product2(Name = 'Test 1', Family = 'Cleaning', IsActive = true));
//        products.add(new Product2(Name = 'Test 2', Family = 'Code Violation', IsActive = true));
//        products.add(new Product2(Name = 'Test 3', Family = 'Code Development', IsActive = true));
//        products.add(new Product2(Name = 'Test 4', Family = 'Coding Issues', IsActive = true));
//        
//        //insert product here
//        insert products;
//        
//        //List to hold Invoice records
//        List<Invoice__c> invoices = new List<Invoice__c>();
//        invoices.add(new Invoice__c(Work_Order__c = workOrders[0].Id, RecordTypeId = recordTypes[2].Id));
//        invoices.add(new Invoice__c(Work_Order__c = workOrders[0].Id, RecordTypeId = recordTypes[3].Id));
//        
//        //insert invoices here
//        insert invoices;
//        
//        List<RecordType> lineRecordTypes = [SELECT Id FROM RecordType WHERE (DeveloperName = 'Receivable' OR DeveloperName = 'Payable') 
//        										AND SobjectType = 'Invoice_Line__c' ORDER BY DeveloperName];
//		
//		System.assertEquals(2, lineRecordTypes.size());
//		
//        //List to hold Invoice Line records
//        List<Invoice_Line__c> invoiceLines = new List<Invoice_Line__c>();
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[0].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[0].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[0].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[0].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[1].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[1].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[1].Id, Invoice__c = invoices[1].Id, Product__c = products[2].Id, Quantity__c = 1, Unit_Price__c = 500));
//        invoiceLines.add(new Invoice_Line__c(RecordTypeId = lineRecordTypes[0].Id, Invoice__c = invoices[0].Id, Product__c = products[2].Id, Quantity__c = 1, Unit_Price__c = 500));
//        
//        //insert line items of invoices
//        insert invoiceLines;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Controller instance
//        WOInvoiceSummaryController controller = new WOInvoiceSummaryController();
//        controller.workOrderId = workOrders[0].Id;
//        controller.invoicesSummaryWrap = controller.getinvoicesSummaryWrap();
//        
//        controller.setinvoicesSummaryWrap(controller.invoicesSummaryWrap);
//        
//        //assert statement
//        System.assert(controller.invoicesSummaryWrap.size() > 0);
//        
//        //Test stops here
//        Test.stopTest();
//    }
}