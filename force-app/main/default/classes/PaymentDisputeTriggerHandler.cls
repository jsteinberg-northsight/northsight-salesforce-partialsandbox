public with sharing class PaymentDisputeTriggerHandler {
	
	public static void onBeforeInsert(List<Payment_Dispute__c> newPaymentDispute, Map<Id, Payment_Dispute__c> paymentDisputeMap) {
		// EXECUTE BEFORE INSERT LOGIC
		DenyPaymentDispute.denyInsertIfRecordExists(newPaymentDispute, paymentDisputeMap);
	}
}