@isTest
private class Test_VendorInstructionsController {
///**
// *  Purpose         :   This is used for testing and covering VendorInstructionsController.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   1/9/2015
// *
// *  Current Version :   V1.0
// *
// *  Revision Log    :   V1.0 - Created 
// * 
// *  Coverage        :   100% - V1.0
// **/
//    
//    //Method to test the functionality of VendorInstructionsController
//    static testMethod void test_inIt() {
//        
//        //Query record of Record Type of Account and Case objects
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//                                            OR DeveloperName = 'Vendor_Instructions' OR DeveloperName = 'Vendor_Instructions_Hidden') 
//                                            AND (SobjectType =: Constants.CASE_SOBJECTTYPE OR SobjectType = 'Work_Order_Instructions__c'
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(4, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsToInsert = new List<Account>();
//        accountsToInsert.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsToInsert.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsToInsert;
//        
//        //List to hold Case records
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account;
//		
//        List<Case> workOrders = new List<Case>();
//        workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, Client__c = AssignmentMapConstants.CASE_CLIENT_SG,
//                                    AccountId = accountsToInsert[0].Id, Due_Date__c = Date.today().addDays(-2), Status = Constants.CASE_STATUS_OPEN,
//                                    Street_Address__c = '301 Front St', City__c = 'Nome', State__c = 'AK', Zip_Code__c = '99762', Vendor_Code__c = 'CHIGRS',
//                                    Work_Ordered__c = 'Initial Grass Cut',Client_Name__c = account.Id));
//        
//        //insert case records
//        insert workOrders;
//        
//        //List to hold Work Order Instructions test records
//        List<Work_Order_Instructions__c> woInstructions = new List<Work_Order_Instructions__c>();
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[2].Id, Instruction_Type__c = 'ACCESS UNKNOWN', 
//                                                            Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[2].Id, Instruction_Type__c = 'KNOWN ISSUE', 
//                                                            Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[2].Id, Instruction_Type__c = 'TESTED OK', 
//                                                            Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[2].Id, Instruction_Type__c = 'READY WORK', 
//                                                            Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id));
//        
//        //insert instructions here
//        insert woInstructions;
//        
//        //Test starts here
//        Test.startTest();
//
//		//Instance of Standard controller        
//        ApexPages.StandardController sc = new ApexPages.StandardController(workOrders[0]);
//        
//        //Instance of controller
//        VendorInstructionsController controller = new VendorInstructionsController(sc);
//        
//        //assert statements
//        System.assertEquals(4, controller.vendorInstructions.size());
//        
//        //Test stops here
//        Test.stopTest();
//    }
}