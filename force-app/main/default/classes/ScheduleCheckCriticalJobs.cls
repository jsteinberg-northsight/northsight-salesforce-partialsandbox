global class ScheduleCheckCriticalJobs implements Schedulable {
global void execute(SchedulableContext  sc){
        //instantiate a system alert list and a system alert object
        List<System_Alerts__c> saList = new List<System_Alerts__c>();
        
        //check if the markedLogs list is null or empty
        string stoppedJobsMessage = ''; 
        Scheduled_Job__c[] stoppedList = [select Id, Job_Name__c from Scheduled_Job__c where Status__c != 'Scheduled'];
        for(Scheduled_Job__c sj:stoppedList){
            //build out the system alert and add it to the list
            stoppedJobsMessage += sj.Job_Name__c+'\r\n';
        }
        if(stoppedJobsMessage != ''){
        stoppedJobsMessage = 'The following critical jobs are not running:\r\n'+stoppedJobsMessage;
        System_Alerts__c sa = new System_Alerts__c();
        sa.Subject_Line__c = 'Scheduled Processes Alert';
        sa.Process_Name__c = 'Critical Processes';
        sa.Body__c = stoppedJobsMessage;
        saList.add(sa);
        }
        //if the list has a size > 0, insert the values
        if(saList.size() > 0){
            insert saList;
        }
    }
}