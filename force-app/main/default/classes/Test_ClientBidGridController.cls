/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_ClientBidGridController {
/**
 *  Purpose         :   This class is used to cover ClientBidGridController class. 
 *
 *  Created By      :   Shawn Johnson
 *
 *  Created Date    :   7/17/2018
 *
 *  Current Version :   V1.1
 *
 *  Revision Log    :   V1.0 - Created
 **/
 
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Test.startTest();
        
        ClientBidGridController controller = new ClientBidGridController();
        String bidSerialized = controller.bidSerialized;
        
        ClientBidGridController.SaveHtml(bidSerialized);
        
        controller.GoToExportPage();
        
        String baseURL = controller.baseURL;
        controller.TheUserAddress = 'Test';
        
        Test.stopTest();
    }
    
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        
        Test.startTest();
        
        ClientBidGridController controller = new ClientBidGridController();
        String bidSerialized = controller.bidSerialized;
        
        ClientBidGridController.CHUNK_SIZE = 4;
        ClientBidGridController.SaveHtml('Testing is Going On');
        
        Test.stopTest();
    }
    
    @testsetup
    private static void createDataset() {
    	
    	List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();
        
        for(integer i = 1; i <= 1; i++) {        
            accounts.add(new Account(Name = 'Tester Account' + i, Status__c = 'Active', Account_Supervisor__c = Userinfo.getUserId()));
        }
        
        insert accounts;
        
        for(integer i = 1; i <= 1; i++) {        
            contacts.add(new Contact(FirstName = 'Test', LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active'));
        }   
        
        insert contacts;
        
        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
                                            OR DeveloperName = 'Client'
                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
        
        List<Account> accountsOnOrders = new List<Account>();
        accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id, Status__c = 'Inactive', 
        									Account_Supervisor__c = Userinfo.getUserId()));
        accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id, Status__c = 'Active', 
        									Client_Code__c = AssignmentMapConstants.CASE_CLIENT_SG, Account_Supervisor__c = Userinfo.getUserId()));
        
        insert accountsOnOrders;
        
        Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
        insert property;
        
        List<Case> workOrders = new List<Case>();
        workOrders.add(new Case(Geocode_Cache__c = property.Id, AccountId = accountsOnOrders[0].Id, Zip_Code__c = '12345', Client_Name__c = accountsOnOrders[1].Id,
                                    Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', Vendor_Code__c = 'Follow',
                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX', Street_Address__c = 'test street',
                                    Department__c = 'Test', Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual'));
        
        insert workOrders;
        
        List<Client_Bid__c> clientBids = new List<Client_Bid__c>();
        clientBids.add(new Client_Bid__c(Related_Work_Order__c = workOrders[0].Id, Description__c = 'Tested OK', Submitted_To__c = 'Client', Type__c = 'Violation'));
        
        insert clientBids;
    }
}