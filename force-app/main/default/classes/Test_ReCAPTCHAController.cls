@isTest(seeAllData=false)
private class Test_ReCAPTCHAController {
///**
// *  Purpose         :   This is used for testing and covering ReCAPTCHAController class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   09/09/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - SM-204: Customize self-register apex
// *	
// *	Coverage		:	79%
// **/
//    static testMethod void myUnitTest() {
//        
//        //Insert ReCaptcha Configuration Defaults values
//        ReCaptcha_Configuration__c configDefault = new ReCaptcha_Configuration__c();
//        configDefault.PrivateKey__c = 'test';
//        configDefault.BaseUrl__c = 'www.test.com'; 
//        configDefault.PrivateKey__c = 'test2';
//        insert configDefault;
//        
//        //Test starts here
//        Test.startTest();
//        
//        // Set mock callout class 
//        Test.setMock(HttpCalloutMock.class, new ReCaptchaMockRespGenerator());
//        
//        //Custom setting instance of ReCaptcha
//        ReCaptcha_Configuration__c configDefaults = ReCAPTCHAController.CONFIG_DEFAULT;
//        
//        ReCAPTCHAController controller = new ReCAPTCHAController();
//        System.assertEquals(true, controller.verify('test', 'test', configDefaults.BaseUrl__c, configDefaults.PrivateKey__c));
//        
//        //Test stops here
//        Test.stopTest();
//    }
}