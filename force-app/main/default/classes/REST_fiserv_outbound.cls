@RestResource(urlMapping='/integrations/fiserv/outbound/*')
global without sharing class REST_fiserv_outbound {
	
	static final string PASSWORD='RGWv7YMm0kmVidM6FeLtisETrH36jL';
	
	static final string FETCH_TYPE_FIRST = 'fetchFirst';
	static final string FETCH_TYPE_NEXT = 'fetchNext';
	static final string FETCH_TYPE_SUCCESS = 'fetchSuccess';
	static final string FETCH_TYPE_FAIL= 'fetchFail';
	
	static final set<string> validParams = new set<string>{FETCH_TYPE_FIRST, FETCH_TYPE_NEXT, FETCH_TYPE_SUCCESS, FETCH_TYPE_FAIL};
	
	static final integer RECORDS_PER_CHUNK = 250;
	static final id CARRINGTON_ACCT_ID = '0014000000bm1EB';  //fiserv3SANDBOX='001S000000qGPl5'
	
	class Payload{
		boolean more{get;set;}
		id batchID{get;set;}
		INT_FiServ_Outbound__c[] results {get;set;}
	}	
	
	@HttpPost //using a POST here so as not to hvae the password in the URL
	global static void doPost() {
		RestRequest req = RestContext.request;
		string paramBatchID=req.headers.get('batchID');
		string paramPassword= req.headers.get('password');
		string paramFetchType=req.headers.get('fetchType');
		string paramLog=req.headers.get('log');
		
		id batchID = string.isEmpty(paramBatchID) ? null : (id)paramBatchID;
		boolean batchWasGiven = batchID!=null;
		
		if (paramPassword!=PASSWORD){
			RestContext.response.statuscode=400;
			RestContext.response.responsebody=blob.valueof('bad password');
		}
		else if(!validParams.contains(paramFetchType)){
			RestContext.response.statuscode=400;
			RestContext.response.responsebody=blob.valueof('bad fetchType param');
		}
		else if(
			//a batchid would not be provided on first request
			(paramFetchType==FETCH_TYPE_FIRST && batchWasGiven) ||
			(paramFetchType==FETCH_TYPE_NEXT && !batchWasGiven) ||
			((paramFetchType==FETCH_TYPE_SUCCESS || paramFetchType==FETCH_TYPE_FAIL)  && !batchWasGiven)
		)
		{
			
			RestContext.response.statuscode=400;
			RestContext.response.responsebody=blob.valueof('bad parameter combo');
		}		
		else	
			try{
				
				if(paramFetchType==FETCH_TYPE_SUCCESS || paramFetchType==FETCH_TYPE_FAIL){
					INT_FiServ_Outbound_Batch__c batch =
						[select id, Transmission_Result__c, log__c
						from INT_FiServ_Outbound_Batch__c
						where id=:batchID];
					
					system.assert(batch!=null);
					
					batch.Transmission_Result__c= paramFetchType==FETCH_TYPE_SUCCESS ? 'Success' : 'Failure';
					addLineToBatchLog(batch, 'result from caller: ' + batch.Transmission_Result__c);
					addLineToBatchLog(batch, 'log sent from caller: ' + paramLog);
					update batch;
				}
				else{	
					Payload payload = new Payload();
					INT_FiServ_Outbound__c[] resultsChunkToSend;			
					
					if(doResultsForSendingExist()){
						INT_FiServ_Outbound_Batch__c batch=fetchOrCreateBatchToUse(batchID);
						addlinetobatchlog(batch, 'batch selected for upload');
						batchID=batch.id;					
						payload.batchID = batchID;
						
						if (paramFetchType==FETCH_TYPE_FIRST){
							resetStatusOfChildrenToUnsent(batchID);
							associateReadyResultsToBatch(batchID);
						}
						
						integer numResultsReady = fetchCountOfResultsReadyForSend(batchID);
						addLineToBatchLog(batch, 'Results Ready for Send: ' + numResultsReady);
						payload.more = (numResultsReady > RECORDS_PER_CHUNK);	
						resultsChunkToSend=fetchChunkofReadyResults(batchID);
						addLineToBatchLog(batch, 'Results Sent in Chunk: ' + resultsChunkToSend.size());
						setResultsTransmissionStatusesToSent(resultsChunkToSend);
						payload.results=resultsChunkToSend;
						
						update batch; // for the log entries
					
						
					}
					else{
						payload.more=false;
						payload.results=new INT_FiServ_Outbound__c[]{};
						
					}
					RestContext.response.responsebody=blob.valueof(json.serialize(payload));
					
				}
				
				
				
								
				
			}
			catch(exception e){	
				RestContext.response.statuscode=500;
				RestContext.response.responsebody=blob.valueof(e.getMessage());
				sendErrorNotificationEmail(e);
			}
	}
	
	static boolean doResultsForSendingExist(){
		if (
				[select id 
				from INT_FiServ_Outbound_Batch__c 
				where Transmission_Result__c != 'Success'].size()>0) return true;
		
		else if (fetchCountOfResultsReadyForSend(null)>0) return true;
		else return false;
	
	}
	
	public static void tmp_fetchCountOfResultsReadyForSend(){
		system.debug(fetchCountOfResultsReadyForSend(null));
	}
	
	static void addLineToBatchLog(INT_FiServ_Outbound_batch__c batch, string log){
		batch.log__c=(string.isblank(batch.log__c) ? '' : batch.log__c) +'\r\n'+ datetime.now() + '\t' +  log;
		
	}
	
	static void setResultsTransmissionStatusesToSent(INT_FiServ_Outbound__c[] results /*modified*/ ){
		for (INT_FiServ_Outbound__c res : results) res.Transmission_Status__c='Sent';
		update results;
	}
	
	static INT_FiServ_Outbound__c[] fetchChunkofReadyResults(id batchID){
		
		INT_FiServ_Outbound__c[] ret =[ 
				SELECT Action_Needed_Board_Screen__c,Action_Needed_Change_Locks__c,Action_Needed_Cut_Grass__c,Action_Needed_Other_Description__c,Action_Needed_Other__c,Action_Needed_Remove_Debris__c,Action_Needed_Replace_Glass__c,Action_Needed_Secure_Pool_Hot_Tub_Spa__c,Action_Needed_Trim_Shrubs__c,Action_Needed_Winterize__c,Amount_of_Broken_or_Boarded_Openings__c,Amount_of_Debris_on_Site__c,Broker_Name__c,Broker_Telephone__c,Card_left_at_Property__c,Construction_Type_Other__c,Construction_Type__c,Corrected_Alternate_Phone_Nbr_1__c,Corrected_Alternate_Phone_Nbr_2__c,Corrected_Alt_Phone_1_Location_Tag__c,Corrected_Alt_Phone_2_Location_Tag__c,Corrected_Primary_Phone_Number__c,Corrected_Prim_Phone_Location_Tag__c,Description_of_Partially_Vacant__c,Doors_Windows_Boarded__c,Electric_On__c,Exterior_Problem_Environmental_Hazards__c,Exterior_Problem_Fire_Damage__c,Exterior_Problem_Flood_Damage__c,Exterior_Problem_Freeze_Damage__c,Exterior_Problem_Neglect__c,Exterior_Problem_Other_Description__c,Exterior_Problem_Other__c,Exterior_Problem_Roof_Leakage__c,Exterior_Problem_Storm_Damage__c,Exterior_Problem_Unable_To_Determine__c,Exterior_Problem_Vandalized__c,External_Property_Condition__c,Fees_Due_Cancellations__c,Fees_Due_Other__c,Fees_Due_Photos__c,Fees_Due_Total__c,Fee_Due_Cancellation_s__c,Fee_Due_Inspection__c,Fee_Due_Other__c,Fee_Due_Photo_s__c,Fee_Due_Total__c,Field_Servicing_Company_ID__c,Field_Servicing_Company__c,First_Known_Vacancy_Date__c,First_Time_Vacant__c,Gas_On__c,Grass_Weeds_Height_Inches__c,Id,Inspection_Address__c,Inspection_Complete_Date__c,Inspection_Request_Date__c,Inspection_Request_Source__c,Inspection_Type_Modifier__c,Inspection_Type__c,Inspector_Name_Code__c,Interior_Problem_Environmental_Hazards__c,Interior_Problem_Fire_Damage__c,Interior_Problem_Flood_Damage__c,Interior_Problem_Freeze_Damage__c,Interior_Problem_Neglect__c,Interior_Problem_Other_Description__c,Interior_Problem_Other__c,Interior_Problem_Roof_Leakage__c,Interior_Problem_Storm_Damage__c,Interior_Problem_Unable_To_Determine__c,Interior_Problem_Vandalized__c,INT_FiServ_Result_Batch__c,Loan_Number__c,Maintenance_Recommended__c,Map_Reference_Number_Mailing__c,Map_Reference_Number_Property__c,Mortgage_Company__c,Name,Neighborhood_Condition_Code__c,Occupancy_Code__c,Occupancy_Verified_Other_or_Neighbor__c,Occupancy_Verified__c,Office_Region__c,Other_Fee_Description__c,Personal_Property_On_Site__c,Phone_Verification_Tag__c,Photos_Forwarded__c,Pool_Hot_Tub_Spa_on_Site__c,Pool_Hot_Tub_Spa_Secured__c,Previously_Winterized_by_Service_Company__c,Property_For_Sale__c,Property_Secured__c,Property_Type__c,Record_ID__c,Requestor_Code__c,Requestor_Department__c,Service_Bureau__c,Status__c,Test_Record__c,Total_CFS_Records__c,Total_Fees_Due_for_Insp_Type_A__c,Total_Fees_Due_for_Insp_Type_B__c,Total_Fees_Due_for_Insp_Type_C__c,Total_Fees_Due_for_Insp_Type_D__c,Total_Fees_Due_for_Insp_Type_E__c,Total_Fees_Due_for_Insp_Type_F__c,Total_Fees_Due_for_Insp_Type_G__c,Total_Fees_Due_for_Insp_Type_H__c,Total_Fees_Due_for_Insp_Type_I__c,Total_Fees_Due_for_Insp_Type_J__c,Total_Fees_Due_for_Insp_Type_K__c,Total_Fees_Due_for_Insp_Type_R__c,Total_Fees_Due_for_Insp_Type_S__c,Total_Fees_Due_for_Insp_Type_T__c,Total_Fees_Due_for_Insp_Type_U__c,Total_Fees_Due_for_Insp_Type_V__c,Total_Fees_Due_for_Insp_Type_W__c,Total_PIRS_Records__c,Version_Number__c,Water_On__c,Windows_Broken__c,Wint_Tags_Observed_from_Another_Company__c,Work_Order_Completion_Date__c
				from int_fiserv_outbound__c
				where 
					INT_FiServ_Result_Batch__c=:batchID AND
					Transmission_Status__c != 'Sent'
				limit :RECORDS_PER_CHUNK
				for update /*for update clause is critical to preventing accidental simultaneous calls to endpoint from double submitting to fiserv*/
			];
		return ret;
	}
	
	static integer fetchCountOfResultsReadyForSend(id batchID){
		integer ret;
		
		AggregateResult[] ar  = 
			[SELECT count(id) cnt
			FROM int_fiserv_outbound__c 
			where
				INT_FiServ_Result_Batch__c=:batchID AND
				Transmission_Status__c != 'Sent' AND 
				status__c='Ready' AND
				Test_Record__c = false
			];
		
		ret = (integer)ar[0].get('cnt'); 
		return ret;				
	}
	
	static void sendErrorNotificationEmail(exception e){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		
		// Strings to hold the email addresses to which you are sending the email.
		String[] toAddresses = new String[] {'software@northsight.com'}; 
		mail.setToAddresses(toAddresses);
		mail.setReplyTo('DONOTREPLY@northsight.com');
		mail.setSenderDisplayName('Fiserv Integration Error');
		mail.setSubject('Fiserv Integration Error');		
		mail.setPlainTextBody(e.getmessage() + e.getstacktracestring());
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	

	static void associateReadyResultsToBatch(id batchID){
		INT_FiServ_Outbound__c[] readyResults = 
			[select id, INT_FiServ_Result_Batch__c
			from int_fiserv_outbound__c
			where 
				status__c='Ready' AND
				Test_Record__c = false AND
				INT_FiServ_Result_Batch__c=null
			];
		
		for (int_fiserv_outbound__c r : readyResults) r.INT_FiServ_Result_Batch__c=batchID;
		
		update readyResults;
	}
	
	/*
		It will probably rarely if ever happen, but there could be an situation where a batch was large enough
		that we send in chunks.  in this scenario, we need to mark the result as "sent" to keep track of the chunks.
		if, say, there was an error in the external service and the full send to fiserv never happened, we could have
		result records that say sent but the parent batch will have its transmission status as unsent b/c the confirmation
		call never came through from the external service.  	
	*/
	static void resetStatusOfChildrenToUnsent(id batchID){
		
		INT_FiServ_Outbound__c[] children =
			[select id,transmission_status__c 
			from INT_FiServ_Outbound__c 
			where 
				INT_FiServ_Result_Batch__c=:batchID
			for update];
		
		for (INT_FiServ_Outbound__c child : children){
			child.transmission_status__c='Unsent';
		}
		
		update children;
		 
	}
	
	static INT_FiServ_Outbound_Batch__c fetchOrCreateBatchToUse(id batchID){
		INT_FiServ_Outbound_Batch__c ret;
		
		if (batchID == null){
			INT_FiServ_Outbound_Batch__c[] availableBatches= 
				[select id, Transmission_Result__c, log__c 
				from INT_FiServ_Outbound_Batch__c 
				where Transmission_Result__c != 'Success'];
			
			system.assert(availableBatches.size()<=1, 'There should be only 1 failed or unsent batch at a time');
			
			if (availableBatches.isEmpty()){
				ret = new INT_FiServ_Outbound_Batch__c(account__c=CARRINGTON_ACCT_ID);	
				insert ret;
			}
			else{
				ret=availableBatches[0];
			}			
		}
		else
		{
			ret= 
				[select id, Transmission_Result__c,log__c 
				from INT_FiServ_Outbound_Batch__c 
				where id=:batchID];
		
			system.assert(ret!=null, 'Batch ' + batchID + ' not found.');
			system.assert(ret.Transmission_Result__c != 'Success', 'batch already processed');
				
		}
		return ret;	
	}
	
	
	global static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		
		
	}
    
}