@isTest(SeeAllData=true)
public class RHX_TEST_Client_Bid {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Client_Bid__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Client_Bid__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}