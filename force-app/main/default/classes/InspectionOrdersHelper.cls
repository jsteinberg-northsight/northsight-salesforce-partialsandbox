public with sharing class InspectionOrdersHelper {
	//global variables
	private static RecordType rt;
	private static Map<Id, Case> ordersToUpdate;
	private static Map<Id, Case> ordersToCreate;
	private static Set<Id> contactIds; 
	private static Map<Id, User> owners;
	
	//initialize method
	public static void init(Set<Id> ordersNeedingInspections){
		rt = [SELECT Id FROM RecordType WHERE Name = 'REO' limit 1];
		ordersToUpdate = new Map<Id, Case>();
		ordersToCreate = new Map<Id, Case>();
		contactIds = new Set<Id>();
		owners = new Map<Id, User>();
		//select the needed fields from the cases
		for(Case c : [SELECT Id,
							 Street_Address__c,
							 City__c,
							 State__c,
							 Zip_Code__c,
							 CaseNumber,
							 Last_Crew_Name_Lookup__c,
							 Geocode_Cache__c,
							 View_Images_Public_Link__c,
							 Date_Serviced__c,
							 Entire_Property_Serviced__c,
							 Debris_Present__c,
							 Shrubs_Maintained__c,
							 Edging_Completed__c,
							 Fraud_or_Other__c,
							 Vendor_Notes_to_Staff__c,
							 Regarding_Work_Order__c
					  FROM Case
					  WHERE Id IN: ordersNeedingInspections]){
			
			//instantiate the ordersToUpdate map
			if(!ordersToUpdate.containsKey(c.Id)){
				ordersToUpdate.put(c.Id, c);
			}
			//instantiate the ordersToCreate map
			if(!ordersToCreate.containsKey(c.Id)){
				ordersToCreate.put(c.id, new Case());
			}
			//instantiate the contactIds set
			if(!contactIds.contains(c.Last_Crew_Name_Lookup__c)){
				contactIds.add(c.Last_Crew_Name_Lookup__c);
			}
		}
		//generate the owners map
		generateowners();
		//call createOrders() to create new orders
		createOrders();
		//call finalize() to insert new orders and update existing orders 
		finalize();
	}
	//method for selecting back users
	private static void generateowners(){
		if(contactIds != null && contactIds.size() > 0){
			for(User u : [SELECT Id, ContactId FROM User WHERE ContactId IN: contactIds]){
				if(!owners.containsKey(u.ContactId)){
					owners.put(u.ContactId, u);
				}
			}
		}
	}
	//method for creating new REO orders
	private static void createOrders(){
		
		if(ordersToCreate != null && ordersToUpdate != null){
			
			//Code added - Padmesh Soni - 2/4/2015
			//Getting host name of current Salesforce server
			String hostname =URL.getSalesforceBaseUrl().getHost();
			
			//Append hostname with HTTPs to create domain URL 
			String domainURL = 'https://' + hostname ;
			
			for(String orderId : ordersToUpdate.keySet()){
				if(ordersToCreate.containsKey(orderId)){
					Datetime dt = ordersToUpdate.get(orderId).Date_Serviced__c;
					Case inspectionsCase = ordersToCreate.get(orderId);
						inspectionsCase.RecordTypeId = rt.Id;
						inspectionsCase.Work_Ordered__c = 'Grass Cut Follow Up';
						inspectionsCase.Street_Address__c = ordersToUpdate.get(orderId).Street_Address__c;
						inspectionsCase.City__c = ordersToUpdate.get(orderId).City__c;
						inspectionsCase.State__c = ordersToUpdate.get(orderId).State__c;
						inspectionsCase.Zip_Code__c = ordersToUpdate.get(orderId).Zip_Code__c;
						//inspectionsCase.Internal_Notes__c = 'Follow up generated from ' + ordersToUpdate.get(orderId).CaseNumber + ' -- https://na2.salesforce.com/' + orderId;
						inspectionsCase.Internal_Notes__c = 'Follow up generated from ' + ordersToUpdate.get(orderId).CaseNumber + ' -- '+ domainURL +'/'+ orderId;
						inspectionsCase.Work_Order_Type__c = 'Grass';
						inspectionsCase.Vendor_Code__c = 'FOLLOW';
						inspectionsCase.Client__c = 'FOLLOW';
						inspectionsCase.OwnerId = owners.get(ordersToUpdate.get(orderId).Last_Crew_Name_Lookup__c).Id;
						inspectionsCase.Geocode_Cache__c = ordersToUpdate.get(orderId).Geocode_Cache__c;
						inspectionsCase.Description = 'This follow up is a result of a failed quality control inspection on Work Order Number ' + ordersToUpdate.get(orderId).CaseNumber + '. Please see results and photos below.' + '\n'
														 + '\n' + 'Inspection Photo Link: ' + ordersToUpdate.get(orderId).View_Images_Public_Link__c + '\n'
														 + '\n' + 'Inspection Date: ' + dt.month()+'/'+dt.day()+'/'+dt.year() + '\n'
														 + 'Entire Property Serviced: ' + ordersToUpdate.get(orderId).Entire_Property_Serviced__c + '\n'
														 + 'Debris Present: ' + ordersToUpdate.get(orderId).Debris_Present__c + '\n'
														 + 'Shrubs Maintained: ' + ordersToUpdate.get(orderId).Shrubs_Maintained__c + '\n'
														 + 'Edging Completed: ' + ordersToUpdate.get(orderId).Edging_Completed__c + '\n'
														 + 'Other Issues: ' + ordersToUpdate.get(orderId).Fraud_or_Other__c + '\n'
														 + 'Comments: ' + ordersToUpdate.get(orderId).Vendor_Notes_to_Staff__c;
						inspectionsCase.Due_Date__c = Date.today().addDays(1);
				}
			}
		}
	}
	//method to perform DML operations
	private static void finalize(){
		if(ordersToCreate != null && ordersToCreate.size() > 0){
			insert ordersToCreate.values();
		}
		
		if(ordersToUpdate != null && ordersToUpdate.size() > 0){
			for(String orderId : ordersToCreate.keySet()){
				if(ordersToUpdate.containsKey(orderId)){
					Case orderToUpdate = ordersToUpdate.get(orderId);
					orderToUpdate.Regarding_Work_Order__c = ordersToCreate.get(orderId).Id;
				}
			}
			update ordersToUpdate.values();
		}
	}
}