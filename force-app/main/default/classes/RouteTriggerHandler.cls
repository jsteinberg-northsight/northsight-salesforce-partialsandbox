public without sharing class RouteTriggerHandler {
	public static void onAfterInsert(Route__c[] newRoute, Map<Id, Route__c> routeMap) {
		// EXECUTE AFTER INSERT LOGIC
		UpdateLastRoute.updateContacts(newRoute, routeMap);
	}
}