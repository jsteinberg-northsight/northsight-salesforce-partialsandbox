@isTest
private class Task1TriggerControllerTest {
    
    @testSetup
    private static void setup() {
        Task_Code__c code = new Task_Code__c(
            Task_Category__c = 'Process',
            Task_Sub_Category__c = 'Process Work Order',
            Default_Task_Name__c = 'Test'
        );
        insert code;
        Task_Code_Item__c item = new Task_Code_Item__c(
            Task_Code__c = code.Id,
            Default_Sort__c = 200
        );
        insert item;
        Task_Team__c team = new Task_Team__c(
            Name = 'test'
        );
        insert team;
    }
    
    @isTest
    private static void addNewTask1WithFlagTest() {
        Task_Code__c code = [SELECT Id FROM Task_Code__c LIMIT 1];
        Task_Team__c team = [SELECT Id FROM Task_Team__c LIMIT 1];
        
        Task1__c task = new Task1__c(
            Task_Code__c = code.Id,
            Auto_Fill_Task_Items__c = true,
            Task_Team__c = team.Id,
            Status__c = 'New'
        );
        insert task;
        
        List<Task_Item__c> result = [SELECT Id FROM Task_Item__c WHERE Task__c =: task.Id];
        System.assertEquals(1, result.size(), '1 task code item copied');
    }
    
    @isTest
    private static void addNewTask1WithNoFlagTest() {
        Task_Code__c code = [SELECT Id FROM Task_Code__c LIMIT 1];
        Task_Team__c team = [SELECT Id FROM Task_Team__c LIMIT 1];
        
        Task1__c task = new Task1__c(
            Task_Code__c = code.Id,
            Auto_Fill_Task_Items__c = false,
            Task_Team__c = team.Id,
            Status__c = 'New'
        );
        insert task;
        
        List<Task_Item__c> result = [SELECT Id FROM Task_Item__c WHERE Task__c =: task.Id];
        System.assertEquals(0, result.size(), '0 task code item copied');
    }
}