/**
 *	Purpose			:	This class is controller for Page ActivatePortalUsers used on the "Activate Portal User" custom button on contact layout. 
 *
 *	Created By		:	Don Kotter
 *
 *	Created Date	:	10/29/2014
 *
 *	Current Version	:	V1.1
 *
 *	Revision Log	:	V1.0 - Created - VCP-29: Scope the work involved in automating the creation of a Vendor in the Partner Portal
 *						V1.1 - Modified - Padmesh Soni (10/30/2014) - VCP-29: Scope the work involved in automating the creation of 
 *										  a Vendor in the Partner Portal. Changes are:
 *											1. Comments added and code formatted.
 *											2. Fixed some issues mentioned by Don and also some exists code issues. 
 **/
public with sharing class ActivatePortalUser {

	//Contact object
	public final Contact con {get;set;}
	
	//is activation possible?  to be used on page so activate or deactivate the buttons
	public boolean activationPossible{get;set;}
	
	//The account to use when activating vendor.  Set in constructor
	public Account vendorCommunityAccount{get;set;}
	
	//Profile to use for OS vendors.  Set in constructor
	public Profile OSCommunityProfile{get;set;}
	
	//Profile to use for non-OS vendors.  Set in constructor
	public Profile VCommunityProfile{get;set;}
	
	//Set in constructor
	public UserRole VCPURole{get;set;}
	
	//for display on the page.
	public string license{get;set;}   
	public User[] portalVendors{get;set;}
	//Constructor
	public ActivatePortalUser(ApexPages.StandardController stdController){

	 	if(!System.Test.isRunningTest()){
	 		//Need to add fields that won't appear on the page. 
	 		stdController.addFields(new string[]{'Id','FirstName','LastName','MailingCity','MailingState','MailingStreet',
	 												'MailingPostalCode','AccountId', 'Email'});
	 	}
	 	
	 	//Getting standard controller's Sobject record
	 	con = (Contact)stdController.getRecord();
	 	
	 	String accountName = '';
	 	String roleName = '';
	 	if(Test.isRunningTest()) {
	 		
	 		accountName = 'Test Vendor Community';
	 		roleName = 'Test Vendor Community Partner User';
	 	} else {
	 		
	 		accountName = 'Vendor Community';
	 		roleName = 'Vendor Community Partner User';
	 	}
	 	
	 	//query result of Account where Name = Vendor Community
		vendorCommunityAccount = [select Id from Account where Name =: accountName limit 1];
		
		//query result of Profile where Name = Partner Community - Vendors
		VCommunityProfile = [select Id from Profile where Name = 'Partner Community - Vendors' limit 1];
		
		//query result of Profile where Name = Partner Community - Vendors OrderSelect
		OSCommunityProfile  = [select Id from Profile where Name = 'Partner Community - Vendors OrderSelect' limit 1];
		
		//query result of UserRole where Name = Vendor Community Partner User
		VCPURole = [Select Id from UserRole where Name =:roleName limit 1];
	
		//query result of User where IsPortalEnabled = true and ContactId = :con.Id
		User[] portalVendors = [select Id, Profile.Name, License_Type__c from User where IsPortalEnabled = true and ContactId = :con.Id];
		activationPossible=true;
		
		try {
			if(portalVendors.size()>0){
			
				//Need to review this field to be sure it's correct.
				license = portalVendors[0].License_Type__c; 
				
				//If there is already a user we should not activate a new user unless the old user was an old customer portal user.
				if(license != 'Customer Portal Manager Standard'){
					activationPossible = false; 
				}
				
				//Getting label for warning
				String warnMsg = Label.ACTIVATE_PORTAL_USER_WARNING_CONTACT;
				
				//throw custom warning
				throw new CustomException(warnMsg.replace('USRPROFILE', portalVendors[0].Profile.Name));
			}
		}catch(Exception e) {
			
			//A user friendly warning message added on page
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, e.getMessage()));
		}
	}
	
	//Activate a standard vendor user
	public void activateNormalVendorUser(){
		
		//Check for flag
		if(!activationPossible){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Contact is already a User in a Community'));
			return;
		}
		
		//instance of User record
		User newUser = new User();
		newUser.Email = con.Email;
		newUser.FirstName = con.FirstName;
		newUser.LastName = con.LastName;
		newUser.City = con.MailingCity;
		newUser.State = con.MailingState;
		newUser.Street = con.MailingStreet;
		newUser.PostalCode = con.MailingPostalCode;
		newUser.ContactId = con.Id;
		newUser.ProfileId = VCommunityProfile.Id;
		newUser.Username = con.Email;
		newUser.TimeZoneSidKey = 'America/Los_Angeles';
		newUser.LocaleSidKey = 'en_US';
		newUser.EmailEncodingKey = 'UTF-8'; 
		newUser.LanguageLocaleKey = 'en_US';
		setRole(newUser);
		newUser.alias = getAlias();
		newUser.CommunityNickname = newUser.alias;
		newUser.IsActive = true;
		Database.saveResult result = Database.insert(newUser,false);
		if(result.isSuccess()){
	 		
	 		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'New Vendor user created successfully.'));
	 	} else {
	 		for(Database.Error e:result.getErrors()){
	 			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'This process is not completed. Cause: '+ e.getMessage()));
	 		}
	 	}
	}
	
	//Activate an Order Select vendor user. 
	public void activateOrderSelectVendorUser(){
		
		//Check for flag
		if(!activationPossible){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Contact is already a User in a Community'));
			return;
		}
			
		//instance of User record
		User newUser = new User();
		newUser.Email = con.Email;
		newUser.FirstName = con.FirstName;
		newUser.LastName = con.LastName;
		newUser.City = con.MailingCity;
		newUser.State = con.MailingState;
		newUser.Street = con.MailingStreet;
		newUser.PostalCode = con.MailingPostalCode;
		newUser.ContactId = con.Id;
		newUser.ProfileId = OSCommunityProfile.Id;
		newUser.Username = con.Email;
		newUser.TimeZoneSidKey = 'America/Los_Angeles';
		newUser.LocaleSidKey = 'en_US';
		newUser.EmailEncodingKey = 'UTF-8'; 
		newUser.LanguageLocaleKey = 'en_US';
		setRole(newUser);
		newUser.alias = getAlias();
		newUser.CommunityNickname = newUser.alias;
		newUser.IsActive = true;
		Database.saveResult result = Database.insert(newUser,false);
	 	if(result.isSuccess()){
	 		
	 		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'New OrderSelect Vendor user created successfully.'));
	 	} else {
	 		for(Database.Error e:result.getErrors()){
	 			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'This process is not completed. Cause: '+ e.getMessage()));
	 		}
	 	}	
	}
	
	//Generate a unique alias.
	private string getAlias(){
		string alias = '';
		string a = '';
		a+=con.FirstName==null?'':con.FirstName.left(1);
		a+=con.LastName==null?'':con.LastName.left(4);
		alias = a;
		a+='%';
		User[] users = [select alias from User where alias like :a];
		Set<string> aliases = new Set<string>();
		for(User u:users){
			aliases.add(u.alias);
		}
		integer i = 2;
		string tmpAlias = alias;
		while(aliases.contains(tmpAlias)){
			tmpAlias = alias+string.valueOf(i); 
			i++;
		}
		alias = tmpAlias;
		return alias;
	}
	
	//change the contact's account prior to inserting new user.
	public void changeContactAccount(){
		
		//Check for flag
		if(!activationPossible)
			return;
		
	 	con.AccountId  = vendorCommunityAccount.Id;
	 	Database.saveResult result = database.update(con,false);
	 	if(!result.isSuccess()){
	 		for(Database.Error e:result.getErrors()){
	 			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'This process is not completed. Cause: '+ e.getMessage()));
	 		}
	 	}
	}
	
	public void setRole(User newUser){
		newUser.UserRoleId = VCPURole.Id;
	}
	
public void deactivateExistingVendors(){
 	//Check for flag
	if(!activationPossible)
	{
		return;
	}
 	if(portalVendors == null || portalVendors.Size()<=0){
 		return;
 	}
 	for(User u:portalVendors){
 			u.IsActive = false;
 			u.IsPortalEnabled = false;
 			u.Alias = '_'+u.Alias;
 			u.CommunityNickname = '_'+u.CommunityNickname;
 	}
 	Database.saveResult[] results = database.update(portalVendors,false);
 	for(Database.saveResult r:results){
 		if(!r.isSuccess()){
 			for(Database.Error e:r.getErrors()){
 				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getMessage()));
 			}
 		}
 	}
 	//loadVendors();
 	//Database.ExecuteBatch(new BatchMigateVendor(),1);
 }

public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
	
}