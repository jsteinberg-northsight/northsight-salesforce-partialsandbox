public without sharing class PortalUpdaterControllerHelper {
	
	static public List<Portal_Update_Setup__c> fetchVendorCodes() {
		return [SELECT Vendor_Code__c FROM Portal_Update_Setup__c];
	}
	
	static public List<Case> fetchWorkOrders(Set<String> vendorCodes) {
		return [SELECT
		                Vendor_Code__c,
		                Client__c,
		                Update_Priority__c
		            FROM
		                Case
		            WHERE
		                Approval_Status__c = 'Pending' AND
		                Date_Serviced1__c <= TODAY AND
		                Special_Review_Checkbox__c = false AND
		                Vendor_Code__c IN: vendorCodes
		                ORDER BY Client__c, Vendor_Code__c];
	}
	
	static public List<Case> fetchWorkOrderInprocess(Id userId) {
		return [SELECT
		                Id,
		                CaseNumber
		            FROM 
		                Case
		            WHERE
		                In_Process_By__c =: userId AND
		                Approval_Status__c = 'In Process'
		             LIMIT 1];
	}
	
	static public Id setWorkOrderToInProcess(String vendorCode) {
		try {
			Case selectedWorkOrder = [SELECT
			                                              Approval_Status__c
			                                          FROM
			                                              Case
			                                          WHERE
			                                              Approval_Status__c = 'Pending' AND
			                                              Date_Serviced1__c <= TODAY AND
			                                              Special_Review_Checkbox__c = false AND
			                                              Vendor_Code__c =: vendorCode
			                                          ORDER BY
			                                              Update_Priority__c DESC,
			                                              Pre_Pending_Date_Time__c ASC
			                                          LIMIT 1];
			selectedWorkOrder.Approval_Status__c = 'In Process';
			selectedWorkOrder.In_Process_By__c = UserInfo.getUserId();
			update selectedWorkOrder;
			return selectedWorkOrder.Id;
		} catch (Exception e) {
			return null;
		}
	}
	
	@RemoteAction
	public static Id getWorkOrderUpdatedToInProcess(String vendorCode) {
		Case selectedWorkOrder = [SELECT
		                                              Id
		                                          FROM
		                                              Case
		                                          WHERE
		                                              OwnerId =: UserInfo.getUserId() AND
		                                              Approval_Status__c = 'In Process' AND
		                                              Date_Serviced1__c <= TODAY AND
		                                              Special_Review_Checkbox__c = false AND
		                                              Vendor_Code__c =: vendorCode
		                                          ORDER BY
		                                              Update_Priority__c DESC,
		                                              Pre_Pending_Date_Time__c ASC
		                                          LIMIT 1];
		return selectedWorkOrder.Id;
	}
}