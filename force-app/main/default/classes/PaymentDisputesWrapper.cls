public with sharing class PaymentDisputesWrapper {

private final Payment_Dispute__c dispute;

public  PaymentDisputesWrapper(ApexPages.StandardController controller){
	dispute = (Payment_Dispute__c) controller.getRecord();
}

}