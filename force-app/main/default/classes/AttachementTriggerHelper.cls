/**
 *  Purpose         :   This is helper class for Trigger on Attachment to performing all Post and Pre logic.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   09/26/2014
 *
 *  Current Version :   V1.1
 *
 *  Revision Log    :   V1.0 - Created - SM-215: Evaluate work involved to upload EchoSign documents to Content
 *						V1.1 - Modified - Padmesh (10/14/2014) - SM-235: Echosign Content: Set Author
 **/
public without sharing class AttachementTriggerHelper {
	
	
	/**
	 *	@description	:	This method is used for creating Salesforce Content when an attachement is inserted as signed with EchoSign Agreement. 
	 *							 						
	 *	@param			:	New Attachments(Trigger.new)
	 *
	 *	@retrun			:	void
	 **/
	public static void stampContentOnContact(List<Attachment> newAttachments) {
		
		//Map to hold list of Attachements corresponding to their ParentId
		Map<Id,List<Attachment>> attachmentsAsAgreement = new Map<Id,List<Attachment>>();
		
		//Map to hold attachement with corresponding to their Ids
		Map<Id,Attachment> attachments = new Map<Id,Attachment>();
		
		//Getting keyprefix of echosign_dev1__SIGN_Agreement__c sobject for 
		//comparison and reducing the loop and populate the 
		//map of only Agreement related Attachements into map
		String keyPrefix = echosign_dev1__SIGN_Agreement__c.sObjectType.getDescribe().getKeyPrefix();
		
		//Loop through Trigger.new
		for( Attachment attachment : newAttachments) {
			
			//If the attachment parent id is not set, skip this attachment
			if(attachment.ParentId != null && String.valueOf(attachment.ParentId).left(3) == keyPrefix) {
				
				//Check for map is already contains Attachement's parentId
				if(attachmentsAsAgreement.containsKey(attachment.ParentId)) {
					
					//add attachement on existing key of map
					attachmentsAsAgreement.get(attachment.ParentId).add(attachment);
				} else {
					
					//populate the map
					attachmentsAsAgreement.put(attachment.ParentId, new List<Attachment>{attachment});
				}
				
				//Save attachments keyed off it id
				attachments.put(attachment.Id,attachment);
			}
		}
		
		//Check for size of map
		if(attachmentsAsAgreement.size() > 0) {
			
			//Default library Id to store all Salesforce Content
			Id VendorSignedDocumentsLibraryId = [Select Id from ContentWorkspace where Name ='Vendor Info' limit 1].Id;
			
			//query records of Attachment
			attachments = new Map<Id, Attachment>([SELECT Id, Name, Description, ParentId, Body, BodyLength, ContentType, OwnerId FROM Attachment 
													WHERE Id IN :attachments.keySet()]);
		
			//Code Modified - Padmesh (10/14/2014) - SM-235: Echosign Content: Set Author
			//New field OwnerId added into SOQL
			//Populate a map as querying records on Agreement
			Map<Id, echosign_dev1__SIGN_Agreement__c> agreements = new Map<Id, echosign_dev1__SIGN_Agreement__c>
																			([SELECT Name, echosign_dev1__Recipient__c, OwnerId FROM echosign_dev1__SIGN_Agreement__c 
																					where Id IN :attachmentsAsAgreement.keySet()]);
			
			//List to hold ContentVersion records to be inserted
			List<ContentVersion> versions = new List<ContentVersion>();
			
			//Loop through map of Attachments
			for( Attachment attachment : attachments.values() ) {
				
				//Check for - signed keyword is containing into name
				if(String.isNotBlank(attachment.Name) && attachment.Name.contains('- signed') ) {
					
					//Getting an instance of agreement form map
					echosign_dev1__SIGN_Agreement__c agreement = agreements.get( attachment.ParentId );
					
					//Check for agreement not null
					if(agreement != null) {
							
						//instance of ContentVersion
						ContentVersion version = new ContentVersion();
						version.Title = agreement.Name;
						version.VersionData = attachment.Body;
						version.Description = attachment.Description;
						version.PathOnClient = attachment.Name;
						version.FirstPublishLocationId = VendorSignedDocumentsLibraryId;
						version.Contact__c = agreement.echosign_dev1__Recipient__c;
						
						//Code Modified - Padmesh (10/14/2014) - SM-235: Echosign Content: Set Author
						//assign author to Agreement's Owner
						version.OwnerId = agreement.OwnerId;
						
						//populate the list of ContentVersion
						versions.add(version);
					}
				}
			}
			
			//Check for size of ContentVersion list and insert it
			if(versions.size() > 0)
				insert versions;
		}
	}	
}