public with sharing class AAAxmlTest {

	/*
	
	BatchGetZillowLotSize b = new BatchGetZillowLotSize();
		
		Database.executeBatch(b, 1);
	*/

	public static void main(){
	
		string NOLAxml = '<?xml version="1.0" encoding="UTF-8"?><SearchResults:searchresults xsi:schemaLocation="http://www.zillow.com/static/xsd/SearchResults.xsd http://www.zillowstatic.com/vstatic/43cd01f/static/xsd/SearchResults.xsd" xmlns:SearchResults="http://www.zillow.com/static/xsd/SearchResults.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><request><address>2515 Dublin St</address><citystatezip>new orleans LA 70118</citystatezip></request><message><text>Request successfully processed</text><code>0</code></message><response><results><result><zpid>73845596</zpid><links><homedetails>http://www.zillow.com/homedetails/2515-Dublin-St-New-Orleans-LA/73845596_zpid/</homedetails><graphsanddata>http://www.zillow.com/homedetails/2515-Dublin-St-New-Orleans-LA/73845596_zpid/#charts-and-data</graphsanddata><mapthishome>http://www.zillow.com/homes/73845596_zpid/</mapthishome><comparables>http://www.zillow.com/homes/comps/73845596_zpid/</comparables></links><address><street>2515 Dublin St</street><zipcode> </zipcode><city>New Orleans</city><state>LA</state><latitude>29.957577</latitude><longitude>-90.120287</longitude></address><FIPScounty>22071</FIPScounty><useCode>SingleFamily</useCode><taxAssessmentYear>2015</taxAssessmentYear><taxAssessment>87900.0</taxAssessment><finishedSqFt>1188</finishedSqFt><bathrooms>1.0</bathrooms><bedrooms>2</bedrooms><totalRooms>2</totalRooms><lastSoldDate>10/22/1998</lastSoldDate><lastSoldPrice currency="USD">42500</lastSoldPrice><zestimate><amount currency="USD">208786</amount><last-updated>03/13/2017</last-updated><oneWeekChange deprecated="true" /><valueChange duration="30" currency="USD">45316</valueChange><valuationRange><low currency="USD">189995</low><high currency="USD">233840</high></valuationRange><percentile>0</percentile></zestimate><localRealEstate><region name="Leonidas" id="274294" type="neighborhood"><links><overview>http://www.zillow.com/local-info/LA-New-Orleans/Leonidas/r_274294/</overview><forSaleByOwner>http://www.zillow.com/leonidas-new-orleans-la/fsbo/</forSaleByOwner><forSale>http://www.zillow.com/leonidas-new-orleans-la/</forSale></links></region></localRealEstate></result></results></response></SearchResults:searchresults>';
		string exampleXML = '<?xml version="1.0" encoding="UTF-8"?><SearchResults:searchresults xsi:schemaLocation="http://www.zillow.com/static/xsd/SearchResults.xsd http://www.zillowstatic.com/vstatic/43cd01f/static/xsd/SearchResults.xsd" xmlns:SearchResults="http://www.zillow.com/static/xsd/SearchResults.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><request>    <address>2114 Bigelow Ave</address>    <citystatezip>Seattle, WA</citystatezip>    </request>    <message>    <text>Request successfully processed</text>    <code>0</code>    </message>    <response>    <results>    <result>    <zpid>48749425</zpid>    <links>    <homedetails>    http://www.zillow.com/homedetails/2114-Bigelow-Ave-N-Seattle-WA-98109/48749425_zpid/    </homedetails>    <graphsanddata>    http://www.zillow.com/homedetails/charts/48749425_zpid,1year_chartDuration/?cbt=8224687894635748395%7E7%7EjS-H-hFDCRzaVl6bMy4IjMErWd4OhP23IK8vmp4_m9u_SO1ruBhoCA**    </graphsanddata>    <mapthishome>http://www.zillow.com/homes/map/48749425_zpid/</mapthishome>    <comparables>http://www.zillow.com/homes/comps/48749425_zpid/</comparables>    </links>    <address>    <street>2114 Bigelow Ave N</street>    <zipcode>98109</zipcode>    <city>Seattle</city>    <state>WA</state>    <latitude>47.63793</latitude>    <longitude>-122.347936</longitude>    </address>    <FIPScounty>33</FIPScounty>    <useCode>SingleFamily</useCode>    <taxAssessmentYear>2008</taxAssessmentYear>    <taxAssessment>1054000.0</taxAssessment>    <yearBuilt>1924</yearBuilt>    <lotSizeSqFt>4680</lotSizeSqFt>    <finishedSqFt>3470</finishedSqFt>    <bathrooms>3.0</bathrooms>    <bedrooms>4</bedrooms>    <lastSoldDate>11/26/2008</lastSoldDate>    <lastSoldPrice currency="USD">995000</lastSoldPrice>    <zestimate>    <amount currency="USD">1219500</amount>    <last-updated>12/31/1969</last-updated>    <oneWeekChange deprecated="true"/>    <valueChange duration="30" currency="USD">-41500</valueChange>    <valuationRange>    <low currency="USD">1024380</low>    <high currency="USD">1378035</high>    </valuationRange>    <percentile>0</percentile>    </zestimate>    <localRealEstate>    <region id="271856" type="neighborhood" name="East Queen Anne">    <zindexValue>525,397</zindexValue>    <links>    <overview>    http://www.zillow.com/local-info/WA-Seattle/East-Queen-Anne/r_271856/    </overview>    <forSaleByOwner>    http://www.zillow.com/homes/fsbo/East-Queen-Anne-Seattle-WA/    </forSaleByOwner>    <forSale>    http://www.zillow.com/east-queen-anne-seattle-wa/    </forSale>    </links>    </region>    <region id="16037" type="city" name="Seattle">    <zindexValue>381,764</zindexValue>    <links>    <overview>    http://www.zillow.com/local-info/WA-Seattle/r_16037/    </overview>    <forSaleByOwner>http://www.zillow.com/homes/fsbo/Seattle-WA/</forSaleByOwner>    <forSale>http://www.zillow.com/seattle-wa/</forSale>    </links>    </region>    <region id="59" type="state" name="Washington">    <zindexValue>263,278</zindexValue>    <links>    <overview>    http://www.zillow.com/local-info/WA-home-value/r_59/    </overview>    <forSaleByOwner>http://www.zillow.com/homes/fsbo/WA/</forSaleByOwner>    <forSale>http://www.zillow.com/wa/</forSale>    </links>    </region>    </localRealEstate>    </result>    </results>    </response>    </SearchResults:searchresults>';
		
		Dom.Document document=new Dom.Document();
		document.load(exampleXML);
		
		Dom.XMLNode rootElement = document.getRootElement();
		
		//Rerieve the child element response from root element
        Dom.XmlNode responseNode = rootElement.getChildElement('response', null);
		
		Dom.XmlNode resultsNode = responseNode.getChildElement('results', null);
		Dom.XmlNode resultNode = resultsNode.getChildElement('result', null);
		
		Dom.XmlNode localRealEstateNode = resultNode.getChildElement('localRealEstate', null);
		if(localRealEstateNode.getChildElement('region', null) != null) { 
			Dom.XmlNode regionNode = localRealEstateNode.getChildElement('region', null);
			Dom.XmlNode links1Node = regionNode.getChildElement('links', null);
			Dom.XmlNode zindexValueNode = regionNode.getChildElement('zindexValue', null);
			
			//string val = regionNode.getAttributeValue('name', regionNode.getNamespace());
			string val = zindexValueNode.getText();
			system.debug(val);						
			
		}
			
	}

}