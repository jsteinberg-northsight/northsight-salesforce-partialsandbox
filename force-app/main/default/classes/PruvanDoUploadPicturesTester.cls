@isTest
private class PruvanDoUploadPicturesTester {
//	private static Case newCase;
//	private static String photoHelperPayload;
//	
//	//New variable add to hold input JSON string of Survey input payload 
//	//Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
//	private static String surveyHelperPayload;
//	
//	private static void generateTestCaseWithContact(){
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//   		//create a new account
//		Account newAccount = new Account(
//			Name = 'Tester Account'
//		);
//		insert newAccount;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//		//create a new contact
//		Contact newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest'
//		);
//			newContact.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//	newContact.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//	newContact.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//	newContact.Shrub_Trim__c = 3;//Pricing...required 
//	newContact.REO_Grass_Cut__c = 3;//Pricing
//	newContact.Pay_Terms__c = 'Net 30';//Terms required
//	newContact.Trip_Charge__c = 25;//Pricing
//	newContact.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//	newContact.Date_Active__c = Date.Today();//Date Active must be set. 
//	newContact.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//	newContact.MailingStreet = '30 Turnberry Dr';
//	newContact.MailingCity = 'La Place';
//	newContact.MailingState = 'LA';
//	newContact.MailingPostalCode = '70068';
//	newContact.OtherStreet = '30 Turnberry Dr';
//	newContact.OtherCity = 'La Place';
//	newContact.OtherState = 'LA';
//	newContact.OtherPostalCode = '70068';
//	newContact.Other_Address_Geocode__Latitude__s = 30.00;
//	newContact.Other_Address_Geocode__Longitude__s = -90.00;
//		
//		
//		insert newContact;
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//   		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		//create a new test case
//		newCase = new Case();
//		newCase.street_address__c = '1234 Street';
//        newCase.state__c = 'TX';
//        newCase.city__c = 'Austin';
//        newCase.zip_code__c = '78704';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        newCase.Client__c = 'NOT-SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'Grass Recut';
//        newCase.Client_Name__c = account.Id;
//        
//        insert newCase;
//	}
//	
//	private static void setPayload(){
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//   		//create a new photo payload
//		for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//			photoHelperPayload = 	'{' +
//								    '"timestamp": 1377559122,' +
//								    '"serviceId": "32981757",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "before",' +
//								    '"fileType": "picture",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "d9aab2a9-eec6-470e-bf13-0cb74ca47e75",' +
//								    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//								    '"fileName": "1382536931021.jpg",' +
//								    '"gpsLatitude": "31.62020098900615",' +
//								    '"gpsLongitude": "-97.099915453764",' +
//								    '"gpsTimestamp": "1382536931",' +
//								    '"gpsAccuracy": "10.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706368.jpg"' +
//									'}';
//		}
//	}
//	
//	//Method added for getting some JSON String to call REST service methods
//	//Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
//	private static void setPayloads(){
//		
//		//Loop through queried record of Work Order(Case)
//		for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//			
//			//JSON String to hold getting Picture input payload
//			photoHelperPayload = 	'{' +
//								    '"timestamp": 1377559122,' +
//								    '"serviceId": "32981757",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "Survey",' +
//								    '"fileType": "picture",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "d9aab2a9-eec6-470e-bf13-0cb74ca47e75",' +
//								    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//								    '"fileName": "1382536931021.jpg",' +
//								    '"gpsLatitude": "31.62020098900615",' +
//								    '"gpsLongitude": "-97.099915453764",' +
//								    '"gpsTimestamp": "1382536931",' +
//								    '"gpsAccuracy": "2.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706368.jpg"' +
//									'}';
//			
//			//JSON string to hold input	payload of Survey data
//			surveyHelperPayload = 	'{' +
//								    '"timestamp": 1377559123,' +
//								    '"serviceId": "32786734",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "Survey",' +
//								    '"fileType": "survey",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//								    '"parentUuid": "37064308",' +
//								    '"fileName": "survey_1382537334014.json",' +
//								    '"gpsLatitude": "42.01235463",' +
//								    '"gpsLongitude": "-87.89172686",' +
//								    '"gpsTimestamp": "1382537316",' +
//								    '"gpsAccuracy": "10.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"uploaderVersion": "mobile 3.9.1",' +
//								    '"deviceId": "ios-1B3F968D-5F14-47F0-95EC-89A15EFBA580",' +
//								    '"sdkVersion": "7.0.4",' +
//								    '"survey": {' +
//								        '"answers": [{' +
//								            '"answer": ["yes"],' +
//								            '"id": "Work_Completed__c",' +
//								            '"picIds": ["d9aab2a9-eec6-470e-bf13-0cb74ca47e75"],' +
//								            '"hint": "Select No for Trip Charge"' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Backyard_Serviced__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Excessive_Growth__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Shrubs_Trimmed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Debris_Removed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["Excessive Grass/Weed Height"],' +
//								            '"id": "Bid_Type__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["This is the bid description."],' +
//								            '"id": "Bid_Description__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["100abc"],' +
//								            '"id": "Bid_Cost__c",' +
//								            '"hint": ""' +
//								        '}],' +
//								        '"meta": {' +
//								            '"currentQuestionIndex": 18,' +
//								            '"surveyId": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//								            '"parentId": "37064308",' +
//								            '"surveyTemplateId": "sarcj07::GC_001-v4"' +
//								        '}' +
//								    '},' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706991.json"' +
//									'}';
//		}
//	}
//	
//	//--------------------------------------------------------
//   	//TESTS
//   	//Unit tests for method testing and regression testing. 
//   	//--------------------------------------------------------
//   		
//   	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - send a photo payload to the PruvanDoUploadPictures REST resource
//   	//and assert that we get the correct result back
//    static testMethod void testRestResourceFunctionality() {
//        system.Test.startTest();
//        
//        generateTestCaseWithContact();
//        setPayload();
//		
//    	//Code commented because webservice callout cann't be possible with testmethod
//		//Padmesh Soni - 02/20/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
//		//instantiate a new http var
//        /**Http h = new Http();
//
//		//Instantiate a new HTTP request, specify the method (GET) as well as the endpoint 
//	    HttpRequest req = new HttpRequest();
//	    req.setEndpoint('http://full-allyearservices.cs15.force.com/services/apexrest/UploadPicturesREST');
//	    req.setMethod('POST');
//	    req.setBody(photoHelperPayload);
//	
//		//Send the request, and set a http response var with the return value
//	    HttpResponse res = h.send(req);
//	    
//	    //assert that the proper response was sent back
//        //system.assertEquals('{"status":true,"error":null}', res.toString());**/
//	    
//	    //Code added for testing REST service class
//	    //Padmesh Soni - 02/20/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
//	    // set up the request object
//        RestContext.request = new RestRequest();
//        
//        //Setting request URI of RestContext
//        RestContext.request.requestURI = '/UploadPicturesREST';
//        
//        //Setting body of RestContext
//        RestContext.request.requestBody = Blob.valueOf(photoHelperPayload);
//        
//        // set up the response object
//        RestContext.response = new Restresponse();
//        
//        //Call webservice method of REST class
//		PruvanDoUploadPictures.doPost();
//		
//        //assert that the proper response was sent back
//        //system.assertEquals('{"status":true,"error":null}', RestContext.response.responseBody.toString());
//        
//        system.Test.stopTest();
//    }
//    
//    //Test method to test the functionality after modification
//    //Padmesh Soni - 02/20/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
//    static testMethod void testRestResourceSurveyFunctionality() {
//    	
//        Test.startTest();
//        
//        generateTestCaseWithContact();
//        setPayloads();
//        
//        // set up the request object
//        RestContext.request = new RestRequest();
//        
//        //Setting request URI of RestContext
//        RestContext.request.requestURI = '/UploadPicturesREST';
//        
//        //Setting body of RestContext
//        RestContext.request.requestBody = Blob.valueOf(surveyHelperPayload);
//        
//        // set up the response object
//        RestContext.response = new Restresponse();
//        
//        //Call webservice method of REST class
//		PruvanDoUploadPictures.doPost();
//		
//        //assert that the proper response was sent back
//        //system.assertEquals('{"status":true,"error":null}', RestContext.response.responseBody.toString());
//        
//        Test.stopTest();
//    }
}