global class ScheduleBatchCheckGeocoder implements Schedulable{
	global void execute(SchedulableContext ctx){
		BatchCheckGeocoder b = new BatchCheckGeocoder();
		
		b.start();
	}
	
	//Padmesh Soni (05/05/2014) - Required Maintenance: Unit Test Improvements
    //Test method added
	static testmethod void batchCheckGeocoderTest(){
		
		//create a cron expression test obj
		string CRON_EXP = '0 0 0 * * ? *';
		
		//start the test
		Test.startTest();
		
		//create a string of the jobId for a new schedule instance of the ScheduleBatchCheckGeocoder class
		string jobId = System.schedule('ScheduleBatch11thHourCommits', CRON_EXP, new ScheduleBatchCheckGeocoder());
		
		//query the CronTrigger table selecting in CronTrigger fields where the Id = jobId
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id =: jobId];
		system.assertEquals(CRON_EXP, ct.CronExpression);
		
		//assert that the job has not been triggered
		system.assertEquals(0, ct.TimesTriggered);
		
		//assert when the next fire time will be
		system.assert(ct.NextFireTime != null);
		
		//Test stops here
		Test.stopTest();
	}
}