/**
 *	Purpose			:	This class is to handle email services
 *
 *	Created By		:	Padmesh Soni
 *
 *	Created Date	:	02/04/2014
 *
 *	Version			:	V1.0
 **/
public class EmailHandler {

	/**
	 * 	@description	:	This method is used to send single email messages.
	 * 
	 * 	@param			:	Recipient email address, Subject of email, Body of email
	 * 
	 * 	@return			:	void
	 **/
	 public static void sendEmail(String toAddress, String subject, String body) {
    
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       // email address of the salesforce email service
       String[] toAddresses = new String[] {toAddress};
       mail.setToAddresses(toAddresses);
       mail.setSubject(subject);
       mail.setPlainTextBody(body);
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
	/**
	 * 	@description	:	This method is used to send single email messages.
	 * 
	 * 	@param			:	Recipient email address, Subject of email, Body of email, HTML body of email
	 * 
	 * 	@return			:	void
	 **/
    public static void sendEmail(String toAddress, String subject, String body, String htmlBody) {
    
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       // email address of the salesforce email service
       String[] toAddresses = new String[] {toAddress};
       mail.setToAddresses(toAddresses);
       mail.setSubject(subject);
       mail.setPlainTextBody(body);
       mail.setHtmlBody(htmlBody);
       if(!system.test.isrunningtest()){
       	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       }
    }
    
	/**
	 * 	@description	:	This method is used to send single email messages.
	 * 
	 * 	@param			:	Recipients email addresses, Parent object id, Related object id, Email template id 
	 * 
	 * 	@return			:	void
	 **/
    public static void sendEmail(List<String> toAddresses, Id whatId, Id targetObjectId, Id templateId) {
    
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setTemplateId(templateId);
        mail.setToAddresses(toAddresses);
        mail.setWhatId(whatId);
        mail.setTargetObjectId(targetObjectId);
        if(!system.test.isrunningtest()){
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}