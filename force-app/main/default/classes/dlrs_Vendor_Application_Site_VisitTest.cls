/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Vendor_Application_Site_VisitTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Vendor_Application_Site_Va3pTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Vendor_Application_Site_Visit__c());
    }
}