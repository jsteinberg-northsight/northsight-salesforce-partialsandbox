public without sharing class ViewImagesPublicController {
    public WorkOrderWrapper[] workorders{get;set;}

    public ViewImagesPublicController(){
    	workorders = new WorkOrderWrapper[]{};
    	
        for(Case c : [SELECT Id, Work_Ordered__c, CaseNumber, Display_Address__c, Reference_Number__c FROM Case where Id =: ApexPages.currentPage().getParameters().get('sfid')]){
              WorkOrderWrapper w = new WorkOrderWrapper(c);
              workorders.add(w);
        }
    }
}