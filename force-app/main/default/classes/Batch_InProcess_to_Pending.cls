global without sharing class Batch_InProcess_to_Pending implements Database.Batchable<Case> {
    Datetime hourOld = Datetime.now().addHours(-1);//datetime var that will be used to show if a an object has a datetime that is an hour old
    Datetime twoHoursOld = Datetime.now().addHours(-2);
    Datetime fiveHoursOld = Datetime.now().addHours(-5);
    
    global Iterable<Case> start(Database.BatchableContext BC) {
        
        //Padmesh Soni (06/18/2014) - Support Case Record Type
        //Query record of Record Type of Case object
        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
                                            AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
        
        //select the approval status and auto bounce count of work orders where the approval status = 'In Process' and the last status change is an hour old or more
        //Padmesh Soni (06/18/2014) - Support Case Record Type
        //New filter criteria of RecordType added
        List<Case> cases = [select Id, 
                                   Approval_Status__c, 
                                   Auto_Bounce_Count__c,
                                   Work_Completed__c ,
                                   Work_Ordered__c,
                                   Last_Status_Change__c,
                                   Department__c
                            from Case 
                            where RecordTypeId NOT IN: recordTypes 
                            AND Approval_Status__c = 'In Process' 
                            and Last_Status_Change__c <: hourOld];
        
        return cases;//send the cases list to the execute method
    }
    
    global void execute(Database.BatchableContext BC, List<Case> selectedCases) {
        if(selectedCases != null && selectedCases.size() > 0){
            for(Case c : selectedCases){//for each case in the list
                if(c.Work_Ordered__c.contains('Grass')){
                    if(c.Work_Completed__c == 'Yes' || c.Work_Completed__c == 'Trip Charge'){
                        c.Approval_Status__c = 'Pending';//set approval status to 'Pending'
                        if(c.Auto_Bounce_Count__c == null){
                            c.Auto_Bounce_Count__c = 0;
                        }
                        c.Auto_Bounce_Count__c = c.Auto_Bounce_Count__c + 1;//add 1 to auto bounce count
                    }
                } else{
                    if(c.Last_Status_Change__c < fiveHoursOld && c.Department__c == 'Repairs'){
                        if(c.Work_Completed__c == 'Yes' || c.Work_Completed__c == 'Trip Charge'){
                            c.Approval_Status__c = 'Pending';//set approval status to 'Pending'
                            if(c.Auto_Bounce_Count__c == null){
                                c.Auto_Bounce_Count__c = 0;
                            }
                            c.Auto_Bounce_Count__c = c.Auto_Bounce_Count__c + 1;//add 1 to auto bounce count
                        }
                    } else if(c.Last_Status_Change__c < twoHoursOld && c.Department__c != 'Repairs'){
                            if(c.Work_Completed__c == 'Yes' || c.Work_Completed__c == 'Trip Charge'){
                                c.Approval_Status__c = 'Pending';//set approval status to 'Pending'
                            if(c.Auto_Bounce_Count__c == null){
                                c.Auto_Bounce_Count__c = 0;
                            }
                            c.Auto_Bounce_Count__c = c.Auto_Bounce_Count__c + 1;//add 1 to auto bounce count
                        }
                    }
                }
            }
        }
        
        if(selectedCases != null && selectedCases.size() > 0){//if the list has cases
            database.update(selectedCases, false);//update the list
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        //section not used
    }
}