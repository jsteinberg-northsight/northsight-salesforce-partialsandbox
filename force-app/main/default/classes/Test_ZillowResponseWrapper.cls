@isTest(seeAllData=false)
private class Test_ZillowResponseWrapper {
/**
 *  Purpose         :   Test class for ZillowResponseWrapper. 
 *
 *  Created By      :   
 *
 *  Created Date    :   12/12/2013
 *
 *  Current Version :   V1.0
 *
 *  Code Coverage   :   100%    
 **/
    
    //Method to test ZillowResponseWrapper
    static testMethod void myUnitTest() {
        
        //Http request instance
        Httprequest req = new Httprequest();
        
        //Mock class instance
        ZillowControllerMockClass zMock = new ZillowControllerMockClass();
        
        //Instance of test response
        Httpresponse response = zMock.respond(req);
        
        //Test starts here
        Test.startTest();
        
        //Constructor of ZillowResponseWrapper
        ZillowResponseWrapper zResp = new ZillowResponseWrapper(response.getBodyDocument());
        
        //assert statements
        System.assertEquals('4680', zResp.lotSizeSqFt);
        
        //Test stops here
        Test.stopTest(); 
    }
}