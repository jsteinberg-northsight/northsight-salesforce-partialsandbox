public with sharing class InvoicingWrapperTester {
	public string workOrder;
	public string vendorId;
	public string vendor;
	public string total;
	public string subTotal;
	public string status;
	public string sent;
	public string recordType;
	public string publicNotes;
	public string privateNotes;
	public string priceBook;
	public string paidDate;
	public string invoiceNumber;
	public string id;
	public decimal discount;
	public string created;
	public string clientId;
	


}