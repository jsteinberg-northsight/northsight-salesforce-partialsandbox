@isTest(SeeAllData=true)
public without sharing class InvoicingTester {
//    private static Case newCase;
//    private static Invoice__c inv1;
//    private static Invoice_Line__c invL;
//    private static void generateTestCase() {
//
//        Account newAccount = new Account(
//            Name = 'Tester Account'
//        );
//        insert newAccount;
//
//        List<RecordType> contactRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Vendor'
//            AND SobjectType = 'Contact' AND IsActive = true];
//        Contact newContact = new Contact(
//            FirstName = 'New',
//            LastName = 'Tester',
//            AccountId = newAccount.Id,
//            Pruvan_Username__c = 'NSBobTest',
//            MailingStreet = '30 Turnberry Dr',
//            MailingCity = 'La Place',
//            MailingState = 'LA',
//            MailingPostalCode = '70068',
//            OtherStreet = '30 Turnberry Dr',
//            OtherCity = 'La Place',
//            OtherState = 'LA',
//            OtherPostalCode = '70068',
//            Other_Address_Geocode__Latitude__s = 30.00,
//            Other_Address_Geocode__Longitude__s = -90.00,
//            RecordTypeId = contactRecordTypes[0].Id
//        );
//        insert newContact;
//
//        //create a new test case
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//
//        Account account =  new Account(
//            Name = 'Invoice Test Account No Duplicate',
//            recordTypeId = accountRecordTypes[0].Id
//        );
//        insert account;
//
//        newCase = new Case();
//        newCase.street_address__c = '1234 Street';
//        newCase.state__c = 'TX';
//        newCase.city__c = 'Austin';
//        newCase.zip_code__c = '78704';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Grass';
//        newCase.Client__c = 'NOT-SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'Grass Recut';
//        newCase.Work_Completed__c = 'No';
//        newCase.Client_Name__c = account.Id;
//
//        insert newCase;
//        newCase = [SELECT Id
//                FROM Case
//                WHERE Id = :newCase.Id];
//
//        Id receivableInv = [SELECT Id FROM RecordType WHERE Name = 'Receivable' AND SobjectType = 'Invoice__c'].Id;
//        Date currTime = Date.today();
//        inv1 = new Invoice__c(
//            Work_Order__c = newCase.Id,
//            Paid_Date__c = currTime,
//            Status__c = 'Open', Sent__c =  currTime,
//            Receive_From__c = newAccount.Id,//'0014000000IsTotAAF',
//            RecordTypeId  = receivableInv//'012f00000008rWJ'
//        );
//        insert inv1;
//        inv1 = [SELECT Id, Paid_Date__c, Work_Order__c,Name, Status__c, Receive_From__c, RecordTypeId
//                FROM Invoice__c
//                WHERE Id = :inv1.Id];
//
//        Product2 newProduct = new Product2();
//        newProduct.name ='Trip Charge';
//        newProduct.Family = 'Service Fee';
//        newProduct.IsActive = true;
//        insert newProduct;
//
//
//        Id receivableInvLine = [SELECT Id FROM RecordType WHERE Name = 'Receivable' AND SobjectType = 'Invoice_Line__c'].Id;
//        Id tripCharge = [SELECT Id FROM Product2 WHERE Name = 'Trip Charge' limit 1].Id;
//        invL = new Invoice_Line__c(
//            Product__c= tripCharge,//'01t40000004sJnVAAU',
//            Quantity__c = 24,
//            Unit_Price__c = 45, Discount__c =  40,
//            Invoice__c = inv1.Id,
//            RecordTypeId  = receivableInvLine//'012f00000008rWT'//AR
//        );
//
//        insert invL;
//    }
//
//    //Test method to test the functionality of InvoicingController class
//    static testMethod void unitTesting() {
//        generateTestCase();
//        System.assert(newCase.Id != null);
//        System.Debug('Id: '+newCase.Id);
//
//        PageReference myVfPage = Page.Invoicing;
//        Test.setCurrentPage(myVfPage);
//        system.currentPageReference().getParameters().put('Id', newCase.Id);
//        ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
//        InvoicingController testController = new InvoicingController(sc);
//        System.assert(testController != null);
//        System.assertEquals(testController.ARTotal ,1);
//        System.assert(testController.APTotal >= 0);
//        System.assert(testController.APLineTypeId != null);
//        System.assert(testController.ARLineTypeId != null);
//        System.assert(testController.APInvoiceTypeId != null);
//        System.assert(testController.ARInvoiceTypeId != null);
//        System.assert(testController.workOrder != null);
//        System.assertEquals(testController.workOrderId ,newCase.Id );
//        System.Debug('AR TOTAL: '+testController.ARTotal);
//
//        List<InvoiceLineWrapper> response = InvoicingController.loadLines(inv1.Id);
//        System.Debug(response);
//        System.assert(response.size() > 0);
//
//        List<Case> badData = [SELECT id, street_address__c, Client__c FROM Case where Id =: newCase.Id];
//        String badDataJSON = JSON.serializePretty(badData);
//        string badResponse = InvoicingController.saveInvoice(badDataJSON);
//        System.assertEquals(badResponse, 'Invalid');
//        String jsonR = testController.invoiceSerializedReceivable;
//        jsonR = jsonR.replace('[', '');
//        jsonR = jsonR.replace(']', '');
//        InvoicingWrapperTester linesJson = (InvoicingWrapperTester)JSON.deserialize(jsonR, InvoicingWrapperTester.class);
//        linesJson.discount = 20;
//        System.Debug('id = '+linesJson.id);
//        System.Debug('Id invoice: '+invL.id);
//        List<Invoice_Line__c> auxInvoiceLine = [SELECT Id, Work_Order__c, public_notes__c, Product__c ,Product__r.Name,Product__r.Description,Quantity__c,Unit_Price__c,Discount__c,
//                                            Product__r.Family,Invoice__c, Invoice__r.Name,RecordTypeId, UOM__c,Price_Book_Entry__c , Contact_Price__c, Original_Price__c FROM Invoice_Line__c WHERE id =: invL.id ];
//
//        InvoiceLineWrapper auxLine = new InvoiceLineWrapper(auxInvoiceLine.get(0));
//
//        auxLine.discount = linesJson.discount;
//        auxLine.qty = 30;
//        auxLine.unitPrice = 20;
//        auxLine.invoiceId = inv1.id ;
//        List<InvoiceLineWrapper> testLines = new List<InvoiceLineWrapper> ();
//        testLines.add(auxLine);
//
//        //Integer aux = InvoicingController.saveInvoiceLine(JSON.serialize(testLines),'AR');
//        Integer aux = InvoicingController.saveInvoiceLine(JSON.serialize(testLines));
//        System.Debug('save: '+aux);
//        System.assert(aux > 0);
//
//        auxInvoiceLine = [SELECT Id, Work_Order__c, public_notes__c, Product__c ,Product__r.Name,Product__r.Description,Quantity__c,Unit_Price__c,Discount__c,
//                                            Product__r.Family,Invoice__c, Invoice__r.Name,RecordTypeId,UOM__c FROM Invoice_Line__c WHERE id =: invL.id ];
//
//        System.assertEquals(auxInvoiceLine.get(0).Quantity__c, 30);
//        System.assertEquals(auxInvoiceLine.get(0).Unit_Price__c, 20);
//
//        system.Test.startTest();
//        List<InvoicingUtilities.iuOption> ioui = InvoicingController.getPriceBooks(newCase.Id);
//        System.assert(ioui != null);
//
//        List<InvoicingUtilities.iuOption> pricesIoui = InvoicingController.getARPriceBooks(newCase.Id);
//        System.assert(pricesIoui != null);
//
//        List<InvoicingUtilities.iuOption> pricesIouiAP = InvoicingController.getAPPriceBooks(newCase.Id);
//        System.assert(pricesIouiAP != null);
//
//        List<InvoicingUtilities.iuOption> pricesIouiAPPro = InvoicingController.getProductCategories(newCase.Id);
//        System.assert(pricesIouiAPPro != null);
//        System.Debug('uuii');
//        System.Debug(pricesIouiAPPro);
//        List<InvoicingUtilities.iuOption> pricesIouiAPProd = InvoicingController.getProducts(newCase.Id);
//        System.assert(pricesIouiAPProd != null);
//
//        List<InvoicingUtilities.iuOption> pricesIouivalues = InvoicingController.getStatusValues();
//        System.assert(pricesIouivalues != null);
//
//        try{
//            testController.refreshAP();
//            System.assert(true);
//        }catch(Exception ex)
//        {
//            System.assert(false);
//        }
//        try{
//            testController.refreshAR();
//            System.assert(true);
//        }catch(Exception ex)
//        {
//            System.assert(false);
//        }
//        System.debug('rc: '+testController.invoiceSerializedReceivable);
//        System.debug('py: '+testController.invoiceSerializedPayable);
//        String JSONString = testController.invoiceSerializedReceivable;
//        List<InvoicingWrapper> deserializedInvoices = (List<InvoicingWrapper>)JSON.deserialize(JSONString, List<InvoicingWrapper>.class);
//        System.assertEquals(deserializedInvoices.size(), 1);
//
//        InvoiceJsonWrapper invJW = new InvoiceJsonWrapper();
//        invJW.id = inv1.Id;
//        invJW.client = inv1.Receive_From__c;
//        invJW.sent = null;
//        invJW.priceBook = null;
//        invJW.status = inv1.Status__c;
//        invJW.subTotal = 324;
//        invJW.total = 332;
//        invJW.vendor = null;
//        invJW.vendorId = null;
//        invJW.privateNotes = 'test';
//        invJW.publicNotes = 'test';
//        invJW.invoiceNumber = inv1.Name;
//        invJW.workOrder = newCase.Id;
//        invJW.recordType = null;
//
//        String invJson = JSON.serialize(invJW);
//        string responseOk = InvoicingController.saveInvoice(invJson);
//        System.Debug('json:');
//        System.Debug(responseOk);
//        System.assertEquals(responseOk, inv1.Id);
//
//        //Add for more test coverage
//        invJW = new InvoiceJsonWrapper();
//        invJW.id = null;
//        invJW.client = inv1.Receive_From__c;
//        invJW.sent = null;
//        invJW.priceBook = null;
//        invJW.status = inv1.Status__c;
//        invJW.subTotal = 324;
//        invJW.total = 332;
//        invJW.vendor = null;
//        invJW.vendorId = null;
//        invJW.privateNotes = 'test';
//        invJW.publicNotes = 'test';
//        invJW.invoiceNumber = inv1.Name;
//        invJW.workOrder = newCase.Id;
//        invJW.recordType = [SELECT Id FROM RecordType WHERE Name = 'Receivable' AND SobjectType = 'Invoice__c'].Id;
//        invJson = JSON.serialize(invJW);
//        responseOk = InvoicingController.saveInvoice(invJson);
//
//        String deleteResponse = InvoicingController.deleteInvoiceLine(invL.Id);
//        System.assertEquals(deleteResponse, '');
//
//        deleteResponse = InvoicingController.deleteInvoiceLine('212');
//        System.assertEquals(deleteResponse, 'This is not a valid Invoice Line.');
//
//        system.Test.stopTest();
//    }
//
//    private static @isTest void getVendorProductsTest() {
//        Contact testContact = new Contact(
//            FirstName = 'first',
//            LastName = 'test'
//        );
//        insert testContact;
//
//        InvoicingUtilities.getVendorProducts(testContact.Id);
//    }
//
//    private static @isTest void productWrapperTest() {
//        InvoicingUtilities.productWrapper wrapper = new InvoicingUtilities.productWrapper(
//            new Contact_Price__c(
//                Price__c = 2
//            )
//        );
//        System.assertEquals(2, wrapper.rate, 'Wrapper rate set');
//
//        wrapper = new InvoicingUtilities.productWrapper(
//            new PricebookEntry(
//                UnitPrice = 3
//            )
//        );
//        System.assertEquals(3, wrapper.rate, 'Wrapper rate set');
//    }
}