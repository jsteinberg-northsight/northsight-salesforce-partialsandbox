@isTest(seeAllData=false)
private class Test_PropertyWorkOrdersListController {
///**
// *  Purpose         :   This is used for testing and covering PropertyWorkOrdersListController functionality.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   02/04/2014
// *
// *  Current Version :   V1.0
// *
// *  Revision Log    :   V1.0 - Created
// *
// *  Coverage        :   100%    
// **/
//
//    static testmethod void testbypass(){
//        
//        PropertyWorkOrdersListController.testbypass();
//    }
//    
//    //Test method to test the functionality of inIt method of controller
//    static testMethod void testGettingWorkOrders() {
//        return;
//        //Query record of Record Type of Account and Case objects
//        /*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client') AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        
//        //List of Account to store testing records
//        List<Account> accounts = new List<Account>();
//        accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accounts;
//        
//        Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//        insert property;
//        
//        //List to hold Case records
//        List<Case> workOrders = new List<Case>();
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN));
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1), Status = Constants.CASE_STATUS_OPEN));
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[1].Id, Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN));
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[1].Id, Due_Date__c = Date.today().addDays(1), Status = Constants.CASE_STATUS_OPEN));
//        
//        //insert case records
//        insert workOrders;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Initialize standard contorller of Property object
//        ApexPages.StandardController stdController = new ApexPages.standardController(property);
//        
//        //Instance of PropertyWorkOrdersListController
//        PropertyWorkOrdersListController controller = new PropertyWorkOrdersListController(stdController);
//        
//        //Query records of Cases
//        workOrders = [SELECT Id From Case WHERE Geocode_Cache__c =: property.Id];
//        
//        //Assert statements
//        System.assertEquals(workOrders.size(), controller.workOrders.size());
//        
//        //Test stops here
//        Test.stopTest();
//    }
}