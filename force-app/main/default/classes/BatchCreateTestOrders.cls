global without sharing class BatchCreateTestOrders{// implements Database.Batchable<Case> {
	/*
	integer refNum = 69696000; //# between 69696000 - 69696999 to view under testOrders filter
	 
	global Iterable<Case> start(Database.BatchableContext BC) {
		List<Case> testOrders = new List<Case>();
		for(integer i = 0; i < 10; i++){ 
			Case newCase = new Case(
				Street_Address__c = '2420 ormond blvd.',
			    City__c = 'Destrehan',
			    State__c = 'LA',
			    Zip_Code__c = '70047',
			    Due_Date__c = date.today().addDays(2),
			    Client__c = 'NOT-SG',
			    Vendor_Code__c = 'SRSRSR',
			    Work_Ordered__c = 'Initial Grass Cut',
			    Reference_Number__c = string.valueOf(refNum),
			    OwnerId = '00540000001yas1AAA'
			);
			testOrders.add(newCase);
			refNum++;
		}
		
		return testOrders;//send the cases list to the execute method
	}
	
	global void execute(Database.BatchableContext BC, List<Case> ordersToInsert) {
		if(ordersToInsert.size() > 0){//if the list has cases
			insert ordersToInsert;//update the list
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		//section not used
	}
*/
}