@isTest(seeAllData=false)
private class Test_AttachementTriggerHelper {
///**
// *  Description     :   This is test class for testing of pre-post functionality on Attachment trigger. 
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   10/14/2014
// *
// *  Current Version :   V1.0
// *
// *  Revisiion Logs  :   V1.0 - Created
// *      
// *  Code Coverage   :   V1.0 - 100%
// **/
//    
//    private static List<Account> accounts;
//    private static List<Contact> contacts;
//    private static Profile newProfile;
//    private static List<User> users;
//    private static Group queues;
//    
//    private static void generateTestData(integer countData){
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP 
//        //create a new user with the Customer Portal Manager Standard profile
//        newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Vendors'];
//        
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            /******** PRE-TEST SETUP create a new account *********/
//            accounts.add(new Account(Name = 'Tester Account' + i,RecordTypeId = '0124000000011Q2'));
//        }
//        
//        insert accounts;
//        
//        List<RecordType> recordTypes = [SELECT Id FROM RecordType WHERE DeveloperName = 'Vendor' AND SobjectType = 'Contact' AND IsActive = true];
//        
//        System.assertEquals(1, recordTypes.size());
//        
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            //--------------------------------------------------------
//            //PRE-TEST SETUP
//            //create a new contact
//            contacts.add(new Contact(RecordTypeId = recordTypes[0].Id, LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest'+i, 
//                                        Status__c = '3 - Active', Date_Active__c = Date.today(), OtherStreet = '123 Main Street', OtherCity = 'Midland',
//                                        OtherState = 'TX', OtherPostalCode = '123344', MailingStreet = '123 Main Street', MailingCity = 'Midland', 
//                                        MailingState = 'TX', MailingPostalCode = '123344', Shrub_Trim__c = 23, REO_Grass_Cut__c = 12,
//                                        Pay_Terms__c = 'Net 3', Trip_Charge__c = 10, 
//                                        Other_Address_Geocode__Latitude__s = 15.34444, Other_Address_Geocode__Longitude__s = -68.34444));
//        }
//        
//        insert contacts;
//        
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            //create a new user with the profile
//            users.add(new User(alias = 'standt'+i, contactId = contacts[i-1].Id, email = Math.random()+Math.random()+Math.random()+i+'@northsighttestorg.com', emailencodingkey = 'UTF-8',
//                                    lastname = 'Testing'+i, languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//                                    timezonesidkey = 'America/Los_Angeles', username = Math.random()+Math.random()+Math.random()+i+'@northsighttestorg.com', isActive = true));
//        }
//        
//        insert users;
//    }
//    
//    //Testing functionality of method stampContentOnContact 
//    static testMethod void test_stampContentOnContact() {
//        
//        //initialize variables
//        accounts = new List<Account>();
//        contacts = new List<Contact>();
//        users = new List<User>();
//        
//        //Create testing data
//        generateTestData(1);
//        
//        //List to hold Agreements test records
//        List<echosign_dev1__SIGN_Agreement__c> agreements = new List<echosign_dev1__SIGN_Agreement__c>();
//        agreements.add(new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Recipient__c = contacts[0].Id, echosign_dev1__SignatureType__c = 'e-signature',
//                                                                Name = 'Combined Agreement 1'));
//        
//        agreements.add(new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Recipient__c = contacts[0].Id, echosign_dev1__SignatureType__c = 'e-signature',Name = 'Combined Agreement 2'));
//        
//        insert agreements;
//                                                                
//        //List to hold Attachment test records
//        List<Attachment> attachments = new List<Attachment>();
//        attachments.add(new Attachment(ParentId = agreements[0].Id, Name = 'Test Attachment for Parent 1', Body = Blob.valueOf('Test Data 1')));
//        attachments.add(new Attachment(ParentId = agreements[0].Id, Name = 'Test Attachment for Parent 2 - signed', Body = Blob.valueOf('Test Data 2')));
//        attachments.add(new Attachment(ParentId = agreements[0].Id, Name = 'Test Attachment for Parent 3', Body = Blob.valueOf('Test Data 3')));
//        attachments.add(new Attachment(ParentId = agreements[1].Id, Name = 'Test Attachment for Parent 4 - signed', Body = Blob.valueOf('Test Data 4')));
//        attachments.add(new Attachment(ParentId = agreements[1].Id, Name = 'Test Attachment for Parent 5 - signed', Body = Blob.valueOf('Test Data 5')));
//        attachments.add(new Attachment(ParentId = agreements[1].Id, Name = 'Test Attachment for Parent 6', Body = Blob.valueOf('Test Data 6')));
//        
//        //Test starts here
//        Test.startTest();
//        
//        //attachments created and inserted here
//        insert attachments;
//        
//        //query result of ContentVersion
//        List<ContentVersion> sfdcContents = [SELECT Id, OwnerId FROM ContentVersion WHERE Contact__c IN: contacts];
//        
//        //assert statement here
//        System.assertEquals(3, sfdcContents.size());
//        
//        //Test stops here
//        Test.stopTest();
//    }
}