/**
 *  Purpose         :   This scheduler is used to schedule Batch_AttachmentCleanupOnCases class.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   01/28/2014
 *
 *  Current Version :   V1.0
 *
 **/
global class Sched_Batch_AttachmentCleanupOnCases implements Schedulable{
    
    //execute method to execute the logic of batch processing
    global void execute(SchedulableContext ctx) {
        
        //Batch instance
        Batch_AttachmentCleanupOnCases batchJob = new Batch_AttachmentCleanupOnCases();
        
        //Batch executes here
        Database.executeBatch(batchJob, 200);
    }
}