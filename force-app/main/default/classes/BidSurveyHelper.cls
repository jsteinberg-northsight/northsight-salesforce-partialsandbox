public without sharing class BidSurveyHelper {
    //global variables
    public static Case receivedOrder;
    public static String survey;
    public static Map<string,List<Map<string,List<object>>>> bidSurveyMap;
    public static List<Bid__c> bidObjects;
    public static Map<String, ImageLinks__c> imageList;
    public static RecordType recType;
    
    //initialize method
    public static void initialize(Case workOrder, String surveyId, Map<string, Map<string, object>> outerAnswerMap){
        receivedOrder = workOrder;
        survey = surveyId;
        bidSurveyMap = new Map<string,List<Map<string,List<object>>>>();
        bidObjects = new List<Bid__c>();
        imageList = new Map<String, ImageLinks__c>();
        recType = [SELECT Id FROM RecordType WHERE Name = 'Bid from Mobile' AND sObjectType = 'Bid__c'];//select this record type for bid objects

        buildBidSurveyMap(outerAnswerMap);
    }
    
    //function for building bidSurveyMap
    private static void buildBidSurveyMap(Map<string, Map<string, object>> outerAnswerMap){
        for(string answerKey : outerAnswerMap.keySet()){//loop through the questions
            //check to see if we have one of these sfdc fields as part of the question
            if(answerKey.contains('Mobile_Bid_Type__c') || answerKey.contains('Bid_Description__c') || answerKey.contains('Bid_Cost__c')){
                String[] splitKeyArray = answerKey.split(':');//split the key to get the question Id prefix to use as a map key//String[] splitKeyArray = answerKey.split(':', 2)
                String prefixKey = splitKeyArray[0];
                String bidField = splitKeyArray[1];
                if(!bidSurveyMap.containsKey(prefixKey)){//check the bidSurveyMap for the key (the prefix to the question that was split)
                    List<Map<string,List<object>>> bidAnswers = new List<Map<string,List<object>>>();//create a new map of string to string
                    bidSurveyMap.put(prefixKey, bidAnswers);//put the key with the newMap into bidSurveyMap
                }
                Map<string,List<object>> bidAnswerMap = new Map<string,List<object>>();
                bidAnswerMap.put(bidField, (list<object>)outerAnswerMap.get(answerKey).get('answer'));
                //get the newMap and add the sfdc field as the key and the answer as the value
                bidSurveyMap.get(prefixKey).add(bidAnswerMap);
            }
            
            if(answerKey.contains('photo')){
                if(outerAnswerMap.get(answerKey).get('picIds') != null){//check if there are any picture IDs on the current question
                    for(object objUUID : (list<object>)outerAnswerMap.get(answerKey).get('picIds')){//for each picture, create an image link related to the question for this work order
                        addImageLinkForSurveyQuestion(answerKey, string.valueOf(objUUID));
                    }
                }
            }
        }
        //after we have finished looping
        createBidObjects();//call this function to create the bid objects
    }
    
    //this method will create image links and add them to the imageList
    public static void addImageLinkForSurveyQuestion(string questionId, string uuId){
        ImageLinks__c newImageLink = new ImageLinks__c(
            CaseId__c = receivedOrder.Id,
            Survey_Id__c = survey,
            Survey_Question_Id__c = questionId,
            Pruvan_uuid__c = uuId
        );
        imageList.put(uuId, newImageLink);
    }
    
    //function for creating new bid objects
    private static void createBidObjects(){
        if(system.test.isrunningtest()) {
            Case obj = new Case();
            if(receivedOrder == null) { obj.addError('Order is null');}
            if(survey == null){obj.addError('Survey is null');}
            if(bidSurveyMap == null || bidSurveyMap.size() == 0){obj.addError('bidSurveyMap is null/empty');}
        }
        if(bidSurveyMap != null && bidSurveyMap.size() > 0){
            for(string key : bidSurveyMap.keySet()){//loop over the bidSurveyMap using the keys
                //each key in the bidSurveyMap represents one bid object
                //so number of new bid objects == number of keys in BidSurveyMap
                //create new bid object
                Bid__c newBid = new Bid__c();
                    newBid.RecordTypeId = recType.Id;
                    newBid.Work_Order__c = receivedOrder.Id;
                List<object> bidData = (list<object>)bidSurveyMap.get(key);//get the map of string to list at the key
                for(Object o : bidData){//loop over the bidData map using the keys
                    Map<string,List<object>> bidAnswerMap = (Map<string,List<object>>)o;
                    for(string dataKey : bidAnswerMap.keySet()){
                        List<object> answerList = bidAnswerMap.get(dataKey);
                        //set the new bid object fields depending on the dataKey
                        if(dataKey.contains('Mobile_Bid_Type__c')){
                            newBid.Mobile_Bid_Type__c = string.valueOf(answerList[0]);
                        } else if(dataKey.contains('Bid_Description__c')){
                            newBid.Bid_Description__c = string.valueOf(answerList[0]);
                        } else {
                            newBid.Bid_Cost__c = integer.valueOf(answerList[0]);
                        }
                    }
                }
                bidObjects.add(newBid);//add the new bid object to bidObjects
            }
        }
    }
    
    //function for inserting the new bid objects
    public static void finalize(){
        if(bidObjects != null && bidObjects.size() > 0){
            receivedOrder.Bid_Survey_Received__c = true;
            receivedOrder.Bid_Survey_Received_Datetime__c = Datetime.now();
            
            insert bidObjects;//create the bid objects
            //upsert imageList.values() Pruvan_uuid__c;//upset the image links
        }
        if(imageList != null && imageList.size() > 0){
            upsert imageList.values() Pruvan_uuid__c;//upset the image links
        }
    }
}