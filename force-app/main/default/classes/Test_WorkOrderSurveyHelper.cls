@isTest(SeeAllData=false)
public with sharing class Test_WorkOrderSurveyHelper {
//    //Globals
//    //****************************************************************
//    private static List<RecordType> recordTypes;
//    private static Account newAccount;
//    private static Work_Code__c newWorkCode;
//    private static Work_Code_Mapping__c newWorkCodeMapping;
//    private static Case newWorkOrder;
//    private static Work_Survey__c[] newWorkSurvey = new Work_Survey__c[]{};
//    private static Work_Code_Survey__c[] newWorkCodeSurvey = new Work_Code_Survey__c[]{};
//    //****************************************************************
//    
//    //PRE-TEST SETUP
//    //***********************************************************************************************
//    private static void generateTestData() {
//        //retrieve record types of account and case objects
//        recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: AssignmentMapConstants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                        OR DeveloperName = 'Client') AND (SobjectType =: AssignmentMapConstants.CASE_SOBJECTTYPE 
//                        OR SobjectType =: AssignmentMapConstants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        //Assert statement
//        System.assertEquals(2, recordTypes.size());
//        
//        //create new account
//        newAccount = new Account(
//            Name = 'Test Account 1',
//            Phone = '1234567890',
//            RecordTypeId = recordTypes[0].Id
//        );
//        insert newAccount;
//        
//        //create new work code
//        newWorkCode = new Work_Code__c(
//            Name = 'GCREO01',
//            Work_Code_Description__c = 'Grass Cut REO',
//            Review_Needed__c =true,
//            Work_Ordered__c = 'Grass Cut', 
//            Department__c='Maintenance'
//        );
//        insert newWorkCode;
//        
//        //create new work code mapping
//        newWorkCodeMapping = new Work_Code_Mapping__c(
//            City__c = 'Dallas', 
//            Client__c = 'SG', 
//            Client_Special_1__c = 'Test Client1', 
//            Client_Special_2__c = 'Test Client2', 
//            Client_Special_3__c = 'Test Client3', 
//            Customer__c = 'Test Customer', 
//            Loan_Type__c = 'REO', 
//            Mapping_Priority__c = 300, 
//            State__c = 'CA', 
//            Vendor_Code__c = '123', 
//            Work_Code__c = newWorkCode.Id, 
//            Work_Ordered_Text__c = 'Test WO Data', 
//            Zip_Code__c = '12345'
//        );
//        insert newWorkCodeMapping;
//        
//        //create a new work survey
//        newWorkSurvey = new Work_Survey__c[]{new Work_Survey__c(
//            Comments__c = 'Work Survey Comments1',
//            Description__c = 'Work Survey Description1',
//            Survey_External_ID__c = 'Work Survey External ID1',
//            Service_Name__c = 'Service Name1'
//        ),new Work_Survey__c(
//            Comments__c = 'Work Survey Comments2',
//            Description__c = 'Work Survey Description2',
//            Survey_External_ID__c = 'Work Survey External ID2',
//            Service_Name__c = 'Service Name2'
//        )
//        };
//        insert newWorkSurvey;
//        
//        //create a new work code survey
//        newWorkCodeSurvey = new Work_Code_Survey__c[]{
//        	new Work_Code_Survey__c(
//            Comments__c = 'Work Code Survey Comments 1',
//            Make_Survey_Required__c = true,
//            Survey_Number__c = newWorkSurvey[0].Id,
//            Work_Code_Name__c = newWorkCode.Id
//        	),
//        		new Work_Code_Survey__c(
//            Comments__c = 'Work Code Survey Comments 2',
//            Make_Survey_Required__c = true,
//            Survey_Number__c = newWorkSurvey[1].Id,
//            Work_Code_Name__c = newWorkCode.Id
//        	)};
//        insert newWorkCodeSurvey;
//        
//        //create new work order
//        newWorkOrder = new Case(
//            RecordTypeId = recordTypes[1].Id, 
//            AccountId = newAccount.Id,
//            Client_Name__c = newAccount.Id, 
//            Due_Date__c = Date.today(), 
//            Status = AssignmentMapConstants.CASE_STATUS_OPEN, 
//            Approval_Status__c = 'Rejected',
//            Client__c = AssignmentMapConstants.CASE_CLIENT_SG, 
//            Work_Order_Type__c = 'Routine', 
//            Department__c = 'Test', 
//            Work_Ordered__c = 'Grass Recut', 
//            Assignment_Program__c = 'Mannual', 
//            City__c = 'Dallas', 
//            Client_Special_1__c = 'Test Client1', 
//            Client_Special_2__c = 'Test Client2', 
//            Client_Special_3__c = 'Test Client3', 
//            Customer__c = 'Test Customer', 
//            Loan_Type__c = 'REO', 
//            State__c = 'CA', 
//            Vendor_Code__c = '123', 
//            Work_Ordered_Text__c = 'Test WO Data', 
//            Zip_Code__c = '12345'
//        );
//        insert newWorkOrder;
//    }
//    
//    //TEST METHODS
//    //***********************************************************************************************
//    
//    //UNIT TEST
//    public static testmethod void workOrderSurveyHelper_UnitTest() {
//        system.test.startTest();
//        
//        generateTestData();
//        
//        List<Work_Order_Survey__c> workOrderSurveys = [SELECT Comments__c,
//                                                              Make_Survey_Required__c,
//                                                              Survey_Comments__c,
//                                                              Survey_Description__c,
//                                                              Survey_External_ID__c,
//                                                              Survey_Number__c,
//                                                              Work_Code_Survey_Entry__c,
//                                                              Work_Order_Number__c
//                                                       FROM Work_Order_Survey__c];
//        system.assertEquals(2, workOrderSurveys.size());
//        for(Work_Order_Survey__c woSurvey : workOrderSurveys) {
//            //system.assertEquals('Work Code Survey Comments', woSurvey.Comments__c);
//            //system.assertEquals(true, woSurvey.Make_Survey_Required__c);
//            //system.assertEquals('Work Survey Comments', woSurvey.Survey_Comments__c);
//            //system.assertEquals('Work Survey Description', woSurvey.Survey_Description__c);
//            //system.assertEquals('Work Survey External ID', woSurvey.Survey_External_ID__c);
//            //system.assertEquals(newWorkSurvey.Id, woSurvey.Survey_Number__c);
//            //system.assertEquals(newWorkCodeSurvey.Id, woSurvey.Work_Code_Survey_Entry__c);
//            //system.assertEquals(newWorkOrder.Id, woSurvey.Work_Order_Number__c);
//        }
//        
//        system.test.stopTest();
//    }
}