@isTest(seeAllData=false)
private class AssignmentMapSearchDaemonTest {
///**
// *  Purpose         :   This is used for testing and covering AssignmentMapSearchDaemon.
// *
// *  Created By      :   
// *
// *  Created Date    :   03/11/2014
// *
// *	Current Version	:	V1.5
// *
// *	Revision Log	:	V1.0 - Created
// *						V1.2 - Modified - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes - Enhancement of functionality on OAM project.
// *						V1.3 - Modified - Padmesh Soni (05/08/2014) - 140503 OAM Date Widgets (1) - Enhancement of functionality on OAM project.
// *											Changes are:  
// *												1. OAM-40: Better Date Filter UI (Full specifications on all date filters.)
// *						V1.4 - Padmesh Soni (06/02/2014) - OAM Tasks 05/28/2014
// *						V1.5 - Modified - Padmesh Soni (06/19/2014) - Support Case Record Type
// * 			                                 Changes are: New condition and query filter added of new RecordType excluding of Case object to process.
// *
// *	Coverage		:	96% - V1.0
// *						96% - V1.2
// *						93% - V1.3
// *						97% - V1.4 
// *						97% - V1.5 
// *
// **/
// 	
// 	//class property
//	private static List<Account> accounts;
//	private static List<Contact> contacts;
//	
//	private static void generateTestData(integer countData){
//		
//		//Loop through countData time
//		for(integer i = 1; i <= countData; i++) {
//		
//			/******** PRE-TEST SETUP create a new account *********/
//			accounts.add(new Account(Name = 'Tester Account' + i));
//		}
//		
//		insert accounts;
//		
//		//Loop through countData time
//		for(integer i = 1; i <= countData; i++) {
//		
//			//--------------------------------------------------------
//	   		//PRE-TEST SETUP
//			//create a new contact
//			contacts.add(new Contact(LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active'));
//		}	
//		
//		insert contacts;
//	}
//	
//    static testMethod void myUnitTest() {
//    	       	integer vendorsPerZone = 1;//# of vendors to create per zone
//		integer casesPerZone = 1;//# of cases to create per zone per work order type
//		
//		OrderAssignmentSystemUtilityClass.vendorPlacementWrapper[] vendors = OrderAssignmentSystemUtilityClass.setupVendorsAndZones(vendorsPerZone);//creates zones with vendors
//		
//		List<Case> cases = OrderAssignmentSystemUtilityClass.genStandardCaseSet(casesPerZone);//creates a standard set of cases
//		insert cases;
//		PageReference p = Page.AssignmentMapSearchJSON;
//
//		
//		Test.setCurrentPage(p);
//		Test.startTest();
//		ApexPages.currentPage().getParameters().put('minLat','-90');
//		ApexPages.currentPage().getParameters().put('maxLat','90');
//		ApexPages.currentPage().getParameters().put('minLon','-180');
//		ApexPages.currentPage().getParameters().put('maxLon','180');
//		ApexPages.currentPage().getParameters().put('maxLon','180');
//		ApexPages.currentPage().getParameters().put(AssignmentMapConstants.URL_PARAM_WORKORDERTYPES,'');
//    	AssignmentMapSearchDaemon sd = new AssignmentMapSearchDaemon();
//    	sd.doSearch();
//    	Test.stopTest();
//    	system.debug(sd.outputJSON);
//    	
//    }
//    
//    //Test method is used to test functionality of doSearch method
//    static testMethod void testDoSearch() {
//    	
//    	//initialize variables
//    	accounts = new List<Account>();
//		contacts = new List<Contact>();
//		
//		generateTestData(1);
//		
//		//Query record of Record Type of Account and Case objects
//    	//Padmesh Soni (06/18/2014) - Support Case Record Type
//		//Query one more record type of Case Object "SUPPORT"    
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client' 
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(3, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accountsOnOrders = new List<Account>();
//    	accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accountsOnOrders;
//		
//		List<Geocode_Cache__c> properties = new List<Geocode_Cache__c>();
//		properties.add(new Geocode_Cache__c(Active__c = true, Location__Latitude__s = 30.09567, Location__Longitude__s = -23.4532));
//		properties.add(new Geocode_Cache__c(Active__c = true, Location__Latitude__s = 12.09567, Location__Longitude__s = -45.4532));
//		properties.add(new Geocode_Cache__c(Active__c = true, Location__Latitude__s = 50.09567, Location__Longitude__s = -10.4532));
//		
//		//insert Property here
//		insert properties;
//		
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	
//    	for(integer i = 0; i < 2; i++) {
//    		
//    		workOrders.add(new Case(Geocode_Cache__c = properties[0].Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//	    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//	    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX',
//	    								Scheduled_Date_Override__c = Date.today(), Work_Ordered__c = 'Grass Recut'));
//	    	workOrders.add(new Case(Geocode_Cache__c = properties[0].Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//	    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//	    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX',
//	    								Scheduled_Date_Override__c = Date.today().addDays(-6), Work_Ordered__c = 'Grass Recut'));
//	    	workOrders.add(new Case(Geocode_Cache__c = properties[0].Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//	    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//	    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', 
//	    								State__c = 'LA', Work_Ordered__c = 'Grass Recut'));
//	    	workOrders.add(new Case(Geocode_Cache__c = properties[0].Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//	    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//	    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'AR',
//	    								Scheduled_Date_Override__c = Date.today().addDays(8), Work_Ordered__c = 'Grass Initial Cut'));
//	    }
//    	
//    	for(integer i = 0; i < 3; i++) {
//    		
//    		workOrders.add(new Case(Geocode_Cache__c = properties[1].Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//	    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//	    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', 
//	    								State__c = 'TX', Work_Ordered__c = 'Grass Initial Cut', Department__c = 'Test-Department'));
//	    	workOrders.add(new Case(Geocode_Cache__c = properties[1].Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//	    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'In Process',
//	    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, State__c = 'TX'));
//	    	workOrders.add(new Case(RecordTypeId = recordTypes[2].Id, AccountId = accountsOnOrders[0].Id, 
//	    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'In Process',
//	    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, State__c = 'TX'));
//	    }
//	    
//	    for(integer i = 0; i < 2; i++) {
//    		
//    		workOrders.add(new Case(Geocode_Cache__c = properties[2].Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//	    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//	    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX'));
//	    	workOrders.add(new Case(Geocode_Cache__c = properties[2].Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//	    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//	    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'LA', Assignment_Program__c = 'Manual'));
//	    	workOrders.add(new Case(Geocode_Cache__c = properties[2].Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//	    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//	    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', 
//	    								State__c = 'AR', Department__c = 'Test-Department', Assignment_Program__c = 'OrderSelect'));
//	    }
//	    
//    	//insert case records
//    	insert workOrders;
//		
//		//Initialize page reference of AssignmentMapSearchJSON page
//		PageReference p = Page.AssignmentMapSearchJSON;
//		
//		//Test start here
//		Test.startTest();
//		
//		ApexPages.currentPage().getParameters().put('minLat','30');
//		ApexPages.currentPage().getParameters().put('maxLat','30');
//		ApexPages.currentPage().getParameters().put('minLon','-46');
//		ApexPages.currentPage().getParameters().put('maxLon','-40');
//		ApexPages.currentPage().getParameters().put('filterClients',AssignmentMapConstants.CASE_CLIENT_NOT_SG + ';' + AssignmentMapConstants.CASE_CLIENT_SG);
//		ApexPages.currentPage().getParameters().put('workOrderTypes', 'Routine');
//		ApexPages.currentPage().getParameters().put('stateParam', 'TX');
//		ApexPages.currentPage().getParameters().put('department', 'Test-Department');
//		ApexPages.currentPage().getParameters().put(AssignmentMapConstants.URL_PARAM_FILTERASSIGNPROG, 'Manual');
//		ApexPages.currentPage().getParameters().put(AssignmentMapConstants.URL_PARAM_DEPARTMENT, 'OrderSelect;Manual');
//		ApexPages.currentPage().getParameters().put(AssignmentMapConstants.URL_PARAM_STOP_COLOR, 'Due Date');
//		
//		//OAM-48: Self Assigned filter - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes
//		//Adding url paramter
//		ApexPages.currentPage().getParameters().put(AssignmentMapConstants.URL_PARAM_FILTERSELFASSIGN, 'false');
//		
//    	AssignmentMapSearchDaemon aMSD = new AssignmentMapSearchDaemon();
//    	aMSD.doSearch();
//    	
//    	//assert statement
//    	System.assert(!String.isBlank(aMSD.outputJSON));
//    	
//    	p = Page.AssignmentMapSearchJSON;
//    	
//    	ApexPages.currentPage().getParameters().put('minLat','10');
//		ApexPages.currentPage().getParameters().put('maxLat','50');
//		ApexPages.currentPage().getParameters().put('minLon','-46');
//		ApexPages.currentPage().getParameters().put('maxLon','-10');
//		ApexPages.currentPage().getParameters().put('wrkOrderedType', 'Grass Initial Cut');
//		ApexPages.currentPage().getParameters().put(AssignmentMapConstants.URL_PARAM_STOP_COLOR, 'Schedule Date');
//		
//		//OAM-48: Self Assigned filter - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes
//		//Adding url paramter
//		ApexPages.currentPage().getParameters().put(AssignmentMapConstants.URL_PARAM_FILTERSELFASSIGN, 'false');
//		
//    	aMSD = new AssignmentMapSearchDaemon();
//    	aMSD.doSearch();
//    	
//    	//assert statement
//    	System.assert(!String.isBlank(aMSD.outputJSON));
//    	
//    	integer dateNum = Date.today().day();
//    	integer monthNum = Date.today().month();
//    	integer yearNum = Date.today().year();
//    	
//    	String suffix = 'T00:00:00-05:00';
//    	String presentDTM = DateTime.now().format();
//    	String presentDTM2Day = DateTime.now().addDays(2).format();
//    	
//    	//Setting current page URL parameters
//    	ApexPages.currentPage().getParameters().put('minLat','10');
//		ApexPages.currentPage().getParameters().put('maxLat','50');
//		ApexPages.currentPage().getParameters().put('minLon','-46');
//		ApexPages.currentPage().getParameters().put('maxLon','-10');
//		ApexPages.currentPage().getParameters().put('workOrderTypes', ';');
//		ApexPages.currentPage().getParameters().put('dueStartDate', monthNum +'/'+ dateNum +'/'+ (yearNum-1));
//		ApexPages.currentPage().getParameters().put('dueStopDate', monthNum +'/'+ dateNum +'/'+ (yearNum+1));
//		ApexPages.currentPage().getParameters().put('scheduleStartDate', (monthNum+1) +'/'+ dateNum +'/'+ (yearNum-1));
//		ApexPages.currentPage().getParameters().put('scheduleStopDate', monthNum +'/'+ (dateNum+1) +'/'+ yearNum);
//		ApexPages.currentPage().getParameters().put('assignedStartDate', '2/3/2014 2:44 AM');
//		ApexPages.currentPage().getParameters().put('assignedStopDate', '6/2/2014 2:44 AM');
//		ApexPages.currentPage().getParameters().put('createdStartDate', presentDTM);
//		ApexPages.currentPage().getParameters().put('createdStopDate', presentDTM2Day);
//		
//		//new instance of controller
//    	aMSD = new AssignmentMapSearchDaemon();
//    	aMSD.doSearch();
//    	
//    	//assert statement
//    	System.assert(!String.isBlank(aMSD.outputJSON));
//    	
//    	//OAM-40: Better Date Filter UI - Padmesh Soni (05/08/2014) - 140503 OAM Date Widgets (1)
//    	//Line of code added from 248 - 317 
//		//Setting current page URL parameters
//    	ApexPages.currentPage().getParameters().put('minLat','10');
//		ApexPages.currentPage().getParameters().put('maxLat','50');
//		ApexPages.currentPage().getParameters().put('minLon','-46');
//		ApexPages.currentPage().getParameters().put('maxLon','-10');
//		ApexPages.currentPage().getParameters().put('dueStartDate', AssignmentMapConstants.TODAYS);
//		ApexPages.currentPage().getParameters().put('dueStopDate', '');
//		ApexPages.currentPage().getParameters().put('scheduleStartDate', AssignmentMapConstants.TODAYS);
//		ApexPages.currentPage().getParameters().put('scheduleStopDate', '');
//		ApexPages.currentPage().getParameters().put('assignedStartDate', AssignmentMapConstants.TODAYS);
//		ApexPages.currentPage().getParameters().put('assignedStopDate', '');
//		ApexPages.currentPage().getParameters().put('createdStartDate', AssignmentMapConstants.TODAYS);
//		ApexPages.currentPage().getParameters().put('createdStopDate', '');
//		
//		//new instance of controller   
//    	aMSD = new AssignmentMapSearchDaemon();
//    	aMSD.doSearch();
//    	
//    	//assert statement
//    	System.assert(!String.isBlank(aMSD.outputJSON));
//    	
//    	//Setting current page URL parameters
//    	ApexPages.currentPage().getParameters().put('minLat','10');
//		ApexPages.currentPage().getParameters().put('maxLat','50');
//		ApexPages.currentPage().getParameters().put('minLon','-46');
//		ApexPages.currentPage().getParameters().put('maxLon','-10');
//		ApexPages.currentPage().getParameters().put('dueStartDate', AssignmentMapConstants.TODAYLATE);
//		ApexPages.currentPage().getParameters().put('dueStopDate', '');
//		ApexPages.currentPage().getParameters().put('scheduleStartDate', AssignmentMapConstants.TODAYLATE);
//		ApexPages.currentPage().getParameters().put('scheduleStopDate', '');
//		
//		//new instance of controller
//    	aMSD = new AssignmentMapSearchDaemon();
//    	aMSD.doSearch();
//    	
//    	//assert statement
//    	System.assert(!String.isBlank(aMSD.outputJSON));
//    	
//    	//Setting current page URL parameters
//    	ApexPages.currentPage().getParameters().put('minLat','10');
//		ApexPages.currentPage().getParameters().put('maxLat','50');
//		ApexPages.currentPage().getParameters().put('minLon','-46');
//		ApexPages.currentPage().getParameters().put('maxLon','-10');
//		ApexPages.currentPage().getParameters().put('dueStartDate', AssignmentMapConstants.LATE);
//		ApexPages.currentPage().getParameters().put('dueStopDate', '');
//		ApexPages.currentPage().getParameters().put('scheduleStartDate', AssignmentMapConstants.LATE);
//		ApexPages.currentPage().getParameters().put('scheduleStopDate', '');
//		
//		//new instance of controller
//    	aMSD = new AssignmentMapSearchDaemon();
//    	aMSD.doSearch();
//    	
//    	//assert statement
//    	System.assert(!String.isBlank(aMSD.outputJSON));
//    	
//    	//Setting current page URL parameters
//    	ApexPages.currentPage().getParameters().put('minLat','10');
//		ApexPages.currentPage().getParameters().put('maxLat','50');
//		ApexPages.currentPage().getParameters().put('minLon','-46');
//		ApexPages.currentPage().getParameters().put('maxLon','-10');
//		ApexPages.currentPage().getParameters().put('dueStartDate', AssignmentMapConstants.LAST2DAYS);
//		ApexPages.currentPage().getParameters().put('dueStopDate', '');
//		ApexPages.currentPage().getParameters().put('scheduleStartDate', AssignmentMapConstants.CURRENTSRVC);
//		ApexPages.currentPage().getParameters().put('scheduleStopDate', '');
//		
//		//new instance of controller
//    	aMSD = new AssignmentMapSearchDaemon();
//    	aMSD.doSearch();
//    	
//    	//assert statement
//    	System.assert(!String.isBlank(aMSD.outputJSON));
//    	
//    	try {
//    		
//	    	//Setting current page URL parameters
//	    	ApexPages.currentPage().getParameters().put('minLat','abc');
//	    	ApexPages.currentPage().getParameters().put('assignedStopDate', '23 ');
//		
//	    	//new instance of controller
//	    	aMSD = new AssignmentMapSearchDaemon();
//	    	aMSD.doSearch();
//    	} catch(Exception e) {
//    		
//    		System.assert(e.getMessage()!= null);
//    	}
//    	
//    	try {
//    		
//	    	//Setting current page URL parameters
//	    	ApexPages.currentPage().getParameters().put('maxLat','abc');
//		
//	    	//new instance of controller
//	    	aMSD = new AssignmentMapSearchDaemon();
//	    	aMSD.doSearch();
//    	} catch(Exception e) {
//    		
//    		System.assert(e.getMessage()!= null);
//    	}
//    	
//		try {
//    		
//	    	//Setting current page URL parameters
//	    	ApexPages.currentPage().getParameters().put('minLon','cdf');
//		
//	    	//new instance of controller
//	    	aMSD = new AssignmentMapSearchDaemon();
//	    	aMSD.doSearch();
//    	} catch(Exception e) {
//    		
//    		System.assert(e.getMessage()!= null);
//    	}
//    	
//		try {
//    		
//	    	//Setting current page URL parameters
//	    	ApexPages.currentPage().getParameters().put('maxLon','cef');
//		
//	    	//new instance of controller
//	    	aMSD = new AssignmentMapSearchDaemon();
//	    	aMSD.doSearch();
//    	} catch(Exception e) {
//    		
//    		System.assert(e.getMessage()!= null);
//    	}
//    	
//    	try {
//    		
//	    	//Setting current page URL parameters
//	    	ApexPages.currentPage().getParameters().put(AssignmentMapConstants.URL_PARAM_TOGGLESTATE, 'true');
//	    	ApexPages.currentPage().getParameters().put(AssignmentMapConstants.URL_PARAM_STATEPARAM, null);
//		
//	    	//new instance of controller
//	    	aMSD = new AssignmentMapSearchDaemon();
//	    	aMSD.doSearch();
//    	} catch(Exception e) {
//    		
//    		System.assert(e.getMessage()!= null);
//    	}
//    	
//    	try {
//    		
//	    	//Setting current page URL parameters
//	    	ApexPages.currentPage().getParameters().put('filterClients', '345678');
//		
//	    	//new instance of controller
//	    	aMSD = new AssignmentMapSearchDaemon();
//	    	aMSD.doSearch();
//    	} catch(Exception e) {
//    		
//    		System.assert(e.getMessage()!= null);
//    	}
//    	
//    	try {
//    		
//	    	//Setting current page URL parameters
//	    	ApexPages.currentPage().getParameters().put('workOrderTypes', '345678');
//		
//	    	//new instance of controller
//	    	aMSD = new AssignmentMapSearchDaemon();
//	    	aMSD.doSearch();
//    	} catch(Exception e) {
//    		
//    		System.assert(e.getMessage()!= null);
//    	}
//    	
//    	try {
//    		
//	    	//Setting current page URL parameters
//	    	ApexPages.currentPage().getParameters().put('workOrderTypes', '345678');
//		
//	    	//new instance of controller
//	    	aMSD = new AssignmentMapSearchDaemon();
//	    	aMSD.doSearch();
//    	} catch(Exception e) {
//    		
//    		System.assert(e.getMessage()!= null);
//    	}
//    	
//    	try {
//    		
//	    	//Setting current page URL parameters
//	    	ApexPages.currentPage().getParameters().put('department', '345678');
//		
//	    	//new instance of controller
//	    	aMSD = new AssignmentMapSearchDaemon();
//	    	aMSD.doSearch();
//    	} catch(Exception e) {
//    		
//    		System.assert(e.getMessage()!= null);
//    	}
//    	
//	   	try {
//    		
//	    	//Setting current page URL parameters
//	    	ApexPages.currentPage().getParameters().put('UA', null);
//		
//	    	//new instance of controller
//	    	aMSD = new AssignmentMapSearchDaemon();
//	    	aMSD.doSearch();
//    	} catch(Exception e) {
//    		
//    		System.assert(e.getMessage()!= null);
//    	}
//    	
//    	try {
//    		
//	    	//Setting current page URL parameters
//	    	ApexPages.currentPage().getParameters().put('toggleState','true');
//	    	ApexPages.currentPage().getParameters().put('stateParam','TX');
//	    	
//	    	//OAM-48: Self Assigned filter - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes
//			//Adding url paramter
//			ApexPages.currentPage().getParameters().put(AssignmentMapConstants.URL_PARAM_FILTERSELFASSIGN, null);
//			
//		
//	    	//new instance of controller
//	    	aMSD = new AssignmentMapSearchDaemon();
//	    	aMSD.doSearch();
//    	} catch(Exception e) {
//    		
//    		System.assert(e.getMessage()!= null);
//    	}
//		
//    	//new instance of controller
//    	aMSD = new AssignmentMapSearchDaemon();
//    	
//    	//Setting current page URL parameters
//    	ApexPages.currentPage().getParameters().put('minLat','10');
//		ApexPages.currentPage().getParameters().put('maxLat','50');
//		ApexPages.currentPage().getParameters().put('minLon','-46');
//		ApexPages.currentPage().getParameters().put('maxLon','-10');
//		ApexPages.currentPage().getParameters().put('UA','yes');
//		
//		//Call doSearch method
//		aMSD.doSearch();
//    	System.assert(aMSD.outputJSON != null);
//    	
//    	//new instance of controller
//    	aMSD = new AssignmentMapSearchDaemon();
//    	
//    	//Setting current page URL parameters
//    	ApexPages.currentPage().getParameters().put('minLat','10');
//		ApexPages.currentPage().getParameters().put('maxLat','50');
//		ApexPages.currentPage().getParameters().put('minLon','-46');
//		ApexPages.currentPage().getParameters().put('maxLon','-10');
//		ApexPages.currentPage().getParameters().put('UA','no');
//		
//		//Call doSearch method
//		aMSD.doSearch();
//    	System.assert(aMSD.outputJSON != null);
//    	
//    	//new instance of controller
//    	aMSD = new AssignmentMapSearchDaemon();
//    	
//    	//Setting current page URL parameters
//    	ApexPages.currentPage().getParameters().put('minLat','10');
//		ApexPages.currentPage().getParameters().put('maxLat','50');
//		ApexPages.currentPage().getParameters().put('minLon','-46');
//		ApexPages.currentPage().getParameters().put('maxLon','-10');
//		ApexPages.currentPage().getParameters().put('UA','only');
//		
//		//Call doSearch method
//		aMSD.doSearch();
//    	System.assert(aMSD.outputJSON != null);
//    	
//		//Test stops here 
//		Test.stopTest();
//    }
}