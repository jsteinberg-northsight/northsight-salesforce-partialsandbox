public without sharing class MassGeocodeWorkOrderPropertiesController {
	public WorkOrderWrapper[] workorders{get;set;}
	public integer workOrderCount{get;set;}
	public boolean autorun{get;set;}
	public string propertiesJson{get;set;}
	private Map<string,string> addressHashToCounty;
	private Map<id,Geocode_Cache__c> caseGeoCacheMap; //Map<case Id, Geocode_Cache__c> 
	private Map<string,Case[]> addrWOMap; //Map<address_Hash, case[]>
	private Map<Id, Geocode_Cache__c> propsToUpdate; //Map<propertyId, Geocode_Cache__c>
	private Geocode_Cache__c[] propsToInsert;
	private static string regexNumbers = '\\d+';
	private static string regexLetters = '[a-zA-Z]\\w+';
	private static string regexWhitespace = '^\\s';
	
	//Padmesh Soni (06/24/2014) - Support Case Record Type
	//Variable to hold list of RecordTypes of Case object
	List<RecordType> recordTypes;
	
	public MassGeocodeWorkOrderPropertiesController(){
		autorun=false;
		
		//Padmesh Soni (06/24/2014) - Support Case Record Type
		//Query record of Record Type of Case object
		recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
											
		init();
	}
	
	//initialize function
	public void init(){
		try{
			caseGeoCacheMap = new Map<id,Geocode_Cache__c>();
			addrWOMap = new Map<string,Case[]>();
			propsToUpdate = new Map<Id, Geocode_Cache__c>{};
			propsToInsert = new Geocode_Cache__c[]{};
			addressHashToCounty = new Map<string,string>();
			
			//Padmesh Soni (06/24/2014) - Support Case Record Type
	    	//New filter criteria added for RecordTypeId
			AggregateResult r = [select count(id) WOCount from Case where RecordTypeId NOT IN: recordTypes AND Serviceable__c = 'Yes' 
									and ((Geocode_Cache__r.Location__Latitude__s = 0 and Geocode_Cache__r.Location__Longitude__s = 0 
									and Geocode_Cache__r.Last_Geocoded__c = null) or geocode_cache__c = null)];
									
			workOrderCount = (integer)r.get('WOCount');
			
			//Padmesh Soni (06/24/2014) - Support Case Record Type
	    	//New filter criteria added for RecordTypeId
			Case[] cases = [select id, CaseNumber, Geocode_Cache__c, Street_Add_Concat__c, Street_Address__c, City__c, State__c, Zip_Code__c, County__c
								from Case where RecordTypeId NOT IN: recordTypes AND Serviceable__c = 'Yes' 
								and ((Geocode_Cache__r.Location__Latitude__s = 0 and Geocode_Cache__r.Location__Longitude__s = 0 
								and Geocode_Cache__r.Last_Geocoded__c = null) or geocode_cache__c = null) limit 200];
			
			workorders = new WorkOrderWrapper[]{};
			
			//create addrWOMap and set approriate property to each case
			preCheckGeocodeCache(cases);
			
			//create work order wrappers to display on page
			for(string addrKey : addrWOMap.KeySet()){
				for(Case c:addrWOMap.get(addrKey)){
					if(c.Geocode_Cache__c != null){
						WorkOrderWrapper w = new WorkOrderWrapper(c);
						w.addressHash = addrKey;
						workorders.add(w);
					}
				}
			}
		}
		catch(Exception e){
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.error, e.getMessage()+'*****'+e.getStackTraceString()));
		}
	}
	
	
	private void preCheckGeocodeCache(Case[] cases){
		try{	
			for(Case workorder : cases){
				string formattedAddr = GeoUtilities.formatAddress(workorder.Street_Address__c, workorder.City__c, workorder.State__c, workorder.Zip_Code__c);
				string addrKey = GeoUtilities.createAddressHash(formattedAddr);//original value before change: workorder.Street_Add_Concat__c
				
				if(!addrWOMap.containsKey(addrKey)){
					addrWOMap.put(addrKey, new Case[]{});	
				}
				addrWOMap.get(addrKey).add(workorder);
			}
				
			Geocode_Cache__c[] propertyList = [select id, 
													  Formatted_Street_Address__c, 
													  Formatted_City__c, 
													  Formatted_State__c, 
													  Formatted_Zip_Code__c, 
													  Street_Address__c, 
													  City__c, 
													  State__c, 
													  Zip_Code__c, 
													  Last_Geocoded__c, 
													  location__latitude__s, 
													  location__longitude__s, 
													  Geocode_Status__c, 
													  Partial_Match__c, 
													  Address_hash__c,
													  quarantined__c,
													  Flag__c,
													  Geo_System__c,
													  Geo_Timestamp__c
												from Geocode_Cache__c where Address_hash__c in : addrWOMap.KeySet()];
			
			for(Geocode_Cache__c property : propertyList){
				for(Case c : addrWOMap.get(property.Address_hash__c)){
					c.Geocode_Cache__c = property.id;
					caseGeoCacheMap.put(c.Id, property);
				}
			}
		}
		catch(Exception e){
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.error, e.getMessage()+'*****'+e.getStackTraceString()));
		}
	}
	
	
	public void saveGeocodeRecords(){
		try{
			if(propertiesJson != null && propertiesJson != '') {
				Map<string,object> payloadList = (map<string,object>)JSON.deserializeUntyped(propertiesJson);
				buildaddressHashToCountyMap(payloadList);
			}

			insertPropertiesForWorkOrdersWithNoPropertyLinked();
			
			updatePropertiesLinkedToGeocodedWorkOrders();
			
			savePrematchedGeocodeCache();
			
			init();
		}
		catch(Exception e){
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.error, e.getMessage()+'*****'+e.getStackTraceString()));
		}
	}
	
	private void buildaddressHashToCountyMap(Map<string,object> geocodedPropsMap) {
		List<object> propertiesMapList = (list<object>)geocodedPropsMap.get('properties');
		if(propertiesMapList != null && propertiesMapList.size() > 0) {
			for(object o : propertiesMapList) {
				Map<string,object> tempMap = (map<string,object>)o;
			
				string addrHash = GeoUtilities.createAddressHash((string)tempMap.get('address'));
				if(!addressHashToCounty.containsKey(addrHash)) {
					addressHashToCounty.put(addrHash, (string)tempMap.get('county'));
				}
			}
		}
	}
	
	private void insertPropertiesForWorkOrdersWithNoPropertyLinked(){
		try{
			//query properties
			Geocode_Cache__c[] propertyList = [select id, Last_Geocoded__c, location__latitude__s, location__longitude__s, Geocode_Status__c, Partial_Match__c, Address_hash__c, quarantined__c, Flag__c, Geo_System__c, Geo_Timestamp__c from Geocode_Cache__c where Address_hash__c in : addrWOMap.KeySet()];
			Map<string, Geocode_Cache__c> propMap = new Map<string, Geocode_Cache__c>{};
			for(Geocode_Cache__c g : propertyList){
				propMap.put(g.Address_hash__c, g);
			}
			
			for(WorkOrderWrapper wo : workOrders){
				//insert new property only if work order isn't linked to a property and the property doesn't exist
				if(!propMap.keySet().contains(wo.addressHash) && wo.wo.Geocode_Cache__c == null){
					boolean partial = true;
					string geocode_status = 'OK';
					if(wo.addressTypes.contains('street_address') || wo.addressTypes.contains('premise') || wo.addressTypes.contains('subpremise')) {
		    			partial = false;
		    		} else {
		    			partial = true;
		    			geocode_status = 'NOT_PRECISE';
		    		}
		    		string county = (addressHashToCounty != null && addressHashToCounty.containsKey(wo.addressHash)) ? addressHashToCounty.get(wo.addressHash) : wo.wo.County__c;
					propsToInsert.add(new Geocode_Cache__c(
						Last_Geocoded__c = Date.Today(), 
						Address_hash__c = wo.addressHash,
						location__latitude__s= wo.latitude.setScale(6),
						location__longitude__s =  wo.longitude.setScale(6),
						Formatted_Address__c = GeoUtilities.formatAddress(wo.wo.Street_Address__c,wo.wo.City__c,wo.wo.State__c,wo.wo.Zip_Code__c),
						Formatted_Street_Address__c = GeoUtilities.capitalizeAll(GeoUtilities.preformatAddress(wo.wo.Street_Address__c)),
						Formatted_City__c = wo.wo.City__c,
						Formatted_State__c = wo.wo.State__c,
						Formatted_Zip_Code__c = GeoUtilities.preformatZip(wo.wo.Zip_Code__c),
	    				Street_Address__c = wo.wo.Street_Address__c,
	    				City__c = wo.wo.City__c,
	    				State__c = wo.wo.State__c,
	    				Zip_Code__c = wo.wo.Zip_Code__c,
	    				Geocode_Status__c = geocode_status,
						Partial_Match__c = partial,
						Quarantined__c = partial,
						Geo_System__c = 'MassGeocodeWorkOrderPropertiesController.cls',
						Geo_Timestamp__c = Datetime.now(),
						
						//Code added - Padmesh Soni (12/12/2014) - CCP-15: Fill County on Property
						//County__c = wo.wo.County__c
						County__c = county
						)
					); 
				}	
			}
			
			if(propsToInsert != null && propsToInsert.size() > 0) {
				insert propsToInsert;
				
				for(Geocode_Cache__c geo : propsToInsert){
					for(Case c:addrWOMap.get(geo.Address_hash__c)){
						caseGeoCacheMap.put(c.id, geo);
					}
				}
			}
		} catch(Exception e) {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.error, e.getMessage()+'*****'+e.getStackTraceString()));
		}
	}
	
	private void updatePropertiesLinkedToGeocodedWorkOrders(){
		try{
			for(WorkOrderWrapper wo:workOrders){
				//get partial and geo status
				if(wo.latitude != 0 && wo.longitude != 0){
					boolean partial = true;
					string geocode_status = 'OK';
					if(wo.addressTypes.contains('street_address') || wo.addressTypes.contains('premise') || wo.addressTypes.contains('subpremise')) {
		    			partial = false;
		    		} else {
		    			partial = true;
		    			geocode_status = 'NOT_PRECISE';
		    		}
		    		
		    		//get the property linked to the workorder then set the geocoded fields on property
		    		for(string addrKey : addrWOMap.KeySet()){
			    		if(wo.addressHash == addrKey){
			    			
			    			//New condition added - Padmesh Soni - As per Debugging the Mass Geocoder assignment 
			    			if(!addrWOMap.containsKey(addrKey))
			    				continue;
			    			
			    			Case c = addrWOMap.get(addrKey)[0]; //get first case in list with matching addrkey to get the case's property
			    			
			    			//New condition added - Padmesh Soni - As per Debugging the Mass Geocoder assignment 
			    			if(!caseGeoCacheMap.containsKey(c.Id) && propsToUpdate.keySet().contains(caseGeoCacheMap.get(c.Id).Id))
			    				continue;
			    				
		    				Geocode_Cache__c prop = caseGeoCacheMap.get(c.Id);
		    					
	    					if(!propsToUpdate.keySet().contains(prop.Id)){
	    						prop.Last_Geocoded__c = Date.Today();
								prop.Address_hash__c = wo.addressHash;
								prop.location__latitude__s= wo.latitude.setScale(6);
								prop.location__longitude__s =  wo.longitude.setScale(6);
								prop.Geocode_Status__c = geocode_status;
								prop.Partial_Match__c = partial;
								prop.Quarantined__c = partial;
								prop.Geo_System__c = 'MassGeocodeWorkOrderPropertiesController.cls';
								prop.Geo_Timestamp__c = Datetime.now();
								
								//Code added - Padmesh Soni (12/12/2014) - CCP-15: Fill County on Property
								//prop.County__c = wo.wo.County__c;
								prop.County__c = (addressHashToCounty != null && addressHashToCounty.containsKey(wo.addressHash)) ? addressHashToCounty.get(wo.addressHash) : c.County__c;
								
								propsToUpdate.put(prop.Id, prop);
	    					}
		    				//propsToUpdate.put(prop.Id, prop);
			    		}
		    		}
				}	
			}
			
			if(propsToUpdate != null && propsToUpdate.size() > 0) {
				update propsToUpdate.values();
			}
		} catch(Exception e) {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.error, e.getMessage()+'*****'+e.getStackTraceString()));
		}
	}
	
	public void savePrematchedGeocodeCache(){
		try{
			List<Case> cases = new List<Case>();
			if(caseGeoCacheMap != null && caseGeoCacheMap.size() > 0) {
				for(Id caseId : caseGeoCacheMap.KeySet()){
					if(caseGeoCacheMap.get(caseId).Quarantined__c && caseGeoCacheMap.get(caseId).Flag__c){
						cases.add(new Case(Id=caseId, Approval_Status__c = 'Quarantined-Flagged', Geocode_Cache__c = caseGeoCacheMap.get(caseId).Id));
					}else if(caseGeoCacheMap.get(caseId).Quarantined__c){
						cases.add(new Case(Id=caseId, Approval_Status__c = 'Quarantined', Geocode_Cache__c = caseGeoCacheMap.get(caseId).Id));
					}
					else{
						cases.add(new Case(Id=caseId, Automatic_Reassignment__c = true,Geocode_Cache__c = caseGeoCacheMap.get(caseId).Id));
					} 
				}
				if(cases != null && cases.size() > 0) {
					database.update(cases,false);
					
					/*******************************************************************************************************************/
					//create a list for case IDs
					List<ID> caseIDs = new List<ID>();
					//add case IDs from ordersToUpdate into caseIDs
					for(Case cID : cases){
						caseIDs.add(cID.Id);
					}
					/*******************************************************************************************************************/
				}
			}
		}
		catch(Exception e){
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.error, e.getMessage()+'*****'+e.getStackTraceString()));
		}
	}
	
		public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}