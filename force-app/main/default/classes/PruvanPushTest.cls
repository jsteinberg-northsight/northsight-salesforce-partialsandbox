@isTest()
public without sharing class PruvanPushTest {
//	
//  	private static Case newCase;
//    private static Account newAccount;
//    private static string photoHelperPayload;
//    private static string surveyHelperPayload;
//    private static string surveyPayload;
//    
//    private static void generateTestCase() {
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new account
//        newAccount = new Account(
//            Name = 'Tester Account'
//        );
//        insert newAccount;
//    
//        //create a new contact
//        Contact newContact = new Contact(
//            LastName = 'Tester',
//            AccountId = newAccount.Id,
//            Pruvan_Username__c = 'NSBobTest',
//            MailingStreet = '30 Turnberry Dr',
//            MailingCity = 'La Place',
//            MailingState = 'LA',
//            MailingPostalCode = '70068',
//            OtherStreet = '30 Turnberry Dr',
//            OtherCity = 'La Place',
//            OtherState = 'LA',
//            OtherPostalCode = '70068',
//            Other_Address_Geocode__Latitude__s = 30.00,
//            Other_Address_Geocode__Longitude__s = -90.00
//        );
//        insert newContact;
//        
//        //Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//        //Fetch record type of Account Sobject
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client' 
//                                                    AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//        
//        //Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//        //Create a test instance for Account sobject
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account; 
//        
//        //create a new test case
//        newCase = new Case();
//        newCase.street_address__c = '1234 Street';
//        newCase.state__c = 'TX';
//        newCase.city__c = 'Austin';
//        newCase.zip_code__c = '78704';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        newCase.Client__c = 'NOT-SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'Grass Recut';
//        newCase.Due_Date__c = Date.today();
//        
//        //Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//        //New field populated
//        newCase.Client_Name__c = account.Id;
//        System.AssertEquals(0,OrderAssignmentHelper.triggerDepth);
//        insert newCase;
//        System.AssertEquals(0,OrderAssignmentHelper.triggerDepth);
//    }
//    public static testmethod void  testPush(){
//    	generateTestCase();
//    	//create a new contact
//        Contact newContact2 = new Contact(
//            LastName = 'Tester2',
//            AccountId = newAccount.Id,
//            Pruvan_Username__c = 'NSBobTest2',
//            MailingStreet = '30 Turnberry Dr',
//            MailingCity = 'La Place',
//            MailingState = 'LA',
//            MailingPostalCode = '70068',
//            OtherStreet = '30 Turnberry Dr',
//            OtherCity = 'La Place',
//            OtherState = 'LA',
//            OtherPostalCode = '70068',
//            Other_Address_Geocode__Latitude__s = 30.00,
//            Other_Address_Geocode__Longitude__s = -90.00
//        );
//        insert newContact2;
//    	
//    	system.test.startTest();
//		OrderAssignmentHelper.initialized = false;    	
//    	newCase.ContactId = newContact2.Id;
//    	newCase.Vendor_Instructions_Summary__c = 'testing testing';
//    	//orderAssignmentHelper.triggerDepth = 0;
//    	System.AssertEquals(0,OrderAssignmentHelper.triggerDepth);
//    	update newCase;
//    	System.AssertEquals(0,OrderAssignmentHelper.triggerDepth);
//    	System.AssertNotEquals(null,OrderAssignmentHelper.pruvanPushSet);
//    	System.AssertNotEquals(0,OrderAssignmentHelper.pruvanPushSet.size());
//		System.AssertNotEquals(0,OrderAssignmentHelper.pushCallCount);
//    	PruvanPushController.pushWorkOrdersToPruvan(new Set<Id>{newCase.Id});
//    	system.test.stopTest();
//    
//    } 
}