/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/
*/
public without sharing class PruvanPhotoHelper {
    //create a map variable for the request data
    private static Map<string,object> picturesRequest;
    private static Map<string,object> photoResponse;
    private static string responseJSON;
    private static Case workOrder;
    private static Database.Dmloptions dml;
    public static String uploaderVersion;
    public static String deviceId;
    public static String sdkVersion;
    public static String pruvanUser;
    public static String surveyID;
        
    public static void uploadPictures(string input){
        //System.Assert(false,'STOP ERROR');
        //system.debug('**********'+input+'**********');
        
        //set the dml var Dmloptions to allow for field truncation
            //this will be used to allow data to be truncated if it is too long before being inserted/updated
        dml = new Database.Dmloptions();
        dml.allowFieldTruncation = true;
        
        //parse input into a map that is generic data
        picturesRequest = (map<string,object>)JSON.deserializeUntyped(input);
        
        String logCaseNum = (string)picturesRequest.get('key2');
        /*Pruvan_UploadPictures_Log__c log = new Pruvan_UploadPictures_Log__c(
            Log_Data__c = input,
            Case_Number__c = logCaseNum
        );
        insert log;*/
        
        //instantiate the surveyResponse
        photoResponse = new Map<string,object>();
        
        //if the fileType is survey, then send the survey data with key2 to PruvanSurveyHelper.initialize(string, string)
        if(picturesRequest.get('fileType') == 'survey' && picturesRequest.containsKey('nsSurveyExtract')){
            //get the nsSurveyExtract data and cast it as a string
            Map<string,object> surveyDataObj = (map<string,object>)picturesRequest.get('nsSurveyExtract');
            String surveyData = JSON.serialize(surveyDataObj);
            //get key2 and cast it as a string
            object key2Obj = (object)picturesRequest.get('key2');
            String key2 = (string)key2Obj;
            
            Map<string, string>mobileAndUserInfo = new Map<string, string>();//make a new map of string, string for holding mobile info
            
            if(picturesRequest.containsKey('uploaderVersion')){//if there is an uploader version
                uploaderVersion = (string)picturesRequest.get('uploaderVersion');
            }
            if(uploaderVersion != null && uploaderVersion != ''){//if uploaderVersion not null && not blank
                mobileAndUserInfo.put('uploaderVersion', uploaderVersion.substring(7));
            } else {//initialize to 0
                mobileAndUserInfo.put('uploaderVersion', '0');
            }
            if(picturesRequest.containsKey('deviceId')){//if there is a device id
                deviceId = (string)picturesRequest.get('deviceId'); 
            }
            if(deviceId != null && deviceId != ''){//if deviceId not null && not blank
                if (deviceId.length() > 2) {
                	mobileAndUserInfo.put('deviceId', deviceId.substring(0, 3));                    
                }
                else {
                    mobileAndUserInfo.put('deviceId', deviceId);
                }
            } else {//initialize to 0
                mobileAndUserInfo.put('deviceId', '0');
            }
            if(picturesRequest.containsKey('sdkVersion')){//if there is an sdk version
                sdkVersion = (string)picturesRequest.get('sdkVersion'); 
            }
            if(sdkVersion != null && sdkVersion != ''){//if sdkVersion is not null && not blank
                mobileAndUserInfo.put('sdkVersion', sdkVersion);
            } else {//initialize to 0
                mobileAndUserInfo.put('sdkVersion', '0');
            }
            if(picturesRequest.containsKey('createdBySubUser')){//if there is a pruvan user
                pruvanUser = (string)picturesRequest.get('createdBySubUser');   
            } 
            if(pruvanUser != null && pruvanUser != ''){//if pruvanUser is not null && not blank
                mobileAndUserInfo.put('pruvanUser', pruvanUser);
            } else {//initialize to 'Could Not Find'
                mobileAndUserInfo.put('pruvanUser', 'Could Not Find');
            }
            
            photoResponse = PruvanSurveyHelper.initialize(surveyData, key2, mobileAndUserInfo);
            
            //photoResponse.put('status', true);
            //photoResponse.put('error', null);
        }
        else if (picturesRequest.get('fileType') == 'csr'){//check if the file type is csr
            /*
            send back a valid response to pruvan if file type was csr
            */
            photoResponse.put('status', true);
            photoResponse.put('error', null);
        }
        else{
            //try sending request data to the createImageLinks method and build out the surveyReponse
            try{
                //if the fileType is survey, then send the survey data with key2 to PruvanSurveyHelper.initialize(string, string)
                if(picturesRequest.get('fileType') == 'survey' && picturesRequest.containsKey('survey')){
                    //get the nsSurveyExtract data and cast it as a string
                    Map<string,object> surveyDataObj = (map<string,object>)picturesRequest.get('survey');
                    
                    //get the surveyTemplateID in the meta data and re-cast it as a string
					Map<string,object> nsSurveyMeta = (map<string, object>)surveyDataObj.get('meta');
					string surveyTemplateID = (string)nsSurveyMeta.get('surveyTemplateId');
					List<String> surveyTemplate = surveyTemplateID.split('::');
					surveyID = surveyTemplate[surveyTemplate.size()-1];

                    String surveyData = JSON.serialize(surveyDataObj);
                    //get key2 and cast it as a string
                    String key2 = (string)picturesRequest.get('key2');
                    
                    Map<string, string>mobileAndUserInfo = new Map<string, string>();//make a new map of string, string for holding mobile info
                    
                    if(picturesRequest.containsKey('uploaderVersion')){//if there is an uploader version
                        uploaderVersion = (string)picturesRequest.get('uploaderVersion');
                    }
                    if(uploaderVersion != null && uploaderVersion != ''){//if uploaderVersion not null && not blank
                        mobileAndUserInfo.put('uploaderVersion', uploaderVersion.substring(7));
                    } else {//initialize to 0
                        mobileAndUserInfo.put('uploaderVersion', '0');
                    }
                    if(picturesRequest.containsKey('deviceId')){//if there is a device id
                        deviceId = (string)picturesRequest.get('deviceId'); 
                    }
                    if(deviceId != null && deviceId != ''){//if deviceId not null && not blank
                        mobileAndUserInfo.put('deviceId', deviceId.substring(0, 3));
                    } else {//initialize to 0
                        mobileAndUserInfo.put('deviceId', '0');
                    }
                    if(picturesRequest.containsKey('sdkVersion')){//if there is an sdk version
                        sdkVersion = (string)picturesRequest.get('sdkVersion'); 
                    }
                    if(sdkVersion != null && sdkVersion != ''){//if sdkVersion is not null && not blank
                        mobileAndUserInfo.put('sdkVersion', sdkVersion);
                    } else {//initialize to 0
                        mobileAndUserInfo.put('sdkVersion', '0');
                    }
                    if(picturesRequest.containsKey('createdBySubUser')){//if there is a pruvan user
                        pruvanUser = (string)picturesRequest.get('createdBySubUser');   
                    } 
                    if(pruvanUser != null && pruvanUser != ''){//if pruvanUser is not null && not blank
                        mobileAndUserInfo.put('pruvanUser', pruvanUser);
                    } else {//initialize to 'Could Not Find'
                        mobileAndUserInfo.put('pruvanUser', 'Could Not Find');
                    }
                    
                    photoResponse = PruvanSurveyHelper.initialize(surveyData, key2, mobileAndUserInfo);
                }
                
                if(photoResponse.containsKey('status') && photoResponse.containsKey('error')){
                    if(photoResponse.get('status') != true && (photoResponse.get('error') != null || photoResponse.get('error') != '')){//if there was an error with saving the survey data
                        //return responseJSON = JSON.serialize(photoResponse);//return the response
                    }
                }
                
                createImageLinks(picturesRequest);
                photoResponse.put('status', true);
                photoResponse.put('error', null);
            }
            catch(Exception e){
                photoResponse.put('status', false);
                photoResponse.put('error', e.getMessage()+'*****'+e.getStackTraceString()+'*****'+input);
                
                if(system.Test.isRunningTest()){//will show errors/exceptions hidden from tests
                    throw e;
                }
            }
        }
            
        
        //serialize the surveyResponse and return as a JSON string
        //return responseJSON = JSON.serialize(photoResponse);
    }
    
    private static void createImageLinks(Map<string,object> requestData){
        //System.Assert(false,'STOP ERROR');
        //get the reference number from the meta and re-cast it as string
        string caseNum = (string)requestData.get('key2');
        
        //convert the gpsTimestamp into a standard Datetime
        string timeStampValue = (string)requestData.get('gpsTimestamp');
        integer secondsToAdd = (integer.valueOf(timeStampValue));
        Datetime formattedTimeStamp = datetime.newinstance(0);
        formattedTimeStamp = formattedTimeStamp.addSeconds(secondsToAdd);
        
        //convert the latitude and longitude into a compatible data type for the geolocation field type in salesforce
        string strLat = (string)requestData.get('gpsLatitude');
        string strLong = (string)requestData.get('gpsLongitude');
        Decimal decLat;
        Decimal decLong;
        if(strLat != null && strLat != ''){
            decLat = decimal.valueOf(strLat);
        }
        if(strLong != null && strLong != ''){
            decLong = decimal.valueOf(strLong);
        }
        
        //convert the gps accuracy into a compatible data type for the number field type in salesforce
        string strAccuracy = (string)requestData.get('gpsAccuracy');
        Decimal decAccuracy;
        if(strAccuracy != null && strAccuracy != ''){
            decAccuracy = decimal.valueOf(strAccuracy);
        }
        
        //create a new ImageLink__c object
        ImageLinks__c newImageLink = new ImageLinks__c();
        //set the dml option on the newImageLink
        newImageLink.setOptions(dml);
        
        //set the newImageLink fields with the POST data
        if((string)requestData.get('uuid') != null && (string)requestData.get('uuid') != ''){
            newImageLink.Pruvan_uuid__c = (string)requestData.get('uuid');
        }
        
        if((string)requestData.get('parentUuid') != null && (string)requestData.get('parentUuid') != ''){
            newImageLink.Pruvan_parentUuid__c = (string)requestData.get('parentUuid');
        }
        
        if((string)requestData.get('evidenceType') != null && (string)requestData.get('evidenceType') != ''){
            newImageLink.Pruvan_evidenceType__c = (string)requestData.get('evidenceType');
        }
        
        if((string)requestData.get('fileType') != null && (string)requestData.get('fileType') != ''){
            newImageLink.Pruvan_fileType__c = (string)requestData.get('fileType');
        }
        
        if(requestData.containsKey('pdfFilename')){//check to see if there is a pdfFilename field
            if((string)requestData.get('pdfFilename') != null && (string)requestData.get('pdfFilename') != ''){//check for null or blank values
                newImageLink.Original_fileName__c = (string)requestData.get('pdfFilename');//set the original filename to be the pdfFilename field value
            }
        }
        else if((string)requestData.get('fileName') != null && (string)requestData.get('fileName') != ''){
            
            newImageLink.Original_fileName__c = (string)requestData.get('fileName');
        }
        
        if(decLat != null && (decLat <= 90 && decLat >= -90)){
            Decimal scaledDecLat = decLat.setScale(6);
            newImageLink.GPS_Location__latitude__s = scaledDecLat;
        }
        else{
            newImageLink.GPS_Location__latitude__s = 0;
        }
        
        if(decLong != null && (decLong <= 180 && decLong >= -180)){
            Decimal scaledDecLong = decLong.setScale(6);
            newImageLink.GPS_Location__longitude__s = scaledDecLong;
        }
        else{
            newImageLink.GPS_Location__longitude__s = 0;
        }
        
        if(formattedTimeStamp != null && formattedTimeStamp != datetime.newInstance(0)){
            newImageLink.GPS_Timestamp__c = formattedTimeStamp;
        }
        
        if(decAccuracy != null && decAccuracy != 0){
            newImageLink.Pruvan_gpsAccuracy__c = decAccuracy;
        }
        
        if(requestData.containsKey('surveyPdfLink')){//check to see if there is a surveyPdfLink field
            if((string)requestData.get('surveyPdfLink') != null && (string)requestData.get('surveyPdfLink') != ''){//check for null or blank values
                newImageLink.ImageUrl__c = (string)requestData.get('surveyPdfLink');//set the image url to be the surveyPdfLink field value
            }
        }
        else if((string)requestData.get('imageLink') != null && (string)requestData.get('imageLink') != ''){
            
            newImageLink.ImageUrl__c = (string)requestData.get('imageLink');
        }
        
        
        if((string)requestData.get('notes') != null && (string)requestData.get('notes') != ''){
            newImageLink.Notes__c = (string)requestData.get('notes');
        }
        
        //this value is hard coded, the default is 'Other'
        newImageLink.Source__c = 'Pruvan';
        
        if(caseNum != null){
        	
        	//Padmesh Soni (06/26/2014) - Support Case Record Type
			//Query record of Record Type of Case object
			List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
															AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
			
            //Padmesh Soni (06/26/2014) - Support Case Record Type
	    	//New filter criteria added for RecordTypeId
		    for(Case getCase : [SELECT Id FROM Case WHERE RecordTypeId NOT IN: recordTypes AND CaseNumber =: caseNum]){
                newImageLink.CaseId__c = getCase.Id;    
            }
        }
        
        if(surveyID != null && surveyID != ''){
        	newImageLink.Survey_Id__c = surveyID;
        }
        
        /*
        //current fields not being used that may be used if Josh wants them
        newImageLink.Pruvan_csrCertifiedPhoto__c = requestData.get('csrCertifiedPhoto'); //data type: Number
        newImageLink.Pruvan_csrCertifiedTime__c= requestData.get('csrCertifiedTime'); //data type: Number
        newImageLink.Pruvan_csrCertifiedLocation__c= requestData.get('csrCertifiedLocation'); //data type: Number
        newImageLink.Pruvan_csrPictureCount__c= requestData.get('csrPictureCount'); //data type: Number
        */
        
        //create the newImageLink
        if(newImageLink.Pruvan_uuid__c != null){
            upsert newImageLink Pruvan_uuid__c;
        }
        else{
            upsert newImageLink;
        }
    }

public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}    
}