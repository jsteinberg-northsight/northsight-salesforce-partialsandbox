/**
 *  Purpose         :   This class is controller for PropertyGeocoderMap page which is used to verify Property address from Google map Apis. 
 *
 *  Modified By		:   Padmesh Soni
 *
 *  Modified Date	:   1/12/2015
 *
 *  Current Version :   V1.0
 *
 *  Revision Log    :   V1.0 - Modified - CCP-15:Fill County on Property
 *								Changes are:
 *                                  1. New feature of filling County__c field with getting result from Google map of 
 *                                  	attribute "administrative_area_level_2".
 *                                  2. Code fromatted with the appropriate code comments.
 **/
public with sharing class propertyGeocodeController {
	
	//class properties
	public Geocode_Cache__c property{get;set;}
	public string status{get;set;}
	public string param_lat{get;set;}
	public string param_lng{get;set;}
	public string param_street {get; set;}
	public string param_city {get; set;}
	public string param_state {get; set;}
	public string param_zip {get; set;}
	public string param_notes {get; set;}
	public decimal zillowLotSize;
    public decimal zillowPropertySize;
	public string zillowStatus;
	public ZillowResponseWrapper zillowResWrapper;
	public String param_FIP_County;
	
	//Code added - Padmesh Soni (1/12/2015) - CCP-15:Fill County on Property
	//New variable added to store county value from page
	public string param_county {get; set;}
	
	//Constructor defintion
	public propertyGeocodeController(ApexPages.StandardController controller){
		
		//Gettirng record from standard conroller of Geocode_Cache__c
		this.property = (Geocode_Cache__c )controller.getRecord();
		
		//get the property record
		property = [SELECT Id, Formatted_Address__c, Street_Address__c, Formatted_City__c, City__c, Formatted_State__c, State__c,
						Formatted_Zip_Code__c, Zip_Code__c, Location__Latitude__s, Location__Longitude__s, Notes__c, Manual_Location__c, 
						Partial_Match__c, quarantined__c, Flag__c, Geocode_Status__c, Formatted_Street_Address__c 
						FROM Geocode_Cache__c WHERE Id =: property.Id]; 
		
		//string addrHash = GeoUtilities.createAddressHash(GeoUtilities.formatAddress(property.Street_Address__c,property.City__c,property.State__c,property.Zip_Code__c));//create the address hash using the property fields
		
		//set param_street = property.Formatted_Address__c if not null, else call the propertyUtilities.capitalizeAll method passing in propertyUtilities.preformatAddress(property.Street_Address__c)
		
		//Commented and new line of code added - Padmesh Soni - 03/01/2014 - As per Debugging the Geocode Button Subsystem
		//param_street = property.Formatted_Address__c != null ? property.Formatted_Address__c : GeoUtilities.capitalizeAll(GeoUtilities.preformatAddress(property.Street_Address__c));
		param_street = property.Formatted_Street_Address__c != null ? property.Formatted_Street_Address__c : GeoUtilities.capitalizeAll(GeoUtilities.preformatAddress(property.Street_Address__c));
		
		//set param_city = property.Formatted_City__c if not null, else use property.City__c
		param_city = property.Formatted_City__c != null ? property.Formatted_City__c : property.City__c;
		
		//set param_state = property.Formatted_State__c if not null, else use property.State__c
		param_state = property.Formatted_State__c != null ? property.Formatted_State__c : property.State__c;
		
		//set param_zip = property.Formatted_Zip_Code__c if not null, else use property.Zip_Code__c
		param_zip = property.Formatted_Zip_Code__c != null ? property.Formatted_Zip_Code__c : property.Zip_Code__c;
		
		//set param_notes = property.Notes__c
		param_notes = property.Notes__c;
		
		//Getting lot size from Zillow APIs
		getZillowLotSize(param_street, param_city, param_state, param_zip);
	}
	
	/**
     *  @descpriton		:	This method is used to store values from page instances to Property record.
     *
     *  @param			:
     *
     *  @return			:	PageReference
     **/
    public PageReference saveAndRelease(){
    	
		try{
			
			//Trimming all page parametere values and assigning to Proeprty fields
			param_lat = param_lat.trim();
			param_lng = param_lng.trim();
			property.Formatted_Address__c = param_street + ', ' + param_city + ', ' + param_state + ' ' + param_zip;
			property.Formatted_Street_Address__c = param_street;
			property.Formatted_City__c = param_city;
			property.Formatted_State__c = param_state;
			property.Formatted_Zip_Code__c = param_zip;
			property.Location__Latitude__s = decimal.valueOf(param_lat);
			property.Location__Longitude__s = decimal.valueOf(param_lng);
			property.Notes__c = param_notes;
			property.Manual_Location__c = true;
			property.Partial_Match__c = false;
			property.quarantined__c = false;
			property.Flag__c = false;
			property.Geocode_Status__c = 'OK';
			property.Last_Geocoded__c = Date.today();
			property.SqFt_of_Lot_Size__c = zillowLotSize;
            property.Property_Square_Feet__c = zillowPropertySize;
			property.Zillow_Status__c = zillowStatus;
			property.Geo_System__c = 'propertyGeocodeController.cls';
			property.Geo_Timestamp__c = Datetime.now();
			
			//Code added - Padmesh Soni (1/12/2015) - CCP-15:Fill County on Property
			//County field value populated.
			property.County__c = param_county;
			
			update property;
			
			//redirect to Property record when got success
			PageReference p = new PageReference('/' + property.Id);
			p.setRedirect(true);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, p.getUrl()));
			return p;
		} catch(Exception e){
			
			ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' : ' + e.getStackTraceString());
			ApexPages.addMessage(errorMsg);
		}
		return null;
	}
	
	/**
     *  @descpriton		:	This method is used to redirect to previous detail page of Property.
     *
     *  @param			:
     *
     *  @return			:	PageReference
     **/
    public PageReference cancel(){//cancels the geocode
		return new PageReference('/' + property.Id);
	}
	
	/**
     *  @descpriton		:	This function calls the ZillowAPIUtility class to get the lot size in sq.ft.
     *
     *  @param			:	string streetAddrParameter, string cityParameter, string stateParameter, string zipCodeParameter
     *
     *  @return			:	PageReference
     **/
    public void getZillowLotSize(string streetAddrParameter, string cityParameter, string stateParameter, string zipCodeParameter){

		if(streetAddrParameter != null && ((cityParameter != null && stateParameter != null) || zipCodeParameter != null)){//if the address fields are not null
		
			//call the ZillowAPIUtility passing in the address parameters to get the lot size
			zillowResWrapper = ZillowAPIUtility.fetchListingFromZillow(streetAddrParameter, cityParameter, stateParameter, zipCodeParameter);
		
			if(zillowResWrapper != null) {
				
				//if the lotSizeSqFt is not null/empty
				if(String.isNotBlank(zillowResWrapper.lotSizeSqFt)) {
					zillowLotSize = decimal.valueOf(zillowResWrapper.lotSizeSqFt);//set SqFt_of_Lot_Size__c = decimal value of lotSizeSqFt
				} else {					
					//set SqFt_of_Lot_Size__c = 0
					zillowLotSize = 0;
				}
                
                if(String.isNotBlank(zillowResWrapper.finishedSqFt)) {
					zillowPropertySize = decimal.valueOf(zillowResWrapper.finishedSqFt);//set Property_Square_Feet__c = decimal value of finishedSqFt
				} else {
					zillowPropertySize = 0;
				}
				
				//if the message/code are not null/empty
				if((zillowResWrapper.message != null && zillowResWrapper.code != null) && (zillowResWrapper.message != '' && zillowResWrapper.code != '')) {
				
					if(zillowResWrapper.lotSizeSqFt != null && zillowResWrapper.lotSizeSqFt != '') {
						
						//set Zillow_Status__c = a string concatenation of the code & message
						zillowStatus = 'Code: ' + zillowResWrapper.code + ', ' + zillowResWrapper.message;
					} else {
						
						if(zillowResWrapper.code == '0'){
							
							//use this status if we get a code of 0 but there is no lot size returned
							zillowStatus = 'The request was successfully processed, but there was no return on the lot size';
						} else {
							
							//set Zillow_Status__c = a string concatenation of the code & message
							zillowStatus = 'Code: ' + zillowResWrapper.code + ', ' + zillowResWrapper.message;
						}
					}
				} else {
					
					//set Zillow_Status__c = There was no Zillow response message
					zillowStatus = 'There was no Zillow response message';
				}
			} else {
				zillowLotSize = 0;
                zillowPropertySize = 0;
				zillowStatus = 'Zillow did not return a response';
			}
		} else {
			zillowLotSize = 0;
            zillowPropertySize = 0;
			zillowStatus = 'Please make sure all address fields are filled out correctly';
		}
	}
}