public with sharing class DenyPaymentDispute {
	
	public static void denyInsertIfRecordExists(List<Payment_Dispute__c> newPaymentDispute, Map<Id, Payment_Dispute__c> paymentDisputeMap) {
		//create set that holds Ids of work orders that already have a dispute record;
		Set<Id> workOrderSet = new Set<Id>();
		for (Payment_Dispute__c currentDispute : newPaymentDispute) {
			workOrderSet.add(currentDispute.Work_Order_Number__c);
		}
		Set<Id> workOrdersWithPaymentRecords = new Set<Id>();
		for(Payment_Dispute__c currentDispute : [SELECT
																			Work_Order_Number__c
																		FROM
																			Payment_Dispute__c
																		WHERE
																			Work_Order_Number__c IN: workOrderSet]) {
			workOrdersWithPaymentRecords.add(currentDispute.Work_Order_Number__c);
		}
		for (Payment_Dispute__c currentDispute : newPaymentDispute) {
			if (workOrdersWithPaymentRecords.contains(currentDispute.Work_Order_Number__c)) {
				currentDispute.Name.addError('Work Order already contains a Payment Dispute record.');
				break;
			}
		}
	}
}