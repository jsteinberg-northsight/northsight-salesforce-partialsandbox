/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/


20170411:TBH - JSON example payload below
//survey
{"timestamp":1491933154,"serviceId":"427137208","workOrderId":"65205983","evidenceType":"Survey","fileType":"survey","csrCertifiedPhoto":"1","csrCertifiedTime":"1","csrCertifiedLocation":"1","csrPictureCount":null,"uuid":"c5c35836-0924-4a73-80cd-8284438a01bf","parentUuid":null,"fileName":"survey_1491933138637.json","gpsLatitude":"25.59916404","gpsLongitude":"-80.34404666","gpsTimestamp":"1491933138","gpsAccuracy":"5.0","key2":"03989491","key4":"C22 Grass Cut","notes":null,"uploaderVersion":"mobile 3.16.24","deviceId":"864792030392432","sdkVersion":"23","createdBySubUser":"nmmharrison","pictureId":"758409176","username":"pruvan","password":"8c8cd34c37d6813808baa358b18169c211cb47f3","survey":{"meta":{"surveyTemplateId":"sarcj07::C23_Grass_CV_BOA-v1","surveyId":"c5c35836-0924-4a73-80cd-8284438a01bf","answersVersion":1},"answers":[{"answer":["yes"],"hint":"","id":"Work_Completed__c","picIds":[]},{"answer":[],"hint":"","id":"CHECKIN.front of house photo","picIds":["f221f954-53bd-4e09-a5fe-a0d6b858ab27","ae8e2a94-806c-4a99-a72a-0d3204cceaff"]},{"answer":[],"hint":"If no house number, take photo of neighboring property as reference.","id":"CHECKIN.house number photo","picIds":["da16cd3f-afee-4f92-8c55-86af6a139ef0"]},{"answer":["no"],"hint":"","id":"Excessive_Growth__c","picIds":[]},{"answer":["no"],"hint":"","id":"Shrubs/Trees/Vines Present","picIds":[]},{"answer":["no"],"hint":"","id":"leaves present?","picIds":[]},{"answer":["no"],"hint":"","id":"Debris_Removed__c","picIds":[]},{"answer":["no"],"hint":"These items can include but are not limited to toys, patio furniture,grills, and tools","id":"Personal Debris","picIds":[]},{"answer":["yes"],"hint":"","id":"Backyard_Serviced__c","picIds":[]},{"answer":[],"hint":"","id":"before right towards house","picIds":["efcb7ad5-6de1-42dc-9268-6aa6c553a007","db6e31c4-42bd-4088-a517-e8b043b2cb02"]},{"answer":[],"hint":"","id":"before rear towards house","picIds":["ea45d2e4-4d5b-4f95-9dfa-098a7b5803ee","43854cfa-ac1f-440f-ad10-7a0b1d2af86a"]},{"answer":[],"hint":"","id":"before rear away from house","picIds":["4e41524f-87e4-4dd7-87e6-ab2dd1744820","f57b1bc6-530a-4ae8-90c7-8b8ad7290516"]},{"answer":[],"hint":"","id":"before left towards house","picIds":["42616207-04a3-4f0b-8db3-724567f68765","75130380-1dd6-4fdc-bc07-a25ee8e64fcd"]},{"answer":[],"hint":"","id":"front yard during","picIds":["8add4c8a-9c3e-4428-a0ba-525d4ed1644f","284d1371-818e-4db1-8cbd-b3f7fdee33e8"]},{"answer":[],"hint":"","id":"rear yard during","picIds":["70679645-24de-4678-a48c-d83353a1a95c"]},{"answer":[],"hint":"(including along house foundation and fence line)","id":"weed whack photos","picIds":["560d9bd8-1c8b-4987-a167-6d9a746b78e5"]},{"answer":[],"hint":"(including all paved surfaces)","id":"edging photos","picIds":["1b1a7749-24d4-4a63-b34e-97e04b76bb37","22251319-1b7b-4a40-9f3e-c4a926764094"]},{"answer":[],"hint":"","id":"weed spray photos","picIds":["6288805c-c31e-4ccc-8585-33ee3115ee69","afeb7484-e1de-4634-9101-a2a557fa7ccb"]},{"answer":[],"hint":"","id":"after front towards house","picIds":["1cc830c1-e1e9-4003-ac7d-4d11e873c4cd","2f65cf64-c7c6-4df2-af8f-69e42a74be0b"]},{"answer":[],"hint":"(must be able to read 2" off the ruler)","id":"after front ruler photos","picIds":["ca5d65f7-b3e0-4730-b3ec-99e61d952bd5"]},{"answer":[],"hint":"","id":"after right towards house","picIds":["35497d35-6e20-4262-8f44-95f95b4b8f07","9584d43b-a18b-44d9-affc-b9062d3e3416"]},{"answer":[],"hint":"","id":"after rear towards house","picIds":["903cc3e7-d868-42f9-8c09-98216a7977d5","ae502200-f367-4ddd-b06b-c76eb4e44f28"]},{"answer":[],"hint":"","id":"after rear away from house","picIds":["81d97808-2be9-410f-bda2-bda1a30b3c05","adbd9232-8625-4278-a5a1-4b7fc6c22112"]},{"answer":[],"hint":"(demonstrating areas are clear of overgrowth or weeds)","id":"after foundation photos","picIds":["741b4c22-b51d-4334-93d6-454d6b19a4df","fc6bfbf4-c6cb-49f4-9b0b-6041a79937c3","7ee70889-f0c3-4eb8-ada1-e71e891e1a39"]},{"answer":[],"hint":"","id":"after left towards house","picIds":["485b3e80-b01d-4dda-9fbf-76e760df5f1d","d4b91377-1377-4323-8620-59af552750bc"]},{"answer":[],"hint":"(Stick edger is generally required)","id":"after edging photos","picIds":["2dbf1ae3-bc0c-464d-8957-41716e32c207","cc5991db-da4b-4524-a92e-d87a1b3cdc97"]},{"answer":[],"hint":"","id":"For Sale Sign Photo","picIds":["6dfa2125-392f-4407-aa02-2ebf26e33df0"]},{"answer":[],"hint":"","id":"Gate Photos","picIds":["f8d68d25-e338-4165-a5d8-e3ecfe2006b2"]},{"answer":[],"hint":"","id":"Street View Photos","picIds":["94c41553-04a1-4a83-8830-d27b115b8f3a","53ea6a80-f9c4-4975-b7db-7df037943ff5","56d9e68a-b2e0-431a-973f-221fccce9a91","38392e48-a24f-4b22-b059-52d21e038a25"]},{"answer":[],"hint":"(if applicable)","id":"Behind Outbuilding/Garage Photo","picIds":[]},{"answer":[],"hint":"(if applicable)","id":"Under Deck/Stairwell Photo","picIds":[]},{"answer":[],"hint":"(if applicable)","id":"Flower Bed Photo","picIds":[]},{"answer":["no"],"hint":"","id":"work order notes?","picIds":[]}]}}

//csr
{"timestamp":1491933179,"serviceId":"426907849","workOrderId":"65195959","evidenceType":"csr","fileType":"csr","csrCertifiedPhoto":"40","csrCertifiedTime":"40","csrCertifiedLocation":"40","csrPictureCount":"40","uuid":"35629e18-6205-42f7-9cc7-153ea0ffa92f","parentUuid":null,"fileName":"PruvanCSR-batch99087713.jpg","gpsLatitude":"38.951790","gpsLongitude":"-76.947190","gpsTimestamp":"1491933168","gpsAccuracy":"22.39","key2":"03982752","key4":"Grass Cut","notes":null,"uploaderVersion":"mobile 3.16.24","deviceId":"35560207396369","sdkVersion":"23","createdBySubUser":"nmtaking","pictureId":"758409935","username":"pruvan","password":"8c8cd34c37d6813808baa358b18169c211cb47f3"}

//photo
{"timestamp":1491933191,"serviceId":"426128194","workOrderId":"65172316","evidenceType":"weed spray photos","fileType":"picture","csrCertifiedPhoto":"1","csrCertifiedTime":"1","csrCertifiedLocation":"0","csrPictureCount":null,"uuid":"d1162591-26a2-4713-9e2f-8d14014e7d38","parentUuid":"1bb91734-b3b1-4f2e-8122-735d697d1e58","fileName":"1491933171062.jpg","gpsLatitude":"41.50687996","gpsLongitude":"-74.02253023","gpsTimestamp":"1491933171","gpsAccuracy":"12.0","key2":"03965287","key4":"C22 Initial Grass Cut","notes":null,"uploaderVersion":"mobile 3.16.24","deviceId":"35489207776979","sdkVersion":"22","createdBySubUser":"nmjzenzel","pictureId":"758410083","username":"pruvan","password":"8c8cd34c37d6813808baa358b18169c211cb47f3"}
*/


public without sharing class PruvanSurveyHelper {
    private static Map<string,object> surveyRequest;
    private static string caseNum;
    private static Map<string,object> survey;
    private static Map<string,object> meta;
    private static string surveyID;
    private static List<object> answers;
    private static Map<string,object> surveyResponse;
    private static string responseJSON;
    private static Map<string,object> nsSurveyExtract;
    private static Map<string,object> nsSurveyMeta;
    private static Id bidMobile;
    
    public static Map<String, ImageLinks__c> imageList;
    
    private static Map<string, object> innerAnswerMap;
    private static Map<string, Map<string, object>> outerAnswerMap;
    private static Case newWorkOrder;
    private static Map<string,object> validResponse;
    private static Database.Dmloptions dml;
    private static List<string> tripCharges;
    private static List<string> questionPrefixes;
    
    //New variable add to hold input JSON string of input payload 
    //Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
    private static String inputPayload;
    
    //this method returns validation back to Pruvan, bypassing the submission of survey data into SF until photos are also uploaded
    public static string validatePruvanSurveys(string input){   
        //parse input into a map that is generic data
        surveyRequest = (map<string,object>)JSON.deserializeUntyped(input);
        
        //get the meta data from nsSurveyExtract
        if(surveyRequest.size() > 0){
            if(surveyRequest.containsKey('meta')){
                meta = (map<string, object>)surveyRequest.get('meta');
            } else {
                meta = new Map<string, object>();
            }
        } else {
            meta = new Map<string, object>();
        }
                
        //start building the response
        surveyResponse = new Map<string, object>();
        surveyResponse.put('meta', meta);   
            
        try {
            //call this method for updating a work order's survey fields
            //setMobileVersion(surveyRequest);
            surveyResponse.put('error', null);
        }
        catch(Exception e) {
            //if there is an exception put the error in the response
            //surveyResponse.put('answers', answers);
            surveyResponse.put('error', e.getMessage()+' '+e.getStackTraceString());
            
            if(system.Test.isRunningTest()){//will show errors/exceptions hidden from tests
                    throw e;
            }
        }
        
        return responseJSON = JSON.serialize(surveyResponse);
    }
    
    
    //this method will set the mobile version of the uploader on the work order
    //private static void setMobileVersion(Map<string,object> surveyData){
    private static void extractMobileInfoAndUser(Map<string,string> mobileData){//String woNumber
        //get the mobile version from the payload(first 2 numbers only)
        String uploaderVersion = mobileData.get('uploaderVersion');
        //String mobileVersion = uploaderVersion.substring(7, 10);
        String device = mobileData.get('deviceId');
        String deviceOS = mobileData.get('sdkVersion');
        String pruvanUser = mobileData.get('pruvanUser');
        
        //select in the mobile version field for the order that has order number = key2
        //Case caseToUpdate = [Select Id, Mobile_Version__c, Pruvan_Mobile_App__c, Vendor_Mobile_Device__c, Vendor_Device_OS_Version__c, Pruvan_Username__c From Case Where CaseNumber =: woNumber for update];
        newWorkOrder.Mobile_Version__c = uploaderVersion != '0' ? decimal.valueOf(uploaderVersion.substring(0, 3)) : 0;
        newWorkOrder.Pruvan_Mobile_App__c = uploaderVersion != '0' ? uploaderVersion : 'Pruvan app version not specified';
        newWorkOrder.Vendor_Mobile_Device__c = device == 'ios' ? 'iOS' : device == '0' ? 'Device not specified' : 'Android';
        newWorkOrder.Vendor_Device_OS_Version__c = deviceOS != '0' ? deviceOS : 'Device OS not specified';
        newWorkOrder.Pruvan_Username__c = pruvanUser;
        //update the order
        //update caseToUpdate;
    }
    
    
    //this method initializes many of the required variables needed for updating a work order's survey fields
    public static Map<string,object> initialize(String input, String key, Map<string,string>mobileAndUserInfo){
        validResponse = new Map<string, object>();
        
        //set the dml var Dmloptions to allow for field truncation
            //this will be used to allow data to be truncated if it is too long before being inserted/updated
        dml = new Database.Dmloptions();
        dml.allowFieldTruncation = true;

        //
        tripCharges = new List<string>{
            'tc.',
            'tc1.',
            'tc2.'
        };
        //
        questionPrefixes = new List<string>{
            'gcg.',
            'gck.',
            'gcna.',
            'cl.',
            'occov.',
            'occ.',
            'hoary.',
            'hoa.',
            'sc.',
            'rfc.',
            'oa.',
            'other.',
            'req.',
            'inv:',
            'na.',
            'nl.'
        };
        
        //instantiate the imageList
        imageList = new Map<String, ImageLinks__c>();
        
        //parse input into a map that is generic data
        nsSurveyExtract = new Map<string,object>();
        nsSurveyExtract = (map<string,object>)JSON.deserializeUntyped(input);
        //the key is the work order's CaseNumber
        caseNum = key;
        
        //get the bid from mobile record type
        bidMobile = [SELECT Id FROM RecordType WHERE Name = 'Bid from Mobile' AND sObjectType = 'Bid__c'].Id;
        
        //get the meta data from nsSurveyExtract
        try{
            //get the mobile info first
            //extractMobileInfoAndUser(mobileAndUserInfo, caseNum);
            
            //Padmesh Soni (06/26/2014) - Support Case Record Type
            //Query record of Record Type of Case object
            List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
                                                            AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
            
            //instantiate newWorkOrder with the id = id of an existing work order that has a CaseNumber = caseNum
            if(caseNum != null){
                
                //Padmesh Soni (06/26/2014) - Support Case Record Type
                //New filter criteria added for RecordTypeId
                for(Case getCase : [SELECT Id FROM Case WHERE RecordTypeId NOT IN: recordTypes AND CaseNumber =: caseNum]){
                    newWorkOrder = getCase; 
                }
            }
            
            //get the surveyTemplateID in the meta data and re-cast it as a string
            nsSurveyMeta = (map<string, object>)nsSurveyExtract.get('meta');
            string surveyTemplateID = (string)nsSurveyMeta.get('surveyTemplateId');
            List<String> surveyTemplate = surveyTemplateID.split('::');
            surveyID = surveyTemplate[surveyTemplate.size()-1];
            
            //get the answers data from nsSurveyExtract
            answers = (List<object>)nsSurveyExtract.get('answers');
            
            //instantiate innerAnswerMap
            innerAnswerMap = new Map<string, object>();
            
            //instantiate outerAnswerMap
            outerAnswerMap = new Map<string, Map<string, object>>();
            
            //loop thru the list of answers data, append the data to the innerAnswerMap, then append that map to the outerAnswerMap
            for(Object o : answers){
                innerAnswerMap = (map<string, object>)o;
                outerAnswerMap.put((string)innerAnswerMap.get('id'), innerAnswerMap);       
            }
            
            //update the survey data on the specified work order
            updateSFWorkOrderSurveyFields();
            //get the mobile info first
            extractMobileInfoAndUser(mobileAndUserInfo);
            //update the order
            finalize();
            
            validResponse.put('status', true);
            validResponse.put('error', null);
            
            

            
        }
        catch(Exception e){
            validResponse.put('status', false);
            validResponse.put('error', e.getMessage()+'\r\n'+e.getStackTraceString()+'\r\n'+input);
        }
        
        return validResponse;
    }
    
   
    
    
    //this method will update the WO survey fields in Salesforce and create any image links found on questions
    private static void updateSFWorkOrderSurveyFields(){
        //check to see if the current survey is the BidSurvey
        if(surveyID.contains('BidSurvey')){//if we have the BidSurvey
            BidSurveyHelper.initialize(newWorkOrder, surveyID, outerAnswerMap);
            //BidSurveyHelper.finalize();  //TBH:20170411-seems like this should not be called as finalize() is called in initialize.  Commenting out this line.
        } else {
            //nulling out reason fields to prevent old values causing errors
            newWorkOrder.Trip_Charge_Reason__c = null;
            newWorkOrder.Backyard_Serviced__c = 'Yes';
            newWorkOrder.No_Service_Reason__c = null;
    
            //make sure the surveyID matches the survey Id of the survey being used in Pruvan
            //if(surveyID == 'GC_001-v4'||surveyID == 'GC_001-v3'||surveyID == 'GC_001-v2'||surveyID == 'GC_001-v1'||surveyID == 'NS_REO_010'){
            
            //check for questions in outerAnswerMap, if the question exists set the appropriate field on newWorkOrder
            //also check for image links on question, if there are, create image links for those questions
            
            //if statement for handling the absence of Work_Completed__c, if incoming survey is GC22_IGC_001
            if(outerAnswerMap.containsKey('Trip_Charge_Reason__c') && !surveyID.contains('BidSurvey')){
                newWorkOrder.Work_Completed__c = 'Trip Charge';
            } else {
                newWorkOrder.Work_Completed__c = 'Yes';
            }
            
            //if statement for handling the creation of a Bid object if the map contains keys that relate to Bid fields
            if(outerAnswerMap.containsKey('Bid.Shrub_Length__c') && outerAnswerMap.containsKey('Bid.Shrub_Width__c') && outerAnswerMap.containsKey('Bid.Shrub_Height__c') && outerAnswerMap.containsKey('Bid.Quantity__c') && outerAnswerMap.containsKey('Bid.Bid_Cost__c') && outerAnswerMap.containsKey('Bid.Bid_Description__c')){
                createBid();
            }
            
            //if statement for handling the setting of the Work_Completed__c field based on the following two question Ids
            if(outerAnswerMap.containsKey('inspection not completed photos')){
                newWorkOrder.Work_Completed__c = 'Trip Charge';
                newWorkOrder.Trip_Charge_Reason__c = 'Other';
            }
            else if (outerAnswerMap.containsKey('completed QC checklist')){
                newWorkOrder.Work_Completed__c = 'Yes';
            }

            for(string answerKey : outerAnswerMap.keySet()){//loop through each question
                
                if(answerKey == 'Work_Completed__c'){//if we are on the work completed question
                    List<object> answerList = (list<object>)outerAnswerMap.get(answerKey).get('answer');
                    if(answerList[0] != 'No'){//we set work completed = 'Yes' if the answer is not 'No'
                        newWorkOrder.Work_Completed__c = 'Yes';
                    }
                    else {//if the answer was 'No' this means that the work order is a 'Trip Charge'
                        newWorkOrder.Work_Completed__c = 'Trip Charge';
                    }
                }
                else if(answerKey != 'Bid.Shrub_Length__c' && answerKey != 'Bid.Shrub_Width__c' && answerKey != 'Bid.Shrub_Height__c' && answerKey != 'Bid.Quantity__c' && answerKey != 'Bid.Bid_Cost__c' && answerKey != 'Bid.Bid_Description__c') {
                    //if we did not hit any of the specified questions before, then call the FieldHelper to set the field value on the work order
                    List<object> answerList = (list<object>)outerAnswerMap.get(answerKey).get('answer');
                    //if there is a question of ID 
                    if(answerKey == 'Bad_Weather_Process__c'){
                        answerList[0] = true;
                    }
                    
                    if(answerList != null && answerList.size() > 0){
                        string K = answerKey;
                        if(k.indexOf('INV:', 0) == 0){//check for the inverse prefix (INV:)
                            //inverse the answer
                            if(((string)answerList[0]).toLowerCase() == 'yes'){
                                answerList[0] = 'No';
                            }
                            else
                            {
                                answerList[0] = 'Yes';
                            }
                        }
                        //check to see if there are other prefixes on the question Ids
                        for(integer i = 0; i < tripCharges.size(); i++){//these are trip charge prefixes, so Work_Completed__c = Trip Charge
                            if(K.toLowerCase().contains(tripCharges[i])){
                                K = K.toLowerCase().replace(tripCharges[i], '');
                                newWorkOrder.Work_Completed__c = 'Trip Charge';
                                newWorkOrder.Trip_Charge_Reason__c = K;
                            }
                        }
                        for(integer i = 0; i < questionPrefixes.size(); i++){//these are misc. question prefixes that should be removed
                            if(K.toLowerCase().contains(questionPrefixes[i])){
                                K = K.toLowerCase().replace(questionPrefixes[i], '');
                            }
                        }
                        K = K.trim();
                        
                        //system.debug('*****answerList for valid questions: '+ answerList + '*****');
                        FieldHelper.setFieldValue(newWorkOrder, K, answerList[0]);
                    }
                }
                
                if(outerAnswerMap.get(answerKey).get('picIds') != null){//check if there are any picture IDs on the current question
                    for(object objUUID : (list<object>)outerAnswerMap.get(answerKey).get('picIds')){//for each picture, create an image link related to the question for this work order
                        addImageLinkForSurveyQuestion(answerKey, string.valueOf(objUUID));
                    }
                }
            }
                
            //if there is a question of id 'HOA.service backyard stmt', then set Work_Completed = 'Yes'
            if(outerAnswerMap.containsKey('HOA.service backyard stmt')){
                newWorkOrder.Work_Completed__c = 'Yes';
            }
            
            //if there is a question of id 'HOA.service backyard stmt', then set Work_Completed = 'Yes'
            if(outerAnswerMap.containsKey('Shrub_Quantity__c') || outerAnswerMap.containsKey('trim and bid shrubs stmt')){
                newWorkOrder.Shrubs_Trimmed__c = 'Yes';
            }
            
            //this is a check to make sure debris removed is 'No' if values of <= 0 or null are in the Number_of_Bags_Removed__c & Number_of_Cubic_Yards_Removed__c fields
            if((newWorkOrder.Number_of_Bags_Removed__c <= 0 || newWorkOrder.Number_of_Bags_Removed__c == null) && (newWorkOrder.Number_of_Cubic_Yards_Removed__c <= 0 || newWorkOrder.Number_of_Cubic_Yards_Removed__c == null)){
                    newWorkOrder.Debris_Removed__c = 'No';
                    newWorkOrder.Number_of_Bags_Removed__c = null;
                    newWorkOrder.Number_of_Cubic_Yards_Removed__c = null;
            }
            else if(newWorkOrder.Number_of_Bags_Removed__c > 0 || newWorkOrder.Number_of_Cubic_Yards_Removed__c > 0){
                newWorkOrder.Debris_Removed__c = 'Yes';
            }
            
            //if the vendor did not provide notes to staff when it was required..
            if(newWorkOrder.Vendor_Notes_To_Staff__c == null || newWorkOrder.Vendor_Notes_To_Staff__c == ''){
                newWorkOrder.Vendor_Notes_To_Staff__c = 'Vendor did not submit notes.';
            }
        }
    }
    
    //this method will create a new bid object linked to the work order      
    private static void createBid(){
        //extract all bid answers into a map of string field name to value
        Map<string, object> bidMap = new Map<string, object>();
            bidMap.put('Shrub_Length__c', string.valueOf(outerAnswerMap.get('Bid.Shrub_Length__c').get('answer')).replace('(', '').replace(')', ''));
            bidMap.put('Shrub_Width__c', string.valueOf(outerAnswerMap.get('Bid.Shrub_Width__c').get('answer')).replace('(', '').replace(')', ''));
            bidMap.put('Shrub_Height__c', string.valueOf(outerAnswerMap.get('Bid.Shrub_Height__c').get('answer')).replace('(', '').replace(')', ''));
            bidMap.put('Quantity__c', string.valueOf(outerAnswerMap.get('Bid.Quantity__c').get('answer')).replace('(', '').replace(')', ''));
            bidMap.put('Bid_Cost__c', string.valueOf(outerAnswerMap.get('Bid.Bid_Cost__c').get('answer')).replace('(', '').replace(')', ''));
            bidMap.put('Bid_Description__c', string.valueOf(outerAnswerMap.get('Bid.Bid_Description__c').get('answer')).replace('(', '').replace(')', ''));

        //create a new bid object of trim shrub record type associated with the current work order
        Bid__c newBid = new Bid__c(
            RecordTypeId = bidMobile,
            Work_Order__c = newWorkOrder.Id
        );
        //map the values to the bid object values if able to
        for(string bidField : bidMap.keySet()){
            FieldHelper.setFieldValue(newBid, bidField, bidMap.get(bidField));
        }
        insert newBid;
    }
    
    //this method will create image links and add them to the imageList
    public static void addImageLinkForSurveyQuestion(string questionId, string uuId){
        ImageLinks__c newImageLink = new ImageLinks__c(
            CaseId__c = newWorkOrder.Id,
            Survey_Id__c = surveyID,
            Survey_Question_Id__c = questionId,
            Pruvan_uuid__c = uuId
        );
        imageList.put(uuId, newImageLink);
    }
    
    //this method upserts images links, sets remaining work order fields, and updates the work order
    private static void finalize(){
        if(!surveyID.contains('BidSurvey')){
            //if the imageList has a size > 0, then it means there are image links that need to be upserted
            if(imageList.size() > 0){
                upsert imageList.values() Pruvan_uuid__c;
            }
            //set the below work order fields accordingly
            newWorkOrder.Date_Serviced__c = Date.today();
            newWorkOrder.Request_Reassignment__c = false;
            newWorkOrder.Survey_Received__c = true;
            newWorkOrder.Survey_Received_Datetime__c = Datetime.now();
            
            List<ImageLinks__c> currentCasePhotos = [select ImageUrl__c from ImageLinks__c where CaseId__c =: newWorkOrder.Id];//get the current work order's image links, if any
            
            if(currentCasePhotos.size() > 0){//if there are any image links on this work order then...
                newWorkOrder.All_Images_Received__c = true;//set all images received to true
                for(ImageLinks__c iL:currentCasePhotos){
                    if(iL.ImageUrl__c == null || iL.ImageUrl__c == ''){//if even one image link does not have a URL, then the work order's approval status is not set
                        newWorkOrder.All_Images_Received__c = false;//set all images received to false
                    }
                }
            }
        }
        
        //set the dml option on the newWorkOrder
        newWorkOrder.setOptions(dml);
        
        //Add try catch block for DmlException handling and update the Case record into catch block         
        //Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
        try {
            //update any changes made to the queried case object
            update newWorkOrder;
        } catch(DMLException e) {
            //Padmesh Soni - 02/19/2013 - Northsight: Pruvantm Surveys - Handling Field Data Exceptions
            //Getting fields which are getting exceptions
            Schema.sObjectField [] workOrderFields = e.getDmlFields(0);
            
            //Loop through all field which encountered DML Exceptions
            for(Schema.sObjectField field : workOrderFields) {
                
                //Check for Bid Cost field and put 1 into this 
                if(field.getDescribe().getName() == 'Bid_Cost__c') {
                    
                    //assign default value on field
                    newWorkOrder.put(field.getDescribe().getName(), 1);
                    
                    //Check if Invalid Survey Data already exists, any value
                    if(newWorkOrder.get('Invalid_Survey_Data__c') != null || newWorkOrder.get('Invalid_Survey_Data__c') != '') {
                        
                        //Create invalid value string
                        String invalidValue = newWorkOrder.get('Invalid_Survey_Data__c') + '\n Request Input Payload :' + inputPayload;
                        
                        //Putting JSON String into Invalid_Survey_Data field of case
                        newWorkOrder.put('Invalid_Survey_Data__c', invalidValue);
                    } else
                        //Putting JSON String into Invalid_Survey_Data field of case
                        newWorkOrder.put('Invalid_Survey_Data__c', inputPayload);
                    
                    //Continue to next field processing
                    continue;
                } else {
                    
                    //Putting JSON String into Invalid_Survey_Data field of case
                    newWorkOrder.put('Invalid_Survey_Data__c', inputPayload);
                }
                
                //putting field with default value
                newWorkOrder.put(field.getDescribe().getName(), null);
            }
            
            //update any changes made to the queried case object
            update newWorkOrder;
        }
    }
    
	
	public static void unitTestBypass(){
		integer i = 0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
			
	}
	
	
	    
}