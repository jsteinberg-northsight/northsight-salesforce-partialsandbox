public without sharing class RouteStopWrapper implements Comparable {
private Route_Stop__c stop{
	get{
		if(stop==null){
			stop = new Route_Stop__c();
		}
		return stop;
	}
	set;
	}
public Route_Stop__c innerStop(){
	return stop;
}  
public Case workOrder{
	get{
		if(workOrder==null){
			workOrder=new Case();
		}
		return workOrder;
	}
	set;
	}
public string WorkOrderId{
	get{
		if(stop==null){stop=new Route_Stop__c();}
		return stop.Work_Order__c;
	}
	set{
		if(stop==null){stop=new Route_Stop__c();}
		try{
		stop.Work_Order__c = value;
		}catch(Exception e){
			stop.Work_Order__c = null;
		}
	}
} 

public integer CompareTo(Object compareTo){
	RouteStopWrapper otherStop = (RouteStopWrapper)compareTo;
	if(this.stopNumber == otherStop.stopNumber) return 0;
	if(this.stopNumber > otherStop.stopNumber) return 1;
	return -1;
}
public ID Id{
	get{
		if(stop==null){stop=new Route_Stop__c();}
		return stop.Id;
	}
	set{
		if(stop==null){stop=new Route_Stop__c();}
		Route_Stop__c tmpStop = new Route_Stop__c(Id=value);
		tmpStop.Address__c = stop.Address__c;
		tmpStop.City__c = stop.City__c;
		tmpStop.State__c = stop.State__c;
		tmpStop.Zip_Code__c = stop.Zip_Code__c;
		tmpStop.Work_Order__c = stop.Work_Order__c;
		tmpStop.Stop_Number__c = stop.Stop_Number__c;
		stop = tmpStop;
	}
}
public Id routeId{
	get{
		if(stop==null){stop=new Route_Stop__c();}
		return stop.Route__c;
	}
	set{
		if(stop==null){stop=new Route_Stop__c();}
		stop.Route__c = value;
	}
}

public integer originalIndex{
	get{
		if(stop==null){stop=new Route_Stop__c();}
		return (integer)stop.originalIndex__c;
	}
	set{
		if(stop==null){stop=new Route_Stop__c();}
		stop.originalIndex__c = value;
	}
}
public integer stopNumber{
	get{
		if(stop==null){stop=new Route_Stop__c();}
		return (integer)stop.Stop_Number__c;
	}
	set{
		if(stop==null){stop=new Route_Stop__c();}
		stop.Stop_Number__c = value;
	}
}
public decimal latitude{
	get{
	if(stop==null){stop=new Route_Stop__c();}
		return stop.Location__Latitude__s==null?0:stop.Location__Latitude__s;
	}
	set{
		stop.Location__Latitude__s=value;
	}
}
public decimal longitude{
	get{
	if(stop==null){stop=new Route_Stop__c();}
		return stop.Location__Longitude__s==null?0:stop.Location__Longitude__s;
	}
	set{
		stop.Location__Longitude__s=value;
	}
}
public string address{
	get{ 
		if(stop==null){stop=new Route_Stop__c();}
		return stop.Address__c;
	   }
	set{
		if(stop==null){stop=new Route_Stop__c();}
		stop.Address__c = value;
		}
}
public string city{
	get{
		if(stop==null){stop=new Route_Stop__c();}
		return stop.City__c;
	}
	set{
		if(stop==null){stop=new Route_Stop__c();}
		stop.City__c = value;
	}
}
public string directions{
	get{
		if(stop==null){stop=new Route_Stop__c();}
		return stop.Directions__c;
	}
	set{
		if(stop==null){stop=new Route_Stop__c();}
		stop.Directions__c = value!=null?value.unescapehtml4():null;
	}
}
public string state{
	get{
		if(stop==null){stop=new Route_Stop__c();}
		return stop.State__c;
	}
	set{
		if(stop==null){stop=new Route_Stop__c();}
		stop.State__c = value;
	}
}
public string zip{
	get{
		if(stop==null){stop=new Route_Stop__c();}
		return stop.Zip_Code__c;
	}
	set{
		if(stop==null){stop=new Route_Stop__c();}
		stop.Zip_Code__c = value;
	}
}
public   string color{
	get{
		if(workOrder==null){
			return null;
		}
		else{
			return workOrder.Status_Health_Text__c;
		}
	}
	
}
public string status {
	get { 
		if(workOrder==null){
			return null;
		}
		else{
			return workOrder.Vendor_Status__c;
		}
	}
	
}
public string schedDate{
	get{
		if(workOrder==null){
			return null;
		}
		else{
			if(workOrder.Scheduled_Date__c == null){
				return null;
			}else{
				return workOrder.Scheduled_Date__c.format();
			}
		}
	}
}
public  string WON{
	get{
		if(workOrder!=null){
			return workOrder.CaseNumber;
		}
		else{
			return null;
		}
	}
}
public  string WOD{
	get{
		if(workOrder!=null){
			return workOrder.Work_Ordered_Text__c;
		}
		else{
			return null;
		}
	}
}
public integer Client{
	get{
		if(workOrder!=null){
			return (integer)workOrder.Client_Number__c;
		}
		else{
			return null;
		} 
	}
}
public string hasNotes {
	get {
		if(workOrder!=null) {
			return workOrder.notes__c;
		} else {
			return null;
		}
	}
}
public string Notes{
	get{
		if(workOrder!=null){
			return workOrder.Description;
		}
		else{
			return null;
		} 
	}
}		
public RouteStopWrapper(){
	stop = new Route_Stop__c();
}
public RouteStopWrapper(Route_Stop__c s){
	stop = 	s;
			
	if(stop.Work_Order__r!=null){
		workOrder = stop.Work_Order__r;
		if(workOrder.Geocode_Cache__r.Formatted_Address__c!=null){
			address = workOrder.Display_Street__c;
			city = workOrder.Display_City__c;
			state = workOrder.Display_State__c;
			zip = workOrder.Display_Zip_Code__c;
		}else{
			address = workOrder.Street_Address__c;
			city = workOrder.City__c;
			state = workOrder.State__c;
			zip = workOrder.Zip_Code__c;
		}
	}
}
public RouteStopWrapper(Case wo){
	System.assert(wo!=null);
	System.assert(wo.Id!=null);
	stop = new Route_Stop__c();	
	stop.Work_Order__c = wo.Id;
	workOrder = wo;
		if(workOrder.Geocode_Cache__r.Formatted_Address__c!=null){
			address = workOrder.Display_Street__c;
			city = workOrder.Display_City__c;
			state = workOrder.Display_State__c;
			zip = workOrder.Display_Zip_Code__c;
		}else{
			address = workOrder.Street_Address__c;
			city = workOrder.City__c;
			state = workOrder.State__c;
			zip = workOrder.Zip_Code__c;
		}
}
public static void deleteList(RouteStopWrapper[] stops){
	delete(RouteStopWrapper.prepAndUnwrapList(stops));
}
public static void saveList(RouteStopWrapper[] stops){
	upsert(RouteStopWrapper.prepAndUnwrapList(stops));
}
public static Route_Stop__c[] prepAndUnwrapList(RouteStopWrapper[] stops){
	Route_Stop__c[] unwrappedStops = new Route_Stop__c[]{};
	for(RouteStopWrapper r:stops){
		if(r.stop!=null){
			unwrappedStops.add(r.stop);
		}
	}
	return unwrappedStops;
}
public static testmethod void testSerialization(){
	RouteStopWrapper r = new RouteStopWrapper();
	r.address = '123 main st.';
	r.city = 'Covington';
	r.state = 'LA';
	r.zip = '70460';
	string dataString = Json.serialize(r);
	dataString  = '{"zip":"70460","WorkOrderId":null,"workOrder":{"attributes":{"type":"Case"}},"WON":null,"WOD":null,"stopNumber":null,"state":"LA","schedDate":null,"routeId":null,"originalIndex":null,"Notes":null,"longitude":0,"latitude":0,"Id":null,"directions":null,"color":null,"Client":null,"city":"Covington","address":"123 main st."}';
	//System.debug(dataString);
	RouteStopWrapper nr = (RouteStopWrapper)Json.deserialize(dataString, RouteStopWrapper.class);
	string dataString2 = Json.serialize(nr);
	//System.debug(dataString2);
	string dataString3 = '{"zip" : "70460","state" : "LA","Id" : null,"city" : "Covington","Address2" : "Covington LA 70460","Address1" : "123 main st."}';
	RouteStopWrapper nr2 = (RouteStopWrapper)Json.deserialize(dataString3, RouteStopWrapper.class);
	string dataString4 = Json.serialize(nr2);
	//System.debug(dataString4);
	Route__c route = new Route__c();
	route.Route_Label__c = 'my route';
	route.Est_Completion_Date__c = Date.Today();
	upsert route;
	r.stop.Route__c = route.Id;
	upsert r.stop;
	//System.debug(Json.serialize(r));
}

 
}