global class ScheduleBatchContactSubmittedOrderCounts implements Schedulable{
	global void execute(SchedulableContext ctx) {
		BatchUpdateContactSubmittedOrderCounts b = new BatchUpdateContactSubmittedOrderCounts();
		
		Database.executeBatch(b, 200);
	}
}