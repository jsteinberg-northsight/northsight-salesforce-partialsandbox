/**
 *  Purpose         :   This class is controller for ClientInvoicesGrid Visualforce page. 
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   1/22/2015
 *
 *  Current Version :   V1.1
 *
 *  Revision Log    :   V1.0 - Created - SM-269- Invoices: Client page
 *						V1.1 - Modified - Padmesh Soni (1/23/2014) - SM-269- Invoices: Client page
 *							Changes are:
 *							A) Add these columns to the invoice grid:
 *								1. Line Item Count (new field)
 *								2. Total (correctly formatted. $0.00)
 *								3. Loan Number - This is the loan number on the property object.
 *							B) Add Export to Excel on Invoices: Please duplicate the feature as it is on the ClientOrderGrid page. 
 *								This uses a custom object and a visualforce page to render table data as an excel sheet and it should be 
 *								very simple to implement for a new page.
 **/
global with sharing class ClientInvoicesGridController {
    
    //Class properties
    public string rInvoiceSerialized{
        
        get{        
            
            //Query result of User
            List<User> user = [SELECT Contact.AccountId FROM User WHERE Id=: UserInfo.getUserId() AND ContactId != null];
            
            //Check for size
            if(user.size() > 0) {
                
                List<InvoiceWrapper> recvInvoices = new List<InvoiceWrapper>();
                
                //Code modified - Padmesh Soni (1/23/2014) - SM-269- Invoices: Client page
                //New fields added into query  
                //Query result of Invoice sobject
                for(Invoice__c recvInvoice : [SELECT Id, Name, Work_Order__c, Work_Order__r.CaseNumber, Work_Order__r.Reference_Number__c, 
                                                Work_Order__r.Geocode_Cache__c, Work_Order__r.Geocode_Cache__r.Name,
                                                Work_Order__r.Display_Address__c,Work_Order__r.Display_Street__c,Work_Order__r.Display_City__c, Work_Order__r.Display_State__c,Work_Order__r.Display_Zip_Code__c, Work_Order__r.Work_Ordered__c, CreatedDate,
                                                Sent__c, Status__c, Line_Item_Count__c, Invoice_Total__c, Work_Order__r.Geocode_Cache__r.Loan_Number__c 
                                                FROM Invoice__c 
                                                WHERE RecordType.DeveloperName =: InvoicingConstants.INVOICE_RECORD_TYPE_DEVELOPERNAME_AR 
                                                AND Receive_From__c =: user[0].Contact.AccountId order by LastModifiedDate desc, Id desc LIMIT 3000]) {
                	
                	//populate wrapper list
                    recvInvoices.add(new InvoiceWrapper(recvInvoice));                                  
                }
                
                //Serialized the Sobject list into JSON string
                return JSON.serializePretty(recvInvoices);
            } else {
                
                //Serialized the Sobject list into JSON string
                return '';
            }
        } set;
    }
    
    //Code added - Padmesh Soni (1/23/2014) - SM-269- Invoices: Client page    
    public string HtmlDataId { get; set; }
    public string TheUserAddress { get; set; }
    public static Final integer CHUNK_SIZE = 30000;
    
    public String baseURL {
        get {
            
            return URL.getSalesforceBaseUrl().toExternalForm();
        }
        set;
    }
    
    //Constructor definition
    public ClientInvoicesGridController(){}
    
    /**					~~~~~~ Code added - Padmesh Soni (1/23/2014) - SM-269- Invoices: Client page ~~~~~~
     *  @descpriton     :   This method is used to create a string of Recieveable Invoices into a HTML Batch object's HTML Chunk field and 
     *						associate it with Listview HTML object.
     *
     *  @param          :   String invoiceTable
     *
     *  @return         :   Id of Listview HTML object
     **/
    @RemoteAction
    global static string SaveHtml(String invoiceTable) {
        
        try {
            
            //create parent record
            Listview_Html__c listViewObject = new Listview_Html__c();
            insert listViewObject;
            
            List<Html_Batch__c> batchList = new List<Html_Batch__c>();
            
            integer counter = 0;
            integer chunkSize = CHUNK_SIZE;
            string newChunk = '';
            
            //is html bigger than 30k?
            if (invoiceTable.length() > chunkSize) {
                
                //break the html into pieces of 30k
                while (invoiceTable.length() > chunkSize) {
                    
                    //get a chunk
                    newChunk = invoiceTable.substring(0, chunkSize);
                    
                    //remove chunk from HTML string
                    invoiceTable = invoiceTable.substring(chunkSize, invoiceTable.length());
                    
                    //add chunk
                    Html_Batch__c newBatch = new Html_Batch__c();
                    newBatch.Listview_html__c = listViewObject.Id;
                    newBatch.Html_Chunk__c = newChunk;
                    newBatch.Order__c = counter;  //keep order so we can put it back together
                    batchList.add(newBatch);
                    
                    counter++;
                }
                
                //any remaining data?
                if(invoiceTable.length() > 0) {
                    
                    Html_Batch__c newBatch = new Html_Batch__c();
                    newBatch.Listview_html__c = listViewObject.Id;
                    newBatch.Html_Chunk__c = invoiceTable;
                    
                    //keep order so we can put it back together
                    newBatch.Order__c = counter;
                    
                    batchList.add(newBatch);
                }        
            } else {
                
                //only need one chunk
                Html_Batch__c newBatch = new Html_Batch__c();
                newBatch.Listview_html__c = listViewObject.Id;
                newBatch.Html_Chunk__c = invoiceTable;
                
                //keep order so we can put it back together
                newBatch.Order__c = counter;  
                
                batchList.add(newBatch);
            }
            
            //insert chunks
            insert batchList;
            
            //return the parent Id
            return listViewObject.Id;
        } catch(exception e) {
            return Label.ORDER_GRID_SAVING_ERROR;
        }
    }
    
    /**				~~~~~~ Code added - Padmesh Soni (1/23/2014) - SM-269- Invoices: Client page ~~~~~~
     *  @descpriton     :   This method is used to return user to the Export Page.
     *
     *  @param          :
     *
     *  @return         :   PageReference
     **/
    public PageReference GoToExportPage() {
        
        //Creating PageReference instance of ExportToExcel page
        PageReference exportPage = new PageReference('/apex/ExportToExcel?id=' + HtmlDataId + '&filename=Invoice');
        return exportPage;
    }
    
    //inner class
    public class InvoiceWrapper {
    
        //Class properties
        public string id{get;set;}
        public string invoiceNumber{get;set;}
        public String created{get;set;}
        public string status{get;set;}
        public String sent{get;set;} 
        public string workOrder {get;set;}
        public string refNum {get;set;}
        public String displayAdd {get; set;}
        public String displayCity {get; set;}
        public String displayState {get; set;}
        public String displayZip {get; set;}
        public String workOrderd {get; set;}
        public String caseNumber {get; set;}
        public String property {get; set;}
        public String propName {get; set;}
        
        //Code added - Padmesh Soni (1/23/2014) - SM-269- Invoices: Client page
        //New variables to hold
        public String lineItemCount {get; set;}
        public String invTotal {get; set;}
        public String loanNumber {get; set;}
        
        //Constructor definition
        public InvoiceWrapper(Invoice__c inv){
    		
    		//assigning local to global variables
            id = inv.Id;
            created = inv.CreatedDate.date().format();
            invoiceNumber = inv.Name;
            status = inv.Status__c;
            workOrder = inv.Work_Order__c;
            caseNumber = inv.Work_Order__r.CaseNumber;
            property = inv.Work_Order__r.Geocode_Cache__c;
            propName = inv.Work_Order__r.Geocode_Cache__r.Name;
            if(inv.Sent__c != null)
                sent = inv.Sent__c.format();
            refNum = inv.Work_Order__r.Reference_Number__c;
            workOrderd = inv.Work_Order__r.Work_Ordered__c;
            displayAdd = inv.Work_Order__r.Display_Street__c;
            displayCity = inv.Work_Order__r.Display_City__c;
            displayState = inv.Work_Order__r.Display_State__c;
            displayZip = inv.Work_Order__r.Display_Zip_Code__c;
            
            
            //Code added - Padmesh Soni (1/23/2014) - SM-269- Invoices: Client page
            //New parameters for new changes
            invTotal = '$'+ String.valueOf(inv.Invoice_Total__c);
            lineItemCount = String.valueOf(inv.Line_Item_Count__c);
            loanNumber = inv.Work_Order__r.Geocode_Cache__r.Loan_Number__c;
        }
    }
    
    public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}