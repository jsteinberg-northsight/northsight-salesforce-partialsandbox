public with sharing class GeocodeCacheHelper {
	
	public static Set<Id> geoIds;
	public static boolean geosAlreadyUpdated;
	public static Map<Id, Geocode_Cache__c> geosToUpdate;
	public static Map<Id, Geocode_Cache__c> triggerNewMap; 
	public static void getTriggerNewMap(Map<Id, Geocode_Cache__c> newMap){
		triggerNewMap = newMap;
		//for(Geocode_Cache__c gc : triggerNewMap.values()){
		//	system.debug('TRIGGER NEW MAP!!!!!' + gc.Id);
		//}
	}
	
	public static boolean initialized = initialize();
	public static boolean initialize(){
		geosToUpdate = new Map<Id, Geocode_Cache__c>();
		geosAlreadyUpdated = false;
		
		return true;
	}
	
	public static void createInspectionWorkOrder(){
		Case[] newCases = new Case[]{};
		Set<Id> updateInspecFieldIds = new Set<Id>();
		RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name =:'INSP' limit 1];
		Map<Id,Geocode_Cache__c> propMap = new map<Id,Geocode_Cache__c>([SELECT Id, Create_Inspection__c, Last_Completed_Serviced_By__r.FirstName FROM Geocode_Cache__c WHERE Id IN :triggerNewMap.keySet()]);
		 
		for(Geocode_Cache__c gc : triggerNewMap.Values()){
			//check if Create_Inspection__c = true
			if(gc.Create_Inspection__c == true){
				//create new work order with recordType 'INSP'
				string lastCrewFirstName = propMap.get(gc.Id).Last_Completed_Serviced_By__r.FirstName;
				string approvalStatus = 'Not Started';
				if(gc.quarantined__c){
					approvalStatus = 'Quarantined';
				}
				if(gc.Flag__c){
					approvalStatus = 'Quarantined-Flagged';
				}
				Case c = new Case(
														Approval_Status__c = approvalStatus, 
														Last_Service_Date__c = gc.Last_Completed_Date_Serviced__c, 
														Due_Date__c = Date.Today() + 2, 
														Street_Address__c = gc.Street_Address__c, 
														State__c = gc.State__c, 
														City__c = gc.City__c, 
														Zip_Code__c = gc.Zip_Code__c, 
														Last_Crew_First_Name__c = lastCrewFirstName, 
														Last_Work_Order_ID__c = gc.Last_Completed_Work_Order_ID__c, 
														Last_Work_Ordered__c = gc.Last_Completed_Work_Ordered__c, 
														Last_Work_Order_Number__c = gc.Last_Completed_Client_Order_Number__c, 
														Client__c = gc.Client__c, 
														Customer__c = gc.Customer__c, 
														Loan_Type__c = gc.Loan_Type__c, 
														RecordTypeId = rt.Id, 
														Work_Ordered__c = 'QC Inspection',
														Work_Ordered_Text__c = 'QC Inspection', 
														Work_Order_Type__c = 'Inspections',
														Vendor_Code__c = 'FOLLOW', 
														Last_Crew_Name_Lookup__c = gc.Last_Completed_Serviced_By__c, 
														Date_Inspected__c = gc.Last_Inspected_Date__c,
														Last_Reference__c = gc.Last_Reference_Number__c,
														Geocode_Cache__c = gc.Id,
														Automatic_Reassignment__c = true);
				newCases.add(c);
				system.debug('Id added to updateInspecFieldIds');
				updateInspecFieldIds.add(gc.id); 
				
				/*
				-Record Type = INSP
				-Work Ordered = QC Inspection
				-Vendor Code = FOLLOW
				-Last Crew Name = Property.Last Completed Serviced By
				-Loan Type = Property.Loan Type
				-Customer = Property.Customer
				-Client = Property.Client
				-Last Work Order NUmber = Property.Last Completed Work Order Number
				-Last Work Order ID = Property.Last Completed Work Order ID
				-Last Work Ordered = Property.Last_Completed_Work_Ordered__c
				-Last Crew First Name = Property.[First Name of Property.Last Crew Name]
				*/
				gc.Create_Inspection__c = false;
			}
		}
		//insert work orders
		OrderAssignmentHelper.skipPropertyUpdate = true;
		insert newCases;
		OrderAssignmentHelper.skipPropertyUpdate = false;
	}
	
    public static void testbypass(){
        
        integer i=0;
        i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
        
    }
}