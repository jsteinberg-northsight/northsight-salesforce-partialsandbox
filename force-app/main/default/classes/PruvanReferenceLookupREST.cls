@RestResource(urlMapping='/PruvanReferenceLookupREST')
global without sharing class PruvanReferenceLookupREST {
		@HttpPost
	global static void doPost(){
		//get the request blob body and cast it into a string var
		RestRequest req = RestContext.request;
		String payload = req.requestBody.ToString();
		
		//create response string
		string errorMessage = '{"error":"The web service call has failed to hit the proper web service to initiate execution."}';
		string response = errorMessage;
		
		try{
				response = PruvanWebservices.doReferenceLookup(payload);
		}	
		catch(Exception e){
			response = '{"error":"'+e.getMessage()+'*****'+e.getStackTraceString()+'"}';
		}
		
		//set the response body = to a blob of response
		RestResponse res = RestContext.response;
		res.responseBody = Blob.valueOf(response);
		}
}