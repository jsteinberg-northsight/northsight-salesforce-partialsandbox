@isTest(seeAllData=false)
private class Test_BatchDeleteSurveyResponseLogs {
///**
// *  Purpose         :   This is used for testing and covering BatchDeleteSurveyResponseLogs. 
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   05/02/2014
// *
// *	Current Version	:	V1.0 - Padmesh Soni (05/02/2014) - Required Maintenance: Unit Test Improvements
// *
// *	Coverage		:	85%
// **/
// 	
// 	//Test method to test the functionality of Batch
//    static testMethod void testBatchDeleteSurveyResponseLogs() {
//        
//        //List to hold Pruvan Pruvan_SurveyResponse_Log__c Log records
//        List<Pruvan_SurveyResponse_Log__c> pruvanSurveyResLogs = new List<Pruvan_SurveyResponse_Log__c>();
//        pruvanSurveyResLogs.add(new Pruvan_SurveyResponse_Log__c(Name = 'Test Log 1', Log_Data__c = 'Testin is going on for Deletion of first record.'));
//        pruvanSurveyResLogs.add(new Pruvan_SurveyResponse_Log__c(Name = 'Test Log 2', Log_Data__c = 'Testin is going on for Deletion of Second record.'));
//        
//        //insert Pruvan Survay Response logs
//        insert pruvanSurveyResLogs;
//        
//        //Batch instance
//        BatchDeleteSurveyResponseLogs b = new BatchDeleteSurveyResponseLogs();
//		
//        //Test starts here
//        Test.startTest();
//        
//        //Execute batch here
//		Database.executeBatch(b, 200);
//		
//        //Test stops here
//        Test.stopTest();
//        
//        //Query result of Pruvan_SurveyResponse_Log__c object
//        pruvanSurveyResLogs = [SELECT Id FROM Pruvan_SurveyResponse_Log__c WHERE Id IN: pruvanSurveyResLogs];
//        
//        //assert statement here
//        System.assertEquals(0, pruvanSurveyResLogs.size());
//        
//    }
}