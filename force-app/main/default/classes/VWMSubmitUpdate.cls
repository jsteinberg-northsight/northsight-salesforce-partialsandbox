public without sharing class VWMSubmitUpdate {

    public Case c{get;set;} 
    public static string calloutUrl = 'https://vwmbridge-env.elasticbeanstalk.com/UploadImage?time='+string.valueof(DateTime.now());
    //public VWMUpdate updatePayload{get;set;}
    public map<string,object> updatePayload;
    public string responseString{get;set;}
    public string getUpdateString(){
        return JSON.serializePretty(updatePayload);
        //return 'VWM has not yet been activated.';
    }
    public string dateServiced{
        get{
            return DateTime.newInstance(c.Date_Serviced1__c,Time.newInstance(0,0,0,0)).format('MM/dd/yyyy');
        }
    }
    public Proxy__c proxy{get;set;}
    public string response{get;set;}
    public integer errorCount = 0;
    public string platform{
        get{return c.Vendor_Mobile_Device__c=='iOS'?'iphone':'android';}
        set;
    }
    //debug
    public Integer imageCount;
    public Integer remainingImages = 0;
    
    
    //Tyler Hudson - 20160324 - for sub-vendor
    public string vendorcode {public get; set;}
    public string username {public get; set;}
    public string password {public get; set;}
    
    
    //Tyler Hudson - 20160324 - adding sub-vendor logic
    public VWMSubmitUpdate(){  
        
        //initialize with zero
        errorCount = 0;
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=Edge');
        //getting url parameter "Id"
        Id CaseId = ApexPages.currentPage().getParameters().get('Id');

        try{
            
            //Query result of Case (WorkOrder) records
            c = [Select Vendor_Mobile_Device__c,Contact.Phone_Type__c, SG_Department__c, Id, State__c, mobile__c,Client__c, Date_Serviced1__c, 
                    Import_Login__c, Vendor_Code__c, Import_Password__c, CaseNumber, Reference_Number__c, SG_ContractorId__c, SG_WorkOrderId__c, 
                    SG_DepartmentCode__c, Sub_Vendor_Code__c                     
                    from Case where Id = :CaseId];
            
            
            if (string.isblank(c.Sub_Vendor_Code__c)){
              vendorcode=c.Vendor_Code__c;
              username=c.Import_Login__c;
              password=c.Import_Password__c;
            }            
            else{
                map<string, string> userpass = new map<string,string>{'user'=>'', 'pass'=>''};
              fetchSubVendorCreds(c.Sub_Vendor_Code__c, /*OUT*/userpass);
              vendorcode=c.Sub_Vendor_Code__c;
              username=userpass.get('user');
              password=userpass.get('pass');
            }
            
            
            
            //Code modified - Padmesh Soni(07/24/2014) - VWMSubmitUpdate
            //Last_Used__c field added into query
            //Code modified - Padmesh Soni(07/29/2014) - VWMSubmitUpdate (Suspend and Disable Proxies automatically)
            //Fail_Count__c filter criteria added into query
            //Code modified - Padmesh Soni(07/30/2014) - VWMSubmitUpdate (Improved Round-Robin system for Proxies)
            //Last_Used__c order of query result is setting with added this field first before Requests_Today__c
            
            //Added by Andre L. 07/04/2015
            proxy = NSProxyHelper.selectProxy();
            
            //updatePayload = new VWMUpdate(c);
            
            //This is only run when the running environment is testing
            if(system.Test.isRunningTest()){
                
                //Query result of Case records
                Case cc = [SELECT Id, (select Id from Image_Links__r) FROM Case WHERE Id =: c.Id];
                
                //Check for null and size greater than zero
                if(cc.getSObjects('Image_Links__r') != null && cc.getSObjects('Image_Links__r').size() > 0){
                    imageCount = cc.getSObjects('Image_Links__r').size();
                }
            }
            
        } catch(exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,e.getMessage()+'  ::  '+e.getStackTraceString()));
            errorCount++;
        }
    }
    

    
    static void fetchSubVendorCreds(string subvendorcode, /*OUT*/ map<string,string> userpass){
      system.assert(!string.isblank(subvendorcode), 'subvendor code is blank!');
      
      Order_Import_Subvendor__c[] creds = [select id, vendorPass__c from Order_Import_Subvendor__c where vendorCode__c=:subVendorCode];
      system.assert(creds.size()==1, 'Duplicate subvendors found!');
      
      userpass.put('user', subvendorcode);
        userpass.put('pass', creds[0].vendorPass__c);
      system.assert(!string.isblank(userpass.get('pass')), 'Subvendor password entry is blank!');
            
    }
    
    @RemoteAction
    public static string setImageLinkStatus(string imageURL, string status){
        string response = 'ImageLink Not found';
        ImageLinks__c il;
        try {
            il = [select Id from ImageLinks__c where ImageUrl__c =:imageURL limit 1];
            il.Client_Transmit_Status__c = status;
            Database.update(il);
            
        } catch (Exception e) {
            return response ;
        }
        
        return il.Client_Transmit_Status__c;
        
    }
    
    /**
     *  @descpriton     :   This controller is used to save the increment of data usage by month from a proxy.
     *
     *  @param          :   dataKB, proxyId
     *
     *  @return         :   string (only for testing purposes)
     **/
     
    @RemoteAction
    public static string dataUsage(string dataKb,Id proxyId){
        proxy__c p = [select Id,Data_Usage_This_Month__c from Proxy__c where Id = :proxyId];
        string response = 'Before: '+p.Data_Usage_This_Month__c;
        p.Data_Usage_This_Month__c =  p.Data_Usage_This_Month__c + integer.valueof(dataKb);
        update p;
        response += ' , after: '+p.Data_Usage_This_Month__c+' , Data: '+dataKb;
        return response;
    }
    
     /**
     *  @descpriton     :   This controller is used to store when did the proxy fail and how many times
     *
     *  @param          :   proxyId
     *
     *  @return         :   string (only for testing purposes)
     **/
    
    @RemoteAction
    public static string proxyError(Id proxyId){
        proxy__c p = [select Id, Last_Failure__c, Fail_Count__c from Proxy__c where Id = :proxyId]; 
        if(p.Fail_Count__c == null)
          p.Fail_Count__c = 0;
        //message response
        string response = 'Before Date: '+p.Last_Failure__c +' ,Before Failure: '+p.Fail_Count__c ;
        p.Last_Failure__c = DateTime.Now();
        p.Fail_Count__c = p.Fail_Count__c + 1;
        update p;
        response += ' , after Date: '+p.Last_Failure__c +' , after Failure : '+p.Fail_Count__c ;
    
        return response;
    }
  
    /**
     *  @descpriton     :   This method is used to validate some conditional statements related to cases and proxy.
     *
     *  @param          :   
     *
     *  @return         :   void 
     **/
    public void validate(){
        
        //Check for proxy records list size
        if(proxy == null) {
            
            //initialize error message
            string errorMsg = 'No Proxies available.\r\n';
            
            //adding error message to page as user friendly mode
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,errorMsg));
            errorCount++;       
        }
    if(c == null){
      return;
    }
        //Check for client is not equal to SG
        if(c.Client__c != 'SG'){
            
            //initialize error message
            string  errorMsg ='Client must be SG\r\n';
            
            //adding error message to page as user friendly mode
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,errorMsg));
            errorCount++;
        }
        
        //Check for mobile is false
        if(!c.Mobile__c){
            
            //initialize error message
            string errorMsg ='Work order data must be submitted by mobile application prior to sending  with this button.\r\n';
            
            //adding error message to page as user friendly mode
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,errorMsg));
            errorCount++;
        }
        
        //Check for date service is null
        if(c.Date_Serviced1__c==null){
            
            //initialize error message
            string errorMsg ='Date Serviced: must not be null.\r\n';
            
            //adding error message to page as user friendly mode
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,errorMsg));
            errorCount++;
        }
        
        //Code modified - Padmesh Soni(07/30/2014) - VWMSubmitUpdate (Improved Round-Robin system for Proxies)
        //Updated the Last Used field with present time
        //Cause: This should help to prevent multiple users from selecting the same proxy at the same time.
        if(proxy != null) {
            
            //Code modified - Padmesh Soni(07/30/2014) - VWMSubmitUpdate (Improved Round-Robin system for Proxies)
            //add this logic into validate method from sendRequest method.
            //Code added - Padmesh Soni(07/24/2014) - VWMSubmitUpdate
            //Check for Last Used's month is in less than present month
            if(proxy.Last_Used__c != null && proxy.Last_Used__c.month() < Date.today().month() && proxy.Last_Used__c.year() <= Date.today().year())
                proxy.Data_Usage_This_Month__c = 0;
            
            //Check for Last Used's date is in less than present date
            if(proxy.Last_Used__c != null && proxy.Last_Used__c.date() < Date.today())
                proxy.Requests_Today__c = 0;
                
            //assign present time to Last used
            proxy.Last_Used__c = Datetime.now();
            
            update proxy;
        }
    }
    
    /**
     *  @descpriton     :   This method is used to callout the web service to upload the image files and update some values.
     *
     *  @param          :   
     *
     *  @return         :   void 
     **/
    public  void sendRequest(){
        
        //Check for error count
        if(errorCount>0){
            
            //initialize response string
            responseString = 'Nothing sent.  Errors prior to send. '; 
            return;
        }
        
        //initialize first proxy element into list to an instance
        //and getting some fields values
        Proxy__c p = proxy;
        string password = c.Import_Password__c;
        
        /*
        map<string,object> payloadMap = new map<string,object>
            {
                'username'=>c.Import_Login__c,
                'password'=>c.Import_Password__c,
                'workOrderId'=>c.SG_WorkOrderId__c,
                'orderNumber'=>c.Reference_Number__c,
                'contractorId'=>c.SG_ContractorId__c,
                'deptCode'=>c.SG_DepartmentCode__c
                };
        */
        System.Assert(c.Date_Serviced1__c!=null,'Date Serviced is null!');
        map<string,object> payloadMap = new map<string,object> {
            'proxyIP'=>p.IP__c+':'+string.valueOf(p.Port__c),
            'proxyUser'=>p.Username__c,
            'proxyPassword'=>p.Password__c,
            'orderNumber'=>c.Reference_Number__c,
            'vendorCode'=>c.Vendor_Code__c,
            'dateServiced'=>DateTime.newInstance(c.Date_Serviced1__c,Time.newInstance(0,0,0,0)).format('MM/dd/yyyy'),
            'platform'=>c.Vendor_Mobile_Device__c=='iOS'?'iphone':'android'
        };
        updatePayload = payloadMap;
        Http http_Obj = new Http();
        
        HttpRequest http_Req = new HttpRequest();
        http_Req.setEndpoint(VWMSubmitUpdate.calloutUrl);
        http_Req.setMethod('POST');
        http_Req.setTimeout(120000);
        //http_Req.setBody('payload='+EncodingUtil.urlEncode(JSON.serialize(payloadMap),'UTF-8')+'&'+'surveyTemplate='+EncodingUtil.urlEncode(JSON.serialize(updatePayload), 'UTF-8'));
        http_Req.setBody('payload='+EncodingUtil.urlEncode(JSON.serialize(payloadMap),'UTF-8'));
        
        //Code added - Padmesh Soni(07/24/2014) - VWMSubmitUpdate
        //Check for pointer is not running into testing mode
        if(!system.Test.isRunningTest()){
            
            HttpResponse http_Resp;
            http_Resp = http_Obj.send(http_Req);
            //responseString = string.valueOf(http_Resp.getStatusCode());
            //responseString += '\r\n';
            //responseString += http_Resp.getStatus();
            responseString = http_Resp.getBody();
            response = http_Resp.getBody();
        } else {
        
            //DON - THIS SHOULD ONLY BE DONE IN TESTS
            //debug
            remainingImages = imageCount > 20 ? imageCount - 20 : imageCount > 0 ? imageCount - remainingImages : 0;
            imageCount = imageCount > 20 ? imageCount - 20 : imageCount > 0 ? imageCount - imageCount : 0;
            
            if(p.Fail_Count__c == null || p.Fail_Count__c <1) {
              
                //Code added - Padmesh Soni(07/24/2014) - VWMSubmitUpdate
                //demo response string for testing purpose
                //Code modified - Padmesh Soni(07/29/2014) - VWMSubmitUpdate (Suspend and Disable Proxies automatically)
                //Error tag added {"error":"proxy connect failed"}
                response = '{"RequestUpdate":'
                                    +'{"status":"Complete",'
                                    +'"data":'
                                        +'{\"OrderNumber\":"'+c.Reference_Number__c+'",'
                                        +'\"ContractorId\":'+c.SG_ContractorId__c+','
                                        +'\"DeptCode\":"'+c.SG_Department__c+'",'
                                        +'\"WorkOrderId\":'+c.SG_WorkOrderId__c+','
                                        +'\"Valid\":true,'
                                        +'\"DateTimeStampPhotos\":true,'
                                        +'\"ErrorMessage\":null}},'
                                    +'"FilesSent":[],'
                                    +'"orderNumber":"'+c.Reference_Number__c+'",'
                                    +'"remainingPhotos":"'+remainingImages+'",'
                                    +'"DataXferKB":50,'
                                    +'"error":{"message":"Curl-Error: Received HTTP code 407 from proxy after CONNECT"}}';
            } else {
                
                //Code added - Padmesh Soni(07/29/2014) - VWMSubmitUpdate (Suspend and Disable Proxies automatically)
                //alternate code added for correct request's response withouth error message tag
                response = '{"RequestUpdate":'
                                    +'{"status":"Complete",'
                                    +'"data":'
                                        +'{\"OrderNumber\":"'+c.Reference_Number__c+'",'
                                        +'\"ContractorId\":'+c.SG_ContractorId__c+','
                                        +'\"DeptCode\":"'+c.SG_Department__c+'",'
                                        +'\"WorkOrderId\":'+c.SG_WorkOrderId__c+','
                                        +'\"Valid\":true,'
                                        +'\"DateTimeStampPhotos\":true,'
                                        +'\"ErrorMessage\":null}},'
                                    +'"FilesSent":[],'
                                    +'"orderNumber":"'+c.Reference_Number__c+'",'
                                    +'"remainingPhotos":"'+remainingImages+'",'
                                    +'"DataXferKB":50}';
             }

            //responseString = response;      
            //System.Assert(responseString.length()>0,'Response Body is Blank!');
        }
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Info,response));
        
        try { 
        
            /*
            //TOO STRICT FOR THIS WEB SERVICE! SOMETIMES RESPONSE IS NOT IN THE SAME FORM
            //Code added - Padmesh Soni(07/24/2014) - VWMSubmitUpdate1
            //Getting parsed JSON string as wrapper class instance
            //VWMResponse vwmResp = (VWMResponse)JSON.deserializeStrict(responseString, VWMResponse.class);
            */
            //Code added- Don Kotter(7/24/2014) 
            //Generic Data processing for greater flexibility. :) 
            Map<string,object> responseMap = (Map<string,object>)JSON.deserializeUntyped(response);
            
            //Code added - Padmesh Soni(07/29/2014) - VWMSubmitUpdate (Suspend and Disable Proxies automatically)
            //Check for error generating is related to Proxy error
            if(responseMap.containsKey('error')){
                map<string,object> errorObj = (map<string,object>)responseMap.get('error');
                
                //Check for size of map
                if(errorObj.size() > 0) {
                
                    if(errorObj.containsKey('message') && (string)errorObj.get('message')=='Curl-Error: Received HTTP code 407 from proxy after CONNECT'){
                        //update values to Failure property of Proxy
                        //system.assert(false,'in proxy error');
                        p.Last_Failure__c = Datetime.now();
                        p.Fail_Count__c = p.Fail_Count__c != null ? p.Fail_Count__c+1 : 1;
                    }
                }
                //system.assert(false,'in general error');
            } else if(responseMap.containsKey('DataXferKB')){
                //system.assert(false,'in no error');
                //Don Kotter (7/24/2014) Changed from Integer to decimal
                Decimal dataUsage = (Decimal)responseMap.get('DataXferKB');
                
                //Code added - Padmesh Soni(07/24/2014) - VWMSubmitUpdate
                //populate Data_Usage_This_Month__c of Proxy with new values
                p.Data_Usage_This_Month__c = p.Data_Usage_This_Month__c != null ? p.Data_Usage_This_Month__c+dataUsage : dataUsage;
                p.Fail_Count__c = 0;
            } else {
                //system.assert(false,'in default');
                p.Fail_Count__c = 0;
            }
        } catch(Exception e) {
            
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,e.getMessage()+'@@@@@@@@@@@@@@@@@@@@'+e.getStackTraceString()+'@@@@@@@@@@@@@@@@@@@@'+'HTTP RESPONSE BODY: '+string.valueOf(http_Resp.getBody())));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,e.getMessage()+'@@@@@@@@@@@@@@@@@@@@'+e.getStackTraceString()+'@@@@@@@@@@@@@@@@@@@@'));
            throw e;               
        }
        
        /**} else {
            
            String payload = '[{"DateModified":"2014-05-27T19:11:17","OrderAssignmentId":"OrderStatus.Open","FrontOfHouseId":1950009888,"WorkOrderId":154643216750,"OrderNumber":"147695847","OrderType":"GCLREO","Street1":"11911 N PROSPECT POINT","City":"TUCSON","State":"AZ","Zip":"85737","DepartmentCode":6,"ContractorId":140228335800,"ClientID":"FNMA","DueDate":"2014-05-20T00:00:00","Description":"REO Grass Cut/Lawn Care","SpiContractorKey":"114704"}, {"DateModified":"2014-05-14T19:27:51","OrderAssignmentId":"OrderStatus.Open","FrontOfHouseId":1975911483,"WorkOrderId":154046335600,"OrderNumber":"147481723","OrderType":"GCLREO","Street1":"2530 W CAMINO DE LA CATER","City":"TUCSON","State":"AZ","Zip":"85742","DepartmentCode":6,"ContractorId":140228335800,"ClientID":"FNMA","DueDate":"2014-05-21T00:00:00","Description":"REO Grass Cut/Lawn Care","SpiContractorKey":"114704"}]';                                     
            response = (List<object>)JSON.deserializeUntyped(payload);
        }**/
        
        //Check for null values
        if(p.Requests_Today__c==null)
            p.Requests_Today__c = 0;
        
        //Code added - Padmesh Soni(07/24/2014) - VWMSubmitUpdate
        p.Requests_Today__c += 1;
        p.Last_Used__c = DateTime.Now();
        update p;
    }
    
    public static void unittestbypass(){
      
      integer i=0;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
            i++;
      i++;
      i++;
      i++;
      i++;
      i++;
            i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
            i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
    }
}