@isTest(seeAllData=false)
private class Test_SearchUserLookupController {
///**
// *  Purpose         :   This is used for testing and covering SearchUserLookupController.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   02/24/2014
// *
// *	Current Version	:	V1.0
// *
// *	Coverage		:	100%
// **/
// 	
// 	private static List<Account> accounts;
//	private static List<Contact> contacts;
//	private static Profile newProfile;
//	private static List<User> users;
//	private static Group queues;
//	
//	private static void generateTestData(integer countData){
//		
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP 
//		//create a new user with the Customer Portal Manager Standard profile
//		newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Clients'];
//		
//		System.runAs ( new User(Id = UserInfo.getUserId()) ) {
//			//create a new user with the profile
//			queues = new Group(Name='Queue',Type='Queue');
//			insert queues;
//			
//			//Queue sObject   
//			QueueSobject queueObj = new QueueSobject(QueueId = queues.Id, SobjectType = 'Case');
//			insert queueObj;
//		}
//		//Loop through countData time
//		for(integer i = 1; i <= countData; i++) {
//		
//			/******** PRE-TEST SETUP create a new account *********/
//			accounts.add(new Account(Name = 'Tester Account' + i,RecordTypeId = '01240000000UT5T'));
//		}
//		
//		insert accounts;
//		
//		//Loop through countData time
//		for(integer i = 1; i <= countData; i++) {
//		
//			//--------------------------------------------------------
//	   		//PRE-TEST SETUP
//			//create a new contact
//			contacts.add(new Contact(LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active', RecordTypeId = '01240000000UT5d'));
//		}	
//		
//		insert contacts;
//		
//		//Loop through countData time
//		for(integer i = 1; i <= countData; i++) {
//		
//			//create a new user with the profile
//			users.add(new User(alias = 'standt'+i, contactId = contacts[i-1].Id, email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8',
//									lastname = 'Testing'+i, languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//									timezonesidkey = 'America/Los_Angeles', username = 'NS_stdusr'+i+'@NStest.test', isActive = true));
//		}
//		
//		insert users;
//	}
//	
// 	//Test method is used to cleaning Attachments on Cases
//    static testMethod void testSearchUserLookup() {
//    	
//    	//initialize variables
//    	accounts = new List<Account>();
//		contacts = new List<Contact>();
//		users = new List<User>();
//		
//		generateTestData(5);
//		    	
//    	//search element
//    	String srch = '*esting';
//    	String lktype = '005';
//    	
//        //Strat Test here
//        Test.startTest();
//        
//        //First use case with 1 already choosed and serach element given
//        ApexPages.currentPage().getParameters().put('lksrch',srch);
//        ApexPages.currentPage().getParameters().put('lktp',lktype);
//        
//		//call the constructor of SearchAssetSerialNController 
//        SearchUserLookupController controller = new SearchUserLookupController();
//        System.assertEquals(5, controller.result.size());
//        
//        //search element
//    	srch = '*Don';
//    	
//    	//First use case with 1 already choosed and serach element given
//        ApexPages.currentPage().getParameters().put('lksrch',srch);
//		
//        //call the constructor of SearchAssetSerialNController
//        controller = new SearchUserLookupController();
//        System.assertEquals(0, controller.result.size());
// 		
// 		srch = '*';
// 		lktype = 'case_queue';
// 		
// 		//First use case with 1 already choosed and serach element given
//        ApexPages.currentPage().getParameters().put('lksrch',srch);
//        ApexPages.currentPage().getParameters().put('lktp',lktype);
//        
//		//call the constructor of SearchAssetSerialNController 
//        controller = new SearchUserLookupController();
//        System.assert(controller.queueResult.size() > 0);
//        
//        srch = '*';
// 		lktype = 'test';
// 		
// 		//First use case with 1 already choosed and serach element given
//        ApexPages.currentPage().getParameters().put('lksrch',srch);
//        ApexPages.currentPage().getParameters().put('lktp',lktype);
//        
//		//call the constructor of SearchAssetSerialNController 
//        controller = new SearchUserLookupController();
//        System.assert(controller.queueResult==null);
//        //Stop Test here
//        Test.stopTest();
//    }
}