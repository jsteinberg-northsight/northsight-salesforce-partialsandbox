public without sharing class ImageLinksHelper {
    public static List<ImageLinks__c> imageTriggerList;
    public static Set<Id> caseIDs;
    public static Map<Id, List<List<Datetime>>> timeMap;
    public static List<List<Datetime>> dateTimeLists;
    public static List<Datetime> timeList;
    public static Integer currentDay;
    public static Datetime minTime;
    public static Datetime maxTime;
    public static Integer minutesOnProperty;
    public static Integer totalMinutesOnProperty;
    public static List<Case> selectedCases;
    public static List<Geocode_Cache__c> selectedProperties;
    //public static Map<Id,Geocode_Cache__c> geoMap;
    public static Map<Id, Case> casesToUpdate;
    
    //this method instantiates certain variables
    public static void initialize(List<ImageLinks__c> triggerNew){
        imageTriggerList = triggerNew != null ? triggerNew : new List<ImageLinks__c>();
        caseIDs = new Set<Id>();
        timeMap = new Map<Id, List<List<Datetime>>>();
        dateTimeLists = new List<List<Datetime>>();
        timeList = new List<Datetime>();
        casesToUpdate = new Map<Id, Case>();
        setCaseIDs();
        selectedCases = [SELECT Id, Minutes_At_Property__c, Request_Reassignment__c, All_Images_Received__c, Geocode_Cache__c, Location_GPS_Verified__c, (SELECT Id, GPS_Timestamp__c, ImageUrl__c, Distance_from_Property_in_Ft__c, GPS_Location__Latitude__s, GPS_Location__Longitude__s, Pruvan_gpsAccuracy__c FROM Image_Links__r WHERE Pruvan_fileType__c != 'csr' AND Pruvan_uuid__c != null ORDER BY GPS_Timestamp__c ASC) FROM Case WHERE Id IN: caseIDs for update];
        selectedProperties = [SELECT Id, SqFt_of_Lot_Size__c FROM Geocode_Cache__c WHERE Id IN (SELECT Geocode_Cache__c FROM Case WHERE Id IN: caseIDs)];
        //geoMap = new Map<Id,Geocode_Cache__c>();
    }
    
    //image link wrapper class used to implement custom comparable method
    public class imageLinkWrapper implements Comparable{
        public ImageLinks__c iL{get; set;}
        
        public imageLinkWrapper(ImageLinks__c imageLink){
            iL = imageLink;
        }
        //custom compare method to compare image link pruvan gps accuracy data
        public integer CompareTo(Object compareTo){
            imageLinkWrapper otherIL = (imageLinkWrapper)compareTo;
            
            if((decimal)this.iL.get('Pruvan_gpsAccuracy__c') > (decimal)otherIL.iL.get('Pruvan_gpsAccuracy__c')) return 1;
            
            if((decimal)this.iL.get('Pruvan_gpsAccuracy__c') < (decimal)otherIL.iL.get('Pruvan_gpsAccuracy__c')) return -1;
            
            return 0;
        }
    }
    
    //this method will fill the caseIDs set with case IDs on the image links in the imageTriggerList
    private static void setCaseIDs(){
        if(imageTriggerList != null && imageTriggerList.size() > 0){
            for(ImageLinks__c ilTrigger : imageTriggerList){
                caseIDs.add(ilTrigger.CaseId__c);
            }
        }
    }
    
    //this method will be a map of id to a list of lists
        //the key is the case id
        //the list of lists contains groupings of datetime timestamps from the image links associated with the case
    private static void buildTimeMap(){
        //select the image link's gps timestamp in a sub select on cases that have an ID in the caseIDs set
        if(selectedCases != null && selectedCases.size() > 0){
            for(Case c : selectedCases){
                //check if the current case's ID is already existing as a key in the timeMap, and if not add it to the timeMap while instantiating a new list of datetime lists
                if(!timeMap.containsKey(c.Id)){
                    timeMap.put(c.Id, new List<List<Datetime>>());
                }
                
                //add the timeList to the dateTimeLists
                dateTimeLists.add(timeList);
                //loop the the image links in the sub select of the select in the outermost for loop
                if(c.getSObjects('Image_Links__r') != null && c.getSObjects('Image_Links__r').size() > 0){
                    for(ImageLinks__c ilTimestamps : c.getSObjects('Image_Links__r')){
                        if(ilTimestamps.GPS_Timestamp__c != null){
                            //instantiate currentDay if it hasn't already been
                            if(currentDay == null){
                                currentDay = ilTimestamps.GPS_Timestamp__c.day();
                            }
                            //if the current ilTimestamps object has the same day as currentDay, then add it to the timeList
                            if(ilTimestamps.GPS_Timestamp__c.dayGmt() == currentDay){
                                timeList.add(ilTimestamps.GPS_Timestamp__c);
                            }
                            else{
                                //reset the currentDay value if the current image link's gps timestamp value is a day after the current value of currentDay
                                currentDay = ilTimestamps.GPS_Timestamp__c.day();
                                //instantiate a new timeList
                                timeList = new List<Datetime>();
                                //add the current timestamp to the new list
                                timeList.add(ilTimestamps.GPS_Timestamp__c);
                                //add the new timeList to the dateTimeLists
                                dateTimeLists.add(timeList);
                            }
                        }
                    }
                }
                //loop through the dateTimeLists selecting each list and sorting it
                if(dateTimeLists != null && dateTimeLists.size() > 0){
                    for(List<Datetime> listOfTimes : dateTimeLists){
                        listOfTimes.sort();
                    }
                }
                //add dateTimeLIsts to the timeMap
                timeMap.put(c.Id, dateTimeLists);
            }
        }
    }
    
    //this method will perform the calculations using the image link timestamps
        //the calculations will show how many minutes a vendor was at a property
        //first it will build the timeMap
        //second it will go thru the timeMap getting each list from the list of lists and pulling out the min time and max time timestamps
            //we use the min & max time to find the difference between the time from when the first photo was taken and when the last photo was taken
            //this will give us the minutes a vendor was at a property
    public static void setMinutesOnProperty(){
        //build the timeMap
        buildTimeMap();
        //set the totalMinutesOnProperty = 0
        totalMinutesOnProperty = 0;
        //select cases to update if casesToUpdate has cases in it
        if(selectedCases != null && selectedCases.size() > 0){
            for(Case cs : selectedCases){
                //loop through the timeMap and get the min and max times for each day in each list
                if(timeMap != null && timeMap.size() > 0){
                    for(List<Datetime> dt : timeMap.get(cs.Id)){
                        if(dt != null && dt.size() > 0){
                            //get the min time
                            minTime = dt[0];
                            //system.debug('*****minTime: ' + minTime);
                            //get the max time
                            maxTime = dt[dt.size() - 1];
                            //system.debug('*****maxTime: ' + maxTime);
                            //calculate the minutes on property using (Integer)(maxTime.getTime() - minTime.getTime()) / 60000
                            minutesOnProperty = (Integer)(maxTime.getTime() - minTime.getTime()) / 60000;
                            //system.debug('*****minutesOnProperty: ' + minutesOnProperty);
                            //add the minutes to the total minutes
                            totalMinutesOnProperty += minutesOnProperty;
                            //system.debug('*****totalMinutesOnProperty: ' + totalMinutesOnProperty);
                        }
                    }
                }
                //update the current case's Minutes_At_Property__c field
                if(cs.Minutes_At_Property__c != totalMinutesOnProperty){
                    cs.Minutes_At_Property__c = totalMinutesOnProperty;
                    casesToUpdate.put(cs.Id, cs);
                }
            }
        }
    }
    
    //use this method to set the approval status of the work orders if there are image links for those work orders and
        //all image links on a work order have a View__c and the Mobile__c flag on a work order is set to true
    public static void setAllImagesReceivedFlag(){
        //Boolean setStatus = true;//flag that indicates the approval status of a work order should be set to 'Pre-Pending'
        
        if(selectedCases != null && selectedCases.size() > 0){//if there are work orders to be updated then...
            for(Case c : selectedCases){
                c.All_Images_Received__c = true;//set all images received to true
                if(c.getSObjects('Image_Links__r') != null){    
                    for(ImageLinks__c iL:c.getSObjects('Image_Links__r')){
                        if(iL.ImageUrl__c == null || iL.ImageUrl__c == ''){//if even one image link does not have a URL, then the work order's approval status is not set
                            //setStatus = false;
                            c.All_Images_Received__c = false;//set all images received to false
                        }
                    }
                }
                casesToUpdate.put(c.Id, c);
            }
        }
    }
    
    //use this method to set the Location_GPS_Verified field on the case object
        //we set this field yes/no by determining if the vendor serviced the correct property
        //using the photo geolocation and the property geolocation
    public static void verifyLocationGPS(){
        string verified = '';
        //map of id to property, use for keeping track of properties
        Map<Id,Geocode_Cache__c> geoMap = new Map<Id,Geocode_Cache__c>();
        if(selectedProperties != null && selectedProperties.size() > 0){
            for(Geocode_Cache__c gc : selectedProperties){
                if(!geoMap.containsKey(gc.Id)){
                    geoMap.put(gc.Id, gc);
                    //System.Debug('Geocode ADDED:: '+gc.Id);
                }
            }
        }
        
        //map of id to list of image links, use to keep track of lists of image links
        Map<Id,List<ImageLinks__c>> imageListMap = new Map<Id,List<ImageLinks__c>>();
        if(selectedCases != null && selectedCases.size() > 0){
            //get the image links for the current case
            for(Case c : selectedCases){
                if(c.getSObjects('Image_Links__r') != null && c.getSObjects('Image_Links__r').size() > 0){
                    verified = 'Yes';//var for setting the case location_gps_verified field
                    
                    //integer position = c.getSObjects('Image_Links__r').size();//get the size of the current image links related list
                    
                    if(!imageListMap.containsKey(c.Id)){//if c.Id is not a key in the imageListMap
                        imageListMap.put(c.Id, new List<ImageLinks__c>());//add the case id with a new list of image links to imageListMap 
                    }
                    
                    List<imageLinkWrapper> tmpList = new List<imageLinkWrapper>();//create an imageLinkWrapper list to sort the image links
                    for(ImageLinks__c iL : c.getSObjects('Image_Links__r')){
                        if(iL.Pruvan_gpsAccuracy__c != null && iL.Pruvan_gpsAccuracy__c >= 0){
                            imageLinkWrapper img = new imageLinkWrapper(iL);//make a new imageLinkWrapper object
                            tmpList.add(img);//add the imageLinkWrapper object to the tmpList
                        }
                    }
                    //check if there are images in the tmpList, if not verified should be 'No'
                    if(tmpList == null || tmpList.size() <= 0){
                        verified = 'No';
                    }
                    tmpList.sort();//sort the list of imageLinkWrapper objects
                    ////debug
                    //system.debug('The sorted list of Image Links: ' + tmpList);
                    
                    integer counter = 1;//set a counter var to 1
                    for(integer i = 0; i < tmpList.size(); i++){//loop through the list of imageLinkWrapper objects
                        if(counter > 5) continue;//if the counter var is greater than 5, continue to next iteration
                        
                        imageListMap.get(c.Id).add(tmpList[i].iL);//add image links to the list of image links at the current key in the imageListMap
                        
                        //position--;//sub 1 from the position var
                        counter++;//add 1 to the counter var
                    }
                }
                
                Geocode_Cache__c tmpCache = geoMap.get(c.Geocode_Cache__c);//get the current property for the current case
                List<ImageLinks__c> iLList = imageListMap.get(c.Id) != null ? imageListMap.get(c.Id) : new List<ImageLinks__c>();//get the list of image links at the current case in the imageListMap
                if(iLList!= null && iLList.size() > 0){
                    //The execution of a trigger will sometimes generate an error message like the following: NullPointerException. Needs previous validation of the objects
                    if(tmpCache != null)
                    { 
                        if(tmpCache.sqft_of_lot_size__c != null){//if the property has a lot size, and it is greater than 0
                            if(tmpCache.sqft_of_lot_size__c > 0){
                                //List<ImageLinks__c> tmpList = imageListMap.get(c.Id);//get the list of image links at the current case in the imageListMap
                                decimal max_distance = 200 + (0.15 * (tmpCache.sqft_of_lot_size__c));//set the max_distance using the lot size of the property
                                for(ImageLinks__c iL : iLList){//loop thru the image links in the list
                                    ////debug
                                    //system.debug('Current Image Links Distance From Property: ' + iL.Distance_from_Property_in_Ft__c);
                                    
                                    if(iL.Distance_from_Property_in_Ft__c > max_distance){//if any image links have a distance from the property value greater than max_distance
                                        verified = 'No';//set the location_gps_verified field to 'No'
                                    }
                                }
                            }
                        } else {//since the property does not have a lot size
                            //List<ImageLinks__c> tmpList = imageListMap.get(c.Id);//get the list of image links at the current case in the imageListMap
                            for(ImageLinks__c iL : iLList){//loop thru the image links in the list
                                if(iL.Distance_from_Property_in_Ft__c > 650){//if the distance from the property is greater than 650
                                    verified = 'No';//set the location_gps_verified field to 'No'
                                }
                            }
                        }
                    }
                }
                
                if(!casesToUpdate.containsKey(c.Id)){//check to see if the casesToUpdate map has the current case Id as a key
                    casesToUpdate.put(c.Id, c);//if not, add the current case to the map
                }
                Case tmpCase = casesToUpdate.get(c.Id);//get the current case from the casesToUpdate map
                tmpCase.Location_GPS_Verified__c = verified;//make sure the location_gps_verified is set as the value that was just set in this function
            }
        }
    }
    
    //perform DML operations here
    public static void finalize(){
        //update the cases that have had their Minutes_At_Property__c fields changed
        if(casesToUpdate != null && casesToUpdate.size() > 0){
            database.update(casesToUpdate.values(),false);
        }
    }
    
    //
    public static void updateProperty(List<ImageLinks__c> triggerNewList){
       
       Set<String> casedId = new Set<String>();
       for(ImageLinks__c img : triggerNewList) {
              casedId.add(img.caseId__c);
       }
       
       //Map<Id,Case> caseMap = new Map<Id,Case> ([Select Id ,Geocode_Cache__c From Case WHERE Id in : casedId]);
       //for(ImageLinks__c img : triggerNewList) {
       // if(caseMap.get(img.caseId__c).Geocode_Cache__c != null)
       //  img.property__c = caseMap.get(img.caseId__c).Geocode_Cache__c;
       //}
       
    }
    
}