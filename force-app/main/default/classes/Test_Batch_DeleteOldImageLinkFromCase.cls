@isTest(seeAllData=false)
private class Test_Batch_DeleteOldImageLinkFromCase {
//
///**
// *  Purpose         :   This is used for testing and covering Batch_DeleteOldImageLinkFromCase class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/06/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Padmesh Soni(06/10/2014) - Tasks 06/09/2014
// *	
// *	Coverage		:	88%
// **/
// 	
//    //test method added to check the functionality with code coverage
//    static testMethod void myUnitTest() {
//        
//		//Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	RecordType caseRecordType = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'MISC' AND SobjectType = 'CASE' 
//    										AND IsActive = true ORDER BY DeveloperName][0];
//        
//        RecordType accountRecordType = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true][0];
//        
//
//    	
//    	//List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = accountRecordType.Id));
//    	accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = accountRecordType.Id));
//    	
//    	insert accounts;
//		
//		Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//		insert property;
//		
//
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordType.Id);
//		insert account; 
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(
//    							Geocode_Cache__c = property.Id, 
//    							RecordTypeId = caseRecordType.Id, 
//    							AccountId = accounts[0].Id, 
//    							Due_Date__c = Date.today(), 
//    							Status = 'Closed',
//    							Street_Address__c = '301 Front St',
//    							City__c = 'Nome',
//    							State__c = 'AK',
//    							Zip_Code__c = '99762',
//    							Client__c = 'TestClient',
//    							Vendor_Code__c = 'TestCode',
//    							Work_Ordered__c = 'Snow Removal',
//    							Last_Open_Verification__c = Date.today(),
//    							Approval_Status__c = 'Approved',
//    							Work_Completed__c = 'Yes',
//    							Date_Serviced__c = Date.today(),Client_Name__c = account.Id
//    							));
//    	workOrders.add(new Case(
//    							Geocode_Cache__c = property.Id, 
//    							RecordTypeId = caseRecordType.Id, 
//    							AccountId = accounts[0].Id, 
//    							Due_Date__c = Date.today().addDays(1), 
//    							Status = 'Closed',
//    							Street_Address__c = '302 Front St',
//    							City__c = 'Nome',
//    							State__c = 'AK',
//    							Zip_Code__c = '99762',
//    							Client__c = 'TestClient',
//    							Vendor_Code__c = 'TestCode',
//    							Work_Ordered__c = 'Snow Removal',
//    							Last_Open_Verification__c = Date.today(),
//    							Approval_Status__c = 'Approved',
//    							Work_Completed__c = 'Yes',
//    							Date_Serviced__c = Date.today(),Client_Name__c = account.Id
//    							));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//    	//List to hold image Link records
//    	List<ImageLinks__c> imageLinks = new List<ImageLinks__c>();
//    	for(integer i = 0; i < 10; i++) {
//	    	imageLinks.add(new ImageLinks__c(Pruvan_evidenceType__c= 'Survey', GPS_Timestamp__c= DateTime.now(), 
//	    										ImageUrl__c='https://s3.amazonaws.com/NorthsightTmp/02570223/52820869.jpg', CaseId__c= workOrders[0].Id, 
//	    										GPS_Location__Latitude__s = 30.087567, GPS_Location__Longitude__s=-90.485625, Pruvan_gpsAccuracy__c=2531, 
//	    										Survey_Question_Id__c='Trip_Charge_Reason__c', Survey_Id__c='GC23_001-v5', 
//	    										Notes__c ='This is a picture of the lawn.'+ i, Is_Summarized__c = true));
//	    	imageLinks.add(new ImageLinks__c(Pruvan_evidenceType__c= 'Survey', GPS_Timestamp__c= DateTime.now(), 
//	    										ImageUrl__c='https://s3.amazonaws.com/NorthsightTmp/02570223/52820869.jpg', CaseId__c= workOrders[1].Id, 
//	    										GPS_Location__Latitude__s = 30.087567, GPS_Location__Longitude__s=-90.485625, Pruvan_gpsAccuracy__c=2531, 
//	    										Survey_Question_Id__c='Trip_Charge_Reason__c', Survey_Id__c='GC23_001-v5', 
//	    										Notes__c = i + 'This is a picture of the lawn.', Is_Summarized__c = true));
//    	}
//    	
//    	//insert Image Links here
//    	insert imageLinks;
//    	
//    	//Test starts here
//    	Test.startTest();
//    	
//    	//Batch instance
//        Batch_DeleteOldImageLinkFromCase batchJob = new Batch_DeleteOldImageLinkFromCase();
//        
//        //Batch executes here
//        Database.executeBatch(batchJob, 200);
//        
//    	//Test stops here
//    	Test.stopTest();
//    	
//    	//Query result
//    	imageLinks = [SELECT Id FROM ImageLinks__c];
//    	
//    	//assert statement here
//    	System.assertEquals(0, imageLinks.size());
//    }
}