/**
 *	Purpose			:	This is a service class to encapsulate all needed Twilio api calls. 
 *
 *	Created By		:	Padmesh Soni
 *
 *	Created Date	:	01/01/2014
 *
 *	Current Version	:	V1.0
 *
 *	Revision Log	:	V1.0 - Created  
 **/
global class TwilioService {

	//String to hold VERSION
    private static final String VERSION = '3.2.0';

    //String to hold endpoint
    private String endpoint = 'https://api.twilio.com';

    //String to hold DEFAULT_VERSION
    public static final String DEFAULT_VERSION = '2010-04-01';

    //String to hold account sid
    private String accountSid;

    //String to hold auth token
    private String authToken;
    
    //String to hold From Phone Number
    public String phoneNumber;

    //Constructor. It gathers the Twilio Configuration for Account SID and Authorization Token 
	public TwilioService() {
    	
    	//getting custom setting from org of Twilio API 
		Twilio_Configuration__c config = getTwilioConfiguration();
        
        //get values and assign to class variables
		accountSid = config.Account_SID__c;
		authToken = config.authToken__c;
        
        //Check if it is related to test methods
		if(Test.isRunningTest()) {
		
			//get values and assign to class variables
			//accountSid = config.testAccountSID__c;
			//authToken = config.testAuthToken__c;
		}
        
		//get values and assign to class variables
		phoneNumber = config.From_Phone_Number__c;
    }
    
    /**
	 * 	@description	:	Function to Send SMS Messages
	 * 
	 * 	@args			:	map of params
	 * 
	 * 	@return			:	SMSResponse
	 **/
    public SMSResponse sendSMS(Map<String, String> params) {
    	
    	params.put('From', phoneNumber);
    	
    	//Create request with map of params
    	HttpRequest request = generatePostRequest(params);
    	
    	//create request to Twilio and getting response
    	String responseBody = makeHTTPsRequest(request);
    	
    	//System.debug('responseBody :::::'+ responseBody);
    	
    	// Read entire invoice object, including its array of line items.
        SMSResponse smsRes = (SMSResponse)JSON.deserialize(responseBody, SMSResponse.class);
        return smsRes;
    }

    /**
	 * 	@description	:	Method to generate post request.
	 * 
	 * 	@args			:	map of params
	 * 
	 * 	@return			:	HttpRequest
	 **/
    public HttpRequest generatePostRequest(Map<String,String> params) {
        
        String path = getResourceLocation();
        
        URL uri = new URL(path);

        String entity = buildEntityBody(params);
		
		HttpRequest request = new HttpRequest();
		
		request.setHeader('X-Twilio-Client', 'salesforce-' + VERSION);
        request.setHeader('User-Agent', 'twilio-salesforce/' + VERSION);
        request.setHeader('Accept', 'application/json');
        request.setHeader('Accept-Charset', 'utf-8');
        request.setHeader('Authorization', 'Basic '+EncodingUtil.base64Encode(Blob.valueOf(this.accountSid + ':' + this.authToken)));
        request.setEndpoint(uri.toExternalForm());
        request.setMethod('POST');
        request.setBody(entity);

        return request;
    }
    
    /**
	 * 	@description	:	Method to Builds the entity body.
	 * 
	 * 	@args			:	map of params
	 * 
	 * 	@return			:	String(url encoded form entity)
	 **/
    private String buildEntityBody(Map<String,String> params) {
        String entityBody='';
        if (params != null) {
            for (String key : params.keySet()) {
                try {
                    entityBody += (entityBody=='' ? '' : '&')
                                    + key + '=' + EncodingUtil.urlEncode(params.get(key), 'UTF-8');
                } catch (Exception e) {
                    System.debug('ERROR: ' + e);
                }
            }
        }
        return entityBody;
    }
    
    /**
	 * 	@description	:	Method to make the webservice callout.
	 * 
	 * 	@args			:	HttpRequest
	 * 
	 * 	@return			:	String(url encoded form entity)
	 **/
	public String makeHTTPsRequest(HttpRequest request) {
		
		//instance of HTTP
    	Http h = new Http();
        
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = request;
        
        String responseBody = '';
        
        if(!Test.isRunningTest()) {
        	HttpResponse res = h.send(request);
        	responseBody = res.getBody();
        }
        else {
        	responseBody = '{"sid":"SMba4ab2fafa3d709004359818f50a67eb","date_created":"Thu, 12 Dec 2013 15:13:06 +0000","date_updated":"Thu, 12 Dec 2013 15:13:06 +0000","date_sent":null,"account_sid":"ACfdd0fcec687020b86d09fa8451c4b614","to":"+919820242561","from":"+13047132065","body":"Hello there!","status":"queued","direction":"outbound-api","api_version":"2010-04-01","price":-0.01,"price_unit":"USD","uri":"/2010-04-01/Accounts/ACfdd0fcec687020b86d09fa8451c4b614/SMS/Messages/SMba4ab2fafa3d709004359818f50a67eb.json","num_segments":"1"}';
        }
        
        return responseBody;
    }
    
    /**
	 * 	@description	:	Method to get the Path for sending SMS.
	 * 
	 * 	@args			:	HttpRequest
	 * 
	 * 	@return			:	String(url encoded form entity)
	 **/
    public String getResourceLocation() {
		return endpoint + '/' + DEFAULT_VERSION + '/Accounts/'
				+ this.accountSid + '/SMS/Messages.json';
	}
	
	/**
	 * 	@description	:	Method to get Custom Setting Value
	 * 
	 * 	@args			:	HttpRequest
	 * 
	 * 	@return			:	String(url encoded form entity)
	 **/
    private Twilio_Configuration__c getTwilioConfiguration() {
		return Twilio_Configuration__c.getInstance();
	}
	
	//inner class SMSResponse
	public class SMSResponse {
		public String status;
	}
}