@isTest(seeAllData=false)
//Mock Class implement the interface
global class ZillowControllerMockClass implements HttpCalloutMock {
/**
 *  Purpose         :   Mock class for Zillow GetDeepSearchResult API request. 
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   12/12/2013
 *
 *  Current Version :   V1.0
 *
 *  Revision Log    :   V1.0(Created)
 **/
 
	global static integer noOfResponse = 0;
	
	global static String getResponseBody() {
		
		//variable to hold response body
		String responseBody;
			
		//Check for response number
		//Response for : Code: 0, Request successfully processed
		if(noOfResponse == 0) {
			
			responseBody = '<?xml version="1.0" encoding="utf-8"?>'
		                         +	'<SearchResults:searchresults xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
		                         +		'xsi:schemaLocation="http://www.zillow.com/static/xsd/SearchResults.xsd http://www.zillowstatic.com/vstatic/9a5534270ac0b1ad8c5f06087052fbff/static/xsd/SearchResults.xsd"'
		                         +      'xmlns:SearchResults="http://www.zillow.com/static/xsd/SearchResults.xsd">'
		                         +		'<request>'
		                         +			'<address>2114 Bigelow Ave</address>'
		                         +			'<citystatezip>Seattle, WA</citystatezip>'
		                         +      '</request>'
		                         +      '<message>'
								 +			'<text>Request successfully processed</text>'
								 +			'<code>0</code>'
								 +		'</message>'
		                         +      '<response>'
		                         +			'<results>'
		                         +				'<result>'
								 +					'<zpid>48749425</zpid>'
								 +					'<links>'
								 +						'<homedetails>http://www.zillow.com/homedetails/2114-Bigelow-Ave-N-Seattle-WA-98109/48749425_zpid/</homedetails>'
								 +						'<graphsanddata>http://www.zillow.com/homedetails/2114-Bigelow-Ave-N-Seattle-WA-98109/48749425_zpid/#charts-and-data</graphsanddata>'
								 +						'<mapthishome>http://www.zillow.com/homes/48749425_zpid/</mapthishome><comparables>http://www.zillow.com/homes/comps/48749425_zpid/</comparables>'
								 +					'</links>'
								 +					'<address>'
								 +						'<street>2114 Bigelow Ave N</street>'
								 +						'<zipcode>98109</zipcode>'
								 +						'<city>Seattle</city>'
								 +						'<state>WA</state>'
								 +						'<latitude>47.637933</latitude>'
								 +						'<longitude>-122.347938</longitude>'
								 +					'</address>'
								 +					'<FIPScounty>53033</FIPScounty>'
								 +					'<useCode>SingleFamily</useCode>'
								 +					'<taxAssessmentYear>2011</taxAssessmentYear>'
								 +					'<taxAssessment>928000.0</taxAssessment>'
								 +					'<yearBuilt>1924</yearBuilt>'
								 +					'<lotSizeSqFt>4680</lotSizeSqFt>'
								 +					'<finishedSqFt>3470</finishedSqFt>'
								 +					'<bathrooms>3.0</bathrooms>'
								 +					'<bedrooms>4</bedrooms>'
								 +					'<lastSoldDate>11/26/2008</lastSoldDate>'
								 +					'<lastSoldPrice currency="USD">1025000</lastSoldPrice>'
								 +					'<zestimate>'
								 +						'<amount currency="USD">1120006</amount>'
								 +						'<last-updated>02/24/2013</last-updated>'
								 +						'<oneWeekChange deprecated="true"></oneWeekChange>'
								 +						'<valueChange duration="30" currency="USD">-5970</valueChange>'
								 +						'<valuationRange>'
								 +							'<low currency="USD">851205</low>'
								 +							'<high currency="USD">1444808</high>'
								 +						'</valuationRange>'
								 +						'<percentile>0</percentile>'
								 +					'</zestimate>'
								 +					'<localRealEstate>'
								 +						'<region id="271856" type="neighborhood" name="East Queen Anne">'
								 +							'<links>'
								 +								'<overview>http://www.zillow.com/local-info/WA-Seattle/East-Queen-Anne/r_271856/</overview>'
								 +								'<forSaleByOwner>http://www.zillow.com/east-queen-anne-seattle-wa/fsbo/</forSaleByOwner>'
								 +								'<forSale>http://www.zillow.com/east-queen-anne-seattle-wa/</forSale>'
								 +							'</links>'
								 +						'</region>'
								 +					'</localRealEstate>'
								 +				'</result>'
		                         +			'</results>'
		                         +		'</response>'
		                         +'</SearchResults:searchresults>';
		}
		//Response for : The request was successfully processed, but there was no return on the lot size 
		else if(noOfResponse == 1) {

			responseBody = '<?xml version="1.0" encoding="utf-8"?>'
							+'<SearchResults:searchresults xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.zillow.com/static/xsd/SearchResults.xsd http://www.zillowstatic.com/vstatic/LATEST/static/xsd/SearchResults.xsd"'
							+'xmlns:SearchResults="http://www.zillow.com/static/xsd/SearchResults.xsd">'
								+'<request>'
									+'<address>3265 KIRCHOFF RD UNIT 320, ROLLING MEADOWS, IL 60008</address>'
									+'<citystatezip>ROLLING MEADOWS IL 60008</citystatezip>'
								+'</request>'
								+'<message>'
									+'<text>Request successfully processed</text>'
									+'<code>0</code>'
								+'</message>'
								+'<response>'
									+'<results>'
										+'<result>'
											+'<zpid>54529695</zpid>'
											+'<links>'
												+'<homedetails>http://www.zillow.com/homedetails/3265-Kirchoff-Rd-APT-320-Rolling-Meadows-IL-60008/54529695_zpid/</homedetails>'
												+'<graphsanddata>http://www.zillow.com/homedetails/3265-Kirchoff-Rd-APT-320-Rolling-Meadows-IL-60008/54529695_zpid/#charts-and-data</graphsanddata>'
												+'<mapthishome>http://www.zillow.com/homes/54529695_zpid/</mapthishome>'
												+'<comparables>http://www.zillow.com/homes/comps/54529695_zpid/</comparables>'
											+'</links>'
											+'<address>'
												+'<street>3265 Kirchoff Rd APT 320</street>'
												+'<zipcode>60008</zipcode>'
												+'<city>Rolling Meadows</city>'
												+'<state>IL</state>'
												+'<latitude>42.07769</latitude>'
												+'<longitude>-88.022194</longitude>'
											+'</address>'
											+'<FIPScounty>17031</FIPScounty>'
											+'<useCode>Condominium</useCode>'
											+'<taxAssessmentYear>2011</taxAssessmentYear>'
											+'<taxAssessment>10902.0</taxAssessment>'
											+'<yearBuilt>1991</yearBuilt>'
											+'<finishedSqFt>1100</finishedSqFt>'
											+'<bathrooms>2.0</bathrooms>'
											+'<bedrooms>2</bedrooms>'
											+'<totalRooms>5</totalRooms>'
											+'<lastSoldDate>05/14/2013</lastSoldDate>'
											+'<lastSoldPrice currency="USD">70000</lastSoldPrice>'
											+'<zestimate>'
												+'<amount currency="USD">65043</amount>'
												+'<last-updated>03/04/2014</last-updated>'
												+'<oneWeekChange deprecated="true"></oneWeekChange>'
												+'<valueChange duration="30" currency="USD">-26</valueChange>'
												+'<valuationRange>'
													+'<low currency="USD">51384</low>'
													+'<high currency="USD">79352</high>'
												+'</valuationRange>'
												+'<percentile>0</percentile>'
											+'</zestimate>'
											+'<localRealEstate>'
											+'<region id="36955" type="city" name="Rolling Meadows">'
											+'<links>'
												+'<overview>http://www.zillow.com/local-info/IL-Rolling-Meadows/r_36955/</overview>'
												+'<forSaleByOwner>http://www.zillow.com/rolling-meadows-il/fsbo/</forSaleByOwner>'
												+'<forSale>http://www.zillow.com/rolling-meadows-il/</forSale>'
											+'</links>'
											+'</region>'
											+'</localRealEstate>'
										+'</result>'
									+'</results>'
								+'</response>'
							+'</SearchResults:searchresults>';
		}
		//Response for :Code: 508, Error: no exact match found for input address
		else if(noOfResponse == 2) {
			
			responseBody = '<?xml version="1.0" encoding="utf-8"?>'
							+'<SearchResults:searchresults xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
							+'xsi:schemaLocation="http://www.zillow.com/static/xsd/SearchResults.xsd http://www.zillowstatic.com/vstatic/LATEST/static/xsd/SearchResults.xsd"'
							+'xmlns:SearchResults="http://www.zillow.com/static/xsd/SearchResults.xsd">'
								+'<request>'
									+'<address>4418 Beau Monde Dr Unit 2</address>'
									+'<citystatezip>LISLE IL 60532</citystatezip>'
								+'</request>'
								+'<message>'
									+'<text>Error: no exact match found for input address</text>'
									+'<code>508</code>'
								+'</message>'
							+'</SearchResults:searchresults>';
		}
		
		//Response for : Error: invalid or missing city/state/ZIP parameter
		else if(noOfResponse == 3) {
			
			responseBody = '<?xml version="1.0" encoding="utf-8"?>'
							+'<SearchResults:searchresults xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
							+	'xsi:schemaLocation="http://www.zillow.com/static/xsd/SearchResults.xsd http://www.zillowstatic.com/vstatic/LATEST/static/xsd/SearchResults.xsd"'
							+	'xmlns:SearchResults="http://www.zillow.com/static/xsd/SearchResults.xsd">'
							+	'<request>'
							+		'<address></address>'
							+		'<citystatezip></citystatezip>'
							+	'</request>'
							+	'<message>'
							+		'<text>Error: invalid or missing city/state/ZIP parameter</text>'
							+		'<code>501</code>'
							+	'</message>'
							+'</SearchResults:searchresults>';
		}
		return responseBody;				
	}
	
	//Implement the interface method
    global HTTPResponse respond(HTTPRequest req) {

        //Create a fake HTTP Response
        HttpResponse response = new HttpResponse();
        
        //Set the value of header
        response.setHeader('Content-Type', 'text/xml');
        
        //Getting response body envlop
        String respBody = getResponseBody();
        
        //Set the value of body
        response.setBody(respBody);
        
        //Set the status code
        response.setStatusCode(200);
        
        //Return the response
        return response;
    }
}