@isTest(seeAllData=false)
private class AssignmentMapControllerTest {
///**
// *  Purpose         :   This is used for testing and covering AssignmentMapController.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   03/01/2014
// *
// *  Current Version :   V1.5
// *
// *  Revision Log    :   V1.0 - Created
// *                      V1.1 - Modified - Padmesh Soni(03/10/2014) - New test method "testAssignmentMapController" and 
// *                             a generic method "generateTestData" added.
// *                      V1.2 - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes - Enhancement of functionality on OAM project.
// *                      V1.3 - Modified - Padmesh Soni (05/29/2014) - OAM Tasks 05/28/2014
// *                                           Changes are:
// *                                              1.OAM-67 - Submitted counts columns on vendor display.
// *                                              2.OAM-68 - Show Vendors on Map.
// *                                              3.Additional Task - When we open up the OAM, by default, no "W.O. Types" should be selected. 
// *                                                 A work order without a type cannot be assigned and should not be mapped. The test data is in error.
// *                      V1.4 - New method added - Padmesh Soni (06/03/2014) - OAM 06/02/2014 -
// *                                           Changes are: OAM-65 Speed Optimization.
// *                      V1.5 - Modified - Padmesh Soni (06/19/2014) - Support Case Record Type
// *                                           Changes are: New condition and query filter added of new RecordType excluding of Case object to process.
// * 
// *  Coverage        :   95% - V1.0
// *                      93% - V1.2
// *                      95% - V1.3
// *                      96% - V1.4
// *                      96% - V1.5
// **/
//    
//    private static List<Account> accounts;
//    private static List<Contact> contacts;
//    private static Profile newProfile;
//    private static List<User> users;
//    private static Group queues;
//    
//    private static void generateTestData(integer countData){
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP 
//        //create a new user with the Customer Portal Manager Standard profile
//        newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Vendors'];
//        
//        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
//            //create a new user with the profile
//            queues = new Group(Name='Queue',Type='Queue');
//            insert queues;
//            
//            //Queue sObject   
//            QueueSobject queueObj = new QueueSobject(QueueId = queues.Id, SobjectType = 'Case');
//            insert queueObj;
//        }
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            /******** PRE-TEST SETUP create a new account *********/
//            accounts.add(new Account(Name = 'Tester Account' + i,RecordTypeId='0124000000011Q2'));
//        }
//        
//        insert accounts;
//        
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            //--------------------------------------------------------
//            //PRE-TEST SETUP
//            //create a new contact
//            //Processor Record Type ID: 01240000000UbtfAAC
//            Contact newContact = new Contact(LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active', RecordTypeId = '01240000000UbtfAAC');
//                        newContact.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//                        newContact.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//                        newContact.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//                        newContact.Shrub_Trim__c = 3;//Pricing...required 
//                        newContact.REO_Grass_Cut__c = 3;//Pricing
//                        newContact.Pay_Terms__c = 'Net 30';//Terms required
//                        newContact.Trip_Charge__c = 25;//Pricing
//                        newContact.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//                        newContact.Date_Active__c = Date.Today();//Date Active must be set. 
//                        newContact.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//                        newContact.MailingStreet = '30 Turnberry Dr';
//                        newContact.MailingCity = 'La Place';
//                        newContact.MailingState = 'LA';
//                        newContact.MailingPostalCode = '70068';
//                        newContact.OtherStreet = '30 Turnberry Dr';
//                        newContact.OtherCity = 'La Place';
//                        newContact.OtherState = 'LA';
//                        newContact.OtherPostalCode = '70068';
//                        newContact.Other_Address_Geocode__Latitude__s = 30.00;
//                        newContact.Other_Address_Geocode__Longitude__s = -90.00;
//                        contacts.add(newContact);
//        }   
//        
//        insert contacts;
//        
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            //create a new user with the profile
//            users.add(new User(alias = 'standt'+i, contactId = contacts[i-1].Id, email = Math.random()+Math.random()+Math.random()+Math.random()+'@northsighttestorg.com', emailencodingkey = 'UTF-8',
//                                    lastname = 'Testing'+i, languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//                                    timezonesidkey = 'America/Los_Angeles', username = Math.random()+Math.random()+Math.random()+Math.random()+'@northsighttestorg.com', isActive = true));
//        }
//        
//        insert users;
//    }
//    
//    static testMethod void myUnitTest() {
//        integer vendorsPerZone = 1;//# of vendors to create per zone
//        integer casesPerZone = 1;//# of cases to create per zone per work order type
//        
//        OrderAssignmentSystemUtilityClass.vendorPlacementWrapper[] vendors = OrderAssignmentSystemUtilityClass.setupVendorsAndZones(vendorsPerZone);//creates zones with vendors
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account; 
//        
//        List<Case> cases = OrderAssignmentSystemUtilityClass.genStandardCaseSet(casesPerZone);//creates a standard set of cases
//        for(Case c:cases){
//            c.Client_Name__c = account.Id;
//        }
//        
//        insert cases;
//        List<string> orderIds  = new List<string>();
//        for(Case c:cases){
//            orderIds.add(c.Id);
//        }
//        AssignmentMapController amc = new AssignmentMapController();
//        
//        //OAM-48: Self Assigned filter - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes
//        //New paramter added into method calling
//        AssignmentMapController.assignWorkOrders(orderIds, vendors[0].usr.Id);
//    }
//    
//    //Test method is used to test functionality of AssignmentMapController
//    static testMethod void testAssignmentMapController() {
//        
//        //initialize variables
//        accounts = new List<Account>();
//        contacts = new List<Contact>();
//        users = new List<User>();
//        
//        generateTestData(1);
//        
//        //Query record of Record Type of Account and Case objects
//        //Padmesh Soni (06/19/2014) - Support Case Record Type
//        //Query one more record type of Case Object "SUPPORT"    
//        /*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client' 
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(3, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsOnOrders = new List<Account>();
//        accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsOnOrders;
//        
//        Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true, Formatted_State__c = 'TX');
//        insert property;
//        
//        //List to hold Case records
//        List<Case> workOrders = new List<Case>();
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX',
//                                    Department__c = 'Test', Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual'));
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX'));
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'LA',
//                                    Department__c = 'Test', Assignment_Program__c = 'Mannual'));
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'AR',
//                                    Department__c = 'Test1', Work_Ordered__c = 'Grass Initial Cut', Assignment_Program__c = 'OrderSelect'));
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[2].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Test', State__c = 'AR',
//                                    Department__c = 'Facebook', Work_Ordered__c = 'Testing', Assignment_Program__c = 'Test Prog'));
//        
//        //insert case records
//        insert workOrders;
//        
//        //Test start here
//        Test.startTest();
//        
//        User tempUser = [SELECT Name, Alias FROM User WHERE Id =: users[0].Id Limit 1];
//        
//        //Initialize controller of AssignmentMap 
//        AssignmentMapController controller = new AssignmentMapController();
//        controller.vendorDetails = tempUser.Name + '(' + tempUser.Alias + ') :2';
//        //controller.stateInstant = 'TX';
//        
//        //assert statements
//        //Should this be 1 TX.  
//        System.assertEquals(1, controller.stateList.size());
//        System.assertEquals(2, controller.clientList.size());
//        System.assertEquals(2, controller.workOrderTypeList.size());
//        
//        //call method to create Wrapper list
//        controller.populateVendorTable();
//        
//        //assert statement
//        System.assertEquals(1, controller.getVendorWrapList().size());
//        
//        //Query WorkOrders from Texas
//        List<Case> orders = [SELECT Id FROM Case WHERE RecordTypeId !=: recordTypes[2].Id AND State__c = 'TX'];
//        
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account; 
//        
//        for(Case c : orders)
//        {
//            c.Client_Name__c = account.Id;
//        }
//        
//        update orders;
//        
//        //assert statement
//        System.assertEquals(2, orders.size());
//        
//        String[] orderIds = new List<String>();
//        
//        for(integer i = 0; i < orders.size(); i++) {
//            
//            orderIds.add(orders[i].Id);
//        }
//        
//        //OAM-48: Self Assigned filter - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes
//        //New paramter added into method calling
//        AssignmentMapController.assignWorkOrders(orderIds, users[0].Id);
//        
//        //Call setWOAssignmentProgram method to change the Assignment Program
//        AssignmentMapController.setWOAssignmentProgram(orderIds, 'Manual');
//        
//        for(Case assignOrder : [SELECT Id, Assignment_Program__c FROM Case WHERE  RecordTypeId !=: recordTypes[2].Id AND Id IN: orderIds]) {
//            
//            System.assertEquals('Manual', assignOrder.Assignment_Program__c);   
//        }
//        
//        //Query WorkOrders from Texas
//        orders = [SELECT Id FROM Case WHERE  RecordTypeId !=: recordTypes[2].Id AND OwnerId =: users[0].Id];
//        
//        //assert statement
//        System.assertEquals(2, orders.size());
//        
//        //re-initialize list
//        orderIds = new List<String>();
//        
//        try{
//            
//            //OAM-48: Self Assigned filter - Padmesh Soni (05/06/2014) - 5/4/2014 OAM Changes
//            //New paramter added into method calling
//            AssignmentMapController.assignWorkOrders(orderIds, users[0].Id);
//        } catch(Exception e) {
//            
//            //assert statement
//            System.assertEquals(Label.WOAMP_Not_Selected, e.getMessage());
//        }
//        
//        String[] newOrderIds = new String[0];
//        
//        try{
//        
//            //New paramter added into method calling
//            AssignmentMapController.setWOAssignmentProgram(newOrderIds, 'Manual');
//        } catch(Exception e) {
//            
//            //assert statement
//            System.assertEquals(Label.WOAMP_Not_Selected, e.getMessage());
//        }
//        
//        User cus_User = [SELECT Name, Alias FROM User WHERE Id =: users[0].Id];
//        
//        //Initialize controller of AssignmentMap 
//        controller = new AssignmentMapController();
//        controller.vendorDetails = tempUser.Name + '(' + tempUser.Alias + ') :2,'+ cus_User.Name + '(' + cus_User.Alias + ') :2,';
//        //controller.stateInstant = 'TX';
//        
//        //call method to create Wrapper list
//        controller.populateVendorTable();
//        
//        //assert statement    
//        //System.assertEquals(2, controller.getVendorWrapList().size());
//        
//        //call method to create Wrapper list
//        controller.populateVendorTable();
//        
//        //assert statement
//        //System.assertEquals(2, controller.getVendorWrapList().size());
//        
//        //Query result of OS queues
//        Group osGroup = [SELECT Id, Name FROM Group WHERE Name = 'UA'];
//        
//        workOrders = [SELECT Id, OwnerId FROM Case WHERE  RecordTypeId !=: recordTypes[2].Id AND State__c = 'AR'];
//        
//        //Loop through case list
//        for(Case wo : workOrders) {
//            wo.Client_Name__c = account.Id;
//            wo.OwnerId = osGroup.Id;    
//        }
//        
//        //update orders
//        update workOrders;
//        
//        //string to controller variable
//        controller.vendorDetails = tempUser.Name + '(' + tempUser.Alias + ') :2,'+ cus_User.Name + '(' + cus_User.Alias + ') :2,'
//                                    + osGroup.Name + '(null) :1,';
//        
//        //call method to create Wrapper list
//        controller.updateVendorTable();
//        
//        //assert statement
//        //System.assertEquals(3, controller.getVendorWrapList().size());
//        
//        cus_User = [SELECT Name, Alias FROM User WHERE Id =: cus_User.Id];
//        cus_User.IsActive = false;
//        
//        //Initialize controller of AssignmentMap 
//        controller = new AssignmentMapController();
//        controller.vendorDetails = tempUser.Name + '(' + tempUser.Alias + ') :2,'+ cus_User.Name + '(' + cus_User.Alias + ') :2,'
//                                    + osGroup.Name + '(null) :1,';
//        
//        //call method to create Wrapper list
//        controller.populateVendorTable();
//        
//        //assert statement    
//        //System.assertEquals(3, controller.getVendorWrapList().size());
//        
//        controller.ordersJSON = '';
//        
//        workOrders = [SELECT Id, CaseNumber FROM Case];
//        
//        String[] woIds = new String[workOrders.size()];
//        
//        integer index = 0;
//        //Loop through case list
//        for(Case wo : workOrders) {
//            
//            woIds[index] = wo.Id;
//            controller.ordersJSON +=  wo.CaseNumber +',';
//            index++;
//        }
//        
//        //Checking for string endswith comma (,) character
//        if(controller.ordersJSON.endsWith(',')) {
//            
//            controller.ordersJSON = controller.ordersJSON.substring(0, controller.ordersJSON.lastIndexOf(','));
//        }
//        
//        //Get current user's Name and assign it to "toBeOwned"
//        controller.toBeOwned = UserInfo.getName();
//        
//        //call controller method
//        controller.populateAssignedOrdersList();
//        
//        //assert statements
//        //System.assertEquals(4, controller.selCounter);
//        System.assertEquals(true, controller.displayPopup);
//                
//        controller.closePopup();
//        System.assertEquals(false, controller.displayPopup);
//        
//        controller.showPopup();
//        
//        //Get current user's Name and assign it to "toBeOwned"
//        controller.toBeOwned = cus_User.Name +'('+ cus_User.Alias +')';
//        
//        //call controller method
//        controller.populateAssignedOrdersList();
//        
//        //assert statements
//        //System.assertEquals(4, controller.selCounter);
//        System.assertEquals(true, controller.displayPopup);
//                
//        //Test stops here
//        Test.stopTest();
//    }
//    
//    //Padmesh Soni (06/03/2014) - Changes are: OAM-65 Speed Optimization.
//    //Test method to test the funcationality of getSearchResult method
//    static testMethod void testGetSearchResult() {
//        
//        //List to hold Account records
//        List<Account> testAccounts = new List<Account>();
//        testAccounts.add(new Account(Name = 'Tester Account'));
//        
//        //insert account here
//        insert testAccounts;
//        
//        //List to hold Account records
//        List<Contact> testContacts = new List<Contact>();
//        Contact newContact = new Contact(LastName = 'Tester02', AccountId = testAccounts[0].Id, Pruvan_Username__c = 'NSBobTest', Status__c = 'Active',
//                                    Other_Address_Geocode__Latitude__s = 41.785896, Other_Address_Geocode__Longitude__s = -87.598936);
//                    newContact.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//                        newContact.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//                        newContact.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//                        newContact.Shrub_Trim__c = 3;//Pricing...required 
//                        newContact.REO_Grass_Cut__c = 3;//Pricing
//                        newContact.Pay_Terms__c = 'Net 30';//Terms required
//                        newContact.Trip_Charge__c = 25;//Pricing
//                        newContact.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//                        newContact.Date_Active__c = Date.Today();//Date Active must be set. 
//                        newContact.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//                        newContact.MailingStreet = '30 Turnberry Dr';
//                        newContact.MailingCity = 'La Place';
//                        newContact.MailingState = 'LA';
//                        newContact.MailingPostalCode = '70068';
//                        newContact.OtherStreet = '30 Turnberry Dr';
//                        newContact.OtherCity = 'La Place';
//                        newContact.OtherState = 'LA';
//                        newContact.OtherPostalCode = '70068';
//                        newContact.Other_Address_Geocode__Latitude__s = 30.00;
//                        newContact.Other_Address_Geocode__Longitude__s = -90.00;
//        testContacts.add(newContact);
//        //insert contact here
//        insert testContacts;
//        
//        //Query record of Record Type of Account and Case objects
//        //Padmesh Soni (06/19/2014) - Support Case Record Type
//        //Query one more record type of Case Object "SUPPORT"    
//        /*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client' 
//                                            OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(3, recordTypes.size());
//        
//        
//        //List of Account to store testing records
//        List<Account> testAccountsOnOrders = new List<Account>();
//        testAccountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        testAccountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert testAccountsOnOrders;
//        
//        //List to hold Case records
//        List<Case> workOrders = new List<Case>();
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account; 
//        
//        workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = testAccountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Reference_Number__c = '122302', State__c = 'TX',
//                                    Scheduled_Date_Override__c = Date.today(), Work_Ordered__c = 'Grass Recut',Client_Name__c = account.Id));
//        workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = testAccountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Reference_Number__c = '021223', State__c = 'TX',
//                                    Scheduled_Date_Override__c = Date.today().addDays(-6), Work_Ordered__c = 'Grass Recut',Client_Name__c = account.Id));
//        workOrders.add(new Case(RecordTypeId = recordTypes[2].Id, AccountId = testAccountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Reference_Number__c = '120203', State__c = 'TX',
//                                    Scheduled_Date_Override__c = Date.today().addDays(-6), Work_Ordered__c = 'Grass Recut',Client_Name__c = account.Id));
//        
//        //insert case records
//        insert workOrders;
//        
//        //List to hold all Property records
//        List<Geocode_Cache__c> properties = new List<Geocode_Cache__c>();
//        
//        for(Geocode_Cache__c property : [SELECT Id, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c]) {
//            
//            //assign Geolocation values to fields
//            property.Location__Latitude__s = 50.09567;
//            property.Location__Longitude__s = -10.4532;
//            properties.add(property);
//        }
//        
//        System.assertEquals(1, properties.size());
//        update properties;
//        
//        List<Id> fixedSearchResults= new List<Id>();
//        
//        String searchString = '';
//        
//        //Loop through Case records
//        for(Case workOrder : [SELECT Id, CaseNumber, Geocode_Cache__r.Location__Latitude__s, Geocode_Cache__r.Location__Longitude__s
//                                FROM Case WHERE RecordTypeId !=: recordTypes[2].Id AND Id IN: workOrders 
//                                AND (CaseNumber LIKE '%0%' OR Reference_Number__c LIKE '%02%')]) {
//            
//            fixedSearchResults.add(workOrder.Id);
//            
//            //format the result values
//            String geoLoacationValue = String.valueOf(workOrder.Geocode_Cache__r.Location__Latitude__s) + '**' 
//                                        + String.valueOf(workOrder.Geocode_Cache__r.Location__Longitude__s);
//            searchString += 'label:' + workOrder.CaseNumber + '@@value:' + geoLoacationValue + '$$image:Order##';
//        }
//        
//        //Loop through Contact records
//        for(Contact con : [SELECT Name, Other_Address_Geocode__Latitude__s, Other_Address_Geocode__Longitude__s FROM Contact
//                                WHERE Id IN: testContacts AND (Name LIKE '%02%' OR FirstName LIKE '%02%' 
//                                OR LastName LIKE '%02%' OR OtherStreet LIKE '%02%' OR OtherState LIKE '%02%'
//                                OR OtherCountry LIKE '%02%' OR OtherCity LIKE '%02%' 
//                                OR OtherPostalCode LIKE '%02%')]) {
//            
//            fixedSearchResults.add(con.Id);
//            
//            //format the result values
//            String geoLoacationValue = String.valueOf(con.Other_Address_Geocode__Latitude__s) + '**' 
//                                        + String.valueOf(con.Other_Address_Geocode__Longitude__s);
//            searchString += 'label:' + con.Name + '@@value:' + geoLoacationValue + '$$image:Contact##';
//        }
//        
//        Test.setFixedSearchResults(fixedSearchResults);
//        
//        //Test start here
//        Test.startTest();
//        
//        //Call method
//        String searchResult = AssignmentMapController.getSearchResult('02');
//        
//        //Test stops here
//        Test.stopTest(); 
//        
//        //assert statement here
//        System.assertEquals(searchString, searchResult);
//    }
}