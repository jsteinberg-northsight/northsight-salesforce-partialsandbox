@isTest()
/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/
*/
public without sharing class PruvanWorkOrderStatusController_Test {
//    private static Case newCase;
//    private static Case newCase2;
//    private static Case newCase3;
//    private static string workOrderStatusPayload;
//    
//    private static void generateTestCases() {
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new account
//        Account newAccount = new Account(
//            Name = 'Tester Account',
//            RecordTypeId = '0124000000011Q2'
//            
//        );
//        insert newAccount;
//        
//        //Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//        //Fetch record type of Account Sobject
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client' 
//                                                    AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//        
//        //Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//        //Create a test instance for Account sobject
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account; 
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new contact
//        Contact newContact = new Contact(
//            LastName = 'Tester',
//            RecordTypeId = '01240000000UPSr',
//            AccountId = newAccount.Id,
//            Pruvan_Username__c = 'NSBobTest',
//            MailingStreet = '30 Turnberry Dr',
//            MailingCity = 'La Place',
//            MailingState = 'LA',
//            MailingPostalCode = '70068',
//            OtherStreet = '30 Turnberry Dr',
//            OtherCity = 'La Place',
//            OtherState = 'LA',
//            OtherPostalCode = '70068',
//            Other_Address_Geocode__Latitude__s = 30.00,
//            Other_Address_Geocode__Longitude__s = -90.00
//        );
//        insert newContact;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new test case
//        newCase = new Case();
//        newCase.street_address__c = '1234 Street';
//        newCase.state__c = 'TX';
//        newCase.city__c = 'Austin';
//        newCase.zip_code__c = '78704';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        newCase.Client__c = 'NOT-SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'Grass Recut';
//        newCase.Approval_Status__c = 'Not Started';
//        newCase.Work_Completed__c = 'Yes';
//        newCase.Date_Serviced__c = Date.today();
//    
//        //Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//        //new field populated 
//        newCase.Client_Name__c = account.Id;
//    
//        insert newCase;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new test case
//        newCase2 = new Case();
//        newCase2.street_address__c = '1234 Street';
//        newCase2.state__c = 'TX';
//        newCase2.city__c = 'Austin';
//        newCase2.zip_code__c = '78704';
//        newCase2.Vendor_code__c = 'SRSRSR';
//        newCase2.Work_Order_Type__c = 'Routine';
//        newCase2.Client__c = 'NOT-SG';
//        newCase2.Loan_Type__c = 'REO';
//        newCase2.ContactId = newContact.Id;
//        newCase2.Work_Ordered__c = 'Grass Recut';
//        newCase2.Approval_Status__c = 'Not Started';
//        newCase2.Work_Completed__c = 'Yes';
//        newCase2.Date_Serviced__c = Date.today();
//        
//        //Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//        //new field populated 
//        newCase2.Client_Name__c = account.Id;
//        
//        insert newCase2;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new test case
//        newCase3 = new Case();
//        newCase3.street_address__c = '1234 Street';
//        newCase3.state__c = 'TX';
//        newCase3.city__c = 'Austin';
//        newCase3.zip_code__c = '78704';
//        newCase3.Vendor_code__c = 'SRSRSR';
//        newCase3.Work_Order_Type__c = 'Routine';
//        newCase3.Client__c = 'NOT-SG';
//        newCase3.Loan_Type__c = 'REO';
//        newCase3.ContactId = newContact.Id;
//        newCase3.Work_Ordered__c = 'Grass Recut';
//        newCase3.Approval_Status__c = 'Not Started';
//        newCase3.Work_Completed__c = 'Yes';
//        newCase3.Date_Serviced__c = Date.today();
//        
//        //Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//        //new field populated 
//        newCase3.Client_Name__c = account.Id;
//        
//        insert newCase3;
//    }
//    
//    private static void setPayload(){
//        Case case1 = [SELECT CaseNumber From Case Where Id =: newCase.Id];
//        Case case2 = [SELECT CaseNumber From Case Where Id =: newCase2.Id];
//        Case case3 = [SELECT CaseNumber From Case Where Id =: newCase3.Id];
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a workOrderStatus payload
//        workOrderStatusPayload = '{"workOrders": [{"workOrderNumber": "NSM::' + case1.CaseNumber + '","status": "Complete"},{"workOrderNumber": "NSM::' + case2.CaseNumber + '","status": "Complete"},{"workOrderNumber": "NSM::' + case3.CaseNumber + '","status": "Complete"}]}';
//    }
//    
//    //--------------------------------------------------------
//    //TESTS
//    //Unit tests for method testing and regression testing. 
//    //--------------------------------------------------------
//        
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - test that verifies the controller is working properly
//    public static testmethod void testPruvanWorkOrderStatusController(){
//        system.Test.startTest();
//        
//        generateTestCases();
//        setPayload();
//        
//        //instantiate the page
//        PageReference pageRef = Page.PruvanWorkOrderStatus;
//        
//        //set the page as the test starting point
//        Test.setCurrentPage(pageRef);
//        
//        //instantiate the controller
//        PruvanWorkOrderStatusController controller = new PruvanWorkOrderStatusController();
//        
//        //add the payload to the page parameters
//        ApexPages.currentPage().getParameters().put('payload', workOrderStatusPayload);
//        
//        //assert that we get an error before controller execution
//        system.assertEquals('{"error":"The web service call has failed to hit the proper web service to initiate execution."}', controller.getResponse());
//        
//        //call the controller's execute method
//        controller.execute();
//        
//        //assert that we get the expected value back from the controller
//        system.assertEquals('{"error":null}', controller.getResponse());
//        
//        system.Test.stopTest();
//    }
}