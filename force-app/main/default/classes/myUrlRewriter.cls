/*
class that implements Site.UrlRewriter
used for mapping service url calls to a specified VF page
*/
global with sharing class myUrlRewriter implements Site.UrlRewriter {

    String VALIDATE_SERVICEURL = '/Pruvan/validate';
    String VALIDATE_PAGE = '/PruvanValidate';
    
    String SURVEYVALIDATE_SERVICEURL = '/Pruvan/surveyValidate';
    String SURVEYVALIDATE_PAGE = '/PruvanSurveyValidate';
    
    String GETWORKORDERS_SERVICEURL = '/Pruvan/getWorkOrders';
    String GETWORKORDERS_PAGE = '/PruvanGetWorkOrders';
    
    //String UPLOADPICTURES_SERVICEURL = '/Pruvan/uploadPictures';
    //String UPLOADPICTURES_PAGE = '/PruvanUploadPictures';
    
    String REFERENCELOOKUP_SERVICEURL = '/Pruvan/referenceLookup';
    String REFERENCELOOKUP_PAGE = '/PruvanNorthsightReferenceLookup';
    
    String WORKORDERSTATUS_SERVICEURL = '/Pruvan/workOrderStatus';
    String STATUS_SERVICEURL = '/Pruvan/status';
    String WORKORDERSTATUS_PAGE = '/PruvanWorkOrderStatus';
    
	map<string,string> urlMap = new map<string,string>{
		VALIDATE_SERVICEURL=>VALIDATE_PAGE,
		SURVEYVALIDATE_SERVICEURL=>SURVEYVALIDATE_PAGE,
		GETWORKORDERS_SERVICEURL=>GETWORKORDERS_PAGE,
		REFERENCELOOKUP_SERVICEURL=>REFERENCELOOKUP_PAGE,
		WORKORDERSTATUS_SERVICEURL=>WORKORDERSTATUS_PAGE,
		STATUS_SERVICEURL=>WORKORDERSTATUS_PAGE
		};
    global PageReference mapRequestUrl(PageReference
            myFriendlyUrl){
        String url = myFriendlyUrl.getUrl();
		string newUrl = urlMap.get(url);
		if(newUrl == null){
			urlRewriter rw = new urlRewriter();
			return rw.mapRequestUrl(myFriendlyUrl);
		}
		return new PageReference(newUrl);
    }
    
    
    global List<PageReference> generateUrlFor(List<PageReference> 
            mySalesforceUrls){
       
    return null;
  }

}