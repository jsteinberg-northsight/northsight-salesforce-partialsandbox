global without sharing class BatchCommitmentTimeout implements Database.Batchable<sObject>{
	global Database.Querylocator start(Database.BatchableContext BC) {
		DateTime timeout = DateTime.Now().addHours(-1);
		return Database.getQueryLocator([select id from Client_Commitment__c where (Sync_Status__c = 'In Progress' and Attempt_Count__c >= 3 and Last_Sync_Attempt__c <=:timeout) or (Sync_Status__c in ('Unsent','In Progress') and Commitment_Date__c < Today)]);
	}
	global void execute(Database.BatchableContext BC, List<Client_Commitment__c> timeoutCommits) {
		for(Client_Commitment__c c:timeoutCommits){
			c.Sync_Status__c = 'Failed';
		}
		update timeoutCommits;
	}
	global void finish(Database.BatchableContext BC) {
		
	}

}