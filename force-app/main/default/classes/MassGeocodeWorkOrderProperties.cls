public with sharing class MassGeocodeWorkOrderProperties {
	public WorkOrderWrapper[] workorders{get;set;}
	public integer workOrderCount{get;set;}
	public boolean autorun{get;set;}
	private Map<id,Geocode_Cache__c> caseGeoCacheMap = new Map<id,Geocode_Cache__c>();
	private Map<string,Case[]> addrWOMap = new Map<string,Case[]>();
	private Map<string,Geocode_Cache__c> addrGeoMap = new Map<string,Geocode_Cache__c>();
	
	//Padmesh Soni (06/24/2014) - Support Case Record Type
	//Variable to hold list of RecordTypes of Case object
	List<RecordType> recordTypes;
	
	public MassGeocodeWorkOrderProperties(){
		autorun=false;
		init();
		
		//Padmesh Soni (06/24/2014) - Support Case Record Type
		//Query record of Record Type of Case object
		recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
		
	}
	private void init(){
		caseGeoCacheMap = new Map<id,Geocode_Cache__c>();
		addrWOMap = new Map<string,Case[]>();
		addrGeoMap = new Map<string,Geocode_Cache__c>();
		
		//Padmesh Soni (06/24/2014) - Support Case Record Type
    	//New filter criteria added for RecordTypeId
		AggregateResult r = [select count(id) WOCount from Case where RecordTypeId NOT IN: recordTypes AND Serviceable__c = 'Yes' 
								and Geocode_Cache__c = null];
								
		workOrderCount = (integer)r.get('WOCount');
		
		//Padmesh Soni (06/24/2014) - Support Case Record Type
    	//New filter criteria added for RecordTypeId
		Case[] cases = [select id,CaseNumber, Geocode_Cache__c, Street_Add_Concat__c, Street_Address__c, City__c, State__c, Zip_Code__c from Case 
							where RecordTypeId NOT IN: recordTypes AND Serviceable__c = 'Yes' and ((Geocode_Cache__r.Location__Latitude__s = 0 
							and Geocode_Cache__r.Location__Longitude__s = 0 and Geocode_Cache__r.Last_Geocoded__c = null) 
							or geocode_cache__c = null) limit 200];
							
		workorders = new WorkOrderWrapper[]{};
		preCheckGeocodeCache(cases);
		for(string addrKey: addrWOMap.KeySet()){
			for(Case c:addrWOMap.get(addrKey)){
				if(c.Geocode_Cache__c == null){
					WorkOrderWrapper w = new WorkOrderWrapper(c);
					w.addressHash = addrKey;
					workorders.add(w);
					
				}
			}
		}
	}
	private Case[] preCheckGeocodeCache(Case[] cases){
		for(Case workorder:cases){
			string addrKey = GeoUtilities.createAddressHash(workorder.Street_Add_Concat__c);
			if(!addrWOMap.containsKey(addrKey)){
				addrWOMap.put(addrKey,new Case[]{});	
			}
				
				addrWOMap.get(addrKey).add(workorder);
		}
		Geocode_Cache__c[] cacheList = [select id,Address_hash__c,quarantined__c, Flag__c from Geocode_Cache__c where Address_hash__c in :addrWOMap.KeySet()];
		for(Geocode_Cache__c cache:cacheList){
			for(Case c:addrWOMap.get(cache.Address_hash__c)){
				c.Geocode_Cache__c = cache.id;
				caseGeoCacheMap.put(c.Id,cache);
			}
		}
		return cases;
	}
	public void saveGeocodeRecords(){
		for(WorkOrderWrapper wo:workOrders){
			
			if((!addrGeoMap.ContainsKey(wo.addressHash)) && wo.latitude != 0 && wo.longitude != 0){
				boolean partial = true;
				string geocode_status = 'OK';
				if(wo.addressTypes.contains('street_address') || wo.addressTypes.contains('premise') || wo.addressTypes.contains('subpremise')) {
	    			partial = false;
	    		} else {
	    			partial = true;
	    			geocode_status = 'NOT_PRECISE';
	    		}
				addrGeoMap.put(wo.addressHash,new Geocode_Cache__c(
					Last_Geocoded__c = Date.Today(), 
					Address_hash__c = wo.addressHash,
					location__latitude__s= wo.latitude,
					location__longitude__s =  wo.longitude,
					Formatted_Address__c = GeoUtilities.formatAddress(wo.wo.Street_Address__c,wo.wo.City__c,wo.wo.State__c,wo.wo.Zip_Code__c),
					Formatted_Street_Address__c = GeoUtilities.capitalizeAll(GeoUtilities.preformatAddress(wo.wo.Street_Address__c)),
					Formatted_City__c = wo.wo.City__c,
					Formatted_State__c = wo.wo.State__c,
					Formatted_Zip_Code__c = GeoUtilities.preformatZip(wo.wo.Zip_Code__c),
    				Street_Address__c = wo.wo.Street_Address__c,
					Geocode_Status__c = geocode_status,
					Partial_Match__c = partial,
					Quarantined__c = partial
					)
					); 
			}	
		}
		insert addrGeoMap.Values();
		for(string addrKey:addrGeoMap.KeySet()){
			for(Case c:addrWOMap.get(addrKey)){
				caseGeoCacheMap.put(c.id,addrGeoMap.get(addrKey));
			}
		}
		savePrematchedGeocodeCache();
		init();
	}
	public void savePrematchedGeocodeCache(){
		List<Case> cases = new List<Case>();
		for(Id caseId:caseGeoCacheMap.KeySet()){
			
			if(caseGeoCacheMap.get(caseId).Quarantined__c&&caseGeoCacheMap.get(caseId).Flag__c){
				cases.add(new Case(Id=caseId,Approval_Status__c = 'Quarantined-Flagged',Geocode_Cache__c = caseGeoCacheMap.get(caseId).Id));
			}else if(caseGeoCacheMap.get(caseId).Quarantined__c){
				cases.add(new Case(Id=caseId,Approval_Status__c = 'Quarantined',Geocode_Cache__c = caseGeoCacheMap.get(caseId).Id));
			}
			else{
				cases.add(new Case(Id=caseId,Automatic_Reassignment__c = true,Geocode_Cache__c = caseGeoCacheMap.get(caseId).Id));
			} 
		}
		database.update(cases,false);
		
		/*******************************************************************************************************************/
		//create a list for case IDs
		List<ID> caseIDs = new List<ID>();
		//add case IDs from ordersToUpdate into caseIDs
		for(Case cID : cases){
			caseIDs.add(cID.Id);
		}
		/*******************************************************************************************************************/
	}
	
	    public static void unitTestBypass(){
        integer i=0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
	
}