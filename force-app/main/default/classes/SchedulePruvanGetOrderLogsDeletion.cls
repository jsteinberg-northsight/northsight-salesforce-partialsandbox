global class SchedulePruvanGetOrderLogsDeletion implements Schedulable{
	global void execute(SchedulableContext ctx) {
		BatchDeletePruvanGetWorkOrderLogs b = new BatchDeletePruvanGetWorkOrderLogs();
		
		Database.executeBatch(b, 200);
	}
}