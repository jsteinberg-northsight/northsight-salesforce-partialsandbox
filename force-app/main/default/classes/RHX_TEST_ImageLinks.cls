@isTest(SeeAllData=true)
public class RHX_TEST_ImageLinks {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM ImageLinks__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new ImageLinks__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}