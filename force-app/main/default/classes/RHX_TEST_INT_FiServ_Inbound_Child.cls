@isTest(SeeAllData=true)
public class RHX_TEST_INT_FiServ_Inbound_Child {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM INT_FiServ_Inbound_Child__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new INT_FiServ_Inbound_Child__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}