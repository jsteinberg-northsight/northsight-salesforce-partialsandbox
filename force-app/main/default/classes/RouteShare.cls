public without sharing class RouteShare {
	public string directions{
		get;
		private set;
		}
	public RouteWrapper route{get;private set;}
	public RouteShare(){
		directions = '';
		try{
		string routeId = Apexpages.currentPage().getParameters().get('routeId');
		if(routeId==null||routeId.trim()==''){
			routeId = Apexpages.currentPage().getParameters().get('id');
		}
		List<Route__c> r = [select Id,
						Name,
						Directions__c,
						Est_Completion_Date__c,
						Number_of_Stops__c,
						Route_Label__c,
						serializedMapMetaData__c,
						Calculate_Round_Trip__c,
						Avoid_HIghways__c,
						ownerid,
						owner.name,
						permalink__c,
						permalink_directions__c,
						createdDate,
						distance_duration__c,
						(select
						Id,
						Name,
						Address__c,
						City__c,
						State__c,
						Zip_Code__c,
						Stop_Number__c,
						originalIndex__c,
						Location__Latitude__s,
						Location__Longitude__s,
						Directions__c,
						Work_Order__c,
						Work_Order__r.id,
					    Work_Order__r.Zip_Code__c, 
					    Work_Order__r.Scheduled_Date__c,
					    Work_Order__r.Work_Ordered__c, 
					    Work_Order__r.Work_Ordered_Text__c, 
						Work_Order__r.Street_Address__c, 
					    Work_Order__r.City__c,
						Work_Order__r.Status_Health_Text__c, 
						Work_Order__r.Status,
						Work_Order__r.Vendor_Status__c, 
						Work_Order__r.State__c, 
						Work_Order__r.Notes__c, 
						Work_Order__r.Client_Number__c, 
						Work_Order__r.CaseNumber,
						Work_Order__r.Display_Street__c,
						Work_Order__r.Display_City__c,
						Work_Order__r.Display_State__c,
						Work_Order__r.Display_Zip_Code__c,
						Work_Order__r.Geocode_Cache__r.Formatted_Address__c
						from Route_Stops__r order by Stop_Number__c) from Route__c where id = :routeId and CreatedDate >= Last_N_Days:10];
						
		if(r.size() > 0){
			for(Route__c rt : r){
				route = new RouteWrapper(rt);
				for(Route_Stop__c stop:rt.route_stops__r){
					if(stop.Directions__c != null){
						directions += stop.Directions__c;
					} 	
				}
			}
		}
		else{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Oops!  That\'s not a valid Route URL!  The route has expired or does not exist.'));
			directions='<div><h1>Oops!</h1></div><div><h2>That\'s not a valid Route URL!</h2></div><div>The route has expired or does not exist.</div>';
		}
						
		/*route = new RouteWrapper(r);
		for(Route_Stop__c stop:r.route_stops__r){
			if(stop.Directions__c != null){
				directions += stop.Directions__c;
			} 	
		}*/	
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' ' + e.getStackTraceString()));
			directions= e.getMessage() + ' ' + e.getStackTraceString();
		}
	}
		public string renderType{
		get{
			Set<string> supportedValues=new Set<string>(new string[]{'pdf','gpx-rte','gpx-wpt','itn'});
			string t = ApexPages.currentPage().getParameters().get('renderAs');
			if(supportedValues.contains(t==null?null:t.toLowerCase())){
				
				return t.toLowerCase().substring(0,3);
			}
			else{
				return null;
			}
		}
	}
	public string getExport(){
		string t = ApexPages.currentPage().getParameters().get('renderAs');
		if(t=='gpx-rte'){
			return route.getGarminRouteXML();
		}
		else if(t=='gpx-wpt'){
			return route.getGarminWaypointXML();
		}
		else if(t=='itn'){
			return route.getTomTomITN();
		}else{
			return 'ERROR: Unsupported Export Type';
		}
	} 
	public string SITE_URL { 
				get { 
					return public_site.url; 
				} 
			}
}