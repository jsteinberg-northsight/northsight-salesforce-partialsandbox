/*
    Some concerns pertaining to correcting bad geocodes
    1. If an address is wrong, all work order addresses will need to be updated
        -- What is the source of the addresses? Can we fix the parent source? 
            We don't want future WOs coming in with the same bad address
    2. If an address geocodes correctly, how do we determine "released" status?
    
*/
public with sharing class WOQuarantineController {
    public string geocode_param_id {get; set;}
    public string param_lat {get; set;}
    public string param_lng {get; set;}
    public string param_street {get; set;}
    public string param_city {get; set;}
    public string param_state {get; set;}
    public string param_zip {get; set;}
    public string param_notes {get; set;}
    public boolean showFlagged {get; set;}
    private geocode_cache__c[] save_list;
    public Set<String> approvalStatusesToCheck = new Set<String>{'Not Started','Quarantined','Quarantined-Flagged'};
    
    //Code added - Padmesh Soni (1/12/2015) - CCP-15:Fill County on Property
	//New variable added to store county value from page
	public string param_county {get; set;}
    
    public integer total {get; set;}
    
    public Map<String,String> status {
        get {
            return new Map<String, String> {
                'OK' => 'OK',
                'ZERO_RESULTS' => 'Address Not Found', 
                'INVALID_REQUEST' => 'Bad Address', 
                'REQUEST_DENIED' => 'Geocode Error',
                'UNKNOWN_ERROR' => 'Geocode Error',
                'OVER_QUERY_LIMIT' => 'Over Rate Limit',
                'NOT_PRECISE'=>'Not Precise'  // Not an official google response, added for partial matches
            };
        }
        private set;
    }
    public class woWrapper implements Comparable{
        public Case wo{get;set;}
        public string defaultStatus = '';//Set in cacheWrapper
        public woWrapper(Case workOrder){
            wo = workOrder;
        }
        /*********************************************************************************************************************************************/
        //compare to method to be used for sorting work orders by date
        public integer CompareTo(Object compareTo){
            woWrapper otherWorkOrder = (woWrapper)compareTo;//order to compare against
            
            if((date)this.wo.get('Due_Date__c') > (date)otherWorkOrder.wo.get('Due_Date__c')) return 1;//if this.wo due date > otherWorkOrder dute date: return 1
            
            if((date)this.wo.get('Due_Date__c') < (date)otherWorkOrder.wo.get('Due_Date__c')) return -1;//if this.wo due date < otherWorkOrder dute date: return -1
            
            return 0;//return 0: this.wo due date == otherWorkOrder due date
        }
        /*********************************************************************************************************************************************/
        public void toggleOverride(){
            wo.Override_Quarantine__c = !wo.Override_Quarantine__c;
            if(wo.Override_Quarantine__c){
                wo.Approval_Status__c = 'Not Started';
            }else{
                wo.Approval_Status__c = defaultStatus;
            }
            /*6-10-2013 code for setting work order reassignment*/
            if(wo.Override_Quarantine__c == true){
                wo.Automatic_Reassignment__c = true;
            }
            upsert wo;
        }
    }
    public class cacheWrapper implements Comparable{
        //public boolean isSelected {get; set;}
        public geocode_cache__c geo {get; private set; }
        public List<woWrapper> workOrders {get;set;}
        public void toggleFlag(){
            geo.Flag__c = !geo.Flag__c;
            upsert geo.workorders__r;
            upsert geo;
            return;
        }
        public string status_img {
            get {
                if(geo.geocode_status__c!='OK') {
                    return '/servlet/servlet.FileDownload?file=01540000000aRgp';
                } else {
                    return '/servlet/servlet.FileDownload?file=01540000000aRgu';
                }
            }
            private set;
        }
        public cacheWrapper(geocode_cache__c rec) {
            geo = rec;
            //isSelected = false;
            workOrders = new List<woWrapper>();
            if(rec.workorders__r != null){
                for(Case c:rec.workorders__r){
                    woWrapper w = new woWrapper(c);
                    w.defaultStatus = rec.Flag__c?'Quarantined-Flagged':'Quarantined';
                    workOrders.add(w);
                }
            }
            workOrders.sort();
        }
        /*********************************************************************************************************************************************/
        public integer CompareTo(Object compareTo){
            cacheWrapper otherProperty = (cacheWrapper)compareTo;//property to compare against
            //use workOrders[0] b/c it is known that work orders are pre-sorted on each cache wrapper
            return workOrders[0].compareTo(otherProperty.workOrders[0]);
        }
        /*********************************************************************************************************************************************/
    }
    public cacheWrapper[] geocode_list {get; private set;} // Working set of geocode records
    
    public WOQuarantineController() {
        showFlagged = false;
        load_list();
    }
    
    private void load_list() {
        geocode_cache__c[] tmplist;
        
        // Get all Geocode cache records that are marked as quarantined
        if(showFlagged==true){
        tmplist = [select address_hash__c, 
                                             formatted_address__c,
                                             Formatted_Street_Address__c,
                                             Formatted_City__c,
                                             Formatted_State__c,
                                             Formatted_Zip_Code__c, 
                                             Street_Address__c,
                                             City__c,
                                             State__c,
                                             Zip_Code__c,
                                             geocode_status__c,     
                                             location__latitude__s, 
                                             location__longitude__s, 
                                             manual_location__c, 
                                             partial_match__c, 
                                             quarantined__c,
                                             notes__c,
                                             flag__c,
                                             geo_system__c,
                                             geo_timestamp__c,
                                             (SELECT id, 
                                                     casenumber, 
                                                     Work_Ordered_Text__c,
                                                     Due_Date__c,
                                                     client__c,
                                                     override_quarantine__c,
                                                     loan_type__c,
                                                     reference_number__c
                                              from workorders__r
                                              where override_quarantine__c = false
                                              and approval_status__c in: approvalStatusesToCheck
                                              order by Due_Date__c ASC)
                                        from geocode_cache__c 
                                        where (partial_match__c = true or quarantined__c = true)
                                        order by id
                                        ];
        }else{
            tmplist = [select address_hash__c, 
                                             formatted_address__c,
                                             Formatted_Street_Address__c,
                                             Formatted_City__c,
                                             Formatted_State__c,
                                             Formatted_Zip_Code__c,
                                             Street_Address__c,
                                             City__c,
                                             State__c,
                                             Zip_Code__c,  
                                             geocode_status__c,     
                                             location__latitude__s, 
                                             location__longitude__s, 
                                             manual_location__c, 
                                             partial_match__c, 
                                             quarantined__c,
                                             notes__c,
                                             flag__c,
                                             geo_system__c,
                                             geo_timestamp__c,
                                             (SELECT id, 
                                                     casenumber, 
                                                     Work_Ordered_Text__c,
                                                     Due_Date__c,
                                                     client__c,
                                                     override_quarantine__c,
                                                     loan_type__c,
                                                     reference_number__c
                                              from workorders__r
                                              where override_quarantine__c = false
                                              and approval_status__c in: approvalStatusesToCheck
                                              order by Due_Date__c ASC)
                                        from geocode_cache__c 
                                        where (partial_match__c = true or quarantined__c = true)
                                        and flag__c = false
                                        order by id
                                        ];
        }
                                        
        geocode_list = new cacheWrapper[] {};
        total = 0;
        if(tmplist.size() > 0) {
            for(geocode_cache__c g : tmplist) {
                if (g.workorders__r.size() > 0) {
                    total += g.workorders__r.size();
                    geocode_list.add(new cacheWrapper(g));
                }
            }
            geocode_list.sort();
        }
    }
    public pageReference refresh(){
        return null;
    }
    public pageReference toggleShowFlagged(){
        if(showFlagged==true){
            showFlagged=false;
        }else{
            showFlagged=true;
        }
        return reload();
    }
    public pageReference reload(){
        load_list();
        return null;
    }
    // Remove an item from the quarantine
    public pageReference releaseItem() {
        /*6-10-2013 instantiate a new case list to hold work orders that are being reassigned*/
        List <Case> cases = new List<Case>();
        /*************************************************************************************/
        for(cacheWrapper rec : geocode_list) {
            if(rec.geo.id == geocode_param_id) {
                //system.debug('releasting item');
                rec.geo.quarantined__c = false;
                /*6-10-2013 iterate through work orders within the wrapper and append them to the cases list for updating*/
                for(woWrapper w : rec.workOrders){
                    w.wo.Automatic_Reassignment__c = true;
                    cases.add(w.wo);
                }
                /*********************************************************************************************************/
                update rec.geo;
                load_list();
                return null;
            }
        }
        if(cases.Size()>0){
            update cases;
        }
        return null;
    }
    
    /*/ Release a set of items from the quarantine (complete geocodes only)
    public pageReference releaseSelectedItems() {
        //6-10-2013 instantiate a new case list to hold work orders that are being reassigned
        List <Case> cases = new List<Case>();
        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        geocode_cache__c[] savelist = new geocode_cache__c[] {};
        for(cacheWrapper rec : geocode_list) {
            if(rec.isSelected) {
                if( ! rec.geo.partial_match__c)  {
                    // Only release "GOOD" geocodes
                    rec.geo.quarantined__c = false;
                    savelist.add(rec.geo);
                    //6-10-2013 iterate through work orders within the wrapper and append them to the cases list for updating
                    for(woWrapper w : rec.workOrders){
                        w.wo.Automatic_Reassignment__c = true;
                        cases.add(w.wo);
                    }
                    update cases;
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                }
            }
        }
        
        upsert savelist;
        load_list();
        
        return null;
    }*/
    
    // Retry geocoding addresses
    public pageReference GeocodeItem() {
        save_list = new geocode_cache__c[] {};
        string[] id_list = geocode_param_id.split(':');
        for(string id: id_list) {
            for(cacheWrapper rec : geocode_list) {
                if(rec.geo.id == id) {
                    //system.debug('!!!! Current GeocodeItem Record: ' + rec.geo);
                    doReGeocode(rec.geo);
                    //save_list.add(rec.geo);
                    //load_list();
                    //  return null;
                }
            }
        }
        upsert save_list;
        return null;
    }
    
    
    public pageReference updateWO() {
        /*6-10-2013 instantiate a new case list to hold work orders that are being reassigned*/
        List <Case> cases = new List<Case>();
        /*************************************************************************************/
        //system.debug('updatewo param value ' + decimal.valueof(param_lat));
        
        try{
            for(cacheWrapper rec : geocode_list) {
                if(rec.geo.id == geocode_param_id) {
                    param_lat = param_lat.trim();
                    param_lng = param_lng.trim();
                    //system.debug('updatewo param value ' + decimal.valueof(param_lat));
                    rec.geo.formatted_address__c = GeoUtilities.formatAddress(param_street, param_city, param_state, param_zip);
                    rec.geo.formatted_street_address__c = param_street; 
                    rec.geo.formatted_city__c = param_city;
                    rec.geo.formatted_state__c = param_state;
                    rec.geo.formatted_zip_code__c = param_zip;
                    rec.geo.location__latitude__s = decimal.valueof(param_lat);
                    rec.geo.location__longitude__s = decimal.valueof(param_lng);
                    rec.geo.notes__c = param_notes;
                    rec.geo.manual_location__c = true;
                    rec.geo.partial_match__c = false;
                    rec.geo.quarantined__c = false;
                    rec.geo.Flag__c = false;
                    rec.geo.geocode_status__c = 'OK';
                    rec.geo.geo_system__c = 'WOQuarantineController.cls';
                    rec.geo.geo_timestamp__c = Datetime.now();
                    
                    //system.debug('record value: ' + rec.geo.location__latitude__s);
                    
                    //Code added - Padmesh Soni (1/12/2015) - CCP-15:Fill County on Property
					//County field populates
                    rec.geo.County__c = param_county;
                    
                    upsert rec.geo;
                    /*6-10-2013 iterate through work orders within the wrapper and append them to the cases list for updating*/
                        for(woWrapper w : rec.workOrders){
                            w.wo.Approval_Status__c = 'Not Started';
                            w.wo.Automatic_Reassignment__c = true;
                            cases.add(w.wo);
                        }
                        update cases;
                    /*********************************************************************************************************/
                    
                    load_list();
                    
                    return null;
                }
            }
        }
        catch(Exception e){
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.error, e.getMessage()+'*****'+e.getStackTraceString()));
        }
        
        return null;
    }
    
    private void doReGeocode(geocode_cache__c geo) {
        
        //system.debug('!!! geo : ' + geo);
        
        // Send the saved address back through the geocoder
        map<string, string> geoCodeResult = GeoUtilities.getGeoData(geo.formatted_address__c);
        
        // Update the geocode record
        //geo.formatted_address__c = geoCodeResult.get('formatted');
        geo.location__latitude__s = decimal.valueOf(geoCodeResult.get('lat'));
        geo.location__longitude__s = decimal.valueOf(geoCodeResult.get('lng'));
        geo.last_geocoded__c = date.today();
        geo.manual_location__c = false;
        geo.partial_match__c = boolean.ValueOf(geoCodeResult.get('partial'));
        geo.geocode_status__c = geoCodeResult.get('status');
        geo.geo_system__c = 'WOQuarantineController.cls';
        geo.geo_timestamp__c = Datetime.now();
        geo.url_used__c = geoCodeResult.get('url'); 
        
        //Code added - Padmesh Soni (12/11/2014) - CCP-15: Fill County on Property
		//County assignment
        geo.County__c = geoCodeResult.get('administrative_area_level_2');
        
        // save
        save_list.add(geo);
    }
    
    public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}