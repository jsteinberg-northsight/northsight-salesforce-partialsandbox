/**
 *  Purpose         :   This class is to hold the zillow response wrapper.
 *
 *  Create By       :	Padmesh Soni
 *
 *  Created Date    :   12/11/2013
 *
 *  Current Version :   V1.0
 *
 *  Revision Log    :   V1.0(Created)
 **/
public class ZillowResponseWrapper {
    
    //20170314 - Tyler H. - adding so that we can store the raw xml in tables to verify functionality of api result digestion
    public string rawXML{
    	get{
    		return DOMdoc.toXmlString();
    	}    	
    }
    
    public dom.document DOMdoc{
    	get{
    		return DOMdoc;
    	}
    	private set;
    }
    
    //Class poperties
    public String address {get; set;}
    public String citystatezip {get; set;}
    public String message {get; set;}
    public String code {get; set;}
    public String zpid {get; set;}
    
    //Home Links
    public String homedetails {get; set;}
    public String graphsanddata {get; set;}
    public String mapthishome {get; set;}
    public String comparables {get; set;}
    
    //Address Variables
    public String street {get; set;}
    public String zipcode {get; set;}
    public String city {get; set;}
    public String state {get; set;}
    public String latitude {get; set;}
    public String longitude {get; set;}
    
    //Other Information
    public String FIPScounty {get; set;}
    public String useCode {get; set;}
    public String taxAssessmentYear {get; set;}
    public String taxAssessment {get; set;}
    public String yearBuilt {get; set;}
    public String lotSizeSqFt {get; set;}
    public String finishedSqFt {get; set;}
    public String bathrooms {get; set;}
    public String bedrooms {get; set;}
    public String totalRooms {get;set;}
    public String lastSoldDate {get; set;}
    public String lastSoldPrice {get; set;}
    
    //ZEstimate Variables
    public String amount {get; set;}
    public String lastupdated {get; set;}
    public String oneWeekChange {get; set;}
    public String valueChange {get; set;}
    public String valueChangeAttributeValue {get; set;}
    public String low {get; set;}
    public String high {get; set;}
    public String percentile {get; set;}
    
    //Local Real Estate Variables
    public String regionName {get; set;}
    public String regionID {get; set;}
    public String regionType {get; set;}
    public String regionZindexValue {get; set;}
    public String links1 {get; set;}
    public String overview {get; set;}
    public String forSaleByOwner {get; set;}
    public String forSale {get; set;}
    
    //Constructor definition
    public ZillowResponseWrapper(Dom.Document document) {
    	
    	//System.debug('document ::::::'+ document);
    	
    	//process the request only if document is not null
        if(document != null) {
            DOMdoc=document;
            
            //Retrieve the root element for this document.
            Dom.XMLNode rootElement = document.getRootElement();
            //System.debug('document:::::' + rootElement);
            
            //Retrieve the child element from the root element
            Dom.XmlNode request = rootElement.getChildElement('request', null);
            
            //Check for request not null
            if(request != null) {
             
                //Retrieve the child element from the request
                address = request.getChildElement('address', null).getText();
                citystatezip = request.getChildElement('citystatezip', null).getText();
            }
            
            //Retrieve the child element message from root element
            Dom.XmlNode messageNode = rootElement.getChildElement('message', null);
            
            //Check for messageNode
            if(messageNode != null) {
            
                //Retrieve the child element from message
                message = messageNode.getChildElement('text', null).getText();
                code = messageNode.getChildElement('code', null).getText();
            }
                
            //Rerieve the child element response from root element
            Dom.XmlNode responseNode = rootElement.getChildElement('response', null);
            
            //Check for the responseNode
            if(responseNode != null) {
                Dom.XmlNode resultsNode = responseNode.getChildElement('results', null);
                Dom.XmlNode resultNode = resultsNode.getChildElement('result', null);
                
                if(resultNode.getChildElement('zpid', null) != null)
                    zpid = resultNode.getChildElement('zpid', null).getText();
                
                //Retrieve links and homedetatils from result
                Dom.XmlNode linksNode = resultNode.getChildElement('links', null);
                
                if(linksNode.getChildElement('homedetails', null) != null)
                    homedetails = linksNode.getChildElement('homedetails', null).getText();
                
                //Retrieve graphsenddata
                if(linksNode.getChildElement('graphsanddata', null) != null)
                    graphsanddata = linksNode.getChildElement('graphsanddata', null).getText();
                if(linksNode.getChildElement('mapthishome', null) != null)
                    mapthishome = linksNode.getChildElement('mapthishome', null).getText();
                if(linksNode.getChildElement('comparables', null) != null)
                    comparables = linksNode.getChildElement('comparables', null).getText();
                
                //Address Information
                Dom.XmlNode address1Node = resultNode.getChildElement('address', null);
                if(address1Node.getChildElement('street', null) != null)
                    street = address1Node.getChildElement('street', null).getText();
                if(address1Node.getChildElement('zipcode', null) != null)
                    zipcode = address1Node.getChildElement('zipcode', null).getText();
                if(address1Node.getChildElement('city', null) != null)
                    city = address1Node.getChildElement('city', null).getText();
                if(address1Node.getChildElement('state', null) != null)
                    state = address1Node.getChildElement('state', null).getText();
                if(address1Node.getChildElement('latitude', null) != null)
                    latitude = address1Node.getChildElement('latitude', null).getText();
                if(address1Node.getChildElement('longitude', null) != null)
                    longitude = address1Node.getChildElement('longitude', null).getText();
                
                //FIPScounty
                if(resultNode.getChildElement('FIPScounty', null) != null)
                    FIPScounty = resultNode.getChildElement('FIPScounty', null).getText();
                if(resultNode.getChildElement('useCode', null) != null)
                    useCode = resultNode.getChildElement('useCode', null).getText();
                if(resultNode.getChildElement('taxAssessmentYear', null) != null)
                    taxAssessmentYear = resultNode.getChildElement('taxAssessmentYear', null).getText();
                if(resultNode.getChildElement('taxAssessment', null) != null)
                    taxAssessment = resultNode.getChildElement('taxAssessment', null).getText();
                if(resultNode.getChildElement('yearBuilt', null) != null)
                    yearBuilt = resultNode.getChildElement('yearBuilt', null).getText();
                if(resultNode.getChildElement('lotSizeSqFt', null) != null)
                    lotSizeSqFt = resultNode.getChildElement('lotSizeSqFt', null).getText();
                if(resultNode.getChildElement('finishedSqFt', null) != null)
                    finishedSqFt = resultNode.getChildElement('finishedSqFt', null).getText();
                if(resultNode.getChildElement('bathrooms', null) != null)
                    bathrooms = resultNode.getChildElement('bathrooms', null).getText();
                if(resultNode.getChildElement('bedrooms', null) != null)
                    bedrooms = resultNode.getChildElement('bedrooms', null).getText();
                if(resultNode.getChildElement('totalRooms', null) != null)
                    totalRooms = resultNode.getChildElement('totalRooms', null).getText();
                if(resultNode.getChildElement('lastSoldDate', null) != null)
                    lastSoldDate = resultNode.getChildElement('lastSoldDate', null).getText();
                if(resultNode.getChildElement('lastSoldPrice', null) != null)
                    lastSoldPrice = resultNode.getChildElement('lastSoldPrice', null).getText();
                
                //zestimate
                Dom.XmlNode zestimateNode = resultNode.getChildElement('zestimate', null); 
                if(zestimateNode.getChildElement('amount', null) != null)
                    amount = zestimateNode.getChildElement('amount', null).getText();
                if(zestimateNode.getChildElement('last-updated', null) != null)
                    lastupdated = zestimateNode.getChildElement('last-updated', null).getText();
                if(zestimateNode.getChildElement('oneWeekChange', null) != null)
                    oneWeekChange = zestimateNode.getChildElement('oneWeekChange', null).getText();
                if(zestimateNode.getChildElement('valueChange', null) != null)
                    valueChange = zestimateNode.getChildElement('valueChange', null).getText();
                    valueChangeAttributeValue = zestimateNode.getChildElement('valueChange', null).getAttributeValue('duration', null);

                //valuationRange
                Dom.XmlNode valuationRangeNode = zestimateNode.getChildElement('valuationRange', null); 
                if(valuationRangeNode.getChildElement('low', null) != null)
                    low = valuationRangeNode.getChildElement('low', null).getText();
                if(valuationRangeNode.getChildElement('high', null) != null)
                    high = valuationRangeNode.getChildElement('high', null).getText();
                
                //percentile
                if(zestimateNode.getChildElement('percentile', null) != null)
                    percentile = zestimateNode.getChildElement('percentile', null).getText();
                
                //localRealEstate 
                Dom.XmlNode localRealEstateNode = resultNode.getChildElement('localRealEstate', null);
                if(localRealEstateNode.getChildElement('region', null) != null) { 
                	
                    Dom.XmlNode regionNode = localRealEstateNode.getChildElement('region', null);
                    Dom.XmlNode links1Node = regionNode.getChildElement('links', null);
                    Dom.XmlNode zindexValueNode = regionNode.getChildElement('zindexValue', null);
                    
                    regionName = regionNode.getAttributeValue('name', regionNode.getNamespace());
                    regionID = regionNode.getAttributeValue('id', regionNode.getNamespace());
                    regionType  = regionNode.getAttributeValue('type', regionNode.getNamespace());
                    
                    
                    if(regionNode.getChildElement('zindexValue', null)!=null)
                    	regionZindexValue=regionNode.getChildElement('zindexValue', null).getText();
                    if(links1Node.getChildElement('overview', null) != null)
                        overview = links1Node.getChildElement('overview', null).getText();
                    if(links1Node.getChildElement('forSaleByOwner', null) != null)
                        forSaleByOwner = links1Node.getChildElement('forSaleByOwner', null).getText();
                    if(links1Node.getChildElement('forSale', null) != null)
                        forSale = links1Node.getChildElement('forSale', null).getText();
                }
            }
        }
    }
}