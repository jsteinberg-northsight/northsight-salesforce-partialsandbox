@isTest
private class MultiStatusControllerTest 
{
    
//    static testMethod void defaultTest() 
//    {
//        Set<string> ApprovalStatusSet = new Set<string>{'Not Started','Rejected'};
//        Set<string> OwnerNameSet = new Set<string>{'CANCELED','REO Unassigned'};
//        
//        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
//        User user1 = new User(Alias = 'user1', Email='user1@example.com', firstName='test', 
//            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
//            LocaleSidKey='en_US', ProfileId = p.Id,
//            TimeZoneSidKey='America/Los_Angeles', UserName='user1-gvp-AccountSchedulerControllerTest@example.com');
//        insert user1;
//        
//        //User user1 = [select id, Name from User where id = '005f0000000M1FM'];
//        //system.debug(user1.Name);
//        
//        RecordType types = [select Id from RecordType where Name = 'REO'];
//        system.assertEquals('01240000000UPMoAAO', types.Id);
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account; 
//        
//        Case testCase = new Case(Street_Address__c = '12 street', Vendor_Code__c = '10', Approval_Status__c = 'Not Started', status='RUSH');
//        testCase.Client_Name__c = account.Id;
//        testCase.RecordType = types;
//        testCase.OwnerId = user1.Id;
//        insert testCase;
//        testCase.OwnerId = user1.Id;
//        update testCase;
//        
//        Case checkCase = [select RecordType.Name, OwnerId, Approval_Status__c, Status, Owner.Name from Case where Vendor_Code__c = '10'];
//        system.assertEquals('REO', checkCase.RecordType.Name);
//        //system.assertEquals('ted', checkCase.Owner.Name);
//        system.assertEquals('Not Started', checkCase.Approval_Status__c);
//        system.assertEquals('RUSH', checkCase.Status);
//        
//        Case testCase2 = new Case(Street_Address__c = '12 street', Vendor_Code__c = '01', Approval_Status__c = 'Not Started');
//        testCase2.RecordType = types;
//        testCase2.OwnerId = User1.Id;
//        insert testCase2;
//        testCase2.OwnerId = user1.Id;
//        testCase2.Client_Name__c = account.Id;
//        update testCase2;
//        
//        Status__c testStatus = new Status__c();
//        testStatus.Case__c = testCase.Id;
//        testStatus.Expected_Upload_Date__c = Date.Today();
//        testStatus.Delay_Reason__c = 'Test';
//        testStatus.Order_Status__c = 'Done';        
//        insert testStatus;
//        
//        List<Case> currentCase = [select CaseNumber,
//                                                    Scheduled_Date__c,
//                                                    Status,
//                                                    Street_Address__c, 
//                                                    City__c,
//                                                    State__c,
//                                                    Zip_Code__c,
//                                                    Work_Ordered__c,
//                                                    Client_Number__c,
//                                                    Notes__c,
//                                                    Status_Health__c,
//                                                    Owner.Name
//                                                from 
//                                                    Case 
//                                                where 
//                                                    RecordType.Name = 'REO' and
//                                                    Approval_Status__c IN: ApprovalStatusSet and
//                                                    Status != 'Canceled'];
//                                                    
//        system.assertEquals(2, currentCase.size());
//        system.debug(currentCase[0].Owner.Name);
//        
//        MultiStatusController testController = new MultiStatusController();
//        testController.Status.Delay_Reason__c = 'Other';
//        testController.Status.Expected_Upload_Date__c = Date.Today();
//        testController.Status.Explanation__c = 'test data';
//        testController.Status.Order_Status__c = 'On Route';
//        
//        //testController.SelectedQuery = '1';
//        //testController.LoadQuery();
//        
//        testController.WorkOrderList[0].Selectbox = true;
//        testController.WorkOrderList[1].Selectbox = true;
//        
//        testController.SelectedQuery = '2';
//        testController.LoadQuery();
//        testController.WorkOrderList[0].Selectbox = true;
//        testController.WorkOrderList[1].Selectbox = true;
//        
//        testController.SelectedQuery = '3';
//        testController.LoadQuery();
//        testController.WorkOrderList[0].Selectbox = true;
//        testController.WorkOrderList[1].Selectbox = true;
//        
//        testController.ShowStatus();
//         
//        testController.SaveStatus();
//        
//        testController.WorkOrderList[0].Selectbox = false;
//        testController.WorkOrderList[1].Selectbox = false;
//        testController.ShowStatus();   
//        
//        testController.BackToHomePage();
//        testController.Cancel();
//        
//        string htmlTest = '';
//        for (integer i = 0; i < 10000; i++)
//        {
//            htmlTest = htmlTest + '<table><tr><td>test</td></tr></table>';
//        }
//        MultiStatusController.SaveHtml(htmlTest);
//    }
//    
//    //Padmesh Soni (06/23/2014) - Support Case Record Type
//    //New test method added for code coverage and check for RecordType filter excluding filters
//    static testMethod void testMultiStatusAfterRTFilterAdded() {
//        
//        Set<string> ApprovalStatusSet = new Set<string>{'Not Started','Rejected'};
//        Set<string> OwnerNameSet = new Set<string>{'CANCELED','REO Unassigned'};
//        
//        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
//        User user1 = new User(Alias = 'user1', Email='user1@example.com', firstName='test', 
//            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
//            LocaleSidKey='en_US', ProfileId = p.Id,
//            TimeZoneSidKey='America/Los_Angeles', UserName='user1-gvp-AccountSchedulerControllerTest@example.com');
//        insert user1;
//        
//        //Query result of RecordType of Case object
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
//                                            OR DeveloperName = 'REO') AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true ORDER BY DeveloperName];
//        
//        //assert statement
//        system.assertEquals(2, recordTypes.size());
//        
//        //List to hold test records of Case sobject 
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//            AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//        insert account; 
//        
//        List<Case> workOrders = new List<Case>();
//        workOrders.add(new Case(Street_Address__c = '12 street', Vendor_Code__c = '10', Approval_Status__c = 'Not Started', status='RUSH',
//                                    RecordTypeId = recordTypes[0].Id, OwnerId = user1.Id,Client_Name__c = account.Id)); 
//        workOrders.add(new Case(Street_Address__c = '12 street', Vendor_Code__c = '10', Approval_Status__c = 'Not Started', status='RUSH',
//                                    RecordTypeId = recordTypes[1].Id, OwnerId = user1.Id,Client_Name__c = account.Id)); 
//        workOrders.add(new Case(Street_Address__c = '12 street', Vendor_Code__c = '01', Approval_Status__c = 'Not Started', 
//                                    RecordTypeId = recordTypes[0].Id, OwnerId = User1.Id,Client_Name__c = account.Id));
//        workOrders.add(new Case(Street_Address__c = '12 street', Vendor_Code__c = '01', Approval_Status__c = 'Not Started', 
//                                    RecordTypeId = recordTypes[1].Id, OwnerId = User1.Id,Client_Name__c = account.Id));
//        
//        insert workOrders;
//        
//        Status__c testStatus = new Status__c(Case__c = workOrders[0].Id, Expected_Upload_Date__c = Date.Today(), Delay_Reason__c = 'Test',
//                                                Order_Status__c = 'Done');      
//        insert testStatus;
//        
//        List<Case> currentCase = [select CaseNumber,
//                                                    Scheduled_Date__c,
//                                                    Status,
//                                                    Street_Address__c, 
//                                                    City__c,
//                                                    State__c,
//                                                    Zip_Code__c,
//                                                    Work_Ordered__c,
//                                                    Client_Number__c,
//                                                    Notes__c,
//                                                    Status_Health__c,
//                                                    Owner.Name
//                                                from 
//                                                    Case 
//                                                where 
//                                                    RecordType.Name = 'REO' and
//                                                    Approval_Status__c IN: ApprovalStatusSet and
//                                                    Status != 'Canceled'];
//                                                    
//        system.assertEquals(2, currentCase.size());
//        system.debug(currentCase[0].Owner.Name);
//        
//        MultiStatusController testController = new MultiStatusController();
//        testController.Status.Delay_Reason__c = 'Other';
//        testController.Status.Expected_Upload_Date__c = Date.Today();
//        testController.Status.Explanation__c = 'test data';
//        testController.Status.Order_Status__c = 'On Route';
//        
//        //testController.SelectedQuery = '1';
//        //testController.LoadQuery();
//        
//        testController.WorkOrderList[0].Selectbox = true;
//        testController.WorkOrderList[1].Selectbox = true;
//        
//        testController.SelectedQuery = '2';
//        testController.LoadQuery();
//        testController.WorkOrderList[0].Selectbox = true;
//        testController.WorkOrderList[1].Selectbox = true;
//        
//        testController.SelectedQuery = '3';
//        testController.LoadQuery();
//        testController.WorkOrderList[0].Selectbox = true;
//        testController.WorkOrderList[1].Selectbox = true;
//        
//        testController.ShowStatus();
//         
//        testController.SaveStatus();
//        
//        testController.WorkOrderList[0].Selectbox = false;
//        testController.WorkOrderList[1].Selectbox = false;
//        testController.ShowStatus();   
//        
//        testController.BackToHomePage();
//        testController.Cancel();
//        
//        string htmlTest = '';
//        for (integer i = 0; i < 10000; i++)
//        {
//            htmlTest = htmlTest + '<table><tr><td>test</td></tr></table>';
//        }
//        MultiStatusController.SaveHtml(htmlTest);
//    }
}