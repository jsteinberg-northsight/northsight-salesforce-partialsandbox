public with sharing class RouteWrapper {

	private Route__c route;
	public string name {
		get{
			return route.name;
		}
	}
	public string Id {
		get{
			return route.Id;
		}
	}
	public string label {
		get{
			return route.Route_Label__c;
		}
		set{
			route.Route_Label__c = value;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Info,value));
		}
	}
	public List<RouteStopWrapper> stops{get;private set;}
	private List<RouteStopWrapper> originalStops;
	public RouteWrapper(){
		route = new Route__c();
		stops = new RouteStopWrapper[]{};
		originalStops = stops.clone();
	}
	public string distanceDuration{
		get{
			return route.distance_duration__c;
		}
		set{
			route.distance_duration__c = value;
		}
	}
	public string serializedMapMetaData{
		get{
			return route.serializedMapMetaData__c;
			}
		set{
			route.serializedMapMetaData__c = value;
		}
		}
	public string Directions{
	   get{
	   		return route.Directions__c;
	   }
	   set{
	   		route.Directions__c = value.unescapeHtml4();
	   }
	}
	public boolean avoidHighways{
		get{
			return route.Avoid_Highways__c;
		}
		set{
			route.Avoid_Highways__c = value;
		}
	}
	public boolean calcRoundTrip{
		get{
			return route.Calculate_Round_Trip__c;
		}
		set{
			route.Calculate_Round_Trip__c = value;
		}
	}
	public string EstCompletionDate{
		get{
			return route.Est_Completion_Date__c==null?'':route.Est_Completion_Date__c.Format();
		}
		set{
			try{
				route.Est_Completion_Date__c = Date.Parse(value);
			}catch(Exception e){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Unable to parse date -'+e+' : '+e.getStackTraceString()));
			
			}
		}
	}
	public integer Number_Of_Stops{
		get{return stops.Size();}
	}
	public string owner{
		get{ return route.Owner.name; }
	}
	public datetime createdDate {
		get{ return route.createdDate; }
	}
	public string permaLink {
		get{ return route.Permalink__c; }
	}
	public string permaLinkDirections {
		get{ return route.Permalink_directions__c; }
	}
	public string urlName {
		get{ return getURLName(route); }
	}
	
	public RouteWrapper(Route__c r){
		route = r;
		stops = new RouteStopWrapper[]{};
		originalStops = new RouteStopWrapper[]{};
		if(route.Route_Stops__r!=null){
			this.setRouteStops(r.Route_Stops__r);
			for(Route_Stop__c s:r.route_Stops__r){
				originalStops.add(new RouteStopWrapper(s));
			}
		}
	
		
	}
	public static RouteWrapper fetchRoute(Id routeId){
		Route__c r = [SELECT CreatedById, 
							 CreatedDate, 
							 IsDeleted, 
							 LastModifiedById, 
							 LastModifiedDate, 
							 OwnerId, 
							 Id, 
							 Route_Label__c, 
							 Name, 
							 SystemModstamp, 
							 	(SELECT Id, 
							 			Work_Order__r.Geocode_Cache__r.Formatted_Address__c,
							 			Route__c,
							 			Name, 
							 			Address__c,
							 			City__c,
							 			State__c,
							 			Zip_Code__c,
							 			Work_Order__r.Display_Street__c,
							 			Work_Order__r.Display_City__c,
							 			Work_Order__r.Display_State__c,
							 			Work_Order__r.Display_Zip_Code__c, 
							 			Stop_Number__c, 
							 			Work_Order__c, 
							 			Work_Order__r.Street_Address__c,
							 			Work_Order__r.City__c, 
							 			Work_Order__r.State__c, 
							 			Work_Order__r.Zip_Code__c  
							 	 FROM Route_Stops__r) 
					  FROM Route__c where id=:routeId];
		return new RouteWrapper(r);
	}
	public boolean setRouteStops(List<Route_Stop__c> routeStops){
		boolean success = true;
		this.stops = new List<RouteStopWrapper>();
		for(Route_Stop__c rs:routeStops){
			this.stops.add(new RouteStopWrapper(rs));
		}
		return success;
	}
	public boolean setRouteStops(List<RouteStopWrapper> routeStops){
		boolean success = true;
		this.stops = routeStops;
		return success;
	}
	public boolean setRouteStops(string jsonRouteStops){
		boolean success = false;
		try{
			List<RouteStopWrapper> newStops =  (List<RouteStopWrapper>)Json.deserialize(jsonRouteStops.replace('\t','   ').unescapeHtml4(), List<RouteStopWrapper>.class);
			this.stops = newStops;
		}catch(Exception e){
			success = false;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Unable to parse route stops -'+e+' : '+e.getStackTraceString()));
			if(jsonRouteStops==null){
				jsonRouteStops='null';
			}
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'JSON VALUE-'+jsonRouteStops.unescapeHtml4()));
		}
		return success;
	}
	public string getRouteStopsJSON(){
		return Json.serializePretty(this.stops);
	}
	public class RouteException extends Exception {}
	public string getGarminWaypointXML(){
		  XmlStreamWriter w = new XmlStreamWriter();
          w.writeStartDocument(null, '1.0');
          w.setDefaultNamespace('http://www.topografix.com/GPX/1/1');
          //<gpx>
          w.writeStartElement(null, 'gpx', null);
          w.writedefaultNamespace('http://www.topografix.com/GPX/1/1');
		  w.writeAttribute(null, null, 'version', '1.1');	
          w.writeAttribute(null, null, 'creator', 'OptiMap');
	          stops.sort();//make sure we're adding these in the proper order!
	          	for(RouteStopWrapper stop:stops){
					string fullAddress = stop.address+' '+stop.city+' '+stop.state+' '+stop.zip;
	          		fullAddress = 'OptiMap '+(stop.stopNumber==0?'Start':string.valueof(stop.stopNumber))+' ('+fullAddress+')';
	          		fullAddress = fullAddress.escapeXml();
	          		//<wpt>
	          		w.writeStartElement(null, 'wpt', null);
			  		w.writeAttribute(null, null, 'lat', string.valueof(stop.latitude));	
			  		w.writeAttribute(null, null, 'lon', string.valueof(stop.longitude));
			  			//<name>
			  			w.writeStartElement(null, 'name', null);
			  			w.writeCharacters(fullAddress);
			  			w.writeEndElement();//</name>
			  			//<cmt>
			  			w.writeStartElement(null, 'cmt', null);
			  			w.writeCharacters(fullAddress);
			  			w.writeEndElement();//</cmt>
			  			//<desc>
			  			w.writeStartElement(null, 'desc', null);
			  			w.writeCData(fullAddress);
			  			w.writeEndElement();//</desc>
			  			//<sym>
			  			w.writeStartElement(null, 'sym', null);
			  			w.writeCharacters(stop.stopNumber==0?'Start':'Via');
			  			w.writeEndElement();//</sym>
			  			//<type>
			  			w.writeStartElement(null, 'type', null);
			  			w.writeCdata(stop.stopNumber==0?'Start':'Via');
			  			w.writeEndElement();//</type>
			  		w.writeEndElement();//</wpt>
	          	}	

          w.writeEndElement(); //</gpx>  
          w.writeEndDocument();
          String xmlOutput = w.getXmlString();
          w.close();
          return xmlOutput;
	}
	public string getGarminRouteXML(){
		  XmlStreamWriter w = new XmlStreamWriter();
          w.writeStartDocument(null, '1.0');
          w.setDefaultNamespace('http://www.topografix.com/GPX/1/1');
          //<gpx>
          w.writeStartElement(null, 'gpx', null);
          w.writedefaultNamespace('http://www.topografix.com/GPX/1/1');
		  w.writeAttribute(null, null, 'version', '1.1');	
          w.writeAttribute(null, null, 'creator', 'OptiMap');
          //<rte>
          w.writeStartElement(null, 'rte', null);
	          //<name>
	          w.writeStartElement(null, 'name', null);
	          w.writeCharacters(this.label);
	          w.writeEndElement();//</name>
	          //<desc>
	          w.writeStartElement(null, 'desc', null);
	          w.writeCData('Route generated by Optimap');
	          w.writeEndElement();//</desc>
	          //<number>
	          w.writeStartElement(null, 'number', null);
	          w.writeCharacters('1');
	          w.writeEndElement();//</number>
	          stops.sort();//make sure we're adding these in the proper order!
	          	for(RouteStopWrapper stop:stops){
					string fullAddress = stop.address+' '+stop.city+' '+stop.state+' '+stop.zip;
	          		fullAddress = fullAddress.escapeXml();
	          		//<rtept>
	          		w.writeStartElement(null, 'rtept', null);
			  		w.writeAttribute(null, null, 'lat', string.valueof(stop.latitude));	
			  		w.writeAttribute(null, null, 'lon', string.valueof(stop.longitude));
			  			//<name>
			  			w.writeStartElement(null, 'name', null);
			  			w.writeCharacters(fullAddress);
			  			w.writeEndElement();//</name>
			  			//<cmt>
			  			w.writeStartElement(null, 'cmt', null);
			  			w.writeCharacters(fullAddress);
			  			w.writeEndElement();//</cmt>
			  			//<desc>
			  			w.writeStartElement(null, 'desc', null);
			  			w.writeCData(fullAddress);
			  			w.writeEndElement();//</desc>
			  			//<sym>
			  			w.writeStartElement(null, 'sym', null);
			  			w.writeCharacters(stop.stopNumber==0?'Start':'Via');
			  			w.writeEndElement();//</sym>
			  			//<type>
			  			w.writeStartElement(null, 'type', null);
			  			w.writeCdata(stop.stopNumber==0?'Start':'Via');
			  			w.writeEndElement();//</type>
			  		w.writeEndElement();//</rtept>
	          	}	
	      w.writeEndElement(); //</rte>   
          w.writeEndElement(); //</gpx>  
          w.writeEndDocument();
          String xmlOutput = w.getXmlString();
          w.close();
          return xmlOutput;
	}
	public string getTomTomITN(){
		string itn = '';
		for(RouteStopWrapper stop:this.stops){
			string fullAddress = stop.address+' '+stop.city+' '+stop.state+' '+stop.zip;
			itn+=string.valueOf((integer)(stop.longitude*100000))+'|'+string.valueOf((integer)(stop.latitude*100000))+'|'+ fullAddress.replace('|',':') +'|'+((stop.stopNumber==0)?'4':'2')+'|\r\n';
		}
		return itn;
	}
	
	public string getURLName(Route__c r) {
		if (r == null || r.id == null) {
			return '';
		}
		blob digest = Crypto.generateDigest('MD5', Blob.valueOf(r.id));
		if(digest==null){
			return '';
		}
		string randomchars = EncodingUtil.convertToHex(digest).substring(0, 3);
		r.securitySuffix__c = randomchars;
		return r.name + randomchars;
	}
	public boolean save(){
		Savepoint p = Database.setSavePoint();
		boolean success = false;
		try{			
			upsert this.route;
			
			// Get the new generated route name
			route__c newroute = [select name from route__c where id=:this.route.id];
			string longURLDetail = public_site.url + page.RouteShareDetail.getUrl() + '?routeid=' + this.route.id;
			string longURL = public_site.url + page.RouteShare.getUrl() + '?routeid=' + this.route.id;
			string RSDetailShortURL = getURLName(newroute);
			string RSShortURL = getURLName(newroute)+'-dir';
			
			update newroute;
			// Make sure we have a short URL defined, one for route detail and directions
			short_url__c[] new_urls = new short_url__c[] {};
			short_url__c[] check = [select id from short_url__c where url__c = :longURL];
			if(check.size()==0) {
				new_urls.add(new short_url__c(url__c = longURL , custom_url__c = RSShortURL));
			}
			short_url__c[] checkDetail = [select id from short_url__c where url__c = :longURLDetail];
			if(checkDetail.size()==0) {
							 new_urls.add(new short_url__c(url__c = longURLDetail, custom_url__c = RSDetailShortURL));
			}
			
			if(new_urls.size()>0){			
				insert new_urls;
			}
			
			integer stopNumber = 0;
			for(RouteStopWrapper rs:stops){
				rs.routeId = this.route.Id;
				rs.stopNumber = stopNumber;
				stopNumber++;
			}
			route_stop__c[] stopsToDelete = [select id from route_stop__c where route__c = :route.Id];
			if(stopsToDelete.size()>0){
				delete stopsToDelete;
			}
			RouteStopWrapper.saveList(stops);
			success = true;
		}catch(Exception e){
			success = false;
			Database.rollback(p);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,e+' : '+e.getStackTraceString()));
		}
		return success;
	}
	
//	public static testMethod void test(){
//		
//		RouteWrapper r = new RouteWrapper();
//		Case[] cases = new Case[]{};
//		for(Integer x=0;x<3;x++){
//			cases.add(new Case(Vendor_Code__c='AYSGRS', Street_Address__c='addr'+x,City__c=new string[]{'Slidell','Lacombe','Mandeville'}[x],State__c='LA',Zip_Code__c = '7046'+x,Work_Ordered_Text__c = 'GRASS CUT', General_Order_Type_Notes__c = 'MUST BE COMPLETED BY 3:00PM' ));
//		}
//		upsert cases;
//		cases = [select Id,
//						CaseNumber,
//						Geocode_Cache__r.Formatted_Address__c,
//						Display_Street__c,
//						Display_City__c,
//						Display_State__c,
//						Display_Zip_Code__c,
//						Street_Address__c,
//						City__c,
//						State__c,
//						Zip_Code__c,
//						Client_Number__c,
//						Status_Health_Text__c,
//						Vendor_Status__c,
//						Scheduled_Date__c,
//						Work_Ordered_Text__c,
//						Notes__c,
//						Description
//				 FROM Case];
//		for(Case c:cases){
//			r.stops.add(new RouteStopWrapper(c));
//		}
//		r.label = 'MyRoute';
//		r.EstCompletionDate = Date.Today().Format();
//		r.save();
//		//System.debug(r.EstCompletionDate);
//		//System.Debug(r.getRouteStopsJSON());
//		//System.Debug(r.getGarminRouteXML());
//		//System.Debug(r.getGarminWaypointXML());
//		//System.Debug(r.getTomTomITN());
//		
//		r.serializedMapMetaData = 'abc';
//		r.directions = 'abc';
//		//System.Debug(r.serializedMapMetaData);
//		//System.Debug(r.name);
//		//System.Debug(r.directions);
//		//System.Debug(r.avoidHighways);
//	} 
//

public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}


}