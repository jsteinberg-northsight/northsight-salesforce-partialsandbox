public without sharing class EleventhHrQueryResultsController {
	/*
	public List<Case> approvedOrders {get;set;}
	public List<Case> pendingOrders {get;set;}
	public List<Case> otherOrders {get;set;}
	
	private Set<Id> casesToAvoid;
	private Date currentDay = Date.today();
	private Datetime twoHours = Datetime.now().addHours(-2);
	private Set<String> statuses = new Set<String>(new string[]{'Sent', 'Unsent', 'In Progress'});
	
	
	public EleventhHrQueryResultsController(){
		casesToAvoid = new Set<Id>();
		approvedOrders = new List<Case>();
		pendingOrders = new List<Case>();
		otherOrders = new List<Case>();
		approvedOrders = queryApprovedOrders();
		pendingOrders = queryPendingOrders();
		otherOrders = queryOtherOrders();
	}
	
	private List<Case> queryApprovedOrders(){
		List<Case> approvedOrdersList = [SELECT Id, 
												Updated_Date_Time__c, 
												(Select Id 
												 From Client_Commitments__r 
												 Where Sync_Status__c IN: statuses 
												 And Commit_Date_Valid__c = true 
												 order by CreatedDate desc limit 1) 
										 FROM Case 
										 WHERE Client__c = 'SG' 
										 AND Commit_By_Date__c <=: currentDay 
										 AND Approval_Status__c = 'Approved' 
										 AND Last_Open_Verification_DateTime__c >: twoHours 
										 AND (Client_Commit_Date__c = null OR Client_Commit_Date__c <=: currentDay)];
		
		if(approvedOrdersList.size() > 0){
			for(Case c : approvedOrdersList){
				casesToAvoid.add(c.Id);
			}
		}
		
		return approvedOrdersList;
	}
	
	private List<Case> queryPendingOrders(){
		List<Case> pendingOrdersList = [SELECT Id, 
											   Date_Serviced1__c, 
											   (Select Id 
											    From Client_Commitments__r 
											    Where Sync_Status__c IN: statuses 
											    And Commit_Date_Valid__c = true 
											    order by CreatedDate desc limit 1) 
										FROM Case 
										WHERE Client__c = 'SG'  
										AND Commit_By_Date__c <=: currentDay 
										AND Approval_Status__c = 'Pending' 
										AND Special_Review__c = 'No' 
										AND Last_Open_Verification__c =: currentDay 
										AND (Client_Commit_Date__c = null OR Client_Commit_Date__c <=: currentDay)];
		
		if(pendingOrdersList.size() > 0){
			for(Case c : pendingOrdersList){
				casesToAvoid.add(c.Id);
			}
		}
		
		return pendingOrdersList;
	}
	
	private List<Case> queryOtherOrders(){
		List<Case> otherOrdersList = [SELECT Id, 
											 (Select Id 
											  From Client_Commitments__r 
											  Where Sync_Status__c IN: statuses 
											  And Commit_Date_Valid__c = true 
											  order by CreatedDate desc limit 1) 
									  FROM Case 
									  WHERE Id NOT IN : casesToAvoid 
									  AND Client__c = 'SG'
									  AND Commit_By_Date__c <=: currentDay 
									  AND Approval_Status__c != 'Approved' 
									  AND Last_Open_Verification__c =: currentDay 
									  AND (Client_Commit_Date__c = null OR Client_Commit_Date__c <=: currentDay)];
		
		return otherOrdersList;
	}
*/
}