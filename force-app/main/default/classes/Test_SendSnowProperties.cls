@isTest()
public with sharing class Test_SendSnowProperties {
//public static testmethod void doTest(){
//
//
//        
//        //Query record of Record Type of Account and Case objects
//    	//Padmesh Soni (06/19/2014) - Support Case Record Type
//		//Query one more record type of Case Object "SUPPORT"    
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client'
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(3, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accountsOnOrders = new List<Account>();
//    	accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accountsOnOrders;
//		
//		//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'OH',
//    								Department__c = 'Test', Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual', 
//    								Updated_Date_Time__c = DateTime.now()));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'OH', 
//    								Updated_Date_Time__c = DateTime.now().addHours(2)));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//    								Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'LA',
//    								Department__c = 'Test', Assignment_Program__c = 'Mannual', Updated_Date_Time__c = DateTime.now(), Vendor_Code__c = 'FOLLOW'));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'AR',
//    								Department__c = 'Test1', Work_Ordered__c = 'Grass Initial Cut', Assignment_Program__c = 'OrderSelect', 
//    								Updated_Date_Time__c = DateTime.now().addHours(5), Vendor_Code__c = 'FOLLOW'));
//    	
//    	//Padmesh Soni (06/19/2014) - Support Case Record Type
//    	//New record created for Customer support recordtypes
//		workOrders.add(new Case(RecordTypeId = recordTypes[2].Id, AccountId = accountsOnOrders[1].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//    								Client__c = AssignmentMapConstants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = 'Maintenance', State__c = 'AZ',
//    								Department__c = 'Test1', Work_Ordered__c = 'Grass Initial Cut', Assignment_Program__c = 'OrderSelect', 
//    								Updated_Date_Time__c = DateTime.now().addHours(5), Vendor_Code__c = 'FOLLOW'));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//    	//Qurey result of Property
//    	List<Geocode_Cache__c> properties = [SELECT Name, Active__c FROM Geocode_Cache__c WHERE State__c IN ('OH', 'AZ')];
//    	
//    	update properties;
//    	
//    	//Test starts here
//    	Test.startTest();
//		Test.setMock(HttpCalloutMock.class, new WeatherAlertCalloutMock());    	
//    	//Execute the batch
//    	Database.executeBatch(new Batch_SendAssetsToVAlert(), 200);
//    	//Geocode_Cache__c[] props  = [SELECT  Id,Name, Formatted_Address__c, VAlert_Id__c FROM Geocode_Cache__c Where VAlert_Id__c = null and Monitor_Snow__c = true ];
//    	//WeatherAssetHelper.pushAssetsToVAlert(props);
//    	//Test stops here
//    	Test.stopTest();
//    	
//    	//Qurey result of Property
//    	
//    	//Assert statement
//    	//Padmesh Soni (06/19/2014) - Support Case Record Type
//    	//assert statement updated from 3 to 4
//		
//    }
}