/**
 *  Purpose         :   This batch class is used for getting response form zillow for Porperty records which are not having lot size. 
 *
 *  Created By      :   
 *
 *  Created Date    :   
 *
 *  Current Version :   V1.1
 *
 *  Revision Log    :   V1.0 - Created
 *                      V1.1 - Modified - Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements - Modified for test method with adding isRunning 
 *                                        test concept.   
 **/
global without sharing class BatchGetZillowLotSize implements Database.Batchable<Geocode_Cache__c>, Database.AllowsCallouts{
    public List<Geocode_Cache__c> propertiesList;
    public ZillowResponseWrapper zillowResWrapper;
    public String streetAddressParameter;
    public String cityParameter;
    public String stateParameter;
    public String zipCodeParameter;
    public Integer limitCounter = 0;//zillow api limit counter
    public String responseXMLString = '';
    
    global Iterable<Geocode_Cache__c> start(Database.BatchableContext BC){//this method will create a list of properties that don't have zillow data
        //list will contain properites with no zillow data
        propertiesList = [SELECT Id, 
                                 Formatted_Street_Address__c, Street_Address__c, 
                                 Formatted_City__c, City__c, 
                                 Formatted_State__c, State__c, 
                                 Formatted_Zip_Code__c, Zip_Code__c,
                                 SqFt_of_Lot_Size__c, Zillow_Status__c
                          FROM  Geocode_Cache__c
                          WHERE ((SqFt_of_Lot_Size__c = null AND Zillow_Status__c = null)
                                OR
                                (Zillow_Status__c = 'Code: 7, Error: this account has reached is maximum number of calls for today'))
                          AND (Formatted_Street_Address__c != null OR Street_Address__c != null)
                          AND (Formatted_City__c != null OR City__c != null)
                          AND (Formatted_State__c != null OR State__c != null)
                          AND (Formatted_Zip_Code__c != null OR Zip_Code__c != null)
                          order by SqFt_of_Lot_Size__c,Zillow_Status__c nulls first
                          LIMIT 1000];
        
        return propertiesList;
    }
    
    
    global void execute(Database.BatchableContext BC, List<Geocode_Cache__c> geoList){//this method will be used to make callouts to zillow to get the lot size for a property
        
        try{
        
            //20170314 - Tyler H.  persisting as much zillow data as possible using new table
            INT_Zillow_Inbound__c[] zillowAPIresults = new INT_Zillow_Inbound__c[]{}; 
            
            if(geoList.size() > 0){
                for(Geocode_Cache__c gc : geoList){//loop thru the geoList
                    streetAddressParameter = gc.Formatted_Street_Address__c != null ? gc.Formatted_Street_Address__c : gc.Street_Address__c;
                    cityParameter = gc.Formatted_City__c != null ? gc.Formatted_City__c : gc.City__c;
                    stateParameter = gc.Formatted_State__c != null ? gc.Formatted_State__c : gc.State__c;
                    zipCodeParameter = gc.Formatted_Zip_Code__c != null ? gc.Formatted_Zip_Code__c : gc.Zip_Code__c;
                    
                    if(limitCounter < 1000){//if we have not hit the zillow api limit of 1000
                        limitCounter++;//add 1 to the limitCounter
                        
                        if(streetAddressParameter != null && ((cityParameter != null && stateParameter != null) || zipCodeParameter != null)){//if the address fields are not null
                            
                            //Padmesh Soni (04/28/2014) - Required Maintenance: Unit Test Improvements 
                            //Condition added for test method running time
                            if(Test.isRunningTest()) {
                                
                                //Check for response string is not blank
                                if(responseXMLString != '' && responseXMLString != null) {
                                
                                    //Create a instance of Dom Document
                                    Dom.Document docx = new Dom.Document();
                                    
                                    //set the body of document
                                    docx.load(responseXMLString);
                                    
                                    //call the ZillowAPIUtility passing in the address parameters to get the lot size
                                    zillowResWrapper = new ZillowResponseWrapper(docx);
                                }
                            } else {
                            
                                //call the ZillowAPIUtility passing in the address parameters to get the lot size
                                zillowResWrapper = ZillowAPIUtility.fetchListingFromZillow(streetAddressParameter, cityParameter, stateParameter, zipCodeParameter);
                            }
                            if(zillowResWrapper != null) {
                                if(zillowResWrapper.lotSizeSqFt != null && zillowResWrapper.lotSizeSqFt != '') {//if the lotSizeSqFt is not null/empty
                                    gc.SqFt_of_Lot_Size__c = decimal.valueOf(zillowResWrapper.lotSizeSqFt);//set SqFt_of_Lot_Size__c = decimal value of lotSizeSqFt
                                } else {
                                    gc.SqFt_of_Lot_Size__c = 0;//set SqFt_of_Lot_Size__c = 0
                                }
                                
                                if(zillowResWrapper.finishedSqFt != null && zillowResWrapper.finishedSqFt != '') {//if the lotSizeSqFt is not null/empty
                                    gc.Property_Square_Feet__c = decimal.valueOf(zillowResWrapper.finishedSqFt);//set SqFt_of_Lot_Size__c = decimal value of lotSizeSqFt
                                } else {
                                    gc.Property_Square_Feet__c = 0;//set SqFt_of_Lot_Size__c = 0
                                }
                                
                                if((zillowResWrapper.message != null && zillowResWrapper.code != null) && (zillowResWrapper.message != '' && zillowResWrapper.code != '')) {//if the message/code are not null/empty
                                    if(zillowResWrapper.lotSizeSqFt != null && zillowResWrapper.lotSizeSqFt != '') {
                                        gc.Zillow_Status__c = 'Code: ' + zillowResWrapper.code + ', ' + zillowResWrapper.message;//set Zillow_Status__c = a string concatenation of the code & message
                                    } else {
                                        if(zillowResWrapper.code == '0'){
                                            gc.Zillow_Status__c = 'The request was successfully processed, but there was no return on the lot size';//use this status if we get a code of 0 but there is no lot size returned
                                        } else {
                                            gc.Zillow_Status__c = 'Code: ' + zillowResWrapper.code + ', ' + zillowResWrapper.message;//set Zillow_Status__c = a string concatenation of the code & message
                                        }
                                    }
                                } else {
                                    gc.Zillow_Status__c = 'There was no Zillow response message';//set Zillow_Status__c = There was no Zillow response message
                                }
                                
                                zillowAPIresults.add(createZillowAPIcallRecord(gc,zillowResWrapper));
                            } else {
                                gc.SqFt_of_Lot_Size__c = 0;
                                gc.Property_Square_Feet__c = 0;
                                gc.Zillow_Status__c = 'Zillow did not return a response';
                            }
                        } else {
                            gc.SqFt_of_Lot_Size__c = 0;
                            gc.Property_Square_Feet__c = 0;
                            gc.Zillow_Status__c = 'Please make sure all address fields are filled out correctly';
                        }
                    } else {
                        continue;
                    }
                }
                
                update geoList;
                insert zillowAPIresults;
            }
        }
        catch (exception e){
            sendErrEmail(e);
        }
    }
    
    static void sendErrEmail(Exception e){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'software@northsight.com'});
        mail.setSenderDisplayName('NSM SFDC Error Alert');
        mail.setSubject('BatchGeoCodeWorkOrderProperties Error');
        mail.setUseSignature(false);
        mail.setPlainTextBody(e.getMessage() + e.getStackTraceString() );       
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    static INT_Zillow_Inbound__c createZillowAPIcallRecord(Geocode_Cache__c property, ZillowResponseWrapper z){
        
        INT_Zillow_Inbound__c ret = new INT_Zillow_Inbound__c();
        
        ret.Related_Property__c=property.id;
        ret.rawXML__c=z.rawXML;
        
        ret.request_address__c=z.address;
        ret.request_citystatezip__c=z.address;
        ret.address_city__c= z.city;
        ret.address_latitude__c=z.latitude;
        ret.address_longitude__c=z.longitude;
        ret.address_state__c=z.state;
        ret.address_street__c=z.street;
        ret.address_zipcode__c=z.zipcode;
        //elementarySchool__c
        //homeDescription__c
        //images_count__c
        //images_image_url__c
        //links_homeInfo__c
        //links_photoGallery__c
        //localRealEstate_name__c
        ret.links_comparables__c=z.comparables;
        ret.links_graphsanddata__c=z.graphsanddata;
        ret.links_homedetails__c=z.homedetails;
        ret.links_mapthishome__c=z.mapthishome;
        ret.localRealEstate_links_forSaleByOwner__c=z.forSaleByOwner;
        ret.localRealEstate_links_overview__c=z.overview;
        ret.localRealEstate_region__c=z.regionName;
        ret.localRealEstate_region_id__c=z.regionID;
        ret.localRealEstate_region_type__c=z.regionType;
        ret.localRealEstate_zindexValue__c=z.regionZindexValue;
        //request_rentzestimate__c
        //zestimate_duration_30_valueChange__c
        ret.Zestimate_valuationRange_high__c = z.high;
        ret.zestimate_valuationRange_low__c = z.low;
        
        ret.request_zpid__c=z.zpid;
        ret.zestimate_amount__c=z.amount;
        ret.zestimate_last_updated__c=z.lastupdated;
        ret.zestimate_percentile__c=z.percentile;
        
        ret.editedFacts_bathrooms__c = z.bathrooms;
        ret.editedFacts_bedrooms__c = z.bedrooms;
        ret.editedFacts_finishedSqFt__c = z.finishedSqFt;
        ret.editedFacts_lotSizeSqFt__c = z.lotSizeSqFt;
        ret.editedFacts_rooms__c = z.totalRooms;
        ret.editedFacts_useCode__c = z.useCode;
        ret.editedFacts_yearBuilt__c = z.yearBuilt;        
        
        /*
        these values need to be retrieved using the GetUpdatedPropertyDetails API
        
        //middleSchool__c
        //neighborhood__c
        //posting_agentName__c
        //posting_agentProfileUrl__c
        //posting_brokerage__c
        //posting_externalUrl__c
        //posting_lastUpdatedDate__c
        //posting_mls__c
        //posting_status__c
        //posting_type__c
        //schoolDistrict__c
        //price__c  
        editedFacts_appliances__c
        editedFacts_basement__c 
        editedFacts_floorCovering__c   
        editedFacts_heatingSources__c
        editedFacts_heatingSystem__c  
        editedFacts_numFloors__c
        editedFacts_parkingType__c
        editedFacts_roof__c
        editedFacts_view__c
        editedFacts_yearUpdated__c
        */      
        
        return ret;
    
    }
    
    
    global void finish(Database.BatchableContext BC){
        //section not needed
    }
    
        global static void unitTestBypass(){
        integer i=0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;        
    }
}