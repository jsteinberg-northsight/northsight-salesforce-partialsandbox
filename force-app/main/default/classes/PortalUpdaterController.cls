public without sharing class PortalUpdaterController {
	
	public class InProcessWorkOrder {
		public String inProcessText { get; set; }
		public Case workOrder { get; set; }
		
		public InProcessWorkOrder(Case inProcessOrder) {
			if (inProcessOrder == null) {
				inProcessText = '';
			} else {
				inProcessText = 'Current In-Process Work Order: ';
			}
			workOrder = inProcessOrder;
		}
	}
	
	public class Vendor {
		public String vendor { get; set; }
		public String client { get; set; }
		public Integer total { get; set; }
		public Integer regular { get; set; }
		public Integer priority { get; set; }
		public Integer severe { get; set; }
		
		public Vendor() {
			vendor = '';
			client = '';
			total = 0;
			regular = 0;
			priority = 0;
			severe = 0;
		}
	}
	
	public InProcessWorkOrder inProcessOrder { get; set; }
	public List<Vendor> vendorList { get; set; }
	public String selectedVendor { get; set; }
	public Integer regularSummary { get; set; }
	public Integer prioritySummary { get; set; }
	public Integer severeSummary { get; set; }
	public Integer totalSummary { get; set; }
	public Integer totalRows { get; set; }
	public Id updateVendor { get; set; }

	public PageReference updateWorkOrder() {
		updateVendor = PortalUpdaterControllerHelper.setWorkOrderToInProcess(selectedVendor);
		if (updateVendor == null) {
			String errorMessage ='No work order updated for the selected vendor.  Another user may have updated the last record for the selected vendor.';
			errorMessage += ' The grid has been updated to reflect any changes';
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
			initialize();
		}
		return null;
	}
	
	private void initialize() {
		inProcessOrder = checkForWorkOrderInProcess(
													PortalUpdaterControllerHelper.fetchWorkOrderInProcess(UserInfo.getUserId()));
		Set<String> vendorCodes = processVendorCodes(
													PortalUpdaterControllerHelper.fetchVendorCodes());
		List<Case> workOrders = PortalUpdaterControllerHelper.fetchWorkOrders(vendorCodes);
		totalRows = workOrders.size();
		vendorList = processWorkOrders(workOrders);
	}
	
	public PortalUpdaterController() {
		initialize();
	}
	
	public InProcessWorkOrder checkForWorkOrderInProcess(List<Case> inProcessOrders) {
		if (!inProcessOrders.isEmpty()) {
			return new InProcessWorkOrder(inProcessOrders[0]);
		} else {
			return new InProcessWorkOrder(null);
		}
	}
	
	public Set<String> processVendorCodes(List<Portal_Update_Setup__c> setupRecords) {
		Set<String> codes = new Set<String>();
		for (Portal_Update_Setup__c currentSetup : setupRecords) {
			codes.add(currentSetup.Vendor_Code__c);
		}
		return codes;
	}
	
	private void updateVendor(Case currentWorkOrder, Vendor tempVendor) {
		if (currentWorkOrder.Update_Priority__c <= 0) {
			tempVendor.regular++;
			tempVendor.total++;
			regularSummary++;
		} else if (currentWorkOrder.Update_Priority__c == 1 || currentWorkOrder.Update_Priority__c == 2) {
			tempVendor.priority++;
			tempVendor.total++;
			prioritySummary++;
		} else if (currentWorkOrder.Update_Priority__c >= 3) {
			tempVendor.severe++;
			tempVendor.total++;
			severeSummary++;
		}
	} 
	
	public List<Vendor> processWorkOrders(List<Case> workOrders) {
		Map<String, Vendor> vendorMap = new Map<String, Vendor>();
		List<Vendor> vendorOrderedList = new List<Vendor>();
		Vendor tempVendor;
		regularSummary = 0;
		prioritySummary = 0;
		severeSummary = 0;
		totalSummary = 0;
		
		for (Case currentWorkOrder : workOrders) {
			if (vendorMap.containsKey(currentWorkOrder.Vendor_Code__c)) {
				tempVendor = vendorMap.get(currentWorkOrder.Vendor_Code__c);
			} else {
				tempVendor = new Vendor();
				tempVendor.vendor = currentWorkOrder.Vendor_Code__c;
				tempVendor.client = currentWorkOrder.Client__c;
				vendorOrderedList.add(tempVendor);
			}
			updateVendor(currentWorkOrder, tempVendor);
			vendorMap.put(currentWorkOrder.Vendor_Code__c, tempVendor);
		}
		//return vendorMap.values();
		return vendorOrderedList;
	}
}