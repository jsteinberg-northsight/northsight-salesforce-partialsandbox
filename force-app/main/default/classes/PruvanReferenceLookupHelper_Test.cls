@isTest()
/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/
*/
public with sharing class PruvanReferenceLookupHelper_Test {
//	public static testmethod void testUpdateSFWorkOrderSurveyFields() {
//		/***********************************************************************************************************************************/
//		/*
//		this sections handles the creation of objects that will need to be used by some of the functions for testing purposes
//		*/
//		///create a new account
//		Account newAccount = new Account(
//			Name = 'Tester Account',
//			RecordTypeId = '01240000000Ubta'
//		);
//		insert newAccount;
//		//assert that the account exists
//		Account a = [SELECT Id FROM Account WHERE Id =: newAccount.Id];
//		system.assert(a != null);
//	
//		//create a new contact
//		Contact newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest',
//			RecordTypeId = '01240000000UbtfAAC'
//		);
//		insert newContact;
//		//assert that the contact exists
//		Contact c = [SELECT Id FROM Contact WHERE Id =: newContact.Id];
//		system.assert(c != null);
//		
//		//Create a new user with the Customer Portal Manager Standard profile
//		Profile newProfile = [SELECT Id FROM profile WHERE Name = 'Partner Community - Processors'];
//		system.assert(newProfile != null);
//		User newUser = new User(
//			alias = 'standt', 
//			contactId = newContact.id, 
//			email='standarduser@testorg.com',
//	  		emailencodingkey='UTF-8', 
//	  		lastname='Testing', 
//	  		languagelocalekey='en_US',
//	  		localesidkey='en_US', 
//	  		profileid = newProfile.Id,
//	  		timezonesidkey='America/Los_Angeles', username='standarduser@northsight.test',IsActive=true
//	  	);
//  		insert newUser;
//  		//assert that the user exists
//		User u = [SELECT Id FROM User WHERE Id =: newUser.Id];
//		system.assert(u != null);
//	
//		//create a new geocode_cache__c
//		Geocode_Cache__c newGeocode = new Geocode_Cache__c(
//			address_hash__c = '400cedarstbrdgprtct60562',
//        	quarantined__c = false,
//        	location__latitude__s = 11.11111111,
//        	location__longitude__s = -111.22222222
//		);
//		insert newGeocode;
//		//assert that the geocode exists
//		Geocode_Cache__c g = [SELECT Id FROM Geocode_Cache__c WHERE Id =: newGeocode.Id];		
//		system.assert(g != null);
//	
//	
//		//create a new case
//		Case newCase = new Case(
//			Client__c = 'SG',
//			Due_Date__c = Date.today(),
//			Backyard_Serviced__c = 'Yes',
//			Excessive_Growth__c = 'No',
//			Debris_Removed__c = 'No',
//			Shrubs_Trimmed__c = 'Yes',
//			Work_Completed__c = 'No',
//			Vendor_Code__c = 'CHIGRS',
//			State__c = 'CT',
//			Status = 'Open',
//			City__c = 'Bridgeport',
//			Zip_Code__c = '60562',
//			Description = 'Work order for grass cutting',
//			OwnerId = newUser.Id,
//			ContactId = newContact.Id,
//			Street_Address__c = '400 Cedar st',
//			Geocode_Cache__c = newGeocode.Id,
//			Work_Ordered__c = 'Initial Grass Cut',
//			Work_Ordered_Text__c = 'Grass Cutting',
//			Reference_Number__c = '99999999'
//		);
//		insert newCase;
//		//assert that the case exists
//		Case cs = [SELECT Id, CaseNumber, Reference_Number__c FROM Case WHERE Id =: newCase.Id];
//		system.assert(cs != null);
//		/***********************************************************************************************************************************/
//
//		//create a dummy payload using the ref. # from the test case
//        string payload;     
//        payload = '{"username": "pruvan", "password": "8c8cd34c37d6813808baa358b18169c211cb47f3","workOrderNumber": "NSTST::' + cs.CaseNumber + '"}';
//         
//        //assert that we get this expected value from doSurveyValidate
//        //system.assertEquals('{"referenceNumber":"' + cs.Reference_Number__c + '","error":""}', PruvanWebservices.doReferenceLookup(payload));
//        
//        //create a dummy payload that causes an error and assert that the error message != null and it exists
//        string payload2 = '{"username": "pruvan", "password": "8c8cd34c37d6813808baa358b18169c211cb47f3","workOrderNumber": "NSTST::' + newCase.Reference_Number__c + '"}';
//        Map<string,object> responseData = new Map<string,object>();
//        responseData = (map<string,object>)JSON.deserializeUntyped(PruvanWebservices.doReferenceLookup(payload2));
//        object errorMessage = (object)responseData.get('error');
//        string message = (string)errorMessage;
//        system.assert(message != null);
//        system.assert(message != '');
//        system.assert(message.length() > 0);
//	}
}