@isTest(seeAllData=false)
private class Test_Batch_DailyRequestCountReset {
//
///**
// *  Purpose         :   This is used for testing and covering Batch_DailyRequestCountReset class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   07/24/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Padmesh Soni(07/24/2014) - VWMSubmitUpdate
// *	
// *	Coverage		:	100%
// **/
// 	
// 	//Test method to test the functionality of Batch_OrderSelectFeeCaseGeneration
//    static testMethod void myUnitTest() {
//    	
//    	//List to hold test records of Proxy sobject
//    	List<Proxy__c> proxies = new List<Proxy__c>();
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now().addDays(-10)));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now(), Requests_Today__c = 2));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now().addDays(-3)));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now().addDays(-5)));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now().addDays(-18), Requests_Today__c = 2));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now().addDays(10), Requests_Today__c = 2));
//    	proxies.add(new Proxy__c(Last_Used__c = Datetime.now(), Requests_Today__c = 2));
//    	
//    	//insert Proxy Sobject's list
//    	insert proxies;
//    	
//        //Test starts here
//        Test.startTest();
//        
//        //instance of Batch
//        Batch_DailyRequestCountReset bc = new Batch_DailyRequestCountReset();
//        
//        //Batch executes here
//        Database.executeBatch(bc, 200);
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //query result of Case
//        proxies = [SELECT OwnerId FROM Proxy__c WHERE Requests_Today__c = 0];
//        	
//    	//assert statement
//        System.assertEquals(4, proxies.size());
//    }
}