@isTest(SeeAllData=true)
public class RHX_TEST_Code_Violation_Item {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Code_Violation_Item__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Code_Violation_Item__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}