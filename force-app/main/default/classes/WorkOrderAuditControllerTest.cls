@isTest
private class WorkOrderAuditControllerTest {
//	
//	static @isTest void testInvalidWorkOrderNumber() {
//		List<Case> workOrders = WorkOrderAuditControllerHelper.fetchWorkOrder('invalid');
//		//system.assertEquals(0, workOrders.size());
//		
//		PageReference workOrderAudit = Page.WorkOrderAudit;
//		Test.setCurrentPage(workOrderAudit);
//		ApexPages.currentPage().getParameters().put('workordernumber', 'invalid');
//		WorkOrderAuditController controller = new WorkOrderAuditController();
//		//system.assertEquals(1, ApexPages.getMessages().size());
//		//system.assertEquals('Invalid Work Order Number.', ApexPages.getMessages()[0].getSummary());
//	}
//	
//	static @isTest void testBlankWorkOrderNumber() {
//		PageReference workOrderAudit = Page.WorkOrderAudit;
//		Test.setCurrentPage(workOrderAudit);
//		WorkOrderAuditController controller = new WorkOrderAuditController();
//		//system.assertEquals(1, ApexPages.getMessages().size());
//		//system.assertEquals('Invalid Work Order Number.', ApexPages.getMessages()[0].getSummary());
//	}
//	
//	static @isTest void testOrderIssueOptions() {
//		String checked;
//		Case testCase = new Case(Order_Issues__c = 'Children Present', Photos_Audited_By__c = UserInfo.getUserId());
//		
//		List<WorkOrderAuditController.Option> options = WorkOrderAuditControllerHelper.fetchOrderIssueOptions(testCase);
//		//system.assertEquals(Case.Order_Issues__c.getDescribe().getPicklistValues().size(), options.size());
//		for (WorkOrderAuditController.Option currentOption : options) {
//			if (currentOption.optionLabel == 'Children Present') {
//				checked = currentOption.checked;
//				break;
//			}
//		}
//		//system.assertEquals('Yes', checked);
//	}
//	
//	static @isTest void testMissingPhotosOptions() {
//		String checked;
//		Case testCase = new Case(Missing_Photos__c = 'Before Full View - Front', Photos_Audited_By__c = UserInfo.getUserId());
//		
//		List<WorkOrderAuditController.Option> options = WorkOrderAuditControllerHelper.fetchMissingPhotoOptions(testCase, new Set<Integer>{4,5,6,7,8});
//		//system.assertEquals(5, options.size());
//		for (WorkOrderAuditController.Option currentOption : options) {
//			System.Debug('currentOption.optionLabel:: '+currentOption.optionLabel);
//			if (currentOption.optionLabel == 'Full View - Front') {
//				checked = currentOption.checked;
//				break;
//			}
//		}
//		//system.assertEquals('Yes', checked);
//	}
//	
//	static @isTest void testCreateOptionString() {
//		List<WorkOrderAuditController.Option> testOptions = new List<WorkOrderAuditController.Option>();
//		testOptions.add(new WorkOrderAuditController.Option('Yes', 'option 1', 'option 1', false, WorkOrderAuditControllerHelper.ORDER_ISSUES));
//		testOptions.add(new WorkOrderAuditController.Option('No', 'option 2', 'option 2', false, WorkOrderAuditControllerHelper.ORDER_ISSUES));
//		testOptions.add(new WorkOrderAuditController.Option('Yes', 'option 3', 'option 3', false, WorkOrderAuditControllerHelper.ORDER_ISSUES));
//		
//		String options = WorkOrderAuditControllerHelper.createOptionString(testOptions, '');
//		//system.assertEquals('option 1; option 3', options);
//	}
//	
//	static @isTest void testSaveAuditOptions() {
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//		Case testCase = new Case(Street_Address__c = '100 test', Vendor_Code__c = 'test', Reference_Number__c = '007',Client_Name__c = account.Id);
//		insert testCase;
//		testCase = [SELECT CaseNumber FROM Case WHERE Id = :testCase.Id];
//		PageReference workOrderAudit = Page.WorkOrderAudit;
//		Test.setCurrentPage(workOrderAudit);
//		ApexPages.currentPage().getParameters().put('workordernumber', testCase.CaseNumber);
//		WorkOrderAuditController controller = new WorkOrderAuditController();
//		//system.assertEquals('https://s3.amazonaws.com', controller.awsUrl, 'S3 Url');
//		//system.assertEquals('allyearimages', controller.bucketName, 'S3 Bucket');
//		for (WorkOrderAuditController.Option currentOption: controller.missingPhotoGeneralOptions) {
//			currentOption.checked = 'Yes';
//			currentOption.getOptions();
//		}
//		for (WorkOrderAuditController.Option currentOption: controller.missingPhotoBeforeOptions) {
//			currentOption.checked = 'Yes';
//		}
//		for (WorkOrderAuditController.Option currentOption: controller.missingPhotoActionOptions) {
//			currentOption.checked = 'Yes';
//		}
//		for (WorkOrderAuditController.Option currentOption: controller.missingPhotoAfterOptions) {
//			currentOption.checked = 'Yes';
//		}
//		for (WorkOrderAuditController.Option currentOption: controller.orderIssueOptions) {
//			currentOption.checked = 'No';
//		}
//		PageReference returnUrl = controller.saveAuditOptions();
//		//system.assertEquals('/' + testCase.Id, returnUrl.getUrl());
//		
//		testCase = [SELECT Order_Issues__c, Missing_Photos__c FROM Case WHERE Id =: testCase.Id];
//		//system.assertEquals(null, testCase.Order_Issues__c);
//		//system.assertEquals(null, testCase.Missing_Photos__c);
//	}
//	
//	static @isTest void testCancelAuditOptions() {
//		Case testCase = new Case(Street_Address__c = '100 test', Vendor_Code__c = 'test', Reference_Number__c = '007');
//		insert testCase;
//		testCase = [SELECT CaseNumber FROM Case WHERE Id = :testCase.Id];
//		PageReference workOrderAudit = Page.WorkOrderAudit;
//		Test.setCurrentPage(workOrderAudit);
//		ApexPages.currentPage().getParameters().put('workordernumber', testCase.CaseNumber);
//		WorkOrderAuditController controller = new WorkOrderAuditController();
//		PageReference returnUrl = controller.cancelAuditOptions();
//		//system.assertEquals('/' + testCase.Id, returnUrl.getUrl());
//	}
//	
//	static @isTest void testValidateOptions() {
//		Case testCase = new Case(Street_Address__c = '100 test', Vendor_Code__c = 'test', Reference_Number__c = '007');
//		insert testCase;
//		testCase = [SELECT CaseNumber FROM Case WHERE Id = :testCase.Id];
//		PageReference workOrderAudit = Page.WorkOrderAudit;
//		Test.setCurrentPage(workOrderAudit);
//		ApexPages.currentPage().getParameters().put('workordernumber', testCase.CaseNumber);
//		WorkOrderAuditController controller = new WorkOrderAuditController();
//		//system.assertEquals('https://s3.amazonaws.com', controller.awsUrl, 'S3 Url');
//		for (WorkOrderAuditController.Option currentOption: controller.missingPhotoGeneralOptions) {
//			currentOption.checked = 'No';
//			currentOption.getOptions();
//		}
//		for (WorkOrderAuditController.Option currentOption: controller.missingPhotoBeforeOptions) {
//			currentOption.checked = 'No';
//		}
//		for (WorkOrderAuditController.Option currentOption: controller.missingPhotoActionOptions) {
//			currentOption.checked = 'No';
//		}
//		for (WorkOrderAuditController.Option currentOption: controller.missingPhotoAfterOptions) {
//			currentOption.checked = 'No';
//		}
//		for (WorkOrderAuditController.Option currentOption: controller.orderIssueOptions) {
//			currentOption.checked = null;
//		}
//		controller.saveAuditOptions();
//		//system.assertEquals('All options must be Yes or No', ApexPages.getMessages()[0].getSummary());
//		
//	}
}