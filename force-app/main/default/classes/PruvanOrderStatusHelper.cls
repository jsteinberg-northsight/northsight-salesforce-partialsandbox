/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/
*/
public without sharing class PruvanOrderStatusHelper {
	//sample payload
	/*
	{
    "workOrders": [{
        "workOrderNumber": "NSM::02614621",
        "workOrderInfo": "",
        "address1": "4719 SILVER CIR",
        "address2": "",
        "city": "ZEPHYRHILLS",
        "state": "FL",
        "zip": "33542",
        "country": "USA",
        "reference": "137309471",
        "description": "",
        "notes": "  ",
        "status": "assigned",
        "invoiceNumber": "",
        "gpsLatitude": "",
        "gpsLongitude": "",
        "attribute1": "",
        "attribute2": "",
        "attribute3": "",
        "attribute4": "",
        "attribute5": "",
        "attribute6": "",
        "attribute7": "",
        "attribute8": "",
        "attribute9": "",
        "attribute10": "",
        "attribute11": "",
        "attribute12": "",
        "attribute13": "",
        "attribute14": "",
        "attribute15": "",
        "instructions": "  ",
        "dueDate": "2013-12-01 00:00:00",
        "assignedTo": "jcbeau",
        "delegatedTo": "",
        "delegatedFromWorkOrderId": "",
        "delegatedFromClientCode": "",
        "history": "[\"2013-11-20 14:07: imported by client NSM\",\"2013-11-20 14:07: created by jcbeau\",\"2013-11-20 14:37: updated by jcbeau - changed status to assigned, dueDate to 2013-12-01 and assignedTo to jcbeau.\"]",
        "options": "",
        "lastUpdateDate": "2013-11-20 14:37:19",
        "lastUpdatedBy": "jcbeau",
        "workOrderId": "220091",
        "createdBy": "jcbeau"
    	}]
	}
	*/
	//create map variables for request and response data
	private static Map<string,object> orderStatusRequest;
	private static Map<string,object> orderStatusResponse;
	private static string responseJSON;
	
	//filter strings and update strings
	private static string orderNum;
	private static string orderStatus;
	private static list<object> workOrderList;
	private static Map<string, object> innerWorkOrderMap;
	private static Map<string, Map<string, object>> outerWorkOrderMap;
	private static Set<string> woNumbers;
	private static Map<string, string> orderStatusMap;
	
	public static string updateOrderStatus(string input){
		//parse input into orderStatusRequest map
		orderStatusRequest = (map<string,object>)JSON.deserializeUntyped(input);
		
		//get the list of work orders
		workOrderList = (List<object>)orderStatusRequest.get('workOrders');
		
		//instantiate inner and outer WorkOrderMap
		innerWorkOrderMap = new Map<string, object>();
		outerWorkOrderMap = new Map<string, Map<string, object>>();
		//instantiate woNumbers set
		woNumbers = new Set<string>();
		//instantiate orderStatusMap
		orderStatusMap = new Map<string, string>();
		
		//populate the inner and outer WorkOrderMap vars
		for(Object o : workOrderList){
			innerWorkOrderMap = (map<string,object>)o;
			outerWorkOrderMap.put((string)innerWorkOrderMap.get('workOrderNumber'), innerWorkOrderMap);
		}
		
		//loop thru the outerWorkOrderMap
		for(string woNumber : outerWorkOrderMap.keySet()){
			//split woNumber and add the actual order # to woNumbers
			List<String> strOrderNum = woNumber.split('::');
			orderNum = strOrderNum[strOrderNum.size()-1];
			woNumbers.add(orderNum);
			
			//get the orderStatus of the current work order and add it to the orderStatusMap using orderNum as the key
			orderStatus = (string)outerWorkOrderMap.get(woNumber).get('status');
			orderStatusMap.put(orderNum, orderStatus);
		}
		
		//instantiate orderStatusResponse
		orderStatusResponse = new Map<string,object>();
		
		//attempt to get work orders and update their approval statuses
		try{
			//call these methods to update a work order's status
				performOrderUpdate(fetchWorkOrders());
				orderStatusResponse.put('error', null);
		}
		catch(Exception e){
			//if there is an exception, fill out the response with the error
			orderStatusResponse.put('error', e.getMessage()+' '+e.getStackTraceString());
		}
		
		return responseJSON = JSON.serialize(orderStatusResponse);
	}
	
	
	//this method will get a work order and will set it's Pruvan_Status field to 'Complete' if the Pruvan_Status__c = 'Complete'
	private static void performOrderUpdate(List<Case> workOrders){
		if(workOrders.size() > 0){
			for(Case c : workOrders){
				string currentStatus = orderStatusMap.get(c.CaseNumber);
					c.Pruvan_Status__c = currentStatus;
			}
			
			update workOrders;
		}
		
	}
	
	//this method will select in a work order's Id and Status where the order's CaseNumber = orderNum and the order's Contact Pruvan username = pruvanUser
	private static List<Case> fetchWorkOrders(){
		
		//Padmesh Soni (06/26/2014) - Support Case Record Type
    	//Query record of Record Type of Case object
		List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
		
		//Padmesh Soni (06/26/2014) - Support Case Record Type
    	//New filter criteria added for RecordTypeId
	    List<Case> currentOrders = [SELECT Id, CaseNumber, Pruvan_Status__c FROM Case WHERE RecordTypeId NOT IN: recordTypes 
	    								AND CaseNumber IN: woNumbers for update];
		
		return currentOrders;
	}
	
	public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
	
	
}