public without sharing class PruvanStatusLogicHelper {
	private static List<Case> casesBeforeUpdate;
	
	//initialize method to get the orders before they are updated
	public static void initialize(List<Case> triggerNew){
		casesBeforeUpdate = triggerNew != null ? triggerNew : new List<Case>();//set casesBeforeUpdate = triggerNew if not null, else make a new List<Case>
	}
	
	//this method will set the order status if the Pruvan_Status = 'Complete', and both Survey_Received & All_Images_Received = true
	public static void setOrderStatuses(){
		if(casesBeforeUpdate.size() > 0){//if casesBeforeUpdate has orders in it
			for(Case c : casesBeforeUpdate){//begin looping thru casesBeforeUpdate list
				//if(c.Mobile_Version__c >= 3.9){
					//check these fields if Mobile_Version = 3.9 or greater
					if((c.Pruvan_Status__c != null && c.Pruvan_Status__c.toLowerCase() == 'complete') && c.Survey_Received__c == true && c.All_Images_Received__c == true && (c.Approval_Status__c == 'Not Started' || c.Approval_Status__c == 'Rejected') && c.Mobile__c == false && (c.Work_Completed__c == 'Yes' || (c.Work_Completed__c == 'Trip Charge' && c.Trip_Charge_Reason__c != null))){
						c.Approval_Status__c = 'Pre-Pending';
						c.Mobile__c = true;
						OrderAssignmentHelper.pruvanPushSet.add(c.Id);
					}
				//}
				/*
				else if(c.Mobile_Version__c <= 3.8){
					
					//check these fields if Mobile_Version = 3.8 or less
					if(c.Survey_Received__c == true && c.All_Images_Received__c == true && (c.Approval_Status__c == 'Not Started' || c.Approval_Status__c == 'Rejected') && c.Mobile__c == false && (c.Work_Completed__c == 'Yes' || (c.Work_Completed__c == 'Trip Charge' && c.Trip_Charge_Reason__c != null))){
						c.Approval_Status__c = 'Pre-Pending';
						c.Mobile__c = true;
					}
				}
				*/
			}
		}
	}
}