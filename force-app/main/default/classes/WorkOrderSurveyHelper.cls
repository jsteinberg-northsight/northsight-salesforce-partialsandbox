public without sharing class WorkOrderSurveyHelper {
    //globals
    //************************************************************
    private static List<Case> casesAfterInsert;
    private static List<Work_Order_Survey__c> workOrderSurveys;
    private static Map<Id,Work_Code_Survey__c[]> workCodeSurveyMap;
    //************************************************************
    
    //initialize method to instantiate globals
    public static void initialize(List<Case> triggerNew) {
        if(triggerNew != null && triggerNew.size() > 0)
            casesAfterInsert = triggerNew;
        workOrderSurveys = new List<Work_Order_Survey__c>();
        workCodeSurveyMap = new Map<Id,Work_Code_Survey__c[]>();
        getWorkCodeSurveys();
    }
    
    //gets the work code survey records using the casesAfterInsert data and adds them to the workCodeSurveyMap
    private static void getWorkCodeSurveys() {
        Set<Id> workCodeIds = new Set<Id>();
        if(casesAfterInsert != null && casesAfterInsert.size() > 0) {
            for(Case c : casesAfterInsert) {
                if(!workCodeIds.Contains(c.Work_Code__c))
                    workCodeIds.Add(c.Work_Code__c);
            }
        }
        
        if(workCodeIds != null && workCodeIds.size() > 0) {
            for(Work_Code_Survey__c workCodeSurvey : [SELECT Id,
                                                             Comments__c,
                                                             Make_Survey_Required__c,
                                                             Survey_Number__c,
                                                             Work_Code_Name__c,
                                                             Service_Name__c, 
                                                             Sort__c
                                                      FROM Work_Code_Survey__c 
                                                      WHERE Work_Code_Name__c IN: workCodeIds]) 
            {
                if(!workCodeSurveyMap.ContainsKey(workCodeSurvey.Work_Code_Name__c)){
                    workCodeSurveyMap.put(workCodeSurvey.Work_Code_Name__c,new Work_Code_Survey__c[]{});
                }
                workCodeSurveyMap.get(workCodeSurvey.Work_Code_Name__c).add(workCodeSurvey);
            }
        }
        createWorkOrderSurveys();
    }
    
    //creates the work order survey records using the caseAfterInsert and workCodeSurveyMap data, then adds the created records to workOrderSurveys list for insert
    private static void createWorkOrderSurveys() {
        if(casesAfterInsert != null && casesAfterInsert.size() > 0) {
            if(workCodeSurveyMap != null && workCodeSurveyMap.size() > 0) {
                for(Case c : casesAfterInsert) {
                    if(workCodeSurveyMap.ContainsKey(c.Work_Code__c)) {
                        for(Work_Code_Survey__c tempWorkCodeSurvey : workCodeSurveyMap.get(c.Work_Code__c)){
                        Work_Order_Survey__c workOrderSurvey = new Work_Order_Survey__c(
                            Work_Order_Number__c = c.Id,
                            Survey_Number__c = tempWorkCodeSurvey.Survey_Number__c,
                            Work_Code_Survey_Entry__c = tempWorkCodeSurvey.Id,
                            Comments__c = tempWorkCodeSurvey.Comments__c,
                            Make_Survey_Required__c = tempWorkCodeSurvey.Make_Survey_Required__c,
                            Service_Name__c = tempWorkCodeSurvey.Service_Name__c, 
                            Sort__c = tempWorkCodeSurvey.Sort__c,
                            HashKey__c = ''+c.Id + tempWorkCodeSurvey.Survey_Number__c
                        );
                        workOrderSurveys.Add(workOrderSurvey);
                        }
                    }
                }
            }
        }
    }
    
    //inserts the list of work order survey records, if any
    public static void finalize() {
        if(workOrderSurveys != null && workOrderSurveys.size() > 0)
            Database.insert(workOrderSurveys,false);
    }
}