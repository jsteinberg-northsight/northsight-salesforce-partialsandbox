@isTest(seeAllData=false)
private class Test_Batch_AttachmentCleanupOnCases {
///**
// *  Purpose         :   This is used for testing and covering Batch_AttachmentCleanupOnCases.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   01/28/2014
// *
// *	Current Version	:	V1.0
// *
// *	Coverage		:	88%
// **/
// 	
// 	//Test method is used to cleaning Attachments on Cases
//    static testMethod void testAttachmentCleanupOnCases() {
//    	
//    	//Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client') AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(2, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accounts;
//    	
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1), Status = Constants.CASE_STATUS_OPEN));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[1].Id, Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[1].Id, Due_Date__c = Date.today().addDays(1), Status = Constants.CASE_STATUS_OPEN));
//    	
//    	insert workOrders;
//    	
//    	//List of Attachment to store testing records
//    	List<Attachment> attachments = new List<Attachment>();
//    	attachments.add(new Attachment(ParentId = workOrders[0].Id, Name='Test1', Body=Blob.valueOf('Test 1'), ContentType='text/plain'));
//    	attachments.add(new Attachment(ParentId = workOrders[0].Id, Name='Test2', Body=Blob.valueOf('Test 2'), ContentType='text/plain'));
//    	attachments.add(new Attachment(ParentId = workOrders[1].Id, Name='Test3', Body=Blob.valueOf('Test 3'), ContentType='text/plain'));
//    	attachments.add(new Attachment(ParentId = workOrders[1].Id, Name='Test4', Body=Blob.valueOf('Test 4'), ContentType='text/plain'));
//    	attachments.add(new Attachment(ParentId = workOrders[2].Id, Name='Test5', Body=Blob.valueOf('Test 5'), ContentType='text/plain'));
//    	attachments.add(new Attachment(ParentId = workOrders[2].Id, Name='Test6', Body=Blob.valueOf('Test 6'), ContentType='text/plain'));
//    	attachments.add(new Attachment(ParentId = workOrders[3].Id, Name='Test7', Body=Blob.valueOf('Test 7'), ContentType='text/plain'));
//    	attachments.add(new Attachment(ParentId = workOrders[3].Id, Name='Test8', Body=Blob.valueOf('Test 8'), ContentType='text/plain'));
//    	
//    	insert attachments;
//    	
//    	//Test starts here
//    	Test.startTest();
//    	
//    	//Batch instance
//    	Batch_AttachmentCleanupOnCases batchJob = new Batch_AttachmentCleanupOnCases();
//    	batchJob.dtmLastMonth = DateTime.now().addDays(2);
//    	
//    	//Batch executes here
//    	Database.executeBatch(batchJob, 200);
//    	
//    	//Test stops here
//    	Test.stopTest();
//    	
//    	//Query result of Attachements
//    	attachments = [SELECT Id FROM Attachment WHERE ParentId IN: workOrders];
//    	
//    	//Assert statemens
//    	System.assertEquals(0, attachments.size());
//    }
}