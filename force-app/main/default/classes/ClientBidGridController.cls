/**
 *  Purpose         :   This class is controller for ClientBidGrid Visualforce page. 
 *
 *  Created By      :   Shawn Johnson
 *
 *  Created Date    :   6/22/2018
 *
 *  Current Version :   V1.1
 *
 *  Revision Log    :   V1.0 - Created
 **/
global with sharing class ClientBidGridController {
    
    //Class properties
    public string bidSerialized{
        
        get{        
            
            List<User> users = [Select Contact.AccountId from User WHERE ContactId != null AND Id =: Userinfo.getUserId()];
            
            String SOQL = 'SELECT ';
            
            Map <String, Schema.SObjectField> fieldMap = Client_Bid__c.sObjectType.getDescribe().fields.getMap(); 
            List<String> fieldAPINames = new List<String>();
            for(Schema.SObjectField sfield : fieldMap.Values())
            {
            schema.describefieldresult dfield = sfield.getDescribe();
            SOQL += dfield.getname() + ', ';
            }
            
            SOQL += ' Property_Lookup__r.Name FROM Client_Bid__c';
            
            if(users.size() > 0 && String.isNotBlank(users[0].Contact.AccountId)) {
            
                SOQL += ' WHERE Related_Work_Order__r.Client_Name__c = \'' + users[0].Contact.AccountId + '\'';
            }
            
            List<String> excludeStatus = new List<String>();
            excludeStatus = Label.EXCLUDE_BID_STATUS.split(',');
            
            if(SOQL.indexOf('WHERE') > -1) {
                
                SOQL += ' AND Bid_Status__c NOT IN: excludeStatus';
            } else {
                
                SOQL += ' WHERE  Bid_Status__c NOT IN: excludeStatus';
            }
            SOQL += ' Order BY Sort_Order__c, Last_Bid_Status_Change__c DESC LIMIT '+ Label.BID_LIMIT;
            
            System.debug('SOQL ::'+ SOQL);
            //Query result of Bid sobject
            Client_Bid__c[] bids = Database.query(SOQL);
        
            //Serialized the Sobject list into JSON string
            return JSON.serializePretty(bids);
        } set;
    }
        
    public string HtmlDataId { get; set; }
    public string TheUserAddress { get; set; }
    public static integer CHUNK_SIZE = 30000;
    
    public String baseURL {
        get {
            
            return URL.getSalesforceBaseUrl().toExternalForm();
        }
        set;
    }
    
    //Constructor definition
    public ClientBidGridController(){}
    
    /**
     *  @descpriton     :   This method is used to create a string of Bids into a HTML Batch object's HTML Chunk field and associate it with
     *                      Listview HTML object.
     *
     *  @param          :   String propertyTable
     *
     *  @return         :   Id of Listview HTML object
     **/
    @RemoteAction
    global static string SaveHtml(String bidTable) {
        
        try {
            
            //create parent record
            Listview_Html__c listViewObject = new Listview_Html__c();
            insert listViewObject;
            
            List<Html_Batch__c> batchList = new List<Html_Batch__c>();
            
            integer counter = 0;
            integer chunkSize = CHUNK_SIZE;
            string newChunk = '';
            
            //is html bigger than 30k?
            if (bidTable.length() > chunkSize) {
                
                //break the html into pieces of 30k
                while (bidTable.length() > chunkSize) {
                    
                    //get a chunk
                    newChunk = bidTable.substring(0, chunkSize);
                    
                    //remove chunk from HTML string
                    bidTable = bidTable.substring(chunkSize, bidTable.length());
                    
                    //add chunk
                    Html_Batch__c newBatch = new Html_Batch__c();
                    newBatch.Listview_html__c = listViewObject.Id;
                    newBatch.Html_Chunk__c = newChunk;
                    newBatch.Order__c = counter;  //keep Bid so we can put it back together
                    batchList.add(newBatch);
                    
                    counter++;
                }
                
                //any remaining data?
                if(bidTable.length() > 0) {
                    
                    Html_Batch__c newBatch = new Html_Batch__c();
                    newBatch.Listview_html__c = listViewObject.Id;
                    newBatch.Html_Chunk__c = bidTable;
                    newBatch.Order__c = counter;  //keep Bid so we can put it back together
                    batchList.add(newBatch);
                }        
            } else {
                
                //only need one chunk
                Html_Batch__c newBatch = new Html_Batch__c();
                newBatch.Listview_html__c = listViewObject.Id;
                newBatch.Html_Chunk__c = bidTable;
                newBatch.Order__c = counter;  //keep Bid so we can put it back together
                
                batchList.add(newBatch);
            }
            
            //insert chunks
            insert batchList;
            
            //System.debug('listViewObject.Id ::::'+ listViewObject.Id);
            
            //return the parent Id
            return listViewObject.Id;
        } catch(exception e) {
            return Label.ORDER_GRID_SAVING_ERROR;
        }
    }
    
    /**
     *  @descpriton     :   This method is used to return user to the Export Page.
     *
     *  @param          :
     *
     *  @return         :   PageReference
     **/
    public PageReference GoToExportPage() {
        
        //Creating PageReference instance of ExportToExcel page
        PageReference exportPage = new PageReference('/apex/ExportToExcel?id=' + HtmlDataId + '&filename=Bid');
        return exportPage;
    }
}