global class ScheduleBatch_Move_Orders_to_PrePending implements Schedulable{
	global void execute(SchedulableContext ctx){
		Batch_Move_Orders_to_PrePending b = new Batch_Move_Orders_to_PrePending();
		
		Database.executeBatch(b, 200);
	}
}