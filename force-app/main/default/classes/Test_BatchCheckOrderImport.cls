@isTest(seeAllData=false)
private class Test_BatchCheckOrderImport {
///**
// *  Purpose         :   This is used for testing and covering BatchCheckOrderImport class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/04/2014
// *
// *	Current Version	:	V1.0 - Created - Required Maintenance: Unit Test Improvements
// *	
// *	Coverage		:	100%
// **/
// 	
// 	//Test method to test the functionality of BatchCheckOrderImport
//    static testMethod void myUnitTest() {
//        
//        //List to hold Order Import test records
//        List<Order_Import__c> orderImports = new List<Order_Import__c>();
//        orderImports.add(new Order_Import__c(Active__c = true, Client__c = 'SG', Login__c = 'testing@test.com', Password__c = '1234'));
//        orderImports.add(new Order_Import__c(Active__c = true, Client__c = 'SG', Login__c = 'test@test1.com', Password__c = '5433'));
//        
//        //insert Order Imports here
//        insert orderImports;
//        
//        //List to hold Order Import History test records
//        List<Order_Import_History__c> orderImportHistory = new List<Order_Import_History__c>();
//        orderImportHistory.add(new Order_Import_History__c(Order_Import__c = orderImports[0].Id, Order_Count__c = 4, 
//        								Attempt_Date_Time__c = DateTime.now().addHours(-2), Retrieval_Outcome__c = 'Success',
//        								Imported_by_Initials__c = 'PSW', Type__c = 'Full'));
//        orderImportHistory.add(new Order_Import_History__c(Order_Import__c = orderImports[0].Id, Order_Count__c = 4, 
//        								Attempt_Date_Time__c = DateTime.now().addHours(-3), Retrieval_Outcome__c = 'Timeout',
//        								Imported_by_Initials__c = 'PSW', Type__c = 'New Only'));
//        orderImportHistory.add(new Order_Import_History__c(Order_Import__c = orderImports[0].Id, Order_Count__c = 4, 
//        								Attempt_Date_Time__c = DateTime.now().addHours(-4), Retrieval_Outcome__c = 'Login Failed',
//        								Imported_by_Initials__c = 'PSW', Type__c = 'Update All'));
//        orderImportHistory.add(new Order_Import_History__c(Order_Import__c = orderImports[0].Id, Order_Count__c = 4, 
//        								Attempt_Date_Time__c = DateTime.now().addHours(-5), Retrieval_Outcome__c = 'Success',
//        								Imported_by_Initials__c = 'PSW', Type__c = 'Full'));
//        orderImportHistory.add(new Order_Import_History__c(Order_Import__c = orderImports[1].Id, Order_Count__c = 4, 
//        								Attempt_Date_Time__c = DateTime.now().addHours(-2), Retrieval_Outcome__c = 'Success',
//        								Imported_by_Initials__c = 'PSW', Type__c = 'Full'));
//        orderImportHistory.add(new Order_Import_History__c(Order_Import__c = orderImports[1].Id, Order_Count__c = 4, 
//        								Attempt_Date_Time__c = DateTime.now().addHours(-3), Retrieval_Outcome__c = 'Timeout',
//        								Imported_by_Initials__c = 'PSW', Type__c = 'New Only'));
//        
//        //insert Order Import History here
//        insert orderImportHistory;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //Execute the batch
//        Database.executeBatch(new BatchCheckOrderImport(), 200);
//        
//        //Test stops here
//        Test.stopTest();
//        
//        //instantiate a system alert list and system alert object
//		List<System_Alerts__c> saList = [SELECT Id FROM System_Alerts__c];
//		
//		//assert statement here
//		//System.assertEquals(1, saList.size());
//    }
}