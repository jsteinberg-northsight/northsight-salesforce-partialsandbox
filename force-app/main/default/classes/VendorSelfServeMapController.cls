/**
 *	Description		:	This is controller for VendorSelfServeMap visualforce page. 
 *
 *	Created By		:	The Nerd Whisperers
 *
 *  Developers      :   Don Kotter, Padmesh Soni
 *
 *	Created Date	:	03/29/2014
 *
 *	Current Version	:	V1.3
 *
 *	Revisiion Logs	:	V1.0 - Created
 *						V1.1 - Modified - Padmesh Soni (05/13/2014) - 140509 OrderSelect Dynamic pricing mod(Self Service Assignment)
 *                               Changes are:
 *                               1. SSA-32 - Check serviceable when assigning order.
 *						V1.2 - Modified - Padmesh Soni (08/19/2014) - NS Communities Migration
 *						V1.3 - Modified - Padmesh Soni (08/28/2014) - SM-183 Last Login isn't updated on Contact. 
 **/
//This needs to be Without Sharing 
public without sharing class VendorSelfServeMapController {
	
	//Class variable
	public Case tempCase{get;set;}
	public integer selectedOrders {get; set;}
	public String ordersJSON {get; set;}
	public Boolean displayPopup {get;set;}
	public integer selCounter {get; set;}
	public String toBeOwned {get; set;}
	public static boolean isAvailable {get; set;}
	public static boolean checkAvailable {get; set;}
	public String infoMessage {get; set;}
	public User currentUser {get; set;}
	public String limitMessage {get; set;}
	public String vendorInitials {get; set;}
	
	//Constructor defintion
	public VendorSelfServeMapController(){
		
		//intialize class variables
		tempCase = new Case();
		
		//Default counters set
		selectedOrders = 0;
		selCounter = 0;
		
		//Default infoMessage set to blank
		infoMessage = '';
		
		//Call method to check time stamp
		inIt();
		
		//new instance of Current User
		currentUser = new User();
		
		//query record of current user if current user is related to Partner Community - Vendors OrderSelect profile user
		//Code Modified - Padmesh Soni (08/19/2014) - NS Communities Migration
		//Modify filter from Order Select profile with Parnter community Vendor profile
		List<User> users = [SELECT Name, ContactId, Contact.FirstName, Contact.LastName, Contact.Order_Count__c, Contact.Grass_Cut_Cap__c, Contact.OtherStreet, Contact.OtherState, 
								Contact.OtherCountry, Contact.OtherCity, Contact.OtherPostalCode, Contact.Other_Address_Geocode__latitude__s,
								Contact.Other_Address_Geocode__longitude__s, Contact.Service_Range__c FROM User WHERE Id =: UserInfo.getUserId() 
								AND (Profile.Name =: VSOAM_Constants.PROFILE_NAME_CUSTOMER_PORTAL_ORDERSELECT or Profile.Name =: VSOAM_Constants.PROFILE_NAME_COMMUNITY_PLUS_USER
								OR Profile.Name =: VSOAM_Constants.PROFILE_NAME_PARTNER_COMMUNITY_VENDORS
								OR Profile.Name =: VSOAM_Constants.PROFILE_NAME_PARTNER_COMMUNITY_VENDORS_LOGIN
								)];
								
		//Check for size of users list
		if(users.size() > 0) {
			
			//assign frist user of list to current user instance
			currentUser = users[0];
			
			//Getting initials of Current logged in user FirstName and LastName
			vendorInitials = (currentUser.Contact.FirstName != null? currentUser.Contact.FirstName.subString(0, 1): '') + currentUser.Contact.LastName.subString(0, 1);
		} else {
			
			//intialize with false on isAvailable flag 
			//thats why no Assignment tool will be shown on page
			isAvailable = VSOAM_Constants.BOOL_OFF;
			
			//add error message on page
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.VOAM_NOT_AVAILABLE));
		}
	}
	
	//Code added - Padmesh Soni (08/28/2014) - SM-183 Last Login isn't updated on Contact
	/**
	 *	@descpriton		:	This method is used to update the Last Login Date/Time field on logged in user's Contact.
	 *
	 *	@param			:
	 *
	 *	@return			:	void
	 **/
	public void checkIp(){
		
		//Getting current IP address
		String ip = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
		
		//Store current logged in user's Id
		Id UserId = UserInfo.getUserId();
		
		//Check if IP address not equal to '98.190.130.251'
		if (ip != '98.190.130.251') {
			
			//Query result of Users
			User currentUser =  [SELECT ContactId FROM User WHERE Id =:UserId];
			
			//List to hold contact query result
			List<Contact> currentContact = [SELECT Last_Login_Date_Time__c FROM Contact WHERE Id =: currentUser.ContactId];
			
			//List to hold Login History of logged in user
			List<LoginHistory> history = [SELECT LoginTime FROM LoginHistory WHERE UserId =: UserId ORDER BY LoginTime DESC LIMIT 1];
			
			//Validate with different conditions & call the future method
			if (currentContact.size() > 0 && !history.isEmpty() && history[0].LoginTime != currentContact[0].Last_Login_Date_Time__c)
				futureUpdateLoginDate(currentContact[0].Id,history[0].LoginTime);
		}
		
	}
	
	//Code added - Padmesh Soni (08/28/2014) - SM-183 Last Login isn't updated on Contact
	/**
	 *	@descpriton		:	This method is future method is used to update the Last Login Date/Time field on Contact.
	 *
	 *	@param			:	Id contactId, DateTime loginDate
	 *
	 *	@return			:	void
	 **/
	@future
	private static void futureUpdateLoginDate(Id contactId, DateTime loginDate) {
		
		//Create an instance of Contact for assignment of Datetime to field of contact
		Contact currentContact = new Contact(Id = contactId, Last_Login_Date_Time__c = loginDate);
		
		//update the contact
		database.update(currentContact,false);
	}
	
	/**
	 *	@descpriton		:	This method is used to refresh the current user instance with querying user of Current logged in.
	 *
	 *	@param			:	 
	 *
	 *	@return			:	PageReference
	 **/
	public PageReference refreshUser(){
		
		//Getting record of current logged in user
		//Code Modified - Padmesh Soni (08/19/2014) - NS Communities Migration
		//Modify filter from Order Select profile with Parnter community Vendor profile
		List<User> users = [SELECT Name, Contact.FirstName, Contact.LastName, ContactId, Contact.Order_Count__c, Contact.Grass_Cut_Cap__c, Contact.OtherStreet, Contact.OtherState, 
								Contact.OtherCountry, Contact.OtherCity, Contact.OtherPostalCode, Contact.Other_Address_Geocode__latitude__s,
								Contact.Other_Address_Geocode__longitude__s, Contact.Service_Range__c FROM User WHERE Id =: UserInfo.getUserId() 
								AND (Profile.Name =: VSOAM_Constants.PROFILE_NAME_CUSTOMER_PORTAL_ORDERSELECT or Profile.Name =: VSOAM_Constants.PROFILE_NAME_COMMUNITY_PLUS_USER
								OR Profile.Name =: VSOAM_Constants.PROFILE_NAME_PARTNER_COMMUNITY_VENDORS
								OR Profile.Name =: VSOAM_Constants.PROFILE_NAME_PARTNER_COMMUNITY_VENDORS_LOGIN)];
								
										
		//Check for size of users list
		if(users.size() > 0) {
			
			//assign frist user of list to current user instance
			currentUser = users[0];
			
			//Getting initials of Current logged in user FirstName and LastName
			vendorInitials = (currentUser.Contact.FirstName != null? currentUser.Contact.FirstName.subString(0, 1): '' )+ currentUser.Contact.LastName.subString(0, 1);
		}
		
		return null;
	}
	
	/**
	 *	@descpriton		:	This method is used to check processing time between 5:00AM and 8:00PM
	 *
	 *	@param			:	 
	 *
	 *	@return			:	void
	 **/
	public static void inIt() {
		
		//Central Standard Time zone calculations
		TimeZone tz = TimeZone.getTimeZone(VSOAM_Constants.TIMEZONE_CST_ID_AMERICA_CHICAGO);
		
		//Getting offset of CST time zone
		Integer tzOffset =  tz.getOffset(DateTime.now());
		
		//Getting hours to add when changing any time into CST zone
		Integer hoursToAdd = (tzOffset/VSOAM_Constants.TZ_OFFSET_DIVISOR);
		
		//Present date with time
		DateTime dtm = System.now();
		
		//Getting time
		Time presentTime = dtm.time();
		
		//Current user time zone
		TimeZone currentTZ = UserInfo.getTimeZone();
		
		//Current user time zone Offset
		Integer currenttzOffset =  currentTZ.getOffset(DateTime.now());
		
		//Difference b/w CST(America/Chicago) timezone and Current user timezone 
		integer diffOffset = tzOffset - currenttzOffset;
		
		//Getting hours to add into current user's present time
		Decimal currentHoursToAdd = (diffOffset * 1.0/VSOAM_Constants.TZ_OFFSET_DIVISOR);
		
		//If currentHoursToAdd is in minus
		if(currentHoursToAdd < 0) {
			
			//Getting split by decimal point
			String tempDuration = String.valueOf(currentHoursToAdd);
			tempDuration = tempDuration.replace(VSOAM_Constants.MINUS, '');
			String[] duration = tempDuration.split(VSOAM_Constants.DECIMAL_POINT);
			
			//Minutes to add into time
			integer minutesToAdd = integer.valueOf(duration[1]) * 6;
			
			//getting time into CST zone 
			presentTime = presentTime.addHours(-(integer.valueOf(duration[0]))).addMinutes(-(minutesToAdd));
		} else {
			
			//Getting split by decimal point 
			String tempDuration = String.valueOf(currentHoursToAdd);
			String[] duration = tempDuration.split(VSOAM_Constants.DECIMAL_POINT);
			
			//Minutes to add into time  
			integer minutesToAdd = integer.valueOf(duration[1]) * 6;
			
			//getting time into CST zone 
			presentTime = presentTime.addHours(integer.valueOf(duration[0])).addMinutes(minutesToAdd);
		}
		
		//Time instance of market openning
		Time openTime = Time.newInstance(5, 0, 0, 0);
		
		//Time instance of market closing
		Time closeTime = Time.newInstance(20, 0, 0, 0);
		
		//Mean time instances
		Time beforeDayTime = Time.newInstance(23, 59, 59, 0);
		Time afterDayTime = Time.newInstance(0, 0, 0, 0);
		
		//System.debug('presentTime :::'+ presentTime);
		
		//Check for current time is between 5:00AM and 8:00PM    
		if((presentTime >= openTime && presentTime <= closeTime)) {
			
			//assign true to isAvailable
			isAvailable = VSOAM_Constants.BOOL_ON;
		} else {
			
			isAvailable = VSOAM_Constants.BOOL_ON;
			//assign false to isAvailable
			//isAvailable = VSOAM_Constants.BOOL_OFF;
			
			//Add user friendly message on page on getting serialization of Summary
			//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, Label.VOAM_Closed));
		}
	}
	
	/**
	 *	@descpriton		:	This method is used to update all related work orders with new Owner.
	 *
	 *	@param			:	String[] orderIds, String ownId 
	 *
	 *	@return			:	void
	 **/
	@RemoteAction
	public static void checkAssignWorkOrders(String[] orderIds) {
		
		//Call method to check time stamp
		inIt();
		
		//Query result of User 
		List<User> assignee = [SELECT Id, Name, Contact.Order_Count__c, Contact.Grass_Cut_Cap__c, Contact.Status__c, IsActive FROM User 
								WHERE Id =: UserInfo.getUserId() AND (Profile.Name =: VSOAM_Constants.PROFILE_NAME_CUSTOMER_PORTAL_ORDERSELECT or Profile.Name =: VSOAM_Constants.PROFILE_NAME_COMMUNITY_PLUS_USER
								OR Profile.Name =: VSOAM_Constants.PROFILE_NAME_PARTNER_COMMUNITY_VENDORS
								OR Profile.Name =: VSOAM_Constants.PROFILE_NAME_PARTNER_COMMUNITY_VENDORS_LOGIN) 
								AND IsActive =: VSOAM_Constants.BOOL_ON];
		
		//Check if queried user's Contact status is containing Active
		if(assignee.size() > 0 && (assignee[0].Contact.Status__c != null 
			&& assignee[0].Contact.Status__c.containsIgnoreCase(VSOAM_Constants.USER_ISACTIVE_ACTIVE))) {
			if(assignee[0].Contact.Order_Count__c == null){
				assignee[0].Contact.Order_Count__c = 0;
			}
			//Check if ownId is not blank or null
			if(orderIds.size() > 0) {
				
				//initialize new list of Cases
				List<Case> workOrders = new List<Case>();
				
				try {
					
					//query Case records which are selected 
					//by user and those are having Owner name as "OS"
					//Padmesh Soni (05/13/2014) - SSA-32 - Check serviceable when assigning order
					//Add "Serviceable__c" field into query
					workOrders = [SELECT Id, Owner.Name, Serviceable__c FROM Case 
									WHERE Id IN: orderIds AND Owner.Name =: VSOAM_Constants.CASE_QUEUE_NAME_OS FOR Update];
				} catch(QueryException e) {
					
					//Check for DML Status code	
					if(e.getDmlStatusCode(0).containsIgnoreCase(VSOAM_Constants.DML_EXCEPTION_UNABLE_TO_LOCK_ROW)) {
						
						//throw this exception on Locking of 
						//row before updating the record from another end
						throw new CustomException(e.getMessage());						
					} else{
						throw e;
					}
				}
				
				//Total orders to be assigned to user
				integer totalOrdersToBeAssign = orderIds.size() + integer.valueOf(assignee[0].Contact.Order_Count__c);
				
				//Check for capability of User 
				if(totalOrdersToBeAssign > assignee[0].Contact.Grass_Cut_Cap__c) {
					
					//Total removable orders for User's contact
					integer limitAssign = totalOrdersToBeAssign - integer.valueOf(assignee[0].Contact.Grass_Cut_Cap__c);
					
					//Getting label of limit
					String errorMeassage = Label.VOAM_LIMIT;
					
					//Format string for error message with values
					errorMeassage = errorMeassage.replace(VSOAM_Constants.VENDOR_CONTACT_ORDER_COUNT, String.valueOf(assignee[0].Contact.Order_Count__c)).replace(VSOAM_Constants.VENDOR_CONTACT_GRASS_CUT_CAP, String.valueOf(assignee[0].Contact.Grass_Cut_Cap__c)).replace(VSOAM_Constants.ORDER_SIZE, String.valueOf(orderIds.size())).replace(VSOAM_Constants.ORDER_ASSIGN_LIMIT, String.valueOf(limitAssign));
					
					//Throw custom exception
					throw new CustomException(errorMeassage);
				}
				
				//Quering records from cases 
				List<Case> workOrdersToUpdate = new List<Case>();
				
				//Padmesh Soni (05/13/2014) - SSA-32 - Check serviceable when assigning order
				//Counter of assigned orders
				integer assignedOrdersCount = 0;
				
				//Loop through queried records
				for(Case wOrder : workOrders) {
					
					//Padmesh Soni (05/13/2014) - SSA-32 - Check serviceable when assigning order
					//Checking for Serviceable is true or Yes
					if(wOrder.Serviceable__c == VSOAM_Constants.CASE_SERVICEABLE_YES) {
					
						//assinging new owner id
						workOrdersToUpdate.add(new Case(Id = wOrder.Id, OwnerId = assignee[0].Id));
					} else {
						
						//Padmesh Soni (05/13/2014) - SSA-32 - Check serviceable when assigning order
						//increment the counter of already assigned Orders counting
						assignedOrdersCount++; 
					}
				}
				
				//Padmesh Soni (05/13/2014) - SSA-32 - Check serviceable when assigning order
				//Check if already assigned Orders count is greater than Zero
				//then throw a custom exception message on page.
				if(assignedOrdersCount > 0) {
					
					//Padmesh Soni (05/13/2014) - SSA-32 - Check serviceable when assigning order
					//Variable to hold error message
					String tempErrorMessage = Label.VOAM_NO_SERVICEABLE.replace('Number', String.valueOf(assignedOrdersCount));
					throw new CustomException(tempErrorMessage);
				}
				
				//Update records with Database.update
				update workOrdersToUpdate;
			
			} else {
				
				//throw User friendly message
				throw new CustomException(Label.VOAMP_Not_Selected);
			}
				
		} else {
			
			//throw User friendly message user don't have access on assignment
			throw new CustomException(Label.VOAM_NO_Access);
		}
		
	}
	
	/**
	 *	@descpriton		:	This method is used to getting popup on window.
	 *
	 *	@param			: 
	 *
	 *	@return			:	void
	 **/
	public void showPopup() {
    	
    	//assign true to displayPopup flag
    	displayPopup = VSOAM_Constants.BOOL_ON;
    }
    
    /**
	 *	@descpriton		:	This method is used to close popup on window.
	 *
	 *	@param			: 
	 *
	 *	@return			:	void
	 **/
	public void closePopup() {
        
        //assign false to displayPopup flag
    	displayPopup = VSOAM_Constants.BOOL_OFF;
    }
	
	/**
	 *	@descpriton		:	This method is used to getting Orders to be claimed if there is no Work Order already changed with another owner except OS queue.
	 *
	 *	@param			: 
	 *
	 *	@return			:	void
	 **/
	public void getOrdersToBeClaimed() {
		
		//Check for present time is 
		//into working duration with calling inIt method
		inIt();
		
		//Set to hold all workOrder number "CaseNumber"
		Set<String> woNumbers = new Set<String>();
		
		//Loop through string with spliting of "comma"
		for(String woNumber : ordersJSON.split(',')) {
			
			//add Case Number into set
			if(!String.isBlank(woNumber))
				woNumbers.add(woNumber);
		}
		
		//Query result of OS queues
        Group osGroup = [SELECT Id, Name FROM Group WHERE Name =: VSOAM_Constants.CASE_QUEUE_NAME_OS AND Type =: VSOAM_Constants.GROUP_TYPE_QUEUE];
        
		//query Case records which are selected by user and those are having another Owner except case_queue OS
		List<Case> workOrders = [SELECT Id, Owner.Name FROM Case WHERE CaseNumber IN: woNumbers AND OwnerId !=: osGroup.Id];
		
		//Check for size of workOrders list
		if(workOrders.size() > 0) {
			
			//Checking if selected order and workorders 
			//both are equal into number of cases
			if(woNumbers.size() == workOrders.size()) {
				
				//Creating error message
				infoMessage = Label.VOAM_WARN_SELECT_ANOTHER;
			} else {
				
				//assign true to flag
				checkAvailable = VSOAM_Constants.BOOL_ON;	
				
				//Creating error message
				infoMessage = Label.VOAM_TOO_SLOW.replace('{!workOrderssize}', workOrders.size()+'');
			}
		} 
				
		//call showPopup
		showPopup();
		
		//Getting size of list
		selCounter = woNumbers.size();	
	}
	
	public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}