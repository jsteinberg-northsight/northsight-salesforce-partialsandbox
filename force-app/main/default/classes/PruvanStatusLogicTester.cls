@isTest(SeeAllData=false)
public without sharing class PruvanStatusLogicTester {
//	private static Case newCase;
//	private static String photoHelperPayload;
//	private static String surveyHelperPayload;
//	private static String surveyPhotoHelperPayload;
//	private static String workOrderStatusPayload;
//	
//	private static void generateTestCase() {
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//   		//create a new account
//		Account newAccount = new Account(
//			Name = 'Tester Account'
//		);
//		insert newAccount;
//	
//		//create a new contact
//		Contact newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest',
//			MailingStreet = '30 Turnberry Dr',
//			MailingCity = 'La Place',
//			MailingState = 'LA',
//			MailingPostalCode = '70068',
//			OtherStreet = '30 Turnberry Dr',
//			OtherCity = 'La Place',
//			OtherState = 'LA',
//			OtherPostalCode = '70068',
//			Other_Address_Geocode__Latitude__s = 30.00,
//			Other_Address_Geocode__Longitude__s = -90.00
//		);
//		insert newContact;
//		
//		//create a new test case
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		newCase = new Case();
//		newCase.street_address__c = '1234 Street';
//        newCase.state__c = 'TX';
//        newCase.city__c = 'Austin';
//        newCase.zip_code__c = '78704';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        newCase.Client__c = 'NOT-SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'Grass Recut';
//        newCase.Work_Completed__c = 'No';
//        newCase.Client_Name__c = account.Id;
//        insert newCase;
//	}
//	
//	private static void generateThreeEightPayloads(){
//		for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//			photoHelperPayload = 	'{' +
//								    '"timestamp": 1377559122,' +
//								    '"serviceId": "32981757",' +
//								    '"workOrderId": "37246450",' +
//								    '"evidenceType": "Survey",' +
//								    '"fileType": "picture",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "d9aab2a9-eec6-470e-bf13-0cb74ca47e75",' +
//								    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//								    '"fileName": "1382536931021.jpg",' +
//								    '"gpsLatitude": "31.62020098900615",' +
//								    '"gpsLongitude": "-97.099915453764",' +
//								    '"gpsTimestamp": "1382536931",' +
//								    '"gpsAccuracy": "2.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706368.jpg"' +
//									'}';
//									
//			surveyHelperPayload = 	'{' +
//									'"uuId": "484e4ad2-0ba8-4c71-bc95-01a8a61e6d80",'+
//									'"username": "sarcj07",'+
//									'"key1": "40775372",'+
//									'"key2": "NSM::'+ c.CaseNumber +'",'+
//									'"key3": "36853608",'+
//									'"key4": "Maid Service",'+
//									'"key5": null,'+
//									'"fileType": "survey",'+
//									'"template": "sarcj07::MS22_001-v10",'+
//									'"uploaderVersion": "mobile 3.8.7",'+
//									'"srCertifiedPhoto": "1",'+
//									'"srCertifiedTime": "1",'+
//									'"srCertifiedLocation": "1",'+
//									'"srPictureCount": null,'+
//									'"gpsLatitude": "41.56850549391771",'+
//									'"gpsLongitude": "-87.563975181468",'+
//									'"gpsTimestamp": "1386617060",'+
//									'"gpsAccuracy": "8.0",'+
//									'"timestamp": 1386617073,'+
//									'"meta": {'+
//										'"currentQuestionIndex": 26,'+
//										'"surveyId": "484e4ad2-0ba8-4c71-bc95-01a8a61e6d80",'+
//										'"parentId": "40775372",'+
//										'"surveyTemplateId": "sarcj07::MS22_001-v10"'+
//									'},'+
//									'"answers": [{' +
//								            '"answer": ["yes"],' +
//								            '"id": "Work_Completed__c",' +
//								            '"picIds": ["d9aab2a9-eec6-470e-bf13-0cb74ca47e75"],' +
//								            '"hint": "Select No for Trip Charge"' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Backyard_Serviced__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Excessive_Growth__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Shrubs_Trimmed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Debris_Removed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["Excessive Grass/Weed Height"],' +
//								            '"id": "Bid_Type__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["This is the bid description."],' +
//								            '"id": "Bid_Description__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["100"],' +
//								            '"id": "Bid_Cost__c",' +
//								            '"hint": ""' +
//								        '}]' +
//									'}';
//									
//			surveyPhotoHelperPayload = 	'{' +
//								    '"timestamp": 1377559123,' +
//								    '"serviceId": "32786734",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "Survey",' +
//								    '"fileType": "survey",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//								    '"parentUuid": "37064308",' +
//								    '"fileName": "survey_1382537334014.json",' +
//								    '"gpsLatitude": "42.01235463",' +
//								    '"gpsLongitude": "-87.89172686",' +
//								    '"gpsTimestamp": "1382537316",' +
//								    '"gpsAccuracy": "10.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"uploaderVersion": "mobile 3.8.1",' +
//								    '"deviceId": "ios-1B3F968D-5F14-47F0-95EC-89A15EFBA580",' +
//								    '"sdkVersion": "7.0.4",' +
//								    '"survey": {' +
//								        '"answers": [{' +
//								            '"answer": ["yes"],' +
//								            '"id": "Work_Completed__c",' +
//								            '"picIds": ["d9aab2a9-eec6-470e-bf13-0cb74ca47e75"],' +
//								            '"hint": "Select No for Trip Charge"' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Backyard_Serviced__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Excessive_Growth__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Shrubs_Trimmed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Debris_Removed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["Excessive Grass/Weed Height"],' +
//								            '"id": "Bid_Type__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["This is the bid description."],' +
//								            '"id": "Bid_Description__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["100"],' +
//								            '"id": "Bid_Cost__c",' +
//								            '"hint": ""' +
//								        '}],' +
//								        '"meta": {' +
//								            '"currentQuestionIndex": 18,' +
//								            '"surveyId": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//								            '"parentId": "37064308",' +
//								            '"surveyTemplateId": "sarcj07::GC_001-v4"' +
//								        '}' +
//								    '},' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706991.json"' +
//									'}';
//									
//			workOrderStatusPayload = '{"workOrders": [{"workOrderNumber": "NSM::' + c.CaseNumber + '","status": "Complete"}]}';
//		}
//	}
//	
//	private static void generateThreeNinePayloads(){
//		for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//			photoHelperPayload = 	'{' +
//								    '"timestamp": 1377559122,' +
//								    '"serviceId": "32981757",' +
//								    '"workOrderId": "37246450",' +
//								    '"evidenceType": "Survey",' +
//								    '"fileType": "picture",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "d9aab2a9-eec6-470e-bf13-0cb74ca47e75",' +
//								    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//								    '"fileName": "1382536931021.jpg",' +
//								    '"gpsLatitude": "31.62020098900615",' +
//								    '"gpsLongitude": "-97.099915453764",' +
//								    '"gpsTimestamp": "1382536931",' +
//								    '"gpsAccuracy": "2.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706368.jpg"' +
//									'}';
//									
//			surveyHelperPayload = 	'{' +
//									'"uuId": "484e4ad2-0ba8-4c71-bc95-01a8a61e6d80",'+
//									'"username": "sarcj07",'+
//									'"key1": "40775372",'+
//									'"key2": "NSM::'+ c.CaseNumber +'",'+
//									'"key3": "36853608",'+
//									'"key4": "Maid Service",'+
//									'"key5": null,'+
//									'"fileType": "survey",'+
//									'"template": "sarcj07::MS22_001-v10",'+
//									'"uploaderVersion": "mobile 3.9",'+
//									'"srCertifiedPhoto": "1",'+
//									'"srCertifiedTime": "1",'+
//									'"srCertifiedLocation": "1",'+
//									'"srPictureCount": null,'+
//									'"gpsLatitude": "41.56850549391771",'+
//									'"gpsLongitude": "-87.563975181468",'+
//									'"gpsTimestamp": "1386617060",'+
//									'"gpsAccuracy": "8.0",'+
//									'"timestamp": 1386617073,'+
//									'"meta": {'+
//										'"currentQuestionIndex": 26,'+
//										'"surveyId": "484e4ad2-0ba8-4c71-bc95-01a8a61e6d80",'+
//										'"parentId": "40775372",'+
//										'"surveyTemplateId": "sarcj07::MS22_001-v10"'+
//									'},'+
//									'"answers": [{' +
//								            '"answer": ["yes"],' +
//								            '"id": "Work_Completed__c",' +
//								            '"picIds": ["d9aab2a9-eec6-470e-bf13-0cb74ca47e75"],' +
//								            '"hint": "Select No for Trip Charge"' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Backyard_Serviced__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Excessive_Growth__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Shrubs_Trimmed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Debris_Removed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["Excessive Grass/Weed Height"],' +
//								            '"id": "Bid_Type__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["This is the bid description."],' +
//								            '"id": "Bid_Description__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["100"],' +
//								            '"id": "Bid_Cost__c",' +
//								            '"hint": ""' +
//								        '}]' +
//									'}';
//									
//			surveyPhotoHelperPayload = 	'{' +
//								    '"timestamp": 1377559123,' +
//								    '"serviceId": "32786734",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "Survey",' +
//								    '"fileType": "survey",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//								    '"parentUuid": "37064308",' +
//								    '"fileName": "survey_1382537334014.json",' +
//								    '"gpsLatitude": "42.01235463",' +
//								    '"gpsLongitude": "-87.89172686",' +
//								    '"gpsTimestamp": "1382537316",' +
//								    '"gpsAccuracy": "10.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"uploaderVersion": "mobile 3.9.1",' +
//								    '"deviceId": "ios-1B3F968D-5F14-47F0-95EC-89A15EFBA580",' +
//								    '"sdkVersion": "7.0.4",' +
//								    '"survey": {' +
//								        '"answers": [{' +
//								            '"answer": ["yes"],' +
//								            '"id": "Work_Completed__c",' +
//								            '"picIds": ["d9aab2a9-eec6-470e-bf13-0cb74ca47e75"],' +
//								            '"hint": "Select No for Trip Charge"' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Backyard_Serviced__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Excessive_Growth__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Shrubs_Trimmed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Debris_Removed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["Excessive Grass/Weed Height"],' +
//								            '"id": "Bid_Type__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["This is the bid description."],' +
//								            '"id": "Bid_Description__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["100"],' +
//								            '"id": "Bid_Cost__c",' +
//								            '"hint": ""' +
//								        '}],' +
//								        '"meta": {' +
//								            '"currentQuestionIndex": 18,' +
//								            '"surveyId": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//								            '"parentId": "37064308",' +
//								            '"surveyTemplateId": "sarcj07::GC_001-v4"' +
//								        '}' +
//								    '},' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706991.json"' +
//									'}';
//									
//			workOrderStatusPayload = '{"workOrders": [{"workOrderNumber": "NSM::' + c.CaseNumber + '","status": "Complete"}]}';
//		}
//	}
//	
//	private static void generateNoWorkCompletedThreeEightPayloads(){
//		for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//			photoHelperPayload = 	'{' +
//								    '"timestamp": 1377559122,' +
//								    '"serviceId": "32981757",' +
//								    '"workOrderId": "37246450",' +
//								    '"evidenceType": "Survey",' +
//								    '"fileType": "picture",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "d9aab2a9-eec6-470e-bf13-0cb74ca47e75",' +
//								    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//								    '"fileName": "1382536931021.jpg",' +
//								    '"gpsLatitude": "31.62020098900615",' +
//								    '"gpsLongitude": "-97.099915453764",' +
//								    '"gpsTimestamp": "1382536931",' +
//								    '"gpsAccuracy": "2.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706368.jpg"' +
//									'}';
//									
//			surveyPhotoHelperPayload = 	'{' +
//								    '"timestamp": 1377559123,' +
//								    '"serviceId": "32786734",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "Survey",' +
//								    '"fileType": "survey",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//								    '"parentUuid": "37064308",' +
//								    '"fileName": "survey_1382537334014.json",' +
//								    '"gpsLatitude": "42.01235463",' +
//								    '"gpsLongitude": "-87.89172686",' +
//								    '"gpsTimestamp": "1382537316",' +
//								    '"gpsAccuracy": "10.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"uploaderVersion": "mobile 3.8.1",' +
//								    '"deviceId": "ios-1B3F968D-5F14-47F0-95EC-89A15EFBA580",' +
//								    '"sdkVersion": "7.0.4",' +
//								    '"survey": {' +
//								        '"answers": [{' +
//								            '"answer": ["yes"],' +
//								            '"id": "Backyard_Serviced__c",' +
//								            '"picIds": ["d9aab2a9-eec6-470e-bf13-0cb74ca47e75"],' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Excessive_Growth__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Shrubs_Trimmed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Debris_Removed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["Excessive Grass/Weed Height"],' +
//								            '"id": "Bid_Type__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["This is the bid description."],' +
//								            '"id": "Bid_Description__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["100"],' +
//								            '"id": "Bid_Cost__c",' +
//								            '"hint": ""' +
//								        '}],' +
//								        '"meta": {' +
//								            '"currentQuestionIndex": 18,' +
//								            '"surveyId": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//								            '"parentId": "37064308",' +
//								            '"surveyTemplateId": "sarcj07::GC_001-v4"' +
//								        '}' +
//								    '},' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706991.json"' +
//									'}';
//		}
//	}
//	
//	private static void generateNoWorkCompletedThreeNinePayloads(){
//		for(Case c : [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id]){
//			photoHelperPayload = 	'{' +
//								    '"timestamp": 1377559122,' +
//								    '"serviceId": "32981757",' +
//								    '"workOrderId": "37246450",' +
//								    '"evidenceType": "Survey",' +
//								    '"fileType": "picture",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "d9aab2a9-eec6-470e-bf13-0cb74ca47e75",' +
//								    '"parentUuid": "7571f782-0ccc-484b-a708-df4a0f5176ee",' +
//								    '"fileName": "1382536931021.jpg",' +
//								    '"gpsLatitude": "31.62020098900615",' +
//								    '"gpsLongitude": "-97.099915453764",' +
//								    '"gpsTimestamp": "1382536931",' +
//								    '"gpsAccuracy": "2.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706368.jpg"' +
//									'}';
//									
//			surveyPhotoHelperPayload = 	'{' +
//								    '"timestamp": 1377559123,' +
//								    '"serviceId": "32786734",' +
//								    '"workOrderId": "' + c.CaseNumber + '",' +
//								    '"evidenceType": "Survey",' +
//								    '"fileType": "survey",' +
//								    '"csrCertifiedPhoto": "1",' +
//								    '"csrCertifiedTime": "1",' +
//								    '"csrCertifiedLocation": null,' +
//								    '"csrPictureCount": null,' +
//								    '"uuid": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//								    '"parentUuid": "37064308",' +
//								    '"fileName": "survey_1382537334014.json",' +
//								    '"gpsLatitude": "42.01235463",' +
//								    '"gpsLongitude": "-87.89172686",' +
//								    '"gpsTimestamp": "1382537316",' +
//								    '"gpsAccuracy": "10.0",' +
//								    '"key2": "' + c.CaseNumber + '",' +
//								    '"key4": "Grass Recut",' +
//								    '"username": "pruvan",' +
//								    '"password": "8c8cd34c37d6813808baa358b18169c211cb47f3",' +
//								    '"uploaderVersion": "mobile 3.9.1",' +
//								    '"deviceId": "ios-1B3F968D-5F14-47F0-95EC-89A15EFBA580",' +
//								    '"sdkVersion": "7.0.4",' +
//								    '"survey": {' +
//								        '"answers": [{' +
//								            '"answer": ["yes"],' +
//								            '"id": "Backyard_Serviced__c",' +
//								            '"picIds": ["d9aab2a9-eec6-470e-bf13-0cb74ca47e75"],' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Excessive_Growth__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["yes"],' +
//								            '"id": "Shrubs_Trimmed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["no"],' +
//								            '"id": "Debris_Removed__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["Excessive Grass/Weed Height"],' +
//								            '"id": "Bid_Type__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["This is the bid description."],' +
//								            '"id": "Bid_Description__c",' +
//								            '"hint": ""' +
//								        '}, {' +
//								            '"answer": ["100"],' +
//								            '"id": "Bid_Cost__c",' +
//								            '"hint": ""' +
//								        '}],' +
//								        '"meta": {' +
//								            '"currentQuestionIndex": 18,' +
//								            '"surveyId": "71e3d9ac-68b1-464d-a09e-9783223a612c",' +
//								            '"parentId": "37064308",' +
//								            '"surveyTemplateId": "sarcj07::GC_001-v4"' +
//								        '}' +
//								    '},' +
//								    '"imageLink": "https://allyearimages.s3.amazonaws.com/' + c.CaseNumber + '/46706991.json"' +
//									'}';
//									
//			workOrderStatusPayload = '{"workOrders": [{"workOrderNumber": "NSM::' + c.CaseNumber + '","status": "Complete"}]}';
//		}
//	}
//	
//	//--------------------------------------------------------
//   	//TESTS
//   	//Unit tests for method testing and regression testing. 
//   	//--------------------------------------------------------
//   		
//   	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - test the functionality of PruvanStatusLogicHelper is working correctly
//   	//for mobile version 3.8 payloads
//	public static testmethod void pruvanStatusLogicTestThreeEight(){
//		system.Test.startTest();
//		
//		generateTestCase();
//		generateThreeEightPayloads();
//		
//		/*/send the surveyHelperPayload thru PruvanWebServices class to the PruvanSurveyHelper class
//		PruvanWebServices.doSurveyValidate(surveyHelperPayload);
//		//assert that the mobile version field for newCase has been set as 3.8 for this test
//		for(Case c : [SELECT Mobile_Version__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals(3.8, c.Mobile_Version__c);
//		}*/
//		
//		//assert that the following fields have not been set when no callouts have been made to the proper methods
//		for(Case c : [SELECT Pruvan_Status__c, Survey_Received__c, All_Images_Received__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals(null, c.Pruvan_Status__c);
//			system.assertEquals(false, c.Survey_Received__c);
//			system.assertEquals(false, c.All_Images_Received__c);
//		}
//		
//		//send the photoHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(photoHelperPayload);
//		//assert that the following fields have not been set except for All_Images_Received
//		for(Case c : [SELECT Pruvan_Status__c, Survey_Received__c, All_Images_Received__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals(null, c.Pruvan_Status__c);
//			system.assertEquals(false, c.Survey_Received__c);
//			system.assertEquals(true, c.All_Images_Received__c);
//		}
//		
//		//send the surveyHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(surveyPhotoHelperPayload);
//		//assert that the All_Images_Received field & the Survey_Received field have been set except for the Pruvan_Status field since mobile version is coming from 3.8
//			//and also assert because we are submitting using mobile 3.8 that the Approval_Status field = 'Pre-Pending'
//		for(Case c : [SELECT Pruvan_Status__c, Survey_Received__c, All_Images_Received__c, Approval_Status__c, Mobile__c, Mobile_Version__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals(3.8, c.Mobile_Version__c);
//			system.assertEquals(null, c.Pruvan_Status__c);
//			system.assertEquals(true, c.Survey_Received__c);
//			system.assertEquals(true, c.All_Images_Received__c);
//			//system.assertEquals('Pre-Pending', c.Approval_Status__c);
//			//system.assertEquals(true, c.Mobile__c);
//		}
//		
//		system.Test.stopTest();
//	}
//	
//	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - test the functionality of PruvanStatusLogicHelper is working correctly
//   	//for mobile version 3.9 payloads
//	public static testmethod void pruvanStatusLogicTestThreeNine(){
//		system.Test.startTest();
//		
//		generateTestCase();
//		generateThreeNinePayloads();
//		
//		/*/send the surveyHelperPayload thru PruvanWebServices class to the PruvanSurveyHelper class
//		PruvanWebServices.doSurveyValidate(surveyHelperPayload);
//		//assert that the mobile version field for newCase has been set as 3.9 for this test
//		for(Case c : [SELECT Mobile_Version__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals(3.9, c.Mobile_Version__c);
//		}*/
//		
//		//assert that the following fields have not been set when no callouts have been made to the proper methods
//		for(Case c : [SELECT Pruvan_Status__c, Survey_Received__c, All_Images_Received__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals(null, c.Pruvan_Status__c);
//			system.assertEquals(false, c.Survey_Received__c);
//			system.assertEquals(false, c.All_Images_Received__c);
//		}
//		
//		//send the photoHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(photoHelperPayload);
//		//assert that the following fields have not been set except for the All_Images_Received field
//		for(Case c : [SELECT Pruvan_Status__c, Survey_Received__c, All_Images_Received__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals(null, c.Pruvan_Status__c);
//			system.assertEquals(false, c.Survey_Received__c);
//			system.assertEquals(true, c.All_Images_Received__c);
//		}
//		
//		//send the surveyHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(surveyPhotoHelperPayload);
//		//assert that the All_Images_Received field & the Survey_Received field have been set except for the Pruvan_Status field
//		for(Case c : [SELECT Pruvan_Status__c, Survey_Received__c, All_Images_Received__c, Mobile_Version__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals(3.9, c.Mobile_Version__c);
//			system.assertEquals(null, c.Pruvan_Status__c);
//			system.assertEquals(true, c.Survey_Received__c);
//			system.assertEquals(true, c.All_Images_Received__c);
//		}
//		
//		//send the workOrderStatusPayload thru PruvanWebServices to the PruvanOrderStatusHelper class
//		PruvanWebServices.doWorkOrderStatus(workOrderStatusPayload);
//		//assert that all fields have been set and that the order's Approval_Status field = 'Pre-Pending'
//		//for(Case c : [SELECT Pruvan_Status__c, Survey_Received__c, All_Images_Received__c, Approval_Status__c, Mobile__c FROM Case WHERE Id =: newCase.Id]){
//			//system.assertEquals('Complete', c.Pruvan_Status__c);
//			//system.assertEquals(true, c.Survey_Received__c);
//			//system.assertEquals(true, c.All_Images_Received__c);
//			//system.assertEquals('Pre-Pending', c.Approval_Status__c);
//			//system.assertEquals(true, c.Mobile__c);
//		//}
//		
//		system.Test.stopTest();
//	}
//	
//   	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - test that Pre-Pending orders can be set back to Not Started/Rejected
//   	//for mobile 3.8
//	public static testmethod void pruvanStatusLogicTestThreeEightPrePending(){
//		system.Test.startTest();
//		
//		generateTestCase();
//		generateThreeEightPayloads();
//		
//		/*/send the surveyHelperPayload thru PruvanWebServices class to the PruvanSurveyHelper class
//		PruvanWebServices.doSurveyValidate(surveyHelperPayload);
//		//assert that the mobile version field for newCase has been set as 3.8 for this test
//		for(Case c : [SELECT Mobile_Version__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals(3.8, c.Mobile_Version__c);
//		}*/
//		//send the photoHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(photoHelperPayload);
//		//send the surveyHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(surveyPhotoHelperPayload);
//		
//		//check current approval status and mobile flag of newCase
//		for(Case c : [SELECT Approval_Status__c, Mobile__c, Mobile_Version__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals(3.8, c.Mobile_Version__c);
//			//system.assertEquals('Pre-Pending', c.Approval_Status__c);
//			//system.assertEquals(true, c.Mobile__c);
//			//now set the approval status back to not started
//			c.Approval_Status__c = 'Not Started';
//			update c;
//		}
//		
//		//assert that the approval status remains not started after update
//		for(Case c : [SELECT Approval_Status__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals('Not Started', c.Approval_Status__c);
//		}
//		
//		system.Test.stopTest();
//	}
//	
//	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - test that Pre-Pending orders can be set back to Not Started/Rejected
//   	//for mobile 3.9
//	public static testmethod void pruvanStatusLogicTestThreeNinePrePending(){
//		system.Test.startTest();
//		
//		generateTestCase();
//		generateThreeNinePayloads();
//		
//		/*/send the surveyHelperPayload thru PruvanWebServices class to the PruvanSurveyHelper class
//		PruvanWebServices.doSurveyValidate(surveyHelperPayload);
//		//assert that the mobile version field for newCase has been set as 3.9 for this test
//		for(Case c : [SELECT Mobile_Version__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals(3.9, c.Mobile_Version__c);
//		}*/
//		//send the photoHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(photoHelperPayload);
//		//send the surveyHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(surveyPhotoHelperPayload);
//		//send the workOrderStatusPayload thru PruvanWebServices to the PruvanOrderStatusHelper class
//		PruvanWebServices.doWorkOrderStatus(workOrderStatusPayload);
//		Case[] casestoupdate = new Case[]{};
//		//check current approval status and mobile flag of new case
//		//for(Case c : [SELECT Approval_Status__c, Mobile__c, Mobile_Version__c FROM Case WHERE Id =: newCase.Id]){
//		//	system.assertEquals(3.9, c.Mobile_Version__c);
//		//	system.assertEquals('Pre-Pending', c.Approval_Status__c);
//		//	system.assertEquals(true, c.Mobile__c);
//			//now set the approval status back to not started
//		//	c.Approval_Status__c = 'Not Started';
//		//	casestoupdate.add(c);
//		//}
//		Database.SaveResult[] srList = Database.update(casestoupdate,false);
//		for (Database.SaveResult sr : srList) {
//		    if (sr.isSuccess()) {
//		        // Operation was successful, so get the ID of the record that was processed
//		        System.debug('Successfully inserted account. Account ID: ' + sr.getId());
//		    }
//		    else {
//		        // Operation failed, so get all errors                
//		        for(Database.Error err : sr.getErrors()) {
//		            System.debug('The following error has occurred.');                    
//		            System.debug(err.getStatusCode() + ': ' + err.getMessage());
//		            System.debug('Account fields that affected this error: ' + err.getFields());
//		        }
//		    }
//		}
//		//assert that the approval status remains not started after update
//		//for(Case c : [SELECT Approval_Status__c FROM Case WHERE Id =: newCase.Id]){
//		//	system.assertEquals('Not Started', c.Approval_Status__c);
//		//}
//		
//		system.Test.stopTest();
//	}
//	
//	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - orders that do not have a work completed of 'Yes' or work completed of 'Trip Charge' with a trip charge reason
//   	//should not get set to Pre-Pending/Pending status
//   	//for mobile 3.9
//	public static testmethod void pruvanStatusLogicTest_WorkCompleted(){
//		system.Test.startTest();
//		//pruvan 3.8 should no longer be in use, if however a vendor is still using this version then the validation rule should not affect the orders
//		//since all orders updated with 3.8 or earlier would have their Work_Completed__c fields set to a value other than 'No' because of the way
//		//the PruvanSurveyHelper class is structured logically, however, version 3.9 and above rely on a work order status payload and before this payload
//		//is received an order could be updated to reflect that work has not be completed
//		/*generateTestCase();
//		generateNoWorkCompletedThreeEightPayloads();
//		
//		//send the photoHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(photoHelperPayload);
//		//send the surveyHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(surveyPhotoHelperPayload);
//
//		//the PruvanSurveyHelper class has logic for setting the Work_Completed__c to 'Yes'
//		//any orders updated with pruvan 3.8 or less should have a valid Work_Completed__c value
//		//this is because orders updated by 3.8 or less or not relian on a work order status payload from Pruvan
//		for(Case c : [SELECT Approval_Status__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals('Pre-Pending', c.Approval_Status__c);
//			delete c;
//		}*/
//		
//		generateTestCase();
//		generateNoWorkCompletedThreeNinePayloads();
//		
//		//send the photoHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(photoHelperPayload);
//		//send the surveyHelperPayload thru PruvanWebServices to the PruvanPhotoHelper class
//		PruvanWebServices.doUploadPictures(surveyPhotoHelperPayload);
//		
//		//assert that the approval status remains not started after update
//		for(Case c : [SELECT Work_Completed__c, Date_Serviced__c FROM Case WHERE Id =: newCase.Id]){
//			c.Work_Completed__c = 'No';
//			c.Date_Serviced__c = null;
//			update c;
//		}
//		
//		//send the workOrderStatusPayload thru PruvanWebServices to the PruvanOrderStatusHelper class
//		PruvanWebServices.doWorkOrderStatus(workOrderStatusPayload);
//		
//		//assert that the approval status remains not started after update
//		for(Case c : [SELECT Approval_Status__c FROM Case WHERE Id =: newCase.Id]){
//			system.assertEquals('Not Started', c.Approval_Status__c);
//		}
//		
//		system.Test.stopTest();
//	}
}