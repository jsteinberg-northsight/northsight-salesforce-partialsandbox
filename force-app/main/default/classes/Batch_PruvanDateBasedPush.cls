global class Batch_PruvanDateBasedPush implements Database.Batchable<sObject>, Database.AllowsCallouts {
	    global Database.Querylocator start(Database.BatchableContext BC) {
        
        //return query result of VWMSurvey records
        return Database.getQueryLocator([SELECT Id FROM Case where Pruvan_Verify__c = true]);
    }
     global void execute(Database.BatchableContext BC, List<Case> ordersToPush) {
     	Set<Id> pushSet = new Set<Id>();
     	for(Case c:ordersToPush){
     		pushSet.add(c.Id);
     	}
     	PruvanPushController.directPushWorkOrdersToPruvan(pushSet);
     	
     }
         //Finish method
    global void finish(Database.BatchableContext BC) {
    	
    }	

}