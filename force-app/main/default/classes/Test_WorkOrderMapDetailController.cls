@isTest(seeAllData=false)
private class Test_WorkOrderMapDetailController {
///**
// *  Purpose         :   This is used for testing and covering WorkOrderMapDetailController.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   05/06/2014
// *
// *  Current Version :   V1.1
// *
// *  Revision Log    :   V1.0 - Created
// *                      
// *  Coverage        :   100%
// **/
//    
//    private static List<Account> accounts;
//    private static List<Contact> contacts;
//    private static Profile newProfile;
//    private static List<User> users;
//    private static Group queues;
//    
//    private static void generateTestData(integer countData){
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP 
//        //create a new user with the Customer Portal Manager Standard profile
//        newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Vendors'];
//        
//        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
//            //create a new user with the profile
//            queues = new Group(Name='Queue',Type='Queue');
//            insert queues;
//            
//            //Queue sObject   
//            QueueSobject queueObj = new QueueSobject(QueueId = queues.Id, SobjectType = 'Case');
//            insert queueObj;
//        }
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            /******** PRE-TEST SETUP create a new account *********/
//            accounts.add(new Account(Name = 'Tester Account' + i,RecordTypeId='0124000000011Q2'));
//        }
//        
//        insert accounts;
//        
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            //--------------------------------------------------------
//            //PRE-TEST SETUP
//            //create a new contact
//            Contact con = new Contact(LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest', Status__c = '3 - Active', RecordTypeId = '01240000000UPSr');
//            //Caps - Number of orders that can be assigned at once (open + all completed this week)
//            con.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//            con.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//            con.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//            con.Shrub_Trim__c = 3;//Pricing...required 
//            con.REO_Grass_Cut__c = 3;//Pricing
//            con.Pay_Terms__c = 'Net 30';//Terms required
//            con.Trip_Charge__c = 25;//Pricing
//            con.Date_Active__c = Date.Today();//Date Active must be set.
//            con.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//            con.MailingStreet = '30 Turnberry Dr';
//            con.MailingCity = 'La Place';
//            con.MailingState = 'LA';
//            con.MailingPostalCode = '70068';
//            con.OtherStreet = '30 Turnberry Dr';
//            con.OtherCity = 'La Place';
//            con.OtherState = 'LA';
//            con.OtherPostalCode = '70068';
//            con.Other_Address_Geocode__Latitude__s = 30.00;
//            con.Other_Address_Geocode__Longitude__s = -90.00;
//            contacts.add(con);
//        }   
//        
//        insert contacts;
//        
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            //create a new user with the profile
//            users.add(new User(alias = 'standt'+i, contactId = contacts[i-1].Id, email = Math.random()+Math.random()+Math.random()+i+'@northsighttestorg.com', emailencodingkey = 'UTF-8',
//                                    lastname = 'Testing'+i, languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//                                    timezonesidkey = 'America/Los_Angeles', username = Math.random()+Math.random()+Math.random()+i+'@northsighttestorg.com', isActive = true));
//        }
//        
//        insert users;
//    }
//    
//    static testMethod void myUnitTest() {
//        
//        //initialize variables
//        accounts = new List<Account>();
//        contacts = new List<Contact>();
//        users = new List<User>();
//        
//        generateTestData(1);
//        
//        
//        //Query record of Record Type of Account and Case objects
//        /*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: AssignmentMapConstants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName =: AssignmentMapConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: AssignmentMapConstants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: AssignmentMapConstants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: AssignmentMapConstants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client') AND (SobjectType =: AssignmentMapConstants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: AssignmentMapConstants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(2, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsOnOrders = new List<Account>();
//        accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsOnOrders;
//        
//        List<Geocode_Cache__c> properties = new List<Geocode_Cache__c>();
//        properties.add(new Geocode_Cache__c(Active__c = true, Location__Latitude__s = 30.09567, Location__Longitude__s = -23.4532));
//        properties.add(new Geocode_Cache__c(Active__c = true, Location__Latitude__s = 12.09567, Location__Longitude__s = -45.4532));
//        properties.add(new Geocode_Cache__c(Active__c = true, Location__Latitude__s = 50.09567, Location__Longitude__s = -10.4532));
//        
//        //insert Property here
//        insert properties;
//        
//        //List to hold Case records
//        List<Case> workOrders = new List<Case>();
//        workOrders.add(new Case(Geocode_Cache__c = properties[0].Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', State__c = 'TX',
//                                    Scheduled_Date_Override__c = Date.today(), Work_Ordered__c = 'Grass Recut', ownerId = users[0].Id));
//        insert workOrders;
//        
//        //Test starts here
//        Test.startTest();
//        
//        //query records of Case
//        workOrders = [SELECT Id, OwnerId FROM Case WHERE Id IN: workOrders];
//        
//        Apexpages.currentPage().getParameters().put('id', workOrders[0].Id);
//        ApexPages.StandardController sc = new ApexPages.standardController(workOrders[0]);
//        WorkOrderMapDetailController controller = new WorkOrderMapDetailController(sc);
//        
//        //query records of User sobject
//        users = [SELECT Id, ContactId FROM User WHERE Id =: users[0].Id];
//        
//        //assert statements here
//        System.assertEquals(users[0].ContactId, controller.vendorContactId);
//        
//        //Test stops here
//        Test.stopTest();
//    }
}