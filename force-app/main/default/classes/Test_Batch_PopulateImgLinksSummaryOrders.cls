@isTest(seeAllData=false)
private class Test_Batch_PopulateImgLinksSummaryOrders {
///**
// *  Purpose         :   This is used for testing and covering Batch_PopulateImageLinksSummaryOnCases functionality.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   02/04/2014
// *
// *	Current Version	:	V1.1
// *
// *	Revision Log	:	V1.0 - Created
// *						V1.1 - Modified - Padmesh Soni(06/10/2014) - Summarize image links for any work orders that have not been summarized.
// *
// *	Coverage		:	95%(V1.0)
// *						100%(V1.1)	
// **/
//
//	//Test method to test the functionality of Batch job
//    static testMethod void testPopulateImageLinksSummaryOnCases() {
//    	Test.setMock(HttpCalloutMock.class, new GenericHttpMock());
//    	//Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client') AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(2, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accounts;
//		
//		Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//		insert property;
//		
//		//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//		//Fetch record type of Account Sobject
//		List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client' 
//													AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//		
//    	//Code added - Padmesh Soni (2/4/2015) - SM-272: Update reassignment fee trigger to include the Client_Name__c field
//    	//Create a test instance for Account sobject
//		Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//			
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN,
//    								Client_Name__c = account.Id));
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1), Status = Constants.CASE_STATUS_OPEN,
//    								Client_Name__c = account.Id));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//    	//List to hold image Link records
//    	List<ImageLinks__c> imageLinks = new List<ImageLinks__c>();
//    	
//    	for(integer i=0; i < 10; i++) {
//	    	imageLinks.add(new ImageLinks__c(Pruvan_evidenceType__c= 'Survey', GPS_Timestamp__c= DateTime.now(), 
//	    										ImageUrl__c='https://s3.amazonaws.com/NorthsightTmp/02570223/52820869.jpg', CaseId__c= workOrders[0].Id, 
//	    										GPS_Location__Latitude__s = 30.087567, GPS_Location__Longitude__s=-90.485625, Pruvan_gpsAccuracy__c=2531, 
//	    										Survey_Question_Id__c='Trip_Charge_Reason__c', Survey_Id__c='GC23_001-v5', Notes__c ='This is a picture of the lawn.'));
//	    	imageLinks.add(new ImageLinks__c(Pruvan_evidenceType__c= 'Survey', GPS_Timestamp__c= DateTime.now(), 
//	    										ImageUrl__c='https://s3.amazonaws.com/NorthsightTmp/02570223/52820869.jpg', CaseId__c= workOrders[1].Id, 
//	    										GPS_Location__Latitude__s = 30.087567, GPS_Location__Longitude__s=-90.485625, Pruvan_gpsAccuracy__c=2531, 
//	    										Survey_Question_Id__c='Trip_Charge_Reason__c', Survey_Id__c='GC23_001-v5', Notes__c ='This is a picture of the lawn.'));
//	    }
//    	
//    	//insert Image Links here
//    	insert imageLinks;
//    	
//    	//Test starts here
//    	Test.startTest();
//    	
//    	//Batch instance
//        Batch_PopulateImageLinksSummaryOnCases batchJob = new Batch_PopulateImageLinksSummaryOnCases();
//        
//        //Batch executes here
//        Database.executeBatch(batchJob, 200);
//        
//    	//Test stops here
//    	Test.stopTest();
//    	
//    	//Query records of Cases
//    	workOrders = [SELECT ImageLinks_Summary__c FROM Case WHERE Id IN: workOrders];
//    	
//    	//Loop through work Order queried records
//    	for(Case workOrder : workOrders) {
//    		
//    		//assert statements here
//    		//System.assert(workOrder.ImageLinks_Summary__c != null);
//    	}
//    	
//    	//Code added - Padmesh Soni(06/10/2014) - Summarize image links for any work orders that have not been summarized.
//    	//Loop through query result of ImageLink records
//    	for(ImageLinks__c imageLink : [SELECT Id, Is_Summarized__c FROM ImageLinks__c WHERE Id IN: imageLinks]) {
//    		
//    		//assert statements here
//    		//System.assertEquals(true, imageLink.Is_Summarized__c);
//    	}
//    }
}