public with sharing class GeoUtilities { 
     
     //20170313 simple wrapper class to return primitive values in parameters
     public class geoUtilOutParam{
     	public string stringValue;
     }
     
     
     // Official map of address abbreviations
     public static map<string, string> address_abbreviations = new map<string, string> {'alley'=>'aly','annex'=>'anx','apartment'=>'apt','arcade'=>'arc','avenue'=>'ave', 'basement'=>'bsmt', 'bayou'=>'byu', 'beach'=>'bch', 'bend'=>'bnd', 'bluff'=>'blf', 'bottom'=>'btm', 'boulevard'=>'blvd', 'branch'=>'br', 'bridge'=>'brg', 'brook'=>'brk', 'building'=>'bldg', 'burg'=>'bg', 'bypass'=>'byp', 'camp'=>'cp', 'canyon'=>'cyn', 'cape'=>'cpe', 'causeway'=>'cswy', 'center'=>'ctr', 'circle'=>'cir', 'cliff'=>'clfs', 'cliffs'=>'clfs', 'club'=>'clb', 'corner'=>'cor', 'corners'=>'cors', 'course'=>'crse', 'court'=>'ct', 'courts'=>'cts', 'cove'=>'cv', 'creek'=>'crk', 'crescent'=>'cres', 'crossing'=>'xing', 'dale'=>'dl', 'dam'=>'dm', 'department'=>'dept', 'divide'=>'dv', 'drive'=>'dr', 'estate'=>'est', 'expressway'=>'expy', 'extension'=>'ext', 'falls'=>'fls', 'ferry'=>'fry', 'field'=>'fld', 'fields'=>'flds', 'flat'=>'flt', 'floor'=>'fl', 'ford'=>'frd', 'forest'=>'frst', 'forge'=>'frg', 'fork'=>'frk', 'forks'=>'frks', 'fort'=>'ft', 'freeway'=>'fwy', 'front'=>'frnt', 'garden'=>'gdns', 'gardens'=>'gdns', 'gateway'=>'gtwy', 'glen'=>'gln', 'green'=>'grn', 'grove'=>'grv', 'hanger'=>'hngr', 'harbor'=>'hbr', 'haven'=>'hvn', 'heights'=>'hts', 'highway'=>'hwy', 'hill'=>'hl', 'hills'=>'hls', 'hollow'=>'holw', 'inlet'=>'inlt', 'island'=>'is', 'islands'=>'iss', 'junction'=>'jct', 'key'=>'ky', 'knoll'=>'knls', 'knolls'=>'knls', 'lake'=>'lk', 'lakes'=>'lks', 'landing'=>'lndg', 'lane'=>'ln', 'light'=>'lgt', 'loaf'=>'lf', 'lobby'=>'lbby', 'lock'=>'lcks', 'locks'=>'lcks', 'lodge'=>'ldg', 'lower'=>'lowr', 'manor'=>'mnr', 'meadow'=>'mdws', 'meadows'=>'mdws', 'mill'=>'ml', 'mills'=>'mls', 'mission'=>'msn', 'mount'=>'mt', 'mountain'=>'mtn', 'neck'=>'nck', 'office'=>'ofc', 'orchard'=>'orch', 'parkway'=>'pkwy', 'penthouse'=>'ph', 'pine'=>'pnes', 'pines'=>'pnes', 'place'=>'pl', 'plain'=>'pln', 'plains'=>'plns', 'plaza'=>'plz', 'point'=>'pt', 'port'=>'prt', 'prairie'=>'pr', 'radial'=>'radl', 'ranch'=>'rnch', 'rapid'=>'rpds', 'rapids'=>'rpds', 'rest'=>'rst', 'ridge'=>'rdg', 'river'=>'riv', 'road'=>'rd', 'room'=>'rm', 'shoal'=>'shl', 'shoals'=>'shls', 'shore'=>'shr', 'shores'=>'shrs', 'space'=>'spc', 'spring'=>'spg', 'springs'=>'spgs', 'square'=>'sq', 'station'=>'sta', 'stravenue'=>'stra', 'stream'=>'strm', 'street'=>'st', 'suite'=>'ste', 'summit'=>'smt', 'terrace'=>'ter', 'trace'=>'trce', 'track'=>'trak', 'trafficway'=>'trfy', 'trail'=>'trl', 'trailer'=>'trlr', 'tunnel'=>'tunl', 'turnpike'=>'tpke', 'union'=>'un', 'upper'=>'uppr', 'valley'=>'vly', 'viaduct'=>'via', 'view'=>'vw', 'village'=>'vlg', 'ville'=>'vl', 'vista'=>'vis', 'way'=>'way', 'well'=>'wls', 'wells'=>'wls'};
	 private static string partBefore(string text, string stopWord){
	 	integer stop = text.indexOf(stopWord);
     	if(stop>0){
     		text = text.substring(0,stop);
     	}
     	
     	return text;
	 }
     public static string preformatAddress(string address){
     	if(address == null) return null;
     	// We want an all lower case string with no full words like "street", no spaces, no separators
     	string addr = address.toLowerCase();
     	addr = addr.remove('\\');
     	addr = partBefore(addr,'lbox');
     	addr = partBefore(addr,'lockbox');
     	addr = partBefore(addr,'lock box');
     	addr = partBefore(addr,'auto secure');
     	addr = partBefore(addr,'autosecure');
    	
    	//Code added - Padmesh Soni (09/11/2013) - SM-204: Why did one of these properties geocode and other not?
    	//If 'Lb Code' found into street address then it also removed before Geo-coding the address form Google
    	addr = partBefore(addr,'lb code');
    	
     	// Transform segments to abbreviations
    	return addr;
     }
     public static string preformatZip(string zip){
		if(zip == null){
     		return zip;
     	}
     	// 70460 - 1234 
     	zip=zip.deleteWhitespace();
     	//70460-1234
     	String[] parts = zip.split('-');
     	string newZip = '';
     	if(parts.size()>1){
     		newZip = formatZip5(parts[0]);
     		string zip4 = formatZip4(parts[1]);
     		if(zip4!=null){
     			newZip+='-'+zip4;
     		}
     	}else{
     		newZip = formatZip5(zip);
     	}
     	return newZip;
     }
     private static string formatZip5(string zip){
     	if(zip == null){
     		return '';
     	}
     	while(zip.length()<5){
     		zip = '0'+zip;
     	}
     	zip = zip.substring(0,5);
     	return zip;
     }
     private static string formatZip4(string zip){
     	if(zip == null){
     		return '';
     	}
     	while(zip.length()<4){
     		zip = '0'+zip;
     	}
     	zip = zip.substring(0,4);
     	if(zip.isNumeric()&&integer.valueOf(zip)==0){
     		return null;
     	}else{
     		return zip;
     	}
     }
     public static string capitalizeAll(string address){
     	if(address == null) return null;
     	List<string> addressParts = address.split(' ');
     	string formattedAddress = '';
     	for(string part:addressParts){
     		if(formattedAddress.length()>0){
     			formattedAddress+=' ';
     		}
     		formattedAddress+=part.trim().capitalize();
     	}
     	return formattedAddress;
     }
     public static string formatAddress(string street,string city, string state, string zip){
     	
     	string address  =  preformatAddress(street) + ', '+city+', '+state+' '+preformatZip(zip);
     	address = address.tolowercase();
     	string formattedAddress = capitalizeAll(address);
     	return formattedAddress;
     }
     public static string createAddressHash(string address) {
     	
     	// take out spaces and , .
    	string address_hash = address.toLowerCase();
    	set<string> abbrs = address_abbreviations.keySet();
     	for(string seg : abbrs) {
	    			if(address_hash.contains(seg)) {
	    				address_hash = address_hash.replace(seg, address_abbreviations.get(seg));
	    			}
    	}
    	address_hash = address_hash.replace(',', '');
     	address_hash = address_hash.replace('.', '');
    	address_hash = address_hash.replace(' ', '');

    	return address_hash;
     }
    
    public static string createClientLoanHash(string loanNumber, string clientCode, string unit, string zipCode) {     	
     	string clientCorrected = '';
    	string formattedHash = '';
    	if (clientCode == 'AMR' || clientCode == 'HHP' || clientCode == 'HHR')
    		clientCorrected = 'AMR-HHP-HHR';
    	else
    		clientCorrected = clientCode;
    		
        if (unit == null || unit == '')
        	formattedHash = clientCorrected + '-' + loanNumber + '-' + zipCode;
        else
        	formattedHash = formattedHash = clientCorrected + '-' + loanNumber + '-' + unit + '-' + zipCode;

    	return formattedHash;
     }
     
     
      public static map<string, string> getGeoData (string address){
      	return getGeoData(address, null);
      }
     
     // TODO: handle over rate limit error from google if we start hitting it too rapidly
     public static map<string, string> getGeoData(string address, geoUtilOutParam out) {
     	// response body 
     	string responseBody;
     	
     	// Values to extract from the JSON response
     	set<string> address_types = new set<string> {};
     	string geocode_status;
     	//string formatted_address;
     	string latitude = '0';
     	string longitude = '0';
     	
     	//Code added - Padmesh Soni (12/11/2014) - CCP-15: Fill County on Property
     	//Variable to hold County value from GeoCode callout
     	String administrative_area_level_2;
     	
     	// Cache Data to return to caller
     	//geocode_cache__c currentGeoData;
 		map <string, string> returnData = new map<string, string> {};
 		
		if(address==null || address.length() == 0){
			//system.debug(Logginglevel.ERROR, 'GeoUtilities getCoordinates no address provided. Return null');   
			return null;
		}
		 
		String url = 'http://northsight-maprelay-env.elasticbeanstalk.com/?';//production
		//String url = 'http://nsmaprelay-env.elasticbeanstalk.com/?';//sandbox
		url += 'address=' + EncodingUtil.urlEncode(address, 'UTF-8');
		url += '&sensor=false'; 
		 
		//system.debug('!!!!!!!URL STRING:' + url);
		 
		responseBody = doCallout(url);
		
		//20170313 - Tyler H.  passing back raw json
		if (out!=null) out.stringValue=responseBody;
		
		
		//system.debug('!!!!! HTTP RESPONSE BODY: ' + responseBody);
		
		JSONParser parser = JSON.createParser(responseBody);
		while (parser.nextToken() != null) {
			
			//system.debug('current parser position : ' + parser.getCurrentToken() +'::' + parser.getText());
			
			// Skip over unneeded nodes
			if(parser.getCurrentToken() == JSONToken.FIELD_NAME && 
					(parser.getText() == 'address_components' || parser.getText() == 'location_type' || parser.getText() == 'viewport')) {
				//system.debug('Should be consuming ' + parser.getText());
				
				//if(parser.getText() == 'address_components')
					//System.debug('consumeObject(parser) :::::'+ consumeObject(parser));
								
				parser.nextToken();
				parser = consumeObject(parser);
			}	
				
			 if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'lat')) {
                
                // Get the value. 
                parser.nextToken(); 
                latitude = parser.getText();
                
            // get longitude    
            } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'lng')) {
            	
            	// Get the value. 
                parser.nextToken();
                longitude = parser.getText();
    		
    		// Get the types list
            } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'types')) {
            	
            	// Get the value. 
                parser.nextToken();
                if(parser.getCurrentToken() == JSONToken.START_ARRAY) {
                	parser.nextToken();
                	while(parser.getCurrentToken() != JSONToken.END_ARRAY) {
	                	address_types.add(parser.getText());
	                	parser.nextToken();
	                }
                }
    
    		// Get the returned status 
            } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'status')) {
            	
            	// Get the value. 
                parser.nextToken();
                geocode_status = parser.getText();
    
            }
        }
        
        //Code added - Padmesh Soni (12/11/2014) - CCP-15: Fill County on Property
        //Map to hold JSON string parsed into type of Map
        Map<String,Object> respMap = (MAP<String,Object>)JSON.deserializeUntyped(responseBody);
        
        //List to hold first node of JSON string of results
		List<Object> resultList =  (List<Object>)respMap.get('results');
		
		//Loop through resultsList
		for (Object innerObj : resultList) {
			
			//Typecast object into Map of String with respect to Object
		    Map<String, Object> mainMap = (Map<String, Object>) innerObj;
		    
			if(mainMap.containsKey('address_components')) {
		    	
		    	//Loop through List of object
		    	for(Object mainObj : (List<Object>)mainMap.get('address_components')) {
					
					//Typecast into Map
					Map<String, Object> pInner = (Map<String, Object>) mainObj;
					
					//Checking for types
					if(pInner.containsKey('types') ){
						
						//getting List of types node
						List<Object> typeVal = (List<Object>) pInner.get('types');
						
						//Loop through 
						for(Object cVal : typeVal) {
							
							//Type cast into string from Object
							String cValStr = String.valueOf(cVal);
							
							//Check for specific node value
							if(cValStr == 'administrative_area_level_2' && pInner.containsKey('long_name')) {
								administrative_area_level_2 = (String)pInner.get('long_name');
							}
						}
					}
				}
			}
		}
		
        //system.debug('!!!!!!!! GEOCODE STATUS ' + geocode_status );
              
        // Analyze and cache our results - create geocode cache object
    	
    	string is_partial;
    	if(geocode_status=='OK') {
	    	if(address_types.contains('street_address') || address_types.contains('premise') || address_types.contains('subpremise')) {
	    		is_partial = '0';
	    		//system.debug(address_types);
	    	} else {
	    		// We need a non-standard status to indicate that the address geocoded OK but is not precise enough for our purposes
	    		is_partial = '1';
	    		geocode_status = 'NOT_PRECISE';
	    	}
    	} else if(geocode_status == 'OVER_QUERY_LIMIT') {
    		is_partial = '0'; // Over query limit is not a partial geocode, it just needs to be re-tried. We won't quarantine these
    	} else {
    		is_partial = '1';
    	}
    	
    	// Build returndata map
    	returnData.put('partial', is_partial);
    	returnData.put('status', geocode_status);
    	//if(geocode_status == 'OVER_QUERY_LIMIT') {
    	returnData.put('formatted', address); 
    	//} else {
    	//	returnData.put('formatted', formatted_address);
    	//}
    	returnData.put('lat', latitude);
    	returnData.put('lng', longitude);
		//add the url used to geocode this property to returnData
		returnData.put('url', url);
		
		//Code added - Padmesh Soni (12/11/2014) - CCP-15: Fill County on Property
		if(String.isNotBlank(administrative_area_level_2))
			returnData.put('administrative_area_level_2', administrative_area_level_2);
		
    	//system.debug('!!!!! geocode returnData : ' + returnData);
    	
		return returnData;
	}
	
	private static string doCallout(string url) {
		Http h = new Http();
		HttpRequest req = new HttpRequest();
		 
		//req.setHeader('Content-type', 'application/x-www-form-urlencoded');
		//req.setHeader('Content-length', '0');
		req.setEndpoint(url);
		req.setMethod('GET');
		req.setTimeout(120000);
		if (!Test.isRunningTest()){
			// Methods defined as TestMethod do not support Web service callouts
			HttpResponse res = h.send(req);
			return  res.getBody();
		} else {
		     // dummy data - standard example from google docs
		     // Send back differing responses based on input addresses to test various scenarios within the rest of this class and the batch geocoder class
		     if(url.toLowerCase()=='http://northsight-maprelay-env.elasticbeanstalk.com/?address=515+partial+st%2c+bridgeport%2c+ct+60562&sensor=false') {
		     	//system.debug('Callout Test returned partial result');
		     	return '{ "results" : [{ "address_components" : [{ "long_name" : "1600", "short_name" : "1600", "types" : [ "street_number" ]},{ "long_name" : "Amphitheatre Pkwy", "short_name" : "Amphitheatre Pkwy", "types" : [ "route" ]},{ "long_name" : "Mountain View", "short_name" : "Mountain View", "types" : [ "locality", "political" ]},{ "long_name" : "Santa Clara", "short_name" : "Santa Clara", "types" : [ "administrative_area_level_2", "political" ]},{ "long_name" : "California", "short_name" : "CA", "types" : [ "administrative_area_level_1", "political" ]},{ "long_name" : "United States", "short_name" : "US", "types" : [ "country", "political" ]},{ "long_name" : "94043", "short_name" : "94043", "types" : [ "postal_code" ]} ], "formatted_address" : "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA", "geometry" : {"location" : { "lat" : 11.11111111, "lng" : -111.22222222},"location_type" : "ROOFTOP","viewport" : { "northeast" : {"lat" : 1,"lng" : -1 }, "southwest" : {"lat" : 1,"lng" : -1 }} }, "types" : [ "somethingelse" ]} ], "status" : "NOT_PRECISE"}';
		     } else if(url.toLowerCase()=='http://northsight-maprelay-env.elasticbeanstalk.com/?address=400+Cedar+St%2C+Bridgeport%2C+Ct+60562&sensor=false'){
		    	//system.debug('Callout Test returned normal result');
		    	return '{"results":[{"address_components":[{"long_name":"400","short_name":"400","types":["street_number"]},{"long_name":"Cedar St","short_name":"Cedar St","types":["route"]},{"long_name":"Bridgeport","short_name":"Bridgeport","types":["locality","political"]},{"long_name":"Conneticut","short_name":"CT","types":["administrative_area_level_1","political"]},{"long_name":"United States","short_name":"US","types":["country","political"]},{"long_name":"60562","short_name":"60562","types":["postal_code"]}],"formatted_address":"400 Cedar St, Bridgeport, CT 60562, USA","geometry":{"location":{"lat":37.4229181,"lng":-122.0854212},"location_type":"ROOFTOP","viewport":{"northeast":{"lat":37.42426708029149,"lng":-122.0840722197085},"southwest":{"lat":37.4215691197085,"lng":-122.0867701802915}}},"types":["street_address"]}],"status":"OK"}';
		     } else {
		     	return '{"results":[{"address_components":[{"long_name":"1234","short_name":"1234","types":["street_number"]},{"long_name":"Street","short_name":"Street","types":["route"]},{"long_name":"Austin","short_name":"Austin","types":["locality","political"]},{"long_name":"Texas","short_name":"TX","types":["administrative_area_level_1","political"]},{"long_name":"United States","short_name":"US","types":["country","political"]},{"long_name":"78704","short_name":"78704","types":["postal_code"]}],"formatted_address":"1234 Street,Austin, TX 78704, USA","geometry":{"location":{"lat":37.4229181,"lng":-122.0854212},"location_type":"ROOFTOP","viewport":{"northeast":{"lat":37.42426708029149,"lng":-122.0840722197085},"southwest":{"lat":37.4215691197085,"lng":-122.0867701802915}}},"types":["street_address"]}],"status":"OK"}';
		     }
		}
	}
	
	
	private static jsonParser consumeObject(JSONParser parser) {
	    Integer depth = 0;
	    do {
	        JSONToken curr = parser.getCurrentToken();
	        if (curr == JSONToken.START_OBJECT ||
	            curr == JSONToken.START_ARRAY) {
	            depth++;
	        } else if (curr == JSONToken.END_OBJECT ||
	            curr == JSONToken.END_ARRAY) {
	            depth--;
	        }
	    } 
	    while (depth > 0 && parser.nextToken() != null);
	    
	    return parser;
	}
	
	
	/*
		Tyler H - converting the JSON payload from google api to a simple key,value pair map	
	*/
	public static map<string, string> convertGoogleAPIjsonToMap(string googleAPIjson){
		map<string, string> ret = new map<string,string>();
        Map<String, Object> m =
		   (Map<String, Object>)
		      JSON.deserializeUntyped(googleAPIjson);
                
        	
        ret.put('status', string.valueof(m.get('status')));
        
        if (ret.get('status')!='OK') return ret;
        
        
        object[] results = (object[])m.get('results');
        Map<String, Object> result = (Map<String, Object>)results[0];
        
        object[] resultTypes = (object[])result.get('types');
        ret.put('result_type', string.valueof(resultTypes[0]));
                
        ret.put('formatted_address', string.valueof(result.get('formatted_address')));
        ret.put('place_id', string.valueof(result.get('place_id')));
        map<string, object> geometry = (map<string, object>)result.get('geometry');
        
        ret.put('location_type',string.valueof(geometry.get('location_type')));
        
        map<string, object> location = (map<string, object>)geometry.get('location');
        ret.put('lat',string.valueof(location.get('lat')));
        ret.put('lng', string.valueof(location.get('lng')));        
        
        
        Object[] addComps = (list<object>)result.get('address_components');
        for (object addComp : addComps ){
        	map<string, object> addCompMap = (map<string, object>)addComp;         	 
        	//system.debug(((map<string, object>)addComp).get('long_name'));
			object[] types=(object[])addCompMap.get('types'); 
        	string key = string.valueof(types[0]);
        	string value =string.valueof(addCompMap.get('long_name'));
        	ret.put(key, value);        	
        }
        return ret;
	}
	

	public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}