global without sharing class BatchDeleteTestOrders implements Database.Batchable<Case> {
	global Iterable<Case> start(Database.BatchableContext BC) {
		
		//Padmesh Soni (06/18/2014) - Support Case Record Type
	    //Query record of Record Type of Case object
		List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
		
		//List<Case> testOrders = [Select Id From Case Where Reference_Number__c >= '69696000' And Reference_Number__c <= '69696999'];
		List<Case> testOrders = [Select Id From Case Where RecordTypeId NOT IN: recordTypes AND State__c = 'LA'];
		
		return testOrders;//send the cases list to the execute method
	}
	
	global void execute(Database.BatchableContext BC, List<Case> ordersToDelete) {
		if(ordersToDelete.size() > 0){//if the list has cases
			delete ordersToDelete;//update the list
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		//section not used
	}
}