@isTest(SeeAllData=false)
public without sharing class EmergencyCommitTester {
//	private static Case newCase;
//		
//	private static void generateTestCase() {
//		//--------------------------------------------------------
//   		//PRE-TEST SETUP
//   		//create a new account
//		Account newAccount = new Account(
//			Name = 'Tester Account'
//		);
//		insert newAccount;
//	
//		//create a new contact
//		Contact newContact = new Contact(
//			LastName = 'Tester',
//			AccountId = newAccount.Id,
//			Pruvan_Username__c = 'NSBobTest',
//			MailingStreet = '30 Turnberry Dr',
//			MailingCity = 'La Place',
//			MailingState = 'LA',
//			MailingPostalCode = '70068',
//			OtherStreet = '30 Turnberry Dr',
//			OtherCity = 'La Place',
//			OtherState = 'LA',
//			OtherPostalCode = '70068',
//			Other_Address_Geocode__Latitude__s = 30.00,
//			Other_Address_Geocode__Longitude__s = -90.00
//		);
//		insert newContact;
//		
//		//create a new test case
//		newCase = new Case();
//		newCase.street_address__c = '1234 Street';
//        newCase.state__c = 'TX';
//        newCase.city__c = 'Austin';
//        newCase.zip_code__c = '78704';
//        newCase.Vendor_code__c = 'SRSRSR';
//        newCase.Work_Order_Type__c = 'Routine';
//        newCase.Client__c = 'SG';
//        newCase.Loan_Type__c = 'REO';
//        newCase.ContactId = newContact.Id;
//        newCase.Work_Ordered__c = 'Grass Recut';
//        newCase.Due_Date__c = Date.today().addDays(-3);
//        newCase.Client_Commit_Date__c = Date.today().addDays(-1);
//        
//        insert newCase;
//	}
//	
//	//--------------------------------------------------------
//   	//TESTS
//   	//Unit tests for method testing and regression testing. 
//   	//--------------------------------------------------------
//   		
//   	//--------------------------------------------------------
//   	//TEST:
//   	//Scenario - test the functionality of EmergencyCommitHelper is working correctly
//   	public static testmethod void testEmergencyCommitHelper(){
//   		system.Test.startTest();
//   		
//   		generateTestCase();
//   		
//   		//get back a list of the client commitment that should have been created for newCase
//   		List<Client_Commitment__c> ccList = [SELECT Commitment_Date__c, Current_Status_of_Order__c, Root_Cause_of_Delay__c, Public_Comments__c, Internal_Comments__c FROM Client_Commitment__c WHERE Work_Order__c = : newCase.Id];
//   		system.assertEquals(1, ccList.size());//assert the list has a size of 1
//   		
//   		//loop thru the list and assert the following fields have been propertly set
//   		for(Client_Commitment__c cc : ccList){
//   			system.assertEquals(Date.today().addDays(3), cc.Commitment_Date__c);
//   			system.assertEquals('Pending Field Complete', cc.Current_Status_of_Order__c);
//   			system.assertEquals('Delay in Receiving Work', cc.Root_Cause_of_Delay__c);
//   			system.assertEquals(null, cc.Public_Comments__c);
//   			system.assertEquals(null, cc.Internal_Comments__c);
//   		}
//   		
//   		system.Test.stopTest();
//   	}
}