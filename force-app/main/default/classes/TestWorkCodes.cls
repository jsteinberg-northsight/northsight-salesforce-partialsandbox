@isTest(seeAllData=false)
private class TestWorkCodes {
//
////Padmesh Soni (05/19/2014) - Work Codes System 
//    //New test method added for Work Code stamping on Work Order as per Mapping records
//    //Test method is used to test functionality of AssignmentMapController
//    static testMethod void testPopulateWorkCodeOnCase() {
//                //Test start here
//        Test.startTest();
//        //Query record of Record Type of Account and Case objects
//        List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: AssignmentMapConstants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client' OR DeveloperName = 'SUPPORT') 
//                                            AND (SobjectType =: AssignmentMapConstants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: AssignmentMapConstants.ACCOUNT_SOBJECTTYPE) 
//                                            AND IsActive = true ORDER BY DeveloperName];                              
//        /*RecordType typeReo = [SELECT Id, Name FROM RecordType WHERE ( DeveloperName = 'REO') 
//                                            AND (SobjectType =: AssignmentMapConstants.CASE_SOBJECTTYPE 
//                                            OR SobjectType =: AssignmentMapConstants.ACCOUNT_SOBJECTTYPE) 
//                                            AND IsActive = true ORDER BY DeveloperName];
//        System.assert(typeReo!=null);*/
//        //Assert statement
//        System.assertEquals(3, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsOnOrders = new List<Account>();
//        accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsOnOrders;
//        
//        //List to hold all test records of Work Codes
//        List<Work_Code__c> workCodes = new List<Work_Code__c>();
//        workCodes.add(new Work_Code__c(Name = 'GCREO01', Work_Code_Description__c = 'Grass Cut REO', Review_Needed__c =true, Work_Ordered__c = 'Grass Cut', 
//                                            Department__c='Maintenance', Work_Order_Record_Type__c = 'SUPPORT'));
//        workCodes.add(new Work_Code__c(Name = 'GCRREO', Work_Code_Description__c = 'Grass Cut - Rush REO', Review_Needed__c =true, Work_Ordered__c = 'Grass Cut',Work_Order_Record_Type__c='NEW ORDER')); 
//        workCodes.add(new Work_Code__c(Name = 'WPBR', Work_Code_Description__c = 'Work Per Bid - Rush', Review_Needed__c = true,Work_Order_Record_Type__c='NEW ORDER')); 
//        workCodes.add(new Work_Code__c(Name = 'MSPFU', Work_Code_Description__c = 'Maid Service - Photo Issued Follow Up', Review_Needed__c = true,Work_Order_Record_Type__C = 'MAID',work_ordered__c = 'Maid Refresh',Type__c = 'Maid',Department__c = 'Maintenance'));
//        
//        insert workCodes;
//        
//        //List to hold test records of Work Code Mapping records
//        List<Work_Code_Mapping__c> workCodeMapping = new List<Work_Code_Mapping__c>();
//        //0
//        workCodeMapping.add(new Work_Code_Mapping__c(
//        	City__c = 'Dallas',
//        	Client__c = 'SG',
//        	Client_Special_1__c = 'Test Client1', 
//            Client_Special_2__c = 'Test Client2',
//            Client_Special_3__c = 'Test Client3', 
//                                                        Customer__c = 'Test Customer', Mapping_Priority__c = 300, 
//                                                        State__c = 'CA', Vendor_Code__c = '123', Work_Code__c = workCodes[0].Id, 
//                                                        Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '12345'));
//        //1
//        workCodeMapping.add(new Work_Code_Mapping__c(City__c = 'Dallas', Client__c = 'SG', Client_Special_1__c = 'Test Client1', 
//                                                        Client_Special_2__c = 'Test Client2', Client_Special_3__c = 'Test Client3', 
//                                                        Customer__c = 'Test Customer', Loan_Type__c = 'REO', Mapping_Priority__c = 200, 
//                                                        State__c = 'CA', Vendor_Code__c = '123', Work_Code__c = workCodes[1].Id, 
//                                                        Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '12345'));
//        
//        //2
//        workCodeMapping.add(new Work_Code_Mapping__c(Customer__c = 'Test Customer', Loan_Type__c = 'REO', Mapping_Priority__c = 100, 
//                                                        State__c = 'AK', Vendor_Code__c = '123', Work_Code__c = workCodes[2].Id, 
//                                                        Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '123456'));
//        
//        //3
//        workCodeMapping.add(new Work_Code_Mapping__c(Customer__c = 'Test Customer', Loan_Type__c = 'REO', Mapping_Priority__c = 50, 
//                                                        Vendor_Code__c = '123', Work_Code__c = workCodes[1].Id, 
//                                                        Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '12345'));
//        //4
//        workCodeMapping.add(new Work_Code_Mapping__c(City__c = 'San Francisco', Client__c = 'NOT-SG', Client_Special_1__c = 'Test 1', 
//                                                        Client_Special_2__c = 'Test 2', Client_Special_3__c = 'Client 3', Customer__c = 'Test Cust 1', 
//                                                        Loan_Type__c = 'FIANM', Mapping_Priority__c = 25, State__c = 'CA', 
//                                                        Vendor_Code__c = '2313', Work_Code__c = workCodes[3].Id));
//        //5 
//        //TESTING LOAN TYPE                                               
//		workCodeMapping.add(new Work_Code_Mapping__c(Customer__c = 'Test Customer', Loan_Type__c = 'REO', Mapping_Priority__c = 999,
//                                                        State__c = 'FL', Vendor_Code__c = '123', Work_Code__c = workCodes[2].Id, 
//                                                        Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '1234563',Client_Name__c = accountsOnOrders[1].Id));
//        //6
//        //MAID WORK ORDER
//        /*
//        workCodeMapping.add(new Work_Code_Mapping__c(Customer__c = 'Test Customer', Loan_Type__c = 'REO', Mapping_Priority__c = 50, 
//                                                        State__c = 'AK', Vendor_Code__c = '123', Work_Code__c = workCodes[3].Id, 
//                                                        Work_Ordered_Text__c = 'Test MAID WO', Zip_Code__c = '12345'));
//                                                        */
//        //---------------------------------------------------------------------------
//        
//        
//        Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//        insert property;
//        
//        //List to hold Case records
//        List<Case> workOrders = new List<Case>();
//        //0
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                            Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                            Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Department__c = 'Test', 
//                            Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual', City__c = 'Dallas', 
//                            Client_Special_1__c = 'Test Client1', Client_Special_2__c = 'Test Client2', Client_Special_3__c = 'Test Client3', 
//                            Customer__c = 'Test Customer', Loan_Type__c = 'REO', State__c = 'CA', Vendor_Code__c = '123', 
//                            Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '12345'));
//        //1
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Department__c = 'Test', 
//                                    Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual', City__c = 'Dallas', 
//                                    Client_Special_1__c = 'Test Client1', Client_Special_2__c = 'Test Client2', Client_Special_3__c = 'Test Client3', 
//                                    Customer__c = 'Test Customer', Loan_Type__c = 'REO', State__c = 'CA', Vendor_Code__c = '123', 
//                                    Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '12345'));
//        //2
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Department__c = 'Test', 
//                                    Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual', City__c = 'Dallas', 
//                                    Client_Special_1__c = 'Test Client1', Client_Special_2__c = 'Test Client2', Client_Special_3__c = 'Test Client3', 
//                                    Customer__c = 'Test Customer', Loan_Type__c = 'REO', State__c = 'CA', Vendor_Code__c = '123', 
//                                    Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '12345'));
//        //3
//        /*
//                workCodeMapping.add(new Work_Code_Mapping__c(Customer__c = 'Test Customer', Loan_Type__c = 'REO', Mapping_Priority__c = 100, 
//                                                        State__c = 'AK', Vendor_Code__c = '123', Work_Code__c = workCodes[2].Id, 
//                                                        Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '123456'));
//                                                        */
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Department__c = 'Test', 
//                                    Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual', City__c = 'Dallas', 
//                                    Customer__c = 'Test Customer', State__c = 'AK', Vendor_Code__c = '123',Loan_Type__c = 'REO', 
//                                    Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '123456'));
//        //4
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN,
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine',
//                                    Customer__c = 'Test Customer', Loan_Type__c = 'REO', State__c = 'AK', Vendor_Code__c = '123', 
//                                    Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '123456'));
//        //5
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN,
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine',
//                                    Customer__c = 'Test Customer', Loan_Type__c = 'REO', State__c = 'AK', Vendor_Code__c = '123', 
//                                    Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '12345'));
//        //6
//        /*
//                workCodeMapping.add(new Work_Code_Mapping__c(Customer__c = 'Test Customer', Loan_Type__c = 'REO', Mapping_Priority__c = 100, 
//                                                        State__c = 'AK', Vendor_Code__c = '123', Work_Code__c = workCodes[2].Id, 
//                                                        Work_Ordered_Text__c = 'Test WO Data', Zip_Code__c = '123456'));
//                                                        */
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN,
//                                    Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine',
//                                    Customer__c = 'Test Customer', Loan_Type__c = 'REO', State__c = 'AK', Vendor_Code__c = '123', 
//                                    Work_Ordered_Text__c = 'Test WO Data',Zip_Code__c = '123456'));
//        //7
//                /*
//                workCodeMapping.add(new Work_Code_Mapping__c(City__c = 'San Francisco', Client__c = 'NOT-SG', Client_Special_1__c = 'Test 1', 
//                                                        Client_Special_2__c = 'Test 2', Client_Special_3__c = 'Client 3', Customer__c = 'Test Cust 1', 
//                                                        Loan_Type__c = 'FIANM', Mapping_Priority__c = 25, State__c = 'CA', 
//                                                        Vendor_Code__c = '2313', Work_Code__c = workCodes[3].Id));*/
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//                                    Loan_Type__c = 'FIANM',Vendor_Code__c = '2313',
//                                    Work_Ordered_Text__c = 'Test WO Data',Client__c = 'NOT-SG', Client_Special_1__c = 'Test 1', 
//                                                        Client_Special_2__c = 'Test 2', Client_Special_3__c = 'Client 3',Customer__c = 'Test Cust 1',
//                                                         Work_Order_Type__c = 'Maintenance', State__c = 'CA',City__c='San Francisco'));
//        //8
//        workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected', 
//                                    City__c = 'San Francisco', Client__c = 'NOT-SG', Client_Special_1__c = 'Test 1', 
//                                    Client_Special_2__c = 'Test 2', Client_Special_3__c = 'Client 3', Customer__c = 'Test Cust 1', 
//                                    Work_Ordered_Text__c = 'Test WO Data',Loan_Type__c = 'FIANM', State__c = 'CA', Vendor_Code__c = '2313',Zip_Code__c = '123456'));
//        //9
//       	//TEST MAID WO
//       	case maidWo = new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//	                            Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN, Approval_Status__c = 'Rejected',
//	                            Client__c = AssignmentMapConstants.CASE_CLIENT_SG, Work_Order_Type__c = 'Routine', Department__c = 'Test', 
//	                            Work_Ordered__c = 'Grass Recut', Assignment_Program__c = 'Mannual', City__c = 'Dallas', 
//	                            Client_Special_1__c = 'Test Client1', Client_Special_2__c = 'Test Client2', Client_Special_3__c = 'Test Client3', 
//	                            Customer__c = 'Test Customer', Loan_Type__c = 'REO', State__c = 'AK', Vendor_Code__c = '123', 
//	                            Work_Ordered_Text__c = 'Test Maid WO', Zip_Code__c = '12345');
//		workOrders.add(maidWo);
//        //TESTING LoanType.REO
//        /*
//        workCodeMapping.add(new Work_Code_Mapping__c(Customer__c = 'Test Customer', Loan_Type__c = 'REO', 
//                                                        State__c = 'FL', Vendor_Code__c = '123',Zip_Code__c = '1234563',
//                                                        Work_Ordered_Text__c = 'Test WO Data',Client_Name__c = accountsOnOrders[1].Id,
//                                                        Mapping_Priority__c = 0,Work_Code__c = workCodes[2].Id));
//                                                        */
//        //10
//        workOrders.add(						new Case(Customer__c = 'Test Customer',  Loan_Type__c = 'LoanType.REO',
//                                    					State__c = 'FL',Vendor_Code__c = '123',Zip_Code__c = '1234563',
//                                    					Work_Ordered_Text__c = 'Test WO Data',Client_Name__c = accountsOnOrders[1].Id,
//														Due_Date__c = Date.today(), Status = AssignmentMapConstants.CASE_STATUS_OPEN,
//                                    					RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id));
//
//        
//
//        insert workCodeMapping;
//        //}
//        //System.assert(OrderAssignmentHelper.workCodeMappings!=null);
//        //System.assert(OrderAssignmentHelper.workCodeMappings.size()>0);
//
//        //insert case records
//        insert workOrders;
//        
//        maidWo.Work_Code__c = workCodes[3].Id;
//        maidWo.Client_Name__c = accountsOnOrders[1].Id;
//        orderAssignmentHelper.initialized = false;
//        orderAssignmentHelper.orderTypes = null;
//        update maidWo;
//        System.assert(OrderAssignmentHelper.workCodeMappings!=null);
//        System.assert(OrderAssignmentHelper.workCodeMappings.size()>0);
//        
//        //Query result of Work Code Mapping
//        workCodeMapping = [SELECT Work_Code__c, Work_Code__r.Work_Ordered__c, Work_Code__r.Department__c, Work_Code__r.Review_Needed__c, State__c, Work_Code__r.Name
//        ,Work_Code__r.Work_Order_Record_Type__c 
//                                FROM Work_Code_Mapping__c WHERE Id IN: workCodeMapping order by Id];
//                                    
//        //Query result of Case records after inserting
//        workOrders = [SELECT Id, Work_Code__c, Work_Ordered__c,Work_Ordered_Text__c, Department__c, Rush__c, RecordTypeId, RecordType.Name, State__c,Work_Code__r.Name,Work_Code__r.Work_Order_Record_Type__c,
//                            (Select Id, Work_Code_Survey_Entry__r.Work_Code_Name__c from Work_Order_Surveys__r)
//                            FROM Case WHERE Id IN: workOrders ORDER BY CaseNumber];
//        integer i = 0;
//        for(Case c: workOrders){
//        	System.AssertNotEquals(Null,c.Work_Code__c,i+'::'+c.Work_Ordered_Text__c);
//        	System.AssertNotEquals(Null,c.Work_Code__r.Work_Order_Record_Type__c);
//        	System.AssertEquals(c.RecordType.Name,c.Work_Code__r.Work_Order_Record_Type__c,i+'::'+c.Work_Ordered_Text__c);
//        	if(c.Id == maidWo.Id){
//        	  	for(Work_Order_Survey__c wcs: c.Work_Order_Surveys__r){
//        	  		System.assertEquals(wcs.Work_Code_Survey_Entry__r.Work_Code_Name__c,c.Work_Code__c);
//        	  	}
//        	}
//        	i++;
//        }
//                            
//        //ASSERT Loan Type
//        System.assertEquals('MAID',workCodes[3].Work_Order_Record_Type__c);
//        System.assertEquals(workCodes[3].Name, workOrders[9].Work_Code__r.Name);
//        
//        //System.assertEquals(8,workOrders.size());
//        //Assert statements here
//        System.assertEquals(workCodeMapping[0].Work_Code__c, workOrders[0].Work_Code__c);
//        
//        System.assertEquals(workCodeMapping[0].Work_Code__c, workOrders[1].Work_Code__c);
//        //System.assertEquals(workCodeMapping[0].Work_Code__c, workOrders[2].Work_Code__c);
//        System.assertEquals(workCodeMapping[2].Work_Code__c, workOrders[3].Work_Code__c);
//        //System.assertEquals(workCodeMapping[3].Work_Code__c, workOrders[4].Work_Code__c);
//        //System.assertEquals(workCodeMapping[3].Work_Code__c, workOrders[5].Work_Code__c);
//        //System.assertEquals(workCodeMapping[5].Work_Code__c, workOrders[6].Work_Code__c);
//        //System.assertEquals(workCodeMapping[5].Work_Code__c, workOrders[7].Work_Code__c);
//        //System.assertEquals(null, workOrders[8].Work_Code__c);
//        
//        //Code added - Padmesh Soni (2/13/2015) - WC-6:Set additional fields using the Work Codes in the Case trigger.
//        //assert statements
//        System.assertEquals(workCodeMapping[0].Work_Code__r.Work_Ordered__c, workOrders[0].Work_Ordered__c);
//        System.assertEquals(recordTypes[2].Id, workOrders[0].RecordTypeId);
//        System.assertEquals(workCodeMapping[0].Work_Code__r.Department__c, workOrders[0].Department__c);
//        System.assertEquals(workCodeMapping[0].Work_Code__r.Review_Needed__c, workOrders[0].Rush__c);
//        System.assertEquals(workCodeMapping[2].Work_Code__r.Work_Ordered__c, workOrders[3].Work_Ordered__c);
//        System.assertEquals(workCodeMapping[2].Work_Code__r.Review_Needed__c, workOrders[3].Rush__c);
//        
//        //Test stops here
//        Test.stopTest();
//    }
}