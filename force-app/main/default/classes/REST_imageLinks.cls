@RestResource(urlMapping='/imageLinks/*')
global without sharing class REST_imageLinks {
	
	//static string tstjsonpayload='{"prefix":"test","caseID":"500S00000085Of2","s3objs":[{"url":"https://s3.amazonaws.com/allyearimages/test/MG.jpg"},{"url":"https://s3.amazonaws.com/allyearimages/test/Northsight%2BQC%2B10-1-2013%2B098.JPG","MODEL":"DSC-W530","MAKE":"SONY","DATETIME_ORIGINAL":"2013:10:01 12:33:10","DATETIME_FILE":null,"DATETIME_DIGITIZED":"2013:10:01 12:33:10"},{"url":"https://s3.amazonaws.com/allyearimages/test/afterupload.jpg","MODEL":"derp","MAKE":null},{"url":"https://s3.amazonaws.com/allyearimages/test/beforeplupload.jpg","MODEL":"derp","MAKE":null},{"url":"https://s3.amazonaws.com/allyearimages/test/billnye.jpg","MODEL":"Canon EOS 5D","MAKE":"Canon","DATETIME_ORIGINAL":"2011:04:14 16:24:32","DATETIME_FILE":null,"DATETIME_DIGITIZED":"2011:04:14 16:24:32"},{"url":"https://s3.amazonaws.com/allyearimages/test/content_example_ibiza.jpg","MODEL":"iPhone 4","MAKE":"Apple","DATETIME_ORIGINAL":"2011:09:04 12:51:11","DATETIME_FILE":null,"DATETIME_DIGITIZED":"2011:09:04 12:51:11","GPS_DATE":"2011:09:04","GPS_TIME":"11:7:47","GPS_LATITUDE":38.009098333333,"GPS_LONGITUDE":1.0043866666667,"GPS_DOP":null,"GPS_ALTITUDE":0},{"url":"https://s3.amazonaws.com/allyearimages/test/test.pdf"}]}';
	
	class Payload{
		id caseID {get;set;}
		string prefix {get;set;}
		list<map<string,string>> s3objs {get;set;}
	}
		
	@HttpPost
	global static void doPost() {
		RestRequest req = RestContext.request;
		Payload payload = (REST_imageLinks.Payload)json.deserialize(req.requestBody.toString(),REST_imageLinks.Payload.class);
		upsertImageLinks(payload.caseID, payload.s3Objs);
		//insert new debug_log__c(log__c=req.requestBody.toString());	
	}
	
    // public static void tstClearEDinfo(){
    //     imagelinks__c[] imageLinks = [select id, ImageUrl__c,ED_GPS_DATE__c,ED_GPS_TIME__c,ED_GPS_LATITUDE__c,ED_GPS_LONGITUDE__c,ED_GPS_DOP__c,ED_GPS_ALTITUDE__c,ED_DATETIME_ORIGINAL__c,ED_DATETIME_FILE__c,ED_DATETIME_DIGITIZED__c,ED_MODEL__c,ED_MAKE__c from ImageLinks__c];
        
    //     for (imagelinks__c il : imageLinks){
    //         il.ED_GPS_DATE__c= null;                      
    //         il.ED_GPS_TIME__c=null;            
    //         il.ED_GPS_LATITUDE__c = null;               
    //         il.ED_GPS_LONGITUDE__c= null;              
    //         il.ED_GPS_DOP__c  =null;                  
    //         il.ED_GPS_ALTITUDE__c  =null;             
    //         il.ED_DATETIME_ORIGINAL__c    =null;       
    //         il.ED_DATETIME_FILE__c    = null;          
    //         il.ED_DATETIME_DIGITIZED__c = null;         
    //         il.ED_MODEL__c=null;                      
    //         il.ED_MAKE__c=null;     
    //     }
    //     update imagelinks;
    // }

    // public static void wipe(){
    //     delete [select id from imagelinks__c];
    //     delete [select id from ext_file_rlt__c];
    //     delete [select id from ext_file__c];
    // }
    
    // public static void tst(){
    //     Payload payload = (REST_imageLinks.Payload)json.deserialize(tstjsonpayload,REST_imageLinks.Payload.class);
    //     upsertImageLinks(payload.caseID, payload.s3Objs);
    // }	
    
	static void upsertImageLinks(id workOrderID,list<map<string,string>> s3Objs){
	    string prefix = fetchPrefixForWorkOrder(workOrderID);
	    map<string, imagelinks__c> preExistingImageLinks=fetchWOimageLinkURL2imagelinkRecordMap(workOrderID);
	    map<string, EXT_file__c> preExistingExtFiles=fetchExtFileURL2ExtFileRecordMap(workOrderID);
	    
		imagelinks__c[] linksToUpsert = new imagelinks__c[]{};
		EXT_file__c[] extFilesToUpert= new EXT_file__c[]{};
		EXT_file_rlt__c[] extFileRltToInsert = new Ext_file_rlt__c[]{};
		
		for (map<string,string> s3obj : s3Objs){
			string url = s3obj.get('url');
			string strippedURL= stripProtocolPrefixFromURL(url);
			string filename=pullFileNameFromURL(url);
			
			if(isImage(url)){
				system.debug(url + ' is an image');
				if(preExistingImagelinks.containsKey(strippedURL)){
					imagelinks__c il = preExistingImagelinks.get(strippedURL);
					il.ED_GPS_DATE__c= s3obj.get('GPS_DATE');                      
					il.ED_GPS_TIME__c=s3obj.get('GPS_TIME');                     
					il.ED_GPS_LATITUDE__c = s3obj.get('GPS_LATITUDE');                 
					il.ED_GPS_LONGITUDE__c= s3obj.get('GPS_LONGITUDE');                 
					il.ED_GPS_DOP__c  =s3obj.get('GPS_DOP');                     
					il.ED_GPS_ALTITUDE__c  =s3obj.get('GPS_ALTITUDE');                
					il.ED_DATETIME_ORIGINAL__c    = s3obj.get('DATETIME_ORIGINAL');         
					il.ED_DATETIME_FILE__c    = s3obj.get('DATETIME_FILE');             
					il.ED_DATETIME_DIGITIZED__c = s3obj.get('DATETIME_DIGITIZED');           
					il.ED_MODEL__c=s3obj.get('MODEL');                         
					il.ED_MAKE__c=s3obj.get('MAKE');
					il.Last_S3_Refresh_Time__c=datetime.now();   
					linksToUpsert.add(il);                      
				}
				else{
					linksToUpsert.add(new imagelinks__c(
						CaseId__c=workorderid,
						imageUrl__c=url,
						ED_GPS_DATE__c=s3obj.get('GPS_DATE'), 
					    ED_GPS_TIME__c=s3obj.get('GPS_TIME') ,
					    ED_GPS_LATITUDE__c = s3obj.get('GPS_LATITUDE'),
					    ED_GPS_LONGITUDE__c = s3obj.get('GPS_LONGITUDE'),
					    ED_GPS_DOP__c=s3obj.get('GPS_DOP'),
					    ED_GPS_ALTITUDE__c=s3obj.get('GPS_ALTITUDE'),
					    ED_DATETIME_ORIGINAL__c = s3obj.get('DATETIME_ORIGINAL'),    
					    ED_DATETIME_FILE__c = s3obj.get('DATETIME_FILE'),    
					    ED_DATETIME_DIGITIZED__c = s3obj.get('DATETIME_DIGITIZED'),
					    ED_MODEL__c=s3obj.get('MODEL'),
					    ED_MAKE__c=s3obj.get('MAKE'),
					    Last_S3_Refresh_Time__c=datetime.now()
					));
				}
			}				
			else{ //not an image so create\update external file record
				string ext=pullExtensionFromString(filename);
				string lab=filename.removeEnd('.' + ext);
				system.debug(url + ' is NOT an image');
				if (preExistingExtFiles.containsKey(strippedURL)){
					EXT_file__c extFile = preExistingExtFiles.get(strippedURL);
					extFile.format__c=ext;
					extFile.host__c='Amazon';
					extFile.host_product__c='S3';
					extFile.location_type__c='Direct';
					extFile.type__c='Other';
					extFile.Label__c=lab;
					extFile.location_url__c=url;
					extfile.name__c=filename;
					extFilesToUpert.add(extFile);
					
				}
				else{

					
					ext_file__c e = new EXT_file__c(
						format__c= ext ,
						host__c='Amazon',
						host_product__c='S3',
						location_type__c='Direct',
						type__c='Other',
						Label__c= lab,
						location_url__c=url,
						Authentication_Type__c='None',
						name__c=filename
					);
										
					ext_file_rlt__c er = new Ext_file_rlt__c(case__c=workOrderID, External_File__r=e);
					
					
					extFilesToUpert.add(e);
					extFileRltToInsert.add(er);
				}
			}
		}//for
		
		upsert linksToUpsert;
		upsert extFilesToUpert;
		linkRelationToFile(extFileRltToInsert);
		insert extFileRltToInsert;
		
		case c=[select id, Last_S3_Refresh_Time__c from case where id=:workOrderID];
		c.Last_S3_Refresh_Time__c=datetime.now();
		update c;
	}
	
	
	static void linkRelationToFile(ext_file_rlt__c[] relations){
		for (ext_file_rlt__c r : relations){
			r.external_file__c=r.external_file__r.id;	
		}
	}
	
	static string pullFileNameFromURL(string url){
		if (url.contains('/')){
			string[] arr = url.split('\\/');
			return arr[arr.size()-1];
		}
		else
			return '';
	}
	
		
	//don't send a url to this funct 	
	static string pullExtensionFromString(string s){
		string[] arr=s.split('\\.'); //split on . character 
		return arr[arr.size()-1];  //last elem should be extension
	}
	

	//don't send a url to this funct
	static boolean isImage(string fileName){
		set<string> imageExtensions = new set<string>{'jpg','jpeg','tiff'};
		string ext=pullExtensionFromString(filename).tolowerCase();
		return imageExtensions.contains(ext);
	}
	
	static string fetchPrefixForWorkOrder(id workOrderID){
		case wo = [select CaseNumber, Reference_Number__c from case where id=:workOrderID];
		return string.isblank(wo.Reference_number__c) ? wo.CaseNumber : wo.Reference_number__c;
	}
	
	//remove "http://" or "https://" from string
	static string stripProtocolPrefixFromURL(string url){
		string ret= url.remove('https://');
		ret=ret.remove('http://');
		return ret;
	}
	

	
	
	
	static map<string, imagelinks__c> fetchWOimageLinkURL2imagelinkRecordMap(id workOrderID){
		map<string, imagelinks__c> ret = new map<string, imagelinks__c>();
		imagelinks__c[] imageLinks = [select id, ImageUrl__c,ED_GPS_DATE__c,ED_GPS_TIME__c,ED_GPS_LATITUDE__c,ED_GPS_LONGITUDE__c,ED_GPS_DOP__c,ED_GPS_ALTITUDE__c,ED_DATETIME_ORIGINAL__c,ED_DATETIME_FILE__c,ED_DATETIME_DIGITIZED__c,ED_MODEL__c,ED_MAKE__c from ImageLinks__c where CaseId__c=:workOrderID];

		for ( imagelinks__c i : imageLinks){
			string strippedURL=stripProtocolPrefixFromURL(i.imageUrl__c);
			ret.put(strippedURL, i);
			system.debug('adding preexisting url key to map: ' + strippedURL );
		}
		return ret;
	}
	
	static map<string, EXT_file__c> fetchExtFileURL2ExtFileRecordMap(id workOrderID){
		map<string, EXT_file__c> ret = new map<string, EXT_file__c>{};
		EXT_file_rlt__c[] extFiles = [select External_File__r.format__c, External_File__r.host__c, External_File__r.host_product__c, External_File__r.location_type__c, External_File__r.location_url__c, External_File__r.type__c from ext_file_rlt__c where case__c=:workOrderID];
		
		for (ext_file_rlt__c e : extFiles){
			string urlStripped=stripProtocolPrefixFromURL(e.external_file__r.location_url__c);
			ret.put(urlStripped, e.external_file__r);
		}
		return ret;
	}
	
	public static void unitTestBypass(){
		integer i = 0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
				i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
	}    
    

    
}