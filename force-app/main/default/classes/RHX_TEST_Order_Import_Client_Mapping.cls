@isTest(SeeAllData=true)
public class RHX_TEST_Order_Import_Client_Mapping {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Order_Import_Client_Mapping__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Order_Import_Client_Mapping__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}