/**
 *  Purpose         :   This scheduler is used to schedule Batch_DailyRequestCountReset class.
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   07/24/2014
 *
 *  Current Version :   V1.0
 *
 *	Revision Log	:	V1.0 - Created - Padmesh Soni(07/24/2014) - VWMSubmitUpdate
 **/
global class Sched_Batch_DailyRequestCountReset implements Schedulable {
	
	//execute method to execute the logic of batch processing 
    global void execute(SchedulableContext ctx) {
        
        //Batch executes here
        Database.executeBatch(new Batch_DailyRequestCountReset(), 200);
    }
}