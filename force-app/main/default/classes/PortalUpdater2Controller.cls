public without sharing class PortalUpdater2Controller {
    public static final String NO_ORDERS_SELECTED = 'No orders have been selected. Click the "Select Orders" button to get started.';
    public static final String NO_ORDERS_AVAILABLE = 'No work orders available';
    public static final String NO_VENDOR_CODES = 'No available vendor codes found';
    
    public List<Case> inProcessOrders { get; set; }
    public String highestPriorityVendorCode { get; set; }
    public String errorText { get; set; }
    
    //Constructor definition
    public PortalUpdater2Controller() {
        
        //call the initialize method
        initialize();
    }
    
    /**
     *  @description    :   Query for orders that are:
     *                          1. In_Process_By__c = Id of user
     *                          2. Approval_Status__c = 'In Process'
     *                      If the user has no 'In Process' orders a message will be displayed, 
     *                      'No orders have been selected.  Click the "Select Orders" button to get started.'
     * 
     *  @params         :
     *
     *  @return         :   void
     **/
    private void initialize() {
        
        //Call helper class method to get Orders(Case) which are in "In Process"
        inProcessOrders = PortalUpdater2ControllerHelper.fetchWorkOrderInProcess(UserInfo.getUserId());
        
        //Check for returned list is empty
        if (inProcessOrders.isEmpty()) {
            
            //assignment of new values
            errorText = NO_ORDERS_SELECTED;
            inProcessOrders = null;
        } else {
            highestPriorityVendorCode = inProcessOrders[0].Vendor_Code__c;
        }
    }
    
    /**
     *  @description    :   Query for orders that are and update their Approval Status as 'In Process' 
     * 
     *  @params         :
     *
     *  @return         :   PageReference
     **/
    public PageReference assignHighestPriorityWorkOrders() {
        
        try{
            
            //Code modified - Padmesh Soni (08/05/2014) - SM-185 Updater User should not be able to get new orders until all 
            //current orders are processed - Assignment - Portal Updater
            //Check for size of inProcessOrders list
            if(inProcessOrders == null || inProcessOrders.size() == 0) {
                
                Set<String> uniqueCodes = new Set<String>();
                
                List<Portal_Update_Setup__c> vendorCodes = PortalUpdater2ControllerHelper.fetchVendorCodes();
                if (vendorCodes.isEmpty()) {
                    errorText = NO_VENDOR_CODES;
                    return null;
                }
                
                for (Portal_Update_Setup__c currentSetup : vendorCodes) {
                    uniqueCodes.add(currentSetup.Vendor_Code__c);
                }
                
                highestPriorityVendorCode = PortalUpdater2ControllerHelper.fetchHighestPriorityVendorCode(uniqueCodes);
                if (highestPriorityVendorCode == null) {
                    highestPriorityVendorCode = NO_VENDOR_CODES;
                    errorText = NO_VENDOR_CODES;
                    return null;
                }
                
                //System.debug('highestPriorityVendorCode ::::::'+ highestPriorityVendorCode);
                
                //Code modified - one more parameter "inProcessOrders.size()" added into method call and modified with condtional statement 
                //Padmesh Soni(02/13/2014)As per Padmesh Assignment  02/12/2014 "Northsight Updater Portal Bug"
                if(inProcessOrders != null)
                    inProcessOrders.addAll(PortalUpdater2ControllerHelper.fetchHighestPriorityWorkOrdersByVendorCode(highestPriorityVendorCode, inProcessOrders.size()));
                else
                    inProcessOrders = PortalUpdater2ControllerHelper.fetchHighestPriorityWorkOrdersByVendorCode(highestPriorityVendorCode, 0);
                
                inProcessOrders = PortalUpdater2ControllerHelper.setWorkOrdersToInProcess(getWorkOrderIds(inProcessOrders));
                if (inProcessOrders == null) {
                    errorText = NO_ORDERS_SELECTED;
                }
            } else {
                
                //Code modified - Padmesh Soni (08/05/2014) - SM-185 Updater User should not be able to get new orders until all 
                //current orders are processed - Assignment - Portal Updater
                //New error message added and throw it as exception
                errorText = 'Please process all the records first.';
                throw new CustomException(errorText);
            }
            return null;
        } catch(Exception e){
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, e.getMessage()));
            return null;
        }
    }
    
    /**
     *  @description    :   Refresh the page list to getting new orders to be processed. 
     * 
     *  @params         :
     *
     *  @return         :   PageReference
     **/
    public PageReference refreshWorkOrderList() {
        if (inProcessOrders == null) {
            errorText = NO_ORDERS_SELECTED;
            return null;
        }
        
        //Code modified - Padmesh Soni (06/23/2014) - Support Case Record Type
        //New filter criteria added for RecordTypeId
        inProcessOrders = [SELECT CaseNumber, Reference_Number__c, Row_Highlight__c, Work_Ordered__c FROM Case
                                WHERE RecordTypeId NOT IN: PortalUpdater2ControllerHelper.recordTypes 
                                AND Approval_Status__c = :PortalUpdater2ControllerHelper.IN_PROCESS 
                                AND Id IN : inProcessOrders ORDER BY Update_Priority__c DESC, CaseNumber];
                                
        if (inProcessOrders.isEmpty()){ 
            errorText = NO_ORDERS_SELECTED;
            inProcessOrders = null;
        }
        return null;
    }
    
    /**
     *  @description    :   This method is used to getting orders Ids of List. 
     * 
     *  @params         :
     *
     *  @return         :   Set<Id>
     **/
    private Set<Id> getWorkOrderIds(List<Case> pWorkOrders) {
        Set<Id> workOrderIds = new Set<Id>();
        if(pWorkOrders != null){
            for (Case currentWorkOrder : pWorkOrders) {
                workOrderIds.add(currentWorkOrder.Id);
            }
        }
        return workOrderIds;
    }
}