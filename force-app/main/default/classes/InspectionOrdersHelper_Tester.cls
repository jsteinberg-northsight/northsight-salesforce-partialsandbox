public with sharing class InspectionOrdersHelper_Tester {
//	private static Account          newAccount;
//    private static Contact          newContact;
//    private static Profile          newProfile;
//    private static User             newUser;
//    private static Geocode_Cache__c newGeocode;
//    private static RecordType       rt;
//	private static Case 			newCase;
//	
//	private static void generateTestData(){
//		//--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new account
//        newAccount = new Account(
//            Name = 'Tester Account'
//            ,RecordTypeId='0124000000011Q2'
//        );
//        insert newAccount;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new contact
//        newContact = new Contact(
//            LastName = 'Tester',
//            AccountId = newAccount.Id,
//            Pruvan_Username__c = 'NSBobTest',
//			RecordTypeId = '01240000000UPSr'
//        );
//                    			newContact.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//						newContact.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//						newContact.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//						newContact.Shrub_Trim__c = 3;//Pricing...required 
//						newContact.REO_Grass_Cut__c = 3;//Pricing
//						newContact.Pay_Terms__c = 'Net 30';//Terms required
//						newContact.Trip_Charge__c = 25;//Pricing
//						newContact.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//						newContact.Date_Active__c = Date.Today();//Date Active must be set. 
//						newContact.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//						newContact.MailingStreet = '30 Turnberry Dr';
//						newContact.MailingCity = 'La Place';
//						newContact.MailingState = 'LA';
//						newContact.MailingPostalCode = '70068';
//						newContact.OtherStreet = '30 Turnberry Dr';
//						newContact.OtherCity = 'La Place';
//						newContact.OtherState = 'LA';
//						newContact.OtherPostalCode = '70068';
//						newContact.Other_Address_Geocode__Latitude__s = 30.00;
//						newContact.Other_Address_Geocode__Longitude__s = -90.00;
//        insert newContact;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new user with the Customer Portal Manager Standard profile
//        newProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Vendors'];
//        //create a new user with the profile
//        newUser = new User(
//            alias = 'standt',
//            contactId = newContact.Id,
//            email = 'standarduser@testorg.com',
//            emailencodingkey = 'UTF-8',
//            lastname = 'Testing',
//            languagelocalekey = 'en_US',
//            localesidkey = 'en_US',
//            profileId = newProfile.Id,
//            timezonesidkey = 'America/Los_Angeles', username = 'standarduser@northsight.test', isActive = true
//        );
//        insert newUser;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create a new property
//        newGeocode = new Geocode_Cache__c(
//            address_hash__c = '400cedarstbrdgprtct60562',
//            quarantined__c = false,
//            location__latitude__s = 11.11111111,
//            location__longitude__s = -111.22222222,
//            Street_Address__c = '400 Cedar st',
//            Zip_Code__c = '60562',
//            City__c = 'Bridgeport',
//            State__c = 'CT',
//            Formatted_Street_Address__c = '400 Cedar st',
//            Formatted_Zip_Code__c = '60562',
//            Formatted_City__c = 'Bridgeport',
//            Formatted_State__c = 'CT'
//        );
//        insert newGeocode;
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP
//        //create work orders with the REO record type
//        rt = [SELECT Id FROM RecordType WHERE Name = 'INSP' limit 1];
//        //create a work order with the record type
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//        newCase = new Case(
//        	Street_Address__c = '400 Cedar st',
//        	City__c = 'Bridgeport',
//        	State__c = 'CT',
//        	Zip_Code__c = '60562',
//        	Last_Crew_Name_Lookup__c = newContact.Id,
//        	Geocode_Cache__c = newGeocode.Id,
//        	Date_Serviced__c = Date.today(),
//        	Entire_Property_Serviced__c = 'Yes',
//        	Debris_Present__c = 'No',
//            Shrubs_Maintained__c = 'Yes',
//            Edging_Completed__c = 'Yes',
//            Fraud_or_Other__c = 'No',
//            Vendor_Notes_to_Staff__c = 'Vendor Notes for Staff',
//            Client__c = 'SG',
//            Due_Date__c = Date.today(),
//            Work_Completed__c = 'Yes',
//            All_Images_Received__c = true,
//            Vendor_Code__c = 'CHIGRS',
//            Status = 'Open',
//            Description = 'Inspection Order',
//            OwnerId = newUser.Id,
//            ContactId = newContact.Id,
//            Work_Order_Type__c = 'Inspection',
//            Work_Ordered__c = 'QC Inspection',
//            Approval_Status__c = 'Not Started',
//            RecordTypeId = rt.Id,
//            Client_Name__c = account.Id
//        );
//        insert newCase;
//	}
//	
//	//--------------------------------------------------------
//    //TESTS
//    //Unit tests for method testing and regression testing. 
//    //--------------------------------------------------------
//        
//    //--------------------------------------------------------
//    //TEST:
//    //Scenario - testing the standard functionality of the InspectionOrdersHelper
//    public static testmethod void InspectionOrdersHelper_test(){
//    	generateTestData();
//    	//get the REO record type id
//    	RecordType newRT = [SELECT Id FROM RecordType WHERE Name = 'REO' limit 1];
//    	//update the existing order
//    	Case c = [SELECT Id, 
//    					Generate_Follow_Up__c,
//    					Street_Address__c,
//    					City__c,
//    					State__c,
//    					Zip_Code__c,
//    					CaseNumber,
//    					Last_Crew_Name_Lookup__c,
//    					Geocode_Cache__c,
//    					View_Images_Public_Link__c,
//    					Date_Serviced__c,
//    					Entire_Property_Serviced__c,
//    					Debris_Present__c,
//    					Shrubs_Maintained__c,
//    					Edging_Completed__c,
//    					Fraud_or_Other__c,
//    					Vendor_Notes_to_Staff__c,
//    					Description
//    			  FROM Case 
//    			  WHERE Id =: newCase.Id];
//    	c.Generate_Follow_Up__c = true;
//    	update c;
//    	//select the new order that was created
//    	Case newOrder = [SELECT Id,
//    							RecordTypeId,
//    							Work_Ordered__c,
//    							Street_Address__c,
//    							City__c,
//    							State__c,
//    							Zip_Code__c,
//    							Internal_Notes__c,
//    							Work_Order_Type__c,
//    							Vendor_Code__c,
//    							Client__c,
//    							OwnerId,
//    							Geocode_Cache__c,
//    							Description,
//    							Due_Date__c
//    					 FROM Case
//    					 WHERE Id !=: newCase.Id
//    					 order by id desc limit 1 
//    					 ];
//    	//asserts to perform	 
//    	system.assertEquals(newRT.Id, newOrder.RecordTypeId);
//    	system.assertEquals('Grass Cut Follow Up', newOrder.Work_Ordered__c);
//    	system.assertEquals(c.Street_Address__c, newOrder.Street_Address__c);
//    	system.assertEquals(c.City__c, newOrder.City__c);
//    	system.assertEquals(c.State__c, newOrder.State__c);
//    	system.assertEquals(c.Zip_Code__c, newOrder.Zip_Code__c);
//    	
//    	//Code commented due to replacing hard coded values - Padmesh Soni - 1/28/2015 - SM-276:Replace Hard Coded NA2 Links
//    	//system.assertEquals('Follow up generated from ' + c.CaseNumber + ' -- https://na2.salesforce.com/' + c.Id, newOrder.Internal_Notes__c);
//    	//Code added - Padmesh Soni - 1/28/2015
//		String hostname =URL.getSalesforceBaseUrl().getHost(); 
//		String domainURL = 'https://' + hostname ;
//		system.assertEquals('Follow up generated from ' + c.CaseNumber + ' -- '+ domainURL +'/' + c.Id, newOrder.Internal_Notes__c);
//    	
//    	system.assertEquals('Grass', newOrder.Work_Order_Type__c);
//    	system.assertEquals('FOLLOW', newOrder.Vendor_Code__c);
//    	system.assertEquals('FOLLOW', newOrder.Client__c);
//    	system.assertEquals(newUser.Id, newOrder.OwnerId);
//    	system.assertEquals(c.Geocode_Cache__c, newOrder.Geocode_Cache__c);
//    	Datetime dt = c.Date_Serviced__c;
//    	system.assertEquals('This follow up is a result of a failed quality control inspection on Work Order Number ' + c.CaseNumber + '. Please see results and photos below.' + '\n'
//							 + '\n' + 'Inspection Photo Link: ' + c.View_Images_Public_Link__c + '\n'
//							 + '\n' + 'Inspection Date: ' + dt.month()+'/'+dt.day()+'/'+dt.year() + '\n'
//							 + 'Entire Property Serviced: ' + c.Entire_Property_Serviced__c + '\n'
//							 + 'Debris Present: ' + c.Debris_Present__c + '\n'
//							 + 'Shrubs Maintained: ' + c.Shrubs_Maintained__c + '\n'
//							 + 'Edging Completed: ' + c.Edging_Completed__c + '\n'
//							 + 'Other Issues: ' + c.Fraud_or_Other__c + '\n'
//							 + 'Comments: ' + c.Vendor_Notes_to_Staff__c, newOrder.Description);
//    	system.assertEquals(Date.today().addDays(1), newOrder.Due_Date__c);
//    	//select back the updated order
//    	Case updatedOrder = [SELECT Regarding_Work_Order__c FROM Case WHERE Id =: newCase.Id];
//    	//asserts to perform
//    	system.assertEquals(newOrder.Id, updatedOrder.Regarding_Work_Order__c);
//    }
}