@isTest(seeAllData=false)
private class Test_InstructionsAssignmentController {
//
///**
// *  Purpose         :   This is used for testing and covering InstructionsAssignmentController.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   10/27/2014
// *
// *  Current Version :   V1.4
// *
// *  Revision Log    :   V1.0 - Created - INT-36: Work Order Instructions Interface.
// *                      V1.1 - Modified - Padmesh Soni (11/05/2014) - INT-36: Work Order Instructions Interface.
// *                              After Don's updated comment.
// *                      V1.2 - Modified - Padmesh Soni (11/10/2014) - INT-36: Work Order Instructions Interface.
// *                              After Don's updated comment.
// *                      V1.3 - Modified - Padmesh Soni (11/17/2014) - INT-36: Work Order Instructions Interface.
// *                              As per attached "20141108 Work Order Instructions Changes" document.
// *                      V1.4 - Modified - Padmesh Soni (11/19/2014) - INT-36: Work Order Instructions Interface.
// *                              As per attached "20141108 Work Order Instructions Changes" document.
// * 
// *  Coverage        :   100% - V1.0
// *                      100% - V1.1
// *                      100% - V1.2
// *                      100% - V1.3
// *                      100% - V1.4
// **/
//    
//   
//   
//   
//    //Query record of Record Type of Account and Case objects
//    /*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                        OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//                                        OR DeveloperName =: 'Client_Instructions' OR DeveloperName = 'Vendor_Instructions' OR DeveloperName = 'Vendor_Instructions_Hidden') 
//                                        AND (SobjectType =: Constants.CASE_SOBJECTTYPE OR SobjectType = 'Work_Order_Instructions__c'
//                                        OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//   
//   static  List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//                                            OR DeveloperName = 'Client'
//                                            OR DeveloperName = 'Client_Instructions' OR DeveloperName = 'Vendor_Instructions' OR DeveloperName = 'Vendor_Instructions_Hidden') 
//                                            AND (SobjectType =: Constants.CASE_SOBJECTTYPE OR SobjectType = 'Work_Order_Instructions__c'
//                                            OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//   
//   
//   static List<Case> workOrders = new List<Case>();
//   
//   static List<Work_Order_Instructions__c> woInstructions = new List<Work_Order_Instructions__c>();
//   
//   static void init(){
//        
//        //Assert statement
//        System.assertEquals(5, recordTypes.size());
//        
//        //List of Account to store testing records
//        List<Account> accountsToInsert = new List<Account>();
//        accountsToInsert.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsToInsert.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsToInsert;
//        
//        
//        //List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//        //  AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//                                        
//        Account account =  new Account(Name = 'Tester Account',recordTypeId='01240000000UT5T');
//        insert account;
//        //List to hold Case records
//        
//        workOrders.add(new Case(RecordTypeId = recordTypes[2].Id, Client__c = AssignmentMapConstants.CASE_CLIENT_SG,
//                                    AccountId = accountsToInsert[0].Id, Due_Date__c = Date.today().addDays(-2), Status = Constants.CASE_STATUS_OPEN,
//                                    Street_Address__c = '301 Front St', City__c = 'Nome', State__c = 'AK', Zip_Code__c = '99762', Vendor_Code__c = 'CHIGRS',
//                                    Work_Ordered__c = 'Initial Grass Cut',Client_Name__c = account.Id));
//        
//        //insert case records
//        insert workOrders;
//        
//
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[1].Id, Instruction_Type__c = 'ACCESS UNKNOWN', 
//                  Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id, Action__c ='Bid'));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[1].Id, Instruction_Type__c = 'KNOWN ISSUE', 
//                  Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id, Action__c ='Call'));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[1].Id, Instruction_Type__c = 'TESTED OK', 
//                  Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id, Action__c ='Document'));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[1].Id, Instruction_Type__c = 'READY WORK', 
//                  Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id, Action__c ='Photograph'));
//        //insert instructions here
//        insert woInstructions;      
//    }
//   
//    //Method to test the functionality of InstructionsAssignmentController
//    static testMethod void test_ReviewAssignmentInstructions() {
//        init();
//        
//
//
//
//        
//        ApexPages.StandardController sc = new ApexPages.StandardController(workOrders[0]);
//        InstructionsAssignmentController controller = new InstructionsAssignmentController(sc);
//        
//        //assert statements
//        System.assertEquals(4, controller.clientInstructions.size());
//        System.assertEquals(0, controller.vendorInstructions.size());
//        
//        //insert Vendor instructions here
//        insert new Work_Order_Instructions__c(RecordTypeId = recordTypes[3].Id, Instruction_Type__c = 'ACCESS UNKNOWN', 
//                                                Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id, 
//                                                Clone_Of__c = woInstructions[0].Id, Action__c = 'Photograph');
//        
//        controller = new InstructionsAssignmentController(sc);
//        
//        //assert statements
//        System.assertEquals(4, controller.clientInstructions.size());
//        System.assertEquals(1, controller.vendorInstructions.size());
//        
//        //assigning values
//        controller.clientInstToAdd.Instruction_Type__c = 'TESTING FAILED';
//        controller.clientInstToAdd.Item_Instructions__c = 'TESTING is failed.';
//        
//        //call controller's method
//        controller.createClientInstruction();
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c];
//        
//        //assert statement
//        System.assertEquals(6, woInstructions.size());
//        
//        //variable to hold orders Ids
//        String clientIds = '';
//        
//        //Loop through query result
//        for(Work_Order_Instructions__c inst : woInstructions) {
//            
//            //Check if instruction type is "Client Instruction"
//            if(inst.RecordTypeId == recordTypes[0].Id)
//                clientIds += inst.Id + ',';
//        }
//        
//        //setting URL parameter
//        Apexpages.currentPage().getParameters().put('clientIds', clientIds);
//        
//        try {
//            
//            //call method
//            controller.createCopyInstructionList();
//        } catch(Exception e) {
//            
//            //assert statement
//            System.assert(e.getMessage().contains(Label.DUPLICACY_EXISTS)); 
//        }
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c WHERE Instruction_Type__c != 'ACCESS UNKNOWN'];
//    
//        //variable to hold orders Ids
//        clientIds = '';
//        
//        //Loop through query result
//        for(Work_Order_Instructions__c inst : woInstructions) {
//            
//            //Check if instruction type is "Client Instruction"
//            if(inst.RecordTypeId == recordTypes[1].Id)
//                clientIds += inst.Id + ',';
//        }
//        
//        //setting URL parameter
//        Apexpages.currentPage().getParameters().put('clientIds', clientIds);
//        
//        //call method
//        controller.createCopyInstructionList();
//        
//        //assert statement
//        System.assertEquals(4, controller.copyInstructions.size());
//        
//        //call method
//        controller.copyClientToVendorInstruction();
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c WHERE RecordTypeId =: recordTypes[3].Id];
//        
//        //assert statement
//        System.assertEquals(5, woInstructions.size());
//        
//        //Code added - Padmesh Soni (11/05/2014) - INT-36:Work Order Instructions Interface
//        //After Don's updated comment
//        //assigning values
//        controller.vendorInstToAdd.Instruction_Type__c = 'TESTING FAILED';
//        controller.vendorInstToAdd.Item_Instructions__c = 'TESTING is failed.';
//        
//        //call controller's method
//        controller.createVendorInstruction();
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c WHERE RecordTypeId =: recordTypes[3].Id];
//        
//        //assert statement
//        System.assertEquals(6, woInstructions.size());
//        
//        //Code added - Padmesh Soni (11/10/2014) - INT-36:Work Order Instructions Interface
//        //After Don's updated comment
//        //Set URL parameters
//        Apexpages.currentPage().getParameters().put('instId', woInstructions[0].Id);
//        Apexpages.currentPage().getParameters().put('isSwap', 'false');
//        
//        //call controller 
//        controller.swapRecordTypeOnInstruction();
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c WHERE RecordTypeId =: recordTypes[4].Id];
//        
//        //assert statement
//        System.assertEquals(1, woInstructions.size());
//        
//        //Set URL parameters
//        Apexpages.currentPage().getParameters().put('instId', woInstructions[0].Id);
//        Apexpages.currentPage().getParameters().put('isSwap', 'true');
//        
//        //call controller method
//        controller.swapRecordTypeOnInstruction();
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c WHERE RecordTypeId =: recordTypes[4].Id];
//        
//        //assert statement
//        System.assertEquals(0, woInstructions.size());
//        
//        //Set URL parameters
//        Apexpages.currentPage().getParameters().put('vendorIds', controller.vendorInstructions[0].woInstruction.Id +','+ controller.vendorInstructions[1].woInstruction.Id);
//        
//        //call controller method
//        controller.createEditVendorInstructionList();
//        
//        //assert statement
//        System.assertEquals(2, controller.editVendorInstructions.size());
//        
//        //setting values to be updated
//        controller.editVendorInstructions[0].woInstruction.Instruction_Type__c = 'Tested Advanced';
//        controller.editVendorInstructions[1].woInstruction.Instruction_Type__c = 'Tested Advanced';
//        
//        //call controller method 
//        controller.editVendorInstruction();
//        
//                test.starttest();
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c WHERE Instruction_Type__c = 'Tested Advanced'];
//        
//        //assert statement
//        System.assertEquals(2, woInstructions.size());
//        
//        //Code added - Padmesh Soni (11/17/2014) - INT-36:Work Order Instructions Interface
//        //As per attached "20141108 Work Order Instructions Changes" document.
//        List<Selectoption> instructionTypes = controller.getInstTypeSelectList();
//                
//        //assert statement
//        System.assert(instructionTypes.size() == 1);
//        
//        //Create Instruction_Types__c Custom Settings
//        Instruction_Types__c configDefault = new Instruction_Types__c();
//        configDefault.Name = 'CL';
//        configDefault.Value__c = 'GRASSCUT';
//        
//        //insert Custom Setting object
//        insert configDefault;
//                
//        //As per attached "20141108 Work Order Instructions Changes" document.
//        instructionTypes = controller.getInstTypeSelectList();
//                
//        //assert statement
//        System.assert(instructionTypes.size() > 0);
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c WHERE RecordTypeId =: recordTypes[1].Id];
//        
//        //Set URL parameters
//        Apexpages.currentPage().getParameters().put('editClientIds', woInstructions[0].Id +','+ woInstructions[1].Id);
//        
//        //call controller method
//        controller.createEditClientInstructionList();
//        
//        //assert statement
//        System.assertEquals(2, controller.editClientInstructions.size());
//        
//        //Update Instruction Type field
//        controller.editClientInstructions[0].woInstruction.Instruction_Type__c = 'XXX';
//        controller.editClientInstructions[1].woInstruction.Instruction_Type__c = 'XXX';
//        
//        //call controller method
//        controller.editClientInstruction();
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c WHERE Instruction_Type__c = 'XXX'];
//        
//        //assert statement
//        System.assertEquals(2, woInstructions.size());
//        
//        //Set URL parameters
//        Apexpages.currentPage().getParameters().put('instId', controller.vendorInstructions[0].woInstruction.Id);
//        Apexpages.currentPage().getParameters().put('isSwap', 'false');
//        
//        //call controller 
//        controller.swapRecordTypeOnInstruction();
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c WHERE RecordTypeId =: recordTypes[4].Id];
//        
//        //assert statement
//        System.assert(controller.vendorInstructionsHidden.size() == 1);
//        
//        //Set URL parameters
//        Apexpages.currentPage().getParameters().put('vendorIds', controller.vendorInstructionsHidden[0].woInstruction.Id);
//        
//        //call controller method
//        controller.createEditVendorInstructionList();
//        
//        //assert statement
//        System.assertEquals(1, controller.editVendorInstructions.size());
//        
//        controller = new InstructionsAssignmentController(sc);
//        
//        //Code added - Padmesh Soni (11/19/2014) - INT-36:Work Order Instructions Interface
//        //assigning values
//        controller.clientInstToAdd.Instruction_Type__c = 'GRASSCUT';
//        controller.clientInstToAdd.Item_Instructions__c = 'TESTING is failed.';
//        
//        //call controller's method
//        controller.createClientInstruction();
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c WHERE Instruction_Type__c = 'GRASSCUT'];
//        
//        //assert statement
//        System.assert(woInstructions.size() == 1);
//        
//        //setting URL parameter
//        Apexpages.currentPage().getParameters().put('clientIds', woInstructions[0].Id);
//        
//        //call method
//        controller.createCopyInstructionList();
//        
//        //assert statement
//        System.assertEquals(1, controller.copyInstructions.size());
//        
//        //call method
//        controller.copyClientToVendorInstruction();
//                
//        //Set URL parameters
//        Apexpages.currentPage().getParameters().put('instId', woInstructions[0].Id);
//        Apexpages.currentPage().getParameters().put('isSwap', 'false');
//        
//        //call controller method
//        controller.swapRecordTypeOnInstruction();
//        
//        //query result
//        woInstructions = [SELECT Id, RecordTypeId FROM Work_Order_Instructions__c WHERE RecordTypeId =: recordTypes[4].Id AND Instruction_Type__c = 'GRASSCUT'];
//                        
//        //assert statement
//        System.assert(woInstructions.size() == 1);
//        Case[] cases = [select id, Vendor_Instructions_Summary__c from Case where Id in :workOrders];
//        for(Case c:cases){
//            System.assert(c.Vendor_Instructions_Summary__c!=Null);
//        }  
//        
//        test.stoptest();                                     
//
//    }
}