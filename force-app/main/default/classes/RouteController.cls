public without sharing class RouteController {
	public string SITE_URL {
		get { return public_site.url; } 
	
	}
	
	// Default start address
	public string start_address {get; set;}
	public string start_city {get; set;}
	public string start_state {get; set;}
	public string start_zip {get; set;}
	public string status_error{get;set;}
	public string status_error_fields{get;set;}
	// The list of work orders selected for mapping by the user
	public id[] selected_work_orders {get; private set;}
	private Integer maxrecords;
	public boolean redirect {get; set;}

	public string print_link;
		
	public Status__c tmpStatus {get; set;}
	
	private string sort_order;
	public string sort_direction {get; private set;}
	public string sort_field {get; set;}
	private string last_sort_field;
	
	// List of work order wrappers	
	//public wo_cls[] work_orders {get; private set;}
	public string queryOption { get; set; } 
	public transient WorkOrderWrapper[] work_orders {get ; private set;}
	
	public String contact_Id {get; set;}
	public String contact_Initials {get; set;}
	
	// JSON string of some WO information to send to the JS engine
	public string getJSON_work_orders(){
		//system.Debug(logginglevel.error,'getJSON');
	
		if(work_orders==null){
			loadQuery(null);
		}
		string JSON_work_orders ='{';
		string tmplat;
		string tmplng;
		for(WorkOrderWrapper c:work_orders){
		if(JSON_work_orders!='{'){
				JSON_work_orders+=',';
			}
			if(c.wo.geocode_cache__r.location__latitude__s==0 || c.wo.geocode_cache__r.location__latitude__s==null) {
				tmplat = '';
			} else {
				tmplat = string.valueOf(c.wo.geocode_cache__r.location__latitude__s);
			}
			
			if(c.wo.geocode_cache__r.location__longitude__s==0 || c.wo.geocode_cache__r.location__longitude__s==null) {
				tmplng = '';
			} else {
				tmplng = string.valueof(c.wo.geocode_cache__r.location__longitude__s);
			}
		/*JSON_work_orders += '"'+ c.wo.CaseNumber +'": { "Address":"'+ (c.wo.geocode_cache__r.Formatted_Street_Address__c!=null?c.wo.geocode_cache__r.Formatted_Street_Address__c:c.wo.Street_Address__c) + ' ",'
													+ '"City":"'+ (c.wo.geocode_cache__r.Formatted_City__c!=null?c.wo.geocode_cache__r.Formatted_City__c:c.wo.City__c) 
													+ '", State: "'+ (c.wo.geocode_cache__r.Formatted_State__c!=null?c.wo.geocode_cache__r.Formatted_State__c:c.wo.State__c)  
													+ '", Zip:"' + (c.wo.geocode_cache__r.Formatted_Zip_Code__c!=null?c.wo.geocode_cache__r.Formatted_Zip_Code__c:c.wo.Zip_Code__c)
													+'",'
													//+ 'Address_concat:"' + c.wo.Street_Address__c + ', ' + c.wo.City__c + ', ' + c.wo.State__c + ' '+ c.wo.Zip_Code__c + '",'
													+ '"Address_concat":"' + c.wo.geocode_cache__r.Formatted_Address__c + '",'
													+ '"Color":"'+ c.wo.stop_color__c +'",'
													+ '"WON":"' + c.wo.CaseNumber + '",'
													+ '"ID":"'+ c.wo.id +'",'
													+ '"WOD":"'+ c.wo.Work_Ordered__c+'",'
													+ '"LAT":"'+ tmplat +'",'
													+ '"LNG":"'+ tmplng +'",'
													+ '"Serviceable":"'+ c.wo.Routing_Serviceable__c +'",'
													+ '"Priority":"'+ c.wo.Routing_Priority__c +'",'
													+ '"Date":"'+ format_date(c.wo.Scheduled_Date__c) +'",'
													+ '"Status":"'+ c.wo.Status+'",'
													+ '"Client":"'+c.wo.Client_Number__c+'",'
													+ '"Notes":"'+c.wo.notes__c +'",'
													+ '"NotesContent":"'+(c.wo.Description==null?'':c.wo.Description.replace('\r','\\r').replace('\n','\\n').escapeHtml4())+'",'
													+ '"RecNotes":"'+((c.wo.Geocode_Cache__r==null || c.wo.Geocode_Cache__r.notes__c==null)?'':c.wo.Geocode_Cache__r.notes__c.replace('\r','\\r').replace('\n','\\n').escapeHtml4())+'"'
													+ '}';*/ 
		JSON_work_orders += '"'+ c.wo.CaseNumber +'": { "Address":"'+ c.wo.Display_Street__c + '",'
													+ '"City": "'+ c.wo.Display_City__c + '",'
													+ '"State": "'+ c.wo.Display_State__c + '",'
													+ '"Zip": "' + c.wo.Display_Zip_Code__c
													+'",'
													//+ 'Address_concat:"' + c.wo.Street_Address__c + ', ' + c.wo.City__c + ', ' + c.wo.State__c + ' '+ c.wo.Zip_Code__c + '",'
													+ '"Address_concat":"' + c.wo.geocode_cache__r.Formatted_Address__c + '",'
													+ '"Color":"'+ c.wo.stop_color__c +'",'
													+ '"WON":"' + c.wo.CaseNumber + '",'
													+ '"ID":"'+ c.wo.id +'",'
													+ '"WOD":"'+ c.wo.Work_Ordered__c+'",'
													+ '"LAT":"'+ tmplat +'",'
													+ '"LNG":"'+ tmplng +'",'
													+ '"Serviceable":"'+ c.wo.Routing_Serviceable__c +'",'
													+ '"Priority":"'+ c.wo.Routing_Priority__c +'",'
													+ '"Date":"'+ format_date(c.wo.Scheduled_Date__c) +'",'
													+ '"Status":"'+ c.wo.Status+'",'
													+ '"Client":"'+c.wo.Client_Number__c+'",'
													+ '"Notes":"'+c.wo.notes__c +'",'
													+ '"NotesContent":"'+(c.wo.Description==null?'':c.wo.Description.replace('\r','\\r').replace('\n','\\n').escapeHtml4())+'",'
													+ '"RecNotes":"'+((c.wo.Geocode_Cache__r==null || c.wo.Geocode_Cache__r.notes__c==null)?'':c.wo.Geocode_Cache__r.notes__c.replace('\r','\\r').replace('\n','\\n').escapeHtml4())+'"'
													+ '}';
		}
		JSON_work_orders+='}';
		
		System.debug('JSON String::::'+ JSON_work_orders);
		
		return JSON_work_orders;
	}
	
	static Set<string> ApprovalStatusSet;	
	static Set<string> OwnerNameSet;
	static
	{
		ApprovalStatusSet = new Set<string>{'Not Started','Rejected'};
		OwnerNameSet = new Set<string>{'CANCELED','REO Unassigned'};
	}
	
	// Work order wrapper class, has a selected option for easier exporting
	/*
	public class wo_cls { 
		
		public case wo {get; private set;}
		public boolean selected {get; set;}
		
		public wo_cls(case c) {
			wo = c;
			selected = false;
		}
		
	}
	*/
	public string renderType{
		get{
			//system.Debug(logginglevel.error,'renderType');
	
			Set<string> supportedValues=new Set<string>(new string[]{'pdf','gpx-rte','gpx-wpt','itn'});
			string t = ApexPages.currentPage().getParameters().get('renderAs');
			if(supportedValues.contains(t==null?null:t.toLowerCase())){
				
				return t.toLowerCase().substring(0,3);
			}
			else{
				return null;
			}
		}
	}
	public string getExport(){
		string t = ApexPages.currentPage().getParameters().get('renderAs');
		if(t=='gpx-rte'){
			return route.getGarminRouteXML();
		}
		else if(t=='gpx-wpt'){
			return route.getGarminWaypointXML();
		}
		else if(t=='itn'){
			return route.getTomTomITN();
		}else{
			return 'ERROR: Unsupported Export Type';
		}
	}
	public string routeStopsJSON{get;set;}
	public PageReference saveRouteStops(){
		
		route.setRouteStops(routeStopsJSON);		
		redirect = route.save();
		
		//Code added - Padmesh Soni (09/16/2014) - VCP-7:After creating route, user is not redirected back appropriately
		//Code Commentd 
		//return null;
		
		//Code added - Padmesh Soni (09/16/2014) - VCP-7:After creating route, user is not redirected back appropriately
		//Code uncommented and to got redirected to Route Detail page 
		// Go to route detail page
		if(redirect){
			return new PageReference('/'+ route.id);
		}else{
			return null;
		}
	}
	private Route__c rt;
	public RouteWrapper route{get;set;}
	public void checkIp(){
		//System.debug(logginglevel.error,'checkIp');
		String ip = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
		Id UserId = UserInfo.getUserId();
		if (ip != '98.190.130.251') {
			User currentUser =  [SELECT ContactId FROM User WHERE Id =:UserId];
			Contact currentContact = [SELECT Last_Login_Date_Time__c FROM Contact WHERE Id =: currentUser.ContactId];
			List<LoginHistory> history = [SELECT LoginTime FROM LoginHistory WHERE UserId =: UserId ORDER BY LoginTime DESC LIMIT 1];
			if (!history.isEmpty() && history[0].LoginTime!=currentContact.Last_Login_Date_Time__c) {
				futureUpdateLoginDate(currentContact.Id,history[0].LoginTime);
			}
		}
		
	}
	@future
	private static void futureUpdateLoginDate(Id contactId, DateTime loginDate) {
		Contact currentContact = new Contact(Id = contactId, Last_Login_Date_Time__c = loginDate);
		database.update(currentContact,false);
	}

	//Padmesh Soni (06/24/2014) - Support Case Record Type
	//List to hold recordType
	public List<RecordType> recordTypes;
	
	public RouteController(ApexPages.StandardController controller){
		//System.Debug(logginglevel.error,'in controller');
		//Padmesh Soni (06/24/2014) - Support Case Record Type
		//Query record type of Case Object "SUPPORT"    
		recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT 
											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true ORDER BY DeveloperName];
		
		status_error_fields = '{}';
		if(!test.isRunningTest()) {
			/*controller.addFields(new string[]
						{
							'Id',
							'Name',
							'Directions__c',
							'Est_Completion_Date__c',
							'Number_of_Stops__c',
							'Route_Label__c',
							'serializedMapMetaData__c',
							'Calculate_Round_Trip__c',
							'Avoid_HIghways__c',
							'ownerid',
							'permalink__c',
							'createdDate',
							'distance_duration__c',
							'Route_Stops__r.Id',
							'Route_Stops__r.Name',
							'Route_Stops__r.Address__c',
							'Route_Stops__r.City__c',
							'Route_Stops__r.State__c',
							'Route_Stops__r.Zip_Code__c',
							'Route_Stops__r.Stop_Number__c',
							'Route_Stops__r.originalIndex__c',
							'Route_Stops__r.Location__Latitude__s',
							'Route_Stops__r.Location__Longitude__s',
							'Route_Stops__r.Work_Order__c',
							'Route_Stops__r.Work_Order__r',
							'Route_Stops__r.Work_Order__r.id',
						    'Route_Stops__r.Work_Order__r.Description',
						    'Route_Stops__r.Work_Order__r.Zip_Code__c', 
						    'Route_Stops__r.Work_Order__r.Scheduled_Date__c',
						    'Route_Stops__r.Work_Order__r.Work_Ordered__c', 
						    'Route_Stops__r.Work_Order__r.Work_Ordered_Text__c', 
							'Route_Stops__r.Work_Order__r.Street_Address__c', 
						    'Route_Stops__r.Work_Order__r.City__c',
							'Route_Stops__r.Work_Order__r.Status_Health_Text__c', 
							'Route_Stops__r.Work_Order__r.Status',
							'Route_Stops__r.Work_Order__r.Vendor_Status__c', 
							'Route_Stops__r.Work_Order__r.State__c', 
							'Route_Stops__r.Work_Order__r.notes__c', 
							'Route_Stops__r.Work_Order__r.Client_Number__c', 
							'Route_Stops__r.Work_Order__r.CaseNumber',
							'Route_Stops__r.Work_Order__r.geocode_cache__r.location__latitude__s',
							'Route_Stops__r.Work_Order__r.geocode_cache__r.location__longitude__s',
						    'Route_Stops__r.Work_Order__r.geocode_cache__r.formatted_address__c',
						  	'Route_Stops__r.Work_Order__r.geocode_cache__r.formatted_street_address__c',
							'Route_Stops__r.Work_Order__r.geocode_cache__r.formatted_city__c',
							'Route_Stops__r.Work_Order__r.geocode_cache__r.formatted_state__c',
						    'Route_Stops__r.Work_Order__r.geocode_cache__r.formatted_zip_Code__c',
							'Route_Stops__r.Work_Order__r.geocode_cache__r.notes__c'
						});*/
			controller.addFields(new string[]
						{
							'Id',
							'Name',
							'Directions__c',
							'Est_Completion_Date__c',
							'Number_of_Stops__c',
							'Route_Label__c',
							'serializedMapMetaData__c',
							'Calculate_Round_Trip__c',
							'Avoid_HIghways__c',
							'ownerid',
							'permalink__c',
							'createdDate',
							'distance_duration__c',
							'Route_Stops__r.Id',
							'Route_Stops__r.Name',
							'Route_Stops__r.Address__c',
							'Route_Stops__r.City__c',
							'Route_Stops__r.State__c',
							'Route_Stops__r.Zip_Code__c',
							'Route_Stops__r.Stop_Number__c',
							'Route_Stops__r.originalIndex__c',
							'Route_Stops__r.Location__Latitude__s',
							'Route_Stops__r.Location__Longitude__s',
							'Route_Stops__r.Work_Order__c',
							'Route_Stops__r.Work_Order__r',
							'Route_Stops__r.Work_Order__r.id',
						    'Route_Stops__r.Work_Order__r.Description',
						    'Route_Stops__r.Work_Order__r.Zip_Code__c', 
						    'Route_Stops__r.Work_Order__r.Scheduled_Date__c',
						    'Route_Stops__r.Work_Order__r.Work_Ordered__c', 
						    'Route_Stops__r.Work_Order__r.Work_Ordered_Text__c', 
							'Route_Stops__r.Work_Order__r.Street_Address__c', 
						    'Route_Stops__r.Work_Order__r.City__c',
							'Route_Stops__r.Work_Order__r.Status_Health_Text__c', 
							'Route_Stops__r.Work_Order__r.Status',
							'Route_Stops__r.Work_Order__r.Vendor_Status__c', 
							'Route_Stops__r.Work_Order__r.State__c', 
							'Route_Stops__r.Work_Order__r.notes__c', 
							'Route_Stops__r.Work_Order__r.Client_Number__c', 
							'Route_Stops__r.Work_Order__r.CaseNumber',
							'Route_Stops__r.Work_Order__r.geocode_cache__r.location__latitude__s',
							'Route_Stops__r.Work_Order__r.geocode_cache__r.location__longitude__s',
						    'Route_Stops__r.Work_Order__r.geocode_cache__r.formatted_address__c',
						  	'Route_Stops__r.Work_Order__r.Display_Street__c',
							'Route_Stops__r.Work_Order__r.Display_City__c',
							'Route_Stops__r.Work_Order__r.Display_State__c',
						    'Route_Stops__r.Work_Order__r.Display_Zip_Code__c',
							'Route_Stops__r.Work_Order__r.geocode_cache__r.notes__c'
						});			
		}
		//System.Debug(logginglevel.error,'in Controller before get record');
		rt = (Route__c) controller.getRecord();
		//System.Debug(logginglevel.error,'After get Record');
		route = new RouteWrapper(rt);
		//System.Debug(logginglevel.error,'After Wrapper Build');
		route.stops.sort();
		
		
		// Get logged in user's information
		// Note: Are these the address fields we want to use for starting point?
		User u = [Select Contact.FirstName, Contact.LastName, Contact.OtherStreet, Contact.OtherState, Contact.OtherPostalCode, Contact.OtherCity From User where Id =:UserInfo.getUserId()];
		Contact c = u.Contact;
		//System.Debug(logginglevel.error,'After Contact ' + c);
	
		if(c!=null) {
		
			start_address = c.OtherStreet;
			start_city = c.OtherCity;
			start_state = c.OtherState;
			start_zip = c.OtherPostalCode;
			
			//Padmesh Soni
			contact_Id = c.Id;
			contact_Initials = (c.FirstName != null || c.FirstName != ''? c.FirstName.left(1): c.FirstName) + (c.LastName != null || c.LastName != ''? c.LastName.left(1): c.LastName);
		}
		
		tmpStatus = new Status__c();
		
		queryOption = '1'; // Default
		MaxRecords = 1000;
		redirect = false;
		sort_order = 'Up'; // not used until the first sort
		
		// Populate work order list - default sort
		loadQuery(null);
	}
	
	
	public pageReference reLoadQuery() {
		//System.Debug(logginglevel.error,'reLoadQuery');
		loadQuery(null);
		return null;
	}
	
	public void loadQuery(Set<Id> aList) {
		//System.Debug(logginglevel.error,'loadQuery ' + aList);
		// Get relevant work orders
		transient case[] caseList;
		//System.Debug(logginglevel.error,'Query ' + queryOption);
		
		if (queryOption == '1') { //open
				caseList =  [select 	  Id,
										  Zip_Code__c, 
										  Scheduled_Date__c,
										  Work_Ordered__c, 
										  Work_Ordered_Text__c, 
										  Street_Address__c, 
										  City__c,
										  Status_Health_Text__c,
										  Status_Health__c, 
										  Vendor_Status__c,
										  Status, 
										  State__c, 
										  notes__c, 
										  Description,
										  Client_Number__c, 
										  CaseNumber,
										  geocode_cache__r.location__latitude__s,
								  		  geocode_cache__r.location__longitude__s,
								  		  geocode_cache__r.formatted_address__c,
								  		  geocode_cache__r.formatted_street_address__c,
								  		  geocode_cache__r.formatted_city__c,
								  		  geocode_cache__r.formatted_state__c,
								  		  geocode_cache__r.formatted_zip_Code__c,
								  		  geocode_cache__r.notes__c,
								  		  Stop_Color__c,
								  		  Routing_Serviceable__c,
								  		  Routing_Priority__c,
								  		  Display_Street__c,
								  		  Display_City__c,
								  		  Display_State__c,
								  		  Display_Zip_Code__c
									from 
										Case 
									where 
										City__c != null and
										RecordTypeId NOT IN: recordTypes AND
										RecordType.Name IN ('REO','INSP','MAID','INSPM') and
										Approval_Status__c IN: ApprovalStatusSet and
										Owner.Name NOT IN: OwnerNameSet and 
										Status != 'Canceled' and
										OwnerId =: UserInfo.getUserId() and
										quarantine__c != 1
									order by
										Scheduled_Date__c
									limit :MaxRecords];
					
		} else if (queryOption == '2') { //submitted
				caseList = [select    Id,
									  Zip_Code__c, 
									  Scheduled_Date__c,
									  Work_Ordered__c, 
									  Work_Ordered_Text__c, 
									  Street_Address__c, 
									  City__c,
									  Status_Health_Text__c, 
									  Status_Health__c,
									  Vendor_Status__c,
									  Status, 
									  State__c, 
									  notes__c, 
									  Description,
									  Client_Number__c, 
									  CaseNumber,
									  geocode_cache__r.location__latitude__s,
								      geocode_cache__r.location__longitude__s,
							  		  geocode_cache__r.formatted_address__c,
								  	  geocode_cache__r.formatted_street_address__c,
							  		  geocode_cache__r.formatted_city__c,
							  		  geocode_cache__r.formatted_state__c,
							  		  geocode_cache__r.formatted_zip_Code__c,
							  		  geocode_cache__r.notes__c,
							  		  Stop_Color__c,
							  		  Routing_Serviceable__c,
							  		  Routing_Priority__c,
							  		  Display_Street__c,
							  		  Display_City__c,
							  		  Display_State__c,
							  		  Display_Zip_Code__c
								from 
									Case 
								where 
									RecordTypeId NOT IN: recordTypes AND
									RecordType.Name IN ('REO','INSP','MAID','INSPM') and
									Approval_Status__c NOT IN: ApprovalStatusSet and
									Owner.Name NOT IN: OwnerNameSet and
									Pre_Pending_Date_Time__c = LAST_N_DAYS:30 and
									OwnerId =: UserInfo.getUserId()
									
								order by
									Scheduled_Date__c
								limit :MaxRecords];
			
		} else if (queryOption == '3')   { //cancelled
				caseList = [select Id, 
								  Zip_Code__c, 
								  Scheduled_Date__c,
								  Work_Ordered__c, 
								  Work_Ordered_Text__c, 
								  Street_Address__c, 
								  City__c,
								  Status_Health_Text__c, 
								  Status_Health__c,
								  Vendor_Status__c,
								  Status, 
								  State__c, 
								  notes__c, 
								  Description,
								  Client_Number__c, 
								  CaseNumber,
								  geocode_cache__r.location__latitude__s,
								  geocode_cache__r.location__longitude__s,
						  		  geocode_cache__r.formatted_address__c,
						  		  geocode_cache__r.formatted_street_address__c,
						  		  geocode_cache__r.formatted_city__c,
						  		  geocode_cache__r.formatted_state__c,
						  		  geocode_cache__r.formatted_zip_Code__c,
								  geocode_cache__r.notes__c,
						  		  Stop_Color__c,
						  		  Routing_Serviceable__c,
						  		  Routing_Priority__c,
						  		  Display_Street__c,
						  		  Display_City__c,
						  		  Display_State__c,
						  		  Display_Zip_Code__c
								from 
									Case 
								where 
									RecordTypeId NOT IN: recordTypes AND
									RecordType.Name IN ('REO','INSP','MAID','INSPM') and
									Approval_Status__c IN: ApprovalStatusSet and
									Owner.Name NOT IN: OwnerNameSet and 
									Status = 'Canceled' and
									OwnerId =: UserInfo.getUserId()
								order by
									Scheduled_Date__c
								limit :MaxRecords];
		} 
		/* -- Josh steered the functionality away from needing this one by not refreshing the
				work order list to only selected items when updating the status
		else if (queryOption == '4') {
				caseList = [select 
									Zip_Code__c, 
								  Scheduled_Date__c,
								  Work_Ordered__c, 
								  Work_Ordered_Text__c, 
								  Street_Address__c, 
								  City__c,
								  Status_Health_Text__c, 
								  Status_Health__c,
								  Vendor_Status__c,
								  Status, 
								  State__c, 
								  notes__c, 
								  Description,
								  Client_Number__c, 
								  CaseNumber
								from 
									Case 
								where 
									Id IN: aList
								order by
									Scheduled_Date__c
								limit :MaxRecords];
			}
			*/

		
		
		// Initialize wrapper list
		work_orders = new WorkOrderWrapper[] {};

		// Populate the work order class list	
		//  Also build a JSON string to make address more easily available to JS		 
		for(case c : caseList) {
			work_orders.add(new WorkOrderWrapper(c));
		} 
		
		
		
	}
	
	
	public pageReference updateStatus() {
		
			loadQuery(null);
			Set<Id>WorkOrderIdList = new Set<Id>();
			
			//if work order is selected, add work order # to the list
			for (WorkOrderWrapper currentObject : work_orders)
			{
				if (currentObject.selected)
				{
					//add to list of selected work orders
					WorkOrderIdList.add(currentObject.wo.Id);  //add to list to get updated
				}	
			}
		
			
			//get all statuses for selected work orders
			List<Status__c> StatusToUpdate = new List<Status__c>();
			
			
			//Do we need to create new status records? Check if there are remaining Ids and add new status records for them
			if (WorkOrderIdList.size() > 0)
			{
				
				//Loop thru ID set and add a new Status object
				for (Id currentId : WorkOrderIdList)
				{
					Status__c newStatus = new Status__c();
					newStatus.Expected_Upload_Date__c = tmpStatus.Expected_Upload_Date__c;
					newStatus.Explanation__c = tmpStatus.Explanation__c;
					newStatus.Delay_Reason__c = tmpStatus.Delay_Reason__c;
					newStatus.Order_Status__c = tmpStatus.Order_Status__c;
					newStatus.Case__c = currentId;
					
					StatusToUpdate.add(newStatus);
				}
			}
			
				Database.SaveResult[] srList = database.insert(StatusToUpdate,false);
				status_error = '';
				set<string> errorset = new set<string>();
				set<string> fieldset = new set<string>();
				for(Database.Saveresult sr:srList){
					if(!sr.isSuccess()){
						for(Database.Error e:sr.getErrors()){
							string m = e.getMessage();
								fieldset.addAll(e.getFields());
								errorset.add(m);
						}
					}
				}
				status_error_fields = Json.serializePretty(fieldset);
				status_error = string.join(new List<string>(errorset),'\r\n');
			
			queryOption = '1';
			loadQuery(null);

			//Clear status information
			tmpStatus = new Status__c();
			
			return null;
		
	}
	
	public pageReference ExportPrint(){
		return null;
	}
	public pageReference Export() {
	pageReference pr = Null;
		pr = Page.WorkOrderExportPrint;
		pr.setRedirect(false);
		this.maxrecords = 10000;
		this.WOSort();
		this.maxrecords = 1000;
		string param = '';
		integer counter = 0;
		for(WorkOrderWrapper w : work_orders) {
			if(w.selected) {
				if(counter != 0) {
					param += ',';
				}
				param += w.wo.id;
				counter++;
			}	
		}
		// if none were selected, add them all
		if(counter==0) {
			for(WorkOrderWrapper w : work_orders) {
				if(counter != 0) {
					param += ',';
				}
				param += w.wo.id;
				counter++;
			}
		}
		
		pr.getParameters().put('wo_list', param);
		return pr;
	}
	
		public pageReference Print() {
	pageReference pr = Null;
		pr = Page.WorkOrderPrint;
		this.maxrecords = 10000;
		this.WOSort();
		this.maxrecords = 1000;
		string param = '';
		integer counter = 0;
		for(WorkOrderWrapper w : work_orders) {
			if(w.selected) {
				if(counter != 0) {
					param += ',';
				}
				param += w.wo.id;
				counter++;
			}	
		}
		// if none were selected, add them all
		if(counter==0) {
			for(WorkOrderWrapper w : work_orders) {
				if(counter != 0) {
					param += ',';
				}
				param += w.wo.id;
				counter++;
			}
		}
		
		pr.getParameters().put('wo_list', param);
		return pr;
	}
	
	public string getprint_link() {
		pageReference pr = Page.WorkOrderPrint;
		
		string param = '';
		integer counter = 0;
		for(WorkOrderWrapper w : work_orders) {
			if(w.selected) {
				if(counter != 0) {
					param += ',';
				}
				param += w.wo.id;
				counter++;
			}	
		}
		// if none were selected, add them all
		if(counter==0) {
			for(WorkOrderWrapper w : work_orders) {
				if(counter != 0) {
					param += ',';
				}
				param += w.wo.id;
				counter++;
			}
		}
		
		pr.getParameters().put('wo_list', param);
		// Return the URL string
		return pr.getUrl();
	}
	
	
	public pageReference WOSort() {
		// If the same field is being sorted again, reverse direction
		
		if(sort_field == last_sort_field) {
			if(sort_direction=='Up') {
				sort_direction = 'Down';
			} else {
				sort_direction = 'Up';
			}
		} else {
			sort_direction = 'Up';
		}
		WorkOrderWrapper.sortField = sort_field;
		WorkOrderWrapper.sortDirection = sort_direction;
		loadQuery(null);
		if(work_orders!=null){
			work_orders.sort();
		}
		last_sort_field = sort_field;
		return null;
	}
	
	
	
	private string format_date(datetime d) {
		if(d==null) {
			return '';
		} else {
			//return d.format();
			return d.month() + '/' + d.day() + '/' + d.year();
		}
	}
	
	public static void unitTestBypass(){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;		
	}
}