/**
 *	Purpose			:	This is a controller for send SMS to given phone number. 
 *
 *	Created By		:	Padmesh Soni
 *
 *	Created Date	:	01/01/2014
 *
 *	Current Version	:	V1.0
 *
 *	Revision Log	:	V1.0 - Created  
 **/
public with sharing class TwilioSendSMSPOC {
	/*
	public String phoneNumber {get; set;}
	public String messageBody {get; set;}
	
	//Constructor definition
	public TwilioSendSMSPOC() {
		
	}
	

//     * 	@description	:	This method is to send the Gate Code to phoneNumber
//     *
//     *	@args			:
//     *
//     *	@return			:	void
//     *

    public void sendMessage() {

    	// Set the To and Body parameters in a Map
		Map<String,String> params = new Map<String,String> {
															'To' => phoneNumber,
															'Body' => messageBody
															};

		// Initialize Twilio Service Class
		TwilioService service = new TwilioService();

		// Send SMS using the parameters
		TwilioService.SMSResponse response = service.sendSMS(params);
    }
*/
}