/**
 *  Purpose         :   This class is helper for holding all of the pre & post functionality logic related to Work Order Instructions trigger. 
 *
 *  Created By      :   Padmesh Soni
 *
 *  Created Date    :   2/12/2015
 *
 *  Current Version :   V1.0
 *
 *  Revision Log    :   V1.0 - Created - WC-3: New "Sort" field and trigger Line_c to Sort__c trigger for Work_Order_Instructions__c
 **/
public without sharing class WOInstructionsTriggerHelper {
	
	/**
	 *	@description	:	This method is used to update the "Vendor_Instructions_Summary__c" field on parent Case.
	 *
	 *	@params			:	List<Work_Order_Instructions__c> newWOInstructions, Map<Id, Work_Order_Instructions__c> oldWOInstructionsMap
	 *
	 *	@return			:	void
	 **/
	public static Set<Id> summariesNeeded = new Set<Id>();
	public static void updateVendorSummaryOnOrder(List<Work_Order_Instructions__c> newWOInstructions, Map<Id, Work_Order_Instructions__c> oldWOInstructionsMap) {
		
		//Set of Ids to hold the Work Order Ids related to Instructions
		summariesNeeded = new Set<Id>();
		//Check for delete request type in execution mode
		if(newWOInstructions == null && oldWOInstructionsMap != null) {
			
			//Loop through map's keys
			for(Id workOrderId : oldWOInstructionsMap.keyset()) {
				
				//Check for not null & populate set of Ids
				if(oldWOInstructionsMap.get(workOrderId).Case__c != null)
					summariesNeeded.add(oldWOInstructionsMap.get(workOrderId).Case__c);	
			}
		}
		
		if(newWOInstructions != null) {
		
			//Loop through trigger.new
			for(Work_Order_Instructions__c newInstruction : newWOInstructions){
				
				//Check for insert & undelete request type in execution mode
				//Check for not null & populate set of Ids
				if(newInstruction.Case__c != null && oldWOInstructionsMap == null)
					summariesNeeded.add(newInstruction.Case__c);
				
				//Check for update request type in execution mode
				//Check for previous and old values are not same for Item_Instructions__c & RecordType
				//and populate set of Ids
				if(oldWOInstructionsMap != null 
					&& (
					newInstruction.Item_Instructions__c != oldWOInstructionsMap.get(newInstruction.Id).Item_Instructions__c ||
					newInstruction.Instruction_Type__c != oldWOInstructionsMap.get(newInstruction.Id).Instruction_Type__c ||
					newInstruction.Action__c != oldWOInstructionsMap.get(newInstruction.Id).Action__c || 
					newInstruction.RecordTypeId != oldWOInstructionsMap.get(newInstruction.Id).RecordTypeId))
					summariesNeeded.add(newInstruction.Case__c);
			}
		}
		
		//Check for size of set
		if(summariesNeeded.size() > 0){
			
			//Map to hold Case records which need to be update
			Map<Id, Case> workOrdersToUpdate = new Map<Id, Case>();
			
			//Map to hold Case records which need to be update
			Map<Id, List<Work_Order_Instructions__c>> wrkOrderWithInstructon = new Map<Id, List<Work_Order_Instructions__c>>();
			
			//Variable to hold values to further process
			String summary = '';
			String actionText = '';
			String summaryType;
			String summaryInstructions;
			
			//Loop throguh queried result on Instructions
			for(Work_Order_Instructions__c woInstruction : [SELECT Instruction_Type__c, Item_Instructions__c, Case__c, Action__c FROM Work_Order_Instructions__c 
															WHERE Case__c IN: summariesNeeded AND RecordType.DeveloperName = 'Vendor_Instructions' ORDER BY Sort__c ASC NULLS FIRST, Line__c ASC NULLS FIRST, CreatedDate DESC]) {
				
				if(wrkOrderWithInstructon.containsKey(woInstruction.Case__c))
					wrkOrderWithInstructon.get(woInstruction.Case__c).add(woInstruction);
				else
					wrkOrderWithInstructon.put(woInstruction.Case__c, new List<Work_Order_Instructions__c>{woInstruction});
			}
			
			if(wrkOrderWithInstructon.size() > 0) {
				
				for(Id woId : wrkOrderWithInstructon.keyset()) {
					
					for(Work_Order_Instructions__c woInstruction : wrkOrderWithInstructon.get(woId)) {
						
						//Check for already existing Case Id as key in map
						if(workOrdersToUpdate.containsKey(woInstruction.Case__c)) {
							
							//assigning new values to variables
							summary = '';
							summaryType = woInstruction.Instruction_Type__c != null ? woInstruction.Instruction_Type__c : '';
							summaryInstructions = woInstruction.Item_Instructions__c != null ? woInstruction.Item_Instructions__c : '';
							actionText = woInstruction.Action__c != null ? woInstruction.Action__c : '';
							summary += '<b>'+summaryType + ' - ' + '</b>';
							if(actionText != '')
								summary += '<b>'+actionText + ' - ' + '</b>';
								
							summary += summaryInstructions + '<br/><br/>';
							
							//populate Vendor Instructions Summary field
							workOrdersToUpdate.get(woInstruction.Case__c).Vendor_Instructions_Summary__c += summary;
						} else {
							
							//assigning new values to variables
							summary = '';
							summaryType = woInstruction.Instruction_Type__c != null ? woInstruction.Instruction_Type__c : '';
							summaryInstructions = woInstruction.Item_Instructions__c != null ? woInstruction.Item_Instructions__c : '';
							actionText = woInstruction.Action__c != null ? woInstruction.Action__c : '';
							summary += '<b>'+summaryType + ' - ' + '</b>';
							if(actionText != '')
								summary += '<b>'+actionText + ' - ' + '</b>';
							summary += summaryInstructions + '<br/><br/>';
							
							//putting case on map
							workOrdersToUpdate.put(woInstruction.Case__c, new Case(Id = woInstruction.Case__c, Vendor_Instructions_Summary__c = summary));
							summariesNeeded.remove(woInstruction.Case__c);
						}		
					}
				}
			}
			//List of Case
			List<Case> workOrders = new List<Case>();
			
			//Check for map size & perform DML update on its values
			if(workOrdersToUpdate.size() > 0)
				workOrders.addAll(workOrdersToUpdate.values());
				
			if(summariesNeeded.size() > 0) {
				
				for(Id woId : summariesNeeded) {
					workOrders.add(new Case(Id = woId, Vendor_Instructions_Summary__c = ''));
				}
			}
			
			//Check for map size & perform DML update on its values
			if(workOrders.size() > 0)
				update workOrders;
		}
	}
	
	/**
	 *	@description	:	This method is used to update the "Sort__c" field from "Line__c" when sort is null.
	 *
	 *	@params			:	List<Work_Order_Instructions__c> newWOInstructions
	 *
	 *	@return			:	void
	 **/
	public static void updateSortFromLine(List<Work_Order_Instructions__c> newWOInstructions) {
		
		//Loop through trigger.new
		for(Work_Order_Instructions__c newInstruction : newWOInstructions){
			
			//Check for not null
			if(newInstruction.Sort__c == null && newInstruction.Line__c != null) {
				
				//assign values from Line to Sort 
				newInstruction.Sort__c = newInstruction.Line__c;
			}	
		}
	}
}