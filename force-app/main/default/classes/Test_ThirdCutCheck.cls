@isTest(seeAllData=false)
private class Test_ThirdCutCheck {
///**
// *  Purpose         :   This is used for testing and covering ThirdCutCheck class.
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   06/18/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created - Padmesh Soni(06/18/2014) - Support Case Record Type
// *	
// *	Coverage		:	100% - V1.0
// **/
// 
//    static testMethod void testIt(){
//    	
//    	//Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client'
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(3, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	//insert Account records here
//    	insert accounts;
//		
//		//Create a test record for Property
//		Geocode_Cache__c property = new Geocode_Cache__c(Active__c = true);
//		insert property;
//		
//    	//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, 
//    								Status = Constants.CASE_STATUS_OPEN, Client__c = Constants.CASE_CLIENT_SG,
//    								Work_Ordered__c = 'Initial Grass Cut', Approval_Status__c = 'Approved',
//    								Date_Serviced_When_Approved__c = Date.today()));
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, 
//    								Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN, Scheduled_Date_Override__c = Date.today(),
//    								Client__c = Constants.CASE_CLIENT_SG, Work_Ordered__c = 'Initial Grass Cut',
//    								Approval_Status__c = 'Approved', Date_Serviced_When_Approved__c = Date.today()));
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, 
//    								Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN, Scheduled_Date_Override__c = Date.today(),
//    								Client__c = Constants.CASE_CLIENT_SG, Work_Ordered__c = 'Initial Grass Cut',
//    								Approval_Status__c = 'Approved', Date_Serviced_When_Approved__c = Date.today()));
//    	workOrders.add(new Case(Geocode_Cache__c = property.Id, RecordTypeId = recordTypes[2].Id, AccountId = accounts[0].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = Constants.CASE_STATUS_OPEN,
//    								Client__c = Constants.CASE_CLIENT_SG, Work_Ordered__c = 'Initial Grass Cut', Approval_Status__c = 'Approved',
//    								Date_Serviced_When_Approved__c = Date.today()));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, 
//    								Status = Constants.CASE_STATUS_OPEN, Client__c = Constants.CASE_CLIENT_SG,
//    								Work_Ordered__c = 'Grass Recut', Approval_Status__c = 'Approved',
//    								Date_Serviced_When_Approved__c = Date.today(), State__c = 'TX'));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, 
//    								Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN, Scheduled_Date_Override__c = Date.today(),
//    								Client__c = Constants.CASE_CLIENT_SG, Work_Ordered__c = 'Grass Recut', State__c = 'TX',
//    								Approval_Status__c = 'Approved', Date_Serviced_When_Approved__c = Date.today()));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, 
//    								Due_Date__c = Date.today(), Status = Constants.CASE_STATUS_OPEN, Scheduled_Date_Override__c = Date.today(),
//    								Client__c = Constants.CASE_CLIENT_SG, Work_Ordered__c = 'Grass Recut', State__c = 'TX',
//    								Approval_Status__c = 'Approved', Date_Serviced_When_Approved__c = Date.today()));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[2].Id, AccountId = accounts[0].Id, 
//    								Due_Date__c = Date.today().addDays(1), Status = Constants.CASE_STATUS_OPEN,
//    								Client__c = Constants.CASE_CLIENT_SG, Work_Ordered__c = 'Grass Recut', Approval_Status__c = 'Approved',
//    								Date_Serviced_When_Approved__c = Date.today(), State__c = 'TX'));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//    	//Test starts here
//    	Test.startTest();
//    	
//    	// Schedule the test job at midnight Sept. 3rd. 2022
//        String jobId = System.schedule('Third Cut Check', '0 0 0 3 9 ? 2022', new ThirdCutCheck());
//        
//        // Get the information from the CronTrigger API object
//        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
//        
//        // Verify the expressions are the same
//        System.assertEquals('0 0 0 3 9 ? 2022', ct.CronExpression);
//        
//        // Verify the job has not run
//        System.assertEquals(0, ct.TimesTriggered);
//        
//		//Test stops here
//		Test.stopTest();
//	}
}