@isTest(seeAllData=false)
private class Test_WOInstructionsTrigger {
//
///**
// *  Purpose         :   This class is used to cover the pre and post functionality of trigger on Work Orde Instruction. 
// *
// *  Created By      :   Padmesh Soni
// *
// *  Created Date    :   2/12/2015
// *
// *  Current Version :   V1.0
// *
// *  Revision Log    :   V1.0 - Created - WC-3: New "Sort" field and trigger Line_c to Sort__c trigger for Work_Order_Instructions__c
// *
// *	Code Coverage	:	100%(V1.0)
// **/
//	
//	static List<RecordType> recordTypes;
//	static List<Account> accountsToInsert;
//	static List<Case> workOrders;
//	
//	static void generateTestData() {
//		
//		//query result of Record Types
//        recordTypes = [SELECT Id, Name FROM RecordType 
//        									WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER OR DeveloperName = 'Client'
//                                            OR DeveloperName = 'Client_Instructions' OR DeveloperName = 'Vendor_Instructions' 
//                                            OR DeveloperName = 'Vendor_Instructions_Hidden') AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//                                            OR SobjectType = 'Work_Order_Instructions__c' OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) 
//                                            AND IsActive = true ORDER BY DeveloperName];
//        
//        //Assert statement
//        System.assertEquals(5, recordTypes.size());
//        
//        //List of Account to store testing records
//        accountsToInsert = new List<Account>();
//        accountsToInsert.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsToInsert.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsToInsert;
//        
//        //List to hold Case records
//        workOrders = new List<Case>();
//        workOrders.add(new Case(RecordTypeId = recordTypes[2].Id, Client__c = AssignmentMapConstants.CASE_CLIENT_SG,
//                                    AccountId = accountsToInsert[0].Id, Due_Date__c = Date.today().addDays(-2), Status = Constants.CASE_STATUS_OPEN,
//                                    Street_Address__c = '301 Front St', City__c = 'Nome', State__c = 'AK', Zip_Code__c = '99762', Vendor_Code__c = 'CHIGRS',
//                                    Work_Ordered__c = 'Initial Grass Cut',Client_Name__c = accountsToInsert[0].Id));
//        
//        //insert case records
//        insert workOrders;
//	}
//	
//	//Test method to test UpdateVendorSummaryOnOrder method of helper class
//    static testMethod void testUpdateVendorSummaryOnOrder() {
//        
//        generateTestData();
//        
//        //List to hold Work Order Instructions test records
//        List<Work_Order_Instructions__c> woInstructions = new List<Work_Order_Instructions__c>();
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[1].Id, Instruction_Type__c = 'ACCESS UNKNOWN', 
//                Item_Instructions__c = 'Testing is going on.', Line__c = 8, Case__c = workOrders[0].id, Action__c ='Bid'));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[3].Id, Instruction_Type__c = 'KNOWN ISSUE', 
//                Item_Instructions__c = 'Testing is going on.', Line__c = 10, Case__c = workOrders[0].id, Action__c ='Bid'));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[3].Id, Instruction_Type__c = 'TESTED OK', Sort__c = 8,
//                Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id, Action__c ='Bid'));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[4].Id, Instruction_Type__c = 'READY WORK', 
//                Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id, Action__c ='Bid'));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[1].Id, Instruction_Type__c = 'KNOWN ISSUE', 
//                                                            Item_Instructions__c = 'Testing is going on.', Line__c = 10, Case__c = workOrders[0].id, Action__c ='Bid'));
//        //Test starts here
//        Test.startTest();
//        
//        //insert instructions here
//        insert woInstructions;
//        
//        //Query result of Work Order
//        workOrders = [SELECT Id, Vendor_Instructions_Summary__c FROM Case WHERE Id IN: workOrders];
//        
//		//assert statement
//		System.assert(workOrders[0].Vendor_Instructions_Summary__c != null);
//		
//		//update the HashKey on instruction
//		woInstructions[0].HashKey__c = 'Testing Hash';
//		update woInstructions[0];
//		
//		//Query result of Work Order
//        workOrders = [SELECT Id, Vendor_Instructions_Summary__c FROM Case WHERE Id IN: workOrders];
//        
//        //assert statement
//		System.assert(workOrders[0].Vendor_Instructions_Summary__c != null);
//		
//		delete woInstructions[1];
//		delete woInstructions[2];
//		
//		//Query result of Work Order
//        workOrders = [SELECT Id, Vendor_Instructions_Summary__c FROM Case WHERE Id IN: workOrders];
//        
//        //assert statement
//		System.assert(workOrders[0].Vendor_Instructions_Summary__c == null);
//		
//		//Test stops here
//        Test.stopTest();
//    }
//    
//    //Test method to test updateSortFromLine method of helper class
//    static testMethod void testUpdateSortFromLine() {
//        
//        generateTestData();
//        
//        //List to hold Work Order Instructions test records
//        List<Work_Order_Instructions__c> woInstructions = new List<Work_Order_Instructions__c>();
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[1].Id, Instruction_Type__c = 'ACCESS UNKNOWN', 
//                                                            Item_Instructions__c = 'Testing is going on.', Line__c = 8.10, Case__c = workOrders[0].id));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[3].Id, Instruction_Type__c = 'KNOWN ISSUE', 
//                                                            Item_Instructions__c = 'Testing is going on.', Line__c = 10, Case__c = workOrders[0].id));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[3].Id, Instruction_Type__c = 'TESTED OK', Sort__c = 8,
//                                                            Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[4].Id, Instruction_Type__c = 'READY WORK', 
//                                                            Item_Instructions__c = 'Testing is going on.', Case__c = workOrders[0].id));
//        woInstructions.add(new Work_Order_Instructions__c(RecordTypeId = recordTypes[1].Id, Instruction_Type__c = 'KNOWN ISSUE', 
//                                                            Item_Instructions__c = 'Testing is going on.', Line__c = 10, Case__c = workOrders[0].id));
//        //Test starts here
//        Test.startTest();
//        
//        //insert instructions here
//        insert woInstructions;
//    	
//    	//query result of Work Order Instructions
//    	woInstructions = [SELECT Sort__c FROM Work_Order_Instructions__c WHERE Id IN: woInstructions];
//    	
//    	//assert statements
//    	System.assertEquals(8.100, woInstructions[0].Sort__c);
//    	System.assertEquals(10.000, woInstructions[1].Sort__c);
//    	System.assertEquals(8.000, woInstructions[2].Sort__c);
//    	System.assertEquals(null, woInstructions[3].Sort__c);
//    	System.assertEquals(10.000, woInstructions[4].Sort__c);
//    	
//    	//Test starts here
//        Test.stopTest();
//    }
}