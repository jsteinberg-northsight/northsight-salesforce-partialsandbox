global without sharing class BatchRecheckGeocodes implements Database.Batchable<Geocode_cache__c>, Database.Stateful, Database.AllowsCallouts{
	private List<Geocode_cache__c> geocodes;
	
	// Set some parameters for the batch
	//  Are we updating by date? By error type?
	global BatchRecheckGeocodes(date startDate, string error_type) {
		// Assign filters
		
	}
	
	global Iterable<Geocode_cache__c> start(Database.BatchableContext BC) {
		//Step 1: Query list of geocodes based on input parameters
		// Todo: add filters
		geocodes = [select id, address_hash__c, Last_Geocoded__c, partial_match__c, Manual_location__c, formatted_address__c, location__latitude__s, location__longitude__s, geocode_status__c from geocode_cache__c ];
		
		// Return list
		return geocodes;
	}
	
	
	global void execute(Database.BatchableContext BC, List<Geocode_cache__c> geocodes) {
		// For up to 10 geocodes, try to re-code the address
		for(geocode_cache__c geo : geocodes) {
			// If status is the same, leave it alone
		
			// Otherwise, update content data
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		// Save geocodes
		upsert geocodes;
	}
}