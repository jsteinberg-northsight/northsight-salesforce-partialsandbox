/*
BASE_URL: http://nspruvanrelay.elasticbeanstalk.com/
BASE_URL sandbox: http://nsprelay-env.elasticbeanstalk.com/
*/
public without sharing class PruvanWorkOrderStatusController {
	//create response string
	private string errorMessage = '{"error":"The web service call has failed to hit the proper web service to initiate execution."}';
	private string response = errorMessage;
	
	public string getResponse(){
		//return response string
		return response;
	}

	public void execute(){
		string payload = ApexPages.currentPage().getParameters().get('payload');
		
		/*try{
			logPayload(payload);
		}catch(Exception e){
			response = '{"error":"'+e.getMessage()+' '+e.getStackTraceString()+'"}';
			return;
		}*/
		
		try{
			response = PruvanWebServices.doWorkOrderStatus(payload);
		}
		catch(Exception e){
			response = '{"error":"'+e.getMessage()+' '+e.getStackTraceString()+'"}';
		}
	}
	
	/*private void logPayload(string payload){
		//create a new workOrderStatus log & fill it with the request data
		Pruvan_WorkOrderStatus_Log__c pruvanLog = new Pruvan_WorkOrderStatus_Log__c();
		pruvanLog.Log_Data__c = payload;
		//insert the log
		insert pruvanLog;
	}*/
}