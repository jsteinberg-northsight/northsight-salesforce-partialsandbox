/*
This class is not a true Batchable class.
It has been altered due to Salesforce limitations.
*/
global without sharing class BatchCheckGeocoder {
	//this method will create a list of geocode process logs where the logs have been created in the last two hours
		//AND
	//the exceeded limit flag is false
	global void start(){
		//create a datetime variable of now minus two hours
		Datetime twoHoursAgo = Datetime.now();
		twoHoursAgo = twoHoursAgo.addHours(-2);
		//create a list of geocode process logs that meet the filter criteria
		List<Geocode_Process_Log__c> matchedLogs = [SELECT Id
													FROM Geocode_Process_Log__c
													WHERE Exceeded_Limit__c = false
													AND CreatedDate >=: twoHoursAgo
								  				   ];
		execute(matchedLogs);
	}
	
	//this method will be used to create a system alert email if there is a problem with the geocoder
	global void execute(List<Geocode_Process_Log__c> markedLogs) {
		//instantiate a system alert list and a system alert object
		List<System_Alerts__c> saList = new List<System_Alerts__c>();
		System_Alerts__c sa = new System_Alerts__c();
		//check if the markedLogs list is null or empty
		if(markedLogs.size() == 0){
			//build out the system alert and add it to the list
			sa.Subject_Line__c = 'Geocoder Alert';
			sa.Process_Name__c = 'Geocoder';
			sa.Body__c = 'No properties have been geocoded in the last two hours. Please check that the Geocoder is functioning properly.';
			
			saList.add(sa);
		}
		//if the list has a size > 0, insert the values
		if(saList.size() > 0){
			insert saList;
		}
	}
}