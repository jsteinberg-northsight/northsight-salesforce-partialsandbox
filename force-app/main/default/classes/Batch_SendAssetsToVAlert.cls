global class Batch_SendAssetsToVAlert implements Database.Batchable<SObject> {
	global Database.Querylocator start(Database.BatchableContext BC) {
		//return query result
		return Database.getQueryLocator([SELECT  Id,Name, Formatted_Address__c, VAlert_Id__c FROM Geocode_Cache__c Where VAlert_Id__c = null and Monitor_Snow__c != 'No update needed' ]);
	}
	global void execute(Database.BatchableContext BC, List<Geocode_Cache__c> props) {
   		System.Assert(props.size()<200);
   		if(!Test.isRunningTest()) {
   			WeatherAssetHelper.pushAssetsToVAlert(props);
   		}
    }
    
	global void finish(Database.BatchableContext BC) {

	}
}