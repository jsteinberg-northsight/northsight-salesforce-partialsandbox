global without sharing class BatchCommitmentUpdater implements Database.Batchable<Case>, Database.Stateful{
	private Map<string, Set<Id>> listMap;//will hold a set of case Id's associated with a string key
	
	//Padmesh Soni (06/19/2014) - Support Case Record Type
	//Variable to hold list of RecordTypes of Case object
	global List<RecordType> recordTypes;
	
	//Padmesh Soni (06/19/2014) - Support Case Record Type
	//Constructor definition
	global BatchCommitmentUpdater() {
		
		//Query record of Record Type of Case object
		recordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName IN: Constants.CASE_CUSTOMER_RECORDTYPE_DEVELOPERNAMES 
											AND SobjectType =: Constants.CASE_SOBJECTTYPE AND IsActive = true];
	}
	
	global Iterable<Case> start(Database.BatchableContext BC) {
		Date currentDay = Date.today();//date var = current day

		Datetime twoHours = Datetime.now().addHours(-2);//datetime var = current day & time minus 2 hours
		
		listMap = new Map<string, Set<Id>>();//this map will contain each one of the lists to be made from the queries
		
		Set<Id> casesToAvoid = new Set<Id>();//fill with case IDs from the first two queries to be used in the third query where ALL OTHER cases are selected so as to avoid re-selecting cases that have already been selected in the first two lists
		
		List<Case> returnValue = new List<Case>();//this will be the value that is returned to execute method
		
		Set<String> statuses = new Set<String>(new string[]{'Sent', 'Unsent', 'In Progress'});//a set of sync statuses to check against in the sub-selects for client commitments
		try{
			
			//select orders where due date is <= current date, approval status = 'Approved, last open verification datetime > twoHours, and client commit date is null or <= current date
			//Code modified - Adding filter "Client__c =: Constants.CASE_CLIENT_SG" into Case object query - Padmesh Soni(02/12/2014) 
			//As per "Padmesh Assignment - 2/10/2014 11th Hour Client Commitment Batch"
			//Padmesh Soni (06/19/2014) - Support Case Record Type
	    	//New filter criteria added for RecordTypeId
		    Map<Id, Case> approvedOrders = new Map<Id, Case>([SELECT Id, Updated_Date_Time__c, 
																	(Select Id From Client_Commitments__r Where Sync_Status__c IN: statuses 
																		And Commit_Date_Valid__c = true order by CreatedDate desc limit 1) 
																	FROM Case WHERE RecordTypeId NOT IN: recordTypes
																	AND Client__c =: Constants.CASE_CLIENT_SG 
																	AND Commit_By_Date__c <=: currentDay AND Approval_Status__c = 'Approved' 
																	AND Last_Open_Verification_DateTime__c >: twoHours 
																	AND (Client_Commit_Date__c = null OR Client_Commit_Date__c <=: currentDay)]);
			
			casesToAvoid.addAll(approvedOrders.keySet());//add the approvedOrders Id's to casesToAvoid
			
			if(approvedOrders.size() > 0){//if the map has objects in it...
				for(Case c : approvedOrders.values()){//loop over the cases...
					if(c.Client_Commitments__r == null || c.Client_Commitments__r.size() == 0){//check that there are no commitments
						returnValue.add(c);//if there are no commitments, we add the case to the returnValue list
					}
				}
			}
			//returnValue.addAll(approvedOrders.values());//add approvedOrders to returnValue
				
			listMap.put('Approved', approvedOrders.keySet());//place approvedOrders in listMap with key 'Approved'
			
			//select orders where due date is <= current date, approval status = 'Pending, last open verification = current date, and client commit date is null or <= current date
			//Code modified - Adding filter "Client__c =: Constants.CASE_CLIENT_SG" into Case object query - Padmesh Soni(02/12/2014) 
			//As per "Padmesh Assignment - 2/10/2014 11th Hour Client Commitment Batch"
			//Padmesh Soni (06/19/2014) - Support Case Record Type
	    	//New filter criteria added for RecordTypeId
		    Map<Id, Case> pendingOrders = new Map<Id, Case>([SELECT Id, Date_Serviced1__c, 
																(Select Id From Client_Commitments__r Where Sync_Status__c IN: statuses 
																	And Commit_Date_Valid__c = true order by CreatedDate desc limit 1) 
																FROM Case WHERE RecordTypeId NOT IN: recordTypes 
																AND Client__c =: Constants.CASE_CLIENT_SG AND Commit_By_Date__c <=: currentDay 
																AND Approval_Status__c = 'Pending' AND Special_Review__c = 'No' 
																AND Last_Open_Verification__c =: currentDay AND (Client_Commit_Date__c = null 
																OR Client_Commit_Date__c <=: currentDay)]);
			
			casesToAvoid.addAll(pendingOrders.keySet());//add the pendingOrders Id's to casesToAvoid
			
			if(pendingOrders.size() > 0){//if the map has objects in it...
				for(Case c : pendingOrders.values()){//loop over the cases...
					if(c.Client_Commitments__r == null || c.Client_Commitments__r.size() == 0){//check that there are no commitments
						returnValue.add(c);//if there are no commitments, we add the case to the returnValue list
					}
				}
			}
			//returnValue.addAll(pendingOrders.values());//add pendingOrders to returnValue
			
			listMap.put('Pending', pendingOrders.keySet());//place pendingOrders in listMap with key 'Pending'
			
			//select orders where the order IDs are not in casesToAvoid, due date is <= current date, approval status != 'Approved, last open verification = current date, and client commit date is null or <= current date
			//Code modified - Adding filter "Client__c =: Constants.CASE_CLIENT_SG" into Case object query - Padmesh Soni(02/12/2014)
			//As per "Padmesh Assignment - 2/10/2014 11th Hour Client Commitment Batch"
			//Padmesh Soni (06/19/2014) - Support Case Record Type
	    	//New filter criteria added for RecordTypeId
		    Map<Id, Case> allOtherOrders = new Map<Id, Case>([SELECT Id, 
																(Select Id From Client_Commitments__r Where Sync_Status__c IN: statuses 
																	And Commit_Date_Valid__c = true order by CreatedDate desc limit 1) 
																FROM Case WHERE RecordTypeId NOT IN: recordTypes
																AND Id NOT IN : casesToAvoid AND Client__c =: Constants.CASE_CLIENT_SG 
																AND Commit_By_Date__c <=: currentDay AND Approval_Status__c != 'Approved' 
																AND Last_Open_Verification__c =: currentDay AND (Client_Commit_Date__c = null 
																OR Client_Commit_Date__c <=: currentDay)]);
			
			if(allOtherOrders.size() > 0){//if the map has objects in it...
				for(Case c : allOtherOrders.values()){//loop over the cases...
					if(c.Client_Commitments__r == null || c.Client_Commitments__r.size() == 0){//check that there are no commitments
						returnValue.add(c);//if there are no commitments, we add the case to the returnValue list
					}
				}
			}
			//returnValue.addAll(allOtherOrders.values());//add allOtherOrders to returnValue
			
			listMap.put('Other', allOtherOrders.keySet());//place allOtherOrders in listMap with key 'Other'
			
			//System.debug('returnValue::::::'+ returnValue);
		} catch(Exception e){
			system.debug(e.getMessage() + '**********' + e.getStackTraceString());
		}
		
		return returnValue;//send returnValue to execute method
	}
	
	global void execute(Database.BatchableContext BC, List<Case> orders) {
		//Date oneDay = Date.today().addDays(1);//date var = current day plus 1
		Date twoDays = Date.today().addDays(2);//date var = current day plus 2
		
		List<Client_Commitment__c> ccList = new List<Client_Commitment__c>();//list to add client commitments to

		try{
			if(orders.size() > 0){//if we have orders...
				for(Case c : orders){//loop thru the orders...
					if(listMap.get('Approved') != null){//do approved logic here
						if(listMap.get('Approved').contains(c.Id)){
							Date dateOfDateTime = c.Updated_Date_Time__c.date();
							String formattedDate = string.valueOf(dateOfDateTime.month()) +'/'+ string.valueOf(dateOfDateTime.day()) +'/'+ string.valueOf(dateOfDateTime.year());
							Client_Commitment__c newClientCommit = new Client_Commitment__c(
								Work_Order__c = c.Id,
								Commitment_Date__c = twoDays,
								Root_Cause_of_Delay__c = 'Delay in Receiving Work',
								Current_Status_of_Order__c = 'Field Completed Pending Update',
								Public_Comments__c = 'Work order has been complete and was updated at ' + formattedDate + '.',
								Autogen__c = true
							);
							ccList.add(newClientCommit);
						}
					}
					if(listMap.get('Pending') != null){//do pending logic here
						if(listMap.get('Pending').contains(c.Id)){
							String formattedDate = string.valueOf(c.Date_Serviced1__c.month()) +'/'+ string.valueOf(c.Date_Serviced1__c.day()) +'/'+ string.valueOf(c.Date_Serviced1__c.year());
							Client_Commitment__c newClientCommit = new Client_Commitment__c(
								Work_Order__c = c.Id,
								Commitment_Date__c = twoDays,
								Root_Cause_of_Delay__c = 'Field Capacity',
								Current_Status_of_Order__c = 'Field Completed Pending Update',
								Public_Comments__c = 'This work order was completed on ' + formattedDate + ' and will be updated soon.',
								Autogen__c = true
							);
							ccList.add(newClientCommit);
						}
					}
					if(listMap.get('Other') != null){//do all other order logic here
						if(listMap.get('Other').contains(c.Id)){
							Client_Commitment__c newClientCommit = new Client_Commitment__c(
								Work_Order__c = c.Id,
								Commitment_Date__c = twoDays,
								Root_Cause_of_Delay__c = 'Field Capacity',
								Current_Status_of_Order__c = 'Pending Field Complete',
								Autogen__c = true
							);
							ccList.add(newClientCommit);
						}
					}
				}
			}
			
			if(ccList.size() > 0){
				insert ccList;
			}
		} catch(Exception e){
			system.debug(e.getMessage() + '**********' + e.getStackTraceString());
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		//section not used
	}
}