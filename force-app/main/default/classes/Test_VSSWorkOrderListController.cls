@isTest(seeAllData=false)
private class Test_VSSWorkOrderListController {
///**
// *  Purpose			:	This is used for testing and covering VSSWorkOrderListController.
// *
// *	Created By		:	Padmesh Soni
// *
// *	Created Date	:	04/01/2014
// *
// *	Current Version	:	V1.1
// *
// *	Revision Log	:	V1.0 - Created
// *						V1.1 - Modified - Padmesh Soni (06/20/2014) - Support Case Record Type
// * 			                                 Changes are: New condition and query filter added of new RecordType excluding of Case object to process.
// *	Coverage		:	99% - V1.0
// *						99% - V1.1
// **/    
//    //class variable
//    private static Profile newProfile;
//    private static List<User> users;
//    private static List<Account> accounts;
//    private static List<Contact> contacts;
//    
//    private static void generateTestData(integer countData){
//        
//        //--------------------------------------------------------
//        //PRE-TEST SETUP 
//        //create a new user with the Partner Community - Vendors OrderSelect profile
//        newProfile = [SELECT Id FROM Profile WHERE Name =: VSOAM_Constants.PROFILE_NAME_PARTNER_COMMUNITY_VENDORS];
//        
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            /******** PRE-TEST SETUP create a new account *********/
//            accounts.add(new Account(Name = 'Tester Account' + i,RecordTypeId='0124000000011Q2'));
//        }
//        
//        insert accounts;
//        
//        //Loop through countData time
//        for(integer i = 1; i <= countData; i++) {
//        
//            //--------------------------------------------------------
//            //PRE-TEST SETUP 
//            //create a new contact
//            Contact newContact = new Contact(LastName = 'Tester'+i, AccountId = accounts[i-1].Id, Pruvan_Username__c = 'NSBobTest', Status__c = '3 - Active', RecordTypeId = '01240000000UPSr',
//                                        Other_Address_Geocode__latitude__s = 32.09567, Other_Address_Geocode__longitude__s = -25.4532, 
//                                        Service_Range__c = 2000, Grass_Cut_Cap__c = 50,	MailingStreet = '30 Turnberry Dr',MailingCity = 'La Place',
//										MailingState = 'LA',MailingPostalCode = '70068');
//            								newContact.Maintenance_Cap__c = 10;//Maintenance type orders that can be assigned to this vendor at once. 
//								newContact.Grass_Cut_Cap__c = 10;//Legacy name ... this maps to orders of type "Routine"
//								newContact.Inspections_Cap__c = 10;//Inspections that can be assigned at once. 
//								newContact.Shrub_Trim__c = 3;//Pricing...required 
//								newContact.REO_Grass_Cut__c = 3;//Pricing
//								newContact.Pay_Terms__c = 'Net 30';//Terms required
//								newContact.Trip_Charge__c = 25;//Pricing
//								newContact.RecordTypeId = '01240000000UPSr';//Hard coded in a validation rule, so hard coded in this test. 
//								newContact.Date_Active__c = Date.Today();//Date Active must be set. 
//								newContact.Status__c = '3 - Active';//Status must be '3 - Active' due to a validation rule
//								newContact.MailingStreet = '30 Turnberry Dr';
//								newContact.MailingCity = 'La Place';
//								newContact.MailingState = 'LA';
//								newContact.MailingPostalCode = '70068';
//								newContact.OtherStreet = '30 Turnberry Dr';
//								newContact.OtherCity = 'La Place';
//								newContact.OtherState = 'LA';
//								newContact.OtherPostalCode = '70068';
//								newContact.Other_Address_Geocode__Latitude__s = 30.00;
//								newContact.Other_Address_Geocode__Longitude__s = -90.00;
//								contacts.add(newContact);
//        }   
//        
//        insert contacts;
//        
//        //Loop through countData time
//        for(integer i = 0; i < countData; i++) {
//        
//            //create a new user with the profile
//            users.add(new User(alias = 'standt'+i, contactId = contacts[i].Id, email = Math.random()+'@testorg.com', emailencodingkey = 'UTF-8',
//                                    lastname = 'Testing'+i, languagelocalekey = 'en_US', localesidkey = 'en_US', profileId = newProfile.Id,
//                                    timezonesidkey = 'Australia/Perth', username = 'NS_stdUserVSS'+i+'@test.com', isActive = true));
//        }
//        
//        insert users;
//    }
//    
//    //Test method is used to test functionality of doSearch method
//    static testMethod void testDoSearch() {
//        
//        //initialize variables
//        accounts = new List<Account>();
//        contacts = new List<Contact>();
//        users = new List<User>();
//        
//        generateTestData(1);
//        
//        //Query record of Record Type of Account and Case objects
//    	//Padmesh Soni (06/18/2014) - Support Case Record Type
//		//Query one more record type of Case Object "SUPPORT"    
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER 
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client'
//    										OR DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_SUPPORT) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(3, recordTypes.size());
//    	
//        //List of Account to store testing records
//        List<Account> accountsOnOrders = new List<Account>();
//        accountsOnOrders.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//        accountsOnOrders.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//        
//        insert accountsOnOrders;
//        
//        //List to hold Case records 
//        List<Case> wOrders = new List<Case>();
//        
//        wOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today(), Status = VSOAM_Constants.CASE_STATUS_OPEN, Approval_Status__c = VSOAM_Constants.CASE_APPROVAL_STATUS_REJECTED,
//                                    Client__c = VSOAM_Constants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = VSOAM_Constants.CASE_WORK_ORDER_TYPE_ROUTINE, 
//                                    State__c = VSOAM_Constants.CASE_STATE_TX, Scheduled_Date_Override__c = Date.today()));
//        wOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[0].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = VSOAM_Constants.CASE_STATUS_OPEN, Approval_Status__c = VSOAM_Constants.CASE_APPROVAL_STATUS_REJECTED,
//                                    Client__c = VSOAM_Constants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = VSOAM_Constants.CASE_WORK_ORDER_TYPE_ROUTINE, 
//                                    State__c = VSOAM_Constants.CASE_STATE_TX, Scheduled_Date_Override__c = Date.today().addDays(-6)));
//        wOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today(), Status = VSOAM_Constants.CASE_STATUS_OPEN, Approval_Status__c = VSOAM_Constants.CASE_APPROVAL_STATUS_REJECTED, 
//                                    Client__c = VSOAM_Constants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = VSOAM_Constants.CASE_WORK_ORDER_TYPE_MAINTENANCE, 
//                                    State__c = VSOAM_Constants.CASE_STATE_TX));
//        wOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = VSOAM_Constants.CASE_STATUS_OPEN, Approval_Status__c = VSOAM_Constants.CASE_APPROVAL_STATUS_REJECTED, 
//                                    Client__c = VSOAM_Constants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = VSOAM_Constants.CASE_WORK_ORDER_TYPE_MAINTENANCE, 
//                                    State__c = VSOAM_Constants.CASE_STATE_TX, Scheduled_Date_Override__c = Date.today().addDays(8)));
//        wOrders.add(new Case(RecordTypeId = recordTypes[2].Id, AccountId = accountsOnOrders[1].Id, 
//                                    Due_Date__c = Date.today().addDays(1), Status = VSOAM_Constants.CASE_STATUS_OPEN, Approval_Status__c = VSOAM_Constants.CASE_APPROVAL_STATUS_REJECTED, 
//                                    Client__c = VSOAM_Constants.CASE_CLIENT_NOT_SG, Work_Order_Type__c = VSOAM_Constants.CASE_WORK_ORDER_TYPE_MAINTENANCE, 
//                                    State__c = VSOAM_Constants.CASE_STATE_TX, Scheduled_Date_Override__c = Date.today().addDays(8)));
//	    								
//        //insert case records
//        insert wOrders;
//        
//        //Test start here
//        Test.startTest();
//        
//        //Query result of OS queues
//        Group osGroup = [SELECT Id, Name FROM Group WHERE Name =: VSOAM_Constants.CASE_QUEUE_NAME_OS AND Type =: VSOAM_Constants.GROUP_TYPE_QUEUE];
//        List<RecordType> accountRecordTypes = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Client'
//			AND SobjectType =: Constants.ACCOUNT_SOBJECTTYPE AND IsActive = true];
//										
//    	Account account =  new Account(Name = 'Tester Account',recordTypeId=accountRecordTypes[0].Id);
//		insert account; 
//		
//		
//        //Loop through case list
//        for(Case wo : wOrders) {
//        	wo.Client_Name__c = account.Id;    
//            wo.OwnerId = osGroup.Id;    
//        }
//        
//        //update the orders list 
//        update wOrders;
//        
//        //Query the Property records and update their latitude and loingitude
//        List<Geocode_Cache__c> properties = [SELECT Id, Location__Latitude__s, Location__Longitude__s FROM Geocode_Cache__c];
//        properties[0].Location__Latitude__s = 30.00000000951058; 
//        properties[0].Location__Longitude__s = -90.49931629999998;
//        properties[0].State__c = VSOAM_Constants.CASE_STATE_TX;
//        
//        //update property record
//        update properties[0];
//        
//        //Initialize page reference of VSSWorkOrderListController page 
//        PageReference p = Page.VSSWorkOrderListJSON;
//        
//        System.runAs(users[0]) {
//            
//            //Set the current page parameters
//            ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_MINLAT,'30.00000000951058');
//            ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_MAXLAT,'30.00000000951058');
//            ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_MINLON,'-90.49931629999998');
//            ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_MAXLON,'-90.49931629999998');
//            ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_CONTACT_LAT,'30.00000000951058');
//            ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_CONTACT_LON,'-90.49931629999998');
//            
//            //initialize the constructor
//            VSSWorkOrderListController aMSD = new VSSWorkOrderListController();
//            aMSD.doSearch();
//         
//            //assert statement
//            System.assert(aMSD.outputJSON != null);
//        }
//        
//        p = new PageReference('/apex/VSSWorkOrderListJSON');
//        
//        //reintialize the constructor
//        VSSWorkOrderListController vswj = new VSSWorkOrderListController();
//        
//        //Set the current page parameters
//        ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_MINLAT,'abc');
//        ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_MAXLAT,'cdf');
//        ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_MINLON,'efg');
//        ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_MAXLON,'hij');
//        ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_CONTACT_LON,'');
//        ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_CONTACT_LAT,'');
//        ApexPages.currentPage().getParameters().put(VSOAM_Constants.URL_PARAM_CONTACT_SERVICE_RANGE,'2000');
//            
//        try {
//            
//            //call doSearch of controller
//            vswj.doSearch();
//        } catch(Exception e) {
//            
//        }
//        
//        //assert statement
//        System.assert(vswj.outputJSON != null);
//    
//        //Test stops here 
//        Test.stopTest();
//    }
}