@isTest(seeAllData=false)
private class Test_ManualUpdateCommitController {
///**
// *	Purpose			:	This is used for testing and covering ManualUpdateCommitController functionality.
// *
// *	Created By		:	Padmesh Soni
// *
// *	Created Date	:	02/08/2014
// *
// *	Current Version	:	V1.0
// *
// *	Revision Log	:	V1.0 - Created  
// **/
//
//	//Test method is to test functionality of ManualUpdateCommitController
//    static testMethod void myUnitTest() {
//
//		//Query record of Record Type of Account and Case objects
//    	/*List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName =: Constants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER) AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];*/
//    	List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE (DeveloperName =: Constants.CASE_RECORDTYPE_DEVELOPERNAME_NEW_ORDER 
//    										OR DeveloperName = 'Client') AND (SobjectType =: Constants.CASE_SOBJECTTYPE 
//    										OR SobjectType =: Constants.ACCOUNT_SOBJECTTYPE) AND IsActive = true ORDER BY DeveloperName];
//    	
//    	//Assert statement
//    	System.assertEquals(2, recordTypes.size());
//    	
//    	//List of Account to store testing records
//    	List<Account> accounts = new List<Account>();
//    	accounts.add(new Account(Name = 'Test Account 1', Phone = '1234567890', RecordTypeId = recordTypes[0].Id));
//    	accounts.add(new Account(Name = 'Test Account 2', Phone = '1234567891', RecordTypeId = recordTypes[0].Id));
//    	
//    	insert accounts;
//		
//		//List to hold Case records
//    	List<Case> workOrders = new List<Case>();
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today(), 
//    								Status = Constants.CASE_STATUS_OPEN, Client__c = Constants.CASE_CLIENT_SG));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(1), 
//    								Status = Constants.CASE_STATUS_OPEN, ImageLinks_Summary__c = 'Test', Client__c = Constants.CASE_CLIENT_SG));
//    	workOrders.add(new Case(RecordTypeId = recordTypes[1].Id, AccountId = accounts[0].Id, Due_Date__c = Date.today().addDays(2), 
//    								Status = Constants.CASE_STATUS_OPEN, Client__c = Constants.CASE_CLIENT_SG));
//    	
//    	//insert case records
//    	insert workOrders;
//    	
//    	//List to hold Client Commitment records
//    	List<Client_Commitment__c> clientCommitments = new List<Client_Commitment__c>();
//    	clientCommitments.add(new Client_Commitment__c(Work_Order__c = workOrders[0].Id, Sync_Status__c = Constants.CLIENT_COMMITMENT_SYNC_STATUS_UNSENT, 
//    														Commitment_Date__c = Date.today(), Current_Status_of_Order__c = 'Not Started'));
//    	clientCommitments.add(new Client_Commitment__c(Work_Order__c = workOrders[1].Id, Sync_Status__c = Constants.CLIENT_COMMITMENT_SYNC_STATUS_UNSENT, 
//    														Commitment_Date__c = Date.today(), Current_Status_of_Order__c = 'Not Started'));
//    	clientCommitments.add(new Client_Commitment__c(Work_Order__c = workOrders[2].Id, Sync_Status__c = Constants.CLIENT_COMMITMENT_SYNC_STATUS_UNSENT, 
//    														Commitment_Date__c = Date.today(), Current_Status_of_Order__c = 'Not Started'));
//    	
//    	//insert commitments here 
//    	insert clientCommitments;
//    	
//    	//Test starts here
//    	Test.startTest();
//    	
//    	//Instance of PropertyWorkOrdersListController
//    	ManualUpdateCommitController controller = new ManualUpdateCommitController();
//    	
//    	//assert statements
//    	System.assertEquals(3, controller.wrapCommitments.size());
//    	
//    	//assign true value to isSelected variable
//    	controller.wrapCommitments[0].isSelected = true;
//    	controller.wrapCommitments[1].isSelected = true;
//    	
//    	//calling updateCommitment method
//    	controller.updateCommitment();
//    	
//    	clientCommitments = [SELECT Id FROM Client_Commitment__c WHERE Sync_Status__c =: Constants.CLIENT_COMMITMENT_SYNC_STATUS_UNSENT 
//							AND Work_Order__r.Client__c =: Constants.CASE_CLIENT_SG];
//		
//		controller = new ManualUpdateCommitController();
//		
//		//assert statements
//    	System.assertEquals(1, clientCommitments.size());
//		
//		//calling updateCommitment method
//    	controller.updateCommitment();
//    	
//    	System.assertEquals(1,controller.wrapCommitments.size());
//    	
//    	//assert statements
//    	System.assert(ApexPages.getMessages() != null);
//		
//		controller = new ManualUpdateCommitController();
//		
//		//assign true value to isSelected variable
//    	controller.wrapCommitments[0].isSelected = true;
//    	
//		//calling updateCommitment method
//    	controller.updateCommitment();
//    	
//    	clientCommitments = [SELECT Id FROM Client_Commitment__c WHERE Sync_Status__c =: Constants.CLIENT_COMMITMENT_SYNC_STATUS_UNSENT 
//							AND Work_Order__r.Client__c =: Constants.CASE_CLIENT_SG];
//		
//		//assert statements
//    	System.assertEquals(0, clientCommitments.size());
//		
//		//Test stops here
//    	Test.stopTest();							
//    }
}