(function(skuid){
skuid.snippet.register('LoadPhotos',function(args) {var params = arguments[0],
	$ = skuid.$;
skuid.$M("Child_ImageLinks_include").loadAllRemainingRecords({
   stepCallback: function(offsetStart,offsetEnd) {
   },
   finishCallback: function(totalRecordsRetrieved) {
   }
});
});
skuid.snippet.register('LoadRelations',function(args) {var params = arguments[0],
	$ = skuid.$;
skuid.$M("Child_ImageLinkRlt").loadAllRemainingRecords({
   stepCallback: function(offsetStart,offsetEnd) {
   },
   finishCallback: function(totalRecordsRetrieved) {
   }
});
});
}(window.skuid));