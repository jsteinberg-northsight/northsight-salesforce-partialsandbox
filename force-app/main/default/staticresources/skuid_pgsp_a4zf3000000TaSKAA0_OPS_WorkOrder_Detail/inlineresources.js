(function(skuid){
skuid.snippet.register('LoadPhotos',function(args) {var params = arguments[0],
	$ = skuid.$;
skuid.$M("Child_ImageLinks").loadAllRemainingRecords({
   stepCallback: function(offsetStart,offsetEnd) {
   },
   finishCallback: function(totalRecordsRetrieved) {
        var screenHeight = $(window).height() - $('.bPageHeader').height();
        $('.applyButton').css('display', 'none !important');
        $('div.nx-page-content').append('<div class="imageContainer" style="height:' + screenHeight + 'px; display: none"><div class="enlargedImage" style="height:100%;"></div><i class="fa fa-angle-left"></i><i class="fa fa-angle-right"></i><i class="fa fa-times"></i></div>');
   }
});
});
skuid.snippet.register('SelectPhotos',function(args) {var params = arguments[0],
	$ = skuid.$;
allImages = $('.picturecards .nx-item');

$('.addedElements').remove();
var divText = '<div class=addedElements>';
divText += '<div class="labels" style="margin-top: 2px"><input type="radio" name="label" value="N/A"> N/A</input><input type="radio" name="label" value="Before"> Before</input><input type="radio" name="label" value="During"> During</input><input type="radio" name="label" value="After"> After</input></div>';
divText += '<div class="disabled" style="margin-left: .6em">Disabled:<select class="disabledDropdown"><option>N/A</option><option value="Dark">Dark</option><option value="Blurry">Blurry</option><option value="Duplicate">Duplicate</option><option value="Unrelated">Unrelated</option><option value="Obscene">Obscene</option></select></div>';
divText += '<div class="room" style="margin-left: .6em">Room:<select><option>N/A</option><option value="General">General</option><option value="Exterior">Exterior</option><option value="Garage">Garage</option><option value="Foyer">Foyer</option><option value="Family RM">Family RM</option><option value="Dining RM">Dining RM</option><option value="Breakfast RM">Breakfast RM</option><option value="Kitchen">Kitchen</option><option value="Laundry RM">Laundry RM</option><option value="Formal Living RM">Formal Living RM</option><option value="Hallway">Hallway</option><option value="Master Bedroom">Master Bedroom</option><option value="Bedroom 1">Bedroom 1</option><option value="Bedroom 2">Bedroom 2</option><option value="Bedroom 3">Bedroom 3</option><option value="Bedroom 4">Bedroom 4</option><option value="1/2 Bath">1/2 Bath</option><option value="Master Bath">Master Bath</option><option value="Guest Bath 1">Guest Bath 1</option><option value="Guest Bath 2">Guest Bath 2</option><option value="Basement">Basement</option><option value="Other">Other</option></select></div>';
divText += '<div class="biditem" style="margin-left: .6em">Bid Item:<select><option>N/A</option><option>Appliances - Whirlpool4317824Dishwasher cord-3 prong Not Applicable</option><option>Cabinets - Counter Tops, Granite, Including Backsplash - Level 1/2</option></select></div>';
divText += '<div class="invoiceline" style="margin-left: .6em">Invoice Line:<select><option>N/A</option><option>Winterization</option></select></div>';
divText += '</div>';
$('.buttonrow').append(divText);
$('.applyButton').show();

$('.rightTopGrid').parent().css('flex 1 0 auto');

$('.picturecards .nx-item').click(function (e) {
    $(this).toggleClass('selected');
    var curr = $('.nx-item').index(this);

    if (e.shiftKey && prev > -1) {
        $('.nx-item').slice(Math.min(prev, curr), 1 + Math.max(prev, curr)).addClass('selected');
        prev = -1;
    } else {
        prev = curr;
    }
});

$('.picturecards .nx-item').dblclick(function () {
    currentIndex = $(allImages).index($(this));
    DisplayImage();
});

$('.enlargedImage').click(function () {
    $('.enlargedImage').html('');
    $('.imageContainer').hide();
});

$('.fa-times').click(function() {
    $('.enlargedImage').html('');
    $('.imageContainer').hide();
});

$('.fa-angle-left').click(function() {
    DecrementCurrentIndex();
    DisplayImage();
});

$('.fa-angle-right').click(function() {
    IncrementCurrentIndex();
    DisplayImage();
});

$(window).keydown(function (e) {
    if (e.which == 39) { // right
        IncrementCurrentIndex();
        DisplayImage();
    }
    else if (e.which == 37) { // left
        DecrementCurrentIndex();
        DisplayImage();
    }
});

function IncrementCurrentIndex() {
    currentIndex = currentIndex+1 < (allImages.length - 1) ? currentIndex+1 : (allImages.length - 1);
}

function DecrementCurrentIndex() {
    currentIndex = currentIndex-1 <= 0 ? 0 : currentIndex-1;
}

function DisplayImage() {
    var img = $(allImages[currentIndex]).find('img');

    var newImg = $(allImages[currentIndex]).find('img').clone();
        
    var maxWidth = $('.imageContainer').width() * 0.95; // Max width for the image
    var maxHeight = $('.imageContainer').height() * 0.9;    // Max height for the image
    var width = $(img).width();    // Current image width
    var height = $(img).height();  // Current image height

    var ratio = Math.min(maxWidth / width, maxHeight / height);
    newImg.height(height*ratio);
    newImg.width(width*ratio);

    $('.enlargedImage').html('').append(newImg);
    $('.imageContainer').show();
}

var buttonRowTop = $('.buttonrow').offset().top;       // get initial position of the element
        
$(window).scroll(function() {                  // assign scroll event listener

    var currentScroll = $(window).scrollTop(); // get current position

    if (currentScroll > buttonRowTop) // apply position: fixed
        $('.buttonrow').addClass('sticky');
    else // apply position: static
        $('.buttonrow').removeClass('sticky');

});
});
skuid.snippet.register('GetSelectedPhotos',function(args) {var params = arguments[0],
	$ = skuid.$;
var rowsToUpdate = {};

var myModel = skuid.model.getModel('Child_ImageLinks');
$('.applyButton').hide();

$('.selected').each(function(i, o){
    var updateStringArray = [];
    if ($('.disabledDropdown').val() !== undefined && $('.disabledDropdown').val() !== 'N/A') {
        updateStringArray.push("Disable_Photos__c: true");
        updateStringArray.push("Disabled_Reason__c: " + $('.disabledDropdown').val());
    }
    var selected = $("input[name='label']:checked");
    if (selected.length > 0 && selected.val() !== 'N/A')
        updateStringArray.push("Image_Label__c: " + selected.val());
    if (updateStringArray.length > 0) {
        var obj = {};
        for (var j = 0; j < updateStringArray.length; j++) {
            var split = updateStringArray[j].split(':');
            obj[split[0].trim()] = split[1].trim();
        }
        rowsToUpdate[$(o).find('.imageLinkId').val()] = obj;
    }
    //var myComponent = myModel.getRowById($(o).find('.imageLinkId').val());
    //myModel.updateRow(myComponent,{Image_Label__c: "Before"});
});

$('.addedElements').remove();
if (rowsToUpdate != {}){
    myModel.updateRows(rowsToUpdate);
    myModel.save({callback: function(result){
        if (result.totalsuccess) {
            $('.selected').removeClass('selected');
        } else {
            alert('Save failed, please try again.');
        }
    }});
}
});
skuid.snippet.register('LockTabs',function(args) {var params = arguments[0],
	$ = skuid.$;
$('.applyButton').css('display', 'none !important');
var elementTop = $('.ui-tabs-nav').offset().top; // get initial position of the element
var screenBottom = $('.footerLink').offset().top; // get bottom of the screen to prevent infinite scroll

$(window).scroll(windowScroll);
$(window).on('wheel', windowScroll);

function windowScroll(){
    var currentScroll = $(window).scrollTop();
    if (currentScroll > elementTop && (currentScroll - 300) + $(window).height() < screenBottom) {
        $('.ui-tabs-nav').css('margin-top', currentScroll).height('300px');
        $('.tabBar').css('display', 'flex');
    }
    else {
        $('.ui-tabs-nav').css('margin-top', '');
        $('.tabBar').css('display', 'inline-block');
    }
}
});
}(window.skuid));