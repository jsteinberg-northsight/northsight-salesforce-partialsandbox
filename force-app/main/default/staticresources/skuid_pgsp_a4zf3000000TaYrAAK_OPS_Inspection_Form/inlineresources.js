(function(skuid){
skuid.snippet.register('Print',function(args) {var params = arguments[0],
	$ = skuid.$;
window.print();

window.open('', '_self').close();
});
skuid.snippet.register('LoadPhotos',function(args) {var params = arguments[0],
	$ = skuid.$;
skuid.$M("Inspection_Photos").loadAllRemainingRecords({
   stepCallback: function(offsetStart,offsetEnd) {
   },
   finishCallback: function(totalRecordsRetrieved) {
      skuid.$.blockUI({ message: 'Finished loading all ' + totalRecordsRetrieved + ' Photos!', timeout: 2000 });
 }
});
});
}(window.skuid));