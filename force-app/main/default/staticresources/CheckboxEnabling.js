function isHighlight(sStatus) {

	var indx = sStatus.id.replace('page:frm:pb:pbt:','').replace(':isChecked', '');
	var intVar = parseInt(indx);
	if(sStatus.checked) {
		document.getElementById('page:frm:pb:pbt').rows[intVar+1].style.backgroundColor="yellow";
	} else {
		document.getElementById('page:frm:pb:pbt').rows[intVar+1].style.backgroundColor="";
	}
}
function checkAll(cb,cbid) {

	var indx = document.getElementById('page:frm:pb:pbt').rows.length;
	for(var i=0; i < indx-1; i++) { 

		document.getElementById('page:frm:pb:pbt:'+i+':'+cbid).checked = cb.checked;
		if(cb.checked) {
			document.getElementById('page:frm:pb:pbt').rows[i+1].style.backgroundColor="yellow";
		} else {
			document.getElementById('page:frm:pb:pbt').rows[i+1].style.backgroundColor="";
		}
	}
}