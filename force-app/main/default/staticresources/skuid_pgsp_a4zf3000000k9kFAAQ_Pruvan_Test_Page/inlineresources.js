(function(skuid){
skuid.snippet.register('newSnippet',function(args) {// Copyright Pruvan 2012-2014

$(document).ready(
      
   function() {
	   
	  var loginMotd;

      // Make the menu invisible for login
      $("#menu").attr("class", "hide");
      
      // Populate the loginMotd
      try {
         $('#motd').html(jQuery.parseJSON($('#loginMotd').text()));
         if($('#motd').not(':empty')) {
            $('#motd').removeClass('hidden'); 
         }
      } catch (e) {         
         null;
      }
      
      $( "#submit" ).button().click(function() { 

         var args = new Object();
         args.username = $('#username').val();
         args.password = $('#password').val();
         args.redirect = $('#redirect').val();

         jargs = JSON.stringify(args);

         $.ajax({
            type : 'POST',
             url : window.location.href,
        dataType : 'json',
            data : {
                    payload : jargs
                   },
         success : function(data){
                     if (data.redirect) {
                        window.location.href = data.redirect;
                     } else if (data.message != "") {
                        $('#result').text(data.message).removeClass('hidden').fadeIn('fast').delay(8000).fadeOut('slow');
                        /* Curretly not doing login CSRF
                        $.ajax({
                          type      : 'GET',
                          url       : window.location.href,
                          dataType  : "text",
                          data      :  { operation : "getCsrfToken" },
                          success   : function(data) {
                             $("body").bind("ajaxSend", function(event, jqXhr, ajaxOptions) {
                                 jqXhr.setRequestHeader('X-CSRF-Token', data);
                              });
                          },
                          error     : function(XMLHttpRequest, textStatus, errorThrown) {
                             location.reload(true);
                          }
                        });
                        */
                     }
                   },
           error : function(XMLHttpRequest, textStatus, errorThrown) {
                     $('#result').text("Error: " + textStatus).removeClass('hidden').fadeIn('fast').delay(8000).fadeOut('slow');
                   }
         });

         return false;
      });
      
      $('#facebook, #google').click(function() {      	
      	event.preventDefault();
      	
         var redirect = $('#redirect').val();
         var addParam = (redirect) ? '&redirect=' + redirect : '';

         // refresh login page with chosen OAuth provider
      	window.location.search = "?provider=" + this.value + addParam;
      });
      
      var params = $.getUrlVars();
      if (params['msg'] != undefined) {
      	var msg = params['msg']; 
      	var hashIndex = msg.lastIndexOf('#') || msg.length;
      	var result = msg.slice(0, hashIndex); // remove appended hash, if any
      	$('#result').text(decodeURIComponent(result)).removeClass('hidden').fadeIn('fast').delay(8000).fadeOut('slow');
      	
      	// remove error from the end of url
      	window.history.replaceState( {} , "", window.location.origin + window.location.pathname);
      }
      
   } // document ready function end
   
); // document ready end
});
}(window.skuid));