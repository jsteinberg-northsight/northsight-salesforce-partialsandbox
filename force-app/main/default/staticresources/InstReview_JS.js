(function( $ ) {
	$.widget( "custom.combobox", {
		_create: function() {
			
			console.log(this.element);
			this.wrapper = $( "<span>" )
			.addClass( "custom-combobox" )
			.insertAfter( this.element );
 
			this.element.hide();
			this._createAutocomplete();
			this._createShowAllButton();
		},
 
		_createAutocomplete: function() {
			var selected = this.element.children( ":selected" ),
			value = selected.val() ? selected.text() : "";
 			
 			this.input = $( "<input>" )
				.appendTo( this.wrapper )
				.val( value )
				.attr( "title", "" )
				.addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
				.autocomplete({
				    delay: 0,
				    minLength: 0,
				    source: $.proxy( this, "_source" )
				})
				.tooltip({
			    	tooltipClass: "ui-state-highlight"
				});
 
			this._on( this.input, {
				autocompleteselect: function( event, ui ) {
					ui.item.option.selected = true;
					this._trigger( "select", event, {
						item: ui.item.option
					});
				},
 
				autocompletechange: "_removeIfInvalid"
			});
		},
 
		_createShowAllButton: function() {
			var input = this.input,
			wasOpen = false;
 
			$( "<a>" )
			.attr( "tabIndex", -1 )
			.attr( "title", "Show All Items" )
			.tooltip()
			.appendTo( this.wrapper )
			.button({
	            icons: {
					primary: "ui-icon-triangle-1-s"
	            },
            	text: false
			})
			.removeClass( "ui-corner-all" )
			.addClass( "custom-combobox-toggle ui-corner-right" )
			.mousedown(function() {
				wasOpen = input.autocomplete( "widget" ).is( ":visible" );
			})
			.click(function() {
				input.focus();
 
				// Close if already visible
				if ( wasOpen ) {
					return;
				}
 
				// Pass empty string as value to search for, displaying all results
				input.autocomplete( "search", "" );
			});
		},
 
		_source: function( request, response ) {
			
			var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
			response( this.element.children( "option" ).map(function() {
				var text = $( this ).text();
				if ( this.value && ( !request.term || matcher.test(text) ) )
					return {
						label: text,
						value: text,
						option: this
					};
				})
				 
			);
		},
 
		_removeIfInvalid: function( event, ui ) {
 
			// Selected an item, nothing to do
			if ( ui.item ) {
				return;
			}
 
			// Search for a match (case-insensitive)
        	var value = this.input.val(),
			valueLowerCase = value.toLowerCase(),
			valid = false;
			this.element.children( "option" ).each(function() {
				if ( $( this ).text().toLowerCase() === valueLowerCase ) {
					this.selected = valid = true;
					return false;
				}
			});
 
			// Found a match, nothing to do
			if ( valid ) {
				return;
			}
 
			// Remove invalid value
			this.input
			.val( "" )
			.attr( "title", value + " didn't match any item" )
			.tooltip( "open" );
			this.element.val( "" );
			this._delay(function() {
				this.input.tooltip( "close" ).attr( "title", "" );
			}, 2500 );
			this.input.autocomplete( "instance" ).term = "";
		},
 
		_destroy: function() {
			this.wrapper.remove();
			this.element.show();
		}
	});
})( jQuery );

function selectAll(selectWOI) {
	
	var cbId = selectWOI.id;
	if(cbId == 'pg:frm:pb:pbs:pbtClient:isAllClient') {
		
		var clientInstTable = document.getElementById('pg:frm:pb:pbs:pbtClient');

		var rows = clientInstTable.rows;
	
		for(var i=0; i < rows.length-1 ; i++) {
	
			var selId = 'pg:frm:pb:pbs:pbtClient:'+i+':isClient';
			var selBox = document.getElementById(selId);
			
			var cBCpyId = 'pg:frm:pb:pbs:pbtClient:'+i+':isAlreadyCopied';
			var isCopyiedAttr = document.getElementById(cBCpyId);
			
			if(!isCopyiedAttr.checked) {
				selBox.checked = selectWOI.checked;
				
				if(selectWOI.checked) {
					document.getElementById('pg:frm:pb:pbs:pbtClient').rows[i+1].style.backgroundColor="#F6FC83";
				} else {
					document.getElementById('pg:frm:pb:pbs:pbtClient').rows[i+1].style.backgroundColor="";
				}
			}
		}
	
	} else if(cbId == 'pg:frm:pb:pbs:pbtVendor:isAllVendor') {
		
		var vendorInstTable = document.getElementById('pg:frm:pb:pbs:pbtVendor');

		var rows = vendorInstTable.rows;
	
		for(var i=0; i < rows.length-1 ; i++) {
	
			var selId = 'pg:frm:pb:pbs:pbtVendor:'+i+':isVendor';
			var selBox = document.getElementById(selId);
			selBox.checked = selectWOI.checked;
			
			if(selectWOI.checked) {
				document.getElementById('pg:frm:pb:pbs:pbtVendor').rows[i+1].style.backgroundColor="#F6FC83";
			} else {
				document.getElementById('pg:frm:pb:pbs:pbtVendor').rows[i+1].style.backgroundColor="";
			}
		}
	} else if(cbId == 'pg:frm:pb:pbs:pbtVendor2:isAllVendor2') {
		
		var vendorInstTable = document.getElementById('pg:frm:pb:pbs:pbtVendor2');

		var rows = vendorInstTable.rows;
	
		for(var i=0; i < rows.length-1 ; i++) {
	
			var selId = 'pg:frm:pb:pbs:pbtVendor2:'+i+':isVendor2';
			var selBox = document.getElementById(selId);
			selBox.checked = selectWOI.checked;
			
			var currentRow = rows[i+1];
			var currentCells = currentRow.cells;
			var currentCellsCount = currentCells.length;
			
			for(var k=0; k < currentCellsCount; k++) {
			
				if(selectWOI.checked) {
					
					currentCells[k].style.backgroundColor="#F6FC83";
				} else {
					currentCells[k].style.backgroundColor="#E0E0E0";
				}
			}
		}
	}
}

function toggle(div_id) {
	var el = document.getElementById(div_id);
	if ( el.style.display == 'none' ) { el.style.display = 'block';}
	else {

	el.style.display = 'none';
	isDueDate=false;
	isScheduleDate=false;
	isOpenedDate=false;
	isAssignedDate=false;
	}
}
function blanket_size(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
	viewportheight = window.innerHeight;
	} else {
	viewportheight = document.documentElement.clientHeight;
	}
	if ((viewportheight > document.body.parentNode.scrollHeight) && (viewportheight > document.body.parentNode.clientHeight)) {
	blanket_height = viewportheight;
	} else {
	if (document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight) {
		blanket_height = document.body.parentNode.clientHeight;
	} else {
		blanket_height = document.body.parentNode.scrollHeight;
	}
	}
	var blanket = document.getElementById('blanket');
	blanket.style.height = blanket_height + 'px';
	var popUpDiv = document.getElementById(popUpDivVar);
	popUpDiv_height=blanket_height/2-150;//150 is half popup's height
}

function window_pos(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		viewportwidth = window.innerHeight;
	} else {
		viewportwidth = document.documentElement.clientHeight;
	}
	if ((viewportwidth > document.body.parentNode.scrollWidth) && (viewportwidth > document.body.parentNode.clientWidth)) {
		window_width = viewportwidth;
	} else {
		if (document.body.parentNode.clientWidth > document.body.parentNode.scrollWidth) {
			window_width = document.body.parentNode.clientWidth;
		} else {
			window_width = document.body.parentNode.scrollWidth;
		}
	}
	var popUpDiv = document.getElementById(popUpDivVar);
	window_width=window_width/2-150;//150 is half popup's width
}

function popup(windowname) {
	
	blanket_size(windowname);
	window_pos(windowname);
	toggle('blanket');
	toggle(windowname);
	var el = document.getElementById("overlay");
    if(el.style.visibility == "visible") 
		el.style.visibility = "hidden";
	
	if(windowname == 'addVendorPopup') {
		var instType = document.getElementById('pg:frm:pb:combobox3');
		instType.value = '';
		var instItem = document.getElementById('pg:frm:pb:vendorItemInst');
		instItem.value = '';
	}
	if(windowname == 'addPopup') {
		
		var instType = document.getElementById('pg:frm:pb:combobox2');
		instType.value = '';
		var instItem = document.getElementById('pg:frm:pb:clientItemInst');
		instItem.value = '';
	}
}

function copyConfirmation(instId) {
	var clientIds = instId;
	copyClientToVendor(clientIds);
}

function editConfirmation(instId) {
	var vendorIds = instId;
	editVendor(vendorIds);
}

function editClientConfirmation(instId) {
	var editClientIds = instId;
	editClient(editClientIds);
}

function userConfirmation() {
	
	var clientTable = document.getElementById('pg:frm:pb:pbs:pbtClient');
	var clientRow = clientTable.rows;
	var isAnySelect = false;
	var clientIds = '';
	
	for(var i=0; i < clientRow.length-1 ; i++) {
		
		var cBId = 'pg:frm:pb:pbs:pbtClient:'+i+':isClient';
		var cBCpyId = 'pg:frm:pb:pbs:pbtClient:'+i+':isAlreadyCopied';
		var isSelectedAttr = document.getElementById(cBId);
		var isCopyiedAttr = document.getElementById(cBCpyId);
		
		if(isSelectedAttr.checked && !isCopyiedAttr.checked) {
			
			var clientId = 'pg:frm:pb:pbs:pbtClient:'+i+':clientId';
			clientIds += document.getElementById(clientId).value + ',';
			
			isAnySelect = true;
		}
	}
	
	if(isAnySelect == false) {
		var errorMsg = document.getElementById('paramId');
        errorMsg.innerHTML = multipleCopyError;
        var el = document.getElementById("overlay");
        el.style.visibility = "visible";
	} else {
		copyClientToVendor(clientIds);
	}
}

function hightlightText() {
	
	var copyTable = document.getElementById('editTable1');
	var copyRows = copyTable.rows;
	for(var k=0; k < copyRows.length ; k++) {
	
		var copyInstTextId = 'pg:frm:pb:repeatCopy:' + k + ':copyInstText';
		var copyInstTxtField = document.getElementById(copyInstTextId);
		var cpyInstTypeId = 'pg:frm:pb:repeatCopy:' + k + ':cpyInstType';
		var cpyInstTypeField = document.getElementById(cpyInstTypeId);
		
		var scrlHeight = copyInstTxtField.scrollHeight;
		copyInstTxtField.style.height = scrlHeight + 5 +'px';
		
		var scrlHeight1 = cpyInstTypeField.scrollHeight;
		cpyInstTypeField.style.height = scrlHeight1 +'px';
	}
	
	for(var k=0; k < copyRows.length ; k++) {
	
		var copyInstTextId = 'pg:frm:pb:repeatCopy:' + k + ':copyInstText';
		var warnImageId = 'pg:frm:pb:repeatCopy:' + k + ':warnImage';
		var copyInstTxtField = document.getElementById(copyInstTextId);
		var copyInstText = copyInstTxtField.value;
		var warnImageField = document.getElementById(warnImageId);
		var phoneno1 = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		var phoneno2 = /^\d{10}$/;
		var phoneno3 = /^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$/;
		var phoneno4 = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/;
		var phoneno5 = /^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
		var phoneno6 = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
		var email1 = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
		if(email1.test(copyInstText) || phoneno1.test(copyInstText) || phoneno2.test(copyInstText) || phoneno3.test(copyInstText) 
			|| phoneno4.test(copyInstText) || phoneno5.test(copyInstText) || copyInstText.indexOf('$') > -1 
			|| copyInstText.toLowerCase().indexOf('contact') > -1  || copyInstText.toLowerCase().indexOf('email') > -1  
			|| copyInstText.toLowerCase().indexOf('telephone') > -1 || copyInstText.toLowerCase().indexOf('phone') > -1  
			|| copyInstText.toLowerCase().indexOf('call') > -1 ) {
				
			copyInstTxtField.style.backgroundColor = '#F78181';
			warnImageField.style.display = 'block';
			continue;
		} else {
			
			var copyInstTextArray = copyInstText.split(' ');
			for(var f=0; f<copyInstTextArray.length; f++) {
			
				if(phoneno1.test(copyInstTextArray[f]) || phoneno2.test(copyInstTextArray[f]) || phoneno3.test(copyInstTextArray[f]) || 
					phoneno4.test(copyInstTextArray[f]) || phoneno5.test(copyInstTextArray[f])) {
					
					copyInstTxtField.style.backgroundColor = '#F78181';
					warnImageField.style.display = 'block';
					break;
				}
			}
		}
	}
}

function openCopyPopup(listSize) {
	
	if(listSize > 0) {
		popup('copyAllPopup'); 
		hightlightText();
	}
}

function openEditVendorPopup(listSize) {
	
	if(listSize > 0) {
		popup('editAllPopup');
		adjustEditTable('editTable2');
	}
}

function openEditClientPopup(listSize) {
	
	if(listSize > 0) {
		popup('editAllClientPopup');
		adjustEditTable('editTable3');
	}
}

function adjustEditTable(tableName) {
	
	if(tableName == 'editTable2') {
		var editTable = document.getElementById('editTable2');
		var editTableRow = editTable.rows;
		for(var k=0; k < editTableRow.length ; k++) {
			var editInstTextId = 'pg:frm:pb:repeatEdit:' + k + ':editInstText';
			var editInstTypeId = 'pg:frm:pb:repeatEdit:' + k + ':editInstType';
			var editInstTextField = document.getElementById(editInstTextId);
			var editInstTypeField = document.getElementById(editInstTypeId);
			var scrlHeight = editInstTextField.scrollHeight;
			editInstTextField.style.height = scrlHeight + 5 +'px';
			var scrlHeight1 = editInstTypeField.scrollHeight;
			editInstTypeField.style.height = scrlHeight1 +'px';
		}
	} 
	if(tableName == 'editTable3') {
		var editTable = document.getElementById('editTable3');
		var editTableRow = editTable.rows;
		for(var k=0; k < editTableRow.length ; k++) {
			var editInstTextId = 'pg:frm:pb:repeatEdit1:' + k + ':editClientInstText';
			var editInstTypeId = 'pg:frm:pb:repeatEdit1:' + k + ':editClientInstType';
			var editInstTextField = document.getElementById(editInstTextId);
			var editInstTypeField = document.getElementById(editInstTypeId);
			var scrlHeight = editInstTextField.scrollHeight;
			editInstTextField.style.height = scrlHeight + 5 +'px';
			var scrlHeight1 = editInstTypeField.scrollHeight;
			editInstTypeField.style.height = scrlHeight1 +'px';
		}
	}
}
function userConfirmationForEdit(vendorType) {
	
	if(vendorType == 'hidden') {
		
		var vendorTable = document.getElementById('pg:frm:pb:pbs:pbtVendor2');
		var vendorRow = vendorTable.rows;
		var vendorIds = '';
		var isAnySelect = false;
		
		for(var i=0; i < vendorRow.length-1 ; i++) {
			
			var cBId = 'pg:frm:pb:pbs:pbtVendor2:'+i+':isVendor2';
			var isSelectedAttr = document.getElementById(cBId);
			
			if(isSelectedAttr.checked) {
				
				var vendorId = 'pg:frm:pb:pbs:pbtVendor2:'+i+':vendorId2';
				vendorIds += document.getElementById(vendorId).value + ',';
				isAnySelect = true;
			}
		}
		
		if(isAnySelect == false) {
			var errorMsg = document.getElementById('paramId');
	        errorMsg.innerHTML = 'Please select atleast one Hidden Vendor Instruction.';
	        var el = document.getElementById("overlay");
	        el.style.visibility = "visible";
		} else {
			
			editVendor(vendorIds);
		}
	} else if(vendorType == 'visible'){
	
		var vendorTable = document.getElementById('pg:frm:pb:pbs:pbtVendor');
		var vendorRow = vendorTable.rows;
		var vendorIds = '';
		var isAnySelect = false;
		
		for(var i=0; i < vendorRow.length-1 ; i++) {
			
			var cBId = 'pg:frm:pb:pbs:pbtVendor:'+i+':isVendor';
			var isSelectedAttr = document.getElementById(cBId);
			
			if(isSelectedAttr.checked) {
				
				var vendorId = 'pg:frm:pb:pbs:pbtVendor:'+i+':vendorId';
				vendorIds += document.getElementById(vendorId).value + ',';
				isAnySelect = true;
			}
		}
		if(isAnySelect == false) {
			var errorMsg = document.getElementById('paramId');
	        errorMsg.innerHTML = 'Please select atleast one visible Vendor Instruction.';
	        var el = document.getElementById("overlay");
	        el.style.visibility = "visible";
		} else {
			
			editVendor(vendorIds);
		}
	}
}

function hideAllInstruction() {
	
	var vendorTable = document.getElementById('pg:frm:pb:pbs:pbtVendor');
	var vendorRow = vendorTable.rows;
	var vendorIds = '';
	var isAnySelect = false;
	
	for(var i=0; i < vendorRow.length-1 ; i++) {
		
		var cBId = 'pg:frm:pb:pbs:pbtVendor:'+i+':isVendor';
		var isSelectedAttr = document.getElementById(cBId);
		
		if(isSelectedAttr.checked) {
			
			var vendorId = 'pg:frm:pb:pbs:pbtVendor:'+i+':vendorId';
			vendorIds += document.getElementById(vendorId).value + ',';
			isAnySelect = true;
		}
	}
	
	if(isAnySelect == false) {
		var errorMsg = document.getElementById('paramId');
        errorMsg.innerHTML = 'Please select atleast one Vendor Instruction to hide';
        var el = document.getElementById("overlay");
        el.style.visibility = "visible";
	} else {
		
		swapInstruction(vendorIds, false);
	}
}
function unhideAllInstruction() {
	
	var vendorTable = document.getElementById('pg:frm:pb:pbs:pbtVendor2');
	var vendorRow = vendorTable.rows;
	var vendorIds = '';
	var isAnySelect = false;
	
	for(var i=0; i < vendorRow.length-1 ; i++) {
		
		var cBId = 'pg:frm:pb:pbs:pbtVendor2:'+i+':isVendor2';
		var isSelectedAttr = document.getElementById(cBId);
		
		if(isSelectedAttr.checked) {
			
			var vendorId = 'pg:frm:pb:pbs:pbtVendor2:'+i+':vendorId2';
			vendorIds += document.getElementById(vendorId).value + ',';
			isAnySelect = true;
		}
	}
	
	if(isAnySelect == false) {
		var errorMsg = document.getElementById('paramId');
        errorMsg.innerHTML = 'Please select atleast one Vendor Instruction to unhide.';
        var el = document.getElementById("overlay");
        el.style.visibility = "visible";
	} else {
		
		swapInstruction(vendorIds, true);
	}
}
function enableVendorButtons() {
	
	var vendorTable = document.getElementById('pg:frm:pb:pbs:pbtVendor');
	var vendorRow = vendorTable.rows;
	var vendorIds = '';
	var isAnySelect = false;
	
	for(var i=0; i < vendorRow.length-1 ; i++) {
		
		var cBId = 'pg:frm:pb:pbs:pbtVendor:'+i+':isVendor';
		var isSelectedAttr = document.getElementById(cBId);
		
		if(isSelectedAttr.checked) {
			
			isAnySelect = true;
			break;
		}
	}
	
	var hideBtn = document.getElementById('pg:frm:pb:pbs:hideAll');
	var editBtn = document.getElementById('pg:frm:pb:pbs:editAll');
	
	if(isAnySelect == false) {
		hideBtn.disabled = true;
		hideBtn.className = 'btnDisabled';
		editBtn.disabled = true;
		editBtn.className = 'btnDisabled';
	} else {
		hideBtn.disabled = false;
		hideBtn.className = 'btn';
		editBtn.disabled = false;
		editBtn.className = 'btn';
	}
	
	adjustRowsColor();
}
function enableUnhideButton() {
	
	var vendorTable = document.getElementById('pg:frm:pb:pbs:pbtVendor2');
	var vendorRow = vendorTable.rows;
	var vendorIds = '';
	var isAnySelect = false;
	
	for(var i=0; i < vendorRow.length-1 ; i++) {
		
		var cBId = 'pg:frm:pb:pbs:pbtVendor2:'+i+':isVendor2';
		var isSelectedAttr = document.getElementById(cBId);
		
		if(isSelectedAttr.checked) {
			
			isAnySelect = true;
			break;
		}
	}
	
	var unhideAllBtn = document.getElementById('pg:frm:pb:pbs:unhideAll');
	
	if(isAnySelect == false) {
		unhideAllBtn.disabled = true;
		unhideAllBtn.className = 'btnDisabled';
	} else {
		unhideAllBtn.disabled = false;
		unhideAllBtn.className = 'btn';
	}
	adjustRowsColor();
}
function enableCopyButton() {
	
	var clientTable = document.getElementById('pg:frm:pb:pbs:pbtClient');
	var clientRow = clientTable.rows;
	var isAnySelect = false;
	
	for(var i=0; i < clientRow.length-1 ; i++) {
		
		var cBId = 'pg:frm:pb:pbs:pbtClient:'+i+':isClient';
		var isSelectedAttr = document.getElementById(cBId);
		
		var cBId = 'pg:frm:pb:pbs:pbtClient:'+i+':isAlreadyCopied';
		var isAlreadyCopy = document.getElementById(cBId);
		
		if(isSelectedAttr.checked && isAlreadyCopy.checked == false) {
			
			isAnySelect = true;
			break;
		}
	}
	
	var copyAllBtn = document.getElementById('pg:frm:pb:pbs:copyAllBtn');
	
	if(isAnySelect == false) {
		copyAllBtn.disabled = true;
		copyAllBtn.className = 'btnDisabled';
	} else {
		copyAllBtn.disabled = false;
		copyAllBtn.className = 'btn';
	}
	
	adjustRowsColor();
}

function isHighlight(sStatus) {
	
	if(sStatus.id.indexOf('pbtClient') > -1) {
	
		var indx = sStatus.id.replace('pg:frm:pb:pbs:pbtClient:','').replace(':isClient', '');
		var intVar = parseInt(indx);
		if(sStatus.checked) {
			document.getElementById('pg:frm:pb:pbs:pbtClient').rows[intVar+1].style.backgroundColor="#F6FC83";
		} else {
			document.getElementById('pg:frm:pb:pbs:pbtClient').rows[intVar+1].style.backgroundColor="";
		}
	} else if(sStatus.id.indexOf('pbtVendor2') > -1) {
		
		var indx = sStatus.id.replace('pg:frm:pb:pbs:pbtVendor2:','').replace(':isVendor2', '');
		var intVar = parseInt(indx);
		
		var currentRow = document.getElementById('pg:frm:pb:pbs:pbtVendor2').rows[intVar+1];
		var currentCells = currentRow.cells;
		var currentCellsCount = currentCells.length;
		
		for(var k=0; k < currentCellsCount; k++) {
		
			if(sStatus.checked) {
				
				currentCells[k].style.backgroundColor="#F6FC83";
			} else {
				currentCells[k].style.backgroundColor="#E0E0E0";
			}
		}
	} else if(sStatus.id.indexOf('pbtVendor') > -1) {
		
		var indx = sStatus.id.replace('pg:frm:pb:pbs:pbtVendor:','').replace(':isVendor', '');
		var intVar = parseInt(indx);
		if(sStatus.checked) {
			document.getElementById('pg:frm:pb:pbs:pbtVendor').rows[intVar+1].style.backgroundColor="#F6FC83";
		} else {
			document.getElementById('pg:frm:pb:pbs:pbtVendor').rows[intVar+1].style.backgroundColor="";
		}
	}
}
function adjustRowsColor() {
	
	var clientInstTable = document.getElementById('pg:frm:pb:pbs:pbtClient');
	
	if(clientInstTable != 'undefined') {
		
		var rows = clientInstTable .rows;
	
		for(var i=0; i < rows.length-1 ; i++) {
	
			var selId = 'pg:frm:pb:pbs:pbtClient:'+i+':isClient';
			var selBox = document.getElementById(selId);
			
			var cBCpyId = 'pg:frm:pb:pbs:pbtClient:'+i+':isAlreadyCopied';
			var isCopyiedAttr = document.getElementById(cBCpyId);
			
			if(!isCopyiedAttr.checked) {
				
				if(selBox.checked) {
					document.getElementById('pg:frm:pb:pbs:pbtClient').rows[i+1].style.backgroundColor="#F6FC83";
				} else {
					document.getElementById('pg:frm:pb:pbs:pbtClient').rows[i+1].style.backgroundColor="";
				}
			}
		}
	} 
	
	var vendorInstTable = document.getElementById('pg:frm:pb:pbs:pbtVendor');

	if(vendorInstTable != 'undefined') {
		
		var rows = vendorInstTable.rows;
	
		for(var i=0; i < rows.length-1 ; i++) {
	
			var selId = 'pg:frm:pb:pbs:pbtVendor:'+i+':isVendor';
			var selBox = document.getElementById(selId);
			
			if(selBox.checked) {
				document.getElementById('pg:frm:pb:pbs:pbtVendor').rows[i+1].style.backgroundColor="#F6FC83";
			} else {
				document.getElementById('pg:frm:pb:pbs:pbtVendor').rows[i+1].style.backgroundColor="";
			}
		}
	} 
	
	var vendorInstTable2 = document.getElementById('pg:frm:pb:pbs:pbtVendor2');
	
	if(vendorInstTable2 != 'undefined') {
		
		var rows = vendorInstTable2.rows;
	
		for(var i=0; i < rows.length-1 ; i++) {
	
			var selId = 'pg:frm:pb:pbs:pbtVendor2:'+i+':isVendor2';
			var selBox = document.getElementById(selId);
			
			var currentRow = rows[i+1];
			var currentCells = currentRow.cells;
			var currentCellsCount = currentCells.length;
			
			for(var k=0; k < currentCellsCount; k++) {
			
				if(selBox.checked) {
					
					currentCells[k].style.backgroundColor="#F6FC83";
				} else {
					currentCells[k].style.backgroundColor="#E0E0E0";
				}
			}
		}
	}
}
function verfiyAndAddClient() {
	
	var clientInstTypeField = document.getElementById('pg:frm:pb:combobox2');
	var clientItemInstField = document.getElementById('pg:frm:pb:clientItemInst');
	var clientInstTypeVal = clientInstTypeField.value;
	console.log(clientInstTypeVal);
	var clientItemInstVal = clientItemInstField.value;
	
	var clientSaveBtn = document.getElementById('clSave');
		
	if(clientInstTypeVal != '' && clientItemInstVal != '') {
        
    	clientSaveBtn.disabled = false;
		clientSaveBtn.className = 'btn';
    } else {
    	clientSaveBtn.disabled = true;
		clientSaveBtn.className = 'btnDisabled';
    }
}
function verfiyAndAddVendor() {
	
	var vendorInstTypeField = document.getElementById('pg:frm:pb:combobox3');
	var vendorItemInstField = document.getElementById('pg:frm:pb:vendorItemInst');
	var vendorInstTypeVal = vendorInstTypeField.value;
	var vendorItemInstVal = vendorItemInstField.value;
	
	var vendorSaveBtn = document.getElementById('vNSave');
	if(vendorInstTypeVal != '' && vendorItemInstVal != '') {
        
        vendorSaveBtn.disabled = false;
		vendorSaveBtn.className = 'btn';
    } else {
    	
    	vendorSaveBtn.disabled = true;
		vendorSaveBtn.className = 'btnDisabled';
    }
}