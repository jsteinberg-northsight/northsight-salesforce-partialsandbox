(function(skuid){
skuid.snippet.register('InvoiceHistory',function(args) {var params = arguments[0],
	$ = skuid.$;
var field = arguments[0];

var value1 = arguments[1];

var model = field.model;

var row = field.row;

var value2 = row.OldValue;

var value3 = row.NewValue;

var text;


// Replace custom fields API name with their labels. You need to hard code label names. 

if (value1 === "BalanceHistory__c"){

    value1 = "Balance History";

}

if (value1 === "Sent__c"){

    value1 = "Original Sent Date";

}


if (value1 === "Auto_Created_By_PB_ID__c"){

    value1 = "Auto-Created By PB ID";

}

if (value1 === "Discount__c"){

    value1 = "Discount";

}

if (value1 === "Last_Sent_Date__c"){

    value1 = "Last Sent Date";

}

if (value1 === "Paid_Date__c"){

    value1 = "Paid Date";

}

if (value1 === "Payment_Release_Date__c"){

    value1 = "Payment Release Date";

}

if (value1 === "Payment_Terms__c"){

    value1 = "Payment Terms";

}

if (value1 === "Pay_To__c"){

    value1 = "Pay To";

}

if (value1 === "Price_Book__c"){

    value1 = "Price Book";

}

if (value1 === "Private_Notes__c"){

    value1 = "Private Notes";

}

if (value1 === "Property__c"){

    value1 = "Property";

}

if (value1 === "Receive_From__c"){

    value1 = "Receive From";

}

if (value1 === "Public_Notes__c"){

    value1 = "Public Notes";

}

if (value1 === "Status__c"){

    value1 = "Status";

}

if (value1 === "SubTotalHistory__c"){

    value1 = "Sub-Total History";

}

if (value1 === "Source"){

    value1 = "Source";

}


if (value1 === "Work _Order__c"){

    value1 = "Work Order";

}


// Handle case creation

if (value1 == "created") {

    text = 'Invoice Created';

}


// Handle text fields where change values are not documented.  

else if (value2 === null && value3 === null) {

    text = 'Changed ' + value1; 

}


// Handle "first entry" situations where "changed from" value is not present. 

else if (value2 === null && value3 !== null) {

    text = 'Changed ' + value1 +' to <b>"' + value3 + '"</b>'; 

}


// Handle full change record where Field, Changed From and Changed To fields are present. 

else if (value2 !== null) {

    text = 'Changed ' + value1 + ' from "'+ value2 + '" to <b>"' + value3 + '"</b>'; 


// If changed from value looks like an ID,  hide the row - since the name field is already there. 

    var n = value2.indexOf("00");

    if (n===0) {

        console.log('correct');

        $(document.body).one('pageload',function() {

           field.element.closest('tr').remove();

        });

    }

}


// Populate text into table cell

field.element.append(model.mergeRow(row,text));
});
skuid.snippet.register('OpenPAR',function(args) {var params = arguments[0],
	$ = skuid.$;
// Open "/apex/skuid__ui?page=ACCT_PAR_Create&id={{$Model.Invoice.data.0.Id}}" in a new browser window
window.open("/apex/skuid__ui?page=ACCT_PAR_Create&id={{$Model.Invoice.data.0.Id}}");
});
skuid.snippet.register('Print',function(args) {var params = arguments[0],
	$ = skuid.$;
window.print();
});
skuid.snippet.register('Wait',function(args) {var SECONDS_TO_DELAY = 2;
var dfd = skuid.$.Deferred();
setTimeout(function(){
    dfd.resolve();
},SECONDS_TO_DELAY*1000);
return dfd.promise();
});
skuid.snippet.register('LoadPhotos',function(args) {var params = arguments[0],
	$ = skuid.$;
skuid.$M("RLT_ImageLinkRLT").loadAllRemainingRecords({
   stepCallback: function(offsetStart,offsetEnd) {
   },
   finishCallback: function(totalRecordsRetrieved) {
      skuid.$.blockUI({ message: 'Finished loading all ' + totalRecordsRetrieved + ' Photos!', timeout: 2000 });
   }
});
});
}(window.skuid));