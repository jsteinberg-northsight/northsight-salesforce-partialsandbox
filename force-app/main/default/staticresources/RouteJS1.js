
var spinnerVisible = false;
function showProgress() {
    //if (!spinnerVisible) {
        jQuery("div#spinner").fadeIn("fast");
        spinnerVisible = true;
   // }
};
function hideProgress() {
   // if (spinnerVisible) {
        var spinner = jQuery("div#spinner");
        spinner.stop();
        spinner.fadeOut("fast");
        spinnerVisible = false;
   // }
};


var ctrldown;

function getFormattedCurDate() {
     var today = new Date();
     var dd = today.getDate();
     var mm = today.getMonth()+1; //January is 0!

     var yyyy = today.getFullYear();
     if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} today = mm+'/'+dd+'/'+yyyy;
     return today;
}


/*
  These are the implementation-specific parts of the OptiMap application at
  http://www.gebweb.net/optimap

  This should serve as an example on how to use the more general BpTspSolver.js
  from http://code.google.com/p/google-maps-tsp-solver/

  Author: Geir K. Engdahl
*/

var tsp; // The BpTspSolver object which handles the TSP computation.
var mode;
var markers = new Array();  // Need pointers to all markers to clean up.
var directions_arr = new Array();
var duplicate_directions = new Array(); // will store stop information to add to the final directions
var dirRenderer;  // Need pointer to path to clean up.
var activemap;
var stop_point_locked = false;
var stopMarker = null;

// Returns a style icon class with a color option
function getColor(color) {

     switch(color)
     {
     case 'Urgent':
     case 'Red':
          return "FF0000";
     case 'Yellow':
          return "FFFF00";
     case 'Dark Green':
          return "0A7A24";
     case 'Light Green':
          return '8CD19C';
     case 'Black':
          return "000000";
     case 'purple':
          return "483D8B";
     case 'white':
     default:
            return "FFFFFF";
     }
}



/* Returns a textual representation of time in the format
 * "N days M hrs P min Q sec". Does not include days if
 * 0 days etc. Does not include seconds if time is more than
 * 1 hour.
 */
function formatTime(seconds) {
    var days;
    var hours;
    var minutes;
    days = parseInt(seconds / (24*3600));
    seconds -= days * 24 * 3600;
    hours = parseInt(seconds / 3600);
    seconds -= hours * 3600;
    minutes = parseInt(seconds / 60);
    seconds -= minutes * 60;
    var ret = "";
    if (days > 0)
     ret += days + " days ";
    if (days > 0 || hours > 0)
     ret += hours + " hrs ";
    if (days > 0 || hours > 0 || minutes > 0)
     ret += minutes + " min ";
    if (days == 0 && hours == 0)
     ret += seconds + " sec";
    return(ret);
}

/* Returns textual representation of distance in the format
 * "N km M m". Does not include km if less than 1 km. Does not
 * include meters if km >= 10.
 */
function formatLength(meters) {
    var km = parseInt(meters / 1000);
    meters -= km * 1000;
    var ret = "";
    if (km > 0)
     ret += km + " km ";
    if (km < 10)
     ret += meters + " m";
    return(ret);
}

/* Returns textual representation of distance in the format
 * "N.M miles".
 */
function formatLengthMiles(meters) {
  var sMeters = meters * 0.621371192;
  var miles = parseInt(sMeters / 1000);
  var commaMiles = parseInt((sMeters - miles * 1000 + 50) / 100);
  var ret = miles + "." + commaMiles + " miles";
  return(ret);
}

//Code added - Padmesh Soni (08/30/2014) - As per SM-169: Optimize Route not generating stop list
//Method to format all double quotes into string for JSON string generation
function formatDobuleQuotes(dataStr) {
	
	var txtStr = dataStr;
	txtStr = txtStr.replace(/\&quot;/g,'\"');
	return txtStr;
}

/* Returns two HTML strings representing the driving directions.
 * Icons match the ones shown in the map. Addresses are used
 * as headers where available.
 * First string is suitable for use in reordering the directions.
 * Second string is suitable for printed directions.
 */
function formatDirections(gdir, mode, pathStr) {
    var addr = tsp.getAddresses();
    var labels = tsp.getLabels();
    var order = tsp.getOrder();
    var colors = tsp.getColors();
    var casekeys = tsp.getCasekeys();
    var notes = tsp.getNotes();
    var duplicates = tsp.getDuplicates();

    var WO_List = getWO_List();

    directions_arr = new Array(); // global directions holder
    duplicate_directions = new Array();

   // var retStr = "<table class='gebddir' border=0 cell-spacing=0>\n";
    var retStr = "<div class='dirWr'>";

    var retStopStr = new Array();

    var dragStr = "Drag to re-order stops:<br><ul class='unsortable'>";
    var retArr = new Array();
    var number;
    for (var i = 0; i < gdir.legs.length; ++i) {
          var route = gdir.legs[i];
          var colour = "g";
          var priority = '';
          number = i;

          // DIRECTIONS
          //retStr += "\t<tr class='directionsHeading'><td width=40>"
          //    + "<div><div class='sideIconBig' style='text-align: center; background-color:"+getColor(colors[order[i]])+"'><b>"+ number + "</b></div></div></td>"
          //    + "<td><div style='text-align: left; margin: 10px;'>";

          if(colors[order[i]]=='Urgent') {
               color = 'red';
               priority = ' [EXTREME PRIORITY]';
          } else if(colors[order[i]]=='Red') {
               color = 'red';
               priority = ' [HIGH PRIORITY]';
          } else {
               color = (typeof colors[order[i]]=='undefined') ? 'white' : colors[order[i]];
          }

          if(number == 0) {
               number = 'Start';
               directions_arr[i] = pathStr + "<div class='dirH'><div class='stop_number'>"+ number +"</div>";
          } else {
               directions_arr[i] = "<div class='dirH'><div class='stop_number'>"+ number +"</div>";
          }


          var headerStr;
          var dragHead;
          var casekey;
          headerStr = '<div class="descH"><b>' + addr[order[i]] + ' ' + priority +'</b><br/><br/>';
          if(typeof casekeys[order[i]] != 'undefined') {
               casekey = casekeys[order[i]];
               headerStr += 'Work Order #: ' + WO_List[casekey].WON + '<br/>';
               headerStr += 'Work Ordered: ' + WO_List[casekey].WOD + '<br/>';
               headerStr += 'Scheduled Date: ' + WO_List[casekey].Date + '<br/>';
               headerStr += 'Client #: ' + WO_List[casekey].Client + '<br/>';
               headerStr += 'Notes: ' + formatDobuleQuotes(WO_List[casekey].Notes)+ '<br/>';
			   headerStr += 'Recurring Notes: ' + formatDobuleQuotes(WO_List[casekey].RecNotes);
          } else if(i==0) {
               headerStr += '<b>Starting Point</b>';
          }     else {
               headerStr += 'Custom Stop';
          }

          directions_arr[i] += headerStr + "</div>";

          dragHead = headerStr;

          //directions_arr[i] += "<div class='hNotes'>";

          /*
          if(i!=0) {
               directions_arr[i] += 'Notes: ';


               if(typeof casekeys[order[i]] == 'undefined') {
                    directions_arr[i] += (typeof notes[order[i]]== 'undefined') ? 'None' : '<br/>'+notes[order[i]];
               } else {
                    //directions_arr[i] += (WO_List[casekeys[order[i]]].NotesContent == '') ? 'None' : '<br/>'+WO_List[casekeys[order[i]]].NotesContent;
                    directions_arr[i] += WO_List[casekeys[order[i]]].Notes;
               }
          }
          */

          //directions_arr[i] += "</div></div>";
          directions_arr[i] += "</div>";

          // Create Headers for Duplicate Work Order Address Info
          var dupheader;
          var dupcasekey;
          if(typeof duplicates[order[i]] != 'undefined' && duplicates[order[i]].length > 0) {
               duplicate_directions[order[i]] = new Array();
               jQuery.each(duplicates[order[i]], function(k, v) {
                    dupcasekey = v;
                    priority = '';

                    if(WO_List[dupcasekey].Color=='Urgent') {
                         color = 'red';
                         priority = ' [EXTREME PRIORITY]';
                    } else if(WO_List[dupcasekey].Color=='Red') {
                         color = 'red';
                         priority = ' [HIGH PRIORITY]';
                    } else {
                         color = (typeof WO_List[dupcasekey].Color== 'undefined') ? 'white' : WO_List[dupcasekey].Color;
                    }
                    dupheader = "<div class='dirH_dup'><div class='stop_number'>"+ number +"</div>";
                    dupheader += "<div class='descH'><b>" + addr[order[i]] + ' ' + priority +'</b><br/><br/>';

                    dupheader += 'Work Order #: ' + WO_List[dupcasekey].WON + '<br/>';
                    dupheader += 'Work Ordered: ' + WO_List[dupcasekey].WOD + '<br/>';
                    dupheader += 'Scheduled Date: ' + WO_List[dupcasekey].Date + '<br/>';
                    dupheader += 'Client #: ' + WO_List[dupcasekey].Client + '<br/>';
                    dupheader += 'Notes: ' + formatDobuleQuotes(WO_List[dupcasekey].Notes) + '<br/>';
                    dupheader += 'Recurring Notes: ' + formatDobuleQuotes(WO_List[dupcasekey].RecNotes)+ '<br/>';
                    //dupheader += "</div><div class='hNotes'>";
                    //dupheader += 'Notes: ';

                    //dupheader += (WO_List[dupcasekey].NotesContent == '') ? 'None' : '<br/>'+WO_List[dupcasekey].NotesContent;
                    //dupheader += WO_List[dupcasekey].Notes;

                    dupheader += "</div></div>";

                    directions_arr[i] += dupheader;
               });
          }


          // DRAGGABLE
          dragStr += "<li id='" + i + "' class='ui-state-"
            + (i ? "default" : "disabled") + "'>"
            + "<table class='dragTable'><tr><td class='left'><div class='sideIconSmall' style='background-color:"+getColor(colors[order[i]])+"'>"+ number +"</div>"
            + "</td><td class='middle'>" + dragHead + "</td><td class='right'>"
          //  + (i ? "<button id='dragEdit"+i+"' value='"+ order[i] +"'/></td><td class='right'><button id='dragClose" + i + "' value='" + i + "'></button>" : "")
            + "</td></tr></table></li>";
          if (i == 0) {
            dragStr += "</ul><ul id='sortable'>";
          }



          for (var j = 0; j < route.steps.length; ++j) {
              var classStr = "o";
              if (j % 2 == 0) classStr = "e";

              directions_arr[i] += "<div class='r" + classStr + "'><div class='leg'>" + route.steps[j].instructions + "</div><div class='distRt'>" + route.steps[j].distance.text + "</div></div>\n";
          }

          //output test
          //jQuery('#testDiv').append('Route stop ' + i +' len: ' + retStopStr[i].length + '<br/>');
          //tmpStrlen += retStopStr[i].length;

          retStr += directions_arr[i];

    }

    // DRAGGABLE - START POINT
    dragStr += "</ul><ul class='unsortable'>";

    // Draw the final step in the directions - Re draw stop 0 for round-trip
    if (mode == 0) {

               var headerStr;
               if (labels[order[0]] != null && labels[order[0]] != "") {
                   headerStr = labels[order[0]];
               } else if (addr[order[0]] != null) {
                   headerStr = addr[order[0]];
               } else {
                   var prevI = gdir.legs.length - 1;
                   var latLng = gdir.legs[prevI].end_location;
                   headerStr = latLng.toString();
               }

          // DRAGGABLE - START POINT
          dragStr += "<li id='" + 0 + "' class='ui-state-disabled'>"
            + "<table class='dragTable'><tr><td><div class='sideIconSmall' style='background-color:"+getColor(colors[order[0]])+"'><img src='" + URLFOR.Resource.TSP['icon.png'] + "'/></div></td><td>" + headerStr
            + "</td></tr></table></li>";

          directions_arr[i] = "<div class='dirH'><div class='stop_number'>"+i+"</div><div class='descH'><b>" + addr[0]  +"</b><br/><br/>Route End</div></div>";

      // Draw directions for last step in A-Z calculation
    } else if (mode == 1) {

                    var color;
                    var priority;
                    if(colors[order[i]]=='Urgent') {
                         color = 'red';
                         priority = '[EXTREME PRIORITY]';
                    } else if(colors[order[i]]=='Red') {
                         color = 'red';
                         priority = ' [HIGH PRIORITY]';
                    } else {
                         color = (typeof colors[order[i]]== 'undefined') ? 'white' : colors[order[i]];
                    }

                    var dragHead;
                    var headerStr = "<div class='descH'>";
                    var casekey;
                    var i = order[gdir.legs.length];
                    headerStr += '<b>' + addr[i] + ' ' + priority +'</b><br/><br/>';
                    if(typeof casekeys[i] != 'undefined' && casekeys[i]!=null) {
                         casekey = casekeys[i];
                         headerStr += 'Work Order #: ' + WO_List[casekey].WON + '<br/>';
                         headerStr += 'Work Ordered: ' + WO_List[casekey].WOD + '<br/>';
                         headerStr += 'Scheduled Date: ' + WO_List[casekey].Date + '<br/>';
                         headerStr += 'Client #: ' + WO_List[casekey].Client + '<br/>';
                         headerStr += 'Notes: ' + formatDobuleQuotes(WO_List[casekey].Notes)+ '<br/>';
						 headerStr += 'Recurring Notes: ' + formatDobuleQuotes(WO_List[casekey].RecNotes)+ '<br/>';
                    } else {
                         headerStr += 'Custom Stop';
                    }

                    headerStr += "</div>";

                    dragHead = headerStr;

                    //headerStr += "<div class='hNotes'>";

                    //headerStr += 'Notes: ';
                    /*
                    if(typeof casekeys[i] == 'undefined') {
                         headerStr += (typeof notes[i]== 'undefined') ? 'None' : '<br/>'+notes[order[i]];
                    } else {
                         //headerStr += (WO_List[casekeys[i]].NotesContent == '') ? 'None' : '<br/>'+WO_List[casekeys[i]].NotesContent;
                         headerStr += WO_List[casekeys[i]].Notes;
                    }
                    */

                    //headerStr += "</div>";

                    // Create Headers for Duplicate Work Order Address Info
                    var dupheader;
                    var dupcasekey;
                    if(typeof duplicates[i] != 'undefined' && duplicates[i].length > 0) {
                         duplicate_directions[i] = new Array();
                         jQuery.each(duplicates[i], function(k, v) {
                              dupcasekey = v;
                              priority = '';

                              if(WO_List[dupcasekey].Color=='Urgent') {
                                   color = 'red';
                                   priority = ' [EXTREME PRIORITY]';
                              } else if(WO_List[dupcasekey].Color=='Red') {
                                   color = 'red';
                                   priority = ' [HIGH PRIORITY]';
                              } else {
                                   color = (typeof WO_List[dupcasekey].Color== 'undefined') ? 'white' : WO_List[dupcasekey].Color;
                              }
                              dupheader = "<div class='dirH_dup'><div class='stop_number'>"+ number +"</div>";
                              dupheader += "<div class='descH'><b>" + addr[i] + ' ' + priority +'</b><br/><br/>';

                              dupheader += 'Work Order #: ' + WO_List[dupcasekey].WON + '<br/>';
                              dupheader += 'Work Ordered: ' + WO_List[dupcasekey].WOD + '<br/>';
                              dupheader += 'Scheduled Date: ' + WO_List[dupcasekey].Date + '<br/>';
                              dupheader += 'Client #: ' + WO_List[dupcasekey].Client + '<br/>';
                              dupheader += 'Notes: ' + formatDobuleQuotes(WO_List[dupcasekey].Notes) + '<br/>';
							  dupheader += 'Recurring Notes: ' + formatDobuleQuotes(WO_List[dupcasekey].RecNotes)+'<br/>';

                              //dupheader += "</div><div class='hNotes'>";

                              //dupheader += 'Notes: ';

                              //dupheader += (WO_List[dupcasekey].NotesContent == '') ? 'None' : '<br/>'+WO_List[dupcasekey].NotesContent;
                              //dupheader += WO_List[dupcasekey].Notes;

                              dupheader += "</div></div>";

                              headerStr += dupheader;
                         });
                    }


               // DRAGGABLE  - END POINT
               dragStr += "<li id='" + gdir.legs.length + "' class='ui-state-disabled'>"
                 + "<table class='dragTable'><tr><td class='left'><div class='sideIconSmall' style='background-color:"+getColor(colors[order[gdir.legs.length]])+"'>"+  (gdir.legs.length ) +"</div></td><td class='middle'>"
                 + dragHead + "</td></tr></table></li>";


               directions_arr[i] = "<div class='dirH'><div class='stop_number'>"+ gdir.legs.length +"</div>"+headerStr;

    }
    dragStr += "</ul>";
    retStr += "</div>";
    retArr[0] = dragStr;
    retArr[1] = retStr;

    if(REORDER_ON_CALC) {
         tsp.reorderSolution(newOrder, onSolveCallback);
         REORDER_ON_CALC = false;
    }

    //jQuery('#testDiv').append('<b>Total Directions Len: </b>' + retStr.length );


    return(retArr);
}

function createTomTomLink(gdir) {
    var addr = tsp.getAddresses();
    var labels = tsp.getLabels();
    var order = tsp.getOrder();
    var addr2 = new Array();
    var label2 = new Array();
    for (var i = 0; i < order.length; ++i) {
     addr2[i] = addr[order[i]];
     if (order[i] < labels.length && labels[order[i]] != null && labels[order[i]] != "")
         label2[i] = labels[order[i]];
    }
    var itn = createTomTomItineraryItn(gdir, addr2, label2);
    var retStr = "<form method='GET' action='tomtom.php' target='tomTomIFrame'>\n";
    retStr += "<input type='hidden' name='itn' value='" + itn + "' />\n";
    retStr += "<input id='tomTomButton' class='calcButton' type='submit' value='Send to TomTom' onClick='jQuery(\"#dialogTomTom\").dialog(\"open\");'/>\n";
    retStr += "</form>\n";
    return retStr;
}

function createGarminLink(gdir) {
    var addr = tsp.getAddresses();
    var labels = tsp.getLabels();
    var order = tsp.getOrder();
    var addr2 = new Array();
    var label2 = new Array();
    for (var i = 0; i < order.length; ++i) {
     addr2[i] = addr[order[i]];
     if (order[i] < labels.length && labels[order[i]] != null && labels[order[i]] != "")
         label2[i] = labels[order[i]];
    }
    var gpx = createGarminGpx(gdir, addr2, label2);
    var gpxWp = createGarminGpxWaypoints(gdir, addr2, label2);
    var retStr = "<form method='POST' action='garmin.php' target='garminIFrame'>\n";
    retStr += "<input type='hidden' name='gpx' value='" + gpx + "' />\n";
    retStr += "<input type='hidden' name='gpxWp' value='" + gpxWp + "' />\n";
    retStr += "<input id='garminButton' class='calcButton' type='submit' value='Send to Garmin' onClick='jQuery(\"#dialogGarmin\").dialog(\"open\");'/>\n";
    retStr += "</form>\n";
    return retStr;
}

function createGoogleLink(gdir) {
    var addr = tsp.getAddresses();
    var order = tsp.getOrder();
    var ret = "http://maps.google.com/maps?saddr=";
    for (var i = 0; i < order.length; ++i) {
     if (i == 1) {
         ret += "&daddr=";
     } else if (i >= 2) {
         ret += " to:";
     }
     if (addr[order[i]] != null && addr[order[i]] != "") {
       ret += escape(addr[order[i]]);
     } else {
         if (i == 0) {
          ret += gdir.legs[0].start_location.toString();
         } else {
          ret += gdir.legs[i-1].end_location.toString();
         }
     }
    }
    return ret;
}

function getWindowHeight() {
    if (typeof(window.innerHeight) == 'number')
     return window.innerHeight;
    if (document.documentElement && document.documentElement.clientHeight)
     return document.documentElement.clientHeight;
    if (document.body && document.body.clientHeight)
     return document.body.clientHeight;
    return 800;
}

function getWindowWidth() {
    if (typeof(window.innerWidth) == 'number')
     return window.innerWidth;
    if (document.documentElement && document.documentElement.clientWidth)
     return document.documentElement.clientWidth;
    if (document.body && document.body.clientWidth)
     return document.body.clientWidth;
    return 1200;
}

function onProgressCallback(tsp) {
  jQuery('#progressBar').progressbar('value', 100 * tsp.getNumDirectionsComputed() / tsp.getNumDirectionsNeeded());
}

function setMarkerAsStart(marker) {
    marker.infoWindow.close();
    tsp.setAsStart(marker.getPosition());
}

function setMarkerAsStop(marker) {
    marker.infoWindow.close();
    tsp.setAsStop(marker.getPosition());
     var order = tsp.getOrder();
     stopMarker = marker.getPosition();
     setStopLock(true);
    if(typeof order != 'undefined' && order.length > 0) {
         reCalculate();
    } else {
         drawMarkers(false);
    }
}

// Sets or unsets stop point locking
function setStopLock(which) {
     if(which) {
          stop_point_locked = true;
          jQuery('#stop_location_msg').show();
     } else {
          stop_point_locked = false;
          jQuery('#stop_location_msg').hide();
          stopMarker = null;
          tsp.correctStops();
          //cancel_directions();
          //must_recalculate();
          //drawMarkers(false);
          var order = tsp.getOrder();
          if(typeof order != 'undefined' && order.length > 0) {
              reCalculate();
         }
     }
}

function removeMarker(marker, stoprefresh) {
    if(typeof marker.infoWindow != 'undefined') {
         marker.infoWindow.close();
    }

    //console.log('Removing Waypoint: ' + marker.getPosition());
    tsp.removeWaypoint(marker.getPosition());

    if(!stoprefresh) {
         drawMarkers(false);
         updateSelectOptions();
    }
}


function selectMarkersByBounds(bnds) {
     for(i=1;i<markers.length;i++) {  // start at 1 to avoid selecting the start position
          // close any info boxes
          if(typeof markers[i].infoWindow!= 'undefined') {
               markers[i].infoWindow.close();
          }
          if(bnds.contains(markers[i].getPosition())) {
               tsp.selectMarker(markers[i].getPosition());
          }
     }
     drawMarkers(false);
     updateSelectOptions();
}

function selectMarker(index, redraw) {
     if(index == 0) {
          return;
     }

     if(typeof markers[index].infoWindow != 'undefined') {
          markers[index].infoWindow.close();
     }

     tsp.selectMarker(markers[index].getPosition());

    if(redraw) {
         drawMarkers(false);
    }

    jQuery('#selected_marker_opts').show();
}

function deSelectMarker(index, redraw) {

     tsp.deSelectMarker(markers[index].getPosition());

     if(redraw) {
          drawMarkers(false);
     }

     updateSelectOptions();
}

function deSelectAllMarkers() {
     tsp.deSelectAllMarkers();
     drawMarkers(false);
     jQuery('#selected_marker_opts').hide();
}


function selectInverseMarkers() {
     tsp.invertSelectedMarkers();
     drawMarkers(false);
}

function deleteSelectedMarkers() {
     var numMarkers = markers.length;
     var caseKeys = tsp.getCasekeys();
     var deleteList = [];

     // Undo selections, clear check boxes first
     for(var i=0;i<numMarkers;i++) {
          if(tsp.isMarkerSelected(markers[i].getPosition())) {
               deSelectMarker(i, false);
               deleteList.push(i);
          }
     }

     //// put confirm here
     if (confirm('Are you sure you want to delete ' + deleteList.length.toString() + ' stop' + (deleteList.length != 1 ? 's' : '') + '?')) {
          bulkDelete(deleteList);
     }

}

// Markers selected? IF so, so selection options
function updateSelectOptions() {
     if(tsp.getSumSelected() > 0 ) {
          jQuery('#selected_marker_opts').show();
     } else {
          jQuery('#selected_marker_opts').hide();
     }
}

// removes the duplication entry on a waypoint index
function removeDuplicate(WO, index) {
     tsp.removeDuplicate(WO, index);
     if(typeof markers[index] != 'undefined' && typeof markers[index].infoWindow != 'undefined') {
          markers[index].infoWindow.close();
     }
     jQuery('.'+WO).attr('checked', false).parent().parent().removeClass('highlighted');
     drawMarkers(false);
}

function drawMarker(latlng, addr, label, color,  num) {
     //console.log(addr + ' ' + num);
     var markertext;
     var marker = null;
     // Draw the home icon for the start
     if(num == 0) {
		
		var truckUrl;
		if(Current_ProfileName.indexOf('Vendors') > -1) {
			
			truckUrl = '/vendors/resource/1403647736000/VendorTruckSmall';
		} else {
			truckUrl = '/resource/1403647736000/VendorTruckSmall';
		}
		  var image = new google.maps.MarkerImage(
            truckUrl,
            new google.maps.Size(65,65),
            new google.maps.Point(0,0),
            new google.maps.Point(65,65)
          );
          var shadow = new google.maps.MarkerImage(
            URLFOR.Resource.TSP['shadow.png'],
            new google.maps.Size(64,65),
            new google.maps.Point(0,0),
            new google.maps.Point(65,65)
          );

          /*var fastMarkers = [];
          // id, latLng, innerHtmlArray, divClassName, zIndex, leftOffset, topOffset
          var markerLabel = new com.redfin.FastMarker(1, new google.maps.Point(0,0), ["<div class='labels'> Hello World </div>"], "myMarker", 99999, 10, 10);
          fastMarkers.push(markerLabel);
          new com.redfin.FastMarkerOverlay(gebMap, fastMarkers);
          */
          var shape = {
            coord: [21,0,22,1,23,2,24,3,25,4,26,5,27,6,28,7,29,8,30,9,31,10,32,11,33,12,34,13,35,14,36,15,37,16,38,17,39,18,39,19,39,20,39,21,38,22,37,23,36,24,35,25,34,26,33,27,32,28,31,29,30,30,29,31,28,32,27,33,26,34,25,35,25,36,24,37,23,38,22,39,18,39,17,38,16,37,15,36,14,35,13,34,12,33,11,32,10,31,9,30,8,29,7,28,6,27,5,26,4,25,3,24,2,23,1,22,0,21,0,20,0,19,1,18,2,17,3,16,4,15,5,14,6,13,7,12,7,11,8,10,9,9,10,8,11,7,12,6,13,5,14,4,15,3,16,2,17,1,18,0,21,0],
            type: 'poly'
          };
          //var vendorInitials = '{!contact_Initials}';
          
			var pixelUrl;
			if(Current_ProfileName.indexOf('Vendors') > -1) {
				
				pixelUrl = '/vendors/resource/1403752839000/pixel';
			} else {
				pixelUrl = '/resource/1403752839000/pixel';
			}
		  var marker1 = new MarkerWithLabel({
                 icon: pixelUrl,
                 position: latlng,
                 draggable: false,
                 raiseOnDrag: false,
                 map: gebMap,
                 labelInBackground: true,
                 labelContent: vendorInitials, //vendorInitials,
                 labelAnchor: new google.maps.Point(65, 50),
                 labelClass: "labels2", // the CSS class for the label
                 labelStyle: {opacity: 1}
               });
          markers[num] = new google.maps.Marker({
            icon: image,
            shadow: shadow,
            shape: shape,
            map: gebMap,
            position: latlng,
            zIndex: 9999,
            title:'Start Location'
          });
          bindMarkerEvents(markers[num],num);
          bindLabelsEvents(markers[num],num,marker1);

          /*
          icon = new google.maps.MarkerImage(URLFOR.Resource.TSP['home24.png']);
         marker = new google.maps.Marker({
             position: latlng,
               icon: icon,
               label: 'Starting Location',
               map: gebMap });
          */

     } else {

               // Determine how to number the markers,
               /// by addition order, or by calculated route order
               var markertext;
               var order = tsp.getOrder();
               if(order != undefined && order.length != 0) {
                    for(var i=0; i< order.length; i++) {
                         if(order[i] == num) {
                              markertext = i.toString();
                              break;
                         }
                    }
               } else {
                    markertext = '•';
               }


               var isSelected = tsp.getSelected();
               var markercolor =  getColor(color);
               var textcolor  = (color == 'Black') ? 'FFFFFF' : '000000';
               var starcolor = (color == 'Urgent') ? 'FFFF00' : 'FFFFFF';
               var iconUrl;
               var shadowUrl = 'https://chart.googleapis.com/chart?chst=d_map_pin_shadow';
               var offset = new google.maps.Point(10,35);
               var size = new google.maps.Size(64,40);
               var shape = {coord: [0, 0, 30, 30],
                                    type: 'rect'
                                   };
               var windowOffset = new google.maps.Size(0, -35)
               /*if(color == 'Urgent'){
                    shadowUrl = 'https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small_shadow&chld=star|bbtl|'+markertext+'|'+markercolor+'|'+textcolor+'|'+starcolor;
                    offset = new google.maps.Point(0,0);
                    size = new google.maps.Size(64,45);
                    shape = {coord: [10, 10, 60, 45],
                                    type: 'rect'
                                   };
                    windowOffset = new google.maps.Size(0, 0)
                    if (isSelected[num]){
                              iconUrl = 'https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=star|bbtl|'+markertext+'|'+markercolor+'|'+textcolor+'|'+starcolor;
                         }else{
                              iconUrl = 'https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=caution|bbtl|'+markertext+'|'+markercolor+'|'+textcolor+'|'+starcolor;
                         }

               }
               else{*/
                    if (isSelected[num]){
                         iconUrl = 'https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_star|'+markertext+'|'+markercolor+'|'+textcolor+'|'+starcolor;
                    }else{
                         iconUrl = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld='+markertext+'|'+markercolor+'|'+textcolor;
                    }
               //}
               var image = new google.maps.MarkerImage(
                 iconUrl,
                 size,
                 new google.maps.Point(0,0),
                   offset
               );
               var shadow = new google.maps.MarkerImage(
                 shadowUrl,
                 size,
                 new google.maps.Point(0,0),
                   offset
               );
               markers[num]=new google.maps.Marker({
            icon: image,
            shadow: shadow,
            shape: shape,
            map: gebMap,
            position: latlng,
            zIndex: 9999,
            title:label
          });
               bindMarkerEvents(markers[num],num,windowOffset);


     }


}

function bindMarkerEvents(marker,num,windowOffset){
 google.maps.event.addListener(marker, 'click', function(event) {

              WO_List = getWO_List();

              // Find the context marker index
              var markerInd = -1;
               for (var i = 0; i < markers.length; ++i) {
                   if (markers[i] != null && marker.getPosition().equals(markers[i].getPosition())) {
                    markerInd = i;
                    break;
                   }
               }

          if(ctrldown) {
               selectMarker(markerInd, true);
          } else {

                    var label_content;

                    // Build marker information from case info
                    colors = tsp.getColors();
                    keys = tsp.getCasekeys();
                    duplicates = tsp.getDuplicates();
                    address_pieces = tsp.getAddressPieces();
                    notes = tsp.getNotes();

                    casekey = keys[num];

                    label_content =  address_pieces[markerInd][0] + '<br/>' + address_pieces[markerInd][1] + ' '+  address_pieces[markerInd][2] +' '+ address_pieces[markerInd][3] + '<br/><br/>';

                    if(typeof WO_List[casekey] != 'undefined') {
                         label_content += 'Work Order #: <a class="info" href="/'+ WO_List[casekey].ID +'" target="_blank">' + WO_List[casekey].WON + '</a><br/>';
                         label_content += 'Work Ordered: ' + WO_List[casekey].WOD + '<br/>';
                         label_content += 'Scheduled Date: ' + WO_List[casekey].Date + '<br/>';
                         label_content += 'Client #: ' + WO_List[casekey].Client + ' ';
                         label_content += 'Notes: ' + WO_List[casekey].Notes + '<br/>';
						 label_content += 'Recurring Notes: ' + WO_List[casekey].RecNotes+ '<br/>';

                         // Fill in Duplicate Work Order details
                         var dupkeys;
                         var dupkey;
                         if(duplicates[markerInd] != undefined) {
                              dupkeys = duplicates[markerInd];
                              for(var d=0;d<dupkeys.length;d++) {
                                   dupkey = dupkeys[d];
                                   label_content += '<hr>Work Order #: <a class="info" href="/'+ WO_List[dupkey].ID +'" target="_blank">' + WO_List[dupkey].WON + '</a>';
                                   label_content += '( <a class="info" href="javascript:removeDuplicate(\''+dupkey+'\', ' + markerInd +')">Remove</a> )<br/>';
                                   label_content += 'Work Ordered: ' + WO_List[dupkey].WOD + '<br/>';
                                   label_content += 'Scheduled Date: ' + WO_List[dupkey].Date + '<br/>';
                                   label_content += 'Client #: ' + WO_List[dupkey].Client + ' ';
                                   label_content += 'Notes: ' + WO_List[dupkey].Notes + '<br/>';
								   label_content += 'Recurring Notes: ' + WO_List[casekey].RecNotes+ '<br/>';
                              }
                         }

                    } else if(markerInd != 0) {
                         label_content += 'Notes: ';
                         label_content +=  (notes[markerInd]==undefined) ? '' : notes[markerInd].replace(/(\r\n|\n|\r)/gm,"<br/>");
                    }

                    if(markerInd ==0) {
                         label_content += 'Starting Point';
                    }


                    label_content += "<hr/> <a class='info' href='javascript:edit_location("+ markerInd +")'>Edit</a>";

                    if(markerInd != 0) {
                         label_content += " | <a class='info' href='javascript:selectMarker(" + markerInd + ", true)'>Select</a>";
                         label_content += " | <a class='info' href='javascript:setMarkerAsStop(markers["+markerInd+"])'>End Here</a>";
                         if(WO_List[casekey] != undefined) {
                              label_content += " | <a class='info_r' href='javascript:enQueue(\"" + casekey + "\", false, true)'>Remove</a>";
                         } else {
                              label_content += " | <a class='info_r' href='javascript:remove_manual_location("+markerInd+")'>Remove</a>";
                         }
                    }

                    for(var x = 0 ; x < markers.length ; x++ ) {
                         if(typeof markers[x].infoWindow != 'undefined') {
                              markers[x].infoWindow.close();
                         }
                    }

                    var infoWindow = new google.maps.InfoWindow({
                        content: label_content ,
                        //position: marker.getPosition(),
                        //pixelOffset:windowOffset
                        pixelOffset: new google.maps.Size(0, 20)
                        });
                    //marker.infoWindow = infoWindow;
                    infoWindow.open(gebMap,marker);
                    //    tsp.removeWaypoint(marker.getPosition());
                    //    marker.setMap(null);
          }

     });


}

function bindLabelsEvents(marker,num,label){
  google.maps.event.addListener(label, 'click', function(event) {
              WO_List = getWO_List();

              // Find the context marker index
              var markerInd = -1;
               for (var i = 0; i < markers.length; ++i) {
                   if (markers[i] != null && label.getPosition().equals(markers[i].getPosition())) {
                    markerInd = i;
                    break;
                   }
               }

          if(ctrldown) {
               selectMarker(markerInd, true);
          } else {

                    var label_content;

                    // Build marker information from case info
                    colors = tsp.getColors();
                    keys = tsp.getCasekeys();
                    duplicates = tsp.getDuplicates();
                    address_pieces = tsp.getAddressPieces();
                    notes = tsp.getNotes();

                    casekey = keys[num];

                    label_content =  address_pieces[markerInd][0] + '<br/>' + address_pieces[markerInd][1] + ' '+  address_pieces[markerInd][2] +' '+ address_pieces[markerInd][3] + '<br/><br/>';

                    if(typeof WO_List[casekey] != 'undefined') {
                         label_content += 'Work Order #: <a class="info" href="/'+ WO_List[casekey].ID +'" target="_blank">' + WO_List[casekey].WON + '</a><br/>';
                         label_content += 'Work Ordered: ' + WO_List[casekey].WOD + '<br/>';
                         label_content += 'Scheduled Date: ' + WO_List[casekey].Date + '<br/>';
                         label_content += 'Client #: ' + WO_List[casekey].Client + ' ';
                         label_content += 'Notes: ' + WO_List[casekey].Notes + '<br/>';
             label_content += 'Recurring Notes: ' + WO_List[casekey].RecNotes+ '<br/>';

                         // Fill in Duplicate Work Order details
                         var dupkeys;
                         var dupkey;
                         if(duplicates[markerInd] != undefined) {
                              dupkeys = duplicates[markerInd];
                              for(var d=0;d<dupkeys.length;d++) {
                                   dupkey = dupkeys[d];
                                   label_content += '<hr>Work Order #: <a class="info" href="/'+ WO_List[dupkey].ID +'" target="_blank">' + WO_List[dupkey].WON + '</a>';
                                   label_content += '( <a class="info" href="javascript:removeDuplicate(\''+dupkey+'\', ' + markerInd +')">Remove</a> )<br/>';
                                   label_content += 'Work Ordered: ' + WO_List[dupkey].WOD + '<br/>';
                                   label_content += 'Scheduled Date: ' + WO_List[dupkey].Date + '<br/>';
                                   label_content += 'Client #: ' + WO_List[dupkey].Client + ' ';
                                   label_content += 'Notes: ' + WO_List[dupkey].Notes + '<br/>';
                   label_content += 'Recurring Notes: ' + WO_List[casekey].RecNotes+ '<br/>';
                              }
                         }

                    } else if(markerInd != 0) {
                         label_content += 'Notes: ';
                         label_content +=  (notes[markerInd]==undefined) ? '' : notes[markerInd].replace(/(\r\n|\n|\r)/gm,"<br/>");
                    }

                    if(markerInd ==0) {
                         label_content += 'Starting Point';
                    }


                    label_content += "<hr/> <a class='info' href='javascript:edit_location("+ markerInd +")'>Edit</a>";

                    if(markerInd != 0) {
                         label_content += " | <a class='info' href='javascript:selectMarker(" + markerInd + ", true)'>Select</a>";
                         label_content += " | <a class='info' href='javascript:setMarkerAsStop(markers["+markerInd+"])'>End Here</a>";
                         if(WO_List[casekey] != undefined) {
                              label_content += " | <a class='info_r' href='javascript:enQueue(\"" + casekey + "\", false, true)'>Remove</a>";
                         } else {
                              label_content += " | <a class='info_r' href='javascript:remove_manual_location("+markerInd+")'>Remove</a>";
                         }
                    }

                    for(var x = 0 ; x < markers.length ; x++ ) {
                         if(typeof markers[x].infoWindow != 'undefined') {
                              markers[x].infoWindow.close();
                         }
                    }

                    var infoWindow = new google.maps.InfoWindow({
                        content: label_content ,
                        //position: marker.getPosition(),
                        //pixelOffset:windowOffset
                        pixelOffset: new google.maps.Size(0, 20)
                        });
                    //marker.infoWindow = infoWindow;
                    infoWindow.open(gebMap,marker);
                    //    tsp.removeWaypoint(marker.getPosition());
                    //    marker.setMap(null);
          }

  });

}


function setViewportToCover(waypoints) {
     // Override this because of the UI tab structure
     if(REFRESH_MAP) {
         var bounds = new google.maps.LatLngBounds();
         for (var i = 0; i < waypoints.length; ++i) {
          bounds.extend(waypoints[i]);
         }
         gebMap.fitBounds(bounds);
      }
}

function initMap(center, zoom, div) {
    var myOptions = {
               zoom: zoom,
               center: center,
               mapTypeId: google.maps.MapTypeId.ROADMAP,
               panControlOptions: {
                  position: google.maps.ControlPosition.LEFT_TOP
              },
              streetViewControlOptions: {
                  position: google.maps.ControlPosition.LEFT_TOP
              },
              zoomControlOptions: {
             position: google.maps.ControlPosition.LEFT_TOP
                   }
    };
    gebMap = new google.maps.Map(div, myOptions);

    gebMap.enableKeyDragZoom({
              key: 'alt',
             visualEnabled: true,
            visualSprite: URLFOR.Resource.TSP['select.png'],
            visualPositionOffset: new google.maps.Size(10, 10),
            visualPosition: google.maps.ControlPosition.TOP_LEFT,
            visualSize: new google.maps.Size(60, 30),
            visualTips: { on: 'Disable lasso-select mode',
                             off: 'Click to lasso-select multiple orders on the map'
                            }
          });

     var dz = gebMap.getDragZoomObject();
      google.maps.event.addListener(dz, 'dragend', function (bnds) {
         selectMarkersByBounds(bnds);
      });

    //google.maps.event.addListener(gebMap, "click", function(event) {
     //tsp.addWaypoint(event.latLng, addWaypointSuccessCallback);
    //});

    // google.maps.event.addListener(gebMap, 'zoom_changed', function() {
    //      alert('ZOOM changed to: ' + gebMap.getZoom());
    // });
}

function loadAtStart(lat, lng, zoom) {

    var center = new google.maps.LatLng(lat, lng);
    initMap(center, zoom, document.getElementById("map"));
   // directionsPanel = document.getElementById("my_textual_div");

    tsp = new BpTspSolver(gebMap);
    google.maps.event.addListener(tsp.getGDirectionsService(), "error", function() {
     alert("Request failed: " + reasons[tsp.getGDirectionsService().getStatus().code]);
    });
}

function addWaypointWithLabel(latLng, label) {
    tsp.addWaypointWithLabel(latLng, label, addWaypointSuccessCallbackZoom);
}

function addWaypoint(latLng) {
    addWaypointWithLabel(latLng, null, addWaypointSuccessCallbackZoom);
}

function addAddressJson(casekey) {
     // There are bugs in the reordering system, so we will disable it any time something is added or removed
     tmporder = tsp.getOrder();
     if(tmporder != undefined && tmporder.length!=0) {
          // if an order has been calculated, reset it
          cancel_directions();
     }

     tsp.addAddressJson(casekey);
}

function setQueueSize(size) {
     tsp.setQueueSize(size);
}

function addAddressAndLabel(addr, city, state, zip, label, color, casekey, note) {
     // There are bugs in the reordering system, so we will disable it any time something is added or removed
     tmporder = tsp.getOrder();
     if(tmporder != undefined && tmporder.length!=0) {
          // if an order has been calculated, reset it
          cancel_directions();
     }
    tsp.addAddressWithLabel(addr,city,state, zip, label, color, casekey, addAddressSuccessCallbackZoom, note);
}

function updateAddress(index, address, city, state, zip) {
     tsp.updateAddress(index, address, city,state, zip);
}

//function addAddress(addr) {
//    addAddressAndLabel(addr, addr, 'purple', null);
//}

function clickedAddAddress() {
     var addresses = tsp.getAddresses();
     var addrString = jQuery('#manual_address').val() + ' ' + jQuery('#manual_city').val() + ' ' +  jQuery('#manual_state').val() + ' '+ jQuery('#manual_zip').val();
     jQuery.each(addresses, function(k, v) {
          if(v==addrString) {
               return; // Prevent adding duplicate addresses for now
          }
     });
    addAddressAndLabel(jQuery('#manual_address').val(),   jQuery('#manual_city').val(),  jQuery('#manual_state').val(),  jQuery('#manual_zip').val()   , 'Custom Stop', 'purple', null, jQuery('#manual_note').val());
     jQuery('.custAdd').val('');
}

function addAddressSuccessCallback(address, latlng) {
    if (latlng) {
     drawMarkers(false);
    } else {
     //alert('Failed to geocode: ' + address);
    }
}

function addAddressSuccessCallbackZoom(address, latlng) {
    if (latlng) {
     drawMarkers(true);
    } else {
     //alert('Failed to geocode: ' + address);
    }
}

function addWaypointSuccessCallback(latlng) {
    if (latlng) {
     drawMarkers(false);
    }
}

function addWaypointSuccessCallbackZoom(latlng) {
    if (latlng) {
     drawMarkers(true);
    }
}

function drawMarkers(updateViewport) {
    removeOldMarkers();
    var waypoints = tsp.getWaypoints();
    var addresses = tsp.getAddresses();
    var labels = tsp.getLabels();
    var colors = tsp.getColors();
    for (var i = 0; i < waypoints.length; ++i) {
     drawMarker(waypoints[i], addresses[i], labels[i], colors[i], i);
    }
    if (updateViewport) {
     setViewportToCover(waypoints);
    }
}

function startOver() {

    //document.getElementById("my_textual_div").innerHTML = "";
    //document.getElementById("path").innerHTML = "";
    var center = gebMap.getCenter();
    var zoom = gebMap.getZoom();
    var mapDiv = gebMap.getDiv();
    //initMap(center, zoom, mapDiv);
    tsp.startOver();

    jQuery('#routeDrag').html('');
    jQuery('#reverseRoute').html('');
    jQuery('.chk').attr('checked', false); // reset checkboxes
    jQuery('#selected_marker_opts').hide();

    if(start_address != '' ) {
         addAddressAndLabel(start_address, start_city, start_state, start_zip, 'Starting Point');
         drawMarkers(true);
    } else {
         jQuery('#dialog_start_address').dialog('open');
    }

    jQuery('#maintab').tabs('disable', 2);

    /*
    if(start_address != '') {
          addAddressAndLabel(start_address, start_city, start_state, start_zip, 'Starting Point');
     } else {
          addAddressAndLabel('1600 Pennsylvania Avenue Northwest', 'Washington', 'DC', '20500', 'Starting Point');
     }*/

}


function directions(m, walking, avoid) {
     deSelectAllMarkers();

     CALC_ANIMATION_ON = false; // stop animation
     // override m
     if(jQuery('[id$=calcRoundTrip]').is(':checked')) {
             m = 0;
             mode = 0;
        } else {
             m = 1;
             mode = 1;
        }

     var test = tsp.getAddresses();
     if(test.length < 2) {
          return;
     }
     //jQuery('#printButton').show();
    jQuery('#dialogProgress').dialog('open');

    tsp.setAvoidHighways(avoid);
    if (walking) {
          tsp.setTravelMode(google.maps.DirectionsTravelMode.WALKING);
    } else {
          tsp.setTravelMode(google.maps.DirectionsTravelMode.DRIVING);
    }

    tsp.setOnProgressCallback(onProgressCallback);

    if (m == 0) {
          tsp.solveRoundTrip(onSolveCallback);
    } else {
          tsp.solveAtoZ(onSolveCallback);
     }

     jQuery('.route_edit_link').removeClass('ui-state-disabled');
     available_accordion_indexes = [0,1,2,3]; // add the third tab to the available list to enable
}

function getTotalDuration(dir) {
    var sum = 0;
    for (var i = 0; i < dir.legs.length; i++) {
     sum += dir.legs[i].duration.value;
    }
    return sum;
}

function getTotalDistance(dir) {
    var sum = 0;
    for (var i = 0; i < dir.legs.length; i++) {
     sum += dir.legs[i].distance.value;
    }
    return sum;
}

function removeOldMarkers() {
    for (var i = 0; i < markers.length; ++i) {
         if(markers[i] != undefined) {
               markers[i].setMap(null);
         }
    }
    markers = new Array();
}

function onSolveCallback(myTsp) {
  jQuery('#dialogProgress').dialog('close');
    var dirRes = tsp.getGDirections();
    var dir = dirRes.routes[0];
    // Print shortest roundtrip data:

    drawMarkers(false);

    var pathStr = "<div>Trip duration: " + formatTime(getTotalDuration(dir)) + "<br>";
    pathStr += "Trip length: " + formatLengthMiles(getTotalDistance(dir)) +
      " (" +   formatLength(getTotalDistance(dir))+ ")</div>";

    jQuery('[id$=distanceDuration]').val(pathStr);
    jQuery('#route_stops').show().find('.val').html(dir.legs.length.toString());
    jQuery('#route_miles').show().find('.val').html(formatLengthMiles(getTotalDistance(dir)));
    jQuery('#route_hours').show().find('.val').html(formatTime(getTotalDuration(dir)));

    //document.getElementById("path").innerHTML = pathStr;
   // document.getElementById("exportDataButton").innerHTML = "<input id='rawButton' class='calcButton' type='button' value='Toggle raw path output' onClick='toggle(\"exportData\"); document.getElementById(\"outputList\").select();'>";
    var durStr = "<input id='csvButton' class='calcButton' type='button' value='Toggle csv durations matrix' onClick='toggle(\"durationsData\");'>";
    //document.getElementById("durations").innerHTML = durStr;
    //document.getElementById("exportLabelButton").innerHTML = "<input id='rawLabelButton' class='calcButton' type='button' value='Raw path with labels' onClick='toggle(\"exportLabelData\"); document.getElementById(\"outputLabelList\").select();'>"
    //document.getElementById("exportAddrButton").innerHTML = "<input id='rawAddrButton' class='calcButton' type='button' value='Optimal address order' onClick='toggle(\"exportAddrData\"); document.getElementById(\"outputAddrList\").select();'>"
    //document.getElementById("exportOrderButton").innerHTML = "<input id='rawOrderButton' class='calcButton' type='button' value='Optimal numeric order' onClick='toggle(\"exportOrderData\"); document.getElementById(\"outputOrderList\").select();'>"

    var formattedDirections = formatDirections(dir, mode, pathStr);
    document.getElementById("routeDrag").innerHTML = formattedDirections[0];
   // document.getElementById("my_textual_div").innerHTML = formattedDirections[1];
    //document.getElementById("garmin").innerHTML = createGarminLink(dir);
    //document.getElementById("tomtom").innerHTML = createTomTomLink(dir);
    //document.getElementById("exportGoogle").innerHTML = "<input id='googleButton' value='View in Google Maps' type='button' class='calcButton' onClick='window.open(\"" + createGoogleLink(dir) + "\");' />";
    document.getElementById("reverseRoute").innerHTML = "<input id='reverseButton' value='Reverse' type='button' class='calcButton' onClick='reverseRoute()' />";
    jQuery('#reverseButton').button();
    jQuery('#rawButton').button();
    jQuery('#rawLabelButton').button();
    jQuery('#csvButton').button();
    jQuery('#googleButton').button();
    jQuery('#tomTomButton').button();
    jQuery('#garminButton').button();
    jQuery('#rawAddrButton').button();
    jQuery('#rawOrderButton').button();

    jQuery("#sortable").sortable({ stop: function(event, ui) {
       var perm = jQuery("#sortable").sortable("toArray");
       var numPerm = new Array(perm.length + 2);
       numPerm[0] = 0;
       for (var i = 0; i < perm.length; i++) {
         numPerm[i + 1] = parseInt(perm[i]);
       }
       numPerm[numPerm.length - 1] = numPerm.length - 1;
       tsp.reorderSolution(numPerm, onSolveCallback);
     } });
    jQuery("#sortable").disableSelection();
    /*
    for (var i = 1; i < dir.legs.length; ++i) {
      var finalI = i;
      jQuery("#dragClose" + i).button({

          icons: { primary: "ui-icon-close" },
         text: false
         }).click(function() {
          //tsp.removeStop(parseInt(this.value), null);
               tmporder = tsp.getOrder();
          remove_address( tmporder[parseInt(this.value)]);
          });

           jQuery("#dragEdit" + i).button({
               icons: { primary: "ui-icon-pencil" },
              text: false
              }).click(function() {
                    edit_location(jQuery(this).val());
               });
    }
    */


    // Clean up old path.
    if (dirRenderer != null) {
     dirRenderer.setMap(null);
    }
    dirRenderer = new google.maps.DirectionsRenderer({
     directions: dirRes,
     hideRouteList: true,
     map: gebMap,
     panel: null,
     preserveViewport: false,
     suppressInfoWindows: true,
     suppressMarkers: true });

    // Raw path output
    var bestPathLatLngStr = dir.legs[0].start_location.toString() + "\n";
    for (var i = 0; i < dir.legs.length; ++i) {
     bestPathLatLngStr += dir.legs[i].end_location.toString() + "\n";
    }
    document.getElementById("exportData_hidden").innerHTML =
     "<textarea id='outputList' rows='10' cols='40'>"
     + bestPathLatLngStr + "</textarea><br>";

    // Raw path output with labels
    var labels = tsp.getLabels();
    var order = tsp.getOrder();
    var bestPathLabelStr = "";
    if (labels[order[0]] == null) {
      bestPathLabelStr += order[0];
    } else {
      bestPathLabelStr += labels[order[0]];
    }
    bestPathLabelStr += ": " + dir.legs[0].start_location.toString() + "\n";
    for (var i = 0; i < dir.legs.length; ++i) {
      if (labels[order[i+1]] == null) {
     bestPathLabelStr += order[i+1];
      } else {
     bestPathLabelStr += labels[order[i+1]];
      }
      bestPathLabelStr += ": " + dir.legs[i].end_location.toString() + "\n";
    }
    document.getElementById("exportLabelData_hidden").innerHTML =
     "<textarea id='outputLabelList' rows='10' cols='40'>"
      + bestPathLabelStr + "</textarea><br>";

    // Optimal address order
    var addrs = tsp.getAddresses();
    var order = tsp.getOrder();
    var bestPathAddrStr = "";
    if (addrs[order[0]] == null) {
      bestPathAddrStr += dir.legs[0].start_location.toString();
    } else {
      bestPathAddrStr += addrs[order[0]];
    }
    bestPathAddrStr += "\n";
    for (var i = 0; i < dir.legs.length; ++i) {
      if (addrs[order[i+1]] == null) {
     bestPathAddrStr += dir.legs[i].end_location.toString();
      } else {
     bestPathAddrStr += addrs[order[i+1]];
      }
      bestPathAddrStr += "\n";
    }
    document.getElementById("exportAddrData_hidden").innerHTML =
     "<textarea id='outputAddrList' rows='10' cols='40'>"
      + bestPathAddrStr + "</textarea><br>";

    // Optimal numeric order
    var bestOrderStr = "";
    for (var i = 0; i < order.length; ++i) {
      bestOrderStr += "" + (order[i] + 1) + "\n";
    }
    document.getElementById("exportOrderData_hidden").innerHTML =
        "<textarea id='outputOrderList' rows='10' cols='40'>"
      + bestOrderStr + "</textarea><br>";

    var durationsMatrixStr = "";
    var dur = tsp.getDurations();
    for (var i = 0; i < dur.length; ++i) {
     for (var j = 0; j < dur[i].length; ++j) {
         durationsMatrixStr += dur[i][j];
         if (j == dur[i].length - 1) {
          durationsMatrixStr += "\n";
         } else {
          durationsMatrixStr += ", ";
         }
     }
    }
    document.getElementById("durationsData_hidden").innerHTML =
     "<textarea name='csvDurationsMatrix' rows='10' cols='40'>"
     + durationsMatrixStr + "</textarea><br>";

     jQuery('#button_save').attr('disabled', false).removeClass('ui-state-disabled');
     SAVE_ANIMATION_ON = true;
     animate_save_btn('on');

}

function clickedAddList() {
  var val = document.listOfLocations.inputList.value;
  val = val.replace(/\t/g, ' ');
  document.listOfLocations.inputList.value = val;
  addList(val);
}

function addList(listStr) {
    var listArray = listStr.split("\n");
    for (var i = 0; i < listArray.length; ++i) {
     var listLine = listArray[i];
     if (listLine.match(/\(?\s*\-?\d+\s*,\s*\-?\d+/) ||
         listLine.match(/\(?\s*\-?\d+\s*,\s*\-?\d*\.\d+/) ||
         listLine.match(/\(?\s*\-?\d*\.\d+\s*,\s*\-?\d+/) ||
         listLine.match(/\(?\s*\-?\d*\.\d+\s*,\s*\-?\d*\.\d+/)) {
         // Line looks like lat, lng.
         var cleanStr = listLine.replace(/[^\d.,-]/g, "");
         var latLngArr = cleanStr.split(",");
         if (latLngArr.length == 2) {
          var lat = parseFloat(latLngArr[0]);
          var lng = parseFloat(latLngArr[1]);
          var latLng = new google.maps.LatLng(lat, lng);
          tsp.addWaypoint(latLng, addWaypointSuccessCallbackZoom);
         }
     } else if (listLine.match(/\(?\-?\d*\.\d+\s+\-?\d*\.\d+/)) {
       // Line looks like lat lng
         var latLngArr = listline.split(" ");
         if (latLngArr.length == 2) {
          var lat = parseFloat(latLngArr[0]);
          var lng = parseFloat(latLngArr[1]);
          var latLng = new google.maps.LatLng(lat, lng);
          tsp.addWaypoint(latLng, addWaypointSuccessCallbackZoom);
         }
     } else if (listLine.match(/\S+/)) {
         // Non-empty line that does not look like lat, lng. Interpret as address.
         tsp.addAddress(listLine, 'purple', null, addAddressSuccessCallbackZoom);
     }
    }
}

function reverseRoute() {
    tsp.reverseSolution();
}


/*********************************
  This encapsulates reusable functionality for resolving TSP problems on
  Google Maps.
  The authors of this code are James Tolley <info [at] gmaptools.com>
  and Geir K. Engdahl <geir.engdahl (at) gmail.com>

  For the most up-to-date version of this file, see
  http://code.google.com/p/google-maps-tsp-solver/

  To receive updates, subscribe to google-maps-tsp-solver@googlegroups.com

  version 1.0; 05/07/10

  // Usage:
  See http://code.google.com/p/google-maps-tsp-solver/
*/

jQuery(function() {

  var tsp; // singleton
  var gebMap;           // The map DOM object
  var directionsPanel;  // The driving directions DOM object
  var gebDirectionsResult;    // The driving directions returned from GMAP API
  var gebDirectionsService;
  var gebGeocoder;      // The geocoder for addresses
  var maxTspSize = 150;  // A limit on the size of the problem, mostly to save Google servers from undue load.
  var maxTspBF = 0;     // Max size for brute force, may seem conservative, but ma
  var maxTspDynamic = 15;     // Max size for brute force, may seem conservative, but many browsers have limitations on run-time.
  var maxSize = 10 ;     // Max number of waypoints in one Google driving directions request.
  var maxTripSentry = 2000000000; // Approx. 63 years., this long a route should not be reached...
  var avoidHighways = false; // Whether to avoid highways. False by default.
  var travelMode;
  var distIndex;
  var GEO_STATUS_MSG;
  var DIR_STATUS_MSG;
  var waypoints = new Array();
  var addresses = new Array();
  var address_pieces = new Array();
  var labels = new Array();
  var colors = new Array(); // NERD
  var stop_bound = false;
  var distance_by_index = new Array(); // for testing only
  var notes = new Array();
  var casekeys = new Array();
  var queuesize = 0;
  var duplicates = new Array();
  var selected_markers = new Array();
  var addr = new Array();
  var wpActive = new Array();
  var addressRequests = 0;
  var addressProcessing = false;
  var requestNum = 0;
  var currQueueNum = 0;
  var wayArr;
  var legsTmp;
  var distances;
  var durations;
  var legs;
  var dist;
  var dur;
  var visited;
  var currPath;
  var bestPath;
  var bestTrip;
  var nextSet;
  var numActive;
  var costForward;
  var costBackward;
  var improved = false;
  var chunkNode;
  var okChunkNode;
  var numDirectionsComputed = 0;
  var numDirectionsNeeded = 0;
  var cachedDirections = false;
  var requestLimitWait = 1100;
  var callCount = 0;
  var directionsTemplate;
  var vb;  // Object used to store travel info like travel mode etc. Needed for route renderer.



  var onSolveCallback = function(){};
  var onProgressCallback = null;
  var originalOnFatalErrorCallback = function(tsp, errMsg) { alert("Request failed: " + errMsg); }
  var onFatalErrorCallback = originalOnFatalErrorCallback;
  var doNotContinue = false;
  var onLoadListener = null;
  var onFatalErrorListener = null;


   /** Computes greedy (nearest-neighbor solution to the TSP)
   */
  function tspGreedy(mode) {
    var curr = 0;
    var currDist = 0;
    var numSteps = numActive - 1;
    var lastNode = 0;
    var numToVisit = numActive;
    if (mode == 1) {
      numSteps = numActive - 2;
      lastNode = numActive - 1;
      numToVisit = numActive - 1;
    }
    for (var step = 0; step < numSteps; ++step) {
      visited[curr] = true;
      bestPath[step] = curr;
      var nearest = maxTripSentry;
      var nearI = -1;
      for (var next = 1; next < numToVisit; ++next) {
     if (!visited[next] && dur[curr][next] < nearest) {
       nearest = dur[curr][next];
       nearI = next;
     }
      }
      currDist += dur[curr][nearI];
      curr = nearI;
    }
    if (mode == 1) bestPath[numSteps] = lastNode;
    else bestPath[numSteps] = curr;
    currDist += dur[curr][lastNode];
    bestTrip = currDist;
  }

  /** Returns the cost of moving along the current solution path offset
   *  given by a to b. Handles moving both forward and backward.
   */
  function cost(a, b) {
    if (a <= b) {
      return costForward[b] - costForward[a];
    } else {
      return costBackward[b] - costBackward[a];
    }
  }

  /** Returns the cost of the given 3-opt variation of the current solution.
   */
  function costPerm(a, b, c, d, e, f) {
    var A = currPath[a];
    var B = currPath[b];
    var C = currPath[c];
    var D = currPath[d];
    var E = currPath[e];
    var F = currPath[f];
    var g = currPath.length - 1;

    var ret = cost(0, a) + dur[A][B] + cost(b, c) + dur[C][D] + cost(d, e) + dur[E][F] + cost(f, g);
    return ret;
  }

  /** Update the datastructures necessary for cost(a,b) and costPerm to work
   *  efficiently.
   */
  function updateCosts() {
    costForward = new Array(currPath.length);
    costBackward = new Array(currPath.length);

    costForward[0] = 0.0;
    for (var i = 1; i < currPath.length; ++i) {
      costForward[i] = costForward[i-1] + dur[currPath[i-1]][currPath[i]];
    }
    bestTrip = costForward[currPath.length-1];

    costBackward[currPath.length-1] = 0.0;
    for (var i = currPath.length - 2; i >= 0; --i) {
      costBackward[i] = costBackward[i+1] + dur[currPath[i+1]][currPath[i]];
    }
  }

  /** Update the current solution with the given 3-opt move.
   */
  function updatePerm(a, b, c, d, e, f) {
    improved = true;
    var nextPath = new Array(currPath.length);
    for (var i = 0; i < currPath.length; ++i) nextPath[i] = currPath[i];
    var offset = a + 1;
    nextPath[offset++] = currPath[b];
    if (b < c) {
      for (var i = b + 1; i <= c; ++i) {
     nextPath[offset++] = currPath[i];
      }
    } else {
      for (var i = b - 1; i >= c; --i) {
     nextPath[offset++] = currPath[i];
      }
    }
    nextPath[offset++] = currPath[d];
    if (d < e) {
      for (var i = d + 1; i <= e; ++i) {
     nextPath[offset++] = currPath[i];
      }
    } else {
      for (var i = d - 1; i >= e; --i) {
     nextPath[offset++] = currPath[i];
      }
    }
    nextPath[offset++] = currPath[f];
    currPath = nextPath;

    updateCosts();
  }

  /** Uses the 3-opt algorithm to find a good solution to the TSP.
   */
  function tspK3(mode) {
    // tspGreedy(mode);
    currPath = new Array(bestPath.length);
    for (var i = 0; i < bestPath.length; ++i) currPath[i] = bestPath[i];

    updateCosts();
    improved = true;
    while (improved) {
      improved = false;
      for (var i = 0; i < currPath.length - 3; ++i) {
     for (var j = i+1; j < currPath.length - 2; ++j) {
       for (var k = j+1; k < currPath.length - 1; ++k) {
         if (costPerm(i, i+1, j, k, j+1, k+1) < bestTrip)
           updatePerm(i, i+1, j, k, j+1, k+1);
         if (costPerm(i, j, i+1, j+1, k, k+1) < bestTrip)
           updatePerm(i, j, i+1, j+1, k, k+1);
         if (costPerm(i, j, i+1, k, j+1, k+1) < bestTrip)
           updatePerm(i, j, i+1, k, j+1, k+1);
         if (costPerm(i, j+1, k, i+1, j, k+1) < bestTrip)
           updatePerm(i, j+1, k, i+1, j, k+1);
         if (costPerm(i, j+1, k, j, i+1, k+1) < bestTrip)
           updatePerm(i, j+1, k, j, i+1, k+1);
         if (costPerm(i, k, j+1, i+1, j, k+1) < bestTrip)
           updatePerm(i, k, j+1, i+1, j, k+1);
         if (costPerm(i, k, j+1, j, i+1, k+1) < bestTrip)
           updatePerm(i, k, j+1, j, i+1, k+1);
       }
     }
      }
    }
    for (var i = 0; i < bestPath.length; ++i) bestPath[i] = currPath[i];
  }

  /* Computes a near-optimal solution to the TSP problem,
   * using Ant Colony Optimization and local optimization
   * in the form of k2-opting each candidate route.
   * Run time is O(numWaves * numAnts * numActive ^ 2) for ACO
   * and O(numWaves * numAnts * numActive ^ 3) for rewiring?
   *
   * if mode is 1, we start at node 0 and end at node numActive-1.
   */
  function tspAntColonyK2(mode) {
    var alfa = 0.1; // The importance of the previous trails
    var beta = 2.0; // The importance of the durations
    var rho = 0.1;  // The decay rate of the pheromone trails
    var asymptoteFactor = 0.9; // The sharpness of the reward as the solutions approach the best solution
    var pher = new Array();
    var nextPher = new Array();
    var prob = new Array();
    var numAnts = 20;
    var numWaves = 20;
    for (var i = 0; i < numActive; ++i) {
      pher[i] = new Array();
      nextPher[i] = new Array();
    }
    for (var i = 0; i < numActive; ++i) {
      for (var j = 0; j < numActive; ++j) {
     pher[i][j] = 1;
     nextPher[i][j] = 0.0;
      }
    }

    var lastNode = 0;
    var startNode = 0;
    var numSteps = numActive - 1;
    var numValidDests = numActive;
    if (mode == 1) {
      lastNode = numActive - 1;
      numSteps = numActive - 2;
      numValidDests = numActive - 1;
    }
    for (var wave = 0; wave < numWaves; ++wave) {
      for (var ant = 0; ant < numAnts; ++ant) {
     var curr = startNode;
     var currDist = 0;
     for (var i = 0; i < numActive; ++i) {
       visited[i] = false;
     }
     currPath[0] = curr;
     for (var step = 0; step < numSteps; ++step) {
       visited[curr] = true;
       var cumProb = 0.0;
       for (var next = 1; next < numValidDests; ++next) {
         if (!visited[next]) {
           prob[next] = Math.pow(pher[curr][next], alfa) *
          Math.pow(dur[curr][next], 0.0 - beta);
           cumProb += prob[next];
         }
       }
       var guess = Math.random() * cumProb;
       var nextI = -1;
       for (var next = 1; next < numValidDests; ++next) {
         if (!visited[next]) {
           nextI = next;
           guess -= prob[next];
           if (guess < 0) {
          nextI = next;
          break;
           }
         }
       }
       currDist += dur[curr][nextI];
       currPath[step+1] = nextI;
       curr = nextI;
     }
     currPath[numSteps+1] = lastNode;
     currDist += dur[curr][lastNode];

     // k2-rewire:
     var lastStep = numActive;
     if (mode == 1) {
       lastStep = numActive - 1;
     }
     var changed = true;
     var i = 0;
     while (changed) {
       changed = false;
       for (; i < lastStep - 2 && !changed; ++i) {
         var cost = dur[currPath[i+1]][currPath[i+2]];
         var revCost = dur[currPath[i+2]][currPath[i+1]];
         var iCost = dur[currPath[i]][currPath[i+1]];
         var tmp, nowCost, newCost;
         for (var j = i+2; j < lastStep && !changed; ++j) {
           nowCost = cost + iCost + dur[currPath[j]][currPath[j+1]];
           newCost = revCost + dur[currPath[i]][currPath[j]]
          + dur[currPath[i+1]][currPath[j+1]];
           if (nowCost > newCost) {
          currDist += newCost - nowCost;
          // Reverse the detached road segment.
          for (var k = 0; k < Math.floor((j-i)/2); ++k) {
            tmp = currPath[i+1+k];
            currPath[i+1+k] = currPath[j-k];
            currPath[j-k] = tmp;
          }
          changed = true;
          --i;
           }
           cost += dur[currPath[j]][currPath[j+1]];
           revCost += dur[currPath[j+1]][currPath[j]];
         }
       }
     }

     if (currDist < bestTrip) {
       bestPath = currPath;
       bestTrip = currDist;
     }
     for (var i = 0; i <= numSteps; ++i) {
       nextPher[currPath[i]][currPath[i+1]] += (bestTrip - asymptoteFactor * bestTrip) / (numAnts * (currDist - asymptoteFactor * bestTrip));
     }
      }
      for (var i = 0; i < numActive; ++i) {
     for (var j = 0; j < numActive; ++j) {
       pher[i][j] = pher[i][j] * (1.0 - rho) + rho * nextPher[i][j];
       nextPher[i][j] = 0.0;
     }
      }
    }
  }

  /* Returns the optimal solution to the TSP problem.
   * Run-time is O((numActive-1)!).
   * Prerequisites:
   * - numActive contains the number of locations
   * - dur[i][j] contains weight of edge from node i to node j
   * - visited[i] should be false for all nodes
   * - bestTrip is set to a very high number
   *
   * If mode is 1, it will return the optimal solution to the related
   * problem of finding a path from node 0 to node numActive - 1, visiting
   * the in-between nodes in the best order.
   */
  function tspBruteForce(mode, currNode, currLen, currStep) {
    // Set mode parameters:
    var numSteps = numActive;
    var lastNode = 0;
    var numToVisit = numActive;
    if (mode == 1) {
      numSteps = numActive - 1;
      lastNode = numActive - 1;
      numToVisit = numActive - 1;
    }

    // If this route is promising:
    if (currLen + dur[currNode][lastNode] < bestTrip) {

      // If this is the last node:
      if (currStep == numSteps) {
     currLen += dur[currNode][lastNode];
     currPath[currStep] = lastNode;
     bestTrip = currLen;
     for (var i = 0; i <= numSteps; ++i) {
       bestPath[i] = currPath[i];
     }
      } else {

     // Try all possible routes:
     for (var i = 1; i < numToVisit; ++i) {
       if (!visited[i]) {
         visited[i] = true;
         currPath[currStep] = i;
         tspBruteForce(mode, i, currLen+dur[currNode][i], currStep+1);
         visited[i] = false;
       }
     }
      }
    }
  }

  /* Finds the next integer that has num bits set to 1.
   */
  function nextSetOf(num) {
    var count = 0;
    var ret = 0;
    for (var i = 0; i < numActive; ++i) {
      count += nextSet[i];
    }
    if (count < num) {
      for (var i = 0; i < num; ++i) {
     nextSet[i] = 1;
      }
      for (var i = num; i < numActive; ++i) {
     nextSet[i] = 0;
      }
    } else {
      // Find first 1
      var firstOne = -1;
      for (var i = 0; i < numActive; ++i) {
     if (nextSet[i]) {
       firstOne = i;
       break;
     }
      }
      // Find first 0 greater than firstOne
      var firstZero = -1;
      for (var i = firstOne + 1; i < numActive; ++i) {
     if (!nextSet[i]) {
       firstZero = i;
       break;
     }
      }
      if (firstZero < 0) {
     return -1;
      }
      // Increment the first zero with ones behind it
      nextSet[firstZero] = 1;
      // Set the part behind that one to its lowest possible value
      for (var i = 0; i < firstZero - firstOne - 1; ++i) {
     nextSet[i] = 1;
      }
      for (var i = firstZero - firstOne - 1; i < firstZero; ++i) {
     nextSet[i] = 0;
      }
    }
    // Return the index for this set
    for (var i = 0; i < numActive; ++i) {
      ret += (nextSet[i]<<i);
    }
    return ret;
  }

  /* Solves the TSP problem to optimality. Memory requirement is
   * O(numActive * 2^numActive)
   */
  function tspDynamic(mode) {
    var numCombos = 1<<numActive;
    var C = new Array();
    var parent = new Array();
    for (var i = 0; i < numCombos; ++i) {
      C[i] = new Array();
      parent[i] = new Array();
      for (var j = 0; j < numActive; ++j) {
     C[i][j] = 0.0;
     parent[i][j] = 0;
      }
    }
    for (var k = 1; k < numActive; ++k) {
      var index = 1 + (1<<k);
      C[index][k] = dur[0][k];
    }
    for (var s = 3; s <= numActive; ++s) {
      for (var i = 0; i < numActive; ++i) {
     nextSet[i] = 0;
      }
      var index = nextSetOf(s);
      while (index >= 0) {
     for (var k = 1; k < numActive; ++k) {
       if (nextSet[k]) {
         var prevIndex = index - (1<<k);
         C[index][k] = maxTripSentry;
         for (var m = 1; m < numActive; ++m) {
           if (nextSet[m] && m != k) {
          if (C[prevIndex][m] + dur[m][k] < C[index][k]) {
            C[index][k] = C[prevIndex][m] + dur[m][k];
            parent[index][k] = m;
          }
           }
         }
       }
     }
     index = nextSetOf(s);
      }
    }
    for (var i = 0; i < numActive; ++i) {
      bestPath[i] = 0;
    }
    var index = (1<<numActive)-1;
    if (mode == 0) {
      var currNode = -1;
      bestPath[numActive] = 0;
      for (var i = 1; i < numActive; ++i) {
     if (C[index][i] + dur[i][0] < bestTrip) {
       bestTrip = C[index][i] + dur[i][0];
       currNode = i;
     }
      }
      bestPath[numActive-1] = currNode;
    } else {
      var currNode = numActive - 1;
      bestPath[numActive-1] = numActive - 1;
      bestTrip = C[index][numActive-1];
    }
    for (var i = numActive - 1; i > 0; --i) {
      currNode = parent[index][currNode];
      index -= (1<<bestPath[i]);
      bestPath[i-1] = currNode;
    }
  }

  function makeLatLng(latLng) {
    return(latLng.toString().substr(1,latLng.toString().length-2));
  }

  function makeDirWp(latLng, address) {
	    if (typeof latLng!= 'undefined' && latLng != null && latLng != ""){
			return ({ location: latLng,
	       stopover: true });
		 }
		return ({ location: address, stopover: true });
	  }

  function getWayArr(curr) {
    var nextAbove = -1;
    for (var i = curr + 1; i < waypoints.length; ++i) {
      if (wpActive[i]) {
     if (nextAbove == -1) {
       nextAbove = i;
     } else {
       wayArr.push(makeDirWp(waypoints[i], addresses[i]));
       wayArr.push(makeDirWp(waypoints[curr], addresses[curr]));
     }
      }
    }
    if (nextAbove != -1) {
      wayArr.push(makeDirWp(waypoints[nextAbove], addresses[nextAbove]));
      getWayArr(nextAbove);
      wayArr.push(makeDirWp(waypoints[curr], addresses[curr]));
    }
  }

  function getDistTable(curr, currInd) {
    var nextAbove = -1;
    var index = currInd;
    for (var i = curr + 1; i < waypoints.length; ++i) {
      if (wpActive[i]) {
     index++;
     if (nextAbove == -1) {
       nextAbove = i;
     } else {
       legs[currInd][index] = legsTmp[distIndex];
       dist[currInd][index] = distances[distIndex];
       dur[currInd][index] = durations[distIndex++];
       legs[index][currInd] = legsTmp[distIndex];
       dist[index][currInd] = distances[distIndex];
       dur[index][currInd] = durations[distIndex++];
     }
      }
    }
    if (nextAbove != -1) {
      legs[currInd][currInd+1] = legsTmp[distIndex];
      dist[currInd][currInd+1] = distances[distIndex];
      dur[currInd][currInd+1] = durations[distIndex++];
      getDistTable(nextAbove, currInd+1);
      legs[currInd+1][currInd] = legsTmp[distIndex];
      dist[currInd+1][currInd] = distances[distIndex];
      dur[currInd+1][currInd] = durations[distIndex++];
    }
  }

  function directions(mode) {
    if (cachedDirections) {
      // Bypass Google directions lookup if we already have the distance
      // and duration matrices.
      doTsp(mode);
    }
    wayArr = new Array();
    numActive = 0;
    numDirectionsComputed = 0;
    for (var i = 0; i < waypoints.length; ++i) {
      if (wpActive[i]) ++numActive;
    }
    numDirectionsNeeded = numActive * (numActive - 1);

    for (var i = 0; i < waypoints.length; ++i) {
      if (wpActive[i]) {
     wayArr.push(makeDirWp(waypoints[i], addresses[i]));
     getWayArr(i);
     break;
      }
    }

    // Roundtrip
    if (numActive > maxTspSize) {
      alert("Too many locations! You have " + numActive + ", but max limit is " + maxTspSize);
    } else {
      legsTmp = new Array();
      distances = new Array();
      durations = new Array();
      chunkNode = 0;
      okChunkNode = 0;
      if (typeof onProgressCallback == 'function') {
     onProgressCallback(tsp);
      }
      nextChunk(mode);
    }
  }

  function nextChunk(mode) {
    //  alert("nextChunk");
    chunkNode = okChunkNode;
    if (chunkNode < wayArr.length) {
      var wayArrChunk = new Array();
      for (var i = 0; i < maxSize && i + chunkNode < wayArr.length; ++i) {
     wayArrChunk.push(wayArr[chunkNode+i]);
      }
      var origin;
      var destination;
      origin = wayArrChunk[0].location;
      destination = wayArrChunk[wayArrChunk.length-1].location;
      var wayArrChunk2 = new Array();
      for (var i = 1; i < wayArrChunk.length - 1; i++) {
     wayArrChunk2[i-1] = wayArrChunk[i];
      }
      chunkNode += maxSize;
      if (chunkNode < wayArr.length-1) {
     chunkNode--;
      }

      var myGebDirections = new google.maps.DirectionsService();
      callCount++;
      //console.log(callCount);
       myGebDirections.route({
     origin: origin,
         destination: destination,
         waypoints: wayArrChunk2,
         avoidHighways: avoidHighways,
         unitSystem: google.maps.UnitSystem.IMPERIAL,
         travelMode: travelMode },
     function(directionsResult, directionsStatus) {
       if (directionsStatus == google.maps.DirectionsStatus.OK) {
      //alert("Request completed!");
      // Save legs, distances and durations
      directionsTemplate = directionsResult;
      for (var i = 0; i < directionsResult.routes[0].legs.length; ++i) {
        ++numDirectionsComputed;
        legsTmp.push(directionsResult.routes[0].legs[i]);
        durations.push(directionsResult.routes[0].legs[i].duration.value);
        distances.push(directionsResult.routes[0].legs[i].distance.value);
      }
      onProgressCallback(tsp);
      okChunkNode = chunkNode;

      if(callCount%10 == 0){
    	  //console.log('Periodic Wait');
    	  setTimeout(function(){ nextChunk(mode) }, requestLimitWait);
      }else{
    	  setTimeout(function(){ nextChunk(mode) }, 110);
      }
    } else if (directionsStatus == google.maps.DirectionsStatus.OVER_QUERY_LIMIT) {
    	//console.log('Over Limit Wait');
    	setTimeout(function(){ nextChunk(mode) }, 2*requestLimitWait);
        } else {
        var errorMsg = "";
        if(directionsStatus == "ZERO_RESULTS")
          errorMsg = "No route could be found between the origin and destination." ;//
        else
          errorMsg = DIR_STATUS_MSG[directionsStatus];
        var doNotContinue = true;
        alert("Request failed: " + errorMsg);
       }
     });
    } else {
      readyTsp(mode);
    }
  }

  function readyTsp(mode) {
    //alert("readyTsp");
    // Get distances and durations into 2-d arrays:
    distIndex = 0;
    legs = new Array();
    dist = new Array();
    dur = new Array();
    numActive = 0;
    for (var i = 0; i < waypoints.length; ++i) {
      if (wpActive[i]) {
     legs.push(new Array());
     dist.push(new Array());
     dur.push(new Array());
     addr[numActive] = addresses[i];
     numActive++;
      }
    }
    for (var i = 0; i < numActive; ++i) {
      legs[i][i] = null;
      dist[i][i] = 0;
      dur[i][i] = 0;
    }
    for (var i = 0; i < waypoints.length; ++i) {
      if (wpActive[i]) {
     getDistTable(i, 0);
     break;
      }
    }

    doTsp(mode);
  }

  function doTsp(mode) {
    //alert("doTsp");
    // Calculate shortest roundtrip:
    visited = new Array();
    for (var i = 0; i < numActive; ++i) {
      visited[i] = false;
    }
    currPath = new Array();
    bestPath = new Array();
    nextSet = new Array();
    bestTrip = maxTripSentry;
    visited[0] = true;
    currPath[0] = 0;
    cachedDirections = true;
    if (numActive <= maxTspBF + mode) {
      tspBruteForce(mode, 0, 0, 1);
    } else if (numActive <= maxTspDynamic + mode) {
      tspDynamic(mode);
    } else {
      tspAntColonyK2(mode);
      tspK3(mode);
    }

    prepareSolution();
  }

  function prepareSolution() {
    var wpIndices = new Array();
    for (var i = 0; i < waypoints.length; ++i) {
      if (wpActive[i]) {
     wpIndices.push(i);
      }
    }
    var bestPathLatLngStr = "";
    var directionsResultLegs = new Array();
    var directionsResultRoutes = new Array();
    var directionsResultOverview = new Array();
    var directionsResultBounds = new google.maps.LatLngBounds();
    for (var i = 1; i < bestPath.length; ++i) {
      directionsResultLegs.push(legs[bestPath[i-1]][bestPath[i]]);
    }
    for (var i = 0; i < bestPath.length; ++i) {
      bestPathLatLngStr += makeLatLng(waypoints[wpIndices[bestPath[i]]]) + "\n";
      directionsResultBounds.extend(waypoints[wpIndices[bestPath[i]]]);
      directionsResultOverview.push(waypoints[wpIndices[bestPath[i]]]);
    }
    directionsResultRoutes.push({
      legs: directionsResultLegs,
       bounds: directionsResultBounds,
    copyrights: "Map data 2012 Google",
    overview_path: directionsResultOverview,
    warnings: new Array()
     });
    gebDirectionsResult = directionsTemplate;
    gebDirectionsResult.routes = directionsResultRoutes;
    if (onFatalErrorListener)
      google.maps.event.removeListener(onFatalErrorListener);
    onFatalErrorListener = google.maps.event.addListener(gebDirectionsService, 'error', onFatalErrorCallback);

    if (typeof onSolveCallback == 'function') {
      onSolveCallback(tsp);
    }
  }


  function reverseSolution() {
    for (var i = 0; i < bestPath.length / 2; ++i) {
      var tmp = bestPath[bestPath.length-1-i];
      bestPath[bestPath.length-1-i] = bestPath[i];
      bestPath[i] = tmp;
    }
    prepareSolution();
  }

  function reorderSolution(newOrder) {
       if(bestPath == undefined) {
            return;
       }
    var newBestPath = new Array(bestPath.length);
    for (var i = 0; i < bestPath.length; ++i) {
      newBestPath[i] = bestPath[newOrder[i]];
    }
    bestPath = newBestPath;
    prepareSolution();
  }

  function removeStop(number) {
       if(bestPath == undefined){
            return;
       }
       if(bestPath.length == 0) {
            return;
       }

    var newBestPath = new Array(bestPath.length - 1);
    for (var i = 0; i < bestPath.length; ++i) {
      if (i != number) {
     newBestPath[i - (i > number ? 1 : 0)] = bestPath[i];
      }
    }
    bestPath = newBestPath;
    prepareSolution();
  }

  function addWaypoint(latLng, label) {
    var freeInd = -1;
    for (var i = 0; i < waypoints.length; ++i) {
      if (!wpActive[i]) {
               freeInd = i;
               break;
      }
    }
    if (freeInd == -1) {
      if (waypoints.length < maxTspSize) {
          waypoints.push(latLng);
          labels.push(label);
          wpActive.push(true);
          freeInd = waypoints.length-1;
      } else {
          return(-1);
      }
    } else {
      waypoints[freeInd] = latLng;
      labels[freeInd] = label;
      wpActive[freeInd] = true;
    }
    return(freeInd);
  }

 function clearPaths() {

            visited = new Array();
         distances = new Array();
         durations = new Array();
         legs = new Array();
         dist = new Array();
         dur = new Array();
         nextSet = new Array();
         currPath = new Array();
         bestPath = new Array();
         nextSet = new Array();
         bestTrip = maxTripSentry;
         visited[0] = true;
         currPath[0] = 0;
         cachedDirections = false;
            if(typeof dirRenderer != 'undefined') {
              dirRenderer.setMap(null);
         }
  }

     function getLastGoodIndex(start) {
          for (var i = start; i > 0; --i) {
                if (wpActive[i]) {
                    return i;
                }
         }
     }


  function addAddress(address_st, city, state, zip, label, color, casekey, callback, note) {
              addressProcessing = true;

              gebGeocoder.geocode({ address: address_st +' '+ city +' '+ state +' '+ zip }, function(results, status) {
               if (status == google.maps.GeocoderStatus.OK) {
                 addressProcessing = false;
                 --addressRequests;
                 ++currQueueNum;
                 if (results.length >= 1) {
                   var latLng = results[0].geometry.location;

                   var WO_List = getWO_List();

                   // Here we check for a duplicated LatLng
                   for(var x=0; x < waypoints.length ; x++) {
                        if(wpActive[x] && waypoints[x].equals(latLng)) {
                             // don't allow duplication on non-workordered stops
                             if(casekeys[x] != undefined) {

                                  if(jQuery.inArray(casekey, duplicates[x])==-1) {
                                       // add a duplication record with the work order number that is being added to the stop
                                       if(duplicates[x] == undefined) {
                                            // add an entry to the duplicates list
                                            duplicates[x] = new Array(casekey);
                                       } else {
                                            duplicates[x].push(casekey);
                                       }

                                        // use the highest priority color, Urgent, Red, or Yellow, anything else don't bother
                                       for(var i=0; i<duplicates[x].length; i++) {
                                            if(WO_List[duplicates[x][i]].Color=='Urgent') {
                                                 colors[x] = 'Urgent';
                                            } else if(WO_List[duplicates[x][i]].Color=='Red') {
                                                 colors[x] = 'Red';
                                            } else if(WO_List[duplicates[x][i]].Color=='Yellow') {
                                                 colors[x] = 'Yellow';
                                            }
                                       }

                                       // Duplicate Added, to be handled elsewhere
                                       if( addressRequests == 0 ) {
                                              callback(address, latLng);
                                              must_recalculate();
                                             anyBlacks();
                                       }
                                   }
                                  return;


                             } else {
                                  alert('Duplicate address not allowed for a non work-ordered stop');
                                  return;
                             }
                        }
                   }



                   var freeInd = addWaypoint(latLng, label);
                   if(address==undefined){
                        var address = '';
                   }
                   address = address_st + ' ' + city + ' ' + state + ' ' + zip;
                   address = address.replace("'", "");
                   address = address.replace("\"", "");
                   addresses[freeInd] = address;
                   address_pieces[freeInd] = new Array(address_st, city, state, zip);

                   colors[freeInd] = color;
                   casekeys[freeInd] = casekey;
                   notes[freeInd] = note;

                   //testing
                   distance_by_index[freeInd] = google.maps.geometry.spherical.computeDistanceBetween (waypoints[0], latLng);

                   // Keep the most distant waypoint at the end
                  if(latLng && waypoints.length > 2) {
                            currentDistance = google.maps.geometry.spherical.computeDistanceBetween (waypoints[0], latLng);
                            prevIndex = getLastGoodIndex(freeInd-1);
                            if(prevIndex > 0) {
                                 lastDistance = google.maps.geometry.spherical.computeDistanceBetween (waypoints[0], waypoints[prevIndex]);

                                 //console.log('Compare ' + freeInd + ' with ' + prevIndex);

                                 //console.log(currentDistance + ' <? ' + lastDistance);

                                 if(currentDistance < lastDistance) {
                                      swapWaypoints(freeInd, prevIndex);
                              }
                            }
                  }
                  ////console.log(distance_by_index);

                   if (typeof callback == 'function' && addressRequests == 0 ) {
                        //correctStops();
                          callback(address, latLng);
                          must_recalculate();
                         anyBlacks();
                   }
                 }
               } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                 setTimeout(function(){ addAddress(address_st, city, state, zip, label, color, casekey, callback, note) }, 100);
               } else {
                 --addressRequests;
               //  alert("Failed to geocode address: " + address + ". Reason: " + GEO_STATUS_MSG[status]);
                 //alert("Failed to geocode address: " + address + ". Reason: " +status );
                 alert('Could not geocode the address you entered: ' + address_st +' '+ city +' '+state+' '+zip);
                 ++currQueueNum;
                 addressProcessing = false;
                 if(markers.length==0) {
                      failedGeocodeStartAddress();
                 } else
                 if (typeof(callback) == 'function')
                   callback(address);
               }

      });
  }


  function addAddressJSON(casekey) {
            if(queuesize > 0) {
            --queuesize;
            //console.log('Q size: ' + queuesize);
            }
            var WO_List = getWO_List();

            // Populate values from the old way of adding addresses, with function parameters
            address_st = WO_List[casekey].Address;
            city = WO_List[casekey].City;
            state = WO_List[casekey].State;
            zip = WO_List[casekey].Zip;
            label =  WO_List[casekey].Address_concat;
            color = WO_List[casekey].Color;
            note = WO_List[casekey].Notes;

         var latLng = new google.maps.LatLng(WO_List[casekey].LAT, WO_List[casekey].LNG);

         // Here we check for a duplicated LatLng
         for(var x=0; x < waypoints.length ; x++) {
              if(wpActive[x] && waypoints[x].equals(latLng)) {
                   // don't allow duplication on non-workordered stops
                   if(casekeys[x] != undefined) {

                        if(jQuery.inArray(casekey, duplicates[x])==-1) {
                             // add a duplication record with the work order number that is being added to the stop
                             if(duplicates[x] == undefined) {
                                  // add an entry to the duplicates list
                                  duplicates[x] = new Array(casekey);
                             } else {
                                  duplicates[x].push(casekey);
                             }

                              // use the highest priority color, Urgent, Red, or Yellow, anything else don't bother
                             for(var i=0; i<duplicates[x].length; i++) {
                                  if(WO_List[duplicates[x][i]].Color=='Urgent') {
                                       colors[x] = 'Urgent';
                                  } else if(WO_List[duplicates[x][i]].Color=='Red') {
                                       colors[x] = 'Red';
                                  } else if(WO_List[duplicates[x][i]].Color=='Yellow') {
                                       colors[x] = 'Yellow';
                                  }
                             }

                             // Duplicate Added, to be handled elsewhere
                             /*
                             if( addressRequests == 0 ) {
                                    callback(address, latLng);
                                    must_recalculate();
                                   anyBlacks();
                             }
                             */

                         }
                        return;


                   } else {
                        alert('Duplicate address not allowed for a non work-ordered stop');
                   }
              }
         }



         var freeInd = addWaypoint(latLng, label);
         if(address==undefined){
              var address = '';
         }
         /*address = address_st + ' ' + city + ' ' + state + ' ' + zip;
         address = address.replace("'", "");
         address = address.replace("\"", "");
         addresses[freeInd] = address;*/
         addresses[freeInd] = WO_List[casekey].Address_concat;
         address_pieces[freeInd] = new Array(address_st, city, state, zip);

        //console.log('add address:: '+ address + ' ::freeind:: ' + freeInd);

         colors[freeInd] = color;
         casekeys[freeInd] = casekey;
         notes[freeInd] = note;

         //testing
         distance_by_index[freeInd] = google.maps.geometry.spherical.computeDistanceBetween (waypoints[0], latLng);

         // Keep the most distant waypoint at the end
        if(latLng && waypoints.length > 2) {
                  currentDistance = google.maps.geometry.spherical.computeDistanceBetween (waypoints[0], latLng);
                  prevIndex = getLastGoodIndex(freeInd-1);
                  if(prevIndex > 0) {
                       lastDistance = google.maps.geometry.spherical.computeDistanceBetween (waypoints[0], waypoints[prevIndex]);

                       //console.log('Compare ' + freeInd + ' with ' + prevIndex);

                       //console.log(currentDistance + ' ? ' + lastDistance);

                       if(currentDistance < lastDistance) {
                            swapWaypoints(freeInd, prevIndex);
                    }
                  }
        }
        ////console.log(distance_by_index);
        /*
        if (queuesize == 0 ) {
                  //console.log('Address Requests: ' + addressRequests);
               //addAddressSuccessCallbackZoom(address, latLng);
                drawMarkers(true);
                must_recalculate();
               anyBlacks();
        }
        */
     }



  function updateAddress(latlng, address_st, city, state, zip) {
                 addressProcessing = true;
                 for(var x=0; x<waypoints.length;x++) {
                      if(wpActive[x] && waypoints[x]==latlng) {
                           index = x;
                      }
                 }
              gebGeocoder.geocode({ address: address_st +' '+ city +' '+ state +' '+ zip }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                      addressProcessing = false;
                      --addressRequests;
                      ++currQueueNum;
                      if (results.length >= 1) {
                        var latLng = results[0].geometry.location;

                       // alert(index);

                        waypoints[index] = latLng;
                       // drawMarkers(true);
                        address = address_st + ' ' + city + ' ' + state + ' ' + zip;
                        address = address.replace("'", "");
                        address = address.replace("\"", "");
                        addresses[index] = address;

                       // alert(address);

                        labels[index] = address_st + '<br>' + city + ', '+ state +' '+ zip;

                        address_pieces[index] = [address_st, city, state, zip];

                        markers[index].setMap(null);
                         drawMarker(waypoints[index], addresses[index], labels[index], colors[index], index);

                         cancel_directions();
                         must_recalculate();
                         correctStops();
                         //reCalculate();

                      }
                    } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                      setTimeout(function(){ updateAddress(latlng, address, city, state, zip) }, 100);
                    } else {
                      --addressRequests;
                      alert("Failed to geocode address: " + address + ". Reason: " + GEO_STATUS_MSG[status]);
                      ++currQueueNum;
                      addressProcessing = false;
                      if(index==0) {
                           failedGeocodeStartAddress();
                      }
                      //if (typeof(callback) == 'function')
                       // callback(address);
                    }
      });

  }

  function swapWaypoints(i, j) {
       if(i==0 || j==0) return;

    var tmpAddr = addresses[j];
    var tmpWaypoint = waypoints[j];
    var tmpActive = wpActive[j];
    var tmpLabel = labels[j];
    var tmpColor = colors[j];
    var tmpkey = casekeys[j];
    var tmpMarker = markers[j];
    var tmpSelected = selected_markers[j];
    var tmpNote = notes[j];
    var tmpPieces = address_pieces[j];
    var tmpDuplicate = duplicates[j];
    address_pieces[j] = address_pieces[i];
    address_pieces[i] = tmpPieces;
    addresses[j] = addresses[i];
    addresses[i] = tmpAddr;
    waypoints[j] = waypoints[i];
    waypoints[i] = tmpWaypoint;
    wpActive[j] = wpActive[i];
    wpActive[i] = tmpActive;
    labels[j] = labels[i];
    labels[i] = tmpLabel;
    colors[j] = colors[i];
    colors[i] = tmpColor;
    casekeys[j] = casekeys[i];
    casekeys[i] = tmpkey;
    markers[j] = markers[i];
    markers[i] = tmpMarker;
    selected_markers[j] = selected_markers[i];
    selected_markers[i] = tmpSelected;
    notes[j] = notes[i];
    notes[i] = tmpNote;
    duplicates[j] = duplicates[i];
    duplicates[i] = tmpDuplicate;

    tmpDist = distance_by_index[j]
    distance_by_index[j] = distance_by_index[i];
    distance_by_index[i] = tmpDist;
  }

  function correctStops() {
       if(waypoints.length < 2)  {
            //console.log('Not enough waypoints');
            return;
       }
       //console.log('Correcting stops!!');
       // Keep the most distant waypoint at the end
          lastIndex = getLastGoodIndex(waypoints.length-1);
        for(var x=1;x<waypoints.length;x++) {
                  if(wpActive[x]) {
                       currentDistance = google.maps.geometry.spherical.computeDistanceBetween (waypoints[0], waypoints[x]);
                       lastDistance = google.maps.geometry.spherical.computeDistanceBetween (waypoints[0], waypoints[lastIndex]);

                       if(currentDistance > lastDistance) {
                            //console.log('swapping '+ x +' - ' + lastIndex);
                            swapWaypoints(x, lastIndex);
                       }

                  }
        }
        //console.log(distance_by_index);
  }



  // Keeps the most distant waypoint as the end point (for A-Z optimizations)
  BpTspSolver.prototype.correctStops = function() {
        correctStops();
  }

  BpTspSolver.prototype.startOver = function() {
    GEO_STATUS_MSG = new Array();
    GEO_STATUS_MSG[google.maps.GeocoderStatus.OK] = "Success.";
    GEO_STATUS_MSG[google.maps.GeocoderStatus.INVALID_REQUEST] = "Request was invalid.";
    GEO_STATUS_MSG[google.maps.GeocoderStatus.ERROR] = "There was a problem contacting the Google servers.";
    GEO_STATUS_MSG[google.maps.GeocoderStatus.OVER_QUERY_LIMIT] = "The webpage has gone over the requests limit in too short a period of time.";
    GEO_STATUS_MSG[google.maps.GeocoderStatus.REQUEST_DENIED] = "The webpage is not allowed to use the geocoder.";
    GEO_STATUS_MSG[google.maps.GeocoderStatus.UNKNOWN_ERROR] = "A geocoding request could not be processed due to a server error. The request may succeed if you try again.";
    GEO_STATUS_MSG[google.maps.GeocoderStatus.ZERO_RESULTS] = "No result was found for this GeocoderRequest.";
    DIR_STATUS_MSG = new Array();
    DIR_STATUS_MSG[google.maps.DirectionsStatus.INVALID_REQUEST] = "The DirectionsRequest provided was invalid.";
    DIR_STATUS_MSG[google.maps.DirectionsStatus.MAX_WAYPOINTS_EXCEEDED] = "Too many DirectionsWaypoints were provided in the DirectionsRequest. The total allowed waypoints is 8, plus the origin and destination.";
    DIR_STATUS_MSG[google.maps.DirectionsStatus.NOT_FOUND] = "At least one of the origin, destination, or waypoints could not be geocoded.";
    DIR_STATUS_MSG[google.maps.DirectionsStatus.OK] = "The response contains a valid DirectionsResult.";
    DIR_STATUS_MSG[google.maps.DirectionsStatus.OVER_QUERY_LIMIT] = "The webpage has gone over the requests limit in too short a period of time.";
    DIR_STATUS_MSG[google.maps.DirectionsStatus.REQUEST_DENIED] = "The webpage is not allowed to use the directions service.";
    DIR_STATUS_MSG[google.maps.DirectionsStatus.UNKNOWN_ERROR] = "A directions request could not be processed due to a server error. The request may succeed if you try again.";
    DIR_STATUS_MSG[google.maps.DirectionsStatus.ZERO_RESULTS] = "No route could be found between the origin and destination.";
    waypoints = new Array();
    addresses = new Array();
    address_pieces = new Array();
    labels = new Array();
    colors = new Array();
    notes = new Array();
    casekeys = new Array();
    selected_markers = new Array();
    addr = new Array();
    wpActive = new Array();
    wayArr = new Array();
    legsTmp = new Array();
    distances = new Array();
    durations = new Array();
    legs = new Array();
    dist = new Array();
    dur = new Array();
    visited = new Array();
    currPath = new Array();
    bestPath = new Array();
    bestTrip = new Array();
    nextSet = new Array();
    travelMode = google.maps.DirectionsTravelMode.DRIVING;
    numActive = 0;
    chunkNode = 0;
    okChunkNode = 0;
    addressRequests = 0;
    addressProcessing = false;
    requestNum = 0;
    currQueueNum = 0;
    cachedDirections = false;
    onSolveCallback = function(){};
    onProgressCallback = null;
    doNotContinue = false;
  }

  /* end (edited) OptiMap code */
  /* start public interface */

  function BpTspSolver(map, onFatalError) {
    if (tsp) {
      alert('You can only create one BpTspSolver at a time.');
      return;
    }

    gebMap               = map;
    //directionsPanel      = panel;
    gebGeocoder          = new google.maps.Geocoder();
    gebDirectionsService = new google.maps.DirectionsService();
    onFatalErrorCallback = onFatalError; // only for fatal errors, not geocoding errors
    tsp                  = this;
  }

  BpTspSolver.prototype.clearAll = function() {
       if(tsp.getOrder() != undefined)     {
            clearPaths();
            document.getElementById("routeDrag").innerHTML = '';
         //document.getElementById("my_textual_div").innerHTML = '';
      }
  }

  BpTspSolver.prototype.removeDuplicate = function(WO, index) {
            if(duplicates[index] == undefined) {
                 return;
            }

            var newDup = new Array();

            for(var x=0; x<duplicates[index].length; x++) {
                 if(duplicates[index][x]!=WO) {
                      newDup.push(duplicates[index][x]);
                 }
            }

            duplicates[index] = newDup;
  }


  BpTspSolver.prototype.prepareSolution = function() {
       prepareSolution();
  }

  BpTspSolver.prototype.setQueueSize = function(size) {
       queuesize = size;
  }

  BpTspSolver.prototype.getQueueSize = function() {
       return queuesize;
  }

  BpTspSolver.prototype.addAddressJson = function(casekey) {
       tsp.addAddressJsonAgain(casekey);
  }

  BpTspSolver.prototype.addAddressJsonAgain = function(casekey) {
       if(addressProcessing) {
            setTimeout(function() { tsp.addAddressJsonAgain(casekey); }, 100);
            return;
       }
       addAddressJSON(casekey);
	   	   if (queuesize == 0 ) {
		  //console.log('Address Requests: ' + addressRequests);
	   //addAddressSuccessCallbackZoom(address, latLng);
		drawMarkers(true);
		must_recalculate();
	   anyBlacks();
}
  }

  BpTspSolver.prototype.addAddressWithLabel = function(address,city,state, zip, label, color, casekey, callback, note) {
    ++addressRequests;
    ++requestNum;
    tsp.addAddressAgain(address,city, state, zip, label, color, casekey, callback, requestNum - 1, note);
  }

  BpTspSolver.prototype.updateAddress = function(latlng, address, city, state, zip) {
       ++addressRequests;
       ++requestNum;
       updateAddress(latlng, address, city, state, zip);
  }

  BpTspSolver.prototype.addAddress = function(address, city, state, zip, callback, note) {
    tsp.addAddressWithLabel(address, city, state, zip, address, 'purple' , null,  callback, note);
  };

  BpTspSolver.prototype.addAddressAgain = function(address, city, state, zip, label, color, casekey, callback, queueNum, note) {
    if (addressProcessing || queueNum > currQueueNum) {
      setTimeout(function(){ tsp.addAddressAgain(address, city, state, zip, label, color, casekey, callback, queueNum, note) }, 100);
      return;
    }
    addAddress(address, city, state, zip, label, color, casekey, callback, note);
  };

  BpTspSolver.prototype.addWaypointWithLabel = function(latLng, label, callback) {
    ++requestNum;
    tsp.addWaypointAgain(latLng, label, callback, requestNum - 1);
  };

  BpTspSolver.prototype.addWaypoint = function(latLng, callback) {
    tsp.addWaypointWithLabel(latLng, null, callback);
  };

  BpTspSolver.prototype.addWaypointAgain = function(latLng, label, callback, queueNum) {
    if (addressProcessing || queueNum > currQueueNum) {
      setTimeout(function(){ tsp.addWaypointAgain(latLng, label, callback, queueNum) }, 100);
      return;
    }
    addWaypoint(latLng, label);
    ++currQueueNum;
    if (typeof(callback) == 'function') {
      callback(latLng);
    }
  }

  BpTspSolver.prototype.getWaypoints = function() {
    var wp = [];
    for (var i = 0; i < waypoints.length; i++) {
      if (wpActive[i]) {
     wp.push(waypoints[i]);
      }
    }
    return wp;
  };

  BpTspSolver.prototype.getAddresses = function() {
    var addrs = [];
    for (var i = 0; i < addresses.length; i++) {
      if (wpActive[i])
     addrs.push(addresses[i]);
    }
    return addrs;
  };

   BpTspSolver.prototype.getAddressPieces = function() {
    var addrs = [];
    for (var i = 0; i < address_pieces.length; i++) {
      if (wpActive[i])
     addrs.push(address_pieces[i]);
    }
    return addrs;
  };

  BpTspSolver.prototype.getLabels = function() {
    var labs = [];
    for (var i = 0; i < labels.length; i++) {
      if (wpActive[i])
     labs.push(labels[i]);
    }
    return labs;
  };


  BpTspSolver.prototype.getColors = function() {
    var cols = [];
    for (var i = 0; i < colors.length; i++) {
      if (wpActive[i])
          cols.push(colors[i]);
    }
    return cols;
  };

    BpTspSolver.prototype.getDuplicates = function() {
    var dups = [];
    for (var i = 0; i < duplicates.length; i++) {
      if (wpActive[i])
          dups.push(duplicates[i]);
    }
    return dups;
  };

  BpTspSolver.prototype.getNotes = function() {
    var nts = [];
    for (var i = 0; i < notes.length; i++) {
      if (wpActive[i])
          nts.push(notes[i]);
    }
    return nts;
  };

  BpTspSolver.prototype.getCasekeys = function() {
    var keys = [];
    for (var i = 0; i < casekeys.length; i++) {
      if (wpActive[i])
          keys.push(casekeys[i]);
    }
    return keys;
  };


  BpTspSolver.prototype.getSelected = function() {
        var sel = [];
    for (var i = 0; i < selected_markers.length; i++) {
      if (wpActive[i])
          sel.push(selected_markers[i]);
    }
    return sel;
  }

       // Nerd additions
  BpTspSolver.prototype.getMap = function() {
       return gebMap;
  }

  BpTspSolver.prototype.selectMarker = function(latlng) {
       for (var i = 0; i < waypoints.length; ++i) {
      if (wpActive[i] && waypoints[i].equals(latlng)) {
               selected_markers[i] = 1;
      }
    }
  }

  BpTspSolver.prototype.deSelectMarker = function(latlng) {
       for (var i = 0; i < waypoints.length; ++i) {
      if (wpActive[i] && waypoints[i].equals(latlng)) {
               selected_markers[i] = 0;
      }
    }
  }

  BpTspSolver.prototype.isMarkerSelected = function(latlng) {
       for (var i = 0; i < waypoints.length; ++i) {
      if (wpActive[i] && waypoints[i].equals(latlng)) {
               return selected_markers[i];
      }
    }
  }

  BpTspSolver.prototype.deSelectAllMarkers = function() {
       for( i = 0; i < selected_markers.length; i++ ) {
            if(wpActive[i]) {
                 selected_markers[i] = 0;
            }
       }
  }

  BpTspSolver.prototype.invertSelectedMarkers = function() {
       var allSelected = true;
       for( i = 1; i < selected_markers.length; i++ ) {
            if(wpActive[i]) {
                 if( ! selected_markers[i]) {
                      allSelected = false;
                      break;
                 }
            }
       }
       if(allSelected) {
            return; // do nothing if everything is selected
       }

       for( i = 1; i < selected_markers.length; i++ ) {
            if(wpActive[i]) {
                 if(selected_markers[i]) {
                      selected_markers[i] = 0;
                 } else {
                      selected_markers[i] = 1;
                 }
            }
       }
  }

  BpTspSolver.prototype.getSumSelected = function() {
       var count = 0;
       for( i = 1; i < selected_markers.length; i++ ) {
            if(wpActive[i]) {
                 if(selected_markers[i]) {
                      count ++;
                 }
            }
       }
       return count;
  }

  // end Nerd additions

  BpTspSolver.prototype.removeWaypoint = function(latLng) {
var returnValue = false;
for (var i = 0; i < waypoints.length; ++i) {
	if (wpActive[i] && waypoints[i].equals(latLng)) {
          wpActive[i] = false;
          return returnValue =  true;
      }
    }
    return returnValue;
  };


  BpTspSolver.prototype.removeAddress = function(addr) {
    for (var i = 0; i < addresses.length; ++i) {
      if (wpActive[i] && addresses[i] == addr) {
     wpActive[i] = false;
     return true;
      }
    }
    return false;
  };

  BpTspSolver.prototype.setAsStop = function(latLng, callback) {
       stop_bound = true;
    var j = -1;
    for (var i = waypoints.length - 1; i >= 0; --i) {
      if (j == -1 && wpActive[i]) {
     j = i;
      }
      if (wpActive[i] && waypoints[i].equals(latLng)) {
     for (var k = i; k < j; ++k) {
       swapWaypoints(k, k + 1);
     }
     break;
      }
    }
  }


  BpTspSolver.prototype.setAsStart = function(latLng) {
    var j = -1;
    for (var i = 0; i < waypoints.length; ++i) {
      if (j == -1 && wpActive[i]) {
     j = i;
      }
      if (wpActive[i] && waypoints[i].equals(latLng)) {
     for (var k = i; k > j; --k) {
       swapWaypoints(k, k - 1);
     }
     break;
      }
    }
  }

  BpTspSolver.prototype.getGDirections = function() {
    return gebDirectionsResult;
  };

  BpTspSolver.prototype.getGDirectionsService = function() {
    return gebDirectionsService;
  };

  // Returns the order that the input locations was visited in.
  //   getOrder()[0] is always the starting location.
  //   getOrder()[1] gives the first location visited, getOrder()[2]
  //   gives the second location visited and so on.
  BpTspSolver.prototype.getOrder = function() {
    return bestPath;
  }

  // Methods affecting the way driving directions are computed
  BpTspSolver.prototype.getAvoidHighways = function() {
    return avoidHighways;
  }

  BpTspSolver.prototype.setAvoidHighways = function(avoid) {
    avoidHighways = avoid;
  }

  BpTspSolver.prototype.getTravelMode = function() {
    return travelMode;
  }

  BpTspSolver.prototype.setTravelMode = function(travelM) {
    travelMode = travelM;
  }

  BpTspSolver.prototype.getDurations = function() {
    return dur;
  }

  // Helper functions
  BpTspSolver.prototype.getTotalDuration = function() {
    return gebDirections.getDuration().seconds;
  }

  // we assume that we have enough waypoints
  BpTspSolver.prototype.isReady = function() {
    return addressRequests == 0;
  };

   BpTspSolver.prototype.getAddressRequests = function() {
    return addressRequests;
  };

  BpTspSolver.prototype.solveRoundTrip = function(callback) {
    if (doNotContinue) {
      alert('Cannot continue after fatal errors.');
      return;
    }

    if (!this.isReady()) {
      setTimeout(function(){ tsp.solveRoundTrip(callback) }, 20);
      return;
    }
    if (typeof callback == 'function')
      onSolveCallback = callback;

    directions(0);
  };

  BpTspSolver.prototype.solveAtoZ = function(callback) {
    if (doNotContinue) {
      alert('Cannot continue after fatal errors.');
      return;
    }

    if (!this.isReady()) {
      setTimeout(function(){ tsp.solveAtoZ(callback) }, 20);
      return;
    }

    if (typeof callback == 'function')
      onSolveCallback = callback;

    directions(1);
  };

  BpTspSolver.prototype.setOnProgressCallback = function(callback) {
    onProgressCallback = callback;
  }

  BpTspSolver.prototype.getNumDirectionsComputed = function () {
    return numDirectionsComputed;
  }

  BpTspSolver.prototype.getNumDirectionsNeeded = function () {
    return numDirectionsNeeded;
  }

  BpTspSolver.prototype.reverseSolution = function () {
    reverseSolution();
  }

  BpTspSolver.prototype.reorderSolution = function(newOrder, callback) {
    if (typeof callback == 'function')
      onSolveCallback = callback;

    reorderSolution(newOrder);
  }



  BpTspSolver.prototype.removeStop = function(number, callback) {
    if (typeof callback == 'function')
      onSolveCallback = callback;

    removeStop(number);
  }

  window.BpTspSolver = BpTspSolver;

 });

/**********************************/
     var REORDER_ON_CALC = false; // use this as a flag to determine if there is a loaded order to restore
     var newOrder = new Array();
     var available_accordion_indexes = [0,1,2]; // Available accordion tabs

     // Since adding the instant select-all box, we need to enqueue the addition of stops
     // to prevent some bad behavior
     var checkbox_waypoint_queue;

     var start_latlng;

     var REFRESH_MAP = false; // do or do not change the bounds of the map, depending on whether tab is open, otherwise, FUBAR

  jQuery.noConflict();


  function init() {

            // We need to load some maps extensions AFTER the google loader is done getting the maps API
            // And then set our starting location
            jQuery.when(
              jQuery.getScript( URLFOR.Resource.jquery['js/StyledMarker.js'] ),
              jQuery.getScript( URLFOR.Resource.jquery['js/keydragzoom.js'] ),
              jQuery.getScript("https://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.9/src/markerwithlabel_packed.js"),
              //jQuery.getScript("/resource/1395350079000/FastMarkerOverlay"),
              //jQuery.getScript( URLFOR.Resource.FastMarkerOverlay),
              jQuery.Deferred(function( deferred ){
                  jQuery( deferred.resolve );
              })
          ).done(function(){

               // Here we restore the JS view state of a saved route
                    // None of this works yet
               /*
               if(typeof route_JSON.input == "object") {

                    for(var i=0;i<route_JSON.length; i++) {
                         //add in as a checkbox click if it has a case number
                         if(route_JSON[i].WON != null) {

                              addAddressAndLabel(route_JSON[i].address,  route_JSON[i].city, route_JSON[i].state, route_JSON[i].zip, route_JSON[i].address + ' ' + route_JSON[i].city + ' ' + route_JSON[i].state + ' ' + route_JSON[i].zip , route_JSON[i].color, route_JSON[i].WON);

                              // check the work order record if it's in the list
                              jQuery('.'+route_JSON.WON).attr('checked', true);

                         // otherwise add it as a manual addition
                         } else {
                              addAddressAndLabel(route_JSON[i].address,  route_JSON[i].city, route_JSON[i].state, route_JSON[i].zip, route_JSON[i].address + ' ' + route_JSON[i].city + ' ' + route_JSON[i].state + ' ' + route_JSON[i].zip , route_JSON[i].color,null, route_JSON[i].Notes );
                         }

                         // create new order
                         newOrder[route_JSON[i].originalIndex] = route_JSON[i].stopNumber;
                    }
                    REORDER_ON_CALC = true;
                    directions(1, false, jQuery('[id$=avoidHighways]').is(':checked'));
               } else {
               */
                   loadAtStart(37.4419, -122.1419, 8);
                 if(start_address != '') {
                         addAddressAndLabel(start_address, start_city, start_state, start_zip, 'Starting Point');
                    } else {
                         jQuery('#dialog_start_address').dialog('open');
                         //addAddressAndLabel('1600 Pennsylvania Avenue Northwest', 'Washington', 'DC', '20500', 'Starting Point');
                    }
               //}
          });

            // Here we load the key drag zoom plugin after the map system has been initialized
            // jQuery.getScript(URLFOR.Resource.jquery['js/keydragzoom.js'], function() {
        //});


            // this is a fix for the map centering problem when combining
            //   google map with the UI tab
            jQuery('#maintab').bind('tabsshow', function(event, ui) {
              if (ui.panel.id == "route") {
                   if(start_address=='') {
                         jQuery('#dialog_start_address').dialog('open');
                    }

                   REFRESH_MAP = true; // enable updating of the map when markers are changed

                   if(checkbox_waypoint_queue == 0 && markers.length == 1) {
                        cancel_directions();
                        alert('Warning: There are no stops selected!');
                   }

                   MAPIT_ANIMATION_ON = false;

                   // process address queue
                   processAddressQueue();

                   // When the map tab is shown, set the viewport to center on existing markers
                   if(map==undefined){
                    var map;
                   }
                   map = tsp.getMap();

                   var latlngbounds = new google.maps.LatLngBounds();
                    jQuery.each(markers, function(i, m){
                       if(m==undefined) return;
                       latlngbounds.extend(m.getPosition());
                    });

                    google.maps.event.trigger(map, 'resize');

                    map.setCenter(latlngbounds.getCenter());

                    map.fitBounds(latlngbounds);

              } else {
                   REFRESH_MAP = false; // disable updating the map if it's not shown
              }


          });

  }

// Hack var to track the meta key pressed during the maps click event
var shiftdown = false;


function toggle(divId) {
  var divObj = document.getElementById(divId);
  if (divObj.innerHTML == "") {
    divObj.innerHTML = document.getElementById(divId + "_hidden").innerHTML;
    document.getElementById(divId + "_hidden").innerHTML = "";
  } else {
    document.getElementById(divId + "_hidden").innerHTML = divObj.innerHTML;
    divObj.innerHTML = "";
  }
}

jQuery(document).ready(function() {

  if (jQuery.browser.msie) {
    jQuery(function() {

      jQuery('input:checkbox').bind('click',function() {
        this.blur();
        this.focus();
      });
    });
  }


     jQuery('.chk').attr('checked', false); // reset checkboxes, some browsers will reload state from cache

     jQuery('#maintab').tabs();

     google.load("maps", "3.12", {callback: init, other_params:"key=AIzaSyC4efn2nRgZohcWKQ8JBuf75yWff52rhmw&sensor=false&libraries=geometry"});


  jQuery( "#accordion" ).accordion({
    collapsible: true,
     autoHeight: false,
     clearStyle: true,
     header: 'h3',
     changestart: function(event, ui) {
        var newIndex = jQuery(ui.newHeader).index('h3');
        if (jQuery.inArray(newIndex, available_accordion_indexes) == -1) {
            var oldIndex = jQuery(ui.oldHeader).index('h3');
            jQuery(this).accordion( "activate" , oldIndex );
            event.stopPropagation();
            return false;
            //alert('That panel is not yet available');
        }
    }
  });


  jQuery("input:button").button();

  jQuery("#dialogProgress" ).dialog({
    height: 140,
     modal: true,
     autoOpen: false
  });
  jQuery("#dialog_geo" ).dialog({
    height: 140,
     modal: true,
     autoOpen: false
  });
  jQuery("#progressBar").progressbar({ value: 0 });
   jQuery("#progressGeoBar").progressbar({ value: 0 });
  jQuery("#dialogTomTom" ).dialog({
    height: 480,
     width: 640,
     modal: true,
     autoOpen: false
  });

  jQuery("#dialogGarmin" ).dialog({
    height: 480,
     width: 640,
     modal: true,
     autoOpen: false
  });

  jQuery( "#dialog_start_address" ).dialog({
      autoOpen: false,
      height: 270,
      width: 350,
      modal: true,
      buttons: {
          "Save Address": function() {
                start_address = jQuery("#edit_start_address").val();
                start_city = jQuery("#edit_start_city").val();
                start_state = jQuery("#edit_start_state").val();
                start_zip = jQuery("#edit_start_zip").val();
                 addAddressAndLabel(jQuery("#edit_start_address").val(),jQuery("#edit_start_city").val(), jQuery("#edit_start_state").val(),jQuery("#edit_start_zip").val(), 'Starting Point');

              jQuery( this ).dialog( "close" );
              drawMarkers(false);
          }
      }
  });

    jQuery( "#dialog_edit_address" ).dialog({
      autoOpen: false,
      height: 200,
      width: 350,
      modal: true,
      buttons: {
          "Update Address": function() {
                 var key = jQuery("#edit_key").val();

                 if(typeof markers[key].infoWindow != 'undefined') {
                   markers[key].infoWindow.close();
              }

                    updateAddress(markers[key].getPosition(), jQuery("#edit_address").val(),jQuery("#edit_city").val(), jQuery("#edit_state").val(),jQuery("#edit_zip").val() );
                    cancel_directions();
                    must_recalculate();

              jQuery( this ).dialog( "close" );
              drawMarkers(false);
          },
          Cancel: function() {
              jQuery( this ).dialog( "close" );
          }
      },
      close: function() {
          jQuery("#edit_key").val('');
          jQuery(".custEdit").val('');
      }
  });

  jQuery( "#dialog_edit_status" ).dialog({
      autoOpen: false,
      height: 480,
      width: 640,
      modal: true,
      buttons: {
          "Save Status": function() {
                var errors = false;
                if( jQuery("#status_upload_date").val() == '') {
                     jQuery("#status_upload_date_tr td").addClass('ui-state-error');
                     errors = true;
                }else{
                     jQuery("#status_upload_date_tr td").removeClass('ui-state-error');
                }
                if(jQuery("[id$=status_upload_status]").val() == '') {
                     jQuery("#status_upload_status_tr td").addClass('ui-state-error');
                     errors = true;
                } else{
                     jQuery("#status_upload_status_tr td").removeClass('ui-state-error');
                }
                if(jQuery("[id$=status_upload_delay]").val() == '') {
                     jQuery("#status_upload_delay_tr td").addClass('ui-state-error');
                     errors = true;
                }else{
                     jQuery("#status_upload_delay_tr td").removeClass('ui-state-error');
                }
                if(jQuery("[id$=status_upload_explanation]").val() == '') {
                     jQuery("#status_upload_explanation_tr td").addClass('ui-state-error');
                     errors = true;
                }else{
                     jQuery("#status_upload_explanation_tr td").removeClass('ui-state-error');
                }
                jQuery('#status_msg').hide();
                if(errors) {
                     jQuery('#status_msg').html('<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>You must complete the required fields').show();

                     return;
                }
                showProgress();

              save_status(
                                  jQuery("#status_upload_date").val(),
                                  jQuery("#status_upload_explanation").val(),
                                  jQuery("[id$=status_upload_delay]").val(),
                                  jQuery("[id$=status_upload_status]").val()
                             );


          },
          Cancel: function() {
              jQuery( this ).dialog( "close" );
          }
      },
      close: function() {
          jQuery(".statusEdit").val('');
      },
      open: function() {

            jQuery('#status_upload_date').val(getFormattedCurDate());

      }
  });


   jQuery( "#dialog_save" ).dialog({
      autoOpen: false,
      height: 250,
      width: 350,
      modal: true,
      buttons: {
          "Save": function() {
                    showProgress();
                    jQuery( this ).dialog( "close" );
                      buildSaveJSON();
          },
          Cancel: function() {
              jQuery( this ).dialog( "close" );
          }
      },
      close: function() {

      }

  });


  // Set some route values if they're empty
  if(jQuery('#routeLabel').val()=='') {

       jQuery('#routeLabel').val('New Route');

       jQuery('#routeDate').val(getFormattedCurDate());
  }

  // Enable DatePicker
  tday = new Date();
  jQuery('#routeDate').datepicker({ dateFormat: 'mm/dd/yy', minDate:tday, numberOfMonths:1 });
  jQuery('#routeDate').attr('readonly', true);
  jQuery('#status_upload_date').datepicker({ dateFormat: 'mm/dd/yy', minDate:tday, numberOfMonths:1 });
  jQuery('#status_upload_date').attr('readonly', true);

  // hide the directions tab initially
  jQuery('#maintab').tabs('disable', 2);

  // Add the class ui-state-disabled to the headers that you want disabled
     jQuery( ".route_edit_link" ).addClass("ui-state-disabled");

     jQuery('#button_calc').attr('disabled', 'disabled').addClass('ui-state-disabled');

        jQuery('#selectmenu').menu({role:null, position: { my: "left top", at: "left-5 bottom" }});

    // initial state, animate select button
    SELECT_ANIMATION_ON = true;
    animate_select_btn('off');


       jQuery('#mapit').attr('disabled', 'disabled').addClass('ui-state-disabled');
       jQuery('#status_btn').attr('disabled', 'disabled').addClass('ui-state-disabled');


       jQuery('.trip_opts').change(function() {
            cancel_directions();
            must_recalculate();
            drawMarkers(false);
       });


       jQuery(window).keydown(function(evtobj) {
        if(evtobj.ctrlKey || evtobj.metaKey) {
          ctrldown = true;
        }
        jQuery(window).keyup(function(evtobj) {
          ctrldown = false;
        });
      });

});


function open_edit_status_dlg() {
      var anyChecked = false;

          // Verify that something is selected
          jQuery('.chk').each(function() {
               if( jQuery(this).is(':checked') ) {
                    anyChecked = true;
                    return;
               }
          });

        if(anyChecked) {
             jQuery('#dialog_edit_status').dialog('open');
        } else {
             alert('At least one Work Order must be selected');
        }

}

function stopDialog(count) {
     //alert(count);
     if(tsp.isReady()) {
          //alert('ready');
          jQuery('#dialog_geo').dialog('close');
     } else {
          //jQuery('#testDiv').append('test<br/>');
          //alert( (count - tsp.getAddressRequests()) + ' / ' + count);
          jQuery('#progressGeoBar').progressbar('value', 100 * (count - tsp.getAddressRequests()) / count);
          setTimeout(function() { stopDialog(count); }, 100);
     }
}


function reCalculate() {

     directions(1, false, jQuery('[id$=avoidHighways]').is(':checked') );

}

function must_recalculate() {
     if( ! CALC_ANIMATION_ON && markers.length > 1) {
          CALC_ANIMATION_ON = true;
          animate_calc_btn('off');
          jQuery('#button_calc').attr('disabled', false).removeClass('ui-state-disabled');
     } else if (markers.length==1) {
          CALC_ANIMATION_ON = false;
          jQuery('#button_calc').attr('disabled', 'disabled').addClass('ui-state-disabled');
     }

     SAVE_ANIMATION_ON = false;

     jQuery('.route_edit_link').addClass('ui-state-disabled');
     available_accordion_indexes = [0,1,2];

     jQuery('#button_save').attr('disabled', 'disabled').addClass('ui-state-disabled');
}




function cancel_directions() {

     // clear any previous calculation
     tsp.clearAll();

}

var SAVE_ANIMATION_ON = false;
function animate_save_btn(state) {
     if(! SAVE_ANIMATION_ON) return;

     if(state=='off') {
          jQuery('#button_save').addClass('ui-state-hover');
          state = 'on';
     } else {
          jQuery('#button_save').removeClass('ui-state-hover');
          state = 'off';
     }

     setTimeout(function() { animate_save_btn(state) } , 500);
}


var CALC_ANIMATION_ON = false;
function animate_calc_btn(state) {
     if(! CALC_ANIMATION_ON) return;

     if(state=='off') {
          jQuery('#button_calc').addClass('ui-state-hover');
          state = 'on';
     } else {
          jQuery('#button_calc').removeClass('ui-state-hover');
          state = 'off';
     }

     setTimeout(function() { animate_calc_btn(state) } , 500);

}

var SELECT_ANIMATION_ON = false;
function animate_select_btn(state) {
     if(! SELECT_ANIMATION_ON) return;

     if(state=='off') {
          jQuery('#selectmenu').addClass('ui-state-hover');
          state = 'on';
     } else {
          jQuery('#selectmenu').removeClass('ui-state-hover');
          state = 'off';
     }

     setTimeout(function() { animate_select_btn(state) } , 500);

}

var MAPIT_ANIMATION_ON = false;
function animate_mapit_btn(state) {
     if(! MAPIT_ANIMATION_ON) return;

     if(state=='off') {
          jQuery('#mapit').addClass('ui-state-hover');
          state = 'on';
     } else {
          jQuery('#mapit').removeClass('ui-state-hover');
          state = 'off';
     }

     setTimeout(function() { animate_mapit_btn(state) } , 500);
}



function edit_location(index) {
       var addresses = tsp.getAddressPieces();
       jQuery("#edit_key").val(index);
       jQuery("#edit_address").val(addresses[index][0]);
       jQuery("#edit_city").val(addresses[index][1]);
       jQuery("#edit_state").val(addresses[index][2]);
       jQuery("#edit_zip").val(addresses[index][3]);
      jQuery( "#dialog_edit_address" ).dialog( "open" );
}

// This is the remove method to call from the interface
function remove_address(index, stop_refresh) {
     if(index==0) {
          alert('You may edit the starting location, but not delete.');
          return;
     }
     keys = tsp.getCasekeys();
     if(typeof keys[index]== 'undefined') {
          remove_manual_location(index);
     } else {

          duplicates = tsp.getDuplicates();

          jQuery('.'+keys[index]).attr('checked', false).parent().parent().removeClass('highlighted');

          if(typeof duplicates[index] != 'undefined') {
               jQuery.each(duplicates[index], function(k, WO) {
                    jQuery('.'+WO).attr('checked', false).parent().parent().removeClass('highlighted');
               });
          }

          tmporder = tsp.getOrder(); // determine if we are in calc mode
          if(typeof tmporder != 'undefined' && !stop_refresh) {
                    cancel_directions();
               }
          removeMarker(markers[index], stop_refresh);
     }

     //Reset the Most distant marker
     if(!stop_refresh) {
           if(stop_point_locked && markers[index].getPosition() == stopMarker) {
              setStopLock(false);
         } else if(!stop_point_locked) {
               tsp.correctStops();
          }
          must_recalculate();
     }
}

function remove_manual_location(index) {
     tmporder = tsp.getOrder();
     tmpaddr = tsp.getAddresses();
     if(typeof tmporder!= 'undefined' && tmpaddr.length==2) {
               startOver();
               return;
     }
     if(typeof tmporder != 'undefined') {
          cancel_directions();
     }
     removeMarker(markers[index]);

     must_recalculate();
}

function failedGeocodeStartAddress() {
     //console.log('Failed geocode start address');
     start_address = '';
     start_city = '';
     dstart_state = '';
     start_zip = '';
}

function updateStartAddress() {
     cancel_directions();
     start_address = jQuery('#start_address').val();
     start_city = jQuery('#start_address_city').val();
     dstart_state = jQuery('#start_address_state').val();
     start_zip = jQuery('#start_address_zip').val();
     if(markers.length==0) {
          addAddressAndLabel(jQuery('#start_address').val(), jQuery('#start_address_city').val(), jQuery('#start_address_state').val(), jQuery('#start_address_zip').val(), 'Starting Point');
          map = tsp.getMap();
          var latlngbounds = new google.maps.LatLngBounds();
          google.maps.event.trigger(map, 'resize');
          map.setCenter(latlngbounds.getCenter());
          map.fitBounds(latlngbounds);
     } else {
          updateAddress(markers[0].getPosition(), jQuery('#start_address').val(), jQuery('#start_address_city').val(), jQuery('#start_address_state').val(), jQuery('#start_address_zip').val());
          must_recalculate();
          jQuery('#ui-accordion-accordion-panel-0').removeClass('ui-state-error');
     }
     jQuery('.st_add').val('');
}

// This is called when the user switches to the map view, the waypoints are geolocated and pinpointed
function processAddressQueue() {
     if(typeof checkbox_waypoint_queue != 'undefined' && checkbox_waypoint_queue.length > 0) {

          WO_List = getWO_List();
          var count = 0;
          jQuery.each(checkbox_waypoint_queue, function(k, work_order_num) {
               if(work_order_num != null) {
                    addAddressAndLabel(WO_List[work_order_num].Address,  WO_List[work_order_num].City, WO_List[work_order_num].State, WO_List[work_order_num].Zip, WO_List[work_order_num].Address + ' ' + WO_List[work_order_num].City + ' ' + WO_List[work_order_num].State + ' ' + WO_List[work_order_num].Zip , WO_List[work_order_num].Color, work_order_num);
                    count ++;
               }
          });

          setTimeout(function() { stopDialog(count); }, 100);
          jQuery('#dialog_geo').dialog('open');

          checkbox_waypoint_queue = new Array();
     }
}


function bulkDelete(indexList) {
     keys  = tsp.getCasekeys();

     duplicates = tsp.getDuplicates();
     jQuery.each(indexList, function(k, v) {
          if(typeof markers[v]!= 'undefined') {
			tsp.removeWaypoint(markers[v].getPosition());
		  }
     });
     jQuery.each(indexList, function(k, v) {
          if(typeof duplicates[v]!= 'undefined') {
               for(var y=0;y<duplicates[v].length;y++) {
                    removeDuplicate(duplicates[v][y], v     );
               }
          }
    });
	jQuery.each(indexList, function(k, v) {
          if(typeof keys[v] != 'undefined') {
               jQuery('.'+keys[v]).attr('checked', false).parent().parent().removeClass('highlighted');
          }
     });

     drawMarkers();
     cancel_directions();
     must_recalculate();
     syncInterface(true);
}


// Functionality has changed, no longer queueing items, immediately add them
function enQueue(WO, checked, verifySelectAll) {
     if(start_address=='') {
          jQuery('#dialog_start_address').dialog('open');
          jQuery('.'+WO).attr('checked', false);
          return;
     }
     //console.log('Start Queue :: ' + checked +' :: ' + WO);
     var something_checked = false;
     if(typeof checkbox_waypoint_queue == 'undefined') {
          checkbox_waypoint_queue = new Array();
     }
     keys = tsp.getCasekeys();
     duplicates = tsp.getDuplicates();
     WO_List = getWO_List();
     if(checked) {
          if(jQuery.inArray(WO, checkbox_waypoint_queue) == -1 && jQuery.inArray(WO, keys) == -1) {
               // Check if we're missing geo data, if so, try to geocode
               if(WO_List[WO].LAT=='' || WO_List[WO].LNG=='') {
                    checkbox_waypoint_queue.push(WO);
               } else {
                    addAddressJson(WO);
               }
               jQuery('.'+WO).parent().parent().addClass('highlighted');

          }

     } else {

          // First cover removing any mapped markers associated with this WO
          if(typeof keys != 'undefined' && keys.length > 0) {
               for(var x=0;x<keys.length;x++) {
                    if(WO==keys[x]) {
                         // check for mapped WO as a duplicate
                         if(typeof duplicates[x]!= 'undefined') {
                              for(var y=0;y<duplicates[x].length;y++) {
                                   removeDuplicate(duplicates[x][y], x);
                              }
                         }
                         //if(typeof markers[x].infoWindow != 'undefined') {
                         //     markers[x].infoWindow.close();
                         //}

                         //console.log('Removing index: ' + x);

                         remove_address(x);
                         break;
                    }
               }
          }

          // Clear it from the mapping queue, if it exists
          var new_queue = new Array();
          // remove WO entry from queue by generating new list
          for(var x=0;x<checkbox_waypoint_queue.length; x++) {
               if(checkbox_waypoint_queue[x] != WO) {
                    new_queue.push(checkbox_waypoint_queue[x]);
               }
          }
          checkbox_waypoint_queue = new_queue;

          jQuery('.'+WO).parent().parent().removeClass('highlighted');

     }

     syncInterface(verifySelectAll);

}

function syncInterface(verifySelectAll) {

          something_checked = false;

          jQuery('.chk').each(function(){
               if(this.checked) {
                    something_checked = true;
                    return;//stops this inner each function, not syncInterface
               }
          });

          // Enable "mapit" animation
          if(checkbox_waypoint_queue.length > 0 || something_checked) {
               if(!MAPIT_ANIMATION_ON) {
                    jQuery('#mapit').attr('disabled', false).removeClass('ui-state-disabled');
                    jQuery('#maintab').tabs('enable');
                    MAPIT_ANIMATION_ON = true;
                    animate_mapit_btn('off');
                    SELECT_ANIMATION_ON = false;
                    if (jQuery('[id$=query_option]').val() == '1') {
                         jQuery('#status_btn').attr('disabled', false).removeClass('ui-state-disabled');
                    }
               }
          } else {
               jQuery('#mapit').attr('disabled', 'disabled').addClass('ui-state-disabled');
               jQuery('#status_btn').attr('disabled', 'disabled').addClass('ui-state-disabled');
               MAPIT_ANIMATION_ON = false;
               SELECT_ANIMATION_ON = true;
               animate_select_btn('off');
          }

          if(verifySelectAll) {
               syncSelectAll();
          }

          anyBlacks();
}


function syncSelectAll() {

     var all_checked = true;

     jQuery('.chk').each(function(){
          if(!this.checked) {
               all_checked = false;
               ////console.log('nope');
               return;
          }
     });

     // Keep the check all button in sync
     if(all_checked && !jQuery('[id$=selectAll]').is(':checked')) {
          jQuery('[id$=selectAll]').attr('checked', true);
     }

     if(!all_checked && jQuery('[id$=selectAll]').is(':checked')) {
          jQuery('[id$=selectAll]').attr('checked', false);
     }
}

function addAllWOs(ischecked) {
     if(start_address=='') {
          jQuery('#dialog_start_address').dialog('open');
          return;
     }
     checkbox_waypoint_queue = new Array();
     WO_List = getWO_List();
     if(ischecked) {
          // Do a pre-count to learn our queuesize
          var qsize = 0
          var totalsize = 0; // for testing
          var keys = tsp.getCasekeys;
          jQuery.each(WO_List, function(k, v) {
               totalsize++;
               if(!jQuery('.'+v.WON).is(':checked')) {
                    // don't add to the queue if the address is already added or if it needs to be geocoded
                    if(v.LAT!='' && v.LNG!='') {
                         qsize++;
                    }
               }
          });
          //console.log('total size: ' + totalsize + ' qsize: ' + qsize);
          setQueueSize(qsize);
          jQuery.each(WO_List, function(k, v) {
               if(!jQuery('.'+ v.WON).is(':checked')) {
                    enQueue(v.WON, true, false);
                    jQuery('.'+v.WON).attr('checked', true);
               }
          });
     } else {
          jQuery.each(WO_List, function(key, wo) {
               if(jQuery('.'+ wo.WON).is(':checked')) {
                    jQuery('.'+wo.WON).attr('checked', false);
                    jQuery('.'+wo.WON).parent().parent().removeClass('highlighted');
               }
          });
          startOver();
          jQuery('#mapit').attr('disabled', 'disabled').addClass('ui-state-disabled');
          jQuery('#status_btn').attr('disabled', 'disabled').addClass('ui-state-disabled');
          MAPIT_ANIMATION_ON = false;
          SELECT_ANIMATION_ON = true;
          animate_select_btn('off');
          anyBlacks();
     }
}

// Restore the checked state of work orders after a list refresh
function reCheckWOs() {
     var something_checked = false;
     for(var x=0;x<checkbox_waypoint_queue.length; x++) {
          jQuery('.'+checkbox_waypoint_queue[x]).attr('checked', true).parent().parent().addClass('highlighted');
          something_checked = true;
     }

     keys = tsp.getCasekeys();

     for(var y=1;y<keys.length;y++) {
          jQuery('.'+keys[y]).attr('checked', true).parent().parent().addClass('highlighted');
          something_checked = true;
     }


     // Enable "mapit" animation
     if(something_checked) {
          if(!MAPIT_ANIMATION_ON) {
               jQuery('#mapit').attr('disabled', false).removeClass('ui-state-disabled');
               jQuery('#maintab').tabs('enable');
               MAPIT_ANIMATION_ON = true;
               animate_mapit_btn('off');
               SELECT_ANIMATION_ON = false;
               if (jQuery('[id$=query_option]').val() == '1') {
                    jQuery('#status_btn').attr('disabled', false).removeClass('ui-state-disabled');
               }
          }
     } else {
          jQuery('#mapit').attr('disabled', 'disabled').addClass('ui-state-disabled');
          jQuery('#status_btn').attr('disabled', 'disabled').addClass('ui-state-disabled');
          MAPIT_ANIMATION_ON = false;
          SELECT_ANIMATION_ON = true;
          animate_select_btn('off');
     }

     syncSelectAll();

}


function addWOs(status) {
     if(start_address=='') {
          jQuery('#dialog_start_address').dialog('open');
          return;
     }
     // keep the select-all checkbox in sync with the selection status
     if(status == 'All') {
          jQuery('[id$=selectAll]').attr('checked', true);
          addAllWOs(true);
          return;
     }

     // Clear the queue and reset checkboxes before continuing to re-do selection
     jQuery('[id$=selectAll]').attr('checked', false);
     addAllWOs(false); // deselect all

     if(status == 'None') {
          cancel_directions();
          return;
     }

     WO_List = getWO_List();
     keys = tsp.getCasekeys();
     // Do a pre-count dry run
     var qsize = 0;
     var totalsize = 0; // for testing
     jQuery.each(WO_List, function(key, wo) {

          if(status == 'Priority' && wo.Priority == 'Yes') {
               totalsize++;
               if( ! jQuery('.'+ wo.WON).is(':checked')) {
                    // don't add to the queue if the address is already added or if it needs to be geocoded
                    if( wo.LNG != '' && wo.LAT != '') {
                         qsize++;
                    }
               }

          } else if (status == 'Serviceable' && wo.Serviceable=='Yes') {
               totalsize++;
               if( ! jQuery('.'+ wo.WON).is(':checked')) {
                    // don't add to the queue if the address is already added or if it needs to be geocoded
                    if( wo.LNG!='' && wo.LAT!='') {
                         qsize++;
                    }
               }
          }

     });
     //console.log('total size: ' + totalsize + ' qsize: ' + qsize);
     setQueueSize(qsize);

     jQuery.each(WO_List, function(key, wo) {

          if(status == 'Priority' && wo.Priority == 'Yes') {
               if( ! jQuery('.'+ wo.WON).is(':checked')) {
                    jQuery('.'+ wo.WON).attr('checked', true);
                    enQueue(wo.WON, true, true);
                    //chk_add_address(wo.WON, true);
               }

          } else if (status == 'Serviceable' && wo.Serviceable=='Yes') {
               if( ! jQuery('.'+ wo.WON).is(':checked')) {
                    jQuery('.'+ wo.WON).attr('checked', true);
                    enQueue(wo.WON, true, true);
                    //chk_add_address(wo.WON, true);
               }
          }

     });

     syncSelectAll();

}


function chk_add_address(work_order_num, ischecked) {

     WO_List = getWO_List();

     // defer until system is loaded
     if(typeof tsp == 'undefined') {
          setTimeout(function(){ chk_add_address(work_order_num, ischecked) }, 100);
          return;
     }


     // Retrieve route order from the TSP system, we need to check to see if it exists
     tmporder = tsp.getOrder();
     keys = tsp.getCasekeys(); // get the list of case keys
     // Remove marker
     if(!ischecked) {
               jQuery('.'+work_order_num).parent().parent().removeClass('highlighted');

               // Reset if we drop down to 1 pin left, to avoid errors
               if(typeof tmporder!= 'undefined' && markers.length==2) {
                    CALC_ANIMATION_ON = false;
                    startOver();
                    return;
               }
               for(var i=0;i<keys.length;i++) {
                    if(keys[i] == work_order_num) {
                         if(typeof tmporder != 'undefined' && tmporder.length!=0) {
                                   // if an order has been calculated, reset it
                                   cancel_directions();
                              }
                         removeMarker(markers[i]);
                    }
               }
               must_recalculate();
     } else {

          tmpaddr = tsp.getAddresses();

          // Check to make sure we aren't about to add the same work order twice
          for(var i=0;i<keys.length;i++) {
               if(keys[i] == work_order_num) {
                    return;
               }
          }

          jQuery('.'+work_order_num).parent().parent().addClass('highlighted');

          // Add marker
          //addAddressAndLabel(WO_List[work_order_num].Address,  WO_List[work_order_num].City, WO_List[work_order_num].State, WO_List[work_order_num].Zip, WO_List[work_order_num].Address + ' ' + WO_List[work_order_num].City + ' ' + WO_List[work_order_num].State + ' ' + WO_List[work_order_num].Zip , WO_List[work_order_num].Color, work_order_num);
          addAddressJson(work_order_num);
     }
}


function toggleInterface() {
     // reset the queue
     checkbox_waypoint_queue = new Array();
     MAPIT_ANIMATION_ON = false;
     SELECT_ANIMATION_ON = false;
     jQuery('[id$=selectAll]').attr('checked', false);
     if( jQuery('[id$=query_option]').val() != '1' ) {
          jQuery('.front_button').attr('disabled', 'disabled').addClass('ui-state-disabled');
          jQuery('.chk').attr('disabled', 'disabled');
          jQuery('[id$=selectmenu]').menu( "option", "disabled", true );
     } else {
          jQuery('.front_button').attr('disabled',false).removeClass('ui-state-disabled');
          jQuery('.chk').attr('disabled', false);
          jQuery('[id$=selectmenu]').menu( "option", "disabled", false );
          reCheckWOs();
     }
}



function buildSaveJSON() {

     var addresses = tsp.getAddressPieces();
     var notes = tsp.getNotes();
     var order = tsp.getOrder();
     var casekeys = tsp.getCasekeys();
     var duplicates = tsp.getDuplicates();

     var JSON_save_string = "[";

     var tmpOrder;
     var tmpNote;

     for(var i = 0; i < addresses.length ; i++ ) {

          tmpOrder = (typeof order== 'undefined' || typeof order[i]== 'undefined') ? i : order[i];

          if(typeof casekeys[tmpOrder] == 'undefined') {
               tmpNote = (typeof notes[i]== 'undefined') ? 'null' : notes[i].replace(/"/g, '\\"');
          } else {
               tmpNote = readWOList(casekeys[tmpOrder], 'NotesContent').replace(/"/g, '\\"').replace(/&/g,'&amp;');
          }
          tmpDirections = (typeof directions_arr[i]== 'undefined') ? '' : directions_arr[i].replace(/"/g, '\\"');

          if(i>0) {
               JSON_save_string += ",";
          }

          JSON_save_string += "{\"address\":\""+ addresses[tmpOrder][0] + "\","
                                + "\"city\":\"" + addresses[tmpOrder][1] + "\","
                                + "\"state\":\"" + addresses[tmpOrder][2] +"\","
                                + "\"zip\":\"" + addresses[tmpOrder][3] + "\","
                                + "\"latitude\":\"" + markers[tmpOrder].getPosition().lat() + "\","
                                + "\"longitude\":\"" + markers[tmpOrder].getPosition().lng() + "\","
                                + "\"stopNumber\":\"" + tmpOrder + "\","
                                + "\"originalIndex\":\"" + i +"\","
                                + "\"Notes\":\"" + tmpNote + "\","
                                + "\"directions\":\"" + tmpDirections + "\","
                                + "\"schedDate\":\"" + readWOList(casekeys[tmpOrder], 'Date') +"\","
                                + "\"Color\":\"" + readWOList(casekeys[tmpOrder], 'Color') +"\","
                                + "\"WorkOrderId\":\"" + readWOList(casekeys[tmpOrder], 'ID') +"\","
                                + "\"WON\":\"" + readWOList(casekeys[tmpOrder], 'WON') +"\","
                                + "\"WOD\":\"" + readWOList(casekeys[tmpOrder], 'WOD') +"\"}";

          // Check for duplication information and add dups in as route stops
          if(typeof duplicates[tmpOrder] != 'undefined') {
               jQuery.each(duplicates[tmpOrder], function(k, casekey) {

                    JSON_save_string += ",{\"address\":\""+ addresses[tmpOrder][0] + "\","
                                + "\"city\":\"" + addresses[tmpOrder][1] + "\","
                                + "\"state\":\"" + addresses[tmpOrder][2] +"\","
                                + "\"zip\":\"" + addresses[tmpOrder][3] + "\","
                                + "\"latitude\":\"" + markers[tmpOrder].getPosition().lat() + "\","
                                + "\"longitude\":\"" + markers[tmpOrder].getPosition().lng() + "\","
                                + "\"stopNumber\":\"" + tmpOrder + "\","
                                + "\"originalIndex\":\"" + i +"\","
                                + "\"Notes\":\"" + tmpNote + "\","
                                + "\"schedDate\":\"" + readWOList(casekey, 'Date') +"\","
                                + "\"Color\":\"" + readWOList(casekey, 'Color') +"\","
                                + "\"WorkOrderId\":\"" + readWOList(casekey, 'ID') +"\","
                                + "\"WON\":\"" + readWOList(casekey, 'WON') +"\","
                                + "\"WOD\":\"" + readWOList(casekey, 'WOD') +"\"}";
               });
          }
     }

     // For round trips, add the start as the last stop
     if(jQuery('[id$=calcRoundTrip]').is(':checked')) {
          tmpDirections =  (typeof directions_arr[i]== 'undefined') ? '' : directions_arr[i].replace(/"/g, '\\"');
          JSON_save_string += ",{\"address\":\""+ addresses[0][0] + "\","
                                + "\"city\":\"" + addresses[0][1] + "\","
                                + "\"state\":\"" + addresses[0][2] +"\","
                                + "\"zip\":\"" + addresses[0][3] + "\","
                                + "\"latitude\":\"" + markers[0].getPosition().lat() + "\","
                                + "\"longitude\":\"" + markers[0].getPosition().lng() + "\","
                                + "\"stopNumber\":\"" + i + "\","
                                + "\"originalIndex\":\"0\","
                                + "\"Notes\":\"\","
                                + "\"directions\":\""+tmpDirections+"\","
                                + "\"schedDate\":\"\","
                                + "\"Color\":\"\","
                                + "\"WorkOrderId\":\"\","
                                + "\"WON\":\"\","
                                + "\"WOD\":\"\"}";
     }

     JSON_save_string += "]";

     // take out any newline characters
     JSON_save_string = JSON_save_string.replace(/(\r\n|\n|\r)/gm,"")

     // set the directions string -- not doing this anymore
     //jQuery('[id$=routeDirections]').val(jQuery('#directions_print').html())

     //jQuery('#testDiv').html(JSON_save_string);
	 saveStops(JSON_save_string,jQuery('#routeLabel').val(),jQuery('#routeDate').val(), jQuery('[id$=calcRoundTrip]').is(':checked'), jQuery('[id$=avoidHighways]').is(':checked') );

}


function readWOList(casekey, field) {

     WO_List = getWO_List();

     if(typeof WO_List[casekey] == 'undefined') {
          return 'null';
     } else {

		  return WO_List[casekey][field];
     }

}

// Are there any blacks here?
function anyBlacks() {
     colors = tsp.getColors();
     //keys = tsp.getCasekeys();
     for(var i=0; i<colors.length;i++) {
          if(colors[i]=='Black') {
               jQuery('#special_opts').show();
               return;
          }
     }
     jQuery('#special_opts').hide();
}

function deleteBlacks() {
     colors = tsp.getColors();
     keys = tsp.getCasekeys();
     deleteList = new Array();
     deSelectAllMarkers(); //
     jQuery.each(colors, function(k, v) {
          if(k==0) return;
          if(v=='Black') {
               deleteList.push(k);
          }
     });

     // close any open infowindows
     for(var x=0;x<markers.length;x++) {
          if(typeof markers[x].infoWindow!='undefined') {
               markers[x].infoWindow.close();
          }
     }

     bulkDelete(deleteList);

     jQuery('#special_opts').hide();
}


function save_dialog() {
     jQuery('#dialog_save').dialog('open');
}

function doOptimizeRoute(){
     if(start_address===''||start_address===null||typeof start_address==='undefined'){
          //jQuery('#ui-accordion-accordion-panel-0').addClass('ui-state-error');
          jQuery('#dialog_start_address').dialog('open');
     }
          directions(1, false, jQuery('[id$=avoidHighways]').is(':checked'));

}
