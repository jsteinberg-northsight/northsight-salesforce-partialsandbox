(function(skuid){
skuid.snippet.register('CompanyDupe',function(args) {var $ = skuid.$,
    field = arguments[0],
    value = arguments[1];
    
labels = {
      'CompanyDupeCompanyDupe' : "Dupe",
    'CompanyDupe' : "Dupe",
     'Unlikely Dupe' : "Unlikely Dupe",
          'Likely Dupe' : "Likely Dupe"
}

$('<div class="tag ' + value +'">' +
        '<div class="border">' +
            labels[value] +
        '</div>' +
    '</div>'
).appendTo( field.element );
});
}(window.skuid));