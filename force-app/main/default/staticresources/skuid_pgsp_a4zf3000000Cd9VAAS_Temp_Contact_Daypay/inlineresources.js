(function(skuid){
skuid.snippet.register('Send Payment Agreement',function(args) {var params = arguments[0],    $ = skuid.$;
//********* Option Declarations (Do not modify )*********// 
var RC = '';var RSL='';var RSRO='';var RROS='';var CCRM='';var CCTM='';var CCNM='';var CRCL=''; var CRL='';var OCO='';var DST='';var LA='';var CEM='';var CES='';var STB='1';var SSB='1';var SES='';var SEM='';var SRS='';var SCS ='';var RES=''; 
//*************************************************// 
var LA ='0';
var CES = 'Payment Election Form';
var CEM = 'Please review and sign the linked Northsight Management payment election form.';
var DST = '73830b48-9a37-4cfb-8dd2-649c0094abb2';
var row = params.row || params.model.getFirstRow();
// write row data to console
console.log(row);
var CRL = 'FirstName~' + row.FirstName + ';LastName~' + row.LastName + ';Email~' + row.Email + ';Role~Customer;RoutingOrder~1'; 
//********* Page Callout (Do not modify) *********// 
window.open("/apex/dsfs__DocuSign_CreateEnvelope?rc=&DSEID=0&SourceID=" + row.Id + "&RC="+RC+"&RSL="+RSL+"&RSRO="+RSRO+"&RROS="+RROS+"&CCRM="+CCRM+"&CCTM="+CCTM+"&CRCL="+CRCL+"&CRL="+CRL+"&OCO="+OCO+"&DST="+DST+"&CCNM="+CCNM+"&LA="+LA+"&CEM="+CEM+"&CES="+CES+"&SRS="+SRS+"&STB="+STB+"&SSB="+SSB+"&SES="+SES+"&SEM="+SEM+"&SRS="+SRS+"&SCS="+SCS+"&RES="+RES,"_blank")
});
}(window.skuid));