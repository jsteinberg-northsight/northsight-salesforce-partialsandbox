(function(skuid){
skuid.snippet.register('DocuSign_Recurring_Services',function(args) {var params = arguments[0],    $ = skuid.$;
//********* Option Declarations (Do not modify )*********// 
var RC = '';var RSL='';var RSRO='';var RROS='';var CCRM='';var CCTM='';var CCNM='';var CRCL=''; var CRL='';var OCO='';var DST='';var LA='';var CEM='';var CES='';var STB='1';var SSB='1';var SES='';var SEM='';var SRS='';var SCS ='';var RES=''; 
//*************************************************// 
var LA ='0';
var CES = 'Northsight Combined Maintenance Agreement';
var CEM = 'Please review and sign the linked maintenance agreement.';
var DST = '5c477d24-c3c6-42d7-82f9-059cd21f73b3';
var row = params.row || params.model.getFirstRow();
// write row data to console
console.log(row);
var CRL = 'FirstName~' + row.FirstName + ';LastName~' + row.LastName + ';Email~' + row.Email + ';Role~Customer;RoutingOrder~1'; 
//********* Page Callout (Do not modify) *********// 
window.location.href ="/apex/dsfs__DocuSign_CreateEnvelope?rc=&DSEID=0&SourceID=" + row.Id + "&RC="+RC+"&RSL="+RSL+"&RSRO="+RSRO+"&RROS="+RROS+"&CCRM="+CCRM+"&CCTM="+CCTM+"&CRCL="+CRCL+"&CRL="+CRL+"&OCO="+OCO+"&DST="+DST+"&CCNM="+CCNM+"&LA="+LA+"&CEM="+CEM+"&CES="+CES+"&SRS="+SRS+"&STB="+STB+"&SSB="+SSB+"&SES="+SES+"&SEM="+SEM+"&SRS="+SRS+"&SCS="+SCS+"&RES="+RES;
});
skuid.snippet.register('Docusign_Maintenance',function(args) {var params = arguments[0],    $ = skuid.$;
//********* Option Declarations (Do not modify )*********// 
var RC = '';var RSL='';var RSRO='';var RROS='';var CCRM='';var CCTM='';var CCNM='';var CRCL=''; var CRL='';var OCO='';var DST='';var LA='';var CEM='';var CES='';var STB='1';var SSB='1';var SES='';var SEM='';var SRS='';var SCS ='';var RES=''; 
//*************************************************// 
var LA ='0';
var CES = 'Northsight Maintenance Agreement';
var CEM = 'Please review and sign the linked maintenance agreement.';
var DST = 'c736bae0-8049-4356-8a4a-3d1931959e35';
var row = params.row || params.model.getFirstRow();
// write row data to console
console.log(row);
var CRL = 'FirstName~' + row.FirstName + ';LastName~' + row.LastName + ';Email~' + row.Email + ';Role~Customer;RoutingOrder~1'; 
//********* Page Callout (Do not modify) *********// 
window.location.href ="/apex/dsfs__DocuSign_CreateEnvelope?rc=&DSEID=0&SourceID=" + row.Id + "&RC="+RC+"&RSL="+RSL+"&RSRO="+RSRO+"&RROS="+RROS+"&CCRM="+CCRM+"&CCTM="+CCTM+"&CRCL="+CRCL+"&CRL="+CRL+"&OCO="+OCO+"&DST="+DST+"&CCNM="+CCNM+"&LA="+LA+"&CEM="+CEM+"&CES="+CES+"&SRS="+SRS+"&STB="+STB+"&SSB="+SSB+"&SES="+SES+"&SEM="+SEM+"&SRS="+SRS+"&SCS="+SCS+"&RES="+RES;
});
skuid.snippet.register('Docusign_3rdParty',function(args) {var params = arguments[0],
	$ = skuid.$;
var params = arguments[0],    $ = skuid.$;
//********* Option Declarations (Do not modify )*********// 
var RC = '';var RSL='';var RSRO='';var RROS='';var CCRM='';var CCTM='';var CCNM='';var CRCL=''; var CRL='';var OCO='';var DST='';var LA='';var CEM='';var CES='';var STB='1';var SSB='1';var SES='';var SEM='';var SRS='';var SCS ='';var RES=''; 
//*************************************************// 
var LA ='0';
var CES = 'Northsight 3rd Party Contract';
var CEM = 'Please review and sign the linked 3rd Party contract.';
var DST = 'e06640d9-26a4-41a9-b1ba-44da4b67141b';
var row = params.row || params.model.getFirstRow();
// write row data to console
console.log(row);
var CRL = 'FirstName~' + row.FirstName + ';LastName~' + row.LastName + ';Email~' + row.Email + ';Role~Customer;RoutingOrder~1'; 
//********* Page Callout (Do not modify) *********// 
window.location.href ="/apex/dsfs__DocuSign_CreateEnvelope?rc=&DSEID=0&SourceID=" + row.Id + "&RC="+RC+"&RSL="+RSL+"&RSRO="+RSRO+"&RROS="+RROS+"&CCRM="+CCRM+"&CCTM="+CCTM+"&CRCL="+CRCL+"&CRL="+CRL+"&OCO="+OCO+"&DST="+DST+"&CCNM="+CCNM+"&LA="+LA+"&CEM="+CEM+"&CES="+CES+"&SRS="+SRS+"&STB="+STB+"&SSB="+SSB+"&SES="+SES+"&SEM="+SEM+"&SRS="+SRS+"&SCS="+SCS+"&RES="+RES;
});
skuid.snippet.register('Docusign_W9',function(args) {var params = arguments[0],
	$ = skuid.$;
var params = arguments[0],    $ = skuid.$;
//********* Option Declarations (Do not modify )*********// 
var RC = '';var RSL='';var RSRO='';var RROS='';var CCRM='';var CCTM='';var CCNM='';var CRCL=''; var CRL='';var OCO='';var DST='';var LA='';var CEM='';var CES='';var STB='1';var SSB='1';var SES='';var SEM='';var SRS='';var SCS ='';var RES=''; 
//*************************************************// 
var LA ='0';
var CES = 'Northsight W9 Form';
var CEM = 'Please review and sign the linked W9 form.';
var DST = '2457a602-a27b-482a-87da-6a2d6e6e24d9';
var row = params.row || params.model.getFirstRow();
// write row data to console
console.log(row);
var CRL = 'FirstName~' + row.FirstName + ';LastName~' + row.LastName + ';Email~' + row.Email + ';Role~Customer;RoutingOrder~1'; 
//********* Page Callout (Do not modify) *********// 
window.location.href ="/apex/dsfs__DocuSign_CreateEnvelope?rc=&DSEID=0&SourceID=" + row.Id + "&RC="+RC+"&RSL="+RSL+"&RSRO="+RSRO+"&RROS="+RROS+"&CCRM="+CCRM+"&CCTM="+CCTM+"&CRCL="+CRCL+"&CRL="+CRL+"&OCO="+OCO+"&DST="+DST+"&CCNM="+CCNM+"&LA="+LA+"&CEM="+CEM+"&CES="+CES+"&SRS="+SRS+"&STB="+STB+"&SSB="+SSB+"&SES="+SES+"&SEM="+SEM+"&SRS="+SRS+"&SCS="+SCS+"&RES="+RES;
});
skuid.snippet.register('Docusign_DDA',function(args) {var params = arguments[0],
	$ = skuid.$;
var params = arguments[0],    $ = skuid.$;
//********* Option Declarations (Do not modify )*********// 
var RC = '';var RSL='';var RSRO='';var RROS='';var CCRM='';var CCTM='';var CCNM='';var CRCL=''; var CRL='';var OCO='';var DST='';var LA='';var CEM='';var CES='';var STB='1';var SSB='1';var SES='';var SEM='';var SRS='';var SCS ='';var RES=''; 
//*************************************************// 
var LA ='0';
var CES = 'Northsight Direct Deposit Authorization';
var CEM = 'Please fill out the attached Direct Deposit Authorization.';
var DST = 'a0bcfdf3-8496-490e-8268-4a1e41675d09';
var row = params.row || params.model.getFirstRow();
// write row data to console
console.log(row);
var CRL = 'FirstName~' + row.FirstName + ';LastName~' + row.LastName + ';Email~' + row.Email + ';Role~Customer;RoutingOrder~1'; 
//********* Page Callout (Do not modify) *********// 
window.location.href ="/apex/dsfs__DocuSign_CreateEnvelope?rc=&DSEID=0&SourceID=" + row.Id + "&RC="+RC+"&RSL="+RSL+"&RSRO="+RSRO+"&RROS="+RROS+"&CCRM="+CCRM+"&CCTM="+CCTM+"&CRCL="+CRCL+"&CRL="+CRL+"&OCO="+OCO+"&DST="+DST+"&CCNM="+CCNM+"&LA="+LA+"&CEM="+CEM+"&CES="+CES+"&SRS="+SRS+"&STB="+STB+"&SSB="+SSB+"&SES="+SES+"&SEM="+SEM+"&SRS="+SRS+"&SCS="+SCS+"&RES="+RES;
});
skuid.snippet.register('Docusign_Wells_CoC',function(args) {var params = arguments[0],
	$ = skuid.$;
var params = arguments[0],    $ = skuid.$;
//********* Option Declarations (Do not modify )*********// 
var RC = '';var RSL='';var RSRO='';var RROS='';var CCRM='';var CCTM='';var CCNM='';var CRCL=''; var CRL='';var OCO='';var DST='';var LA='';var CEM='';var CES='';var STB='1';var SSB='1';var SES='';var SEM='';var SRS='';var SCS ='';var RES=''; 
//*************************************************// 
var LA ='0';
var CES = 'Wells Fargo Code of Conduct';
var CEM = 'Please review and sign the attached Wells Fargo Code of Conduct.';
var DST = 'd1b4170d-2bf9-48dc-bd45-61ff72728903';
var row = params.row || params.model.getFirstRow();
// write row data to console
console.log(row);
var CRL = 'FirstName~' + row.FirstName + ';LastName~' + row.LastName + ';Email~' + row.Email + ';Role~Customer;RoutingOrder~1'; 
//********* Page Callout (Do not modify) *********// 
window.location.href ="/apex/dsfs__DocuSign_CreateEnvelope?rc=&DSEID=0&SourceID=" + row.Id + "&RC="+RC+"&RSL="+RSL+"&RSRO="+RSRO+"&RROS="+RROS+"&CCRM="+CCRM+"&CCTM="+CCTM+"&CRCL="+CRCL+"&CRL="+CRL+"&OCO="+OCO+"&DST="+DST+"&CCNM="+CCNM+"&LA="+LA+"&CEM="+CEM+"&CES="+CES+"&SRS="+SRS+"&STB="+STB+"&SSB="+SSB+"&SES="+SES+"&SEM="+SEM+"&SRS="+SRS+"&SCS="+SCS+"&RES="+RES;
});
skuid.snippet.register('Docusign_SSA89',function(args) {var params = arguments[0],
	$ = skuid.$;
var params = arguments[0],    $ = skuid.$;
//********* Option Declarations (Do not modify )*********// 
var RC = '';var RSL='';var RSRO='';var RROS='';var CCRM='';var CCTM='';var CCNM='';var CRCL=''; var CRL='';var OCO='';var DST='';var LA='';var CEM='';var CES='';var STB='1';var SSB='1';var SES='';var SEM='';var SRS='';var SCS ='';var RES=''; 
//*************************************************// 
var LA ='0';
var CES = 'Northsight SSA 89 Form';
var CEM = 'Please review and sign the attached SSA-89 form.';
var DST = '38e46c5c-6396-4bd7-9978-c219ec3b9abb';
var row = params.row || params.model.getFirstRow();
// write row data to console
console.log(row);
var CRL = 'FirstName~' + row.FirstName + ';LastName~' + row.LastName + ';Email~' + row.Email + ';Role~Customer;RoutingOrder~1'; 
//********* Page Callout (Do not modify) *********// 
window.location.href ="/apex/dsfs__DocuSign_CreateEnvelope?rc=&DSEID=0&SourceID=" + row.Id + "&RC="+RC+"&RSL="+RSL+"&RSRO="+RSRO+"&RROS="+RROS+"&CCRM="+CCRM+"&CCTM="+CCTM+"&CRCL="+CRCL+"&CRL="+CRL+"&OCO="+OCO+"&DST="+DST+"&CCNM="+CCNM+"&LA="+LA+"&CEM="+CEM+"&CES="+CES+"&SRS="+SRS+"&STB="+STB+"&SSB="+SSB+"&SES="+SES+"&SEM="+SEM+"&SRS="+SRS+"&SCS="+SCS+"&RES="+RES;
});
skuid.snippet.register('DocuSign_Auth_For_Consumer_Reports',function(args) {var params = arguments[0],
	$ = skuid.$;
var params = arguments[0],    $ = skuid.$;
//********* Option Declarations (Do not modify )*********// 
var RC = '';var RSL='';var RSRO='';var RROS='';var CCRM='';var CCTM='';var CCNM='';var CRCL=''; var CRL='';var OCO='';var DST='';var LA='';var CEM='';var CES='';var STB='1';var SSB='1';var SES='';var SEM='';var SRS='';var SCS ='';var RES=''; 
//*************************************************// 
var LA ='0';
var CES = 'Authorization for Consumer Reports';
var CEM = 'Please review and sign the linked Authorization For Consumer Reports.';
var DST = '32befe61-e10f-4afa-9a21-5241adb71968';
var row = params.row || params.model.getFirstRow();
// write row data to console
console.log(row);
var CRL = 'FirstName~' + row.FirstName + ';LastName~' + row.LastName + ';Email~' + row.Email + ';Role~Customer;RoutingOrder~1'; 
//********* Page Callout (Do not modify) *********// 
window.location.href ="/apex/dsfs__DocuSign_CreateEnvelope?rc=&DSEID=0&SourceID=" + row.Id + "&RC="+RC+"&RSL="+RSL+"&RSRO="+RSRO+"&RROS="+RROS+"&CCRM="+CCRM+"&CCTM="+CCTM+"&CRCL="+CRCL+"&CRL="+CRL+"&OCO="+OCO+"&DST="+DST+"&CCNM="+CCNM+"&LA="+LA+"&CEM="+CEM+"&CES="+CES+"&SRS="+SRS+"&STB="+STB+"&SSB="+SSB+"&SES="+SES+"&SEM="+SEM+"&SRS="+SRS+"&SCS="+SCS+"&RES="+RES;
});
}(window.skuid));