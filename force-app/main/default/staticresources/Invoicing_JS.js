var	newRowId = 0;
var	grid;
var	ARInvoiceData =	[];
var	APInvoiceData =	[];
var	ARDataView = new Slick.Data.DataView();
var	gridAP
var	APDataView = new Slick.Data.DataView();
var	gridDetail;
var	detailDataView = new Slick.Data.DataView();
detailDataView.getItemMetadata = metadata(detailDataView.getItemMetadata);
var	gridDetailAP;
var	detailAPDataView = new Slick.Data.DataView();
detailAPDataView.getItemMetadata = metadata(detailAPDataView.getItemMetadata);

var	priceBooks = [];
var	invoiceStatuses	= []

var	arPriceBooks =	[];
var	apPriceBooks = [];
var	arPBCategories = [];
var	apPBCategories = [];
var	arPBProducts = [];
var	apVendProducts = [];
var	apPBProducts = [];
var	arInvoiceStatus	=  [];
var	apInvoiceStatus	= [];
var	apSelectedRow;
var	arSelectedRow;
var	APLineItemsToDelete	= [];
var	ARLineItemsToDelete	= [];
var	selectedRowBFNewAR;
var	selectedRowBFNewAP;
var isNewAR = false;
var	previous1;

function metadata(old_metadata) {
	return function(row) {
		var item = this.getItem(row);
    	var meta = old_metadata(row) || {};

    	if (item && item.locked) {
    		meta.cssClasses = meta.cssClasses || '';
    		meta.cssClasses += ' locked-row';
    	}
    	return meta;
	}
}

(function ($) {
	$.fn.extend({
		live: function (event, callback) {
			if (this.selector) {
				jQuery(document).on(event, this.selector, callback);
			}
		}
	});

	$.widget( "custom.combobox", {
		_create: function()	{
			this.wrapper = $( "<span>" )
			  .addClass( "custom-combobox" )
			  .insertAfter(	this.element );

			this.element.hide();
			this._createAutocomplete();
			this._createShowAllButton();
		},
		_createAutocomplete: function()	{
			var	selected = this.element.children( ":selected" ),
			value =	selected.val() ? selected.text() : "";

			this.input = $(	"<input>" )
			.appendTo( this.wrapper	)
			.val( value	)
			.attr( "title",	"" )
			.addClass( "custom-combobox-input ui-widget	ui-widget-content ui-state-default ui-corner-left" )
			.autocomplete({
				delay: 0,
				minLength: 0,
				autoFocus: true,
				source:	$.proxy( this, "_source" )
			});
			var	COMBOBOX = this;
			$(this.input).on({
				autocompleteselect:	function( event, ui	) {
					ui.item.option.selected	= true;
					COMBOBOX._trigger( "select", event,	{
						item: ui.item.option
					});
				},

				autocompletechange:	"_removeIfInvalid"
			});

			// Pass	empty string as	value to search	for, displaying	all	results
			this.input.autocomplete( "search", "" );
			this.input.focus();
		},

		_createShowAllButton: function() {
			var	input =	this.input,
			wasOpen	= false;

			$( "<a>" )
			.attr( "tabIndex", -1 )
			.attr( "title",	"Show All Items" )
			.appendTo( this.wrapper	)
			.button({
				icons: {
					primary: "ui-icon-triangle-1-s"
				},
				text: false
			})
			.removeClass( "ui-corner-all" )
			.addClass( "custom-combobox-toggle ui-corner-right"	)
			.mousedown(function() {
				wasOpen	= input.autocomplete( "widget" ).is( ":visible"	);
			})
			.click(function() {
				input.focus();

				// Close if	already	visible
				if ( wasOpen ) {
					return;
				}

				// Pass	empty string as	value to search	for, displaying	all	results
				input.autocomplete(	"search", "" );
			});
		},

		_source: function( request,	response ) {
			var	matcher	= new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
			response( this.element.children( "option" ).map(function() {
				var	text = $( this ).text();
				if ( this.value	&& ( !request.term || matcher.test(text) ) )
					return {
						label: text,
						value: text,
						option:	this
					};
				}
			));
		},

		_removeIfInvalid: function(	event, ui )	{

			// Selected	an item, nothing to	do
			if ( ui.item ) {
				return;
			}

			// Search for a	match (case-insensitive)
			var	value =	this.input.val(),
			valueLowerCase = value.toLowerCase(),
			valid =	false;
			this.element.children( "option"	).each(function() {
				if ( $(	this ).text().toLowerCase()	===	valueLowerCase ) {
					this.selected =	valid =	true;
					return false;
				}
			});

			// Found a match, nothing to do
			if ( valid ) {
				return;
			}

			var $opt = this.element.children( "option"	).first().next();
			if(value != "" && $opt.length > 0){
				this.input.val( $opt.text() );
				this.element.val( $opt.val() );
				this.input.autocomplete( "instance"	).term = $opt.text();
				return;
			}
			// Remove invalid value
			this.input
			.val( "" )
			.attr( "title",	value +	" didn't match any item" )
			this.element.val( "" );
			this.input.autocomplete( "instance"	).term = "";
		},

		_destroy: function() {
			this.wrapper.remove();
			this.element.show();
		}
	});
	$.extend(true, window, {
		"Slick": {
			"Editors": {
				"Auto":	AutoCompleteEditor
			}
		}
    });
    

    
    //TBH-20180104: important to use (document).on....  as opposed to $("#elem").event() because VisualForce\AJAX partial refreshes
    //which will cause the event bindings to be lost if you use the latter approach
    $(document).on('focus',"#pricebookInput-AR", {}, function(e){
        console.log(this.id + ' focus()');
        previous1 =	this.value;
        }
    );

    
    $(document).on('change','#pricebookInput-AR',{},function(e){
            console.log("pricebookInput-AR changed");
            var	invNum = $('#invoice-AP').text();
            var	invoiceLines = detailDataView.getItems();
            if(invoiceLines.length > 0 && invNum.indexOf('NEW')	< 0) {
                $( "#errorMsg" ).html(INVOICE_PB_VALIDATE);
                $( "#dialog-error" ).dialog("open");
                this.value = previous1;
            }
            else{
                
                var currentValBlank = this.value ? false : true; //TBH-20180104: is the current value blank/null?
                var previousValBlank = previous1 ? false : true;
                if((currentValBlank && !previousValBlank) || (!currentValBlank && previousValBlank) ){  //TBH-20180104:if we transitioned from a blank val to a nonblank val or viceversa
                    console.log(this.id + ' transitioned from blank to non-blank value (or viceversa)');
                    editGridDetailAR();
                }

                previous1 =	this.value;  //TBH-20180104:this handles the situation where user changes pricebook values without losing focus off the dropdown
            }

        }
    )
})(jQuery);


$( document ).ready(function() {
    console.log('$.document.ready');
   
});



var	previous2;

$("select[id=pricebookInput-AP]")
	.focus(function	() {
		// Store the current value on focus, before	it changes
		previous2 =	this.value;
	})
	.change(function() {
		var	invNum = $('#invoice-AP').text();
		var	invoiceLines = detailAPDataView.getItems();
		if(invoiceLines.length > 0 && invNum.indexOf('NEW')	< 0) {
			$( "#errorMsg" ).html(INVOICE_PB_VALIDATE);
			$( "#dialog-error" ).dialog("open");
			this.value = previous2;
		}
	});


//Now you can use jquery to	hook up	your delete	button event
$('.delAR').live('click', function(){
	var	me = $(this), id = me.attr('id');

	//assuming you have	used a dataView	to create your grid
	//also assuming	that its variable name is called 'dataView'
	//use the following	code to	get	the	item to	be deleted from	it
	detailDataView.deleteItem(id);


	//This is possible because in the formatter	we have	assigned the row id	itself as the button id;
	//now assuming your	grid is	called 'grid'
	gridDetail.invalidate();
	gridDetail.render();

	calculateData('AR');
	if(id.substring(0,3)!='new'){
		ARLineItemsToDelete.push(id);
	}
});

//Now you can use jquery to	hook up	your delete	button event
$('.delAP').live('click', function(){
	var	me = $(this), id = me.attr('id');

	//assuming you have	used a dataView	to create your grid
	//also assuming	that its variable name is called 'dataView'
	//use the following	code to	get	the	item to	be deleted from	it
	detailAPDataView.deleteItem(id);

	//This is possible because in the formatter	we have	assigned the row id	itself as the button id;
	//now assuming your	grid is	called 'grid'
	gridDetailAP.invalidate();
	gridDetailAP.render();
	calculateData('AP');
	if(id.substring(0,3)!='new'){
		APLineItemsToDelete.push(id);
	}
});

function makeOption(val) {
	if(val.value!=undefined&&val.value.id!=undefined){
		return "<OPTION	value='"+val.value.id+"'>"+val.label+"</OPTION>";
	}
	return "<OPTION	value='"+((val.value!=undefined)?val.value:"")+"'>"+val.label+"</OPTION>";
}
function requiredFieldValidator(value) {
	if (value == null || value == undefined	|| !value.length) {
		return {valid: false, msg: "This is	a required field"};
	}
	else {
		return {valid: true, msg: null};
	}
}
function loadReceivableInvoice() {

	ARDataView.onRowsChanged.subscribe(function (e, args)	{
		$("#ARCount").html(ARDataView.getLength());
		grid.invalidateRows(args.rows);
		grid.render();
	});

	grid = new Slick.Grid("#myGridAR", ARDataView, columns, invoiceGridOptions);
	grid.setSelectionModel(new Slick.RowSelectionModel());
	grid.onSort.subscribe(function (e, args) {
		var	cols = args.sortCols;
		ARDataView.sort(function (dataRow1,	dataRow2) {
			for	(var i = 0,	l =	cols.length; i < l;	i++) {
			  var field	= cols[i].sortCol.field;
			  var sign = cols[i].sortAsc ? 1 : -1;
			  var value1 = dataRow1[field],	value2 = dataRow2[field];
			  if(value1	===	undefined || value1	===	null) {	value1 = '';}
			  if(value2	===	undefined || value2	===	null) {	value2 = '';}
			  var result = (value1 == value2 ? 0 : (value1 > value2	? 1	: -1)) * sign;
			  if (result !=	0) {
				return result;
			  }
			}
		return 0;
		});
		grid.invalidate();
		grid.render();
	});

	//CLICK	AR
	grid.onClick.subscribe(function(e, args) {
		if(AREditMode){
		  return;
		}
		grid.setSelectedRows([args.row]);
		//arSelectedRow	= [args.row];
	});

	grid.onSelectedRowsChanged.subscribe(function(e, args) {
		$("#AREditButton").show();
		$("#ARPrintButton").show();
		var item = ARDataView.getItem(grid.getSelectedRows()[0]);
		setPrintButtonUrl(item.id, '#ARPrintButton');
		$('#priceBook-AR').text(getOptionLabel(arPriceBooks,item.priceBook));
		$('#pricebookInput-AR').val(item.priceBook);
		var	lines;
		var	formatCreated =	getFormattedDate(item.created);
		var	formatSentDate = getFormattedDate(item.sent);
		$('#invoice-AR').text(item.invoiceNumber);
		$('#to-AR').text(item.client);
		loadARCategories(item.priceBook);
		loadARProductOptions(item.priceBook, function(){

			$('#created-AR').text(formatCreated);
		$('#sent-AR').text(formatSentDate);
		//$('#sentInput-AR').text(item.sent);
		$("#sentInput-AR").datepicker('setDate', formatSentDate);

		//Code added - Padmesh Soni	(12/30/2014) - AC-6:Invoice	Screen | Visual	Force Page Prototype
		$('#status-AR').text(getOptionLabel(invoiceStatuses	,item.status));
		$('#statusInput-AR').text(getOptionLabel(invoiceStatuses ,item.status));
		//$('#statusInput-AR').val(item.status);

		$('#publicCmtsAR').val(item.publicNotes);
		$('#privateCmtsAR').val(item.privateNotes);
		dataDetail = [];
		if(item.id != null && item.id.substring(0,3)!='new'){

			$('#newInvoiceAR').prop( "disabled",true);
			InvoicingController.loadLines(
				item.id,
				function(results, event) {

					if(event.type === 'exception'){
						console.log("exception");
						console.log(event);
					} else if(event.status){

						$.each(results,	function(i,	result){

						  dataDetail[i]	= {
									lineNumber:	i+1,
									id:result.id,
									category: result.productCategory,
									product: result.productId,
									description: result.description,
									uom: result.uom,
									qty: result.qty,
									rate: result.unitPrice,
									originalRate: result.originalPrice,
									discount: result.discount,
									amount:	'',
									recordType:result.recordType,
									locked: result.locked
							  };
						});
					} else{
						console.log(event.message);
					}

					$('#newInvoiceAR').prop( "disabled", false);
					detailDataView.beginUpdate();
					detailDataView.setItems(dataDetail);
					detailDataView.endUpdate();
					gridDetail.invalidateAllRows();
					gridDetail.render();
					gridDetail.resizeCanvas();
					calculateData("AR");
				},{escape: false}
			);
		}else{
			$('#newInvoiceAR').prop( "disabled",false);
			detailDataView.beginUpdate();
			detailDataView.setItems(dataDetail);
			detailDataView.endUpdate();
			gridDetail.invalidateAllRows();
			gridDetail.render();
			gridDetail.resizeCanvas();
			calculateData("AR");
		}


		});

	});

	//END CLICK
	grid.init();

	ARDataView.beginUpdate();
	ARDataView.setItems(ARInvoiceData);
	ARDataView.endUpdate();
	grid.invalidate();
	grid.render();
	loadReceivableInvoiceLines();
}

function setPrintButtonUrl(pInvoiceId, pButtonId) {
	if (pInvoiceId.length != 18) {
		$(pButtonId).hide();
	} else {
		$(pButtonId).show();
	}
	//remove existing invoice Id and append new one
	var href = $(pButtonId).parent().attr('href').split('=')[0];
	href += '=' + pInvoiceId;
	$(pButtonId).parent().attr('href', href);
}

function handleProduct(args){
			var	found = [];
			if(args.grid.getColumns()[args.cell].optionsOverride!=null){
			found = $.grep(args.grid.getColumns()[args.cell].optionsOverride,function(e){
				  var value	= args.item[args.grid.getColumns()[args.cell].field];
				  console.log("value: "+ value);
				  if(e.value!=undefined	&& e.value.id!=undefined){
					return e.value.id == value;
				  }
				  return e.value ==	value;
				  }
				);
			}
			if(found == null || found.length <= 0){
			found =	$.grep(args.grid.getColumns()[args.cell].options,function(e){
			  var value	= args.item[args.grid.getColumns()[args.cell].field];
			  console.log("value: "+ value);
			  if(e.value!=undefined	&& e.value.id!=undefined){
				return e.value.id == value;
			  }
			  return e.value ==	value;
			  }
			);
			}
			if(found.length	> 0	&& found[0].value != undefined){
				args.item.rate = found[0].value.rate;
				args.item.originalRate =args.item.rate;
				args.item.uom  = found[0].value.uom;
				args.item.pbeId = found[0].value.pbeId;
				args.item.cpId = found[0].value.cpId;
				if(args.item.qty ==	null){
				  args.item.qty	= 1;
				}
			}
}
function loadReceivableInvoiceLines() {

	//Detail View
	var	dataDetail = [];

	gridDetail = new Slick.Grid("#myGridDetail", detailDataView, columnsDetail,	invoiceLineGridOptions);
	gridDetail.onCellChange.subscribe(function(e,args){
	  var columnId	= args.grid.getColumns()[args.cell].id;
	  console.log(columnId);
		if(columnId	== "category"){
			args.item.product =	null;
			args.item.cpId = null;
			args.item.pbeId = null;
			args.item.rate = null;
			args.item.originalRate =null;
			args.item.uom =	null;
			args.item.discount = null;
			args.item.qty =	null;
		}
		if(columnId	== "product"){
			handleProduct(args);
		}

		calculateData("AR");
		args.grid.invalidateRow(args.row);
		args.grid.render();
	});
	gridDetail.onSort.subscribe(function (e, args) {
		var	cols = args.sortCols;

		detailDataView.sort(function (dataRow1,	dataRow2) {
			for	(var i = 0,	l =	cols.length; i < l;	i++) {
			  var field	= cols[i].sortCol.field;
			  var sign = cols[i].sortAsc ? 1 : -1;
			  var value1 = dataRow1[field],	value2 = dataRow2[field];
			  if(value1	===	undefined || value1	===	null) {	value1 = '';}
			  if(value2	===	undefined || value2	===	null) {	value2 = '';}
			  var result = (value1 == value2 ? 0 : (value1 > value2	? 1	: -1)) * sign;
			  if (result !=	0) {
				return result;
			  }
			}
			return 0;
		});
		gridDetail.invalidate();
		gridDetail.render();
	});

	gridDetail.onAddNewRow.subscribe(function (e, args)	{
		var	item = args.item;
		item.lineNumber	= gridDetail.getData().getLength() + 1;
		detailDataView.getItemMetadata(item.lineNumber, item.locked);
		item.id	= "new"+newRowId;
		newRowId++;
		gridDetail.invalidateRow(item.lineNumber);
		gridDetail.invalidateRow(item.lineNumber-1);
		gridDetail.getData().addItem(item);
		gridDetail.updateRowCount();
		gridDetail.render();
		gridDetail.resizeCanvas();
		validatePB("AR");
	});

	gridDetail.onBeforeEditCell.subscribe(function(e,args) {
		if (args && args.item && args.item.locked === true) {
			return false;
		}
	});

	gridDetail.init();
	detailDataView.beginUpdate();
	detailDataView.setItems(dataDetail);
	detailDataView.endUpdate();
	gridDetail.render();
	gridDetail.resizeCanvas();
  //grid.setSelectedRows(grid.getSelectedRows());
}

function loadPayableInvoice() {

	APDataView.onRowsChanged.subscribe(function	(e,	args) {
		$("#APCount").html(APDataView.getLength());
		gridAP.invalidateRows(args.rows);
		gridAP.render();
	});

	//GRID AP
	gridAP = new Slick.Grid("#myGridAP", APDataView, APcolumns,	invoiceGridOptions);
	gridAP.setSelectionModel(new Slick.RowSelectionModel());
	gridAP.onSort.subscribe(function (e, args) {
		var	cols = args.sortCols;
		APDataView.sort(function (dataRow1,	dataRow2) {
			for	(var i = 0,	l =	cols.length; i < l;	i++) {
			  var field	= cols[i].sortCol.field;
			  var sign = cols[i].sortAsc ? 1 : -1;
			  var value1 = dataRow1[field],	value2 = dataRow2[field];
			  if(value1	===	undefined || value1	===	null) {	value1 = '';}
			  if(value2	===	undefined || value2	===	null) {	value2 = '';}
			  var result = (value1 == value2 ? 0 : (value1 > value2	? 1	: -1)) * sign;
			  if (result !=	0) {
				return result;
			  }
			}
			return 0;
		});
		gridAP.invalidate();
		gridAP.render();
	});
	//CLICK	AP
	gridAP.onClick.subscribe(function(e, args) {
		if(APEditMode){
		  return;
		}
		gridAP.setSelectedRows([args.row]);
		//apSelectedRow	= [args.row];
	});
	gridAP.onSelectedRowsChanged.subscribe(function(e, args) {
		$("#APEditButton").show();
		$("#APPrintButton").show();
		var item = APDataView.getItem(gridAP.getSelectedRows()[0]);
		setPrintButtonUrl(item.id, '#APPrintButton');
		//alert(item.id);
		var	lines;

		if(item	!= undefined) {
			var	formatCreated =	getFormattedDate(item.created);
			var	formatSentDate = getFormattedDate(item.sent);
			$('#invoice-AP').text(item.invoiceNumber);
			$('#to-AP').text(item.vendor);
			$('#priceBook-AP').text(getOptionLabel(apPriceBooks,item.priceBook));
			$('#pricebookInput-AP').val(item.priceBook);
			loadAPCategories(item.priceBook);
			loadAPProductOptions(item.priceBook,item.vendorId);
			$('#created-AP').text(formatCreated);
			$('#sent-AP').text(formatSentDate);
			//$('#sentInput-AP').text(formatSentDate);
			$("#sentInput-AP").datepicker('setDate', formatSentDate);

			//Code added - Padmesh Soni	(12/30/2014) - AC-6:Invoice	Screen | Visual	Force Page Prototype
			$('#status-AP').text(getOptionLabel(invoiceStatuses	,item.status));
			$('#statusInput-AP').text(getOptionLabel(invoiceStatuses ,item.status));
			//$('#statusInput-AP').val(item.status);

			$('#publicCmtsAP').val(item.publicNotes);
			$('#privateCmtsAP').val(item.privateNotes);
			dataDetailAP = [];
			if(item.id!=null&&item.id.substring(0,3)!='new'){
				InvoicingController.loadLines(
					item.id,
					function(results, event) {

						if(event.type === 'exception'){
							console.log("exception");
							console.log(event);
						}else if(event.status){
							$.each(results,	function(i,	result){

							  //var	delLink	= "<input type='button'	value='Delete' onclick=''/>";
							  //"<a	href = \"{!baseURL}{!$Site.Prefix}/"+data[i].Id+"\"	target='_blank'>"+data[i].CaseNumber+"</a>";

								dataDetailAP[i]	= {
									lineNumber:	i+1,
									id:result.id,
									category: result.productCategory,
									product: result.productId,
									description: result.description,
									uom: result.uom,
									qty: result.qty,
									rate: result.unitPrice,
									originalRate: result.originalPrice,
									discount: result.discount,
									amount:	'',
									//delLinkAP:delLink,
									recordType:result.recordType,
									locked: result.locked
								  };

							});

						}else{
							console.log(event.message);
						}
						detailAPDataView.beginUpdate();
						detailAPDataView.setItems(dataDetailAP);
						detailAPDataView.endUpdate();
						gridDetailAP.invalidateAllRows();
						gridDetailAP.render();
						gridDetailAP.resizeCanvas();
						calculateData("AP");
					},{escape: false}
				);
			} else{
				detailAPDataView.beginUpdate();
				detailAPDataView.setItems(dataDetailAP);
				detailAPDataView.endUpdate();
				gridDetailAP.invalidateAllRows();
				gridDetailAP.render();
				gridDetailAP.resizeCanvas();
				calculateData("AP");
			}
		}
	});
	//END CLICK


	gridAP.init();
	APDataView.beginUpdate();
	APDataView.setItems(APInvoiceData);
	APDataView.endUpdate();
	gridAP.invalidate();
	gridAP.render();
	loadPayableInvoiceLines();
}
function loadPayableInvoiceLines() {
  //Detail View	AP (RIGHT)
	var	dataDetailAP = [];

	gridDetailAP = new Slick.Grid("#myGridDetailAP", detailAPDataView, columnsDetailAPRight, invoiceLineGridOptions);
	gridDetailAP.onCellChange.subscribe(function(e,args){
		var	columnId  =	args.grid.getColumns()[args.cell].id;
		if(columnId	== "category"){
			args.item.product =	null;
			args.item.pbeId = null;
			args.item.cpId = null;
			args.item.rate = null;
			args.item.originalRate = null;
			args.item.uom =	null;
			args.item.discount = null;
			args.item.qty =	null;
		}

		if(columnId	== "product"){
			handleProduct(args);
		}
		calculateData("AP");
		args.grid.invalidateRow(args.row);
		args.grid.render();
	});
	gridDetailAP.onSort.subscribe(function (e, args) {
		var	cols = args.sortCols;

		detailAPDataView.sort(function (dataRow1, dataRow2)	{
			for	(var i = 0,	l =	cols.length; i < l;	i++) {
			  var field	= cols[i].sortCol.field;
			  var sign = cols[i].sortAsc ? 1 : -1;
			  var value1 = dataRow1[field],	value2 = dataRow2[field];
			  var result = (value1 == value2 ? 0 : (value1 > value2	? 1	: -1)) * sign;
			  if (result !=	0) {
				return result;
			  }
			}
			return 0;
		});
		gridDetailAP.invalidate();
		gridDetailAP.render();
	});
	gridDetailAP.onAddNewRow.subscribe(function	(e,	args) {

		var	item = args.item;
		item.lineNumber	= gridDetailAP.getData().getLength() + 1;
		detailAPDataView.getItemMetadata(item.lineNumber, item.locked);
		item.id	= "new"+newRowId;
		newRowId++;
		gridDetailAP.invalidateRow(item.lineNumber);
		gridDetailAP.invalidateRow(item.lineNumber-1);
		gridDetailAP.getData().addItem(item);
		gridDetailAP.updateRowCount();
		gridDetailAP.render();
		validatePB("AP");
	});

	gridDetailAP.onBeforeEditCell.subscribe(function(e,args) {
		if (args && args.item && args.item.locked === true) {
			return false;
		}
	});

	gridDetailAP.init();
	detailAPDataView.beginUpdate();
	detailAPDataView.setItems(dataDetailAP);
	detailAPDataView.endUpdate();
	gridDetailAP.render();
}

 function loadAPPricebookOptions(caseId){

	InvoicingController.getAPPriceBooks(caseId,function(results,event){

		apPriceBooks = results;
		var	option_str = "";
		apPriceBooks.forEach(function(v){
			option_str += makeOption(v);
		});
		$("#pricebookInput-AP").html(option_str);
		$("#sentInput-AP").datepicker();
		gridAP.getColumns()[gridAP.getColumnIndex("priceBook")].options=apPriceBooks;
		gridAP.invalidateAllRows();
		gridAP.render();

		//DEFAULT FIRST	ROW
		// Select the first row
		if(gridAP.getDataLength() > 0) {
			gridAP.setSelectedRows([0]);
		}
	});
}

function loadARPricebookOptions(caseId){
	InvoicingController.getARPriceBooks(caseId,function(results,event){
		arPriceBooks = results;
		var	option_str = "";
		arPriceBooks.forEach(function(v){
			option_str += makeOption(v);
		});
		$("#pricebookInput-AR").html(option_str);
		$("#sentInput-AR").datepicker();
		$("#sentInput-AR").datepicker();

		grid.getColumns()[grid.getColumnIndex("priceBook")].options=arPriceBooks;
		grid.invalidateAllRows();
		grid.render();

		//DEFAULT FIRST	ROW
		// Select the first row
		if(grid.getDataLength() > 0) {
			grid.setSelectedRows([0]);
		}
	}, {escape : false});
}

function loadPriceBookOptions()	{
	InvoicingController.getPriceBooks(orderId,function(results,event){
		priceBooks = results;
		var	option_str = "";
		priceBooks.forEach(function(v){
				option_str += makeOption(v);
			});
		$("#pricebookInput-AR").html(option_str);
		$("#pricebookInput-AP").html(option_str);
		$("#sentInput-AR").datepicker();
		$("#sentInput-AP").datepicker();

		loadStatusOptions();

		gridAP.getColumns()[gridAP.getColumnIndex("priceBook")].options=priceBooks;
		gridAP.invalidateAllRows();
		gridAP.render();

		//DEFAULT FIRST	ROW
		// Select the first row
		if(gridAP.getDataLength() > 0) {
			gridAP.setSelectedRows([0]);
		}

		grid.getColumns()[grid.getColumnIndex("priceBook")].options=priceBooks;
		grid.invalidateAllRows();
		grid.render();

		//DEFAULT FIRST	ROW
		// Select the first row
		if(grid.getDataLength() > 0) {
			grid.setSelectedRows([0]);
		}
  });
}
function loadStatusOptions(){
	InvoicingController.getStatusValues(function(results,event){
		invoiceStatuses	= results;
		var	option_str = "";
		invoiceStatuses.forEach(function(v){
			option_str += makeOption(v);
		});
		//$("#statusInput-AR").html(option_str);
		//$("#statusInput-AP").html(option_str);
	});
}
function newInvoice(type){
  if(type == 'AR') {
	if(AREditMode){
	  return;
	}

	if(ARDataView.getLength() >	0)
		selectedRowBFNewAR = grid.getSelectedRows();
	if(validateNewInvoice(type)) {
	  var newRowNum	= grid.getDataLength();
	  var pbName = '';
	  if(arPriceBooks.length == 2 || (arPriceBooks[1] !== undefined && arPriceBooks[1].zip === true))
		pbName = arPriceBooks[1].value;

	  ARInvoiceData.push({
		id:	"new"+newRowId,
		invoiceNumber:"NEW",
		discount: "",
		total: "",
		status:	"Open",
		priceBook: pbName,
		client:	clientName,
		clientId: clientId,
		vendor:	"",
		vendorId: "",
		sent: "",
		subTotal: "",
		privateNotes: "",
		publicNotes: ""
	  });
	  //console.log(ARInvoiceData);
	  ARDataView.beginUpdate();
	  ARDataView.setItems(ARInvoiceData);
	  ARDataView.endUpdate();
	  grid.updateRowCount();
	  grid.setSelectedRows([newRowNum]);
	  arSelectedRow	= [newRowNum];
	  grid.render();
	  toggleAREditMode();
	  return;
	} else {

	  $( "#errorMsg" ).html(INVOICE_PB_NOT_AVAILABLE);
	  $( "#dialog-error" ).dialog("open");
	}
  }
  if(type == 'AP') {
	if(APEditMode){
	  return;
	}
	if(orderConId==""){
	  alert("No	vendor (Contact) is	assigned to	the	work order.	Vendor is required.");
	  return;
	}

	if(APDataView.getLength() >	0) {
		selectedRowBFNewAP = gridAP.getSelectedRows();
	}
	if(validateNewInvoice(type)) {
	  var newRowNum	= gridAP.getDataLength();

	  var pbName = '';
	  if(newPayablePB != '') {

		apPriceBooks.forEach(function(v){
		  if(v.value!=undefined	&& v.value == newPayablePB){
			pbName = v.value;
		  }
		});
	  }

	  APInvoiceData.push({
		id:	"new"+newRowId,
		invoiceNumber:"NEW",
		discount: "",
		total: "",
		status:	"Open",
		priceBook: pbName,
		client:	"",
		vendor:	orderConName,
		vendorId: orderConId,
		sent: "",
		subTotal: "",
		privateNotes: "",
		publicNotes: ""
	  });
	  //console.log(ARInvoiceData);
	  APDataView.beginUpdate();
	  APDataView.setItems(APInvoiceData);
	  APDataView.endUpdate();
	  gridAP.updateRowCount();
	  gridAP.setSelectedRows([newRowNum]);
	  apSelectedRow	= [newRowNum];
	  gridAP.render();
	  toggleAPEditMode();
	  return;
	} else {
	  $( "#errorMsg" ).html(AP_INVOICE_PB_NOT_AVAILABLE);
	  $( "#dialog-error" ).dialog("open");
	}
  }
}
function validateNewInvoice(type) {
  if(type == 'AR') {
	var	pbList = document.getElementById("pricebookInput-AR");
	//document.getElementById("jsErrors").innerHTML	= pbList.value;
	if(!(pbList.length > 1)){
	  if(pbList.value == "null"	|| pbList.value	== "undefined" || pbList ==	""){return false;}
	}
  }
  if(type == 'AP') {
	var	pbList = document.getElementById("pricebookInput-AP");
	//document.getElementById("jsErrors").innerHTML	= pbList.value;
	if(!(pbList.length > 1)){
	  if(pbList.value == "null"	|| pbList.value	== "undefined" || pbList ==	""){return false;}
	}
  }
  return true;
}

function getFormattedDate(value){
  if(value == null || value	== "")
	return "";
  var formatted	= $.datepicker.formatDate("mm/dd/yy", new Date(value.replace("-", "/","g")));
  return formatted;
}

function sendAR_AP(type,caseId){


	//AR
	var	thisGrid = (type=='AR')?grid:gridAP;
	Slick.GlobalEditorLock.commitCurrentEdit();
	$( "#dialog-save" ).dialog("open");
	$( "#progress-save"	).progressbar("value",0);
	var	dataSelected = thisGrid.getDataItem(thisGrid.getSelectedRows()[0]);

	//get comments
	var	publicC	= (type=='AR')?$("#publicCmtsAR").val().trim():$("#publicCmtsAP").val().trim();
	var	privateC = (type=='AR')?$("#privateCmtsAR").val().trim():$("#privateCmtsAP").val().trim();
	var	sent = (type=='AR')?$("#sentInput-AR").val():$("#sentInput-AP").val().trim();
	var	pricebook =	(type=='AR')?$("#pricebookInput-AR").val():$("#pricebookInput-AP").val().trim();
	var	status = (type=='AR')?$("#statusInput-AR").text():$("#statusInput-AP").val().trim();

	var	client = (type=='AR')?dataSelected.client:'';
	var	vendor = (type=='AR')?'':dataSelected.vendor;
	var	vendorId = (type=='AR')?'':dataSelected.vendorId;
	var	uom	= dataSelected.uom;
	if(sent	== ""){
		sent = null;
	}
	var	id = dataSelected.id;
	if(id.substring(0,3)=='new'){
		id = null;
		client = (type=='AR')?clientId:'';
		vendor = (type=='AR')?'':orderConName;
		vendorId = (type=='AR')?'':orderConId;
	}
	var	obj	= {
				id:	id,
				client:	client,
				sent:sent,
				uom: uom,
				priceBook: pricebook,
				status:	status ,
				subTotal: isNaN(parseFloat(dataSelected.subTotal))?0:parseFloat(dataSelected.subTotal),
				total: isNaN(parseFloat(dataSelected.total))?0:parseFloat(dataSelected.total),
				vendor:	vendor,
				vendorId: vendorId,
				privateNotes: privateC,
				publicNotes: publicC,
				invoiceNumber: dataSelected.invoiceNumber,
				workOrder: caseId,
				recordType:	type=='AR'?ARInvoiceTypeId:APInvoiceTypeId
			};


	var	invoiceLines = (type=='AR')?detailDataView.getItems():invoiceLines = detailAPDataView.getItems();

	if(invoiceLines.length ==0)	{

		$( "#dialog-save" ).dialog("close");

		$( "#errorMsg" ).html("(1)"+INVOICE_BLANK_NOT_SUPPORTED);
		$( "#dialog-error" ).dialog("open");
	} else {
		var lineError = 0;
		for(var i=0;i<invoiceLines.length;i++){
			if(invoiceLines[i].category==null || invoiceLines[i].category==""){
					$( "#dialog-save" ).dialog("close");
					$( "#errorMsg" ).html("Line "+(i+1)+" is missing a Category.  All lines must have a category and a product.");
					$( "#dialog-error" ).dialog("open");
					return;
			}
			if(invoiceLines[i].product==null || invoiceLines[i].product==""){
					$( "#dialog-save" ).dialog("close");
					$( "#errorMsg" ).html("Line "+(i+1)+" is missing a Product.  All lines must have a category and a product.");
					$( "#dialog-error" ).dialog("open");
					return;
			}
		}
		var	jsonInvoice	= JSON.stringify(obj, null,	2);
		var	jsonLines =	JSON.stringify(invoiceLines, null, 2);

		selectedRowBFNewAR = '';
		selectedRowBFNewAP = '';
		//SAVE INVOICE
		$( "#progress-save"	).progressbar("value",10);

		var	itemsToDelete =	(type=='AR')?ARLineItemsToDelete:APLineItemsToDelete;
		while(itemsToDelete.length>0){
			lineId = itemsToDelete.pop();
			InvoicingController.deleteInvoiceLine(lineId,function(results,event){
				if(event.type === 'exception'){
					console.log("exception");
					console.log(event);
				}
			});
		}

		InvoicingController.saveInvoice(
			jsonInvoice,
			function(results, event) {
				$( "#progress-save"	).progressbar("value",50);
				if(event.type === 'exception'){
					console.log("exception");
					console.log(event);
					$( "#dialog-save" ).dialog("close");
					$( "#errorMsg" ).html("(2)"+INVOICE_NOT_SAVED +	' '	+event.message + ' '+ event.where);
					$( "#dialog-error" ).dialog("open");
				}else if(event.status){
					var	invId =	results;

					//SAVE Lines
					var	linesArray = $.parseJSON(jsonLines);
					var	newLinesArray =	new	Array();
					for(var	i=0; i<linesArray.length; i++) {
						var	auxObj = {};
						if(linesArray[i].id.substring(0,3)!='new'){
							auxObj.id =	linesArray[i].id;
						}
						auxObj.productId = linesArray[i].product;
						auxObj.description = linesArray[i].description;
						auxObj.qty = linesArray[i].qty;
						auxObj.unitPrice = linesArray[i].rate;
						if(linesArray[i].originalRate!=null){
							auxObj.originalPrice = linesArray[i].originalRate;
						}
						auxObj.pbeId = linesArray[i].pbeId;
						auxObj.cpId = linesArray[i].cpId;
						auxObj.productCategory = linesArray[i].category;
						auxObj.discount	= linesArray[i].discount;
						auxObj.uom = linesArray[i].uom;
						auxObj.invoiceId = invId;
						auxObj.recordType =	linesArray[i].recordType;

						if(auxObj.recordType ==	null){
							if(type=='AR'){
								auxObj.recordType =	ARLineTypeId;
							}else{
								auxObj.recordType =	APLineTypeId;
							}
						}
						newLinesArray.push(auxObj);
					}

					var	jsonLinesNew = JSON.stringify(newLinesArray, null, 2);
					$( "#progress-save"	).progressbar("value",75);
					InvoicingController.saveInvoiceLine(
						jsonLinesNew,
						function(results, event) {
							if(event.type === 'exception'){
								console.log("exception");
								console.log(event);
								$( "#dialog-save" ).dialog("close");
								$( "#errorMsg" ).html(INVOICE_NOT_SAVED +	' '	+event.message + ' '+ event.where);

								$( "#dialog-error" ).dialog("open");
							}else if(event.status){
								//console.log(results);
								if(type	== 'AR'){
									refreshAR();
								}else{
									refreshAP();
								}
							}else{
								console.log(event.message);
							}
							if(type	== 'AR'){
								toggleAREditMode();
							}else{
								toggleAPEditMode();
							}
							$( "#progress-save"	).progressbar("value",100);
							$( "#dialog-save" ).dialog("close");
						}
					);
				}else{
					console.log(event.message);
				}
			}
		);
	}
}

function calculateData(type){
  var subtotalC	= 0;
  var totalC = 0;
  var amountT =	0;
  //AR
  var currentData =	detailDataView.getItems();
  if(type=='AP')
	currentData	= detailAPDataView.getItems();

  for(var key in currentData) {
	if(currentData[key].qty	!= null	&& currentData[key].rate !=	null){
	  subtotalC	+= currentData[key].qty*currentData[key].rate;
	  if(typeof	currentData[key].discount != "number"){
		//currentData[key].discount	= 0;
	  }
	  amountT =	currentData[key].amount;
	  totalC +=	parseFloat(amountT);
	 }
  }
  subtotalC	= subtotalC.toFixed(2);
  totalC = totalC.toFixed(2);

  var discountTotal	= (100 - (100 *	(totalC/subtotalC))).toFixed(2);
  //Update values
  discountTotal	= isNaN(discountTotal) ? '0' : discountTotal;
  setText('discountTotal-'+type,discountTotal+"%");
  setText('subTotal-'+type,"$"+subtotalC);
  totalC = isNaN(totalC) ? '0.00' : totalC;
  setText('total-'+type,"$"+totalC);

  //SET FOCUS AFTER UPDATE LABELS
  if(isNewAR){
	gridDetail.setActiveCell(0,1);
	gridDetail.editActiveCell(Slick.Editors.SelectCellEditor);
	isNewAR = false;
  }

}
function setText(id, value)	{
  $('#'+id).text(value);
}
var	DateFormatter =	function(row, cell,	value, columnDef, dataContext){
  if(value == null || value	== "")
	return "";
  var formatted	= $.datepicker.formatDate("mm/dd/yy", new Date(value.replace("-", "/","g")));
  return formatted;
}
var	CurrencyFormatter=function(row,	cell, value, columnDef,	dataContext){
  if(value==null ||value==undefined||typeof	value != "number"){
	return "";
  }
  return "$"+(value.toFixed(2));
}
var	WholePercentFormatter=function(row,	cell, value, columnDef,	dataContext){
  if(typeof	value!="number"){
	value =	0;
	return "";
  }
  return value+"%";
}
var	AmountCellFormatter=function(row, cell,	value, columnDef, dataContext){
  var d	= dataContext;
  if(typeof	d.discount != "number"){
	d.discount = 0;
  }
  if(typeof	d.rate != "number"){
	d.rate = 0 ;
  }
  var amt =	d.qty*(d.rate-((d.discount/100)*d.rate));
  if(isNaN(amt)){
	return "";
  }
  d.amount = amt.toFixed(2);
  return "$"+amt.toFixed(2);
}
var	SelectCellFormatter=function(row, cell,	value, columnDef, dataContext) {
  return getOptionLabel(columnDef.options,value);
}
var	getOptionLabel = function(options,value){
  if(options.length==0){
	return value;
  }
  var opt =	$.grep(options,	function(e){
	if(e.value!=undefined && e.value.id!=undefined){
	  return e.value.id	== value;
	}
	return e.value == value;
  });
  if(opt.length==0){
	return '';
  }
  return opt[0].label;
}
var	SelectCellEditor = function(args) {

	var $select;
	var $combobox;
	var defaultValue;
	var scope	= this;

	this.init	= function() {

		args.column.options.sort(function(a, b) {
			if (a.label < b.label) {
				return -1;
			}
			if (a.label > b.label) {
				return 1;
			}
			return 0;
		});

		option_str = ""
		var	options	= [];
		options	=args.column.options;
		options.forEach(function(v){
			option_str += makeOption(v);
		});

		$select	= $("<SELECT tabIndex='0' class='editor-select'>"+ option_str +"</SELECT>");
		$select.val(args.item[args.column.field]);
		$select.appendTo(args.container);
		$combobox =	$select.combobox();
		$('.custom-combobox').bind("keydown.nav", function (e) {
			if (e.keyCode	===	$.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT)
				e.stopImmediatePropagation();
			else if (e.keyCode === $.ui.keyCode.UP ||	e.keyCode === $.ui.keyCode.DOWN)
				e.stopPropagation();
			else if (e.keyCode === $.ui.keyCode.ENTER &&	!$(this).data("selectVisible") )
				e.stopImmediatePropagation();
        });

		//this.focus();
	};

	this.destroy = function(item)	{
		//$select.remove();
		$('.ui-autocomplete').remove();
		$('.custom-combobox').hide();

		if($combobox.val() == '') {

			var options1 = args.column.options;
			if(options1.length > 0) {
				$combobox.val(options1[1]);
			}
		}
		$combobox.remove();
	};

	this.focus = function() {
		//$select.focus();
		$combobox.focus();
		$combobox.autocomplete(	"search", $combobox.val() );
	};

	this.loadValue = function(item) {
		defaultValue = item[args.column.field];
		$combobox.val(defaultValue);
	};
	this.serializeValue =	function(){
		return $combobox.val();
	}
	this.applyValue =	function(item,value){
		item[args.column.field]	= value;
	}
	this.isValueChanged =	function(){
		return (!($combobox.val() == ""	&& defaultValue	== null)) && ($combobox.val() != defaultValue);
	}
	this.validate	= function(){
		if(args.column.validator!=undefined)
			return args.column.validator($combobox.val());
		return {valid:true};
	}
	this.init();
};

function DecimalEditor(args) {
	var $input;
	var defaultValue;
	var scope	= this;

	this.init	= function () {
		$input = $("<INPUT type=text class='editor-text' />");

		$input.bind("keydown.nav", function	(e)	{
			if (e.keyCode === $.ui.keyCode.LEFT	|| e.keyCode === $.ui.keyCode.RIGHT) {
				e.stopImmediatePropagation();
			}
		});

		$input.appendTo(args.container);
		$input.focus().select();
	};

	this.destroy = function () {
		$input.remove();
	};

	this.focus = function	() {
		$input.focus();
	};

	this.loadValue = function	(item) {
		defaultValue = item[args.column.field];
		$input.val(defaultValue);
		$input[0].defaultValue = defaultValue;
		$input.select();
	};

	this.serializeValue =	function ()	{
		return parseFloat($input.val())	|| 0;
	};

	this.applyValue =	function (item,	state) {
		item[args.column.field]	= state;
	};

	this.isValueChanged =	function ()	{
		return (!($input.val() == "" &&	defaultValue ==	null)) && ($input.val()	!= defaultValue);
	};

	this.validate	= function () {
		if (isNaN($input.val())) {
			return {
				valid: false,
				msg: "Please enter a valid number"
			};
		}
		if(args.column.validator!=undefined){
			return args.column.validator($input.val());
		}
		return {
			valid: true,
			msg: null
		};
	};
	this.init();
}
var	SelectCellProductsEditor = function(args) {

	var $select;
	var $combobox;
	var defaultValue;
	var scope	= this;

	var calendarOpen = false;
	var container	= args.container;

	var parentElement = container.parentElement;
	var childerenElements = parentElement.children;
	//if(childerenElements[1].outerText == '')
		//alert('Please select category first');

	this.init	= function() {

		option_str = makeOption({label:"",value:""});
		if(args.column.optionsOverride==null){
			args.column.optionsOverride=[];
		}
		var overrideCheck = $.grep(args.column.optionsOverride, function(e){
				if(e.value !=	undefined){
					return e.value.category	== args.item[args.column.filterField];
				}else{
					return e.value == args.item[args.column.filterField];
				}
			});
		var	options	= $.grep(args.column.options, function(e){
			if(overrideCheck != null && overrideCheck.length>0){
				if($.grep(overrideCheck,function(o){return o.value == null || e.value==null ||  o.value.id == e.value.id;}).length>0)
				{
				return false;
				}
			}
			if(e.value !=	undefined){
				return e.value.category	== args.item[args.column.filterField];
			}else{
				return e.value == args.item[args.column.filterField];
			}
		});
		$.merge(options,overrideCheck).forEach(function(v){
			option_str +=	makeOption(v);
		});

		$select	= $("<SELECT tabIndex='0' id='combobox'>"+ option_str +"</SELECT>");
		$select.val(args.item[args.column.field]);
		$select.appendTo(args.container);
		//console.debug(args.item[args.column.field]);
		$combobox =	$select.combobox();
		$('.custom-combobox').bind("keydown.nav", function (e) {
			if (e.keyCode	===	$.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT)
				e.stopImmediatePropagation();
			else if (e.keyCode === $.ui.keyCode.UP ||	e.keyCode === $.ui.keyCode.DOWN)
				e.stopPropagation();
		});
		$('.custom-combobox').focus();
	};

	this.destroy = function()	{
		$('.custom-combobox').hide();
		$('.ui-autocomplete').remove();
		$select.remove();
		$combobox.remove();
	};

	this.focus = function() {
		//$select.focus();
		$combobox.focus();
		$combobox.autocomplete(	"search", $combobox.val() );
	};

	this.loadValue = function(item) {
		defaultValue = item[args.column.field];
		$combobox.val(defaultValue);
	};
	this.serializeValue = function(){
		return $combobox.val();
	}
	this.applyValue = function(item,value){
		item[args.column.field]	= value;
	}

	this.isValueChanged = function(){
		return (!($combobox.val() == ""	&& defaultValue	== null)) && ($combobox.val() != defaultValue);
	}
	this.validate = function(){
		if(args.column.validator!=undefined)
			return args.column.validator($combobox.val());
		return {valid:true};
	}
	this.init();

};

var	columns	= [
  {	id:	"invoice", name: "Invoice",	field: "invoiceNumber",	sortable: true,focusable:false,width:100 },
  {	id:	"from",	name: "Rec.	From", field: "client",	sortable: true,focusable:false,width:200 },
  {	id:	"priceBook", name: "Price Book", field:	"priceBook", sortable: true,options:[],formatter:SelectCellFormatter,focusable:false,width:200 },
  {	id:	"created", name: "Created",	field: "created", sortable:	true,focusable:false,formatter:DateFormatter,width:100	},
  {	id:	"sent",	name: "Sent", field: "sent", sortable: true,focusable:false, formatter:DateFormatter,width:100 },
  {	id:	"status", name:	"Status", field: "status", sortable: true,focusable:false,width:80 },
  {	id:	"total", name: "Total",	field: "total",	sortable: true,formatter:CurrencyFormatter,focusable:false,width:100 }
];
var	APcolumns =	[
  {	id:	"invoice", name: "Invoice",	field: "invoiceNumber",	sortable: true ,focusable:false,width:100},
  {	id:	"to", name:	"Pay To", field: "vendor", sortable: true,focusable:false, width:200 },
  {	id:	"priceBook", name: "Price Book", field:	"priceBook", sortable: true,options:[],optionsOverride:[],formatter:SelectCellFormatter ,focusable:false, width:200},
  {	id:	"created", name: "Created",	field: "created", sortable:	true ,focusable:false,	formatter:DateFormatter, width:100},
  {	id:	"sent",	name: "Sent", field: "sent", sortable: true,focusable:false,  formatter:DateFormatter,width:100	},
  {	id:	"status", name:	"Status", field: "status", sortable: true ,focusable:false,width:80},
  {	id:	"total", name: "Total",	field: "total",	sortable: true,formatter:CurrencyFormatter ,focusable:false,width:100}
];

function formatter(row,	cell, value, columnDef,	dataContext) {
	return value;
}

var	columnsDetail =	[
  {	id:	"lineNumber", name:	"#", field:	"lineNumber", sortable:	false ,width: 10,focusable:false },
  {	id:	"category",	name: "Category", field: "category",options:[],formatter:SelectCellFormatter,editor:SelectCellEditor, sortable:	false,width:140,validator: requiredFieldValidator },
  {	id:	"product", name: "Product",	field: "product", priceBook:"",filterField:"category", options:[],formatter:SelectCellFormatter,editor:SelectCellProductsEditor, sortable: false,width:	220,validator: requiredFieldValidator },
  {	id:	"description", name: "Description",	field: "description", sortable:	false,width: 220,editor:Slick.Editors.LongText},
  {	id:	"uom", name: "UOM",	field: "uom", sortable:	false ,width: 60 },
  {	id:	"qty", name: "Qty",	field: "qty", sortable:	false,width: 46, editor:Slick.Editors.Integer,validator: requiredFieldValidator},
  {	id:	"rate",	name: "Rate", field: "rate", sortable: false ,width: 80, formatter:CurrencyFormatter,editor:DecimalEditor,validator: requiredFieldValidator},
  {	id:	"discount",	name: "Disc", field: "discount", sortable: false,width:	60,formatter:WholePercentFormatter,editor:DecimalEditor},
  {	id:	"amount", name:	"Amount", field: "amount", sortable: false ,width: 100,formatter:AmountCellFormatter }

];

var	columnsDetailAPRight = [
  {	id:	"lineNumber", name:	"#", field:	"lineNumber", sortable:	false ,width: 10,focusable:false },
  {	id:	"category",	name: "Category", field: "category",options:[],formatter:SelectCellFormatter,editor:SelectCellEditor, sortable:	false,width:140,validator: requiredFieldValidator },
  {	id:	"product", name: "Product",	field: "product", priceBook:"",filterField:"category", options:[],formatter:SelectCellFormatter,editor:SelectCellProductsEditor,  sortable:	false,width: 220,validator:	requiredFieldValidator },
  {	id:	"description", name: "Description",	field: "description", sortable:	false,width: 220,editor:Slick.Editors.LongText},
  {	id:	"uom", name: "UOM",	field: "uom", sortable:	false ,width: 60 },
  {	id:	"qty", name: "Qty",	field: "qty", sortable:	false,width: 46, editor:Slick.Editors.Integer,validator: requiredFieldValidator	},
  {	id:	"rate",	name: "Rate", field: "rate", sortable: false ,width: 80, formatter:CurrencyFormatter,editor:DecimalEditor,validator: requiredFieldValidator},
  {	id:	"discount",	name: "Disc", field: "discount", sortable: false,width:	60,formatter:WholePercentFormatter,editor:DecimalEditor	},
  {	id:	"amount", name:	"Amount", field: "amount", sortable: false ,width: 100,	formatter:AmountCellFormatter }
];

var	invoiceGridOptions = {
	enableColumnReorder: false,
	explicitInitialization: true,
	multiColumnSort: true,
	autoEdit:false,
	forceFitColumns: true
};

var	invoiceLineGridOptions = {
	enableCellNavigation:	true,
	enableColumnReorder: false,
	explicitInitialization: true,
	asyncEditorLoading: false,
	multiColumnSort: true,
	editable:false,
	autoEdit:true,
	enableAddRow:false,
	forceFitColumns: true
};

function loadARCategories(pbID,callback){
	InvoicingController.getProductCategories(pbID,function(results,event){
		arPBCategories = results;
		gridDetail.getColumns()[gridDetail.getColumnIndex("category")].options=arPBCategories;
		//gridDetail.invalidateAllRows();
		//gridDetail.render();
		if($.isFunction(callback)){
			callback();
		}
	});
}

function loadAPCategories(pbID, callback){
	InvoicingController.getProductCategories(pbID,function(results,event){
		apPBCategories = results;
		gridDetailAP.getColumns()[gridDetailAP.getColumnIndex("category")].options=apPBCategories;
		//gridDetailAP.invalidateAllRows();
		//gridDetailAP.render();
		if($.isFunction(callback)){
			callback();
		}
	});
}

function loadARProductOptions(pbID, callback){
	InvoicingController.getProducts(pbID,function(results,event){
		arPBProducts = results;
		console.debug("arPBProducts");
		gridDetail.getColumns()[gridDetail.getColumnIndex("product")].options=arPBProducts;
		//gridDetail.invalidateAllRows();
		//gridDetail.render();
		if($.isFunction(callback)){
			callback();
		}
	});
}

function loadAPProductOptions(pbID,vendorId,callback){
	InvoicingController.getProducts(pbID,function(results,event){
		apPBProducts = results;
		InvoicingController.getVendorProducts(vendorId,function(results,event){
			apVendProducts = results;
			gridDetailAP.getColumns()[gridDetailAP.getColumnIndex("product")].optionsOverride=apVendProducts;
			gridDetailAP.getColumns()[gridDetailAP.getColumnIndex("product")].options=apPBProducts;
			//gridDetailAP.invalidateAllRows();
			//gridDetailAP.render();
			if($.isFunction(callback)){
				callback();
			}
		});
	});
}

var	AREditMode = false;
var	toggleAREditMode = function(){

	console.log('toggleAReditMode()');
	arSelectedRow =	grid.getSelectedRows();
	AREditMode = !AREditMode;
	$('#myGridAR').toggleClass("disabled");
	$('#newInvoiceAR').prop( "disabled", function( i, val )	{return	!val;});
	//$('#newInvoiceAR').toggleClass("disabled");
	$('#AREditButton').hide();
	$('#ARPrintButton').hide();
	$('#ARSaveButton').toggle();
	$('#ARCancelButton').toggle();
	$('#priceBook-AR').toggle();
	$('#sent-AR').toggle();
	$('#pricebookInput-AR').toggle();
	$('#sentInput-AR').toggle();

	//Code added - Padmesh Soni	(12/30/2014) - AC-6:Invoice	Screen | Visual	Force Page Prototype
	$('#status-AR').toggle();
	$('#statusInput-AR').toggle();

	//Code added - Padmesh Soni	(12/29/2014) - AC-6:Invoice	Screen | Visual	Force Page Prototype
	if($('#publicCmtsAR').prop('disabled'))
		$('#publicCmtsAR').prop('disabled',	false);
	else
		$('#publicCmtsAR').prop('disabled',true);
	if($('#privateCmtsAR').prop('disabled'))
		$('#privateCmtsAR').prop('disabled', false);
	else
		$('#privateCmtsAR').prop('disabled',true);

	editGridDetailAR();
};


//TBH-20180104: after reviewing this function it seems like its purpose is to "toggle" the editable mode of the invoiceline items grid which
//makes sense as it is called from "toggleAREditMode()"
function editGridDetailAR() {
	

		$('#myGridDetail').toggleClass('editMode');
		var	o =	gridDetail.getOptions();
		console.log('AR grid Editable?: '+o.editable);
		if(o.editable){
			var	cols = gridDetail.getColumns();
			var	i =	gridDetail.getColumnIndex("amount");
			var	cols = gridDetail.getColumns();
			cols[i].width =	100;
			cols.pop();
			gridDetail.setColumns(cols);
			gridDetail.setOptions({editable:false,
			enableAddRow:false});
			//$('#pricebookInput-AR').attr("disabled","");
		} else if($('#pricebookInput-AR').val()!='') {
			var	i =	gridDetail.getColumnIndex("description");
			var	cols = gridDetail.getColumns();
			cols[i].width =	70;
			cols.push({	id:	"delLinkAR", name: "", field: "delLinkAR", sortable: false ,width: 20,focusable:false, formatter:buttonARFormatter});
			gridDetail.setColumns(cols);
			gridDetail.setOptions({
				editable:true,
				enableAddRow:true
			});
			if($('#pricebookInput-AR').val()=='') {
				$('#pricebookInput-AR').focus();
			} else {
				console.log("test1");
				isNewAR = true;//SEE calculateData function
			}
			//$('#pricebookInput-AR').attr("disabled","disabled");
		}
}
var	APEditMode = false;
var	toggleAPEditMode = function(){

	apSelectedRow	= gridAP.getSelectedRows();
	APEditMode = !APEditMode;
	$('#myGridAP').toggleClass("disabled");
	$('#newInvoiceAP').toggleClass("disabled");
	$('#APEditButton').hide();
	$('#APPrintButton').hide();
	$('#APSaveButton').toggle();
	$('#APCancelButton').toggle();
	$('#priceBook-AP').toggle();
	$('#sent-AP').toggle();
	$('#pricebookInput-AP').toggle();
	$('#sentInput-AP').toggle();

	//Code added - Padmesh Soni (12/30/2014) - AC-6:Invoice Screen | Visual Force	Page Prototype
	$('#status-AP').toggle();
	$('#statusInput-AP').toggle();

	//Code added - Padmesh Soni (12/29/2014) - AC-6:Invoice Screen | Visual Force	Page Prototype
	if($('#publicCmtsAP').prop('disabled'))
		$('#publicCmtsAP').prop('disabled',	false);
	else
		$('#publicCmtsAP').prop('disabled',	true);
	if($('#privateCmtsAP').prop('disabled'))
		$('#privateCmtsAP').prop('disabled', false);
	else
		$('#privateCmtsAP').prop('disabled', true);

	editGridDetailAP();
};

function editGridDetailAP() {

	if($('#pricebookInput-AP').val()!='') {

		$('#myGridDetailAP').toggleClass('editMode');

		var o	= gridDetailAP.getOptions();
		if(o.editable){
			var	i =	gridDetailAP.getColumnIndex("amount");
			var	cols = gridDetailAP.getColumns();
			cols[i].width =	100;
			cols.pop();
			gridDetailAP.setColumns(cols);
			gridDetailAP.setOptions({editable:false,
			enableAddRow:false});
			$('#pricebookInput-AP').removeAttr("disabled");
		}else{
			var	i =	gridDetailAP.getColumnIndex("amount");
			var	cols = gridDetailAP.getColumns();
			cols[i].width =	70;
			cols.push({	id:	"delLinkAP", name: "", field: "delLinkAP", sortable: false ,width: 20,focusable:false, formatter:buttonAPFormatter});
			gridDetailAP.setColumns(cols);
			gridDetailAP.setOptions({editable:true,
			enableAddRow:true});
			gridDetailAP.setActiveCell(0,1);
			gridDetailAP.editActiveCell(Slick.Editors.SelectCellEditor);
			$('#pricebookInput-AP').attr("disabled","disabled");
		}
   }else{
		open($('#pricebookInput-AP'));
   }
}
function open(elem) {
	if (document.createEvent) {
		var e = document.createEvent("MouseEvents");
		e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		elem[0].dispatchEvent(e);
	} else if (element.fireEvent) {
		elem[0].fireEvent("onmousedown");
	}
}
function initializeInvoices(caseId){
	$(".hidden").hide();

	loadARPricebookOptions(caseId);
	loadAPPricebookOptions(caseId);
	$( "#dialog-save"	).dialog({
		resizable: false,
		height:140,
		width:600,
		modal: true,
		autoOpen:false,
		closeOnEscape:false,
		draggable:false,
		position:{ my: "center", at: "center", of: window }
	});
	$( "#dialog-error" ).dialog({
		resizable: false,
		height:140,
		width:600,
		modal: true,
		autoOpen:false,
		closeOnEscape:false,
		draggable:false,
		position:{ my: "center", at: "center", of: window }
	});

	$("#progress-save").progressbar({max:100});
	$("#AREditButton").bind('click',toggleAREditMode);
	$("#APEditButton").bind('click',toggleAPEditMode);
	loadStatusOptions();

	$('#pricebookInput-AP').change(function(){
		loadAPCategories($(this).val());
		loadAPProductOptions($(this).val(),orderConId,editGridDetailAP);
	});
	$('#pricebookInput-AR').change(function(){
		loadARCategories($(this).val());
		loadARProductOptions($(this).val());
	});
}

function refreshAR() {
	refreshARAction();
}

function refreshAP() {
	refreshAPAction();
}

function onCompleteRecv(){

	//Bind action on button at click operation
	$("#AREditButton").bind('click',toggleAREditMode);
	var option_str = "";
	arPriceBooks.forEach(function(v){
		option_str += makeOption(v);
	});
	$("#pricebookInput-AR").html(option_str);
	$("#sentInput-AR").datepicker();

	$('#myGridAR').toggleClass("disabled");

	$('#AREditButton').toggle();
	$('#ARPrintButton').toggle();
	$('#ARSaveButton').toggle();
	$('#ARCancelButton').toggle();

	grid.getColumns()[gridAP.getColumnIndex("priceBook")].options=arPriceBooks;
	grid.invalidateAllRows();
	grid.render();
	grid.setSelectedRows(arSelectedRow);
	$('#pricebookInput-AR').toggle();
	$('#sentInput-AR').toggle();
	$('#statusInput-AR').toggle();
}
function onCompletePay(){
	$("#APEditButton").bind('click',toggleAPEditMode);
	var option_str = "";
	apPriceBooks.forEach(function(v){
		option_str += makeOption(v);
	});
	$("#pricebookInput-AP").html(option_str);
	$("#sentInput-AP").datepicker();

	$('#APEditButton').toggle();
	$('#APPrintButton').toggle();
	$('#APSaveButton').toggle();
	$('#APCancelButton').toggle();

	gridAP.getColumns()[gridAP.getColumnIndex("priceBook")].options=apPriceBooks;
	gridAP.invalidateAllRows();
	gridAP.render();
	gridAP.setSelectedRows(apSelectedRow);

	$('#pricebookInput-AP').toggle();
	$('#sentInput-AP').toggle();
	$('#statusInput-AP').toggle();
}

//Now define your buttonFormatter function
function buttonARFormatter(row,cell,value,columnDef,dataContext){
	var	button = "<input class='delAR' value='x' style='color:red;'	type='button' id='"+ dataContext.id	+"'	/>";
	//the id is	so that	you	can	identify the row when the particular button	is clicked

	return button;
	//Now the row will display your	button
}

//Now define your buttonFormatter function
function buttonAPFormatter(row,cell,value,columnDef,dataContext){

	var	button = "<input class='delAP' value='x' style='color:red;'	type='button' id='"+ dataContext.id	+"'	/>";
	//the id is	so that	you	can	identify the row when the particular button	is clicked

	return button;
	//Now the row will display your	button
}

function validatePB(type){
	var pbVal	= (type	== 'AR')?$('#pricebookInput-AR').val():$('#pricebookInput-AP').val();

	if(pbVal ==	'')	{
		$( "#errorMsg" ).html(INVOICE_PB_REQUIRED);
		$( "#dialog-error" ).dialog("open");
	}
}

function cancelFromEdit(type) {

	var	thisGrid = (type == 'AR') ? grid : gridAP;

	if (type == 'AR') {

		toggleAREditMode();
		if (ARDataView.getLength() == 1) {
			$('#newInvoiceAR').prop( "disabled",false);
			var	d =	ARDataView.getItem(thisGrid.getSelectedRows()[0]);
			if (d != null && d != 'undefined' && d.id.substring(0,3) == 'new') {
				ARDataView.deleteItem(d.id);
				$("#ARCount").html(ARDataView.getLength());
				thisGrid.invalidate();
				thisGrid.render();
				$("#AREditButton").hide();
				$("#ARPrintButton").hide();
				$('#invoice-AR').text("");
				$('#to-AR').text("");
				$('#priceBook-AR').text("");
				$('#status-AR').text("");
				$('#subTotal-AR').text("");
				$('#discountTotal-AR').text("");
				$('#total-AR').text("");

				detailDataView.beginUpdate();
				detailDataView.getItems().length = 0;
				detailDataView.endUpdate();
				gridDetail.invalidateAllRows();
				gridDetail.render();
			} else {
				$("#AREditButton").show();
				$("#ARPrintButton").show();
			}
		} else if (selectedRowBFNewAR != '' && selectedRowBFNewAR != undefined) {

			var	d =	ARDataView.getItem(thisGrid.getSelectedRows()[0]);
			if(d !=	null &&	d != 'undefined'){
				ARDataView.deleteItem(d.id);
				$("#ARCount").html(ARDataView.getLength());
				thisGrid.invalidate();
				thisGrid.render();
				thisGrid.setSelectedRows(selectedRowBFNewAR);
				selectedRowBFNewAR = '';
			}
		} else {
			thisGrid.setSelectedRows(arSelectedRow);
		}

		if ($('#pricebookInput-AR').prop('disabled')) {
			$('#pricebookInput-AR').prop('disabled', false);
		}
	} else {

		toggleAPEditMode();

		if(selectedRowBFNewAP != ''	&& selectedRowBFNewAP != undefined)	{

			var	d =	APDataView.getItem(thisGrid.getSelectedRows()[0]);
			if(d !=	null &&	d != 'undefined'){
				APDataView.deleteItem(d.id);
				$("#APCount").html(APDataView.getLength());
				thisGrid.invalidate();
				thisGrid.render();
				thisGrid.setSelectedRows(selectedRowBFNewAP);
				selectedRowBFNewAP = '';
			}
		} else if (APDataView.getLength() == 1) {
			$('#newInvoiceAR').prop( "disabled",false);
			var	d =	APDataView.getItem(thisGrid.getSelectedRows()[0]);

			if (d != null && d != 'undefined' && d.id.substring(0,3) == 'new') {

				APDataView.deleteItem(d.id);

				APDataView.beginUpdate();
				APDataView.getItems().length = 0;
				APDataView.endUpdate();

				$("#APCount").html(APDataView.getLength());
				thisGrid.invalidateAllRows();
				thisGrid.render();
				$("#APEditButton").hide();
				$("#APPrintButton").hide();
				$('#invoice-AP').text("");
				$('#to-AP').text("");
				$('#status-AP').text("");
				$('#subTotal-AP').text("");
				$('#discountTotal-AP').text("");
				$('#total-AP').text("");

				detailAPDataView.beginUpdate();
				detailAPDataView.getItems().length = 0;
				detailAPDataView.endUpdate();
				gridDetailAP.invalidateAllRows();
				gridDetailAP.render();
			} else {
				$("#APEditButton").show();
				$("#APPrintButton").show();
			}
		} else {

			thisGrid.setSelectedRows(apSelectedRow);
		}
		if ($('#pricebookInput-AP').prop('disabled')) {
			$('#pricebookInput-AP').prop('disabled', false);
		}
	}
}

var	AutoCompleteEditor = function(args)	{
	var $input;
	var defaultValue;
	var scope = this;
	var calendarOpen =	false;


	this.init = function () {

		$input =	$("<INPUT id='tags'	class='editor-text'	/>");
		$input.width($(args.container).innerWidth() - 18);
		$input.appendTo(args.container);

		$input.bind("keydown.nav", function	(e)	{
			if (e.keyCode === $.ui.keyCode.LEFT	|| e.keyCode === $.ui.keyCode.RIGHT)
				e.stopImmediatePropagation();
			else if	(e.keyCode === $.ui.keyCode.UP || e.keyCode	===	$.ui.keyCode.DOWN)
				e.stopPropagation();
		});

        
        $( "<a>"	)
		.attr( "tabIndex", -1 )
		.attr( "title",	"Show All Items" )
		.appendTo( args.container )
		.button({
			icons: {
				primary: "ui-icon-triangle-1-s"
			},
			text: false
		})
		.removeClass( "ui-corner-all" )
		.addClass( "ui-corner-right	ui-combobox-toggle"	)

		.click(function() {

			// close if	already	visible
			if ( $input.autocomplete( "widget" ).is( ":visible"	) )	{
				$input.autocomplete( "close" );
				return;
			}

			// work	around a bug (likely same cause	as #5265)
			$( this	).blur();

			// pass	empty string as	value to search	for, displaying	all	results

			$input.autocomplete( "search", "" );

			//achieve the positioning in case of scrolling
			$(".ui-autocomplete").position({
				my:	"left top",
				at:	"left bottom",
				of:	$("#tags"),
				collision: "flip flip"
			});

			$input.focus().select();
		});

		$input.focus().select();

		$input.autocomplete({
			delay: 0,
			minLength: 0,
			source:	arg.column.options
		});
	};


	this.destroy =	function ()	{
		$input.autocomplete("destroy");
	};

	this.focus	= function () {
		$input.focus();
	};

	this.position = function (position) {

		$(".ui-autocomplete").position({
			my:	"left top",
			at:	"left bottom",
			of:	$("#tags"),
			collision: "flip flip"
		});
	};

	this.loadValue	= function (item) {
		defaultValue	= item[args.column.field];
		$input.val(defaultValue);
		$input[0].defaultValue = defaultValue;
		$input.select();
	};

	this.serializeValue = function	() {
		return $input.val();
	};

	this.applyValue = function	(item, state) {

		var	matcher	= new RegExp( "^" +	$.ui.autocomplete.escapeRegex( state ) + "$", "i" ),
		valid =	false;

		jQuery.each(args.column.options	, function(index, value){

			if ( value.match( matcher )	) {
				valid =	true;
				item[args.column.field]	= state;
				return false;
			}
		});
	};


	this.isValueChanged = function	() {
		return (!($input.val() == ""	&& defaultValue	== null)) && ($input.val() != defaultValue);
	};


	this.validate = function () {
		return {
			valid:	true,
			msg: null
		};
	};

	this.init();
}