//2016-11-09 - Tyler H. - calloutHelper functions similar to a class.  Look up "Anonymous Closures" or JS module design pattern
var calloutHelper = (function () {
        var S3Complete=false;
        var imageLinksComplete=false;
        var S3payload;
        var imageLinksPayload;
        var cback;  //callback function
        var WOnum;
        var refNum;

        var pub = {};// public object - returned at end of module

		function fetchS3(refNum,callback){
		  if (refNum !== null && refNum !== undefined && refNum !== '') {
			$.get("https://restmysql.northsight.io?action=getalls3itemsforworkorder&referenceNumber="+refNum).done(
              function(data){fetchComplete('S3',data)}
          	);
		  }
        }
        
        function fetchImageLinks(workOrderNumber){
          $.get("https://restmysql.northsight.io?action=getImageLinks&workOrderNumber="+workOrderNumber).done(
            function(data){fetchComplete('imageLinks',data)}
          );
        }
        
        function fetchComplete(whoCompleted,payload){
			switch(whoCompleted){
	          	case 'S3':
					S3payload=payload;
	            	S3Complete=true;
					break;
	            case 'imageLinks':
					imageLinksPayload=payload;
					imageLinksComplete=true;
					break;
			}
			
			if (S3Complete && imageLinksComplete) cback(imageLinksPayload,S3payload,WOnum,refNum);
        }
        
        pub.fetchData=function(workOrderNumber,referenceNumber,callback){
          console.log('fetchData');
          cback=callback;
          WOnum=workOrderNumber;
          refNum=referenceNumber;
          fetchS3(refNum,callback);
          fetchImageLinks(workOrderNumber,callback);
        };


        return pub; // expose externally
}());


function main(wonum,refnum){
	var internalUser = ('' != 'true');
	render(internalUser);
	calloutHelper.fetchData(wonum,refnum ,allDataLoadCalloutsComplete);
}


function render(internalUser){
	if(internalUser){
		$("#pdfSection").attr("style", "visibility: visible");

	}
}


function allDataLoadCalloutsComplete(ILpayload, S3payload,workOrderNumber,referenceNumber){
	var S3Keys, S3KeyValues, imageLinkKeys, imageLinkKeyValues;
	var S3OnlyKeys;  //what are the files which are found only in S3 and not both S3 AND database?
	var PDFkeys;  //keys to pdf files which are uploaded from Pruvan
	var imageLinksImageOnlyKeys;  //no PDFs

	tmp=extractNormalizedImageLinkKeys(ILpayload);
	imageLinkKeys = tmp.keys;
	imageLinkKeyValues = tmp.keyValues;

	tmp=extractNormalizedS3Keys(S3payload);
	S3Keys = tmp.keys;
	S3KeyValues = tmp.keyValues;

	S3OnlyKeys=setDiff(S3Keys,imageLinkKeys);
	PDFkeys=findAllPDFkeys(imageLinkKeys);
	imageLinksImageOnlyKeys=setDiff(imageLinkKeys,PDFkeys);

	displayPruvanPhotos(imageLinksImageOnlyKeys,imageLinkKeyValues);
	displayS3Photos(S3OnlyKeys, S3KeyValues, referenceNumber);
	displayPDFs(PDFkeys,imageLinkKeyValues);
	return;
}

function displayPDFs(keys, keyValues){
	var hrefs="<h2><ul>";
    for(i = 0; i < keys.length; i++) {
        //console.log(imageLinksJSON[i]["IMAGEURL__C"]);
        //<a href='" +imageLinksArr[i]["IMAGEURL__C"]+"'' class='swipebox'>\
        var keyValue=keyValues[keys[i]];
        url=replaceHTTPwithHTTPS(keyValue.IMAGEURL__C);

        hrefs+="<li><a href=\""+keyValue.IMAGEURL__C + "\">PDF</a></li>";
    }
    hrefs+="</ul></h2>";
    $( "#pdfs" ).html(hrefs);

}

function findAllPDFkeys(keys){
	var ret=[];
	for (var i = 0, len = keys.length; i < len; i++){
		var arr = keys[i].split('.');
		if (arr[arr.length-1]=='pdf') ret.push(keys[i]);
	}
	return ret;
}

function normalizeImageLinkKey(imageLink){
	//INPUT:  imageLink='https://allyearimages.s3.amazonaws.com/182202040/621520715.jpg'
	//OUTPUT: '621520715.jpg'
	var arr = imageLink.split('/');
	return arr[arr.length-1];
}

function normalizeS3Key(S3key){
	//INPUT: S3key = "01010110\/38006965.jpg"
	//OUTPUT: "38006965.jpg"
	var arr = S3key.split('/');
	return arr[arr.length-1];	
}


function setDiff(A,B){
	return A.filter(function(x) { return B.indexOf(x) < 0 })
}

function extractNormalizedS3Keys(S3payload){
	var keys = [];
	var keyValues = {};
	var s3objs=JSON.parse(S3payload);
	for (var i = 0, len = s3objs.length; i < len; i++){
		var key = normalizeS3Key(s3objs[i].Key);
		keys.push(key);
		keyValues[key]=s3objs[i];
	}

	return {keys: keys, keyValues: keyValues};
}

function extractNormalizedImageLinkKeys(imageLinksPayload){
	var keys = [];
	var keyValues = {};
	var ILobjs=JSON.parse(imageLinksPayload);
	for (var i = 0, len = ILobjs.length; i < len; i++){
		var key = normalizeImageLinkKey(ILobjs[i].IMAGEURL__C);
		keys.push(key);
		keyValues[key]=ILobjs[i];
	}

	return {keys: keys, keyValues: keyValues};

}


function replaceHTTPwithHTTPS(url){
	return url.replace(/^http:\/\//i, 'https://');
}

function displayPruvanPhotos(keys, keyValues){
	var figures="";
    for(i = 0; i < keys.length; i++) {
        //console.log(imageLinksJSON[i]["IMAGEURL__C"]);
        //<a href='" +imageLinksArr[i]["IMAGEURL__C"]+"'' class='swipebox'>\
        var keyValue=keyValues[keys[i]];
        var url=replaceHTTPwithHTTPS(keyValue.IMAGEURL__C);
        var timestamp = keyValue.GPS_TIMESTAMP__C=="1970-01-01 00:00:00" || keyValue.GPS_TIMESTAMP__C==null ? keyValue.CREATEDDATE : keyValue.GPS_TIMESTAMP__C;
        timestamp=timestamp.substr(0,16);

        figures+="\
        	<figure>\
        		<a class='gallery' title='"+ timestamp + "' href='" + url  + "'>\
            	<img class='exif' height='150' alt='" +keyValue.PRUVAN_EVIDENCETYPE__C + "' src='"+ url +"'/>\
            	</a>\
            	<figcaption>" + (keyValue.PRUVAN_EVIDENCETYPE__C==null ? keyValue.SOURCE__C : keyValue.PRUVAN_EVIDENCETYPE__C) + "</figcaption>\
            	<figcaption>" + timestamp + "</figcaption>\
            </figure>";
    }
    $( "#pruvanPhotos" ).html(figures);
    $(".exif").contextmenu(function(){rightClick(this);return false;});
    $("a.gallery").colorbox({ opacity:0.5 , rel:'group1' });
    

	// $("#photos").justifiedGallery(
	// 	{
	// 		cssAnimation: false,
	// 		rowHeight : 150,
	// 		captionSettings: {
	// 			animationDuration: 0,
	// 			visibleOpacity: 1,
	// 		  	nonVisibleOpacity: 1
	// 		}
	// 	}
	// );
}

function formatS3Date(dtString){
	//2016-10-13T00:11:11+00:00
	ret=dtString.substr(0,16);
	ret=ret.replace('T',' ');
	return ret;
}

function displayS3Photos(keys, keyValues,refNum){
	var figures="";
    for(i = 0; i < keys.length; i++) {
        //console.log(imageLinksJSON[i]["IMAGEURL__C"]);
        //<a href='" +imageLinksArr[i]["IMAGEURL__C"]+"'' class='swipebox'>\
        var keyValue=keyValues[keys[i]];
        url="https://allyearimages.s3.amazonaws.com/" + refNum + "/" + keys[i];
        figures+="\
        	<figure>\
        		<a class='gallery' title='"+ '' + "' href='" + url  + "'>\
            	<img class='exif' height='150' alt='" + '?' + "' src='"+ url +"'/>\
            	</a>\
            	<figcaption>" + "---" + "</figcaption>\
            	<figcaption>" + formatS3Date(keyValue.LastModified) + "</figcaption>\
            </figure>";

         


    }
    $( "#uploadPhotos" ).html(figures);
    $(".exif").contextmenu(function(){rightClick(this);return false;});
    $("a.gallery").colorbox({ opacity:0.5 , rel:'group2' });
}

function rightClick(img){
	//disabling this functionality for now since clients now have access to this page and not sure if we want to expose capability
	//of viewing exif data
	// EXIF.getData(img,
	// 	function() {
	//     	alert(EXIF.pretty(this));	    	
	// 	}
	// );
}

function sliderChange(event, ui){
    //$("#photos").justifiedGallery({rowHeight : ui.value});
    $("#photos").height=ui.value;

}

