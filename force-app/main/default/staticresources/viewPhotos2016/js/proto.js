


var calloutHelper = (function () {
        var S3Complete=false;
        var imageLinksComplete=false;
        var S3payload;
        var imageLinksPayload;
        var cback;

        var pub = {};// public object - returned at end of module

				function fetchS3(workOrderNumber,callback){
          $.get("https://restmysql.northsight.io?action=getalls3itemsforworkorder&workOrderNumber="+workOrderNumber).done(
              function(data){fetchComplete('S3',data)}
          );
        }
        
        function fetchImageLinks(workOrderNumber){
          $.get("https://restmysql.northsight.io?action=getImageLinks&workOrderNumber="+workOrderNumber).done(
            function(data){fetchComplete('imageLinks',data)}
          );
        }
        
        function fetchComplete(whoCompleted,payload){
        	console.log('fetchComplete');
          switch(whoCompleted){
          	case 'S3':
              S3payload=payload;
            	S3Complete=true;
              break;
            case 'imageLinks':
              imageLinksPayload=payload;
            	imageLinksComplete=true;
              break;
          }
          if (S3Complete && imageLinksComplete) cback(imageLinksPayload,S3payload);
        }
        
        pub.fetchData=function(workOrderNumber,callback){
          console.log('fetchData');
          cback=callback;
          fetchS3(workOrderNumber,callback);
          fetchImageLinks(workOrderNumber,callback);
        };


        return pub; // expose externally
}());

function calloutDone(ILpayload, S3payload){
	console.log('ILpayload=' + ILpayload);
  console.log('S3payload=' +  S3payload);
}


