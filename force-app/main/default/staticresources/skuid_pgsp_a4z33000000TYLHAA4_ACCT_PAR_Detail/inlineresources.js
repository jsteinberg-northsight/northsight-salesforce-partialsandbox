(function(skuid){
skuid.snippet.register('HighlightRow',function(args) {var field = arguments[0], 

value = skuid.utils.decodeHTML(arguments[1]); 

skuid.ui.fieldRenderers[field.metadata.displaytype][field.mode](field,value); 

if (field.mode == 'read' && value === true) 

    { 

        field.item.element.addClass("HighlightRow");


    }
});
skuid.snippet.register('newWindow',function(args) {window.open('https://www.google.com', '_blank', 'width=' + screen.width + ',height=' + screen.height)
});
}(window.skuid));