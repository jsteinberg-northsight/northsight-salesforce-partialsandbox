(function(skuid){
(function(skuid){
	var $ = skuid.$;
	$(document.body).one('pageload',function(){
		var myModel = skuid.model.getModel('MyModelId');
		var myComponent = skuid.component.getById('MyComponentUniqueId');
		$(function(){
        $(".line1").typed({
            strings: [" Hello and thank you for applying!<br> You're only a few steps away from completing your application! <br><br> 1) Review your contact information <br> 2) Provide insurance information <br> 3) Sign required documents <br><br> Click the 'Review Contact Info' button on the top right to get started!"],
            typeSpeed: 0,
            backDelay: 300,
             contentType: 'html',
            loop: false,
              smartBackspace: true 
        });
      
    });
	});
})(skuid);;
(function(skuid){
	var $ = skuid.$;
	$(document.body).one('pageload',function(){
		var myModel = skuid.model.getModel('MyModelId');
		var myComponent = skuid.component.getById('MyComponentUniqueId');
		$(function(){
        $(".line2").typed({
            strings: ["Thank you for applying.<br>Please proceed to the next step to confirm that the information we captured is accurate.<br>"],
            typeSpeed: 0,
            backDelay: 300,
             contentType: 'html',
            loop: false,
              smartBackspace: true 
        });
      
    });
	});
})(skuid);;
skuid.snippet.register('hidetablefooter',function(args) {var params = arguments[0],
	$ = skuid.$;
});
(function(skuid){
	var $ = skuid.$;
	$(document.body).one('pageload',function(){
		var myModel = skuid.model.getModel('MyModelId');
		var myComponent = skuid.component.getById('MyComponentUniqueId');
		var vendorapp = skuid.model.getModel('Vendor_Application_Form');
        var laststep = vendorapp.data[0].Wizard_Step__c;

		skuid.$C('MyWizard').navigate(laststep); 
		
	});

})(skuid);;
(function(skuid){
	var $ = skuid.$;
	$(document.body).one('pageload',function(){
  var Site_visit = skuid.model.getModel('Site_visit');
   Site_visit.updateRow(Site_visit.createRow(), {
	                        'Browser__c' : navigator.userAgent,
	                         'Screen_Height__c' : window.screen.availHeight,
	                          'Screen_Width__c' : window.screen.availWidth
	                        }
	                    );
	                    Site_visit.save();
  
	console.log(window.screen.availHeight)
	});
})(skuid);;
skuid.snippet.register('Wait2point5Seconds',function(args) {var params = arguments[0],
	$ = skuid.$;
var SECONDS_TO_DELAY = 2.5;
var dfd = skuid.$.Deferred();
setTimeout(function(){
    dfd.resolve();
},SECONDS_TO_DELAY*1000);
return dfd.promise();
});
}(window.skuid));