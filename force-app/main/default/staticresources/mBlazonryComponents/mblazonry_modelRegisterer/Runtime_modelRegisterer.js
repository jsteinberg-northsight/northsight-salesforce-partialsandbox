/**
 * mblazonrycomponents - mBlazonry's custom components!
 * @version v0.2.0-beta
 * @license BSD-3-Clause
 * @author Andréas Kaytar-Lefrançois <andreas@mblazonry.com>
 */
"use strict";!function(e,t,r,i){var o=function(r,o,a){var n,d=o.attr("models").split(",");if("false"!=o.attr("id-toggle")){var l=e(".nx-pagetitle").first().attr("id");n=t.component.getById(l)}else n=t.component.getById(o.attr("title-id"));if(n){var s=!0,f=!1,g=i;try{for(var m,y=d[Symbol.iterator]();!(s=(m=y.next()).done);s=!0){var c=m.value;n.editor.registerModel(t.$M(c))}}catch(p){f=!0,g=p}finally{try{!s&&y["return"]&&y["return"]()}finally{if(f)throw g}}}else a.addProblem("No target page title provided for model registerer.")};t.componentType.register("mblazonry__model_registerer",o)}(window.skuid.$,window.skuid,window);