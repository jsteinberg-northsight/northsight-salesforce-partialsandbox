/**
 * mblazonrycomponents - mBlazonry's custom components!
 * @version v0.2.0-beta
 * @license BSD-3-Clause
 * @author Andréas Kaytar-Lefrançois <andreas@mblazonry.com>
 */
"use strict";!function(e,r,t,n){var a=function(e,t,a){function o(){var e=r.$M(i);Object.keys(e.changes).length<=0&&e.updateData()}var i,l=t.attr("models").split(","),d=t.attr("interval");if(""!==d){var s=!0,f=!1,v=n;try{for(var u,c=l[Symbol.iterator]();!(s=(u=c.next()).done);s=!0){var y=u.value;i=y,setInterval(o,1e3*d)}}catch(h){f=!0,v=h}finally{try{!s&&c["return"]&&c["return"]()}finally{if(f)throw v}}}else a.addProblem("No refresh interval provided to model refresher.")};r.componentType.register("mblazonry__model_refresher",a)}(window.skuid.$,window.skuid,window);