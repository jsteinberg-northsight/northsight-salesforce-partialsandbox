/**
 * mblazonrycomponents - mBlazonry's custom components!
 * @version v0.2.0-beta
 * @license BSD-3-Clause
 * @author Andréas Kaytar-Lefrançois <andreas@mblazonry.com>
 */
"use strict";!function(e,r,t){function i(e){var r=[],i=!0,n=!1,l=t;try{for(var o,s=e.getElementsByTagName("model")[Symbol.iterator]();!(i=(o=s.next()).done);i=!0){var a=o.value;r.push({label:a.getAttribute("id"),value:a.getAttribute("id")})}}catch(d){n=!0,l=d}finally{try{!i&&s["return"]&&s["return"]()}finally{if(n)throw l}}return r}var n=r,l=n.builder,o=(l.core,n.utils);o.makeXMLDoc,e.noConflict(),window.console;l.registerBuilder(new l.Builder({id:"mblazonry__model_refresher",name:"Model Refresher",icon:"fa-repeat",description:"TODO",propertiesRenderer:function(e,t){e.setTitle("Model Refresher Properties");var n=t.state,l=[],o=[{id:"models",type:"multipicklist",label:"Models to refresh:",picklistEntries:i(r.builder.core.getPageModels()[0])},{id:"interval",type:"number",label:"Interval (in seconds):"}];l.push({name:"Basic",props:o}),e.applyPropsWithCategories(l,n)},componentRenderer:function(e){e.setTitle(e.builder.name)}}))}(window.skuid.$,window.skuid);