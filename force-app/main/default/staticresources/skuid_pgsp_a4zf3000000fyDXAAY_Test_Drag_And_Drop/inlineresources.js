(function(skuid){
skuid.snippet.register('Queue.ItemRenderer',function(args) {item = args.item,
                    list = args.list,
                    model = args.model,
                    element = args.element,
                    row = item.row,
                    renderTemplate = '{{{Name}}} ({{CloseDate}})',
                    mergeSettings = {
                        createFields: true,
                        registerFields: false
                    },
                    $ = skuid.$;
                    
                element.html(
                    skuid.utils.merge('row',renderTemplate,mergeSettings,model,row)
                ).draggable({
                    revert: 'invalid',
                    appendTo : 'body',
                	helper : function() {
                        var original = $(this);
                        var helper = original.clone();
                        // Pass along a reference to the current list's contents
                        // so that we can compare lists
                        // to ensure we're not allowing dropping on the current list
                        original.data('listContents',list.contents);
                		
                		helper.css({
                			'z-index' : 1000,
                			'width' : $(this).css('width'),
                			'height' : $(this).css('height'),
                			'border' : '1px black solid',
                			'padding' : '4px',
                			'background-color' : '#EEE',
                			'font-family': 'Arial,Helvetica,sans-serif',
                			'cursor':'move'
                		});
                		
                		return helper; 
                	}
                });
});
skuid.snippet.register('Make Queue Contents Droppable',function(args) {(function(skuid){
                       
                       // Global setting -- if true, then all changes will be immediately saved,
                       // otherwise, changes will remain unsaved until you click Save.
                       var SAVE_IMMEDIATELY = true;
                        
                       var $ = skuid.$;
                       
                        var getQueueList = function(queueElement){
                            var queueList;
                            $.each(skuid.model.list(),function(i,model){
                               $.each(model.registeredEditors,function(){
                                  if (this.element && this.element.is && this.element.is(queueElement)) {
                                      queueList = this.lists[0];
                                      return false;
                                  } 
                               });
                               if (queueList) return false;
                            });
                            return queueList;
                        };
                       
                       $(document.body).one('pageload',function(){
                          $('.nx-queue').each(function(){
                             var queue = $(this);
                             var listContents = queue.find('.nx-list-contents');
                             listContents.droppable({
                                hoverClass: 'ui-state-highlight',
                                accept: function(draggables) {
                                    // Do not accept draggables
                                    // that came from this list
                                    return (!listContents.is($(draggables[0]).data('listContents')));
                                },
                                drop: function(e,ui){
                                    var draggable = ui.draggable;
                                    
                                    var sourceItem = draggable.parent().data('object');
                                    
                                    // You will get a jQUery UI bug unless you detach the draggable.
                                    // We wait until now to detach in order to get a 
                                    draggable.detach();
                                    
                                    var sourceRow = sourceItem.row;
                                    var sourceRowId = sourceRow.Id;
                                    var sourceList = sourceItem.list;
                                    var sourceModel = sourceItem.list.model;
                                    
                                    var targetList = getQueueList(queue);
                                    var targetModel = targetList.model;
                                    
                                    targetModel.adoptRow(sourceRow);
                                    sourceModel.removeRowById(sourceRowId);
                                    
                                    var targetRow = targetModel.getRowById(sourceRowId);
                                    
                    		// Find the first Condition in our target Model,
                                    // and apply it to our target row.
                                    // (that is, change the Stage of the dragged Opportunity)
                                    var targetModelCondition = targetModel.conditions[0];
                                    targetModel.updateRow(
                                        targetRow,
                                        targetModelCondition.field,
                                        targetModelCondition.value
                                    );
                                    
                                    // Re-render just the Source List and the Target List
                                    sourceList.render();
                                    targetList.render();
                                    
                                }
                             });
                          });
                       });
                       
                    })(skuid);
});
}(window.skuid));