//var spinnerVisible = false;

//jQuery = jQuery.noConflict();

jQuery(document).ready(function(){
	console.log('ready function called');
	
	/*************************************************************************************************/
	//Quick Table Search
	jQuery('#nameSearch').keyup(function() {
		var regex = new RegExp(jQuery('#nameSearch').val(), "i");
		var rows = jQuery('[id$=work_orders_data_table] tr:gt(0)');

		console.log('regex: ' + regex);

		rows.each(function (index) {
			caseLink = jQuery(this).find('[id$=contactLink]').html();
			console.log('the rows: ' + caseLink);

			if (caseLink.search(regex) != -1) {
				jQuery(this).show();
			} else {
				jQuery(this).hide();
			}
		});
	});
	/*************************************************************************************************/
	/*************************************************************************************************/
	//modal window jquery for client commitments section
	jQuery( "#dialog_create_commitment" ).dialog({
		autoOpen: false,
		height: 480,
		width: 640,
		modal: true,
		buttons: {
			"Submit Client Commitment": function() {
				var errors = false;
				if( jQuery("#commitment_date").val() == '') {
				jQuery("#commitment_date_tr td").addClass('ui-state-error');
				errors = true;
				}else{
				jQuery("#commitment_date_tr td").removeClass('ui-state-error');
				}
				if(jQuery("[id$=commitment_order_status]").val() == '') {
				jQuery("#commitment_order_status_tr td").addClass('ui-state-error');
				errors = true;
				} else{
				jQuery("#commitment_order_status_tr td").removeClass('ui-state-error');
				}
				if(jQuery("[id$=commitment_delay_cause]").val() == '') {
				jQuery("#commitment_delay_cause_tr td").addClass('ui-state-error');
				errors = true;
				}else{
				jQuery("#commitment_delay_cause_tr td").removeClass('ui-state-error');
				}
				if(jQuery("[id$=commitment_public_comments]").val() == '') {
				jQuery("#commitment_public_comments_tr td").addClass('ui-state-error');
				errors = true;
				}else{
				jQuery("#commitment_public_comments_tr td").removeClass('ui-state-error');
				}
				/*if(jQuery("[id$=commitment_internal_comments]").val() == '') {
				jQuery("#commitment_internal_comments_tr td").addClass('ui-state-error');
				errors = true;
				}else{
				jQuery("#commitment_internal_comments_tr td").removeClass('ui-state-error');
				}*/
				jQuery('#status_msg').hide();
				if(errors) {
				jQuery('#status_msg').html('<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>You must complete the required fields').show();

				return;
				}
				//jQuery( this ).dialog( "close" );
				console.log('sup');
				//showProgress();

				save_status(
				jQuery("#commitment_date").val(),
				jQuery("[id$=commitment_order_status]").val(),
				jQuery("[id$=commitment_delay_cause]").val(),
				jQuery("#commitment_public_comments").val(),
				jQuery("#commitment_internal_comments").val()
				);  
			},
			Cancel: function() {
				jQuery( this ).dialog( "close" );
			}
		},
		close: function() {
			jQuery(".statusEdit").val('');
		},
		open: function() {
			jQuery(".statusEdit").val('');
		}
	});
	/*************************************************************************************************/
	/*************************************************************************************************/
	//date picker functionality section
	tday = new Date();

	jQuery('#commitment_date').datepicker({ dateFormat: 'mm/dd/yy', minDate:tday, numberOfMonths:1 });
			  
	//change these
	jQuery('[id$=due_date_begin]').datepicker({ dateFormat: 'mm/dd/yy', numberOfMonths:1 });
	jQuery('[id$=due_date_end]').datepicker({ dateFormat: 'mm/dd/yy', numberOfMonths:1 });

	jQuery('[id$=commit_by_date_begin]').datepicker({ dateFormat: 'mm/dd/yy', numberOfMonths:1 });
	jQuery('[id$=commit_by_date_end]').datepicker({ dateFormat: 'mm/dd/yy', numberOfMonths:1 });
	/*************************************************************************************************/
});//end ready function
/*************************************************************************************************/
/*************************************************************************************************/
//checks that one or more work orders are selected and then calls the client commitment dialog functionality section
function open_edit_status_dlg() {          
	var anyChecked = false;

	// Verify that something is selected
	jQuery('.chk').each(function() { 
		if( jQuery(this).is(':checked') ){
			anyChecked = true;
			return;
		}
	});

	if(anyChecked){
		jQuery('#dialog_create_commitment').dialog('open');
	} else {
		alert('At least one Work Order must be selected');
	}
}
/*************************************************************************************************/
/*************************************************************************************************/
function statusComplete(){
	var error = "{!status_error}";
	var fields = "{!status_error_fields}";
	
	for(var f in fields){
		jQuery("."+fields[f]+" td").addClass('ui-state-error');
	}
	
	if(error==""){
		jQuery( "#dialog_create_commitment" ).dialog( "close" );
	}
	else{
		jQuery('#status_msg').html('<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>'+error).show();
	};
	
	reCheckWOs();
	hideProgress();
}
/*************************************************************************************************/
/*************************************************************************************************/
//format the current date
function getFormattedCurDate(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	
	if(dd<10){
		dd='0'+dd
	} 
	
	if(mm<10){
		mm='0'+mm
	} 
	
	today = mm+'/'+dd+'/'+yyyy;
	return today;
}
/*************************************************************************************************/
/*************************************************************************************************/
//show the spinner
function showProgress() {
	jQuery("div#spinner").fadeIn("fast");
}
/*************************************************************************************************/
/*************************************************************************************************/
//hide the spinner
function hideProgress() {
	jQuery("div#spinner").stop();
	jQuery("div#spinner").fadeOut("fast");
}
/*************************************************************************************************/