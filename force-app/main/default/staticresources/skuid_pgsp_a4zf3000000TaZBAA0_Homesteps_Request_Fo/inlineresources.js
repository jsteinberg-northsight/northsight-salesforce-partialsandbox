(function(skuid){
skuid.snippet.register('RequestorPhone',function(args) {var params = arguments[0],
	$ = skuid.$;
var ContactNumber = arguments[0],    value = arguments[1];

if(value !== null){
     // strip non-numeric characters...
     value = value.replace(/\D/g,'');
     if(value.length == 10) {
         // use substring to get at the values...
         var stringValue = value.toString();
         stringValue = '(' + value.substring(0,3) + ')' + ' ' + value.substring(3,6) + '-' + value.substring(6,10);
         console.log(value);
         console.log(stringValue);
     } 
}
skuid.ui.fieldRenderers[ContactNumber.metadata.displaytype][ContactNumber.mode](ContactNumber,stringValue);
});
}(window.skuid));