(function(skuid){
skuid.componentType.register('Sign',function(elem) {var element = arguments[0],
   $ = skuid.$;
$(".nx-page").one("pageload", function() {
    // init signature canvas
    element.jSignature();
    element.jSignature("reset");
    // Create new signature in text field
    var model = skuid.model.getModel('Vendor_Application'),
        row = model.getFirstRow(),
        sigField = 'Signature__c',
        sigData = model.getFieldValue(row,sigField);
    if(sigData !== null ) {     // read back Signature data
        element.jSignature("setData", "data:" + sigData);
    }
})
});
skuid.snippet.register('saveSignature',function(args) {var params = arguments[0],
   $ = skuid.$;
var model = skuid.model.getModel('Vendor_Application'),
    row = model.getFirstRow(),
    sigField = 'Signature__c';
// get signature from id
var sig = $("#idSign");
var sigData = sig.jSignature("getData", "base30");
// update signature text field SF and save
model.updateRow(row,sigField,sigData[0] + ',' + sigData[1]);
model.save();
});
skuid.snippet.register('clearSignature',function(args) {var params = arguments[0],

   $ = skuid.$;
// get signature canvas and reset
var sig = $("#idSign");
sig.jSignature("reset");
// get signature field, set to null and save model
var model = skuid.model.getModel("Vendor_Application");
var row = model.getFirstRow();
var sigField = "Signature__c";
model.updateRow(row,sigField,null);
});
(function(skuid){
	var $ = skuid.$;
	$(document.body).one('pageload',function(){
  var Site_visit = skuid.model.getModel('Site_visit');
   Site_visit.updateRow(Site_visit.createRow(), {
	                        'Browser__c' : navigator.userAgent
	                        }
	                    );
	                    Site_visit.save();
    

	});
})(skuid);;
}(window.skuid));