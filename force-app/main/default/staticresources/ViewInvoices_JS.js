var newRowId = 0;
var grid;
var ARInvoiceData = [];
var APInvoiceData = [];
var ARDataView = new Slick.Data.DataView();
var gridAP
var APDataView = new Slick.Data.DataView();
var gridDetail;
var detailDataView = new Slick.Data.DataView();
var gridDetailAP;
var detailAPDataView = new Slick.Data.DataView();

var priceBooks = [];
var invoiceStatuses = []

var arPriceBooks =  [];
var apPriceBooks = [];
var arPBCategories = [];
var apPBCategories = [];
var arPBProducts = [];
var apVendProducts = [];
var apPBProducts = [];
var arInvoiceStatus =  [];
var apInvoiceStatus = [];        
var apSelectedRow;
var arSelectedRow;
var APLineItemsToDelete = [];
var ARLineItemsToDelete = [];
var selectedRowBFNewAR;
var selectedRowBFNewAP;


function makeOption(val)
{
	if(val.value!=undefined&&val.value.id!=undefined){
		return "<OPTION value='"+val.value.id+"'>"+val.label+"</OPTION>";
	}
	return "<OPTION value='"+((val.value!=undefined)?val.value:"")+"'>"+val.label+"</OPTION>";
}
function requiredFieldValidator(value) {
  if (value == null || value == undefined || !value.length) {
    return {valid: false, msg: "This is a required field"};
  }
  else {
    return {valid: true, msg: null};
  }
}
function loadReceivableInvoice() {
	
	ARDataView.onRowsChanged.subscribe(function (e, args) {
        $("#ARCount").html(ARDataView.getLength());
        grid.invalidateRows(args.rows);
        grid.render();
    });
        
	grid = new Slick.Grid("#myGridAR", ARDataView, columns, invoiceGridOptions);
    grid.setSelectionModel(new Slick.RowSelectionModel());
    grid.onSort.subscribe(function (e, args) {
        var cols = args.sortCols;
        ARDataView.sort(function (dataRow1, dataRow2) {
            for (var i = 0, l = cols.length; i < l; i++) {
              var field = cols[i].sortCol.field;
              var sign = cols[i].sortAsc ? 1 : -1;
              var value1 = dataRow1[field], value2 = dataRow2[field];
              if(value1 === undefined || value1 === null) { value1 = '';}
              if(value2 === undefined || value2 === null) { value2 = '';}
              var result = (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
              if (result != 0) {
                return result;
              }
            }
        return 0;
        });
        grid.invalidate();
        grid.render();
    });
    
    //CLICK AR
    grid.onClick.subscribe(function(e, args) {
        grid.setSelectedRows([args.row]);
    });
    
    grid.onSelectedRowsChanged.subscribe(function(e, args) {
        
        var item = ARDataView.getItem(grid.getSelectedRows()[0]);
        
        var lines;
        var formatCreated = getFormattedDate(item.created);
        var formatSentDate = getFormattedDate(item.sent);
        $('#invoice-AR').text(item.invoiceNumber);
        $('#to-AR').text(item.client);
        $('#priceBook-AR').text(getOptionLabel(arPriceBooks,item.priceBook));
        loadARCategories(item.priceBook);
        loadARProductOptions(item.priceBook);
        $('#created-AR').text(formatCreated);
        $('#sent-AR').text(formatSentDate);
        
        //Code added - Padmesh Soni (12/30/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
        $('#status-AR').text(getOptionLabel(invoiceStatuses ,item.status));
        
        $('#publicCmtsAR').val(item.publicNotes);
        $('#privateCmtsAR').val(item.privateNotes);
        dataDetail = [];
        if(item.id!=null&&item.id.substring(0,3)!='new'){
            InvoicingController.loadLines(
                item.id,
                function(results, event) {
                    if(event.type === 'exception'){
                        console.log("exception");
                        console.log(event);
                    }else if(event.status){
                        $.each(results, function(i, result){
                        	
                        	dataDetail[i] = {
                                lineNumber: i+1,
                                id:result.id,
                                category: result.productCategory,
                                product: result.productId,
                                description: result.description,
                                uom: result.uom,
                                qty: result.qty,
                                rate: result.unitPrice,
                                discount: result.discount,
                                amount: '',
                                originalRate: result.unitPrice,
                                recordType:result.recordType
                              };
                        });

                    }else{
                        console.log(event.message);
                    }
                    detailDataView.beginUpdate();
                    detailDataView.setItems(dataDetail);
                    detailDataView.endUpdate();
                    gridDetail.invalidateAllRows();
                    gridDetail.render();
                    calculateData("AR");
                },{escape: false}
            );
        }else{
            detailDataView.beginUpdate();
            detailDataView.setItems(dataDetail);
            detailDataView.endUpdate();
            gridDetail.invalidateAllRows();
            gridDetail.render();
            calculateData("AR");
        }
    });
    
    //END CLICK
    grid.init();
    
    ARDataView.beginUpdate();
    ARDataView.setItems(ARInvoiceData);
    ARDataView.endUpdate();
    
    loadReceivableInvoiceLines();
}
function loadReceivableInvoiceLines() {
	
    //Detail View
    var dataDetail = [];
    
    gridDetail = new Slick.Grid("#myGridDetail", detailDataView, columnsDetail, invoiceLineGridOptions);
    gridDetail.onCellChange.subscribe(function(e,args){
    	var columnId  = args.grid.getColumns()[args.cell].id;
    	if(columnId == "category"){
            args.item.product = null;
            args.item.rate = null;
            args.item.uom = null;
            args.item.discount = null;
            args.item.qty = null;
        } 
        if(columnId == "product"){
            var found = $.grep(args.grid.getColumns()[args.cell].options,function(e){
              var value = args.item[args.grid.getColumns()[args.cell].field];
              console.log("value: "+ value);
              if(e.value!=undefined && e.value.id!=undefined){
                return e.value.id == value;
              }
              return e.value == value; 
              }
            );
            if(found.length > 0 && found[0].value != undefined){
                args.item.rate = found[0].value.rate;
                args.item.uom  = found[0].value.uom;
                if(args.item.qty == null){
                	args.item.qty = 1;
                }
            }
        }
       
        calculateData("AR");
        args.grid.invalidateRow(args.row);
        args.grid.render();
    });
    gridDetail.onSort.subscribe(function (e, args) {
        var cols = args.sortCols;

        detailDataView.sort(function (dataRow1, dataRow2) {
            for (var i = 0, l = cols.length; i < l; i++) {
              var field = cols[i].sortCol.field;
              var sign = cols[i].sortAsc ? 1 : -1;
              var value1 = dataRow1[field], value2 = dataRow2[field];
              if(value1 === undefined || value1 === null) { value1 = '';}
              if(value2 === undefined || value2 === null) { value2 = '';}
              var result = (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
              if (result != 0) {
                return result;
              }
            }
            return 0;
        });
        gridDetail.invalidate();
        gridDetail.render();
    });
    
    /**gridDetail.onAddNewRow.subscribe(function (e, args) {
        
        var item = args.item;
        item.lineNumber = gridDetail.getData().getLength()+1;
        item.id = "new"+newRowId;
        newRowId++;
        gridDetail.invalidateRow(item.lineNumber);
        gridDetail.invalidateRow(item.lineNumber-1);
        gridDetail.getData().addItem(item);
        gridDetail.updateRowCount();
        gridDetail.render();
        validatePB("AR");
    });**/
    gridDetail.init();
    detailDataView.beginUpdate();
    detailDataView.setItems(dataDetail);
    detailDataView.endUpdate();
    gridDetail.render();
    
	//grid.setSelectedRows(grid.getSelectedRows());
}

function loadPayableInvoice() { 
	            
    //GRID AP
    gridAP = new Slick.Grid("#myGridAP", APDataView, APcolumns, invoiceGridOptions);
    gridAP.setSelectionModel(new Slick.RowSelectionModel());
    gridAP.onSort.subscribe(function (e, args) {
        var cols = args.sortCols; 
        APDataView.sort(function (dataRow1, dataRow2) {
            for (var i = 0, l = cols.length; i < l; i++) {
              var field = cols[i].sortCol.field;
              var sign = cols[i].sortAsc ? 1 : -1;
              var value1 = dataRow1[field], value2 = dataRow2[field];
              if(value1 === undefined || value1 === null) { value1 = '';}
              if(value2 === undefined || value2 === null) { value2 = '';}
              var result = (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
              if (result != 0) {
                return result;
              }
            }
            return 0;
        });
        gridAP.invalidate();
        gridAP.render();
    });
    //CLICK AP
    gridAP.onClick.subscribe(function(e, args) {
        gridAP.setSelectedRows([args.row]);
    });
    gridAP.onSelectedRowsChanged.subscribe(function(e, args) {
        $("#APEditButton").show();
        var item = APDataView.getItem(gridAP.getSelectedRows()[0]);
        
        //alert(item.id);
        var lines;
        var formatCreated = getFormattedDate(item.created);
        var formatSentDate = getFormattedDate(item.sent);
        $('#invoice-AP').text(item.invoiceNumber);
        $('#to-AP').text(item.vendor);
        $('#priceBook-AP').text(getOptionLabel(apPriceBooks,item.priceBook));
        loadAPCategories(item.priceBook);
        loadAPProductOptions(item.priceBook,item.vendorId);
        $('#created-AP').text(formatCreated);
        $('#sent-AP').text(formatSentDate);
        
        //Code added - Padmesh Soni (12/30/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
        $('#status-AP').text(getOptionLabel(invoiceStatuses ,item.status));
        
        $('#publicCmtsAP').val(item.publicNotes);
        $('#privateCmtsAP').val(item.privateNotes);
		dataDetailAP = [];
        if(item.id!=null&&item.id.substring(0,3)!='new'){
            InvoicingController.loadLines(
                item.id,
                function(results, event) {
                    
                    if(event.type === 'exception'){
                        console.log("exception");
                        console.log(event);
                    }else if(event.status){
                        $.each(results, function(i, result){
                        	
                            dataDetailAP[i] = {
                                lineNumber: i+1,
                                id:result.id,
                                category: result.productCategory,
                                product: result.productId,
                                description: result.description,
                                uom: result.uom,
                                qty: result.qty,
                                rate: result.unitPrice,
                                discount: result.discount,
                                amount: '',
                                originalRate: result.unitPrice,
                                recordType:result.recordType
                              };
                            
                        });

                    }else{
                        console.log(event.message);
                    }
                                            detailAPDataView.beginUpdate();
                        detailAPDataView.setItems(dataDetailAP);
                        detailAPDataView.endUpdate();
                        gridDetailAP.invalidateAllRows();
                        gridDetailAP.render();
                        calculateData("AP");
                },{escape: false}
            );
        } else{
            detailAPDataView.beginUpdate();
            detailAPDataView.setItems(dataDetailAP);
            detailAPDataView.endUpdate();
            gridDetailAP.invalidateAllRows();
            gridDetailAP.render();
            calculateData("AP");
        }
    });
    //END CLICK
    APDataView.onRowsChanged.subscribe(function (e, args) {
        $("#APCount").html(APDataView.getLength());
        gridAP.invalidateRows(args.rows);
        gridAP.render();
    });
    
    gridAP.init();
    APDataView.beginUpdate();
    APDataView.setItems(APInvoiceData);
    APDataView.endUpdate();
    
    loadPayableInvoiceLines();
}
function loadPayableInvoiceLines() {
	
	//Detail View AP (RIGHT)
    var dataDetailAP = [];

    gridDetailAP = new Slick.Grid("#myGridDetailAP", detailAPDataView, columnsDetailAPRight, invoiceLineGridOptions);
    gridDetailAP.onCellChange.subscribe(function(e,args){
        var columnId  = args.grid.getColumns()[args.cell].id;
        if(columnId == "category"){
            args.item.product = null;
            args.item.rate = null;
            args.item.uom = null;
            args.item.discount = null;
            args.item.qty = null;
        }
        
        if(columnId == "product"){
            var found = $.grep(args.grid.getColumns()[args.cell].options,function(e){
              var value = args.item[args.grid.getColumns()[args.cell].field];
              if(e.value!=undefined && e.value.id!=undefined){
                return e.value.id == value;
              }
              return e.value == value; 
              }
            );
            if(found.length > 0 && found[0].value != undefined){
                args.item.rate = found[0].value.rate;
                args.item.uom  = found[0].value.uom;
                if(args.item.qty == null){
                	args.item.qty = 1;
                }
            }
        }
        calculateData("AP");
        args.grid.invalidateRow(args.row);
        args.grid.render();
    });
    gridDetailAP.onSort.subscribe(function (e, args) {
        var cols = args.sortCols;

        detailAPDataView.sort(function (dataRow1, dataRow2) {
            for (var i = 0, l = cols.length; i < l; i++) {
              var field = cols[i].sortCol.field;
              var sign = cols[i].sortAsc ? 1 : -1;
              var value1 = dataRow1[field], value2 = dataRow2[field];
              var result = (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
              if (result != 0) {
                return result;
              }
            }
            return 0;
        });
        gridDetailAP.invalidate();
        gridDetailAP.render();
    });
    /**gridDetailAP.onAddNewRow.subscribe(function (e, args) {
        
        var item = args.item;
        item.lineNumber = gridDetailAP.getData().getLength()+1;
        item.id = "new"+newRowId;
        newRowId++;
        gridDetailAP.invalidateRow(item.lineNumber);
		gridDetailAP.invalidateRow(item.lineNumber-1);
        gridDetailAP.getData().addItem(item);
        gridDetailAP.updateRowCount();
        gridDetailAP.render();
        validatePB("AP");
    });**/
    
    gridDetailAP.init();
    detailAPDataView.beginUpdate();
    detailAPDataView.setItems(dataDetailAP);
    detailAPDataView.endUpdate();
    gridDetailAP.render();	
}

function loadAPPricebookOptions(caseId){
 
  	InvoicingController.getAPPriceBooks(caseId,function(results,event){
	
		apPriceBooks = results;
		var option_str = ""; 
		apPriceBooks.forEach(function(v){
                option_str += makeOption(v);
            });
		gridAP.getColumns()[gridAP.getColumnIndex("priceBook")].options=apPriceBooks;
		gridAP.invalidateAllRows();
		gridAP.render();
		//DEFAULT FIRST ROW
  		$('#myGridAP').find('.r0').trigger('click');
	});
}

function loadARPricebookOptions(caseId){
  	InvoicingController.getARPriceBooks(caseId,function(results,event){
		arPriceBooks = results;
		var option_str = ""; 
		arPriceBooks.forEach(function(v){
	            option_str += makeOption(v);
	        });
	    
		grid.getColumns()[grid.getColumnIndex("priceBook")].options=arPriceBooks;
		grid.invalidateAllRows();
		grid.render();
		$('#myGridAR').find('.r0').trigger('click');
	});
}

function loadPriceBookOptions() {
    InvoicingController.getPriceBooks(orderId,function(results,event){
        priceBooks = results;
        var option_str = ""; 
        priceBooks.forEach(function(v){
                option_str += makeOption(v);
            });
        
        loadStatusOptions();
        
        gridAP.getColumns()[gridAP.getColumnIndex("priceBook")].options=priceBooks;
        gridAP.invalidateAllRows();
        gridAP.render();
        //DEFAULT FIRST ROW
        $('#myGridAP').find('.r0').trigger('click');
        
        
        grid.getColumns()[grid.getColumnIndex("priceBook")].options=priceBooks;
        grid.invalidateAllRows();
        grid.render();
        $('#myGridAR').find('.r0').trigger('click');
  });
}
function loadStatusOptions(){
    InvoicingController.getStatusValues(function(results,event){
        invoiceStatuses = results;
        var option_str = ""; 
        invoiceStatuses.forEach(function(v){
            option_str += makeOption(v);
        });
        //$("#statusInput-AR").html(option_str);
        //$("#statusInput-AP").html(option_str);
    });
}
    
function getFormattedDate(value){
	if(value == null || value == "")
		return "";
	var formatted = $.datepicker.formatDate("mm/dd/yy", new Date(value.replace("-", "/","g")));
	return formatted;
}
  
function calculateData(type){
	var subtotalC = 0;
	var totalC = 0;
	var amountT = 0;
	//AR
	var currentData = detailDataView.getItems();
	if(type=='AP')
		currentData = detailAPDataView.getItems();
		
	for(var key in currentData) {
		if(currentData[key].qty != null && currentData[key].rate != null){
			subtotalC += currentData[key].qty*currentData[key].rate;
			if(typeof currentData[key].discount != "number"){
				//currentData[key].discount = 0;
			}
			amountT = currentData[key].amount;
			totalC += parseFloat(amountT);
	   }
	}
	subtotalC = subtotalC.toFixed(2);
	totalC = totalC.toFixed(2);
	
	var discountTotal = (100 - (100 * (totalC/subtotalC))).toFixed(2);
	//Update values 
	discountTotal = isNaN(discountTotal) ? '0' : discountTotal;
	setText('discountTotal-'+type,discountTotal+"%");
	setText('subTotal-'+type,"$"+subtotalC);
	setText('total-'+type,"$"+totalC);
}
function setText(id, value) {
	$('#'+id).text(value);  
}
var DateFormatter = function(row, cell, value, columnDef, dataContext){
	if(value == null || value == "")
		return "";
	var formatted = $.datepicker.formatDate("mm/dd/yy", new Date(value.replace("-", "/","g")));
	return formatted;
}   
var CurrencyFormatter=function(row, cell, value, columnDef, dataContext){
	if(value==null ||value==undefined||typeof value != "number"){
		return "";
	}
	return "$"+(value.toFixed(2));
}
var WholePercentFormatter=function(row, cell, value, columnDef, dataContext){
	if(typeof value!="number"){
		value = 0;
		return "";
	}
	return value+"%";
}
var AmountCellFormatter=function(row, cell, value, columnDef, dataContext){
	var d = dataContext;
	if(typeof d.discount != "number"){
		d.discount = 0;
	}
	if(typeof d.rate != "number"){
		d.rate = 0 ;
	}
	var amt = d.qty*(d.rate-((d.discount/100)*d.rate));
	if(isNaN(amt)){
		return "";
	}
	d.amount = amt.toFixed(2);
	return "$"+amt.toFixed(2);
}
var SelectCellFormatter=function(row, cell, value, columnDef, dataContext) {
	return getOptionLabel(columnDef.options,value);
}
var getOptionLabel = function(options,value){
	if(options.length==0){
		return value;
	}
	var opt = $.grep(options, function(e){
		if(e.value!=undefined && e.value.id!=undefined){
			return e.value.id == value;
		}
		return e.value == value; 
	});
	if(opt.length==0){
		return '';
	}
	return opt[0].label;
}
var columns = [
  { id: "invoice", name: "Invoice", field: "invoiceNumber", sortable: true,focusable:false,width:100 },
  { id: "from", name: "Rec. From", field: "client", sortable: true,focusable:false,width:200 },
  { id: "priceBook", name: "Price Book", field: "priceBook", sortable: true,options:[],formatter:SelectCellFormatter,focusable:false,width:200 },
  { id: "created", name: "Created", field: "created", sortable: true,focusable:false,formatter:DateFormatter,width:100  },
  { id: "sent", name: "Sent", field: "sent", sortable: true,focusable:false, formatter:DateFormatter,width:100 },
  { id: "status", name: "Status", field: "status", sortable: true,focusable:false,width:80 },
  { id: "total", name: "Total", field: "total", sortable: true,formatter:CurrencyFormatter,focusable:false,width:100 }
];
var APcolumns = [
  { id: "invoice", name: "Invoice", field: "invoiceNumber", sortable: true ,focusable:false,width:100},
  { id: "to", name: "Pay To", field: "vendor", sortable: true,focusable:false, width:200 },
  { id: "priceBook", name: "Price Book", field: "priceBook", sortable: true,options:[],formatter:SelectCellFormatter ,focusable:false, width:200},
  { id: "created", name: "Created", field: "created", sortable: true ,focusable:false,  formatter:DateFormatter, width:100},
  { id: "sent", name: "Sent", field: "sent", sortable: true,focusable:false,  formatter:DateFormatter,width:100 },
  { id: "status", name: "Status", field: "status", sortable: true ,focusable:false,width:80},
  { id: "total", name: "Total", field: "total", sortable: true,formatter:CurrencyFormatter ,focusable:false,width:100}
];

function formatter(row, cell, value, columnDef, dataContext) {
    return value;
}

var columnsDetail = [
  { id: "lineNumber", name: "#", field: "lineNumber", sortable: false ,width: 10,focusable:false },
  { id: "category", name: "Category", field: "category",options:[],formatter:SelectCellFormatter, sortable: false,width:140,validator: requiredFieldValidator },
  { id: "product", name: "Product", field: "product", priceBook:"",filterField:"category", options:[],formatter:SelectCellFormatter,sortable: false,width: 140,validator: requiredFieldValidator },
  { id: "description", name: "Description", field: "description", sortable: false,width: 275},
  { id: "uom", name: "UOM", field: "uom", sortable: false ,width: 60 },
  { id: "qty", name: "Qty", field: "qty", sortable: false,width: 60,validator: requiredFieldValidator},
  { id: "rate", name: "Rate", field: "rate", sortable: false ,width: 80, formatter:CurrencyFormatter,validator: requiredFieldValidator},
  { id: "discount", name: "Disc", field: "discount", sortable: false,width: 60,formatter:WholePercentFormatter},
  { id: "amount", name: "Amount", field: "amount", sortable: false ,width: 100,formatter:AmountCellFormatter }
  
];
  
var columnsDetailAPRight = [
  { id: "lineNumber", name: "#", field: "lineNumber", sortable: false ,width: 10,focusable:false },
  { id: "category", name: "Category", field: "category",options:[],formatter:SelectCellFormatter, sortable: false,width:140,validator: requiredFieldValidator },
  { id: "product", name: "Product", field: "product", priceBook:"",filterField:"category", options:[],formatter:SelectCellFormatter, sortable: false,width: 140,validator: requiredFieldValidator },
  { id: "description", name: "Description", field: "description", sortable: false,width: 275},
  { id: "uom", name: "UOM", field: "uom", sortable: false ,width: 60 },
  { id: "qty", name: "Qty", field: "qty", sortable: false,width: 60,validator: requiredFieldValidator },
  { id: "rate", name: "Rate", field: "rate", sortable: false ,width: 80, formatter:CurrencyFormatter,validator: requiredFieldValidator},
  { id: "discount", name: "Disc", field: "discount", sortable: false,width: 60,formatter:WholePercentFormatter },
  { id: "amount", name: "Amount", field: "amount", sortable: false ,width: 100, formatter:AmountCellFormatter }
];

var invoiceGridOptions = {
  enableColumnReorder: false,
  explicitInitialization: true,
  multiColumnSort: true,
  autoEdit:false
};

var invoiceLineGridOptions = {
  enableCellNavigation: true,
  enableColumnReorder: false,
  explicitInitialization: true,
  asyncEditorLoading: false,
  multiColumnSort: true,
  editable:false,
  autoEdit:true,
  enableAddRow:false
};
      
function loadARCategories(pbID){
	InvoicingController.getProductCategories(pbID,function(results,event){
		arPBCategories = results;
		gridDetail.getColumns()[gridDetail.getColumnIndex("category")].options=arPBCategories;
		gridDetail.invalidateAllRows();
		gridDetail.render();
	});
}
    
function loadAPCategories(pbID){
	InvoicingController.getProductCategories(pbID,function(results,event){
		apPBCategories = results;
		gridDetailAP.getColumns()[gridDetailAP.getColumnIndex("category")].options=apPBCategories;
		gridDetailAP.invalidateAllRows();
		gridDetailAP.render();
	});
}

function loadARProductOptions(pbID){
	InvoicingController.getProducts(pbID,function(results,event){
		arPBProducts = results;
		gridDetail.getColumns()[gridDetail.getColumnIndex("product")].options=arPBProducts;
		gridDetail.invalidateAllRows();
		gridDetail.render();
	});
}

function loadAPProductOptions(pbID,vendorId){
	InvoicingController.getProducts(pbID,function(results,event){
		apPBProducts = results;
		InvoicingController.getVendorProducts(vendorId,function(results,event){
			apVendProducts = results;
			gridDetailAP.getColumns()[gridDetailAP.getColumnIndex("product")].options=apPBProducts;
			gridDetailAP.invalidateAllRows();
			gridDetailAP.render();
		});
	});
}

function initializeInvoices(caseId){
	loadAPPricebookOptions(caseId);
	loadARPricebookOptions(caseId);  
	$( "#dialog-error" ).dialog({
		resizable: false,
		height:140,
		width:600,
		modal: true,
		autoOpen:false,
		closeOnEscape:false,
		draggable:false,
		position:{ my: "center", at: "center", of: window }
	});
	
	loadStatusOptions();
}

function onCompleteRecv(){
	
	$("#AREditButton").bind('click',toggleAREditMode);
	var option_str = ""; 
    arPriceBooks.forEach(function(v){
    	  option_str += makeOption(v);
    });
    
	grid.getColumns()[gridAP.getColumnIndex("priceBook")].options=arPriceBooks;
    grid.invalidateAllRows();
    grid.render();
    grid.setSelectedRows(arSelectedRow);
}
function onCompletePay(){
	
	$("#APEditButton").bind('click',toggleAPEditMode);
	var option_str = ""; 
    apPriceBooks.forEach(function(v){
    	  option_str += makeOption(v); 
	});
    
    gridAP.getColumns()[gridAP.getColumnIndex("priceBook")].options=apPriceBooks;
    gridAP.invalidateAllRows();
    gridAP.render();
    gridAP.setSelectedRows(apSelectedRow);
}

function validatePB(type){
	
	var pbVal = (type == 'AR')?$('#pricebookInput-AR').val():$('#pricebookInput-AP').val();
    
    if(pbVal == '') {
		$( "#errorMsg" ).html(INVOICE_PB_REQUIRED);
		$( "#dialog-error" ).dialog("open");
	}        
}