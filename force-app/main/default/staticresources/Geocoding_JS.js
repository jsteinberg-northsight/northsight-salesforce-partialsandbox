// Map variables
var map;
var marker;
var greenStatus = new Array("street_address","premise","subpremise");

jQuery.noConflict();

function doSaveAndRelease(){
	updateWO(jQuery('#context_lat').val(), jQuery('#context_lng').val(), jQuery('#context_street').val(), jQuery('#context_city').val(), 
				jQuery('#context_state').val(), jQuery('#context_zip').val(), jQuery('#context_notes').val(), jQuery('#county').val());
	showProgress();
}

jQuery(document).ready(
	function(){
		jQuery("#QTable > tbody > tr:nth-child(even)").addClass("evenRow");
		jQuery('#cancel').bind("click",function() {
			opener.location.reload(); // or opener.location.href = opener.location.href;
			window.close();             
		});
		init_map();
		setTimeout(function() { 
			google.maps.event.trigger(map, 'resize'); 
			if(typeof marker != 'undefined') {
				map.setZoom(15);
				map.setCenter(marker.getPosition());
			}
		}, 500);

		geocodeAddress()
	} 
);

function init_map() {
	var mapOptions = {zoom: 8,
			center: new google.maps.LatLng(-34.397, 150.644),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
	map = new google.maps.Map(document.getElementById('map_div'), mapOptions);
}

function edit_quarantine(id, street,city,state,zip, lat, lng, notes) {
	jQuery('#geocodeStatus').html('&nbsp;');
	jQuery('#geocodeStatusIcon').html('&nbsp;');
	if(lat==0) {
		lat = 38.898748;
		lng = -77.037684;
	}

	// Set the context info
	jQuery('#context_street').val(street);
	jQuery('#context_city').val(city);
	jQuery('#context_state').val(state);
	jQuery('#context_zip').val(zip);
	jQuery('#context_lat').val(lat);
	jQuery('#context_lng').val(lng);
	jQuery('#context_notes').val(notes);
	drawMarker(lat, lng);

	jQuery('#dialog_edit').dialog('open');
}
function getStatusIcon(types){
	for(var i=0;i<types.length;i++){
		if(jQuery.inArray(types[i],greenStatus)){
			return "<img style='vertical-align:middle;' src='/servlet/servlet.FileDownload?file=01540000000aRgp'/>";                    
		}
	}
	return "<img style='vertical-align:middle;' src='/servlet/servlet.FileDownload?file=01540000000aRgu'/>";
}
function geocodeAddress() {
	geocoder = new google.maps.Geocoder();
	geocoder.geocode({ address: jQuery('#context_street').val()+', '+jQuery('#context_city').val()+', '+jQuery('#context_state').val()+' '+jQuery('#context_zip').val() }, function(results, status) {
		console.log(results);
		if (status == google.maps.GeocoderStatus.OK) {
			
			jQuery('#geocodeStatus').html('Response Type:'+results[0].types.join(','));
			jQuery('#geocodeStatusIcon').html(getStatusIcon(results[0].types));
			
			//update the visuals
			jQuery('#context_lat').val(results[0].geometry.location.lat());
			jQuery('#context_lng').val(results[0].geometry.location.lng());

			for (var i = 0; i < results[0].address_components.length; i++) {
				var addr = results[0].address_components[i];
			
				// check if this entry in address_components has a type of country
				if (addr.types[0] == ['administrative_area_level_2'])       // State
					jQuery('#county').val(addr.long_name);
			}

			drawMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng());
			map.setCenter(marker.getPosition());
		} else {
			alert('Geocode failed with a status of: ' + status);
		}
	});
}

function drawMarker(lat, lng) {
	var latlng = new google.maps.LatLng(lat, lng);

	if(typeof marker == 'undefined') {
		marker = new google.maps.Marker({ position: latlng,
							map: map,
							draggable: true
						});
	} else {
		marker.setPosition(latlng);
	}


	google.maps.event.addListener(marker, 'drag', function() {
		jQuery('#context_lat').val(marker.getPosition().lat()); 
		jQuery('#context_lng').val(marker.getPosition().lng());
	});

	google.maps.event.addListener(map, 'dblclick', function (event) {
		marker.setPosition(event.latLng);
		jQuery('#context_lat').val(marker.getPosition().lat());
		jQuery('#context_lng').val(marker.getPosition().lng());
	});
}

function updateLatLng() {
	marker.setPosition(new google.maps.LatLng(
		jQuery('#context_lat').val(),
		jQuery('#context_lng').val()
	));
	map.setCenter(marker.getPosition());
}

function select_all(value) {
	var status;
	if(value) {
		status = 'checked';
	} else {
		status = false;
	}
	jQuery('.chk').attr('checked', status);
}

function keepSelectAllSynced() {
	var all = true;
	jQuery('.chk').each(function(k,v) { 
		if(!v.checked) {
			all = false;
		}
	});

	if(!all) {
		jQuery('#select_all_box').attr('checked', false);
	} else {
		jQuery('#select_all_box').attr('checked', 'checked');
	}
}

var counter = 0;
var idList;
function reGeocodeAll() {
	var temp;
	idList = new Array();
	jQuery('.chk').each(function(k,v) { 
		//console.log(v);
		if(v.checked) {
			temp = v.title.split(':');
			if(temp[0]!='OK') {
				idList.push(temp[1]);
			}
		}
	});

	if(idList.length==0) {
		alert('There are no items selected');
		return;
	}

	// reverse the array b/c we are going to process it backward
	//console.log(idList);
	//idList.reverse();
	//console.log(idList);
	showProgress();
	counter = 0;
	doGeocodeOps();
}

function doGeocodeOps() {
	if(counter>=idList.length) {
		hideProgress();
		return;
	}

	var max;
	if(counter+10 > idList.length) {
		max = idList.length;
	} else {
		max = counter+10;
	}
	var id_string = idList[counter];
	for(var x=counter+1;x<max;x++) {
		id_string = id_string + ':' + idList[x];
	}

	counter += 10;

	//console.log(id_string);
	apexGeocode(id_string); 
}

var spinnerVisible = false;
function showProgress() {
	if (!spinnerVisible) {
		jQuery("div#spinner").fadeIn("fast");
		spinnerVisible = true;
	}
};
function hideProgress() {
	if (spinnerVisible) {
		var spinner = jQuery("div#spinner");
		spinner.stop();
		spinner.fadeOut("fast");
		spinnerVisible = false;
	}
};