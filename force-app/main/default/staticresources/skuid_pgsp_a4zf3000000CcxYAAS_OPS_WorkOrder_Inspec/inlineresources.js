(function(skuid){
skuid.snippet.register('Wait',function(args) {var SECONDS_TO_DELAY = 4;
var dfd = skuid.$.Deferred();
setTimeout(function(){
    dfd.resolve();
},SECONDS_TO_DELAY*1000);
return dfd.promise();
});
skuid.snippet.register('Print',function(args) {var params = arguments[0],
	$ = skuid.$;
window.print();
});
}(window.skuid));