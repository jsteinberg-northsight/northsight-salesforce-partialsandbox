<!--
    Purpose         :   This page is used as an interface for Invoicing system.

    Modified By     :   Padmesh Soni

    Modified Dates  :   12/9/2014 - 1/9/2015

    Current Version :   V1.0

    Revision Log    :   V1.0 - Modified
 -->
<apex:page standardController="Case" extensions="InvoicingController" docType="html-5.0" showHeader="false" standardStylesheets="false" title="Work Order Invoicing">
    <apex:includeScript value="{!URLFOR($Resource.Slickgrid_2_3_2, 'lib/jquery-1.11.2.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Slickgrid_2_3_2, 'lib/jquery-ui-1.11.3.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Slickgrid_2_3_2, 'lib/jquery.event.drag-2.3.0.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Slickgrid_2_3_2, 'slick.core.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Slickgrid_2_3_2, 'slick.formatters.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Slickgrid_2_3_2, 'slick.editors.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Slickgrid_2_3_2, 'slick.dataview.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Slickgrid_2_3_2, 'slick.grid.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Slickgrid_2_3_2, 'lib/firebugx.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Slickgrid_2_3_2, 'plugins/slick.autotooltips.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Slickgrid_2_3_2, 'plugins/slick.rowselectionmodel.js')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Slickgrid_2_3_2, 'slick.grid.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Slickgrid_2_3_2, 'slick-default-theme.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Slickgrid_2_3_2, 'css/smoothness/jquery-ui-1.11.3.custom.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Invoicing_CSS)}"/>

    <div id="dialog-save" title="Saving">
        Saving invoice.
        <div id="progress-save"/>
    </div>

    <div id="dialog-error" title="Errors">
        <div id="errorMsg"/>
    </div>

    <apex:form >

        <apex:actionfunction name="refreshARAction" action="{!refreshAR}" rerender="recvScrptPanel, recvPanel, summaryPanel" oncomplete="onCompleteRecv();"/>
        <apex:actionfunction name="refreshAPAction" action="{!refreshAP}" rerender="payScrptPanel, payPanel, summaryPanel" oncomplete="onCompletePay();"/>

        <div class= "container headerBox"> <div style= "padding-top: 7px;padding-left: 7px;"> Work Order Invoicing </div></div>
        <apex:pageMessages id="errors"/>
        <div class="container">
            <div class="info">
                <span class="bold">Work Order Number:</span><apex:outputPanel styleClass="infoValue"><apex:outputLink value="{!URLFOR($Action.Case.View,Case.Id)}" target="_blank">{!Case.CaseNumber}</apex:outputLink></apex:outputPanel>
            </div>
            <div class="info">
                <span class="bold"> Property ID:</span><apex:outputPanel styleClass="infoValue"><apex:outputLink value="/{!workOrder.Geocode_Cache__r.Id}" target="_blank">{!workOrder.Geocode_Cache__r.Name}</apex:outputLink></apex:outputPanel>
            </div>
            <div class="info info-full">
                <span class="bold">Address:</span><apex:outputPanel styleClass="infoValue"><apex:outputField value="{!Case.Display_Address__c}"/></apex:outputPanel>
            </div>
            <div class="info info-full">
                <span class="bold">Lot Size:</span><apex:outputPanel styleClass="infoValue"><apex:outputField value="{!Case.Geocode_Cache__r.SqFt_of_Lot_Size__c}"/></apex:outputPanel>
            </div>
            <div class="info info-full">
                <span class="bold">Drive Length:</span><apex:outputPanel styleClass="infoValue"><apex:outputField value="{!Case.Geocode_Cache__r.Driveway_Length__c}"/></apex:outputPanel>
            </div>
            <div class="info">
                <span class="bold">Client:</span><apex:outputPanel styleClass="infoValue"><apex:outputLink value="/{!workOrder.Client_Name__r.Id}" target="_blank">{!workOrder.Client_Name__r.Name}</apex:outputLink></apex:outputPanel>
            </div>
            <div class="info">
                <span class="bold">Work Ordered:</span><apex:outputPanel styleClass="infoValue"><apex:outputField value="{!Case.Work_Ordered__c}"/></apex:outputPanel>
            </div>
            <div class="info">
                <span class="bold">Vendor Code:</span><apex:outputPanel styleClass="infoValue"><apex:outputField value="{!Case.Vendor_Code__c}"/></apex:outputPanel>
            </div>
            <div class="info">
                <span class="bold">Last Grass Cut:</span><apex:outputPanel styleClass="infoValue"><apex:outputField value="{!Case.Geocode_Cache__r.Last_Grass_Cut_Date__c}"/></apex:outputPanel>
            </div>
            <div class="info">
                <span class="bold">Last Maid:</span><apex:outputPanel styleClass="infoValue"><apex:outputField value="{!Case.Geocode_Cache__r.Last_Maid_Completion_Date__c}"/></apex:outputPanel>
            </div>
            <div class="info">
                <span class="bold">Last Snow:</span><apex:outputPanel styleClass="infoValue"><apex:outputField value="{!Case.Geocode_Cache__r.Last_Snow_Completion_Date__c}"/></apex:outputPanel>
            </div>
        </div>
        <div class="container">
            <apex:outputPanel id="recvPanel" layout="block" styleClass="panelInvoice">
                <div class="column-content">

                        <span class="title-section">  Receivable Invoices (<span id="ARCount">0</span>)</span>

                        <button class="newInvoiceButton"  id="newInvoiceAR" href="#" onclick="newInvoice('AR'); return false;">New Invoice</button>

                    <br/>
                    <div class="border">
                    <div id="myGridAR" style="width:100%;height:100px;"> </div>
                    </div>
                </div>
                <div class="column-content border">
                    <div class="table-info" style="float:left;">
                        <input id="AREditButton" type="button" class="styled-button-8 bold hidden" value="Edit" />
                        <a id="arPrint" href="{!domain}/apex/InvoicingPdf?id=" target="_blank">
                        	<input id="ARPrintButton" type="button" class="styled-button-8 bold hidden" value="Print"/>
                        </a>
                        <input id="ARSaveButton" type="button" class="editMode styled-button-8 bold hidden" value="Save" onclick="sendAR_AP('AR','{!Case.Id}'); return false;"/>
                        <input id="ARCancelButton" type="button" class="editMode styled-button-8 bold hidden" value="Cancel" onclick="cancelFromEdit('AR'); return false;"/>
                    </div>
                    <div class="table-info three">

                        <table>
                            <tr>
                                <td>
                                    <span class="bold">Invoice #:&nbsp; </span>
                                </td>
                                <td>
                                    <span id="invoice-AR"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="bold">Rec. From:&nbsp; </span>
                                </td>
                                <td>
                                    <span id="to-AR"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="bold">Status:&nbsp; </span>
                                </td>
                                <td>
                                    <span id="status-AR"></span>
                                    <!-- <select class="hidden" id="statusInput-AR"></select> -->
                                    <span class="hidden" id="statusInput-AR"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="table-info three to-right">

                        <table>
                            <tr>
                                <td>
                                    <span class="bold">Created:&nbsp;</span>
                                </td>
                                <td>
                                    <span id="created-AR"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="bold">Sent:&nbsp;</span>
                                </td>
                                <td>
                                     <span id="sent-AR" style="display:inline;"></span>
                                     <input class="hidden" id="sentInput-AR"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="bold">Price Book:&nbsp;</span>
                                </td>
                                <td>
                                    <span id="priceBook-AR"></span><select class="hidden" id="pricebookInput-AR" style="border-color:red;"></select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br/>
                    <div class="border">
                    <div id="myGridDetail" style="width:100%;height:300px;"></div>
                    </div>
                    <div style="width:100%">
                        <div class="table-info" style="width: 390px;">
                            <apex:outputLabel value="Private comments"/>
                            <textarea class="noresize" id="privateCmtsAR"  rows="3" disabled="true"></textarea>
                        </div>
                        <div class="table-info" style="float:left;width: 390px;">
                            <apex:outputLabel value="Public comments"/>
                            <textarea class="noresize" id="publicCmtsAR"  rows="3" disabled="true"></textarea>
                        </div>
                        <div style="display:inline-block;text-align:right;">
                            <span class="bold">Sub-Total:&nbsp;</span>
                             <br/>
                            <span class="bold">Discount:&nbsp;</span>
                             <br/>
                            <span class="bold">Total:&nbsp;</span>
                             <br/>
                        </div>
                        <div style="display:inline-block; text-align:right;">
                            <span class="totalFields" id="subTotal-AR"></span>
                            <br/>
                            <span class="totalFields" id="discountTotal-AR"></span>
                            <br/>
                            <span class="bold totalFields" id="total-AR"></span>
                            <br/>
                        </div>


                    </div>
                </div>
            </apex:outputPanel>
            <apex:outputPanel id="payPanel" layout="block" styleClass="panelInvoice">
                <div class="column-content">

                        <span class="title-section">  Payable Invoices (<span id="APCount">0</span>)</span>

                        <button class="newInvoiceButton" id="newInvoiceAP" href="#" onclick="newInvoice('AP'); return false;">New Invoice</button>

                    <br/>
                    <div class="border">
                    <div id="myGridAP" style="width:100%;height:100px;"> </div>
                    </div>
                </div>
                <div class="column-content border">
                    <div class="table-info" style="float:left;">
                        <input id="APEditButton" type="button" class="styled-button-8 bold hidden" value="Edit" />
                        <a id="apPrint" href="{!domain}/apex/PayableInvoicePdf?id=" target="_blank">
                        	<input id="APPrintButton" type="button" class="styled-button-8 bold hidden" value="Print" />
                        </a>
                        <input id="APSaveButton" type="button" class="editMode styled-button-8 bold hidden" value="Save" onclick="sendAR_AP('AP','{!Case.Id}'); return false;"/>
                        <input id="APCancelButton" type="button" class="editMode styled-button-8 bold hidden" value="Cancel" onclick="cancelFromEdit('AP');  return false;"/>
                    </div>
                    <div class="table-info three">
                        <table>
                        <tr>
                            <td>
                            <span class="bold">Invoice #:&nbsp; </span>
                            </td>
                            <td>
                            <span id="invoice-AP"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <span class="bold">Pay To:&nbsp; </span>
                            </td>
                            <td>
                            <span id="to-AP"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <span class="bold">Status:&nbsp; </span>
                            </td>
                            <td>
                            <span id="status-AP"></span>
                            <span class="hidden" id="statusInput-AP"></span>
                            <!-- <select class="hidden" id="statusInput-AP"></select> -->
                            </td>
                        </tr>
                        </table>
                    </div>
                    <div class="table-info three to-right">

                        <table>
                        <tr>
                            <td>
                            <span class="bold">Created:&nbsp;</span>
                            </td>
                            <td>
                            <span id="created-AP"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <span class="bold">Sent:&nbsp;</span>
                            </td>
                            <td>
                             <span id="sent-AP" style="display:inline;"></span>
                             <input class="hidden" id="sentInput-AP"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <span class="bold">Price Book:&nbsp;</span>
                            </td>
                            <td>
                            <span id="priceBook-AP"></span><select  class="hidden" id="pricebookInput-AP" style="border-color:red;"></select>
                            </td>
                        </tr>
                        </table>
                    </div>
                    <br/>
                    <div class="border">
                    <div id="myGridDetailAP" style="width:100%;height:300px;"></div>
                    </div>
                    <div style="width:100%">
                        <div class="table-info" style="width: 390px;">
                            <apex:outputLabel value="Private comments"/>
                            <textarea class="noresize" id="privateCmtsAP" rows="3" disabled="true"></textarea>
                        </div>
                        <div class="table-info" style="float:left;width: 390px;">
                            <apex:outputLabel value="Public comments"/>
                            <textarea class="noresize" id="publicCmtsAP" rows="3"  disabled="true"></textarea>
                        </div>
                        <div style="display:inline-block;text-align:right;">
                            <span class="bold">Sub-Total:&nbsp;</span><br/>
                            <span class="bold">Discount:&nbsp;</span><br/>
                            <span class="bold">Total:&nbsp;</span><br/>
                        </div>
                        <div style="display:inline-block; text-align:right;">
                            <span id="subTotal-AP"></span><br/>
                            <span id="discountTotal-AP"></span><br/>
                            <span class="bold" id="total-AP"></span><br/>
                        </div>


                    </div>
                </div>
            </apex:outputPanel>
        </div>

    </apex:form>

    <apex:outputPanel id="summaryPanel" layout="block">

        <div  style="width:800px;margin:auto;">
            <span class="title-section no-padding">Work Order Invoice Summary </span>
            <div class="to-right" style="width: 40%;margin: 14px 0px 0px 0px;">
                <span class="title-header" style="float:right;">
                    <span class="bold" style="font-size:13px;">Invoice Count:</span> Receivable: <apex:outputText value="{!ARTotal}"/> | Payable:&nbsp; <apex:outputText value="{!APTotal}"/>
                    </span>
            </div><br/>
            <c:WOInvoiceSummary wOId="{!workOrderId}"/>
        </div>
    </apex:outputPanel>

    <script>
        var APInvoiceTypeId = "{!APInvoiceTypeId}";
        var ARInvoiceTypeId = "{!ARInvoiceTypeId}";
        var APLineTypeId = "{!APLineTypeId}";
        var ARLineTypeId = "{!ARLineTypeId}";

        //global labels.
        var INVOICE_PB_NOT_AVAILABLE = "{!JSENCODE($Label.INVOICE_PB_NOT_AVAILABLE)}";
        var AP_INVOICE_PB_NOT_AVAILABLE = "{!JSENCODE($Label.AP_INVOICE_PB_NOT_AVAILABLE)}";
        var INVOICE_NOT_SAVED = "{!JSENCODE($Label.INVOICE_NOT_SAVED)}";
        var INVOICE_BLANK_NOT_SUPPORTED = "{!JSENCODE($Label.INVOICE_BLANK_NOT_SUPPORTED)}";
        var INVOICE_PB_VALIDATE = "{!JSENCODE($Label.INVOICE_AR_PB_VALIDATE)}";
        var INVOICE_PB_REQUIRED = "{!JSENCODE($Label.INVOICE_PB_REQUIRED)}";

        //Code added - Padmesh Soni (12/30/2014) - AC-6:Invoice Screen | Visual Force Page Prototype
        var orderId = "{!Case.Id}";
        var clientName = "{!JSENCODE(Case.Client_Name__r.Name)}";
        var clientId = "{!Case.Client_Name__c}";
        var orderConName = "{!JSENCODE(Case.Contact.Name)}";
        var orderConId = "{!Case.ContactId}";
        if (orderConId == "") {
            alert("No vendor is assigned to the work order. You will not be able to generate a payable invoice.");
        }
        var newPayablePB = "{!newVendorPayableInv}";
        $(document).ready(function(){initializeInvoices("{!Case.Id}");});

    </script>

    <apex:includeScript value="{!URLFOR($Resource.Invoicing_JS)}"/>
    <apex:outputPanel id="recvScrptPanel">
        <script>
            $(document).ready(function(){

                ARInvoiceData = {!invoiceSerializedReceivable};
                loadReceivableInvoice();
            });
        </script>
    </apex:outputPanel>
    <apex:outputPanel id="payScrptPanel">
        <script>
            $(document).ready(function(){

                APInvoiceData = {!invoiceSerializedPayable};
                loadPayableInvoice();
            });
        </script>
    </apex:outputPanel>


</apex:page>