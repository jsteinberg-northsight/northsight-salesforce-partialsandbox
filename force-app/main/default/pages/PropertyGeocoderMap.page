<!-- 
    Purpose         :   This page is used for geocoding of property addresses from Google Map APIs of Geocode.
    
    Modified By     :   Padmesh Soni
    
    Modified Date   :   1/12/2015
    
    Current Version :   V1.0
    
    Revision Log    :   V1.0 - Modified - CCP-15:Fill County on Property
                                Changes are:
                                    1. New feature of filling County__c field with getting result from Google map of 
                                        attribute "administrative_area_level_2".
                                    2. All JavaScripts and CSS style which were on page is moved to static resources.
-->
<apex:page standardController="Geocode_Cache__c" extensions="propertyGeocodeController" showHeader="true">
    
    <apex:includeScript value="{!URLFOR($Resource.jquery, 'js/jquery-1.8.2.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.jquery, 'js/jquery-ui-1.9.1.custom.min.js')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.jquery, 'css/redmond/jquery-ui-1.9.1.custom.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Geocoding_CSS)}"/>
  //Previous Google URL (updated the key), Josh Sarchet 10/18/2017      <apex:includeScript value="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAhcf6EV7zq0O_WxQ3QApc_Q-a7FUmCbvs"/>
    <apex:includeScript value="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDOGrrpfKRjvIIv-hVZNzehpa6lfTd-glE"/>

    <apex:form >
    
        <apex:pageBlock title="Geocode" mode="edit">
    
            <apex:actionFunction name="updateWO" action="{!saveAndRelease}" rerender="errors">
                <apex:param name="lat" assignTo="{!param_lat}" value="" />
                <apex:param name="lng" assignTo="{!param_lng}" value="" />
                <apex:param name="street" assignTo="{!param_street}" value="" />
                <apex:param name="city" assignTo="{!param_city}" value="" />
                <apex:param name="state" assignTo="{!param_state}" value="" />
                <apex:param name="zip" assignTo="{!param_zip}" value="" />
                <apex:param name="notes" assignTo="{!param_notes}" value="" />
                <apex:param name="county" assignTo="{!param_county}" value="" />
            </apex:actionFunction>
            
            <div id="spinner">
                <div class="inner"><p>Working...</p></div>
            </div>
            <apex:pageBlockButtons >
                <input type="button" class="btn" value="Save & Release" onclick="doSaveAndRelease();"/>
                <apex:commandButton action="{!cancel}" value="Cancel"/>
            </apex:pageBlockButtons>
            
            <apex:pageMessages showDetail="true" id="errors"/>
    
            <apex:pageBlockSection title="Location Details" columns="1">
                <div id="dialog_edit" title="Edit Location">
                    <input type="hidden" id="context_id" />
                    <div>
                        <div>
                            <span class="inlineLabel fixedWidth50">Street:</span>
                            <input type="text" id="context_street" value="{!param_street}" size="50"/><input type="button" value="Geocode Address" onclick="geocodeAddress()" class="btn"/>
                        </div>
                        <div>
                            <span class="inlineLabel fixedWidth50">City: </span>
                            <input type="text" id="context_city" value="{!param_city}" size="30"/> 
                            <span class="inlineLabel fixedWidth50">State: </span><input type="text" id="context_state" value="{!param_state}" size="2"/>
                            <span class="inlineLabel fixedWidth50">Zip: </span><input type="text" id="context_zip" value="{!param_zip}" size="10"/>
                        </div>
                        <hr/>
                        <div>Double click a location, drag pin, or enter lat/lng values to edit</div>
                        <div>
                            <label>Latitude:</label>
                            <input type="text" id="context_lat" onchange="updateLatLng()" value="{!property.location__latitude__s}"/> 
                            <label>Longitude:</label>
                            <input type="text" id="context_lng" value="{!property.location__longitude__s}" onchange="updateLatLng()"/>
                            <div style="vertical-align:top;">
                                Recurring Notes: <textarea id="context_notes" cols="72" rows="3">{!param_notes}</textarea>
                            </div>
                        </div>
                        <div id="geocodeStatusContainer">
                            <span id="geocodeStatus">&nbsp;</span><span id="geocodeStatusIcon">&nbsp;</span>
                            <input type="hidden" id="county" />
                        </div>
                    </div>
                </div>
                <div id="map_container"> 
                    <div id="map_div"></div>
                </div>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
    <apex:includeScript value="{!URLFOR($Resource.Geocoding_JS)}"/>
</apex:page>