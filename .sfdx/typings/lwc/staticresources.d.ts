declare module "@salesforce/resourceUrl/AMImages" {
    var AMImages: string;
    export default AMImages;
}
declare module "@salesforce/resourceUrl/AYSLogo" {
    var AYSLogo: string;
    export default AYSLogo;
}
declare module "@salesforce/resourceUrl/BlockUI" {
    var BlockUI: string;
    export default BlockUI;
}
declare module "@salesforce/resourceUrl/CJSKTHEME" {
    var CJSKTHEME: string;
    export default CJSKTHEME;
}
declare module "@salesforce/resourceUrl/CheckboxEnabling" {
    var CheckboxEnabling: string;
    export default CheckboxEnabling;
}
declare module "@salesforce/resourceUrl/DDSlick" {
    var DDSlick: string;
    export default DDSlick;
}
declare module "@salesforce/resourceUrl/DarkBlueDESIGNSYSTEM" {
    var DarkBlueDESIGNSYSTEM: string;
    export default DarkBlueDESIGNSYSTEM;
}
declare module "@salesforce/resourceUrl/DeepestBluestDESIGNSYSTEM" {
    var DeepestBluestDESIGNSYSTEM: string;
    export default DeepestBluestDESIGNSYSTEM;
}
declare module "@salesforce/resourceUrl/EC2Console_RebootImage" {
    var EC2Console_RebootImage: string;
    export default EC2Console_RebootImage;
}
declare module "@salesforce/resourceUrl/EC2Console_ShutdownImage" {
    var EC2Console_ShutdownImage: string;
    export default EC2Console_ShutdownImage;
}
declare module "@salesforce/resourceUrl/EC2Console_StartImage" {
    var EC2Console_StartImage: string;
    export default EC2Console_StartImage;
}
declare module "@salesforce/resourceUrl/FastMarkerOverlay" {
    var FastMarkerOverlay: string;
    export default FastMarkerOverlay;
}
declare module "@salesforce/resourceUrl/Geocoding_CSS" {
    var Geocoding_CSS: string;
    export default Geocoding_CSS;
}
declare module "@salesforce/resourceUrl/Geocoding_JS" {
    var Geocoding_JS: string;
    export default Geocoding_JS;
}
declare module "@salesforce/resourceUrl/GoogleMapsInternational" {
    var GoogleMapsInternational: string;
    export default GoogleMapsInternational;
}
declare module "@salesforce/resourceUrl/Ink2DESIGNSYSTEM" {
    var Ink2DESIGNSYSTEM: string;
    export default Ink2DESIGNSYSTEM;
}
declare module "@salesforce/resourceUrl/InstReview_CSS" {
    var InstReview_CSS: string;
    export default InstReview_CSS;
}
declare module "@salesforce/resourceUrl/InstReview_JS" {
    var InstReview_JS: string;
    export default InstReview_JS;
}
declare module "@salesforce/resourceUrl/InstReview_Warning" {
    var InstReview_Warning: string;
    export default InstReview_Warning;
}
declare module "@salesforce/resourceUrl/InvoicingPdf_CSS" {
    var InvoicingPdf_CSS: string;
    export default InvoicingPdf_CSS;
}
declare module "@salesforce/resourceUrl/InvoicingPdf_CSS_TA" {
    var InvoicingPdf_CSS_TA: string;
    export default InvoicingPdf_CSS_TA;
}
declare module "@salesforce/resourceUrl/Invoicing_CSS" {
    var Invoicing_CSS: string;
    export default Invoicing_CSS;
}
declare module "@salesforce/resourceUrl/Invoicing_JS" {
    var Invoicing_JS: string;
    export default Invoicing_JS;
}
declare module "@salesforce/resourceUrl/LinkForce" {
    var LinkForce: string;
    export default LinkForce;
}
declare module "@salesforce/resourceUrl/Logo4567687" {
    var Logo4567687: string;
    export default Logo4567687;
}
declare module "@salesforce/resourceUrl/MassCommitmentPageJavaScript" {
    var MassCommitmentPageJavaScript: string;
    export default MassCommitmentPageJavaScript;
}
declare module "@salesforce/resourceUrl/MassCommitmentPageResource" {
    var MassCommitmentPageResource: string;
    export default MassCommitmentPageResource;
}
declare module "@salesforce/resourceUrl/MegaSearch" {
    var MegaSearch: string;
    export default MegaSearch;
}
declare module "@salesforce/resourceUrl/Multiselect" {
    var Multiselect: string;
    export default Multiselect;
}
declare module "@salesforce/resourceUrl/NoPropertyPhoto" {
    var NoPropertyPhoto: string;
    export default NoPropertyPhoto;
}
declare module "@salesforce/resourceUrl/NorthsightCompLogo" {
    var NorthsightCompLogo: string;
    export default NorthsightCompLogo;
}
declare module "@salesforce/resourceUrl/NorthsightLogo" {
    var NorthsightLogo: string;
    export default NorthsightLogo;
}
declare module "@salesforce/resourceUrl/OAM_AutoSearchImage" {
    var OAM_AutoSearchImage: string;
    export default OAM_AutoSearchImage;
}
declare module "@salesforce/resourceUrl/OAM_CSS" {
    var OAM_CSS: string;
    export default OAM_CSS;
}
declare module "@salesforce/resourceUrl/OAM_JS" {
    var OAM_JS: string;
    export default OAM_JS;
}
declare module "@salesforce/resourceUrl/OAM_TableSorter" {
    var OAM_TableSorter: string;
    export default OAM_TableSorter;
}
declare module "@salesforce/resourceUrl/Overlay" {
    var Overlay: string;
    export default Overlay;
}
declare module "@salesforce/resourceUrl/PHPTest1" {
    var PHPTest1: string;
    export default PHPTest1;
}
declare module "@salesforce/resourceUrl/PageBlockTableEnhancerResources" {
    var PageBlockTableEnhancerResources: string;
    export default PageBlockTableEnhancerResources;
}
declare module "@salesforce/resourceUrl/PaperMod" {
    var PaperMod: string;
    export default PaperMod;
}
declare module "@salesforce/resourceUrl/PictureUploader" {
    var PictureUploader: string;
    export default PictureUploader;
}
declare module "@salesforce/resourceUrl/PropertyPageNewSKTHEME" {
    var PropertyPageNewSKTHEME: string;
    export default PropertyPageNewSKTHEME;
}
declare module "@salesforce/resourceUrl/RSSFeed" {
    var RSSFeed: string;
    export default RSSFeed;
}
declare module "@salesforce/resourceUrl/RSSJquery" {
    var RSSJquery: string;
    export default RSSJquery;
}
declare module "@salesforce/resourceUrl/Red" {
    var Red: string;
    export default Red;
}
declare module "@salesforce/resourceUrl/RouteJS1" {
    var RouteJS1: string;
    export default RouteJS1;
}
declare module "@salesforce/resourceUrl/SFBTest" {
    var SFBTest: string;
    export default SFBTest;
}
declare module "@salesforce/resourceUrl/SM199TestData" {
    var SM199TestData: string;
    export default SM199TestData;
}
declare module "@salesforce/resourceUrl/SR201702091551PNG" {
    var SR201702091551PNG: string;
    export default SR201702091551PNG;
}
declare module "@salesforce/resourceUrl/SR201702091618PNG" {
    var SR201702091618PNG: string;
    export default SR201702091618PNG;
}
declare module "@salesforce/resourceUrl/SiteSamples" {
    var SiteSamples: string;
    export default SiteSamples;
}
declare module "@salesforce/resourceUrl/SlickGrid" {
    var SlickGrid: string;
    export default SlickGrid;
}
declare module "@salesforce/resourceUrl/Slickgrid_2_3_2" {
    var Slickgrid_2_3_2: string;
    export default Slickgrid_2_3_2;
}
declare module "@salesforce/resourceUrl/TSP" {
    var TSP: string;
    export default TSP;
}
declare module "@salesforce/resourceUrl/TTSTheme1SKTHEME" {
    var TTSTheme1SKTHEME: string;
    export default TTSTheme1SKTHEME;
}
declare module "@salesforce/resourceUrl/TTSTheme2SKTHEME" {
    var TTSTheme2SKTHEME: string;
    export default TTSTheme2SKTHEME;
}
declare module "@salesforce/resourceUrl/TableSorter" {
    var TableSorter: string;
    export default TableSorter;
}
declare module "@salesforce/resourceUrl/TimeZoneMap" {
    var TimeZoneMap: string;
    export default TimeZoneMap;
}
declare module "@salesforce/resourceUrl/UtilJS" {
    var UtilJS: string;
    export default UtilJS;
}
declare module "@salesforce/resourceUrl/VendorTruck" {
    var VendorTruck: string;
    export default VendorTruck;
}
declare module "@salesforce/resourceUrl/VendorTruckSmall" {
    var VendorTruckSmall: string;
    export default VendorTruckSmall;
}
declare module "@salesforce/resourceUrl/Vendor_Portal" {
    var Vendor_Portal: string;
    export default Vendor_Portal;
}
declare module "@salesforce/resourceUrl/Vendor_Portal_Logo" {
    var Vendor_Portal_Logo: string;
    export default Vendor_Portal_Logo;
}
declare module "@salesforce/resourceUrl/ViewInvoices_CSS" {
    var ViewInvoices_CSS: string;
    export default ViewInvoices_CSS;
}
declare module "@salesforce/resourceUrl/ViewInvoices_JS" {
    var ViewInvoices_JS: string;
    export default ViewInvoices_JS;
}
declare module "@salesforce/resourceUrl/YUI" {
    var YUI: string;
    export default YUI;
}
declare module "@salesforce/resourceUrl/awssdk_2_7_6" {
    var awssdk_2_7_6: string;
    export default awssdk_2_7_6;
}
declare module "@salesforce/resourceUrl/clone_CJSKTHEME" {
    var clone_CJSKTHEME: string;
    export default clone_CJSKTHEME;
}
declare module "@salesforce/resourceUrl/clone_TTSTheme1SKTHEME" {
    var clone_TTSTheme1SKTHEME: string;
    export default clone_TTSTheme1SKTHEME;
}
declare module "@salesforce/resourceUrl/clone_clone_TTSTheme1SKTHEME" {
    var clone_clone_TTSTheme1SKTHEME: string;
    export default clone_clone_TTSTheme1SKTHEME;
}
declare module "@salesforce/resourceUrl/favorite" {
    var favorite: string;
    export default favorite;
}
declare module "@salesforce/resourceUrl/geoxml3" {
    var geoxml3: string;
    export default geoxml3;
}
declare module "@salesforce/resourceUrl/green" {
    var green: string;
    export default green;
}
declare module "@salesforce/resourceUrl/infobox" {
    var infobox: string;
    export default infobox;
}
declare module "@salesforce/resourceUrl/jQueryUI" {
    var jQueryUI: string;
    export default jQueryUI;
}
declare module "@salesforce/resourceUrl/jSignatureMaster" {
    var jSignatureMaster: string;
    export default jSignatureMaster;
}
declare module "@salesforce/resourceUrl/jquery" {
    var jquery: string;
    export default jquery;
}
declare module "@salesforce/resourceUrl/jqueryMultiStatus" {
    var jqueryMultiStatus: string;
    export default jqueryMultiStatus;
}
declare module "@salesforce/resourceUrl/jquery_1_10_2" {
    var jquery_1_10_2: string;
    export default jquery_1_10_2;
}
declare module "@salesforce/resourceUrl/jspdf" {
    var jspdf: string;
    export default jspdf;
}
declare module "@salesforce/resourceUrl/mBlazonryComponents" {
    var mBlazonryComponents: string;
    export default mBlazonryComponents;
}
declare module "@salesforce/resourceUrl/markerLabel" {
    var markerLabel: string;
    export default markerLabel;
}
declare module "@salesforce/resourceUrl/markerwithlabel" {
    var markerwithlabel: string;
    export default markerwithlabel;
}
declare module "@salesforce/resourceUrl/mdslds212" {
    var mdslds212: string;
    export default mdslds212;
}
declare module "@salesforce/resourceUrl/pixel" {
    var pixel: string;
    export default pixel;
}
declare module "@salesforce/resourceUrl/progressindicatorpack" {
    var progressindicatorpack: string;
    export default progressindicatorpack;
}
declare module "@salesforce/resourceUrl/routeJS" {
    var routeJS: string;
    export default routeJS;
}
declare module "@salesforce/resourceUrl/ruler" {
    var ruler: string;
    export default ruler;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4z33000000TYLHAA4_ACCT_PAR_Detail" {
    var skuid_pgsp_a4z33000000TYLHAA4_ACCT_PAR_Detail: string;
    export default skuid_pgsp_a4z33000000TYLHAA4_ACCT_PAR_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4z33000000TYSYAA4_ACCT_PAR_Create" {
    var skuid_pgsp_a4z33000000TYSYAA4_ACCT_PAR_Create: string;
    export default skuid_pgsp_a4z33000000TYSYAA4_ACCT_PAR_Create;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4z33000000TYvMAAW_OPS_Invoice_Detail" {
    var skuid_pgsp_a4z33000000TYvMAAW_OPS_Invoice_Detail: string;
    export default skuid_pgsp_a4z33000000TYvMAAW_OPS_Invoice_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4z33000000TZ6WAAW_OPS_ContactVendor_De" {
    var skuid_pgsp_a4z33000000TZ6WAAW_OPS_ContactVendor_De: string;
    export default skuid_pgsp_a4z33000000TZ6WAAW_OPS_ContactVendor_De;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4z33000000TZ9ZAAW_ACCT_Property_Invoic" {
    var skuid_pgsp_a4z33000000TZ9ZAAW_ACCT_Property_Invoic: string;
    export default skuid_pgsp_a4z33000000TZ9ZAAW_ACCT_Property_Invoic;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4z33000000TZIJAA4_OPS_Property_WorkOrd" {
    var skuid_pgsp_a4z33000000TZIJAA4_OPS_Property_WorkOrd: string;
    export default skuid_pgsp_a4z33000000TZIJAA4_OPS_Property_WorkOrd;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4z33000000TZPOAA4_OPS_Property_CodeVio" {
    var skuid_pgsp_a4z33000000TZPOAA4_OPS_Property_CodeVio: string;
    export default skuid_pgsp_a4z33000000TZPOAA4_OPS_Property_CodeVio;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf300000004zIAAQ_OPS_Property_Detail" {
    var skuid_pgsp_a4zf300000004zIAAQ_OPS_Property_Detail: string;
    export default skuid_pgsp_a4zf300000004zIAAQ_OPS_Property_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf300000004zwAAA_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf300000004zwAAA_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf300000004zwAAA_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009EK3AAM_OPS_ImageLink_Relati" {
    var skuid_pgsp_a4zf30000009EK3AAM_OPS_ImageLink_Relati: string;
    export default skuid_pgsp_a4zf30000009EK3AAM_OPS_ImageLink_Relati;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009EdqAAE_OPS_Property_WorkOrd" {
    var skuid_pgsp_a4zf30000009EdqAAE_OPS_Property_WorkOrd: string;
    export default skuid_pgsp_a4zf30000009EdqAAE_OPS_Property_WorkOrd;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009EfcAAE_Ops_BId_Update_20190" {
    var skuid_pgsp_a4zf30000009EfcAAE_Ops_BId_Update_20190: string;
    export default skuid_pgsp_a4zf30000009EfcAAE_Ops_BId_Update_20190;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009EfcAAE_Ops_Bid_Detail_20190" {
    var skuid_pgsp_a4zf30000009EfcAAE_Ops_Bid_Detail_20190: string;
    export default skuid_pgsp_a4zf30000009EfcAAE_Ops_Bid_Detail_20190;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009EhEAAU_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf30000009EhEAAU_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf30000009EhEAAU_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009EkOAAU_Ops_Bid_Detail_20190" {
    var skuid_pgsp_a4zf30000009EkOAAU_Ops_Bid_Detail_20190: string;
    export default skuid_pgsp_a4zf30000009EkOAAU_Ops_Bid_Detail_20190;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009EmBAAU_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf30000009EmBAAU_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf30000009EmBAAU_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009EonAAE_OPS_Property_Detail" {
    var skuid_pgsp_a4zf30000009EonAAE_OPS_Property_Detail: string;
    export default skuid_pgsp_a4zf30000009EonAAE_OPS_Property_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009F1rAAE_OPS_Property_Recurri" {
    var skuid_pgsp_a4zf30000009F1rAAE_OPS_Property_Recurri: string;
    export default skuid_pgsp_a4zf30000009F1rAAE_OPS_Property_Recurri;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009F2uAAE_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf30000009F2uAAE_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf30000009F2uAAE_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009F2zAAE_ImageLinks_Include" {
    var skuid_pgsp_a4zf30000009F2zAAE_ImageLinks_Include: string;
    export default skuid_pgsp_a4zf30000009F2zAAE_ImageLinks_Include;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009F2zAAE_OPS_ImageLinks_Inclu" {
    var skuid_pgsp_a4zf30000009F2zAAE_OPS_ImageLinks_Inclu: string;
    export default skuid_pgsp_a4zf30000009F2zAAE_OPS_ImageLinks_Inclu;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009FhMAAU_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf30000009FhMAAU_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf30000009FhMAAU_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf30000009G2PAAU_EXT_WO_Queue" {
    var skuid_pgsp_a4zf30000009G2PAAU_EXT_WO_Queue: string;
    export default skuid_pgsp_a4zf30000009G2PAAU_EXT_WO_Queue;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcwGAAS_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000CcwGAAS_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000CcwGAAS_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcwQAAS_OPS_Account_Detail_U" {
    var skuid_pgsp_a4zf3000000CcwQAAS_OPS_Account_Detail_U: string;
    export default skuid_pgsp_a4zf3000000CcwQAAS_OPS_Account_Detail_U;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcwVAAS_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000CcwVAAS_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000CcwVAAS_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcwfAAC_OPS_Loss_Evaluation" {
    var skuid_pgsp_a4zf3000000CcwfAAC_OPS_Loss_Evaluation: string;
    export default skuid_pgsp_a4zf3000000CcwfAAC_OPS_Loss_Evaluation;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcwkAAC_OPS_WorkOrder_Photos" {
    var skuid_pgsp_a4zf3000000CcwkAAC_OPS_WorkOrder_Photos: string;
    export default skuid_pgsp_a4zf3000000CcwkAAC_OPS_WorkOrder_Photos;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcwzAAC_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000CcwzAAC_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000CcwzAAC_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000Ccx9AAC_OPS_Property_WorkOrd" {
    var skuid_pgsp_a4zf3000000Ccx9AAC_OPS_Property_WorkOrd: string;
    export default skuid_pgsp_a4zf3000000Ccx9AAC_OPS_Property_WorkOrd;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcxEAAS_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000CcxEAAS_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000CcxEAAS_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcxJAAS_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000CcxJAAS_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000CcxJAAS_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcxYAAS_OPS_WorkOrder_Inspec" {
    var skuid_pgsp_a4zf3000000CcxYAAS_OPS_WorkOrder_Inspec: string;
    export default skuid_pgsp_a4zf3000000CcxYAAS_OPS_WorkOrder_Inspec;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcxdAAC_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000CcxdAAC_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000CcxdAAC_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcxiAAC_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000CcxiAAC_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000CcxiAAC_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcxnAAC_OPS_ImageLink_Relati" {
    var skuid_pgsp_a4zf3000000CcxnAAC_OPS_ImageLink_Relati: string;
    export default skuid_pgsp_a4zf3000000CcxnAAC_OPS_ImageLink_Relati;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcxsAAC_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000CcxsAAC_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000CcxsAAC_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000Ccy2AAC_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000Ccy2AAC_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000Ccy2AAC_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000CcyCAAS_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000CcyCAAS_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000CcyCAAS_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000Cd9LAAS_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000Cd9LAAS_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000Cd9LAAS_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000Cd9QAAS_OPS_ImageLink_Relati" {
    var skuid_pgsp_a4zf3000000Cd9QAAS_OPS_ImageLink_Relati: string;
    export default skuid_pgsp_a4zf3000000Cd9QAAS_OPS_ImageLink_Relati;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000Cd9VAAS_Temp_Contact_Daypay" {
    var skuid_pgsp_a4zf3000000Cd9VAAS_Temp_Contact_Daypay: string;
    export default skuid_pgsp_a4zf3000000Cd9VAAS_Temp_Contact_Daypay;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000Cd9pAAC_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000Cd9pAAC_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000Cd9pAAC_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PSysAAG_OPS_ImageLink_Relati" {
    var skuid_pgsp_a4zf3000000PSysAAG_OPS_ImageLink_Relati: string;
    export default skuid_pgsp_a4zf3000000PSysAAG_OPS_ImageLink_Relati;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PSyxAAG_CLT_WorkOrder_Inspec" {
    var skuid_pgsp_a4zf3000000PSyxAAG_CLT_WorkOrder_Inspec: string;
    export default skuid_pgsp_a4zf3000000PSyxAAG_CLT_WorkOrder_Inspec;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PSz2AAG_EXT_Inst_Editor" {
    var skuid_pgsp_a4zf3000000PSz2AAG_EXT_Inst_Editor: string;
    export default skuid_pgsp_a4zf3000000PSz2AAG_EXT_Inst_Editor;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PSzCAAW_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000PSzCAAW_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000PSzCAAW_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PSzHAAW_OPS_ImageLink_Relati" {
    var skuid_pgsp_a4zf3000000PSzHAAW_OPS_ImageLink_Relati: string;
    export default skuid_pgsp_a4zf3000000PSzHAAW_OPS_ImageLink_Relati;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PSzMAAW_OPS_Disable_Include" {
    var skuid_pgsp_a4zf3000000PSzMAAW_OPS_Disable_Include: string;
    export default skuid_pgsp_a4zf3000000PSzMAAW_OPS_Disable_Include;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PSzRAAW_Homesteps_Request_Fo" {
    var skuid_pgsp_a4zf3000000PSzRAAW_Homesteps_Request_Fo: string;
    export default skuid_pgsp_a4zf3000000PSzRAAW_Homesteps_Request_Fo;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PSzWAAW_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000PSzWAAW_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000PSzWAAW_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PSzbAAG_OPS_WorkOrder_Inspec" {
    var skuid_pgsp_a4zf3000000PSzbAAG_OPS_WorkOrder_Inspec: string;
    export default skuid_pgsp_a4zf3000000PSzbAAG_OPS_WorkOrder_Inspec;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PSzgAAG_View_All_Photos" {
    var skuid_pgsp_a4zf3000000PSzgAAG_View_All_Photos: string;
    export default skuid_pgsp_a4zf3000000PSzgAAG_View_All_Photos;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PTMGAA4_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000PTMGAA4_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000PTMGAA4_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PTTrAAO_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000PTTrAAO_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000PTTrAAO_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PTpqAAG_OPS_Property_LossWiz" {
    var skuid_pgsp_a4zf3000000PTpqAAG_OPS_Property_LossWiz: string;
    export default skuid_pgsp_a4zf3000000PTpqAAG_OPS_Property_LossWiz;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PTtnAAG_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000PTtnAAG_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000PTtnAAG_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PUUbAAO_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000PUUbAAO_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000PUUbAAO_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PUvpAAG_Ops_Bid_Detail_20190" {
    var skuid_pgsp_a4zf3000000PUvpAAG_Ops_Bid_Detail_20190: string;
    export default skuid_pgsp_a4zf3000000PUvpAAG_Ops_Bid_Detail_20190;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PV9AAAW_Ops_Bid_Detail_20190" {
    var skuid_pgsp_a4zf3000000PV9AAAW_Ops_Bid_Detail_20190: string;
    export default skuid_pgsp_a4zf3000000PV9AAAW_Ops_Bid_Detail_20190;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PVM6AAO_Ops_Bid_Item_Detail" {
    var skuid_pgsp_a4zf3000000PVM6AAO_Ops_Bid_Item_Detail: string;
    export default skuid_pgsp_a4zf3000000PVM6AAO_Ops_Bid_Item_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PVRCAA4_190823_OPS_Image_Lis" {
    var skuid_pgsp_a4zf3000000PVRCAA4_190823_OPS_Image_Lis: string;
    export default skuid_pgsp_a4zf3000000PVRCAA4_190823_OPS_Image_Lis;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PVXvAAO_OPS_WorkOrder_Winter" {
    var skuid_pgsp_a4zf3000000PVXvAAO_OPS_WorkOrder_Winter: string;
    export default skuid_pgsp_a4zf3000000PVXvAAO_OPS_WorkOrder_Winter;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PW9uAAG_EXT_WO_Queue_clone" {
    var skuid_pgsp_a4zf3000000PW9uAAG_EXT_WO_Queue_clone: string;
    export default skuid_pgsp_a4zf3000000PW9uAAG_EXT_WO_Queue_clone;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PWmCAAW_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000PWmCAAW_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000PWmCAAW_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000PX7IAAW_Ops_Bid_Detail_20190" {
    var skuid_pgsp_a4zf3000000PX7IAAW_Ops_Bid_Detail_20190: string;
    export default skuid_pgsp_a4zf3000000PX7IAAW_Ops_Bid_Detail_20190;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaNeAAK_OPS_Property_Detail" {
    var skuid_pgsp_a4zf3000000TaNeAAK_OPS_Property_Detail: string;
    export default skuid_pgsp_a4zf3000000TaNeAAK_OPS_Property_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaNtAAK_OPS_Invoice_Debris_P" {
    var skuid_pgsp_a4zf3000000TaNtAAK_OPS_Invoice_Debris_P: string;
    export default skuid_pgsp_a4zf3000000TaNtAAK_OPS_Invoice_Debris_P;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaNyAAK_OPS_Invoice_Photos_S" {
    var skuid_pgsp_a4zf3000000TaNyAAK_OPS_Invoice_Photos_S: string;
    export default skuid_pgsp_a4zf3000000TaNyAAK_OPS_Invoice_Photos_S;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaOSAA0_Saasli_Vendor_Applic" {
    var skuid_pgsp_a4zf3000000TaOSAA0_Saasli_Vendor_Applic: string;
    export default skuid_pgsp_a4zf3000000TaOSAA0_Saasli_Vendor_Applic;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaOwAAK_Vendor_Application_A" {
    var skuid_pgsp_a4zf3000000TaOwAAK_Vendor_Application_A: string;
    export default skuid_pgsp_a4zf3000000TaOwAAK_Vendor_Application_A;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaPkAAK_OPS_Inspection_Form" {
    var skuid_pgsp_a4zf3000000TaPkAAK_OPS_Inspection_Form: string;
    export default skuid_pgsp_a4zf3000000TaPkAAK_OPS_Inspection_Form;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaPzAAK_OPS_PCR_Form_Create" {
    var skuid_pgsp_a4zf3000000TaPzAAK_OPS_PCR_Form_Create: string;
    export default skuid_pgsp_a4zf3000000TaPzAAK_OPS_PCR_Form_Create;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaR2AAK_OPS_WorkCode_Detail" {
    var skuid_pgsp_a4zf3000000TaR2AAK_OPS_WorkCode_Detail: string;
    export default skuid_pgsp_a4zf3000000TaR2AAK_OPS_WorkCode_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaRgAAK_OPS_Invoice_Photos_S" {
    var skuid_pgsp_a4zf3000000TaRgAAK_OPS_Invoice_Photos_S: string;
    export default skuid_pgsp_a4zf3000000TaRgAAK_OPS_Invoice_Photos_S;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaS0AAK_OPS_ImageLink_Relati" {
    var skuid_pgsp_a4zf3000000TaS0AAK_OPS_ImageLink_Relati: string;
    export default skuid_pgsp_a4zf3000000TaS0AAK_OPS_ImageLink_Relati;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaSKAA0_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaSKAA0_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaSKAA0_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaThAAK_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaThAAK_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaThAAK_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaULAA0_OPS_ContactVendor_De" {
    var skuid_pgsp_a4zf3000000TaULAA0_OPS_ContactVendor_De: string;
    export default skuid_pgsp_a4zf3000000TaULAA0_OPS_ContactVendor_De;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaUVAA0_OPS_ImageLink_Relati" {
    var skuid_pgsp_a4zf3000000TaUVAA0_OPS_ImageLink_Relati: string;
    export default skuid_pgsp_a4zf3000000TaUVAA0_OPS_ImageLink_Relati;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaUfAAK_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaUfAAK_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaUfAAK_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaUpAAK_OPS_Invoice_Photos_S" {
    var skuid_pgsp_a4zf3000000TaUpAAK_OPS_Invoice_Photos_S: string;
    export default skuid_pgsp_a4zf3000000TaUpAAK_OPS_Invoice_Photos_S;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaUuAAK_EXT_ImageLink_Relati" {
    var skuid_pgsp_a4zf3000000TaUuAAK_EXT_ImageLink_Relati: string;
    export default skuid_pgsp_a4zf3000000TaUuAAK_EXT_ImageLink_Relati;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaV9AAK_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000TaV9AAK_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000TaV9AAK_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaVJAA0_OPS_ContactVendor_De" {
    var skuid_pgsp_a4zf3000000TaVJAA0_OPS_ContactVendor_De: string;
    export default skuid_pgsp_a4zf3000000TaVJAA0_OPS_ContactVendor_De;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaVOAA0_OPS_Property_Part_B" {
    var skuid_pgsp_a4zf3000000TaVOAA0_OPS_Property_Part_B: string;
    export default skuid_pgsp_a4zf3000000TaVOAA0_OPS_Property_Part_B;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaVdAAK_EXT_ImageLink_Relati" {
    var skuid_pgsp_a4zf3000000TaVdAAK_EXT_ImageLink_Relati: string;
    export default skuid_pgsp_a4zf3000000TaVdAAK_EXT_ImageLink_Relati;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaViAAK_OPS_Property_Part_B" {
    var skuid_pgsp_a4zf3000000TaViAAK_OPS_Property_Part_B: string;
    export default skuid_pgsp_a4zf3000000TaViAAK_OPS_Property_Part_B;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaVnAAK_OPS_ImageLink_Detail" {
    var skuid_pgsp_a4zf3000000TaVnAAK_OPS_ImageLink_Detail: string;
    export default skuid_pgsp_a4zf3000000TaVnAAK_OPS_ImageLink_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaVsAAK_OPS_ImageLink_Relati" {
    var skuid_pgsp_a4zf3000000TaVsAAK_OPS_ImageLink_Relati: string;
    export default skuid_pgsp_a4zf3000000TaVsAAK_OPS_ImageLink_Relati;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaVxAAK_OPS_Invoice_Detail_U" {
    var skuid_pgsp_a4zf3000000TaVxAAK_OPS_Invoice_Detail_U: string;
    export default skuid_pgsp_a4zf3000000TaVxAAK_OPS_Invoice_Detail_U;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaW2AAK_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaW2AAK_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaW2AAK_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaW7AAK_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000TaW7AAK_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000TaW7AAK_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaWHAA0_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000TaWHAA0_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000TaWHAA0_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaWRAA0_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000TaWRAA0_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000TaWRAA0_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaWbAAK_OPS_Bid_Detail" {
    var skuid_pgsp_a4zf3000000TaWbAAK_OPS_Bid_Detail: string;
    export default skuid_pgsp_a4zf3000000TaWbAAK_OPS_Bid_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaWlAAK_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaWlAAK_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaWlAAK_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaWqAAK_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000TaWqAAK_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000TaWqAAK_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaXeAAK_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000TaXeAAK_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000TaXeAAK_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaXjAAK_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000TaXjAAK_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000TaXjAAK_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaXoAAK_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000TaXoAAK_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000TaXoAAK_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaXtAAK_OPS_WorkOrder_ImageL" {
    var skuid_pgsp_a4zf3000000TaXtAAK_OPS_WorkOrder_ImageL: string;
    export default skuid_pgsp_a4zf3000000TaXtAAK_OPS_WorkOrder_ImageL;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaY3AAK_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaY3AAK_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaY3AAK_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaY8AAK_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaY8AAK_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaY8AAK_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaYIAA0_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaYIAA0_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaYIAA0_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaYSAA0_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaYSAA0_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaYSAA0_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaYcAAK_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaYcAAK_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaYcAAK_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaYhAAK_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaYhAAK_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaYhAAK_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaYmAAK_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000TaYmAAK_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000TaYmAAK_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaYrAAK_OPS_Inspection_Form" {
    var skuid_pgsp_a4zf3000000TaYrAAK_OPS_Inspection_Form: string;
    export default skuid_pgsp_a4zf3000000TaYrAAK_OPS_Inspection_Form;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaYwAAK_OPS_Work_Order_Label" {
    var skuid_pgsp_a4zf3000000TaYwAAK_OPS_Work_Order_Label: string;
    export default skuid_pgsp_a4zf3000000TaYwAAK_OPS_Work_Order_Label;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaZ1AAK_OPS_WorkCode_Detail" {
    var skuid_pgsp_a4zf3000000TaZ1AAK_OPS_WorkCode_Detail: string;
    export default skuid_pgsp_a4zf3000000TaZ1AAK_OPS_WorkCode_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaZBAA0_Homesteps_Request_Fo" {
    var skuid_pgsp_a4zf3000000TaZBAA0_Homesteps_Request_Fo: string;
    export default skuid_pgsp_a4zf3000000TaZBAA0_Homesteps_Request_Fo;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaZBAA0_UI_Mockup_Request_Fo" {
    var skuid_pgsp_a4zf3000000TaZBAA0_UI_Mockup_Request_Fo: string;
    export default skuid_pgsp_a4zf3000000TaZBAA0_UI_Mockup_Request_Fo;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaZGAA0_OPS_Invoice_Tab" {
    var skuid_pgsp_a4zf3000000TaZGAA0_OPS_Invoice_Tab: string;
    export default skuid_pgsp_a4zf3000000TaZGAA0_OPS_Invoice_Tab;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaZLAA0_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000TaZLAA0_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000TaZLAA0_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaZVAA0_OPS_Invoice_Tab_Tabl" {
    var skuid_pgsp_a4zf3000000TaZVAA0_OPS_Invoice_Tab_Tabl: string;
    export default skuid_pgsp_a4zf3000000TaZVAA0_OPS_Invoice_Tab_Tabl;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaZaAAK_OPS_Invoice_Tab_clon" {
    var skuid_pgsp_a4zf3000000TaZaAAK_OPS_Invoice_Tab_clon: string;
    export default skuid_pgsp_a4zf3000000TaZaAAK_OPS_Invoice_Tab_clon;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000TaZfAAK_Homesteps_Request_Fo" {
    var skuid_pgsp_a4zf3000000TaZfAAK_Homesteps_Request_Fo: string;
    export default skuid_pgsp_a4zf3000000TaZfAAK_Homesteps_Request_Fo;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000XZCXAA4_OPS_Property_Detail" {
    var skuid_pgsp_a4zf3000000XZCXAA4_OPS_Property_Detail: string;
    export default skuid_pgsp_a4zf3000000XZCXAA4_OPS_Property_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000XZCcAAO_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000XZCcAAO_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000XZCcAAO_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000XZCwAAO_OPS_ImageGallery_Act" {
    var skuid_pgsp_a4zf3000000XZCwAAO_OPS_ImageGallery_Act: string;
    export default skuid_pgsp_a4zf3000000XZCwAAO_OPS_ImageGallery_Act;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000XZD6AAO_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000XZD6AAO_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000XZD6AAO_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000blu5AAA_OPS_QC_Wint" {
    var skuid_pgsp_a4zf3000000blu5AAA_OPS_QC_Wint: string;
    export default skuid_pgsp_a4zf3000000blu5AAA_OPS_QC_Wint;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000bluAAAQ_OPS_QC_Wint_Internal" {
    var skuid_pgsp_a4zf3000000bluAAAQ_OPS_QC_Wint_Internal: string;
    export default skuid_pgsp_a4zf3000000bluAAAQ_OPS_QC_Wint_Internal;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000bluFAAQ_OPS_QC_Wint_Internal" {
    var skuid_pgsp_a4zf3000000bluFAAQ_OPS_QC_Wint_Internal: string;
    export default skuid_pgsp_a4zf3000000bluFAAQ_OPS_QC_Wint_Internal;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000bluKAAQ_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000bluKAAQ_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000bluKAAQ_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000bluUAAQ_Ops_Property_Detail" {
    var skuid_pgsp_a4zf3000000bluUAAQ_Ops_Property_Detail: string;
    export default skuid_pgsp_a4zf3000000bluUAAQ_Ops_Property_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000bluZAAQ_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000bluZAAQ_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000bluZAAQ_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000blueAAA_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000blueAAA_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000blueAAA_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000blujAAA_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000blujAAA_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000blujAAA_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000blutAAA_OPS_Property_Detail" {
    var skuid_pgsp_a4zf3000000blutAAA_OPS_Property_Detail: string;
    export default skuid_pgsp_a4zf3000000blutAAA_OPS_Property_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000bluyAAA_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000bluyAAA_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000bluyAAA_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000blv3AAA_OPS_WorkOrder_Photos" {
    var skuid_pgsp_a4zf3000000blv3AAA_OPS_WorkOrder_Photos: string;
    export default skuid_pgsp_a4zf3000000blv3AAA_OPS_WorkOrder_Photos;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000blv8AAA_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000blv8AAA_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000blv8AAA_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000blvNAAQ_OPS_WorkOrder_ImageL" {
    var skuid_pgsp_a4zf3000000blvNAAQ_OPS_WorkOrder_ImageL: string;
    export default skuid_pgsp_a4zf3000000blvNAAQ_OPS_WorkOrder_ImageL;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000blvXAAQ_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000blvXAAQ_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000blvXAAQ_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyCUAAY_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000fyCUAAY_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000fyCUAAY_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyCZAAY_OPS_WorkOrder_Inspec" {
    var skuid_pgsp_a4zf3000000fyCZAAY_OPS_WorkOrder_Inspec: string;
    export default skuid_pgsp_a4zf3000000fyCZAAY_OPS_WorkOrder_Inspec;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyCeAAI_OPS_Property_CodeVio" {
    var skuid_pgsp_a4zf3000000fyCeAAI_OPS_Property_CodeVio: string;
    export default skuid_pgsp_a4zf3000000fyCeAAI_OPS_Property_CodeVio;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyCoAAI_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000fyCoAAI_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000fyCoAAI_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyCtAAI_Delete_Broken_Image" {
    var skuid_pgsp_a4zf3000000fyCtAAI_Delete_Broken_Image: string;
    export default skuid_pgsp_a4zf3000000fyCtAAI_Delete_Broken_Image;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyCyAAI_OPS_ContactVendor_De" {
    var skuid_pgsp_a4zf3000000fyCyAAI_OPS_ContactVendor_De: string;
    export default skuid_pgsp_a4zf3000000fyCyAAI_OPS_ContactVendor_De;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyD3AAI_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000fyD3AAI_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000fyD3AAI_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyD8AAI_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000fyD8AAI_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000fyD8AAI_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyDDAAY_OPS_Loss_Form_Printa" {
    var skuid_pgsp_a4zf3000000fyDDAAY_OPS_Loss_Form_Printa: string;
    export default skuid_pgsp_a4zf3000000fyDDAAY_OPS_Loss_Form_Printa;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyDIAAY_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000fyDIAAY_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000fyDIAAY_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyDXAAY_Test_Drag_And_Drop" {
    var skuid_pgsp_a4zf3000000fyDXAAY_Test_Drag_And_Drop: string;
    export default skuid_pgsp_a4zf3000000fyDXAAY_Test_Drag_And_Drop;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyDcAAI_OPS_Vendor_Score_Car" {
    var skuid_pgsp_a4zf3000000fyDcAAI_OPS_Vendor_Score_Car: string;
    export default skuid_pgsp_a4zf3000000fyDcAAI_OPS_Vendor_Score_Car;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyDmAAI_OPS_Loss_Evaluation" {
    var skuid_pgsp_a4zf3000000fyDmAAI_OPS_Loss_Evaluation: string;
    export default skuid_pgsp_a4zf3000000fyDmAAI_OPS_Loss_Evaluation;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyDrAAI_OPS_Loss_Evaluation" {
    var skuid_pgsp_a4zf3000000fyDrAAI_OPS_Loss_Evaluation: string;
    export default skuid_pgsp_a4zf3000000fyDrAAI_OPS_Loss_Evaluation;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyDwAAI_Saasli_Vendor_Applic" {
    var skuid_pgsp_a4zf3000000fyDwAAI_Saasli_Vendor_Applic: string;
    export default skuid_pgsp_a4zf3000000fyDwAAI_Saasli_Vendor_Applic;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyECAAY_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000fyECAAY_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000fyECAAY_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000fyEgAAI_OPS_Loss_Evaluation" {
    var skuid_pgsp_a4zf3000000fyEgAAI_OPS_Loss_Evaluation: string;
    export default skuid_pgsp_a4zf3000000fyEgAAI_OPS_Loss_Evaluation;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9jWAAQ_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000k9jWAAQ_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000k9jWAAQ_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9jbAAA_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000k9jbAAA_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000k9jbAAA_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9jqAAA_Vendor_WorkOrder_Det" {
    var skuid_pgsp_a4zf3000000k9jqAAA_Vendor_WorkOrder_Det: string;
    export default skuid_pgsp_a4zf3000000k9jqAAA_Vendor_WorkOrder_Det;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9kAAAQ_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000k9kAAAQ_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000k9kAAAQ_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9kFAAQ_Pruvan_Test_Page" {
    var skuid_pgsp_a4zf3000000k9kFAAQ_Pruvan_Test_Page: string;
    export default skuid_pgsp_a4zf3000000k9kFAAQ_Pruvan_Test_Page;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9keAAA_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000k9keAAA_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000k9keAAA_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9kjAAA_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000k9kjAAA_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000k9kjAAA_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9koAAA_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000k9koAAA_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000k9koAAA_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9ktAAA_OPS_ImageLink_Relati" {
    var skuid_pgsp_a4zf3000000k9ktAAA_OPS_ImageLink_Relati: string;
    export default skuid_pgsp_a4zf3000000k9ktAAA_OPS_ImageLink_Relati;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9l3AAA_20171023_SimplePhoto" {
    var skuid_pgsp_a4zf3000000k9l3AAA_20171023_SimplePhoto: string;
    export default skuid_pgsp_a4zf3000000k9l3AAA_20171023_SimplePhoto;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9l8AAA_OPS_WorkOrder_Detail" {
    var skuid_pgsp_a4zf3000000k9l8AAA_OPS_WorkOrder_Detail: string;
    export default skuid_pgsp_a4zf3000000k9l8AAA_OPS_WorkOrder_Detail;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9lIAAQ_OPS_Invoice_Photos_S" {
    var skuid_pgsp_a4zf3000000k9lIAAQ_OPS_Invoice_Photos_S: string;
    export default skuid_pgsp_a4zf3000000k9lIAAQ_OPS_Invoice_Photos_S;
}
declare module "@salesforce/resourceUrl/skuid_pgsp_a4zf3000000k9lSAAQ_Vendor_Portal_Link_v" {
    var skuid_pgsp_a4zf3000000k9lSAAQ_Vendor_Portal_Link_v: string;
    export default skuid_pgsp_a4zf3000000k9lSAAQ_Vendor_Portal_Link_v;
}
declare module "@salesforce/resourceUrl/stickyKitJs" {
    var stickyKitJs: string;
    export default stickyKitJs;
}
declare module "@salesforce/resourceUrl/test" {
    var test: string;
    export default test;
}
declare module "@salesforce/resourceUrl/vfFloatingHeaders" {
    var vfFloatingHeaders: string;
    export default vfFloatingHeaders;
}
declare module "@salesforce/resourceUrl/vfMassCommitmentJs" {
    var vfMassCommitmentJs: string;
    export default vfMassCommitmentJs;
}
declare module "@salesforce/resourceUrl/viewPhotos2016" {
    var viewPhotos2016: string;
    export default viewPhotos2016;
}
declare module "@salesforce/resourceUrl/yellow" {
    var yellow: string;
    export default yellow;
}
